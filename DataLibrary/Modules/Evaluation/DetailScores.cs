﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Evaluation.DetailScores
{

    [DataObject]
    [Serializable]
    public partial class EvaDetailScores
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[eva_DetailScores]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _scoreId;
        private System.Int32? _detailId;
        private System.Int32? _residentId;
        private System.Decimal? _july;
        private System.Decimal? _august;
        private System.Decimal? _september;
        private System.Decimal? _october;
        private System.Decimal? _quarter1;
        private System.Decimal? _november;
        private System.Decimal? _december;
        private System.Decimal? _january;
        private System.Decimal? _february;
        private System.Decimal? _quarter2;
        private System.Decimal? _march;
        private System.Decimal? _april;
        private System.Decimal? _may;
        private System.Decimal? _june;
        private System.Decimal? _quarter3;
        private System.Decimal? _average;
        private System.Int32? _lastActionBy;

        #endregion


        #region Properties
        public System.Int32? ScoreId
        {
            get
            {
                return _scoreId;
            }
            set
            {
                _scoreId = value;
            }
        }

        public System.Int32? DetailId
        {
            get
            {
                return _detailId;
            }
            set
            {
                _detailId = value;
            }
        }

        public System.Int32? ResidentId
        {
            get
            {
                return _residentId;
            }
            set
            {
                _residentId = value;
            }
        }

        public System.Decimal? July
        {
            get
            {
                return _july;
            }
            set
            {
                _july = value;
            }
        }

        public System.Decimal? August
        {
            get
            {
                return _august;
            }
            set
            {
                _august = value;
            }
        }

        public System.Decimal? September
        {
            get
            {
                return _september;
            }
            set
            {
                _september = value;
            }
        }

        public System.Decimal? October
        {
            get
            {
                return _october;
            }
            set
            {
                _october = value;
            }
        }

        public System.Decimal? Quarter1
        {
            get
            {
                return _quarter1;
            }
            set
            {
                _quarter1 = value;
            }
        }

        public System.Decimal? November
        {
            get
            {
                return _november;
            }
            set
            {
                _november = value;
            }
        }

        public System.Decimal? December
        {
            get
            {
                return _december;
            }
            set
            {
                _december = value;
            }
        }

        public System.Decimal? January
        {
            get
            {
                return _january;
            }
            set
            {
                _january = value;
            }
        }

        public System.Decimal? February
        {
            get
            {
                return _february;
            }
            set
            {
                _february = value;
            }
        }

        public System.Decimal? Quarter2
        {
            get
            {
                return _quarter2;
            }
            set
            {
                _quarter2 = value;
            }
        }

        public System.Decimal? March
        {
            get
            {
                return _march;
            }
            set
            {
                _march = value;
            }
        }

        public System.Decimal? April
        {
            get
            {
                return _april;
            }
            set
            {
                _april = value;
            }
        }

        public System.Decimal? May
        {
            get
            {
                return _may;
            }
            set
            {
                _may = value;
            }
        }

        public System.Decimal? June
        {
            get
            {
                return _june;
            }
            set
            {
                _june = value;
            }
        }

        public System.Decimal? Quarter3
        {
            get
            {
                return _quarter3;
            }
            set
            {
                _quarter3 = value;
            }
        }

        public System.Decimal? Average
        {
            get
            {
                return _average;
            }
            set
            {
                _average = value;
            }
        }

        public System.Int32? LastActionBy
        {
            get
            {
                return _lastActionBy;
            }
            set
            {
                _lastActionBy = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("ScoreId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("DetailId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ResidentId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("July", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("August", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("September", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("October", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("Quarter1", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("November", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("December", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("January", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("February", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("Quarter2", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("March", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("April", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("May", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("June", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("Quarter3", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("Average", typeof(System.Decimal));
            ds.Tables[TABLE_NAME].Columns.Add("LastActionBy", typeof(System.Int32));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (ScoreId == null)
                dr["ScoreId"] = DBNull.Value;
            else
                dr["ScoreId"] = ScoreId;

            if (DetailId == null)
                dr["DetailId"] = DBNull.Value;
            else
                dr["DetailId"] = DetailId;

            if (ResidentId == null)
                dr["ResidentId"] = DBNull.Value;
            else
                dr["ResidentId"] = ResidentId;

            if (July == null)
                dr["July"] = DBNull.Value;
            else
                dr["July"] = July;

            if (August == null)
                dr["August"] = DBNull.Value;
            else
                dr["August"] = August;

            if (September == null)
                dr["September"] = DBNull.Value;
            else
                dr["September"] = September;

            if (October == null)
                dr["October"] = DBNull.Value;
            else
                dr["October"] = October;

            if (Quarter1 == null)
                dr["Quarter1"] = DBNull.Value;
            else
                dr["Quarter1"] = Quarter1;

            if (November == null)
                dr["November"] = DBNull.Value;
            else
                dr["November"] = November;

            if (December == null)
                dr["December"] = DBNull.Value;
            else
                dr["December"] = December;

            if (January == null)
                dr["January"] = DBNull.Value;
            else
                dr["January"] = January;

            if (February == null)
                dr["February"] = DBNull.Value;
            else
                dr["February"] = February;

            if (Quarter2 == null)
                dr["Quarter2"] = DBNull.Value;
            else
                dr["Quarter2"] = Quarter2;

            if (March == null)
                dr["March"] = DBNull.Value;
            else
                dr["March"] = March;

            if (April == null)
                dr["April"] = DBNull.Value;
            else
                dr["April"] = April;

            if (May == null)
                dr["May"] = DBNull.Value;
            else
                dr["May"] = May;

            if (June == null)
                dr["June"] = DBNull.Value;
            else
                dr["June"] = June;

            if (Quarter3 == null)
                dr["Quarter3"] = DBNull.Value;
            else
                dr["Quarter3"] = Quarter3;

            if (Average == null)
                dr["Average"] = DBNull.Value;
            else
                dr["Average"] = Average;

            if (LastActionBy == null)
                dr["LastActionBy"] = DBNull.Value;
            else
                dr["LastActionBy"] = LastActionBy;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            ScoreId = dr["ScoreId"] != DBNull.Value ? Convert.ToInt32(dr["ScoreId"]) : ScoreId = null;
            DetailId = dr["DetailId"] != DBNull.Value ? Convert.ToInt32(dr["DetailId"]) : DetailId = null;
            ResidentId = dr["ResidentId"] != DBNull.Value ? Convert.ToInt32(dr["ResidentId"]) : ResidentId = null;
            July = dr["July"] != DBNull.Value ? Convert.ToDecimal(dr["July"]) : July = null;
            August = dr["August"] != DBNull.Value ? Convert.ToDecimal(dr["August"]) : August = null;
            September = dr["September"] != DBNull.Value ? Convert.ToDecimal(dr["September"]) : September = null;
            October = dr["October"] != DBNull.Value ? Convert.ToDecimal(dr["October"]) : October = null;
            Quarter1 = dr["Quarter1"] != DBNull.Value ? Convert.ToDecimal(dr["Quarter1"]) : Quarter1 = null;
            November = dr["November"] != DBNull.Value ? Convert.ToDecimal(dr["November"]) : November = null;
            December = dr["December"] != DBNull.Value ? Convert.ToDecimal(dr["December"]) : December = null;
            January = dr["January"] != DBNull.Value ? Convert.ToDecimal(dr["January"]) : January = null;
            February = dr["February"] != DBNull.Value ? Convert.ToDecimal(dr["February"]) : February = null;
            Quarter2 = dr["Quarter2"] != DBNull.Value ? Convert.ToDecimal(dr["Quarter2"]) : Quarter2 = null;
            March = dr["March"] != DBNull.Value ? Convert.ToDecimal(dr["March"]) : March = null;
            April = dr["April"] != DBNull.Value ? Convert.ToDecimal(dr["April"]) : April = null;
            May = dr["May"] != DBNull.Value ? Convert.ToDecimal(dr["May"]) : May = null;
            June = dr["June"] != DBNull.Value ? Convert.ToDecimal(dr["June"]) : June = null;
            Quarter3 = dr["Quarter3"] != DBNull.Value ? Convert.ToDecimal(dr["Quarter3"]) : Quarter3 = null;
            Average = dr["Average"] != DBNull.Value ? Convert.ToDecimal(dr["Average"]) : Average = null;
            LastActionBy = dr["LastActionBy"] != DBNull.Value ? Convert.ToInt32(dr["LastActionBy"]) : LastActionBy = null;
        }

        public static EvaDetailScores[] MapFrom(DataSet ds)
        {
            List<EvaDetailScores> objects;


            // Initialise Collection.
            objects = new List<EvaDetailScores>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[eva_DetailScores] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[eva_DetailScores] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                EvaDetailScores instance = new EvaDetailScores();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static EvaDetailScores Get(System.Int32 scoreId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            EvaDetailScores instance;


            instance = new EvaDetailScores();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].EvaDetailScores_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, scoreId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get EvaDetailScores ID:" + scoreId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.Int32? detailId, System.Int32? residentId, System.Decimal? july, System.Decimal? august, System.Decimal? september, System.Decimal? october, System.Decimal? november, System.Decimal? december, System.Decimal? january, System.Decimal? february, System.Decimal? march, System.Decimal? april, System.Decimal? may, System.Decimal? june, System.Int32? lastActionBy, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].EvaDetailScores_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, detailId, residentId, july, august, september, october, november, december, january, february, march, april, may, june, lastActionBy);

            if (transaction == null)
                this.ScoreId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.ScoreId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.Int32? detailId, System.Int32? residentId, System.Decimal? july, System.Decimal? august, System.Decimal? september, System.Decimal? october, System.Decimal? november, System.Decimal? december, System.Decimal? january, System.Decimal? february, System.Decimal? march, System.Decimal? april, System.Decimal? may, System.Decimal? june, System.Int32? lastActionBy)
        {
            Insert(detailId, residentId, july, august, september, october, november, december, january, february, march, april, may, june, lastActionBy, null);
        }
        /// <summary>
        /// Insert current EvaDetailScores to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(DetailId, ResidentId, July, August, September, October, November, December, January, February, March, April, May, June, LastActionBy, transaction);
        }

        /// <summary>
        /// Insert current EvaDetailScores to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? scoreId, System.Int32? detailId, System.Int32? residentId, System.Decimal? july, System.Decimal? august, System.Decimal? september, System.Decimal? october, System.Decimal? quarter1, System.Decimal? november, System.Decimal? december, System.Decimal? january, System.Decimal? february, System.Decimal? quarter2, System.Decimal? march, System.Decimal? april, System.Decimal? may, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].EvaDetailScores_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@scoreId"].Value = scoreId;
            dbCommand.Parameters["@detailId"].Value = detailId;
            dbCommand.Parameters["@residentId"].Value = residentId;
            dbCommand.Parameters["@july"].Value = july;
            dbCommand.Parameters["@august"].Value = august;
            dbCommand.Parameters["@september"].Value = september;
            dbCommand.Parameters["@october"].Value = october;
            dbCommand.Parameters["@november"].Value = quarter1;
            dbCommand.Parameters["@december"].Value = november;
            dbCommand.Parameters["@january"].Value = december;
            dbCommand.Parameters["@february"].Value = january;
            dbCommand.Parameters["@march"].Value = february;
            dbCommand.Parameters["@april"].Value = quarter2;
            dbCommand.Parameters["@may"].Value = march;
            dbCommand.Parameters["@june"].Value = april;
            dbCommand.Parameters["@lastActionBy"].Value = may;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? scoreId, System.Int32? detailId, System.Int32? residentId, System.Decimal? july, System.Decimal? august, System.Decimal? september, System.Decimal? october, System.Decimal? quarter1, System.Decimal? november, System.Decimal? december, System.Decimal? january, System.Decimal? february, System.Decimal? quarter2, System.Decimal? march, System.Decimal? april, System.Decimal? may)
        {
            Update(scoreId, detailId, residentId, july, august, september, october, quarter1, november, december, january, february, quarter2, march, april, may, null);
        }

        public static void Update(EvaDetailScores evaDetailScores)
        {
            evaDetailScores.Update();
        }

        public static void Update(EvaDetailScores evaDetailScores, DbTransaction transaction)
        {
            evaDetailScores.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].EvaDetailScores_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@scoreId"].SourceColumn = "ScoreId";
            dbCommand.Parameters["@detailId"].SourceColumn = "DetailId";
            dbCommand.Parameters["@residentId"].SourceColumn = "ResidentId";
            dbCommand.Parameters["@july"].SourceColumn = "July";
            dbCommand.Parameters["@august"].SourceColumn = "August";
            dbCommand.Parameters["@september"].SourceColumn = "September";
            dbCommand.Parameters["@october"].SourceColumn = "October";
            dbCommand.Parameters["@november"].SourceColumn = "November";
            dbCommand.Parameters["@december"].SourceColumn = "December";
            dbCommand.Parameters["@january"].SourceColumn = "January";
            dbCommand.Parameters["@february"].SourceColumn = "February";
            dbCommand.Parameters["@march"].SourceColumn = "March";
            dbCommand.Parameters["@april"].SourceColumn = "April";
            dbCommand.Parameters["@may"].SourceColumn = "May";
            dbCommand.Parameters["@june"].SourceColumn = "June";
            dbCommand.Parameters["@lastActionBy"].SourceColumn = "LastActionBy";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? scoreId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].EvaDetailScores_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, scoreId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? scoreId)
        {
            Delete(
            scoreId);
        }

        /// <summary>
        /// Delete current EvaDetailScores from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].EvaDetailScores_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, ScoreId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.ScoreId = null;
        }

        /// <summary>
        /// Delete current EvaDetailScores from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static EvaDetailScores[] Search(System.Int32? scoreId, System.Int32? detailId, System.Int32? residentId, System.Decimal? july, System.Decimal? august, System.Decimal? september, System.Decimal? october, System.Decimal? quarter1, System.Decimal? november, System.Decimal? december, System.Decimal? january, System.Decimal? february, System.Decimal? quarter2, System.Decimal? march, System.Decimal? april, System.Decimal? may, System.Decimal? june, System.Decimal? quarter3, System.Decimal? average, System.Int32? lastActionBy)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].EvaDetailScores_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, scoreId, detailId, residentId, july, august, september, october, quarter1, november, december, january, february, quarter2, march, april, may, june, quarter3, average, lastActionBy);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return EvaDetailScores.MapFrom(ds);
        }


        public static EvaDetailScores[] Search(EvaDetailScores searchObject)
        {
            return Search(searchObject.ScoreId, searchObject.DetailId, searchObject.ResidentId, searchObject.July, searchObject.August, searchObject.September, searchObject.October, searchObject.Quarter1, searchObject.November, searchObject.December, searchObject.January, searchObject.February, searchObject.Quarter2, searchObject.March, searchObject.April, searchObject.May, searchObject.June, searchObject.Quarter3, searchObject.Average, searchObject.LastActionBy);
        }

        /// <summary>
        /// Returns all EvaDetailScores objects.
        /// </summary>
        /// <returns>List of all EvaDetailScores objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static EvaDetailScores[] Search()
        {
            return Search(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


        #region Custom

        [Serializable]
        public class Scores
        {
            public int evaluationId { get; set; }
            public string evaluationName { get; set; }
            public decimal jul { get; set; }
            public decimal aug { get; set; }
            public decimal sep { get; set; }
            public decimal oct { get; set; }
            public decimal nov { get; set; }
            public decimal dec { get; set; }
            public decimal jan { get; set; }
            public decimal feb { get; set; }
            public decimal mar { get; set; }
            public decimal apr { get; set; }
            public decimal may { get; set; }
            public decimal jun { get; set; }
            public decimal average { get; set; }
            public decimal percent { get; set; }
        }

        public static IList<Scores> GetScores(int residentId, int centerId, int teachingYearId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            Scores instance;
            IList<Scores> llist = new List<Scores>();

            instance = new Scores();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "EvaScores_Get";
            dbCommand = db.GetStoredProcCommand(sqlCommand, residentId, centerId, teachingYearId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new Scores();
                instance.evaluationId = (int)item["EvaluationId"];
                instance.evaluationName = (string)item["EvaluationName"];
                instance.jul = (decimal)item["July"];
                instance.aug = (decimal)item["August"];
                instance.sep = (decimal)item["September"];
                instance.oct = (decimal)item["October"];
                instance.nov = (decimal)item["November"];
                instance.dec = (decimal)item["December"];
                instance.jan = (decimal)item["January"];
                instance.feb = (decimal)item["February"];
                instance.mar = (decimal)item["March"];
                instance.apr = (decimal)item["April"];
                instance.may = (decimal)item["May"];
                instance.jun = (decimal)item["June"];
                instance.average = (decimal)item["Average"];
                instance.percent = (decimal)item["EvaluationPercent"];
                llist.Add(instance);
            }
            return llist;
        }

        [Serializable]
        public class ScoreDetails
        {
            public int evaluationId { get; set; }
            public int detailId { get; set; }
            public string detailName { get; set; }
            public int maxPoints { get; set; }
            public decimal score { get; set; }
            public int monthNumber { get; set; }
        }

        public static IList<ScoreDetails> GetScoreDetails(int residentId, int centerId, int teachingYearId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            ScoreDetails instance;
            IList<ScoreDetails> llist = new List<ScoreDetails>();

            instance = new ScoreDetails();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "EvaScoreDetails_Get";
            dbCommand = db.GetStoredProcCommand(sqlCommand, residentId, centerId, teachingYearId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new ScoreDetails();
                instance.evaluationId = (int)item["EvaluationId"];
                instance.detailId = (int)item["DetailId"];
                instance.detailName = (string)item["DetailName"];
                instance.maxPoints = (int)item["DetailMaxPoints"];
                instance.score = (decimal)item["Score"];
                instance.monthNumber = (int)item["MonthNumber"];
                llist.Add(instance);
            }
            return llist;
        }

        public static void IfExists(System.Int32? detailId, System.Int32? residentId, System.Decimal? july, System.Decimal? august, System.Decimal? september, System.Decimal? october, System.Decimal? november, System.Decimal? december, System.Decimal? january, System.Decimal? february, System.Decimal? march, System.Decimal? april, System.Decimal? may, System.Decimal? june, System.Int32? lastActionBy)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].EvaScoreDetails_IfExists";
            dbCommand = db.GetStoredProcCommand(sqlCommand, 0, detailId, residentId, july, august, september, october, november, december, january, february, march, april, may, june, lastActionBy);

            db.ExecuteScalar(dbCommand);

            return;
        }

        [Serializable]
        public class ScoreUpdated
        {
            public bool jul { get; set; }
            public bool aug { get; set; }
            public bool sep { get; set; }
            public bool oct { get; set; }
            public bool nov { get; set; }
            public bool dec { get; set; }
            public bool jan { get; set; }
            public bool feb { get; set; }
            public bool mar { get; set; }
            public bool apr { get; set; }
            public bool may { get; set; }
            public bool jun { get; set; }
        }
        #endregion
    }
}