﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace General.PersonInfo
{
    [DataObject]
    [Serializable]
    public partial class GenPersonInfo
    {

        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[gen_Persons]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _personId;
        private System.Int32? _generalId;
        private System.String _name;
        private System.String _middleName;
        private System.String _firstLastName;
        private System.String _secondLastName;
        private System.Int32? _genderId;
        private System.Int32? _maritalStatusId;
        private System.DateTime? _birthday;

        private System.String _address1;
        private System.String _address2;
        private System.Int32? _cityId;

        private System.Int32? _stateId;

        private System.Int32? _nationalityId;

        private System.String _userName;

        #endregion


        #region Properties
        public System.Int32? PersonId
        {
            get
            {
                return _personId;
            }
            set
            {
                _personId = value;
            }
        }

        public System.Int32? GeneralId
        {
            get
            {
                return _generalId;
            }
            set
            {
                _generalId = value;
            }
        }

        public System.String Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public System.String MiddleName
        {
            get
            {
                return _middleName;
            }
            set
            {
                _middleName = value;
            }
        }

        public System.String FirstLastName
        {
            get
            {
                return _firstLastName;
            }
            set
            {
                _firstLastName = value;
            }
        }

        public System.String SecondLastName
        {
            get
            {
                return _secondLastName;
            }
            set
            {
                _secondLastName = value;
            }
        }

        public System.Int32? GenderId
        {
            get
            {
                return _genderId;
            }
            set
            {
                _genderId = value;
            }
        }

        public System.Int32? MaritalStatusId
        {
            get
            {
                return _maritalStatusId;
            }
            set
            {
                _maritalStatusId = value;
            }
        }

        public System.DateTime? Birthday
        {
            get
            {
                return _birthday;
            }
            set
            {
                _birthday = value;
            }
        }


        public System.String Address1
        {
            get
            {
                return _address1;
            }
            set
            {
                _address1 = value;
            }
        }

        public System.String Address2
        {
            get
            {
                return _address2;
            }
            set
            {
                _address2 = value;
            }
        }

        public System.Int32? CityId
        {
            get
            {
                return _cityId;
            }
            set
            {
                _cityId = value;
            }
        }

        public System.Int32? StateId
        {
            get
            {
                return _stateId;
            }
            set
            {
                _stateId = value;
            }
        }

        public System.Int32? NationalityId
        {
            get
            {
                return _nationalityId;
            }
            set
            {
                _nationalityId = value;
            }
        }

        public System.String UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }
        #endregion


        #region Methods


        #region Mapping Methods

        //protected void MapTo(DataSet ds)
        //{
        //    DataRow dr;


        //    if (ds == null)
        //        ds = new DataSet();

        //    if (ds.Tables["TABLE_NAME"] == null)
        //        ds.Tables.Add(TABLE_NAME);

        //    ds.Tables[TABLE_NAME].Columns.Add("PersonId", typeof(System.Int32));
        //    ds.Tables[TABLE_NAME].Columns.Add("GeneralId", typeof(System.Int32));
        //    ds.Tables[TABLE_NAME].Columns.Add("Name", typeof(System.String));
        //    ds.Tables[TABLE_NAME].Columns.Add("MiddleName", typeof(System.String));
        //    ds.Tables[TABLE_NAME].Columns.Add("FirstLastName", typeof(System.String));
        //    ds.Tables[TABLE_NAME].Columns.Add("SecondLastName", typeof(System.String));
        //    ds.Tables[TABLE_NAME].Columns.Add("FullName", typeof(System.String));
        //    ds.Tables[TABLE_NAME].Columns.Add("GenderId", typeof(System.Int32));
        //    ds.Tables[TABLE_NAME].Columns.Add("MaritalStatusId", typeof(System.Int32));
        //    ds.Tables[TABLE_NAME].Columns.Add("Birthday", typeof(System.DateTime));
        //    ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32));
        //    ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime));

        //    dr = ds.Tables[TABLE_NAME].NewRow();

        //    if (PersonId == null)
        //        dr["PersonId"] = DBNull.Value;
        //    else
        //        dr["PersonId"] = PersonId;

        //    if (GeneralId == null)
        //        dr["GeneralId"] = DBNull.Value;
        //    else
        //        dr["GeneralId"] = GeneralId;

        //    if (Name == null)
        //        dr["Name"] = DBNull.Value;
        //    else
        //        dr["Name"] = Name;

        //    if (MiddleName == null)
        //        dr["MiddleName"] = DBNull.Value;
        //    else
        //        dr["MiddleName"] = MiddleName;

        //    if (FirstLastName == null)
        //        dr["FirstLastName"] = DBNull.Value;
        //    else
        //        dr["FirstLastName"] = FirstLastName;

        //    if (SecondLastName == null)
        //        dr["SecondLastName"] = DBNull.Value;
        //    else
        //        dr["SecondLastName"] = SecondLastName;

        //    if (FullName == null)
        //        dr["FullName"] = DBNull.Value;
        //    else
        //        dr["FullName"] = FullName;

        //    if (GenderId == null)
        //        dr["GenderId"] = DBNull.Value;
        //    else
        //        dr["GenderId"] = GenderId;

        //    if (MaritalStatusId == null)
        //        dr["MaritalStatusId"] = DBNull.Value;
        //    else
        //        dr["MaritalStatusId"] = MaritalStatusId;

        //    if (Birthday == null)
        //        dr["Birthday"] = DBNull.Value;
        //    else
        //        dr["Birthday"] = Birthday;

        //    if (ModifiedBy == null)
        //        dr["ModifiedBy"] = DBNull.Value;
        //    else
        //        dr["ModifiedBy"] = ModifiedBy;

        //    if (ModifiedDate == null)
        //        dr["ModifiedDate"] = DBNull.Value;
        //    else
        //        dr["ModifiedDate"] = ModifiedDate;


        //    ds.Tables[TABLE_NAME].Rows.Add(dr);

        //}

        protected void MapFrom(DataRow dr)
        {
            PersonId = dr["PersonId"] != DBNull.Value ? Convert.ToInt32(dr["PersonId"]) : PersonId = null;
            GeneralId = dr["GeneralId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralId"]) : GeneralId = null;
            Name = dr["Name"] != DBNull.Value ? Convert.ToString(dr["Name"]) : Name = null;
            MiddleName = dr["MiddleName"] != DBNull.Value ? Convert.ToString(dr["MiddleName"]) : MiddleName = null;
            FirstLastName = dr["FirstLastName"] != DBNull.Value ? Convert.ToString(dr["FirstLastName"]) : FirstLastName = null;
            SecondLastName = dr["SecondLastName"] != DBNull.Value ? Convert.ToString(dr["SecondLastName"]) : SecondLastName = null;
            GenderId = dr["GenderId"] != DBNull.Value ? Convert.ToInt32(dr["GenderId"]) : GenderId = null;
            MaritalStatusId = dr["MaritalStatusId"] != DBNull.Value ? Convert.ToInt32(dr["MaritalStatusId"]) : MaritalStatusId = null;
            Birthday = dr["Birthday"] != DBNull.Value ? Convert.ToDateTime(dr["Birthday"]) : Birthday = null;

            Address1 = dr["Address1"] != DBNull.Value ? Convert.ToString(dr["Address1"]) : Address1 = null;
            Address2 = dr["Address2"] != DBNull.Value ? Convert.ToString(dr["Address2"]) : Address2 = null;
            CityId = dr["CityId"] != DBNull.Value ? Convert.ToInt32(dr["CityId"]) : CityId = null;

            StateId = dr["StateId"] != DBNull.Value ? Convert.ToInt32(dr["StateId"]) : StateId = null;

            NationalityId = dr["NationalityId"] != DBNull.Value ? Convert.ToInt32(dr["NationalityId"]) : NationalityId = null;

            UserName = dr["UserName"] != DBNull.Value ? Convert.ToString(dr["UserName"]) : UserName = null;
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenPersonInfo Get(System.Int32 personId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            GenPersonInfo instance;


            instance = new GenPersonInfo();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPersons_GetInfoByPersonId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, personId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenPersons ID:" + personId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #endregion

#endregion
    }
}
