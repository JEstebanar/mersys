﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace General.GeneralCommons
{
    public partial class GeneralCommon
    {
        public enum EntityTypes
        {
            Resident = 1,
            Coordinator = 3,
            Employee = 4,
            Administrator = 5,
            EmergencyContact = 6,
            NotAssignedResident = 7,
            Teacher = 8,
            Center = 9,
            HeadTraining = 11,
            RotatingResident = 12
        }

        //public enum UserRoles
        //{
        //    Administrator = 1,
        //    Supervisor = 2,
        //    User = 3
        //}

        public enum IdentificationTypes
        {
            IdentificationCard = 1,
            Passport = 2
        }

        public enum ResidentStatuses
        {
            Active = 1,
            Inactive = 2,
            Retired = 3,
            Espelled = 4,
            Rotating = 5,
            Promoted = 6,
            Transferred = 7,
            Graduate = 8
        }

        public enum ResidentDocTypes
        {
            Enrollment = 1,
            Exequatur = 2
        }

        public enum SystemScreens
        {
            ResidentRegister = 1,
            MainEvaluation = 2,
            CoordinatorRegister = 3,
            User = 4,
            Resident = 5,
            ChangePassword = 6,
            TeacherRegister = 7,
            Evaluations = 8,
            EvaluationDetails = 9,
            PromoteResidnet = 10,
            Transfers = 11,
            ScoreReport = 12,
            LetterRequest = 13,
            ReviewRequests = 14,
            ApproveRequests = 15,
            CompleteRequests = 16,
            RequestsStatus = 17,
            EmployeeRegister = 18,
            IncomeReport = 19,
            SpecialReport = 20,
            HeadTrainingRegister = 22,
            RoleRegister = 23,
            CenterModules = 24,
            ModuleRegister = 25,
            ScreenRegister = 26,
            States = 27,
            Cities = 28,
            Centers = 29,
            CreateArticle = 30,
            CenterReports = 31,
            RoleScreens = 32,
            Reports = 33,
            UnConfirmedEMails = 34,
            ResidentScores = 35,
            Letters = 36,
            CenterLetters = 37,
            ResidentsList = 38, //coordinators
            CreateComunication = 39,
            //BetweenRanges = 40,
            IndicatorResidencies = 40,
            IndicatorResidencie = 41
        }

        public enum ResidenceGrades
        {
            RI = 1,
            RII = 2,
            RIII = 3,
            RIV = 4,
            RV = 5,
            RVI = 6,
            RVII = 7
        }

        public enum RotationTypes
        {
            Internal = 1,
            External = 2
        }

        public enum Modules
        {
            Residences = 1,
            Evaluation = 2,
            Security = 3,
            Account = 4,
            Reportes = 5,
            Letters = 6,
            Administration = 7
        }

        public enum ReportTypes
        {
            ScoreReports = 1,
            IncomeReports = 2,
            SpecialReports = 3
        }

        public enum ReportList
        {
            JulyOctober = 1,
            NovemberFebruary = 2,
            MarchJune = 3,
            JulyDicember = 4,
            JanuaryJune = 5,
            AllPeriods = 6,
            ResidentDocumentsInfo = 7,
            Incomes = 8,
            IncomesInDetails = 9,
            ResidentStatus = 10,
            ResidentsToGraduate = 11,
            ResidentsPhones = 12
        }

        public enum MailType
        {
            SignUp = 1,
            LetterRequestCompleted = 2,
            LetterRequestCanceled = 3,
            Comunication = 4,
            Contact = 5,
            RecoverPassword = 6
        }

        public enum SystemRoles
        {
            Administrator = 1,
            HeadTraining = 2,
            Coordinator = 3,
            Teacher = 4,
            Resident = 5,
            EvaluationSecretary = 6,
            TeachingSecretary = 7
        }

        public enum PhoneTypes
        {
            HomePhone = 1,
            CellPhone = 2,
            Work = 3
        }

        public enum LetterStatus
        {
            Requested = 1,
            Sent = 2,
            Approved = 3,
            Completed = 4,
            Cancel = 5
        }

        public enum LetterSteps
        {
            Request = 1,
            Review = 2,
            Approve = 3,
            Complete = 4
        }

        public enum InvoiceRequestTypes
        {
            LetterRequest = 1
        }

        public enum CommentProcessType
        {
            LetterRequest= 1,
            Post=2,
            Transfer = 3
        }

        public enum FormConfirmationTypes
        {
            SignUp = 1,
            RecoverPassword = 2
        }

        public enum TypeFilePaths
        {
            Identification = 1,
            TransferDocuments = 2,
            Posts=3
        }

        public enum Countries
        {
            DominicanRepublic = 1
        }

        public enum Nationalities
        {
            Dominican = 1
        }

        public enum PostTypes
        {
            Comunications = 1,
            Articles = 2,
            Encuesta = 3,
            Foro = 4
        }

        public enum PageSize
        {
            Home = 2,
            Articles = 5
        }
    }
}
