﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace General.Cities
{

    [DataObject]
    [Serializable]
    public partial class GenCities
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[gen_Cities]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _cityId;
        private System.String _cityName;
        private System.Boolean? _cityStatus;
        private System.Int32? _stateId;

        #endregion


        #region Properties
        public System.Int32? CityId
        {
            get
            {
                return _cityId;
            }
            set
            {
                _cityId = value;
            }
        }

        public System.String CityName
        {
            get
            {
                return _cityName;
            }
            set
            {
                _cityName = value;
            }
        }

        public System.Boolean? CityStatus
        {
            get
            {
                return _cityStatus;
            }
            set
            {
                _cityStatus = value;
            }
        }

        public System.Int32? StateId
        {
            get
            {
                return _stateId;
            }
            set
            {
                _stateId = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("CityId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("CityName", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("CityStatus", typeof(System.Boolean));
            ds.Tables[TABLE_NAME].Columns.Add("StateId", typeof(System.Int32));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (CityId == null)
                dr["CityId"] = DBNull.Value;
            else
                dr["CityId"] = CityId;

            if (CityName == null)
                dr["CityName"] = DBNull.Value;
            else
                dr["CityName"] = CityName;

            if (CityStatus == null)
                dr["CityStatus"] = DBNull.Value;
            else
                dr["CityStatus"] = CityStatus;

            if (StateId == null)
                dr["StateId"] = DBNull.Value;
            else
                dr["StateId"] = StateId;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            CityId = dr["CityId"] != DBNull.Value ? Convert.ToInt32(dr["CityId"]) : CityId = null;
            CityName = dr["CityName"] != DBNull.Value ? Convert.ToString(dr["CityName"]) : CityName = null;
            CityStatus = dr["CityStatus"] != DBNull.Value ? Convert.ToBoolean(dr["CityStatus"]) : CityStatus = null;
            StateId = dr["StateId"] != DBNull.Value ? Convert.ToInt32(dr["StateId"]) : StateId = null;
        }

        public static GenCities[] MapFrom(DataSet ds)
        {
            List<GenCities> objects;


            // Initialise Collection.
            objects = new List<GenCities>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[gen_Cities] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[gen_Cities] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                GenCities instance = new GenCities();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenCities Get(System.Int32 cityId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            GenCities instance;


            instance = new GenCities();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenCities_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, cityId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenCities ID:" + cityId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.String cityName, System.Boolean? cityStatus, System.Int32? stateId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenCities_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, cityName, cityStatus, stateId);

            if (transaction == null)
                this.CityId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.CityId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.String cityName, System.Boolean? cityStatus, System.Int32? stateId)
        {
            Insert(cityName, cityStatus, stateId, null);
        }
        /// <summary>
        /// Insert current GenCities to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(CityName, CityStatus, StateId, transaction);
        }

        /// <summary>
        /// Insert current GenCities to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? cityId, System.String cityName, System.Boolean? cityStatus, System.Int32? stateId, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenCities_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@cityId"].Value = cityId;
            dbCommand.Parameters["@cityName"].Value = cityName;
            dbCommand.Parameters["@cityStatus"].Value = cityStatus;
            dbCommand.Parameters["@stateId"].Value = stateId;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? cityId, System.String cityName, System.Boolean? cityStatus, System.Int32? stateId)
        {
            Update(cityId, cityName, cityStatus, stateId, null);
        }

        public static void Update(GenCities genCities)
        {
            genCities.Update();
        }

        public static void Update(GenCities genCities, DbTransaction transaction)
        {
            genCities.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenCities_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@cityId"].SourceColumn = "CityId";
            dbCommand.Parameters["@cityName"].SourceColumn = "CityName";
            dbCommand.Parameters["@cityStatus"].SourceColumn = "CityStatus";
            dbCommand.Parameters["@stateId"].SourceColumn = "StateId";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? cityId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenCities_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, cityId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? cityId)
        {
            Delete(
            cityId);
        }

        /// <summary>
        /// Delete current GenCities from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenCities_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, CityId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.CityId = null;
        }

        /// <summary>
        /// Delete current GenCities from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenCities[] Search(System.Int32? cityId, System.String cityName, System.Boolean? cityStatus, System.Int32? stateId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenCities_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, cityId, cityName, cityStatus, stateId);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return GenCities.MapFrom(ds);
        }


        public static GenCities[] Search(GenCities searchObject)
        {
            return Search(searchObject.CityId, searchObject.CityName, searchObject.CityStatus, searchObject.StateId);
        }

        /// <summary>
        /// Returns all GenCities objects.
        /// </summary>
        /// <returns>List of all GenCities objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static GenCities[] Search()
        {
            return Search(null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


        #region Custom

        [Serializable]
        public class Cities
        {
            public int cityId { get; set; }
            public string cityName { get; set; }
        }

        public static IList<Cities> GetCitiesByStateId(int stateId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            Cities instance;
            IList<Cities> llist = new List<Cities>();

            instance = new Cities();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "GenCities_GetByStateId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, stateId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new Cities();
                instance.cityId = (int)item["CityId"];
                instance.cityName = (string)item["CityName"];
                llist.Add(instance);
            }
            return llist;
        }
        #endregion
    }


}