﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace General.States
{

    [DataObject]
    [Serializable]
    public partial class GenStates
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[gen_States]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _stateId;
        private System.String _stateName;
        private System.Boolean? _stateStatus;
        private System.Int32? _countryId;

        #endregion


        #region Properties
        public System.Int32? StateId
        {
            get
            {
                return _stateId;
            }
            set
            {
                _stateId = value;
            }
        }

        public System.String StateName
        {
            get
            {
                return _stateName;
            }
            set
            {
                _stateName = value;
            }
        }

        public System.Boolean? StateStatus
        {
            get
            {
                return _stateStatus;
            }
            set
            {
                _stateStatus = value;
            }
        }

        public System.Int32? CountryId
        {
            get
            {
                return _countryId;
            }
            set
            {
                _countryId = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("StateId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("StateName", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("StateStatus", typeof(System.Boolean));
            ds.Tables[TABLE_NAME].Columns.Add("CountryId", typeof(System.Int32));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (StateId == null)
                dr["StateId"] = DBNull.Value;
            else
                dr["StateId"] = StateId;

            if (StateName == null)
                dr["StateName"] = DBNull.Value;
            else
                dr["StateName"] = StateName;

            if (StateStatus == null)
                dr["StateStatus"] = DBNull.Value;
            else
                dr["StateStatus"] = StateStatus;

            if (CountryId == null)
                dr["CountryId"] = DBNull.Value;
            else
                dr["CountryId"] = CountryId;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            StateId = dr["StateId"] != DBNull.Value ? Convert.ToInt32(dr["StateId"]) : StateId = null;
            StateName = dr["StateName"] != DBNull.Value ? Convert.ToString(dr["StateName"]) : StateName = null;
            StateStatus = dr["StateStatus"] != DBNull.Value ? Convert.ToBoolean(dr["StateStatus"]) : StateStatus = null;
            CountryId = dr["CountryId"] != DBNull.Value ? Convert.ToInt32(dr["CountryId"]) : CountryId = null;
        }

        public static GenStates[] MapFrom(DataSet ds)
        {
            List<GenStates> objects;


            // Initialise Collection.
            objects = new List<GenStates>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[gen_States] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[gen_States] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                GenStates instance = new GenStates();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenStates Get(System.Int32 stateId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            GenStates instance;


            instance = new GenStates();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenStates_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, stateId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenStates ID:" + stateId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.String stateName, System.Boolean? stateStatus, System.Int32? countryId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenStates_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, stateName, stateStatus, countryId);

            if (transaction == null)
                this.StateId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.StateId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.String stateName, System.Boolean? stateStatus, System.Int32? countryId)
        {
            Insert(stateName, stateStatus, countryId, null);
        }
        /// <summary>
        /// Insert current GenStates to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(StateName, StateStatus, CountryId, transaction);
        }

        /// <summary>
        /// Insert current GenStates to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? stateId, System.String stateName, System.Boolean? stateStatus, System.Int32? countryId, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenStates_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@stateId"].Value = stateId;
            dbCommand.Parameters["@stateName"].Value = stateName;
            dbCommand.Parameters["@stateStatus"].Value = stateStatus;
            dbCommand.Parameters["@countryId"].Value = countryId;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? stateId, System.String stateName, System.Boolean? stateStatus, System.Int32? countryId)
        {
            Update(stateId, stateName, stateStatus, countryId, null);
        }

        public static void Update(GenStates genStates)
        {
            genStates.Update();
        }

        public static void Update(GenStates genStates, DbTransaction transaction)
        {
            genStates.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenStates_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@stateId"].SourceColumn = "StateId";
            dbCommand.Parameters["@stateName"].SourceColumn = "StateName";
            dbCommand.Parameters["@stateStatus"].SourceColumn = "StateStatus";
            dbCommand.Parameters["@countryId"].SourceColumn = "CountryId";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? stateId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenStates_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, stateId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? stateId)
        {
            Delete(
            stateId);
        }

        /// <summary>
        /// Delete current GenStates from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenStates_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, StateId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.StateId = null;
        }

        /// <summary>
        /// Delete current GenStates from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenStates[] Search(System.Int32? stateId, System.String stateName, System.Boolean? stateStatus, System.Int32? countryId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenStates_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, stateId, stateName, stateStatus, countryId);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return GenStates.MapFrom(ds);
        }


        public static GenStates[] Search(GenStates searchObject)
        {
            return Search(searchObject.StateId, searchObject.StateName, searchObject.StateStatus, searchObject.CountryId);
        }

        /// <summary>
        /// Returns all GenStates objects.
        /// </summary>
        /// <returns>List of all GenStates objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static GenStates[] Search()
        {
            return Search(null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


        #region Custom

        [Serializable]
        public class States
        {
            public int stateId { get; set; }
            public string stateName { get; set; }
        }

        public static IList<States> GetStatesByCountryId(int? countryId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            States instance;
            IList<States> llist = new List<States>();

            instance = new States();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "GenStates_GetByCountryId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, countryId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new States();
                instance.stateId = (int)item["StateId"];
                instance.stateName = (string)item["StateName"];
                llist.Add(instance);
            }
            return llist;
        }
        #endregion
    }
}