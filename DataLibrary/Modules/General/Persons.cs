﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace General.Persons
{

    [DataObject]
    [Serializable]
    public partial class GenPersons
    {

        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[gen_Persons]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _personId;
        private System.Int32? _generalId;
        private System.String _name;
        private System.String _middleName;
        private System.String _firstLastName;
        private System.String _secondLastName;
        private System.String _fullName;
        private System.Int32? _genderId;
        private System.Int32? _maritalStatusId;
        private System.DateTime? _birthday;
        private System.Int32? _ModifiedBy;
        private System.DateTime? _ModifiedDate;

        #endregion


        #region Properties
        public System.Int32? PersonId
        {
            get
            {
                return _personId;
            }
            set
            {
                _personId = value;
            }
        }

        public System.Int32? GeneralId
        {
            get
            {
                return _generalId;
            }
            set
            {
                _generalId = value;
            }
        }

        public System.String Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public System.String MiddleName
        {
            get
            {
                return _middleName;
            }
            set
            {
                _middleName = value;
            }
        }

        public System.String FirstLastName
        {
            get
            {
                return _firstLastName;
            }
            set
            {
                _firstLastName = value;
            }
        }

        public System.String SecondLastName
        {
            get
            {
                return _secondLastName;
            }
            set
            {
                _secondLastName = value;
            }
        }

        public System.String FullName
        {
            get
            {
                return _fullName;
            }
            set
            {
                _fullName = value;
            }
        }

        public System.Int32? GenderId
        {
            get
            {
                return _genderId;
            }
            set
            {
                _genderId = value;
            }
        }

        public System.Int32? MaritalStatusId
        {
            get
            {
                return _maritalStatusId;
            }
            set
            {
                _maritalStatusId = value;
            }
        }

        public System.DateTime? Birthday
        {
            get
            {
                return _birthday;
            }
            set
            {
                _birthday = value;
            }
        }

        public System.Int32? ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }
            set
            {
                _ModifiedBy = value;
            }
        }

        public System.DateTime? ModifiedDate
        {
            get
            {
                return _ModifiedDate;
            }
            set
            {
                _ModifiedDate = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("PersonId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("GeneralId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("Name", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("MiddleName", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("FirstLastName", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("SecondLastName", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("FullName", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("GenderId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("MaritalStatusId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("Birthday", typeof(System.DateTime));
            ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (PersonId == null)
                dr["PersonId"] = DBNull.Value;
            else
                dr["PersonId"] = PersonId;

            if (GeneralId == null)
                dr["GeneralId"] = DBNull.Value;
            else
                dr["GeneralId"] = GeneralId;

            if (Name == null)
                dr["Name"] = DBNull.Value;
            else
                dr["Name"] = Name.ToUpper();

            if (MiddleName == null)
                dr["MiddleName"] = DBNull.Value;
            else
                dr["MiddleName"] = MiddleName.ToUpper();

            if (FirstLastName == null)
                dr["FirstLastName"] = DBNull.Value;
            else
                dr["FirstLastName"] = FirstLastName.ToUpper();

            if (SecondLastName == null)
                dr["SecondLastName"] = DBNull.Value;
            else
                dr["SecondLastName"] = SecondLastName.ToUpper();

            if (FullName == null)
                dr["FullName"] = DBNull.Value;
            else
                dr["FullName"] = FullName;

            if (GenderId == null)
                dr["GenderId"] = DBNull.Value;
            else
                dr["GenderId"] = GenderId;

            if (MaritalStatusId == null)
                dr["MaritalStatusId"] = DBNull.Value;
            else
                dr["MaritalStatusId"] = MaritalStatusId;

            if (Birthday == null)
                dr["Birthday"] = DBNull.Value;
            else
                dr["Birthday"] = Birthday;

            if (ModifiedBy == null)
                dr["ModifiedBy"] = DBNull.Value;
            else
                dr["ModifiedBy"] = ModifiedBy;

            if (ModifiedDate == null)
                dr["ModifiedDate"] = DBNull.Value;
            else
                dr["ModifiedDate"] = ModifiedDate;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            PersonId = dr["PersonId"] != DBNull.Value ? Convert.ToInt32(dr["PersonId"]) : PersonId = null;
            GeneralId = dr["GeneralId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralId"]) : GeneralId = null;
            Name = dr["Name"] != DBNull.Value ? Convert.ToString(dr["Name"]) : Name = null;
            MiddleName = dr["MiddleName"] != DBNull.Value ? Convert.ToString(dr["MiddleName"]) : MiddleName = null;
            FirstLastName = dr["FirstLastName"] != DBNull.Value ? Convert.ToString(dr["FirstLastName"]) : FirstLastName = null;
            SecondLastName = dr["SecondLastName"] != DBNull.Value ? Convert.ToString(dr["SecondLastName"]) : SecondLastName = null;
            FullName = dr["FullName"] != DBNull.Value ? Convert.ToString(dr["FullName"]) : FullName = null;
            GenderId = dr["GenderId"] != DBNull.Value ? Convert.ToInt32(dr["GenderId"]) : GenderId = null;
            MaritalStatusId = dr["MaritalStatusId"] != DBNull.Value ? Convert.ToInt32(dr["MaritalStatusId"]) : MaritalStatusId = null;
            Birthday = dr["Birthday"] != DBNull.Value ? Convert.ToDateTime(dr["Birthday"]) : Birthday = null;
            ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
            ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
        }

        public static GenPersons[] MapFrom(DataSet ds)
        {
            List<GenPersons> objects;


            // Initialise Collection.
            objects = new List<GenPersons>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[gen_Persons] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[gen_Persons] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                GenPersons instance = new GenPersons();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenPersons Get(System.Int32 personId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            GenPersons instance;


            instance = new GenPersons();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPersons_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, personId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenPersons ID:" + personId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public int Insert(System.Int32? generalId, System.String name, System.String middleName, System.String firstLastName, System.String secondLastName, System.Int32? genderId, System.Int32? maritalStatusId, System.DateTime? birthday, System.Int32? ModifiedBy, System.DateTime? ModifiedDate, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPersons_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, generalId, name, middleName, firstLastName, secondLastName, genderId, maritalStatusId, birthday, ModifiedBy, ModifiedDate);

            if (transaction == null)
                this.PersonId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.PersonId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return Convert.ToInt32(PersonId);
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public int Insert(System.Int32? generalId, System.String name, System.String middleName, System.String firstLastName, System.String secondLastName, System.Int32? genderId, System.Int32? maritalStatusId, System.DateTime? birthday, System.Int32? ModifiedBy, System.DateTime? ModifiedDate)
        {
            return Insert(generalId, name, middleName, firstLastName, secondLastName, genderId, maritalStatusId, birthday, ModifiedBy, ModifiedDate, null);
        }
        /// <summary>
        /// Insert current GenPersons to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public int Insert(DbTransaction transaction)
        {
            return Insert(GeneralId, Name, MiddleName, FirstLastName, SecondLastName, GenderId, MaritalStatusId, Birthday, ModifiedBy, ModifiedDate, transaction);
        }

        /// <summary>
        /// Insert current GenPersons to database.
        /// </summary>
        public int Insert()
        {
            return Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? personId, System.Int32? generalId, System.String name, System.String middleName, System.String firstLastName, System.String secondLastName, System.String fullName, System.Int32? genderId, System.Int32? maritalStatusId, System.DateTime? birthday, System.Int32? ModifiedBy, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPersons_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@personId"].Value = personId;
            dbCommand.Parameters["@generalId"].Value = generalId;
            dbCommand.Parameters["@name"].Value = name;
            dbCommand.Parameters["@middleName"].Value = middleName;
            dbCommand.Parameters["@firstLastName"].Value = firstLastName;
            dbCommand.Parameters["@secondLastName"].Value = secondLastName;
            dbCommand.Parameters["@genderId"].Value = fullName;
            dbCommand.Parameters["@maritalStatusId"].Value = genderId;
            dbCommand.Parameters["@birthday"].Value = maritalStatusId;
            dbCommand.Parameters["@ModifiedBy"].Value = birthday;
            dbCommand.Parameters["@ModifiedDate"].Value = ModifiedBy;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? personId, System.Int32? generalId, System.String name, System.String middleName, System.String firstLastName, System.String secondLastName, System.String fullName, System.Int32? genderId, System.Int32? maritalStatusId, System.DateTime? birthday, System.Int32? ModifiedBy)
        {
            Update(personId, generalId, name, middleName, firstLastName, secondLastName, fullName, genderId, maritalStatusId, birthday, ModifiedBy, null);
        }

        public static void Update(GenPersons genPersons)
        {
            genPersons.Update();
        }

        public static void Update(GenPersons genPersons, DbTransaction transaction)
        {
            genPersons.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPersons_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@personId"].SourceColumn = "PersonId";
            dbCommand.Parameters["@generalId"].SourceColumn = "GeneralId";
            dbCommand.Parameters["@name"].SourceColumn = "Name";
            dbCommand.Parameters["@middleName"].SourceColumn = "MiddleName";
            dbCommand.Parameters["@firstLastName"].SourceColumn = "FirstLastName";
            dbCommand.Parameters["@secondLastName"].SourceColumn = "SecondLastName";
            dbCommand.Parameters["@genderId"].SourceColumn = "GenderId";
            dbCommand.Parameters["@maritalStatusId"].SourceColumn = "MaritalStatusId";
            dbCommand.Parameters["@birthday"].SourceColumn = "Birthday";
            dbCommand.Parameters["@ModifiedBy"].SourceColumn = "ModifiedBy";
            dbCommand.Parameters["@ModifiedDate"].SourceColumn = "ModifiedDate";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? personId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPersons_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, personId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? personId)
        {
            Delete(
            personId);
        }

        /// <summary>
        /// Delete current GenPersons from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPersons_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, PersonId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.PersonId = null;
        }

        /// <summary>
        /// Delete current GenPersons from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenPersons[] Search(System.Int32? personId, System.Int32? generalId, System.String name, System.String middleName, System.String firstLastName, System.String secondLastName, System.String fullName, System.Int32? genderId, System.Int32? maritalStatusId, System.DateTime? birthday, System.Int32? ModifiedBy, System.DateTime? ModifiedDate)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPersons_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, personId, generalId, name, middleName, firstLastName, secondLastName, fullName, genderId, maritalStatusId, birthday, ModifiedBy, ModifiedDate);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return GenPersons.MapFrom(ds);
        }


        public static GenPersons[] Search(GenPersons searchObject)
        {
            return Search(searchObject.PersonId, searchObject.GeneralId, searchObject.Name, searchObject.MiddleName, searchObject.FirstLastName, searchObject.SecondLastName, searchObject.FullName, searchObject.GenderId, searchObject.MaritalStatusId, searchObject.Birthday, searchObject.ModifiedBy, searchObject.ModifiedDate);
        }

        /// <summary>
        /// Returns all GenPersons objects.
        /// </summary>
        /// <returns>List of all GenPersons objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static GenPersons[] Search()
        {
            return Search(null, null, null, null, null, null, null, null, null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


        #region Custom

        [Serializable]
        public class Persons
        {
            public string fullName { get; set; }
            public int generalId { get; set; }
            public int personId { get; set; }
            public int residentId { get; set; }
            public int emergencyContactId { get; set; }
            public int userId { get; set; }
        }

        public static IList<Persons> GetPersonsByEntityType(int? entityTypeId, int centerId, bool? entityStatus, string personName)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            Persons instance;
            IList<Persons> llist = new List<Persons>();

            instance = new Persons();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "GenPersons_GetByEntityType";
            dbCommand = db.GetStoredProcCommand(sqlCommand, entityTypeId, centerId, entityStatus, personName);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new Persons();
                instance.fullName = (string)item["FullName"];
                instance.generalId = (int)item["GeneralId"];
                instance.personId = (int)item["PersonId"];
                instance.residentId = (int)item["ResidentId"];
                instance.emergencyContactId = (int)item["EmergencyContactId"];
                instance.userId = (int)item["UserId"];
                llist.Add(instance);
            }
            return llist;
        }
        
        #endregion
    }

}