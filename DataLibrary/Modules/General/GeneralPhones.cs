﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace General.GeneralPhones
{

    [DataObject]
    [Serializable]
    public partial class GenGeneralPhones
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[gen_GeneralPhones]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _generalPhoneId;
        private System.Int32? _generalId;
        private System.String _phoneNumber;
        private System.Int32? _phoneTypeId;
        private System.Boolean? _phoneStatus;
        private System.Int32? _createdBy;
        private System.DateTime? _createdDate;
        private System.Int32? _modifiedBy;
        private System.DateTime? _modifiedDate;

        #endregion


        #region Properties
        public System.Int32? GeneralPhoneId
        {
            get
            {
                return _generalPhoneId;
            }
            set
            {
                _generalPhoneId = value;
            }
        }

        public System.Int32? GeneralId
        {
            get
            {
                return _generalId;
            }
            set
            {
                _generalId = value;
            }
        }

        public System.String PhoneNumber
        {
            get
            {
                return _phoneNumber;
            }
            set
            {
                _phoneNumber = value;
            }
        }

        public System.Int32? PhoneTypeId
        {
            get
            {
                return _phoneTypeId;
            }
            set
            {
                _phoneTypeId = value;
            }
        }

        public System.Boolean? PhoneStatus
        {
            get
            {
                return _phoneStatus;
            }
            set
            {
                _phoneStatus = value;
            }
        }

        public System.Int32? CreatedBy
        {
            get
            {
                return _createdBy;
            }
            set
            {
                _createdBy = value;
            }
        }

        public System.DateTime? CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        public System.Int32? ModifiedBy
        {
            get
            {
                return _modifiedBy;
            }
            set
            {
                _modifiedBy = value;
            }
        }

        public System.DateTime? ModifiedDate
        {
            get
            {
                return _modifiedDate;
            }
            set
            {
                _modifiedDate = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("GeneralPhoneId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("GeneralId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("PhoneNumber", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("PhoneTypeId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("PhoneStatus", typeof(System.Boolean));
            ds.Tables[TABLE_NAME].Columns.Add("CreatedBy", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime));
            ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (GeneralPhoneId == null)
                dr["GeneralPhoneId"] = DBNull.Value;
            else
                dr["GeneralPhoneId"] = GeneralPhoneId;

            if (GeneralId == null)
                dr["GeneralId"] = DBNull.Value;
            else
                dr["GeneralId"] = GeneralId;

            if (PhoneNumber == null)
                dr["PhoneNumber"] = DBNull.Value;
            else
                dr["PhoneNumber"] = PhoneNumber;

            if (PhoneTypeId == null)
                dr["PhoneTypeId"] = DBNull.Value;
            else
                dr["PhoneTypeId"] = PhoneTypeId;

            if (PhoneStatus == null)
                dr["PhoneStatus"] = DBNull.Value;
            else
                dr["PhoneStatus"] = PhoneStatus;

            if (CreatedBy == null)
                dr["CreatedBy"] = DBNull.Value;
            else
                dr["CreatedBy"] = CreatedBy;

            if (CreatedDate == null)
                dr["CreatedDate"] = DBNull.Value;
            else
                dr["CreatedDate"] = CreatedDate;

            if (ModifiedBy == null)
                dr["ModifiedBy"] = DBNull.Value;
            else
                dr["ModifiedBy"] = ModifiedBy;

            if (ModifiedDate == null)
                dr["ModifiedDate"] = DBNull.Value;
            else
                dr["ModifiedDate"] = ModifiedDate;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            GeneralPhoneId = dr["GeneralPhoneId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralPhoneId"]) : GeneralPhoneId = null;
            GeneralId = dr["GeneralId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralId"]) : GeneralId = null;
            PhoneNumber = dr["PhoneNumber"] != DBNull.Value ? Convert.ToString(dr["PhoneNumber"]) : PhoneNumber = null;
            PhoneTypeId = dr["PhoneTypeId"] != DBNull.Value ? Convert.ToInt32(dr["PhoneTypeId"]) : PhoneTypeId = null;
            PhoneStatus = dr["PhoneStatus"] != DBNull.Value ? Convert.ToBoolean(dr["PhoneStatus"]) : PhoneStatus = null;
            CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : CreatedBy = null;
            CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
            ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
            ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
        }

        public static GenGeneralPhones[] MapFrom(DataSet ds)
        {
            List<GenGeneralPhones> objects;


            // Initialise Collection.
            objects = new List<GenGeneralPhones>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[gen_GeneralPhones] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[gen_GeneralPhones] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                GenGeneralPhones instance = new GenGeneralPhones();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenGeneralPhones Get(System.Int32 generalPhoneId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            GenGeneralPhones instance;


            instance = new GenGeneralPhones();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenGeneralPhones_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, generalPhoneId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenGeneralPhones ID:" + generalPhoneId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenGeneralPhones GetByTheOnlyGeneralId(System.Int32 generalId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            GenGeneralPhones instance;


            instance = new GenGeneralPhones();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenGeneralPhones_GetByTheOnlyGeneralId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, generalId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenGeneralPhones ID:" + generalId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.Int32? generalId, System.String phoneNumber, System.Int32? phoneTypeId, System.Boolean? phoneStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenGeneralPhones_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, generalId, phoneNumber, phoneTypeId, phoneStatus, createdBy, createdDate, modifiedBy, modifiedDate);

            if (transaction == null)
                this.GeneralPhoneId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.GeneralPhoneId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.Int32? generalId, System.String phoneNumber, System.Int32? phoneTypeId, System.Boolean? phoneStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
        {
            Insert(generalId, phoneNumber, phoneTypeId, phoneStatus, createdBy, createdDate, modifiedBy, modifiedDate, null);
        }
        /// <summary>
        /// Insert current GenGeneralPhones to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(GeneralId, PhoneNumber, PhoneTypeId, PhoneStatus, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, transaction);
        }

        /// <summary>
        /// Insert current GenGeneralPhones to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? generalPhoneId, System.Int32? generalId, System.String phoneNumber, System.Int32? phoneTypeId, System.Boolean? phoneStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenGeneralPhones_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@generalPhoneId"].Value = generalPhoneId;
            dbCommand.Parameters["@generalId"].Value = generalId;
            dbCommand.Parameters["@phoneNumber"].Value = phoneNumber;
            dbCommand.Parameters["@phoneTypeId"].Value = phoneTypeId;
            dbCommand.Parameters["@phoneStatus"].Value = phoneStatus;
            dbCommand.Parameters["@createdBy"].Value = createdBy;
            dbCommand.Parameters["@createdDate"].Value = createdDate;
            dbCommand.Parameters["@modifiedBy"].Value = modifiedBy;
            dbCommand.Parameters["@modifiedDate"].Value = modifiedDate;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? generalPhoneId, System.Int32? generalId, System.String phoneNumber, System.Int32? phoneTypeId, System.Boolean? phoneStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
        {
            Update(generalPhoneId, generalId, phoneNumber, phoneTypeId, phoneStatus, createdBy, createdDate, modifiedBy, modifiedDate, null);
        }

        public static void Update(GenGeneralPhones genGeneralPhones)
        {
            genGeneralPhones.Update();
        }

        public static void Update(GenGeneralPhones genGeneralPhones, DbTransaction transaction)
        {
            genGeneralPhones.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenGeneralPhones_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@generalPhoneId"].SourceColumn = "GeneralPhoneId";
            dbCommand.Parameters["@generalId"].SourceColumn = "GeneralId";
            dbCommand.Parameters["@phoneNumber"].SourceColumn = "PhoneNumber";
            dbCommand.Parameters["@phoneTypeId"].SourceColumn = "PhoneTypeId";
            dbCommand.Parameters["@phoneStatus"].SourceColumn = "PhoneStatus";
            dbCommand.Parameters["@createdBy"].SourceColumn = "CreatedBy";
            dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
            dbCommand.Parameters["@modifiedBy"].SourceColumn = "ModifiedBy";
            dbCommand.Parameters["@modifiedDate"].SourceColumn = "ModifiedDate";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? generalPhoneId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenGeneralPhones_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, generalPhoneId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? generalPhoneId)
        {
            Delete(
            generalPhoneId);
        }

        /// <summary>
        /// Delete current GenGeneralPhones from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenGeneralPhones_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, GeneralPhoneId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.GeneralPhoneId = null;
        }

        /// <summary>
        /// Delete current GenGeneralPhones from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenGeneralPhones[] Search(System.Int32? generalPhoneId, System.Int32? generalId, System.String phoneNumber, System.Int32? phoneTypeId, System.Boolean? phoneStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenGeneralPhones_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, generalPhoneId, generalId, phoneNumber, phoneTypeId, phoneStatus, createdBy, createdDate, modifiedBy, modifiedDate);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return GenGeneralPhones.MapFrom(ds);
        }


        public static GenGeneralPhones[] Search(GenGeneralPhones searchObject)
        {
            return Search(searchObject.GeneralPhoneId, searchObject.GeneralId, searchObject.PhoneNumber, searchObject.PhoneTypeId, searchObject.PhoneStatus, searchObject.CreatedBy, searchObject.CreatedDate, searchObject.ModifiedBy, searchObject.ModifiedDate);
        }

        /// <summary>
        /// Returns all GenGeneralPhones objects.
        /// </summary>
        /// <returns>List of all GenGeneralPhones objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static GenGeneralPhones[] Search()
        {
            return Search(null, null, null, null, null, null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


        #region Custom

        [Serializable]
        public class Phones
        {
            public int phoneId { get; set; }
            public string phoneNumber { get; set; }
            public int phoneTypeId { get; set; }
            //public string phoneType { get; set; }
        }

        public static IList<Phones> GetByGeneralId(int generalId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            Phones instance;
            IList<Phones> llist = new List<Phones>();

            instance = new Phones();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "GenGeneralPhones_GetByGeneralId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, generalId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new Phones();
                instance.phoneId = (int)item["PhoneId"];
                instance.phoneNumber = (string)item["PhoneNumber"];
                instance.phoneTypeId = (int)item["PhoneTypeId"];
                //instance.phoneType = (string)item["PhoneType"];
                llist.Add(instance);
            }
            return llist;
        }

        //to delete in db
        //public static void DeleteByGeneralId(int generalId)
        //{
        //    Database db;
        //    string sqlCommand;
        //    DbCommand dbCommand;
            
        //    db = DatabaseFactory.CreateDatabase(DataConnection);
        //    sqlCommand = "[dbo].GenGeneralPhones_DELETEByGeneralId";
        //    dbCommand = db.GetStoredProcCommand(sqlCommand, generalId);

        //    // Execute.
        //    db.ExecuteNonQuery(dbCommand);
        //}

        #endregion

    }


}
