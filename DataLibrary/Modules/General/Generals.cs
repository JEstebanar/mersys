﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace General.Generals
{
[DataObject]
[Serializable]
public partial class GenGenerals
{
	
	
	#region Constants
    private static readonly string TABLE_NAME = "[dbo].[gen_Generals]";
    private static readonly string DataConnection = "DataConnection";
	
	#endregion


    #region Fields
    private System.Int32? _generalId;
    private System.Int32? _nationalityId;
    private System.String _photoPath;

    #endregion


    #region Properties
    public System.Int32? GeneralId
    {
        get
        {
            return _generalId;
        }
        set
        {
            _generalId = value;
        }
    }

    public System.Int32? NationalityId
    {
        get
        {
            return _nationalityId;
        }
        set
        {
            _nationalityId = value;
        }
    }

    public System.String PhotoPath
    {
        get
        {
            return _photoPath;
        }
        set
        {
            _photoPath = value;
        }
    }

    #endregion


    #region Methods


    #region Mapping Methods

    protected void MapTo(DataSet ds)
    {
        DataRow dr;


        if (ds == null)
            ds = new DataSet();

        if (ds.Tables["TABLE_NAME"] == null)
            ds.Tables.Add(TABLE_NAME);

        ds.Tables[TABLE_NAME].Columns.Add("GeneralId", typeof(System.Int32));
        ds.Tables[TABLE_NAME].Columns.Add("NationalityId", typeof(System.Int32));
        ds.Tables[TABLE_NAME].Columns.Add("PhotoPath", typeof(System.String));

        dr = ds.Tables[TABLE_NAME].NewRow();

        if (GeneralId == null)
            dr["GeneralId"] = DBNull.Value;
        else
            dr["GeneralId"] = GeneralId;

        if (NationalityId == null)
            dr["NationalityId"] = DBNull.Value;
        else
            dr["NationalityId"] = NationalityId;

        if (PhotoPath == null)
            dr["PhotoPath"] = DBNull.Value;
        else
            dr["PhotoPath"] = PhotoPath;


        ds.Tables[TABLE_NAME].Rows.Add(dr);

    }

    protected void MapFrom(DataRow dr)
    {
        GeneralId = dr["GeneralId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralId"]) : GeneralId = null;
        NationalityId = dr["NationalityId"] != DBNull.Value ? Convert.ToInt32(dr["NationalityId"]) : NationalityId = null;
        PhotoPath = dr["PhotoPath"] != DBNull.Value ? Convert.ToString(dr["PhotoPath"]) : PhotoPath = null;
    }

    public static GenGenerals[] MapFrom(DataSet ds)
    {
        List<GenGenerals> objects;


        // Initialise Collection.
        objects = new List<GenGenerals>();

        // Validation.
        if (ds == null)
            throw new ApplicationException("Cannot map to dataset null.");
        else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
            return objects.ToArray();

        if (ds.Tables[TABLE_NAME] == null)
            throw new ApplicationException("Cannot find table [dbo].[gen_Generals] in DataSet.");

        if (ds.Tables[TABLE_NAME].Rows.Count < 1)
            throw new ApplicationException("Table [dbo].[gen_Generals] is empty.");

        // Map DataSet to Instance.
        foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
        {
            GenGenerals instance = new GenGenerals();
            instance.MapFrom(dr);
            objects.Add(instance);
        }

        // Return collection.
        return objects.ToArray();
    }


    #endregion


    #region CRUD Methods

    [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
    public static GenGenerals Get(System.Int32 generalId)
    {
        DataSet ds;
        Database db;
        string sqlCommand;
        DbCommand dbCommand;
        GenGenerals instance;


        instance = new GenGenerals();

        db = DatabaseFactory.CreateDatabase(DataConnection);
        sqlCommand = "[dbo].GenGenerals_SELECT";
        dbCommand = db.GetStoredProcCommand(sqlCommand, generalId);

        // Get results.
        ds = db.ExecuteDataSet(dbCommand);
        // Verification.
        if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenGenerals ID:" + generalId.ToString() + " from Database.");
        // Return results.
        ds.Tables[0].TableName = TABLE_NAME;

        instance.MapFrom(ds.Tables[0].Rows[0]);
        return instance;
    }

    #region INSERT
    public int Insert(System.Int32? nationalityId, System.String photoPath, DbTransaction transaction)
    {
        Database db;
        string sqlCommand;
        DbCommand dbCommand;


        db = DatabaseFactory.CreateDatabase(DataConnection);
        sqlCommand = "[dbo].GenGenerals_INSERT";
        dbCommand = db.GetStoredProcCommand(sqlCommand, nationalityId, photoPath);

        if (transaction == null)
            this.GeneralId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
        else
            this.GeneralId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
        return Convert.ToInt32(GeneralId);
    }

    [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
    public int Insert(System.Int32? nationalityId, System.String photoPath)
    {
        return Insert(nationalityId, photoPath, null);
    }
    /// <summary>
    /// Insert current GenGenerals to database.
    /// </summary>
    /// <param name="transaction">optional SQL Transaction</param>
    public int Insert(DbTransaction transaction)
    {
        return Insert(NationalityId, PhotoPath, transaction);
    }

    /// <summary>
    /// Insert current GenGenerals to database.
    /// </summary>
    public int Insert()
    {
        return this.Insert((DbTransaction)null);
    }
    #endregion


    #region UPDATE
    public static void Update(System.Int32? generalId, System.Int32? nationalityId, System.String photoPath, DbTransaction transaction)
    {
        DataSet ds;
        Database db;
        string sqlCommand;
        DbCommand dbCommand;


        db = DatabaseFactory.CreateDatabase(DataConnection);
        sqlCommand = "[dbo].GenGenerals_UPDATE";
        dbCommand = db.GetStoredProcCommand(sqlCommand);
        db.DiscoverParameters(dbCommand);
        dbCommand.Parameters["@generalId"].Value = generalId;
        dbCommand.Parameters["@nationalityId"].Value = nationalityId;
        dbCommand.Parameters["@photoPath"].Value = photoPath;

        if (transaction == null)
            db.ExecuteNonQuery(dbCommand);
        else
            db.ExecuteNonQuery(dbCommand, transaction);
        return;
    }

    [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
    public static void Update(System.Int32? generalId, System.Int32? nationalityId, System.String photoPath)
    {
        Update(generalId, nationalityId, photoPath, null);
    }

    public static void Update(GenGenerals genGenerals)
    {
        genGenerals.Update();
    }

    public static void Update(GenGenerals genGenerals, DbTransaction transaction)
    {
        genGenerals.Update(transaction);
    }

    /// <summary>
    /// Updates changes to the database.
    /// </summary>
    /// <param name="transaction">optional SQL Transaction</param>
    public void Update(DbTransaction transaction)
    {
        DataSet ds;
        Database db;
        string sqlCommand;
        DbCommand dbCommand;


        db = DatabaseFactory.CreateDatabase(DataConnection);
        sqlCommand = "[dbo].GenGenerals_UPDATE";
        dbCommand = db.GetStoredProcCommand(sqlCommand);
        db.DiscoverParameters(dbCommand);
        dbCommand.Parameters["@generalId"].SourceColumn = "GeneralId";
        dbCommand.Parameters["@nationalityId"].SourceColumn = "NationalityId";
        dbCommand.Parameters["@photoPath"].SourceColumn = "PhotoPath";

        ds = new DataSet();
        this.MapTo(ds);
        ds.AcceptChanges();
        ds.Tables[0].Rows[0].SetModified();
        if (transaction == null)
            db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
        else
            db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
        return;
    }

    /// <summary>
    /// Updates changes to the database.
    /// </summary>
    public void Update()
    {
        this.Update((DbTransaction)null);
    }
    #endregion


    #region DELETE
    [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
    public static void Delete(System.Int32? generalId, DbTransaction transaction)
    {
        Database db;
        string sqlCommand;
        DbCommand dbCommand;


        db = DatabaseFactory.CreateDatabase(DataConnection);
        sqlCommand = "[dbo].GenGenerals_DELETE";
        dbCommand = db.GetStoredProcCommand(sqlCommand, generalId);

        // Execute.
        if (transaction != null)
        {
            db.ExecuteNonQuery(dbCommand, transaction);
        }
        else
        {
            db.ExecuteNonQuery(dbCommand);
        }
    }

    [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
    public static void Delete(System.Int32? generalId)
    {
        Delete(
        generalId);
    }

    /// <summary>
    /// Delete current GenGenerals from database.
    /// </summary>
    /// <param name="transaction">optional SQL Transaction</param>
    public void Delete(DbTransaction transaction)
    {
        Database db;
        string sqlCommand;
        DbCommand dbCommand;


        db = DatabaseFactory.CreateDatabase(DataConnection);
        sqlCommand = "[dbo].GenGenerals_DELETE";
        dbCommand = db.GetStoredProcCommand(sqlCommand, GeneralId);

        // Execute.
        if (transaction != null)
        {
            db.ExecuteNonQuery(dbCommand, transaction);
        }
        else
        {
            db.ExecuteNonQuery(dbCommand);
        }
        this.GeneralId = null;
    }

    /// <summary>
    /// Delete current GenGenerals from database.
    /// </summary>
    public void Delete()
    {
        this.Delete((DbTransaction)null);
    }

    #endregion


    #region SEARCH
    [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
    public static GenGenerals[] Search(System.Int32? generalId, System.Int32? nationalityId, System.String photoPath)
    {
        DataSet ds;
        Database db;
        string sqlCommand;
        DbCommand dbCommand;


        db = DatabaseFactory.CreateDatabase(DataConnection);
        sqlCommand = "[dbo].GenGenerals_SEARCH";
        dbCommand = db.GetStoredProcCommand(sqlCommand, generalId, nationalityId, photoPath);

        ds = db.ExecuteDataSet(dbCommand);
        ds.Tables[0].TableName = TABLE_NAME;
        return GenGenerals.MapFrom(ds);
    }


    public static GenGenerals[] Search(GenGenerals searchObject)
    {
        return Search(searchObject.GeneralId, searchObject.NationalityId, searchObject.PhotoPath);
    }

    /// <summary>
    /// Returns all GenGenerals objects.
    /// </summary>
    /// <returns>List of all GenGenerals objects. </returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
    public static GenGenerals[] Search()
    {
        return Search(null, null, null);
    }

    #endregion


    #endregion


    #endregion
	
	
}


}
