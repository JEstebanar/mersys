﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace General.PersonEmergencyContacts
{

    [DataObject]
    [Serializable]
    public partial class GenPersonEmergencyContacts
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[gen_PersonEmergencyContacts]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _emergencyContactId;
        private System.Int32? _personId;
        private System.Int32? _contactId;
        private System.Int32? _relationshipId;
        private System.String _allergies;
        private System.String _drugs;
        private System.Int32? _createdBY;
        private System.DateTime? _createdDate;
        private System.Int32? _modifiedBy;
        private System.DateTime? _modifiedDate;

        #endregion


        #region Properties
        public System.Int32? EmergencyContactId
        {
            get
            {
                return _emergencyContactId;
            }
            set
            {
                _emergencyContactId = value;
            }
        }

        public System.Int32? PersonId
        {
            get
            {
                return _personId;
            }
            set
            {
                _personId = value;
            }
        }

        public System.Int32? ContactId
        {
            get
            {
                return _contactId;
            }
            set
            {
                _contactId = value;
            }
        }

        public System.Int32? RelationshipId
        {
            get
            {
                return _relationshipId;
            }
            set
            {
                _relationshipId = value;
            }
        }

        public System.String Allergies
        {
            get
            {
                return _allergies;
            }
            set
            {
                _allergies = value;
            }
        }

        public System.String Drugs
        {
            get
            {
                return _drugs;
            }
            set
            {
                _drugs = value;
            }
        }

        public System.Int32? CreatedBY
        {
            get
            {
                return _createdBY;
            }
            set
            {
                _createdBY = value;
            }
        }

        public System.DateTime? CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        public System.Int32? ModifiedBy
        {
            get
            {
                return _modifiedBy;
            }
            set
            {
                _modifiedBy = value;
            }
        }

        public System.DateTime? ModifiedDate
        {
            get
            {
                return _modifiedDate;
            }
            set
            {
                _modifiedDate = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("EmergencyContactId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("PersonId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ContactId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("RelationshipId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("Allergies", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("Drugs", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("CreatedBY", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime));
            ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (EmergencyContactId == null)
                dr["EmergencyContactId"] = DBNull.Value;
            else
                dr["EmergencyContactId"] = EmergencyContactId;

            if (PersonId == null)
                dr["PersonId"] = DBNull.Value;
            else
                dr["PersonId"] = PersonId;

            if (ContactId == null)
                dr["ContactId"] = DBNull.Value;
            else
                dr["ContactId"] = ContactId;

            if (RelationshipId == null)
                dr["RelationshipId"] = DBNull.Value;
            else
                dr["RelationshipId"] = RelationshipId;

            if (Allergies == null)
                dr["Allergies"] = DBNull.Value;
            else
                dr["Allergies"] = Allergies;

            if (Drugs == null)
                dr["Drugs"] = DBNull.Value;
            else
                dr["Drugs"] = Drugs;

            if (CreatedBY == null)
                dr["CreatedBY"] = DBNull.Value;
            else
                dr["CreatedBY"] = CreatedBY;

            if (CreatedDate == null)
                dr["CreatedDate"] = DBNull.Value;
            else
                dr["CreatedDate"] = CreatedDate;

            if (ModifiedBy == null)
                dr["ModifiedBy"] = DBNull.Value;
            else
                dr["ModifiedBy"] = ModifiedBy;

            if (ModifiedDate == null)
                dr["ModifiedDate"] = DBNull.Value;
            else
                dr["ModifiedDate"] = ModifiedDate;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            EmergencyContactId = dr["EmergencyContactId"] != DBNull.Value ? Convert.ToInt32(dr["EmergencyContactId"]) : EmergencyContactId = null;
            PersonId = dr["PersonId"] != DBNull.Value ? Convert.ToInt32(dr["PersonId"]) : PersonId = null;
            ContactId = dr["ContactId"] != DBNull.Value ? Convert.ToInt32(dr["ContactId"]) : ContactId = null;
            RelationshipId = dr["RelationshipId"] != DBNull.Value ? Convert.ToInt32(dr["RelationshipId"]) : RelationshipId = null;
            Allergies = dr["Allergies"] != DBNull.Value ? Convert.ToString(dr["Allergies"]) : Allergies = null;
            Drugs = dr["Drugs"] != DBNull.Value ? Convert.ToString(dr["Drugs"]) : Drugs = null;
            CreatedBY = dr["CreatedBY"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBY"]) : CreatedBY = null;
            CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
            ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
            ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
        }

        public static GenPersonEmergencyContacts[] MapFrom(DataSet ds)
        {
            List<GenPersonEmergencyContacts> objects;


            // Initialise Collection.
            objects = new List<GenPersonEmergencyContacts>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[gen_PersonEmergencyContacts] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[gen_PersonEmergencyContacts] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                GenPersonEmergencyContacts instance = new GenPersonEmergencyContacts();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenPersonEmergencyContacts Get(System.Int32 emergencyContactId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            GenPersonEmergencyContacts instance;


            instance = new GenPersonEmergencyContacts();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPersonEmergencyContacts_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, emergencyContactId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenPersonEmergencyContacts ID:" + emergencyContactId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.Int32? personId, System.Int32? contactId, System.Int32? relationshipId, System.String allergies, System.String drugs, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPersonEmergencyContacts_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, personId, contactId, relationshipId, allergies, drugs, createdBY, createdDate, modifiedBy, modifiedDate);

            if (transaction == null)
                this.EmergencyContactId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.EmergencyContactId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.Int32? personId, System.Int32? contactId, System.Int32? relationshipId, System.String allergies, System.String drugs, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
        {
            Insert(personId, contactId, relationshipId, allergies, drugs, createdBY, createdDate, modifiedBy, modifiedDate, null);
        }
        /// <summary>
        /// Insert current GenPersonEmergencyContacts to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(PersonId, ContactId, RelationshipId, Allergies, Drugs, CreatedBY, CreatedDate, ModifiedBy, ModifiedDate, transaction);
        }

        /// <summary>
        /// Insert current GenPersonEmergencyContacts to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? emergencyContactId, System.Int32? personId, System.Int32? contactId, System.Int32? relationshipId, System.String allergies, System.String drugs, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPersonEmergencyContacts_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@emergencyContactId"].Value = emergencyContactId;
            dbCommand.Parameters["@personId"].Value = personId;
            dbCommand.Parameters["@contactId"].Value = contactId;
            dbCommand.Parameters["@relationshipId"].Value = relationshipId;
            dbCommand.Parameters["@allergies"].Value = allergies;
            dbCommand.Parameters["@drugs"].Value = drugs;
            dbCommand.Parameters["@createdBY"].Value = createdBY;
            dbCommand.Parameters["@createdDate"].Value = createdDate;
            dbCommand.Parameters["@modifiedBy"].Value = modifiedBy;
            dbCommand.Parameters["@modifiedDate"].Value = modifiedDate;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? emergencyContactId, System.Int32? personId, System.Int32? contactId, System.Int32? relationshipId, System.String allergies, System.String drugs, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
        {
            Update(emergencyContactId, personId, contactId, relationshipId, allergies, drugs, createdBY, createdDate, modifiedBy, modifiedDate, null);
        }

        public static void Update(GenPersonEmergencyContacts genPersonEmergencyContacts)
        {
            genPersonEmergencyContacts.Update();
        }

        public static void Update(GenPersonEmergencyContacts genPersonEmergencyContacts, DbTransaction transaction)
        {
            genPersonEmergencyContacts.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPersonEmergencyContacts_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@emergencyContactId"].SourceColumn = "EmergencyContactId";
            dbCommand.Parameters["@personId"].SourceColumn = "PersonId";
            dbCommand.Parameters["@contactId"].SourceColumn = "ContactId";
            dbCommand.Parameters["@relationshipId"].SourceColumn = "RelationshipId";
            dbCommand.Parameters["@allergies"].SourceColumn = "Allergies";
            dbCommand.Parameters["@drugs"].SourceColumn = "Drugs";
            dbCommand.Parameters["@createdBY"].SourceColumn = "CreatedBY";
            dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
            dbCommand.Parameters["@modifiedBy"].SourceColumn = "ModifiedBy";
            dbCommand.Parameters["@modifiedDate"].SourceColumn = "ModifiedDate";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? emergencyContactId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPersonEmergencyContacts_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, emergencyContactId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? emergencyContactId)
        {
            Delete(
            emergencyContactId);
        }

        /// <summary>
        /// Delete current GenPersonEmergencyContacts from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPersonEmergencyContacts_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, EmergencyContactId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.EmergencyContactId = null;
        }

        /// <summary>
        /// Delete current GenPersonEmergencyContacts from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenPersonEmergencyContacts[] Search(System.Int32? emergencyContactId, System.Int32? personId, System.Int32? contactId, System.Int32? relationshipId, System.String allergies, System.String drugs, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPersonEmergencyContacts_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, emergencyContactId, personId, contactId, relationshipId, allergies, drugs, createdBY, createdDate, modifiedBy, modifiedDate);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return GenPersonEmergencyContacts.MapFrom(ds);
        }


        public static GenPersonEmergencyContacts[] Search(GenPersonEmergencyContacts searchObject)
        {
            return Search(searchObject.EmergencyContactId, searchObject.PersonId, searchObject.ContactId, searchObject.RelationshipId, searchObject.Allergies, searchObject.Drugs, searchObject.CreatedBY, searchObject.CreatedDate, searchObject.ModifiedBy, searchObject.ModifiedDate);
        }

        /// <summary>
        /// Returns all GenPersonEmergencyContacts objects.
        /// </summary>
        /// <returns>List of all GenPersonEmergencyContacts objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static GenPersonEmergencyContacts[] Search()
        {
            return Search(null, null, null, null, null, null, null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


    }


}
