﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace General.EntityTypes
{

[DataObject]
[Serializable]
public partial class GenEntityTypes
{
	
	
	#region Constants
    private static readonly string TABLE_NAME = "[dbo].[gen_EntityTypes]";
    private static readonly string DataConnection = "DataConnection";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _typeId;
	private System.String _typeName;
	private System.String _typeDescription;
	private System.Boolean? _typeStatus;
	private System.Int32? _createdBY;
	private System.DateTime? _createdDate;
	private System.Int32? _modifiedBy;
	private System.DateTime? _modifiedDate;
	
	#endregion
	
	
	#region Properties
	public System.Int32? TypeId
	{
		get
		{
			return _typeId;
		}
		set
		{
			_typeId = value;
		}
	}
	
	public System.String TypeName
	{
		get
		{
			return _typeName;
		}
		set
		{
			_typeName = value;
		}
	}
	
	public System.String TypeDescription
	{
		get
		{
			return _typeDescription;
		}
		set
		{
			_typeDescription = value;
		}
	}
	
	public System.Boolean? TypeStatus
	{
		get
		{
			return _typeStatus;
		}
		set
		{
			_typeStatus = value;
		}
	}
	
	public System.Int32? CreatedBY
	{
		get
		{
			return _createdBY;
		}
		set
		{
			_createdBY = value;
		}
	}
	
	public System.DateTime? CreatedDate
	{
		get
		{
			return _createdDate;
		}
		set
		{
			_createdDate = value;
		}
	}
	
	public System.Int32? ModifiedBy
	{
		get
		{
			return _modifiedBy;
		}
		set
		{
			_modifiedBy = value;
		}
	}
	
	public System.DateTime? ModifiedDate
	{
		get
		{
			return _modifiedDate;
		}
		set
		{
			_modifiedDate = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("TypeId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("TypeName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("TypeDescription", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("TypeStatus", typeof(System.Boolean) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedBY", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (TypeId == null)
		dr["TypeId"] = DBNull.Value;
		else
		dr["TypeId"] = TypeId;
		
		if (TypeName == null)
		dr["TypeName"] = DBNull.Value;
		else
		dr["TypeName"] = TypeName;
		
		if (TypeDescription == null)
		dr["TypeDescription"] = DBNull.Value;
		else
		dr["TypeDescription"] = TypeDescription;
		
		if (TypeStatus == null)
		dr["TypeStatus"] = DBNull.Value;
		else
		dr["TypeStatus"] = TypeStatus;
		
		if (CreatedBY == null)
		dr["CreatedBY"] = DBNull.Value;
		else
		dr["CreatedBY"] = CreatedBY;
		
		if (CreatedDate == null)
		dr["CreatedDate"] = DBNull.Value;
		else
		dr["CreatedDate"] = CreatedDate;
		
		if (ModifiedBy == null)
		dr["ModifiedBy"] = DBNull.Value;
		else
		dr["ModifiedBy"] = ModifiedBy;
		
		if (ModifiedDate == null)
		dr["ModifiedDate"] = DBNull.Value;
		else
		dr["ModifiedDate"] = ModifiedDate;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		TypeId = dr["TypeId"] != DBNull.Value ? Convert.ToInt32(dr["TypeId"]) : TypeId = null;
		TypeName = dr["TypeName"] != DBNull.Value ? Convert.ToString(dr["TypeName"]) : TypeName = null;
		TypeDescription = dr["TypeDescription"] != DBNull.Value ? Convert.ToString(dr["TypeDescription"]) : TypeDescription = null;
		TypeStatus = dr["TypeStatus"] != DBNull.Value ? Convert.ToBoolean(dr["TypeStatus"]) : TypeStatus = null;
		CreatedBY = dr["CreatedBY"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBY"]) : CreatedBY = null;
		CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
		ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
		ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
	}
	
	public static GenEntityTypes[] MapFrom(DataSet ds)
	{
		List<GenEntityTypes> objects;
		
		
		// Initialise Collection.
		objects = new List<GenEntityTypes>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[gen_EntityTypes] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[gen_EntityTypes] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			GenEntityTypes instance = new GenEntityTypes();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static GenEntityTypes Get(System.Int32 typeId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		GenEntityTypes instance;
		
		
		instance = new GenEntityTypes();
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].GenEntityTypes_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, typeId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenEntityTypes ID:" + typeId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.String typeName, System.String typeDescription, System.Boolean? typeStatus, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].GenEntityTypes_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, typeName, typeDescription, typeStatus, createdBY, createdDate, modifiedBy, modifiedDate);
		
		if (transaction == null)
		this.TypeId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.TypeId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.String typeName, System.String typeDescription, System.Boolean? typeStatus, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Insert(typeName, typeDescription, typeStatus, createdBY, createdDate, modifiedBy, modifiedDate, null);
	}
	/// <summary>
	/// Insert current GenEntityTypes to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(TypeName, TypeDescription, TypeStatus, CreatedBY, CreatedDate, ModifiedBy, ModifiedDate, transaction);
	}
	
	/// <summary>
	/// Insert current GenEntityTypes to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? typeId, System.String typeName, System.String typeDescription, System.Boolean? typeStatus, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].GenEntityTypes_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@typeId"].Value = typeId;
		dbCommand.Parameters["@typeName"].Value = typeName;
		dbCommand.Parameters["@typeDescription"].Value = typeDescription;
		dbCommand.Parameters["@typeStatus"].Value = typeStatus;
		dbCommand.Parameters["@createdBY"].Value = createdBY;
		dbCommand.Parameters["@createdDate"].Value = createdDate;
		dbCommand.Parameters["@modifiedBy"].Value = modifiedBy;
		dbCommand.Parameters["@modifiedDate"].Value = modifiedDate;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? typeId, System.String typeName, System.String typeDescription, System.Boolean? typeStatus, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Update(typeId, typeName, typeDescription, typeStatus, createdBY, createdDate, modifiedBy, modifiedDate, null);
	}
	
	public static void Update(GenEntityTypes genEntityTypes)
	{
		genEntityTypes.Update();
	}
	
	public static void Update(GenEntityTypes genEntityTypes, DbTransaction transaction)
	{
		genEntityTypes.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].GenEntityTypes_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@typeId"].SourceColumn = "TypeId";
		dbCommand.Parameters["@typeName"].SourceColumn = "TypeName";
		dbCommand.Parameters["@typeDescription"].SourceColumn = "TypeDescription";
		dbCommand.Parameters["@typeStatus"].SourceColumn = "TypeStatus";
		dbCommand.Parameters["@createdBY"].SourceColumn = "CreatedBY";
		dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
		dbCommand.Parameters["@modifiedBy"].SourceColumn = "ModifiedBy";
		dbCommand.Parameters["@modifiedDate"].SourceColumn = "ModifiedDate";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? typeId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].GenEntityTypes_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, typeId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? typeId)
	{
		Delete(
		typeId);
	}
	
	/// <summary>
	/// Delete current GenEntityTypes from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].GenEntityTypes_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, TypeId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.TypeId = null;
	}
	
	/// <summary>
	/// Delete current GenEntityTypes from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static GenEntityTypes[] Search(System.Int32? typeId, System.String typeName, System.String typeDescription, System.Boolean? typeStatus, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].GenEntityTypes_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, typeId, typeName, typeDescription, typeStatus, createdBY, createdDate, modifiedBy, modifiedDate);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return GenEntityTypes.MapFrom(ds);
	}
	
	
	public static GenEntityTypes[] Search(GenEntityTypes searchObject)
	{
		return Search ( searchObject.TypeId, searchObject.TypeName, searchObject.TypeDescription, searchObject.TypeStatus, searchObject.CreatedBY, searchObject.CreatedDate, searchObject.ModifiedBy, searchObject.ModifiedDate);
	}
	
	/// <summary>
	/// Returns all GenEntityTypes objects.
	/// </summary>
	/// <returns>List of all GenEntityTypes objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static GenEntityTypes[] Search()
	{
		return Search ( null, null, null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}


}
