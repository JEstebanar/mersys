﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.IO;

namespace General.Forms
{
    public class GenForms
    {
        public static void CheckItemsInGridView(ref GridView grv, string HeaderElement, string ContentElement)
        {
            bool _bit;
            if ((grv.HeaderRow.FindControl(HeaderElement) as CheckBox).Checked)
                _bit = true;
            else
                _bit = false;

            foreach (GridViewRow row in grv.Rows)
                (row.FindControl(ContentElement) as CheckBox).Checked = _bit;
        }

        public static void UnCheckItemsInGridView(ref GridView grv, string HeaderElement, string ContentElement)
        {
            (grv.HeaderRow.FindControl(HeaderElement) as CheckBox).Checked = false;
            CheckItemsInGridView(ref grv, HeaderElement, ContentElement);
        }

        public static int CountSelectedElementsInGridView(ref GridView grv, String ElementName)
        {
            int _selected = 0;
            foreach (GridViewRow row in grv.Rows)
            {
                if ((row.FindControl(ElementName) as CheckBox).Checked)
                    _selected++;
            }
            return _selected;
        }

        public static string GetTransferDocumentsPath()
        {
            return "~/Files/Residents/Transfers/";
        }

        public static string GetIdentificationDocumentsPath()
        {
            return "~/Files/General/Identifications/";
        }

        public static string GetPostFilePath()
        {
            return "/Files/Posts/images/";
        }

        public static void DisableCheckInGridview(GridView grv, string fildName, int conditionId, string chk)
        {
            foreach (GridViewRow row in grv.Rows)
            {
                if ((int)grv.DataKeys[row.RowIndex].Values[fildName] == conditionId)
                    (row.FindControl(chk) as CheckBox).Enabled = false;
            }
        }
    }
}
