﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.IO;
using General.GeneralCommons;
using General.Forms;
using System.Web;
using System.Globalization;

namespace General.Utilities
{
    public class GenUtilities
    {
        public static bool IsNumeric(string txt)
        {
            return Regex.IsMatch(txt, @"^[0-9]+$");
        }

        public static bool IsAlpha(string txt)
        {
            return Regex.IsMatch(txt, @"^[a-z]+$");
        }

        public static bool AllowCapitalLetters(string txt)
        {
            return Regex.IsMatch(txt, "^[A-Z]+$");
        }

        public static bool IsAlphaNumeric(string txt)
        {
            return Regex.IsMatch(txt, "^[a-z0-9]+$");
        }

        public static void SetImagenToPopUp(ref Image img, GeneralCommon.TypeFilePaths type, string name)
        {
            string filePath = string.Empty;
            switch (type)
            {
                case GeneralCommon.TypeFilePaths.Identification:
                    filePath = GenForms.GetIdentificationDocumentsPath();
                    break;
                case GeneralCommon.TypeFilePaths.TransferDocuments:
                    filePath = GenForms.GetTransferDocumentsPath();
                    break;
            }
            img.ImageUrl = filePath + name;
        }

        public static string SaveFileInPath(ref FileUpload ObjUpload, GeneralCommon.TypeFilePaths type, string optionalName)
        {
            string filePath = string.Empty;
            switch (type)
            {
                case GeneralCommon.TypeFilePaths.Identification:
                    filePath = GenForms.GetIdentificationDocumentsPath();
                    break;
                case GeneralCommon.TypeFilePaths.TransferDocuments:
                    filePath = GenForms.GetTransferDocumentsPath();
                    break;
                case GeneralCommon.TypeFilePaths.Posts:
                    filePath = GenForms.GetPostFilePath();
                    break;
            }
            string fileEextensio = Path.GetExtension(ObjUpload.FileName);
            string newFileName;
            if (type == GeneralCommon.TypeFilePaths.Posts)
                newFileName = Utilities.GenUtilities.GenerateTitle(optionalName) + fileEextensio;
            else
                newFileName = Guid.NewGuid().ToString() + fileEextensio;
            ObjUpload.SaveAs(HttpContext.Current.Server.MapPath(filePath + newFileName));

            return newFileName;
        }

        private string SaveFileInPath11(ref FileUpload ObjUpload, string filePath)
        {
            //string fileName = Path.GetFileName(fileUp.PostedFile.FileName);
            string fileEextensio = Path.GetExtension(ObjUpload.FileName);
            string newFileName = Guid.NewGuid().ToString() + fileEextensio;
            ObjUpload.SaveAs(HttpContext.Current.Server.MapPath(filePath + newFileName));

            return newFileName;
        }

        public static bool IsValidEmail(string strMailAddress)
        {
            // Return true if strIn is in valid e-mail format.
            return Regex.IsMatch(strMailAddress, @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
        }

        public static int GetMonthBytxtName(string txt)
        {
            int month = 0;
            switch (txt)
            {
                case "txtJul":
                    month = 7;
                    break;
                case "txtAug":
                    month = 8;
                    break;
                case "txtSep":
                    month = 9;
                    break;
                case "txtOct":
                    month = 10;
                    break;
                case "txtNov":
                    month = 11;
                    break;
                case "txtDec":
                    month = 12;
                    break;
                case "txtJan":
                    month = 1;
                    break;
                case "txtFeb":
                    month = 2;
                    break;
                case "txtMar":
                    month = 3;
                    break;
                case "txtApr":
                    month = 4;
                    break;
                case "txtMay":
                    month = 5;
                    break;
                case "txtJun":
                    month = 6;
                    break;
            }
            return month;
        }

        public static string GenerateURL(string title, int id, GeneralCommon.PostTypes postType)
        {
            string strTitle = GenerateTitle(title);

            switch (postType)
            {
                case GeneralCommon.PostTypes.Comunications:
                    strTitle = "/Modules/Posts/" + id.ToString() + "/" + strTitle;
                    break;
                case GeneralCommon.PostTypes.Articles:
                    strTitle = "/Entradas/Articulos/" + id.ToString() + "/" + strTitle;
                    break;
            }
            return strTitle;
        }

        public static string GenerateTitle(string title)
        {
            //string textoOriginal = "Mañana será otro día";//transformación UNICODE
            //string textoNormalizado = textoOriginal.Normalize(NormalizationForm.FormD);
            ////coincide todo lo que no sean letras y números ascii o espacio
            ////y lo reemplazamos por una cadena vacía.Regex reg = new Regex("[^a-zA-Z0-9 ]");
            //string textoSinAcentos = reg.Replace(textoNormalizado, "");
            //Debug.WriteLine(textoSinAcentos); //muestra 'Manana sera otro dia'

            string normalizeTitle = title.Normalize(NormalizationForm.FormD);
            Regex reg = new Regex("[^a-zA-Z0-9 ]");
            string newTitle = reg.Replace(normalizeTitle, "");

            string strTitle = newTitle.Trim();

            //string strTitle = title.Trim();
            strTitle = strTitle.ToLower();
            strTitle = strTitle.Replace("c#", "C-Sharp");
            strTitle = strTitle.Replace(" ", "-");
            strTitle = strTitle.Trim();
            strTitle = strTitle.Trim('-');

            return strTitle;
        }

        public static string ConvertDateToLongString(DateTime dt)
        {
            return dt.ToString("D", new CultureInfo("es-ES"));
        }
    }
}
