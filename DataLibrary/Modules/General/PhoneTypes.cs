﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace General.PhoneType
{
    [DataObject]
    [Serializable]
    public partial class GenPhoneTypes
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[gen_PhoneTypes]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _typeId;
        private System.String _typeName;
        private System.Boolean? _typeStatus;

        #endregion


        #region Properties
        public System.Int32? TypeId
        {
            get
            {
                return _typeId;
            }
            set
            {
                _typeId = value;
            }
        }

        public System.String TypeName
        {
            get
            {
                return _typeName;
            }
            set
            {
                _typeName = value;
            }
        }

        public System.Boolean? TypeStatus
        {
            get
            {
                return _typeStatus;
            }
            set
            {
                _typeStatus = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("TypeId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("TypeName", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("TypeStatus", typeof(System.Boolean));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (TypeId == null)
                dr["TypeId"] = DBNull.Value;
            else
                dr["TypeId"] = TypeId;

            if (TypeName == null)
                dr["TypeName"] = DBNull.Value;
            else
                dr["TypeName"] = TypeName;

            if (TypeStatus == null)
                dr["TypeStatus"] = DBNull.Value;
            else
                dr["TypeStatus"] = TypeStatus;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            TypeId = dr["TypeId"] != DBNull.Value ? Convert.ToInt32(dr["TypeId"]) : TypeId = null;
            TypeName = dr["TypeName"] != DBNull.Value ? Convert.ToString(dr["TypeName"]) : TypeName = null;
            TypeStatus = dr["TypeStatus"] != DBNull.Value ? Convert.ToBoolean(dr["TypeStatus"]) : TypeStatus = null;
        }

        public static GenPhoneTypes[] MapFrom(DataSet ds)
        {
            List<GenPhoneTypes> objects;


            // Initialise Collection.
            objects = new List<GenPhoneTypes>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[gen_PhoneTypes] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[gen_PhoneTypes] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                GenPhoneTypes instance = new GenPhoneTypes();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenPhoneTypes Get(System.Int32 typeId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            GenPhoneTypes instance;


            instance = new GenPhoneTypes();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPhoneTypes_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, typeId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenPhoneTypes ID:" + typeId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.String typeName, System.Boolean? typeStatus, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPhoneTypes_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, typeName, typeStatus);

            if (transaction == null)
                this.TypeId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.TypeId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.String typeName, System.Boolean? typeStatus)
        {
            Insert(typeName, typeStatus, null);
        }
        /// <summary>
        /// Insert current GenPhoneTypes to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(TypeName, TypeStatus, transaction);
        }

        /// <summary>
        /// Insert current GenPhoneTypes to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? typeId, System.String typeName, System.Boolean? typeStatus, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPhoneTypes_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@typeId"].Value = typeId;
            dbCommand.Parameters["@typeName"].Value = typeName;
            dbCommand.Parameters["@typeStatus"].Value = typeStatus;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? typeId, System.String typeName, System.Boolean? typeStatus)
        {
            Update(typeId, typeName, typeStatus, null);
        }

        public static void Update(GenPhoneTypes genPhoneTypes)
        {
            genPhoneTypes.Update();
        }

        public static void Update(GenPhoneTypes genPhoneTypes, DbTransaction transaction)
        {
            genPhoneTypes.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPhoneTypes_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@typeId"].SourceColumn = "TypeId";
            dbCommand.Parameters["@typeName"].SourceColumn = "TypeName";
            dbCommand.Parameters["@typeStatus"].SourceColumn = "TypeStatus";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? typeId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPhoneTypes_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, typeId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? typeId)
        {
            Delete(
            typeId);
        }

        /// <summary>
        /// Delete current GenPhoneTypes from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPhoneTypes_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, TypeId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.TypeId = null;
        }

        /// <summary>
        /// Delete current GenPhoneTypes from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenPhoneTypes[] Search(System.Int32? typeId, System.String typeName, System.Boolean? typeStatus)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenPhoneTypes_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, typeId, typeName, typeStatus);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return GenPhoneTypes.MapFrom(ds);
        }


        public static GenPhoneTypes[] Search(GenPhoneTypes searchObject)
        {
            return Search(searchObject.TypeId, searchObject.TypeName, searchObject.TypeStatus);
        }

        /// <summary>
        /// Returns all GenPhoneTypes objects.
        /// </summary>
        /// <returns>List of all GenPhoneTypes objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static GenPhoneTypes[] Search()
        {
            return Search(null, null, null);
        }

        #endregion


        #endregion


        #endregion


        #region Custom

        [Serializable]
        public class PhoneTypes
        {
            public int typeId { get; set; }
            public string typeName { get; set; }
        }

        public static IList<PhoneTypes> GetPhoneTypes()
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            PhoneTypes instance;
            IList<PhoneTypes> llist = new List<PhoneTypes>();

            instance = new PhoneTypes();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "GenPhoneTypes_Get";
            dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new PhoneTypes();
                instance.typeId = (int)item["TypeId"];
                instance.typeName = (string)item["TypeName"];
                llist.Add(instance);
            }
            return llist;
        }
        #endregion
    }


}
