﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace General.Nationalities
{

[DataObject]
[Serializable]
public partial class GenNationalities
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[gen_Nationalities]";
        private static readonly string DataConnection = "DataConnection";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _nationalityId;
	private System.String _nationalityName;
	private System.Boolean? _nationalityStatus;
	
	#endregion
	
	
	#region Properties
	public System.Int32? NationalityId
	{
		get
		{
			return _nationalityId;
		}
		set
		{
			_nationalityId = value;
		}
	}
	
	public System.String NationalityName
	{
		get
		{
			return _nationalityName;
		}
		set
		{
			_nationalityName = value;
		}
	}
	
	public System.Boolean? NationalityStatus
	{
		get
		{
			return _nationalityStatus;
		}
		set
		{
			_nationalityStatus = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("NationalityId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("NationalityName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("NationalityStatus", typeof(System.Boolean) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (NationalityId == null)
		dr["NationalityId"] = DBNull.Value;
		else
		dr["NationalityId"] = NationalityId;
		
		if (NationalityName == null)
		dr["NationalityName"] = DBNull.Value;
		else
		dr["NationalityName"] = NationalityName;
		
		if (NationalityStatus == null)
		dr["NationalityStatus"] = DBNull.Value;
		else
		dr["NationalityStatus"] = NationalityStatus;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		NationalityId = dr["NationalityId"] != DBNull.Value ? Convert.ToInt32(dr["NationalityId"]) : NationalityId = null;
		NationalityName = dr["NationalityName"] != DBNull.Value ? Convert.ToString(dr["NationalityName"]) : NationalityName = null;
		NationalityStatus = dr["NationalityStatus"] != DBNull.Value ? Convert.ToBoolean(dr["NationalityStatus"]) : NationalityStatus = null;
	}
	
	public static GenNationalities[] MapFrom(DataSet ds)
	{
		List<GenNationalities> objects;
		
		
		// Initialise Collection.
		objects = new List<GenNationalities>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[gen_Nationalities] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[gen_Nationalities] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			GenNationalities instance = new GenNationalities();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static GenNationalities Get(System.Int32 nationalityId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		GenNationalities instance;
		
		
		instance = new GenNationalities();
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].GenNationalities_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, nationalityId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenNationalities ID:" + nationalityId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.String nationalityName, System.Boolean? nationalityStatus, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].GenNationalities_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, nationalityName, nationalityStatus);
		
		if (transaction == null)
		this.NationalityId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.NationalityId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.String nationalityName, System.Boolean? nationalityStatus)
	{
		Insert(nationalityName, nationalityStatus, null);
	}
	/// <summary>
	/// Insert current GenNationalities to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(NationalityName, NationalityStatus, transaction);
	}
	
	/// <summary>
	/// Insert current GenNationalities to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? nationalityId, System.String nationalityName, System.Boolean? nationalityStatus, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].GenNationalities_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@nationalityId"].Value = nationalityId;
		dbCommand.Parameters["@nationalityName"].Value = nationalityName;
		dbCommand.Parameters["@nationalityStatus"].Value = nationalityStatus;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? nationalityId, System.String nationalityName, System.Boolean? nationalityStatus)
	{
		Update(nationalityId, nationalityName, nationalityStatus, null);
	}
	
	public static void Update(GenNationalities genNationalities)
	{
		genNationalities.Update();
	}
	
	public static void Update(GenNationalities genNationalities, DbTransaction transaction)
	{
		genNationalities.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].GenNationalities_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@nationalityId"].SourceColumn = "NationalityId";
		dbCommand.Parameters["@nationalityName"].SourceColumn = "NationalityName";
		dbCommand.Parameters["@nationalityStatus"].SourceColumn = "NationalityStatus";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? nationalityId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].GenNationalities_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, nationalityId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? nationalityId)
	{
		Delete(
		nationalityId);
	}
	
	/// <summary>
	/// Delete current GenNationalities from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].GenNationalities_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, NationalityId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.NationalityId = null;
	}
	
	/// <summary>
	/// Delete current GenNationalities from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static GenNationalities[] Search(System.Int32? nationalityId, System.String nationalityName, System.Boolean? nationalityStatus)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].GenNationalities_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, nationalityId, nationalityName, nationalityStatus);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return GenNationalities.MapFrom(ds);
	}
	
	
	public static GenNationalities[] Search(GenNationalities searchObject)
	{
		return Search ( searchObject.NationalityId, searchObject.NationalityName, searchObject.NationalityStatus);
	}
	
	/// <summary>
	/// Returns all GenNationalities objects.
	/// </summary>
	/// <returns>List of all GenNationalities objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static GenNationalities[] Search()
	{
		return Search ( null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
    #region Custom

        [Serializable]
        public class Nationalities
        {
            public int nationalityId { get; set; }
            public string nationalityName { get; set; }
        }

        public static IList<Nationalities> GetNationalities()
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            Nationalities instance;
            IList<Nationalities> llist = new List<Nationalities>();

            instance = new Nationalities();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "GenNationalities_Get";
            dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new Nationalities();
                instance.nationalityId = (int)item["NationalityId"];
                instance.nationalityName = (string)item["NationalityName"];
                llist.Add(instance);
            }
            return llist;
        }
        #endregion
}


}