﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace General.Entities
{

[DataObject]
[Serializable]
public partial class GenEntities
{
	
	
	#region Constants
    private static readonly string TABLE_NAME = "[dbo].[gen_Entities]";
    private static readonly string DataConnection = "DataConnection";
	
	#endregion


    #region Fields
    private System.Int32? _entityId;
    private System.Int32? _generalId;
    private System.Int32? _entityTypeId;
    private System.Int32? _centerId;
    private System.Boolean? _entityStatus;
    private System.Int32? _createdBy;
    private System.DateTime? _createdDate;
    private System.Int32? _modifiedBy;
    private System.DateTime? _modifiedDate;

    #endregion


    #region Properties
    public System.Int32? EntityId
    {
        get
        {
            return _entityId;
        }
        set
        {
            _entityId = value;
        }
    }

    public System.Int32? GeneralId
    {
        get
        {
            return _generalId;
        }
        set
        {
            _generalId = value;
        }
    }

    public System.Int32? EntityTypeId
    {
        get
        {
            return _entityTypeId;
        }
        set
        {
            _entityTypeId = value;
        }
    }

    public System.Int32? CenterId
    {
        get
        {
            return _centerId;
        }
        set
        {
            _centerId = value;
        }
    }

    public System.Boolean? EntityStatus
    {
        get
        {
            return _entityStatus;
        }
        set
        {
            _entityStatus = value;
        }
    }

    public System.Int32? CreatedBy
    {
        get
        {
            return _createdBy;
        }
        set
        {
            _createdBy = value;
        }
    }

    public System.DateTime? CreatedDate
    {
        get
        {
            return _createdDate;
        }
        set
        {
            _createdDate = value;
        }
    }

    public System.Int32? ModifiedBy
    {
        get
        {
            return _modifiedBy;
        }
        set
        {
            _modifiedBy = value;
        }
    }

    public System.DateTime? ModifiedDate
    {
        get
        {
            return _modifiedDate;
        }
        set
        {
            _modifiedDate = value;
        }
    }

    #endregion


    #region Methods


    #region Mapping Methods

    protected void MapTo(DataSet ds)
    {
        DataRow dr;


        if (ds == null)
            ds = new DataSet();

        if (ds.Tables["TABLE_NAME"] == null)
            ds.Tables.Add(TABLE_NAME);

        ds.Tables[TABLE_NAME].Columns.Add("EntityId", typeof(System.Int32));
        ds.Tables[TABLE_NAME].Columns.Add("GeneralId", typeof(System.Int32));
        ds.Tables[TABLE_NAME].Columns.Add("EntityTypeId", typeof(System.Int32));
        ds.Tables[TABLE_NAME].Columns.Add("CenterId", typeof(System.Int32));
        ds.Tables[TABLE_NAME].Columns.Add("EntityStatus", typeof(System.Boolean));
        ds.Tables[TABLE_NAME].Columns.Add("CreatedBy", typeof(System.Int32));
        ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime));
        ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32));
        ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime));

        dr = ds.Tables[TABLE_NAME].NewRow();

        if (EntityId == null)
            dr["EntityId"] = DBNull.Value;
        else
            dr["EntityId"] = EntityId;

        if (GeneralId == null)
            dr["GeneralId"] = DBNull.Value;
        else
            dr["GeneralId"] = GeneralId;

        if (EntityTypeId == null)
            dr["EntityTypeId"] = DBNull.Value;
        else
            dr["EntityTypeId"] = EntityTypeId;

        if (CenterId == null)
            dr["CenterId"] = DBNull.Value;
        else
            dr["CenterId"] = CenterId;

        if (EntityStatus == null)
            dr["EntityStatus"] = DBNull.Value;
        else
            dr["EntityStatus"] = EntityStatus;

        if (CreatedBy == null)
            dr["CreatedBy"] = DBNull.Value;
        else
            dr["CreatedBy"] = CreatedBy;

        if (CreatedDate == null)
            dr["CreatedDate"] = DBNull.Value;
        else
            dr["CreatedDate"] = CreatedDate;

        if (ModifiedBy == null)
            dr["ModifiedBy"] = DBNull.Value;
        else
            dr["ModifiedBy"] = ModifiedBy;

        if (ModifiedDate == null)
            dr["ModifiedDate"] = DBNull.Value;
        else
            dr["ModifiedDate"] = ModifiedDate;


        ds.Tables[TABLE_NAME].Rows.Add(dr);

    }

    protected void MapFrom(DataRow dr)
    {
        EntityId = dr["EntityId"] != DBNull.Value ? Convert.ToInt32(dr["EntityId"]) : EntityId = null;
        GeneralId = dr["GeneralId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralId"]) : GeneralId = null;
        EntityTypeId = dr["EntityTypeId"] != DBNull.Value ? Convert.ToInt32(dr["EntityTypeId"]) : EntityTypeId = null;
        CenterId = dr["CenterId"] != DBNull.Value ? Convert.ToInt32(dr["CenterId"]) : CenterId = null;
        EntityStatus = dr["EntityStatus"] != DBNull.Value ? Convert.ToBoolean(dr["EntityStatus"]) : EntityStatus = null;
        CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : CreatedBy = null;
        CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
        ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
        ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
    }

    public static GenEntities[] MapFrom(DataSet ds)
    {
        List<GenEntities> objects;


        // Initialise Collection.
        objects = new List<GenEntities>();

        // Validation.
        if (ds == null)
            throw new ApplicationException("Cannot map to dataset null.");
        else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
            return objects.ToArray();

        if (ds.Tables[TABLE_NAME] == null)
            throw new ApplicationException("Cannot find table [dbo].[gen_Entities] in DataSet.");

        if (ds.Tables[TABLE_NAME].Rows.Count < 1)
            throw new ApplicationException("Table [dbo].[gen_Entities] is empty.");

        // Map DataSet to Instance.
        foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
        {
            GenEntities instance = new GenEntities();
            instance.MapFrom(dr);
            objects.Add(instance);
        }

        // Return collection.
        return objects.ToArray();
    }


    #endregion


    #region CRUD Methods

    [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
    public static GenEntities Get(System.Int32 entityId)
    {
        DataSet ds;
        Database db;
        string sqlCommand;
        DbCommand dbCommand;
        GenEntities instance;


        instance = new GenEntities();

        db = DatabaseFactory.CreateDatabase(DataConnection);
        sqlCommand = "[dbo].GenEntities_SELECT";
        dbCommand = db.GetStoredProcCommand(sqlCommand, entityId);

        // Get results.
        ds = db.ExecuteDataSet(dbCommand);
        // Verification.
        if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenEntities ID:" + entityId.ToString() + " from Database.");
        // Return results.
        ds.Tables[0].TableName = TABLE_NAME;

        instance.MapFrom(ds.Tables[0].Rows[0]);
        return instance;
    }

    #region INSERT
    public void Insert(System.Int32? generalId, System.Int32? entityTypeId, System.Int32? centerId, System.Boolean? entityStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
    {
        Database db;
        string sqlCommand;
        DbCommand dbCommand;


        db = DatabaseFactory.CreateDatabase(DataConnection);
        sqlCommand = "[dbo].GenEntities_INSERT";
        dbCommand = db.GetStoredProcCommand(sqlCommand, generalId, entityTypeId, centerId, entityStatus, createdBy, createdDate, modifiedBy, modifiedDate);

        if (transaction == null)
            this.EntityId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
        else
            this.EntityId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
        return;
    }

    [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
    public void Insert(System.Int32? generalId, System.Int32? entityTypeId, System.Int32? centerId, System.Boolean? entityStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
    {
        Insert(generalId, entityTypeId, centerId, entityStatus, createdBy, createdDate, modifiedBy, modifiedDate, null);
    }
    /// <summary>
    /// Insert current GenEntities to database.
    /// </summary>
    /// <param name="transaction">optional SQL Transaction</param>
    public void Insert(DbTransaction transaction)
    {
        Insert(GeneralId, EntityTypeId, CenterId, EntityStatus, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, transaction);
    }

    /// <summary>
    /// Insert current GenEntities to database.
    /// </summary>
    public void Insert()
    {
        this.Insert((DbTransaction)null);
    }
    #endregion


    #region UPDATE
    public static void Update(System.Int32? entityId, System.Int32? generalId, System.Int32? entityTypeId, System.Int32? centerId, System.Boolean? entityStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
    {
        DataSet ds;
        Database db;
        string sqlCommand;
        DbCommand dbCommand;


        db = DatabaseFactory.CreateDatabase(DataConnection);
        sqlCommand = "[dbo].GenEntities_UPDATE";
        dbCommand = db.GetStoredProcCommand(sqlCommand);
        db.DiscoverParameters(dbCommand);
        dbCommand.Parameters["@entityId"].Value = entityId;
        dbCommand.Parameters["@generalId"].Value = generalId;
        dbCommand.Parameters["@entityTypeId"].Value = entityTypeId;
        dbCommand.Parameters["@centerId"].Value = centerId;
        dbCommand.Parameters["@entityStatus"].Value = entityStatus;
        dbCommand.Parameters["@createdBy"].Value = createdBy;
        dbCommand.Parameters["@createdDate"].Value = createdDate;
        dbCommand.Parameters["@modifiedBy"].Value = modifiedBy;
        dbCommand.Parameters["@modifiedDate"].Value = modifiedDate;

        if (transaction == null)
            db.ExecuteNonQuery(dbCommand);
        else
            db.ExecuteNonQuery(dbCommand, transaction);
        return;
    }

    [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
    public static void Update(System.Int32? entityId, System.Int32? generalId, System.Int32? entityTypeId, System.Int32? centerId, System.Boolean? entityStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
    {
        Update(entityId, generalId, entityTypeId, centerId, entityStatus, createdBy, createdDate, modifiedBy, modifiedDate, null);
    }

    public static void Update(GenEntities genEntities)
    {
        genEntities.Update();
    }

    public static void Update(GenEntities genEntities, DbTransaction transaction)
    {
        genEntities.Update(transaction);
    }

    /// <summary>
    /// Updates changes to the database.
    /// </summary>
    /// <param name="transaction">optional SQL Transaction</param>
    public void Update(DbTransaction transaction)
    {
        DataSet ds;
        Database db;
        string sqlCommand;
        DbCommand dbCommand;


        db = DatabaseFactory.CreateDatabase(DataConnection);
        sqlCommand = "[dbo].GenEntities_UPDATE";
        dbCommand = db.GetStoredProcCommand(sqlCommand);
        db.DiscoverParameters(dbCommand);
        dbCommand.Parameters["@entityId"].SourceColumn = "EntityId";
        dbCommand.Parameters["@generalId"].SourceColumn = "GeneralId";
        dbCommand.Parameters["@entityTypeId"].SourceColumn = "EntityTypeId";
        dbCommand.Parameters["@centerId"].SourceColumn = "CenterId";
        dbCommand.Parameters["@entityStatus"].SourceColumn = "EntityStatus";
        dbCommand.Parameters["@createdBy"].SourceColumn = "CreatedBy";
        dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
        dbCommand.Parameters["@modifiedBy"].SourceColumn = "ModifiedBy";
        dbCommand.Parameters["@modifiedDate"].SourceColumn = "ModifiedDate";

        ds = new DataSet();
        this.MapTo(ds);
        ds.AcceptChanges();
        ds.Tables[0].Rows[0].SetModified();
        if (transaction == null)
            db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
        else
            db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
        return;
    }

    /// <summary>
    /// Updates changes to the database.
    /// </summary>
    public void Update()
    {
        this.Update((DbTransaction)null);
    }
    #endregion


    #region DELETE
    [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
    public static void Delete(System.Int32? entityId, DbTransaction transaction)
    {
        Database db;
        string sqlCommand;
        DbCommand dbCommand;


        db = DatabaseFactory.CreateDatabase(DataConnection);
        sqlCommand = "[dbo].GenEntities_DELETE";
        dbCommand = db.GetStoredProcCommand(sqlCommand, entityId);

        // Execute.
        if (transaction != null)
        {
            db.ExecuteNonQuery(dbCommand, transaction);
        }
        else
        {
            db.ExecuteNonQuery(dbCommand);
        }
    }

    [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
    public static void Delete(System.Int32? entityId)
    {
        Delete(
        entityId);
    }

    /// <summary>
    /// Delete current GenEntities from database.
    /// </summary>
    /// <param name="transaction">optional SQL Transaction</param>
    public void Delete(DbTransaction transaction)
    {
        Database db;
        string sqlCommand;
        DbCommand dbCommand;


        db = DatabaseFactory.CreateDatabase(DataConnection);
        sqlCommand = "[dbo].GenEntities_DELETE";
        dbCommand = db.GetStoredProcCommand(sqlCommand, EntityId);

        // Execute.
        if (transaction != null)
        {
            db.ExecuteNonQuery(dbCommand, transaction);
        }
        else
        {
            db.ExecuteNonQuery(dbCommand);
        }
        this.EntityId = null;
    }

    /// <summary>
    /// Delete current GenEntities from database.
    /// </summary>
    public void Delete()
    {
        this.Delete((DbTransaction)null);
    }

    #endregion


    #region SEARCH
    [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
    public static GenEntities[] Search(System.Int32? entityId, System.Int32? generalId, System.Int32? entityTypeId, System.Int32? centerId, System.Boolean? entityStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
    {
        DataSet ds;
        Database db;
        string sqlCommand;
        DbCommand dbCommand;


        db = DatabaseFactory.CreateDatabase(DataConnection);
        sqlCommand = "[dbo].GenEntities_SEARCH";
        dbCommand = db.GetStoredProcCommand(sqlCommand, entityId, generalId, entityTypeId, centerId, entityStatus, createdBy, createdDate, modifiedBy, modifiedDate);

        ds = db.ExecuteDataSet(dbCommand);
        ds.Tables[0].TableName = TABLE_NAME;
        return GenEntities.MapFrom(ds);
    }


    public static GenEntities[] Search(GenEntities searchObject)
    {
        return Search(searchObject.EntityId, searchObject.GeneralId, searchObject.EntityTypeId, searchObject.CenterId, searchObject.EntityStatus, searchObject.CreatedBy, searchObject.CreatedDate, searchObject.ModifiedBy, searchObject.ModifiedDate);
    }

    /// <summary>
    /// Returns all GenEntities objects.
    /// </summary>
    /// <returns>List of all GenEntities objects. </returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
    public static GenEntities[] Search()
    {
        return Search(null, null, null, null, null, null, null, null, null);
    }

    #endregion


    #endregion


    #endregion


    #region Custom
    [Serializable]
    public class Entities
    {
        public int entityId { get; set; }
        public string entityName { get; set; }
        public bool entityStatus { get; set; }
    }

    public static IList<Entities> GetByGeneralId(int generalId, int centerId)
    {
        DataSet ds;
        string sqlCommand;
        DbCommand dbCommand;
        Entities instance;
        IList<Entities> llist = new List<Entities>();

        instance = new Entities();

        Database db = DatabaseFactory.CreateDatabase("DataConnection");
        sqlCommand = "GenEntities_GetByGeneralId";
        dbCommand = db.GetStoredProcCommand(sqlCommand, generalId, centerId);

        // Get results.
        ds = db.ExecuteDataSet(dbCommand);

        // Return results.
        ds.Tables[0].TableName = TABLE_NAME;

        foreach (DataRow item in ds.Tables[0].Rows)
        {
            instance = new Entities();
            instance.entityId = (int)item["EntityId"];
            instance.entityName = (string)item["EntityName"];
            instance.entityStatus = Convert.ToBoolean(item["EntityStatus"]);
            llist.Add(instance);
        }
        return llist;
    }

    ////no used
    public static void SetEntities(System.Int32 entityId, System.Int32 generalId, System.Int32 entityTypeId, System.Int32 centerId, System.Boolean entityStatus, System.Int32 createdBy, System.Int32 modifiedBy)
    {
        Database db;
        string sqlCommand;
        DbCommand dbCommand;

        db = DatabaseFactory.CreateDatabase("DataConnection");
        sqlCommand = "GenEntities_SetEntities";
        dbCommand = db.GetStoredProcCommand(sqlCommand, entityId, generalId, entityTypeId, centerId, entityStatus, createdBy, DateTime.Now, modifiedBy, DateTime.Now);

        // Get results.
        db.ExecuteScalar(dbCommand);
    }

    #endregion
}


}
