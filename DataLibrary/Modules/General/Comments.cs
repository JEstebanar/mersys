﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using General.GeneralCommons;

namespace General.Comments
{

    [DataObject]
    [Serializable]
    public partial class GenComments
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[gen_Comments]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _commentId;
        private System.Int32? _processId;
        private System.Int32? _processTypeId;
        private System.String _comment;
        private System.Int32? _generalId;
        private System.DateTime? _createdDate;

        #endregion


        #region Properties
        public System.Int32? CommentId
        {
            get
            {
                return _commentId;
            }
            set
            {
                _commentId = value;
            }
        }

        public System.Int32? ProcessId
        {
            get
            {
                return _processId;
            }
            set
            {
                _processId = value;
            }
        }

        public System.Int32? ProcessTypeId
        {
            get
            {
                return _processTypeId;
            }
            set
            {
                _processTypeId = value;
            }
        }

        public System.String Comment
        {
            get
            {
                return _comment;
            }
            set
            {
                _comment = value;
            }
        }

        public System.Int32? GeneralId
        {
            get
            {
                return _generalId;
            }
            set
            {
                _generalId = value;
            }
        }

        public System.DateTime? CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("CommentId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ProcessId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ProcessTypeId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("Comment", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("GeneralId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (CommentId == null)
                dr["CommentId"] = DBNull.Value;
            else
                dr["CommentId"] = CommentId;

            if (ProcessId == null)
                dr["ProcessId"] = DBNull.Value;
            else
                dr["ProcessId"] = ProcessId;

            if (ProcessTypeId == null)
                dr["ProcessTypeId"] = DBNull.Value;
            else
                dr["ProcessTypeId"] = ProcessTypeId;

            if (Comment == null)
                dr["Comment"] = DBNull.Value;
            else
                dr["Comment"] = Comment;

            if (GeneralId == null)
                dr["GeneralId"] = DBNull.Value;
            else
                dr["GeneralId"] = GeneralId;

            if (CreatedDate == null)
                dr["CreatedDate"] = DBNull.Value;
            else
                dr["CreatedDate"] = CreatedDate;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            CommentId = dr["CommentId"] != DBNull.Value ? Convert.ToInt32(dr["CommentId"]) : CommentId = null;
            ProcessId = dr["ProcessId"] != DBNull.Value ? Convert.ToInt32(dr["ProcessId"]) : ProcessId = null;
            ProcessTypeId = dr["ProcessTypeId"] != DBNull.Value ? Convert.ToInt32(dr["ProcessTypeId"]) : ProcessTypeId = null;
            Comment = dr["Comment"] != DBNull.Value ? Convert.ToString(dr["Comment"]) : Comment = null;
            GeneralId = dr["GeneralId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralId"]) : GeneralId = null;
            CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
        }

        public static GenComments[] MapFrom(DataSet ds)
        {
            List<GenComments> objects;


            // Initialise Collection.
            objects = new List<GenComments>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[gen_Comments] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[gen_Comments] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                GenComments instance = new GenComments();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenComments Get(System.Int32 commentId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            GenComments instance;


            instance = new GenComments();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenComments_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, commentId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenComments ID:" + commentId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.Int32? processId, System.Int32? processTypeId, System.String comment, System.Int32? generalId, System.DateTime? createdDate, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenComments_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, processId, processTypeId, comment, generalId, createdDate);

            if (transaction == null)
                this.CommentId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.CommentId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.Int32? processId, System.Int32? processTypeId, System.String comment, System.Int32? generalId, System.DateTime? createdDate)
        {
            Insert(processId, processTypeId, comment, generalId, createdDate, null);
        }
        /// <summary>
        /// Insert current GenComments to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(ProcessId, ProcessTypeId, Comment, GeneralId, CreatedDate, transaction);
        }

        /// <summary>
        /// Insert current GenComments to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? commentId, System.Int32? processId, System.Int32? processTypeId, System.String comment, System.Int32? generalId, System.DateTime? createdDate, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenComments_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@commentId"].Value = commentId;
            dbCommand.Parameters["@processId"].Value = processId;
            dbCommand.Parameters["@processTypeId"].Value = processTypeId;
            dbCommand.Parameters["@comment"].Value = comment;
            dbCommand.Parameters["@generalId"].Value = generalId;
            dbCommand.Parameters["@createdDate"].Value = createdDate;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? commentId, System.Int32? processId, System.Int32? processTypeId, System.String comment, System.Int32? generalId, System.DateTime? createdDate)
        {
            Update(commentId, processId, processTypeId, comment, generalId, createdDate, null);
        }

        public static void Update(GenComments genComments)
        {
            genComments.Update();
        }

        public static void Update(GenComments genComments, DbTransaction transaction)
        {
            genComments.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenComments_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@commentId"].SourceColumn = "CommentId";
            dbCommand.Parameters["@processId"].SourceColumn = "ProcessId";
            dbCommand.Parameters["@processTypeId"].SourceColumn = "ProcessTypeId";
            dbCommand.Parameters["@comment"].SourceColumn = "Comment";
            dbCommand.Parameters["@generalId"].SourceColumn = "GeneralId";
            dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? commentId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenComments_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, commentId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? commentId)
        {
            Delete(
            commentId);
        }

        /// <summary>
        /// Delete current GenComments from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenComments_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, CommentId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.CommentId = null;
        }

        /// <summary>
        /// Delete current GenComments from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static GenComments[] Search(System.Int32? commentId, System.Int32? processId, System.Int32? processTypeId, System.String comment, System.Int32? generalId, System.DateTime? createdDate)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GenComments_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, commentId, processId, processTypeId, comment, generalId, createdDate);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return GenComments.MapFrom(ds);
        }


        public static GenComments[] Search(GenComments searchObject)
        {
            return Search(searchObject.CommentId, searchObject.ProcessId, searchObject.ProcessTypeId, searchObject.Comment, searchObject.GeneralId, searchObject.CreatedDate);
        }

        /// <summary>
        /// Returns all GenComments objects.
        /// </summary>
        /// <returns>List of all GenComments objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static GenComments[] Search()
        {
            return Search(null, null, null, null, null, null);
        }

        #endregion


        #endregion


        #endregion

        #region Custom

        [Serializable]
        public class Comments
        {
            public string comment { get; set; }
            public string fullName { get; set; }
            public string createdDate { get; set; }
        }

        public static IList<Comments> GetByProcessId(int processId, GeneralCommon.CommentProcessType processTypeId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            Comments instance;
            IList<Comments> llist = new List<Comments>();

            instance = new Comments();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "GenComments_GetByProcessId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, processId, (int)processTypeId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new Comments();
                instance.comment = (string)item["Comment"];
                instance.fullName = (string)item["fullName"];
                instance.createdDate = (string)item["CreatedDate"];
                llist.Add(instance);
            }
            return llist;
        }
        #endregion

    }


}