﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using General.GeneralCommons;
using SystemSecurity.CentersInfo;
using SystemSecurity.SysUsers;
using Residences.Residents;

namespace General.EMails
{
    public class GenEMails
    {
        #region Fields
        private System.String _sentTo;
        private System.String _personName;
        private System.String _confirmation;
        private GeneralCommon.MailType _mailType;

        private System.String _sentFrom;
        private System.String _password;

        private System.Int32 _centerId;
        private System.String _centerName;
        private System.String _centerPhone;

        private System.String _letterName;

        private System.String _aditionalComment;

        private System.Int32 _residenceId;
        private System.Int32 _gradeId;
        private System.Int32 _teachingYearId;
        private System.String _title;
        private System.String _body;
        #endregion

        #region Properties
        private System.String SentTo
        {
            get
            {
                return _sentTo;
            }
            set
            {
                _sentTo = value;
            }
        }

        private System.String PersonName
        {
            get { return _personName; }
            set { _personName = value; }
        }

        private System.String Confirmation
        {
            get { return _confirmation; }
            set { _confirmation = value; }
        }

        private GeneralCommon.MailType MailType
        {
            get { return _mailType; }
            set { _mailType = value; }
        }

        protected System.String SentFrom
        {
            get { return _sentFrom; }
            set { _sentFrom = value; }
        }

        protected System.String Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private System.Int32 CenterId
        {
            get { return _centerId; }
            set { _centerId = value; }
        }

        private System.String CenterName
        {
            get { return _centerName; }
            set { _centerName = value; }
        }

        private System.String CenterPhone
        {
            get { return _centerPhone; }
            set { _centerPhone = value; }
        }

        private System.String LetterName
        {

            get { return _letterName; }
            set { _letterName = value; }
        }

        private System.String AditionalComment
        {
            get { return _aditionalComment; }
            set { _aditionalComment = value; }
        }
        
        private System.Int32 ResidenceId
        {
            get { return _residenceId; }
            set { _residenceId = value; }
        }

        private System.Int32 GradeId
        {
            get { return _gradeId; }
            set { _gradeId = value; }
        }

        private System.Int32 TeachingYearId
        {
            get { return _teachingYearId; }
            set { _teachingYearId = value; }
        }

        private System.String Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private System.String Body
        {
            get { return _body; }
            set { _body = value; }
        }
        #endregion

        //private string SentTo { get; set; }
        //private string Confirmation { get; set; }

        private static string GetMail(GeneralCommon.MailType type)
        {
            return "noreply@residenciasmedicasrd.com";
            //return "jesteban.ar@gmail.com";
        }

        private static string GetPass(GeneralCommon.MailType type)
        {
            return "H1mselfjalisc@";
            //return "H1mselfjalisc@gm";
        }

        private string GetSubject(GeneralCommon.MailType type)
        {
            string subject = string.Empty;
            switch (type)
            {
                case GeneralCommon.MailType.SignUp:
                    subject = "Mensaje de Confirmación";
                    break;
                case GeneralCommon.MailType.LetterRequestCompleted:
                    subject = "Solicitud de Carta Lista";
                    break;
                case GeneralCommon.MailType.LetterRequestCanceled:
                    subject = "Solicitud de Carta Cancelada";
                    break;
                case GeneralCommon.MailType.Comunication:
                    subject = this.Title;
                    break;
                case GeneralCommon.MailType.Contact:
                    subject = "Interes en MeSys";
                    break;
                case GeneralCommon.MailType.RecoverPassword:
                    subject = "Solicitaste una nueva contraseña";
                    break;
                default:
                    subject= "Docencia Médica";
                    break;
            }
            return subject;
        }

        private string GetBody(GeneralCommon.MailType type)
        {
            string body = string.Empty;
            var path = string.Empty;
            switch (type)
            {
                case GeneralCommon.MailType.SignUp:
                    path = System.Web.HttpContext.Current.Server.MapPath("~/Files/Templates/EMails/SignUp.html");
                    using (StreamReader reader = new StreamReader(path))
                    {
                        body = reader.ReadToEnd();
                    }
                    //body = body.Replace("{Title}", title);
                    body = body.Replace("{Url}", GetUrl(this.MailType) + "?conf=" + this.Confirmation);
                    break;
                case GeneralCommon.MailType.LetterRequestCompleted:
                    path = System.Web.HttpContext.Current.Server.MapPath("~/Files/Templates/EMails/LetterRequestCompleted.html");
                    using (StreamReader reader = new StreamReader(path))
                    {
                        body = reader.ReadToEnd();
                    }
                    body = body.Replace("{LetterName}", this.LetterName);
                    break;
                case GeneralCommon.MailType.LetterRequestCanceled:
                    path = System.Web.HttpContext.Current.Server.MapPath("~/Files/Templates/EMails/LetterRequestCanceled.html");
                    using (StreamReader reader = new StreamReader(path))
                    {
                        body = reader.ReadToEnd();
                    }
                    body = body.Replace("{LetterName}", this.LetterName);
                    body = body.Replace("{Comment}", this.AditionalComment);
                    break;
                case GeneralCommon.MailType.Comunication:
                    body = this.Body;
                    break;
                case GeneralCommon.MailType.Contact:
                    path = System.Web.HttpContext.Current.Server.MapPath("~/Files/Templates/EMails/Contact.html");
                    using (StreamReader reader = new StreamReader(path))
                    {
                        body = reader.ReadToEnd();
                    }
                    body = body.Replace("{Comment}", this.AditionalComment);
                    break;
                case GeneralCommon.MailType.RecoverPassword:
                    path = System.Web.HttpContext.Current.Server.MapPath("~/Files/Templates/EMails/RecoverPassword.html");
                    using (StreamReader reader = new StreamReader(path))
                    {
                        body = reader.ReadToEnd();
                    }
                    body = body.Replace("{Url}", GetUrl(this.MailType) + "?conf=" + this.Confirmation);
                    break;
                default:
                    body = "Docencia Médica";
                    break;
            }

            body = body.Replace("{PersonName}", this.PersonName);
            body = body.Replace("{Center}", this.CenterName);
            body = body.Replace("{CenterPhone}", this.CenterPhone);
            return body;
            //return "Texto del contenio del mensaje de correo";
        }

        private string GetUrl(GeneralCommon.MailType type)
        {
            string url = string.Empty;
            switch (type)
            {
                case GeneralCommon.MailType.SignUp:
                    url = "http://residenciasmedicasrd.com/Account/ConfirmAccount.aspx";
                    break;
                case GeneralCommon.MailType.RecoverPassword:
                    url = "http://residenciasmedicasrd.com/Account/Recover.aspx";
                    break;
            }
            return url;
        }

        private void Sent()
        {
            //string fromMail = GetMail(mailType);
            //string fromPass = GetPass(mailType);
            //SentTo = GetMail(this.MailType);
            SentFrom = GetMail(this.MailType);
            Password = GetPass(this.MailType);


            /*-------------------------MENSAJE DE CORREO----------------------*/

            //new message object
            System.Net.Mail.MailMessage mmsg = new System.Net.Mail.MailMessage();

            //to
            switch (this.MailType)
            {
                case GeneralCommon.MailType.SignUp:
                    mmsg.To.Add(this.SentTo);
                    break;
                case GeneralCommon.MailType.LetterRequestCompleted:
                    mmsg.To.Add(this.SentTo);
                    break;
                case GeneralCommon.MailType.LetterRequestCanceled:
                    mmsg.To.Add(this.SentTo);
                    break;
                case GeneralCommon.MailType.Comunication:
                    IList<ResResidents.ResidentEMails> cEMail = new List<ResResidents.ResidentEMails>();
                    cEMail = ResResidents.GetEMails(this.CenterId, this.ResidenceId, this.GradeId, this.TeachingYearId);
                    foreach (ResResidents.ResidentEMails emailsInList in cEMail)
                    {
                        mmsg.Bcc.Add(emailsInList.userName);
                        //mmsg.CC.Add(emailsInList.userName);
                        //mmsg.To.Add("xpboy1@hotmail.com");
                        //    mmsg.Bcc.Add("xpboy1@hotmail.com");
                        //    mmsg.Bcc.Add("xpboy1@hotmail.com");
                    }
                    break;
                case GeneralCommon.MailType.Contact:
                    mmsg.To.Add(this.SentTo);
                    break;
                case GeneralCommon.MailType.RecoverPassword:
                    mmsg.To.Add(this.SentTo);
                    break;
                default:
                    mmsg.To.Add(GetMail(GeneralCommon.MailType.SignUp));
                    break;
            }
            
            //sudject
            mmsg.Subject = GetSubject(this.MailType);
            mmsg.SubjectEncoding = System.Text.Encoding.UTF8;

            //bcc and cc
            //mmsg.Bcc.Add("name@mail.com"); //Opcional

            //body
            mmsg.Body = GetBody(this.MailType);
            mmsg.BodyEncoding = System.Text.Encoding.UTF8;
            mmsg.IsBodyHtml = true; ; //sent like html

            //from
            mmsg.From = new System.Net.Mail.MailAddress(this.SentFrom, "Residencias Médicas");

            //mail cleint
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();

            //mail from
            client.Credentials = new System.Net.NetworkCredential(this.SentFrom, this.Password);

            //just for gmail
            //cliente.Port = 587;
            //cliente.EnableSsl = true;
            //cliente.Host = "smtp.gmail.com"; //Para Gmail "smtp.gmail.com";

            client.Port = 26;
            client.Host = "smtp.residenciasmedicasrd.com";
            
            //sent e-mil
            try
            {
                //sent e-mail     
                client.Send(mmsg);
            }
            catch (System.Net.Mail.SmtpException ex)
            {
                //errors
            }
        }

        public static void SignUp(string sentTo, string personName, string confirmationCode, int centerId)
        {
            SysCentersInfo cCenter = SysCentersInfo.Get(centerId);
            GenEMails nMail = new GenEMails();
            nMail.SentTo = sentTo;
            nMail.PersonName = personName;
            nMail.Confirmation = confirmationCode;
            nMail.MailType = GeneralCommon.MailType.SignUp;
            nMail.CenterName = cCenter.CenterName;
            nMail.CenterPhone = cCenter.PhoneNumber;
            nMail.Sent();
        }

        public static void ReSendConfirmation(string sentTo, string personName, string confirmationCode, int centerId)
        {
            SysCentersInfo cCenter = SysCentersInfo.Get(centerId);
            GenEMails nMail = new GenEMails();
            nMail.SentTo = sentTo;
            nMail.PersonName = personName;
            nMail.Confirmation = confirmationCode;
            nMail.MailType = GeneralCommon.MailType.SignUp; //it is the same for resend confirmation
            nMail.CenterName = cCenter.CenterName;
            nMail.CenterPhone = cCenter.PhoneNumber;
            nMail.Sent();
        }

        public static void RecoverPassword(string sentTo, string personName, string confirmationCode)
        {
            GenEMails nMail = new GenEMails();
            nMail.SentTo = sentTo;
            nMail.PersonName = personName;
            nMail.Confirmation = confirmationCode;
            nMail.MailType = GeneralCommon.MailType.RecoverPassword; 
            nMail.Sent();
        }

        public static void LetterRequestStatus(int sentToGeneralId, string personName, GeneralCommon.MailType mailType, string LetterName, string comment, int centerId)
        {
            SysCentersInfo cCenter = SysCentersInfo.Get(centerId);
            GenEMails nMail = new GenEMails();
            nMail.SentTo = SysUsers.GetUserNameByGeneralId(sentToGeneralId);
            nMail.PersonName = personName;
            nMail.LetterName = LetterName;
            nMail.MailType = mailType;
            nMail.AditionalComment = comment;
            nMail.CenterName = cCenter.CenterName;
            nMail.CenterPhone = cCenter.PhoneNumber;
            nMail.Sent();
        }

        public static void Comunication(int centerId, int residenceId, int gradeId, string title, string body, int teachingYearId)
        {
            SysCentersInfo cCenter = SysCentersInfo.Get(centerId);
            GenEMails nMail = new GenEMails();
            nMail.CenterId = centerId;
            nMail.ResidenceId = residenceId;
            nMail.GradeId = gradeId;
            nMail.TeachingYearId = teachingYearId;
            nMail.MailType = GeneralCommon.MailType.Comunication;
            nMail.Title = title;
            nMail.Body = body;
            nMail.CenterName = cCenter.CenterName;
            nMail.CenterPhone = cCenter.PhoneNumber;
            nMail.Sent();
        }

        public static void Contact(string name, string center, string phone, string eMail, string comment)
        {
            GenEMails nMail = new GenEMails();
            nMail.MailType = GeneralCommon.MailType.Contact;
            nMail.PersonName = name;
            nMail.CenterName = center;
            nMail.CenterPhone = phone;
            nMail.AditionalComment = eMail + "<br />" + comment;
            nMail.SentTo = "jesteban.ar@gmail.com";
            nMail.Sent();
        }
    }
}