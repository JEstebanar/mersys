﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Modules.Indicators.Indicators
{

    [DataObject]
    [Serializable]
    public partial class IndIndicators
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[res_Departments]";
        private static readonly string DataConnection = "DataConnection";

        #endregion

        #region Custom

        [Serializable]
        public class Residences
        {
            //public int gradeId { get; set; }
            public int residenceId { get; set; }
            public string residenceName { get; set; }
            public decimal average { get; set; }
            public string progress { get; set; }
            public decimal progressPercent { get; set; }
            public int position { get; set; }

            public decimal r1 { get; set; }
            public decimal r2 { get; set; }
            public decimal r3 { get; set; }
            public decimal r4 { get; set; }
            public decimal r5 { get; set; }
            public decimal r6 { get; set; }
            public decimal r7 { get; set; }

            public decimal july { get; set; }
            public decimal august { get; set; }
            public decimal september { get; set; }
            public decimal october { get; set; }
        }

        public static IList<Residences> GetResidenciesAverages()
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            Residences instance;
            IList<Residences> llist = new List<Residences>();

            instance = new Residences();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "EvaResidencies_Averages";
            dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new Residences();
                //instance.gradeId = (int)item["GradeId"];
                instance.residenceId = (int)item["ResidenceId"];
                instance.residenceName = (string)item["ResidenceName"];
                instance.average = (decimal)item["Average"];
                instance.progress = (string)item["Progress"];
                instance.progressPercent = (decimal)item["ProgressPercent"];
                instance.position = Convert.ToInt32(item["Position"]);
                llist.Add(instance);
            }
            return llist;
        }

        public static int NumberOfResidents(System.Int32 centerId, System.Int32 residenceId)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResResidents_NumberOfResidentsByCenterId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, centerId, residenceId);

            return Convert.ToInt32(db.ExecuteScalar(dbCommand));
        }

        //public static IList<Grades> GetResidenciesAverages()
        //{
        //    DataSet ds;
        //    string sqlCommand;
        //    DbCommand dbCommand;
        //    Grades instance;
        //    IList<Grades> llist = new List<Grades>();

        //    instance = new Grades();

        //    Database db = DatabaseFactory.CreateDatabase("DataConnection");
        //    sqlCommand = "EvaResidencies_Averages";
        //    dbCommand = db.GetStoredProcCommand(sqlCommand);

        //    // Get results.
        //    ds = db.ExecuteDataSet(dbCommand);

        //    // Return results.
        //    ds.Tables[0].TableName = TABLE_NAME;

        //    foreach (DataRow item in ds.Tables[0].Rows)
        //    {
        //        instance = new Grades();
        //        //instance.gradeId = (int)item["GradeId"];
        //        instance.residenceName = (string)item["ResidenceName"];
        //        instance.average = (decimal)item["Average"];
        //        llist.Add(instance);
        //    }
        //    return llist;
        //}

        //public static IList<Grades> GetResidencieGradesAverages()
        //{
        //    DataSet ds;
        //    string sqlCommand;
        //    DbCommand dbCommand;
        //    Grades instance;
        //    IList<Grades> llist = new List<Grades>();

        //    instance = new Grades();

        //    Database db = DatabaseFactory.CreateDatabase("DataConnection");
        //    sqlCommand = "EvaResidencieGradesAverage";
        //    dbCommand = db.GetStoredProcCommand(sqlCommand);

        //    // Get results.
        //    ds = db.ExecuteDataSet(dbCommand);

        //    // Return results.
        //    ds.Tables[0].TableName = TABLE_NAME;

        //    foreach (DataRow item in ds.Tables[0].Rows)
        //    {
        //        instance = new Grades();
        //        //instance.gradeId = (int)item["GradeId"];
        //        instance.residenceName = (string)item["ResidenceName"];
        //        instance.r1 = (decimal)item["R1"];
        //        instance.r2 = (decimal)item["R2"];
        //        instance.r3 = (decimal)item["R3"];
        //        instance.r4 = (decimal)item["R4"];
        //        instance.r5 = (decimal)item["R5"];
        //        instance.r6 = (decimal)item["R6"];
        //        instance.r7 = (decimal)item["R7"];
        //        llist.Add(instance);
        //    }
        //    return llist;
        //}

        //public static IList<Grades> GetResidentMonth(int generalId)
        //{
        //    DataSet ds;
        //    string sqlCommand;
        //    DbCommand dbCommand;
        //    Grades instance;
        //    IList<Grades> llist = new List<Grades>();

        //    instance = new Grades();

        //    Database db = DatabaseFactory.CreateDatabase("DataConnection");
        //    sqlCommand = "EvaResidentScoresByGeneralId";
        //    dbCommand = db.GetStoredProcCommand(sqlCommand, generalId);

        //    // Get results.
        //    ds = db.ExecuteDataSet(dbCommand);

        //    // Return results.
        //    ds.Tables[0].TableName = TABLE_NAME;

        //    foreach (DataRow item in ds.Tables[0].Rows)
        //    {
        //        instance = new Grades();
        //        //instance.gradeId = (int)item["GradeId"];
        //        instance.residenceName = (string)item["ResidenceName"];
        //        instance.july =  Convert.ToDecimal(item["July"]);
        //        instance.august = Convert.ToDecimal(item["August"]);
        //        instance.september = Convert.ToDecimal(item["September"]);
        //        instance.october = Convert.ToDecimal(item["October"]);
        //        llist.Add(instance);
        //    }
        //    return llist;
        //}
        #endregion
    }
}
