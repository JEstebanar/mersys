﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Modules.Residences.RotationTypes
{
    [DataObject]
    [Serializable]
    public partial class ResRotationTypes
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[res_RotationTypes]";
        private static readonly string DataConnection = "DataConnection";

        #endregion

        #region Custom

        [Serializable]
        public class RotationTypes
        {
            public int typeId { get; set; }
            public string typeName { get; set; }
        }

        public static IList<RotationTypes> GetRotationTypes()
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            RotationTypes instance;
            IList<RotationTypes> llist = new List<RotationTypes>();

            instance = new RotationTypes();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "ResRotationTypes_Get";
            dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new RotationTypes();
                instance.typeId = (int)item["TypeId"];
                instance.typeName = (string)item["TypeName"];
                llist.Add(instance);
            }
            return llist;
        }
        #endregion
    }
}
