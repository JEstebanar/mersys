﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Modules.Residences.Rotations
{

    [DataObject]
    [Serializable]
    public partial class ResRotations
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[res_Rotations]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _rotationId;
        private System.Int32? _residentId;
        private System.Int32? _departmentId;
        private System.DateTime? _startDate;
        private System.DateTime? _endDate;
        private System.Int32? _rotationTypeId;
        private System.Int32? _centerId;
        private System.String _note;
        private System.Int32? _createdBy;
        private System.DateTime? _createdDate;
        private System.Int32? _modifiedBy;
        private System.DateTime? _modifiedDate;

        #endregion


        #region Properties
        public System.Int32? RotationId
        {
            get
            {
                return _rotationId;
            }
            set
            {
                _rotationId = value;
            }
        }

        public System.Int32? ResidentId
        {
            get
            {
                return _residentId;
            }
            set
            {
                _residentId = value;
            }
        }

        public System.Int32? DepartmentId
        {
            get
            {
                return _departmentId;
            }
            set
            {
                _departmentId = value;
            }
        }

        public System.DateTime? StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value;
            }
        }

        public System.DateTime? EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                _endDate = value;
            }
        }

        public System.Int32? RotationTypeId
        {
            get
            {
                return _rotationTypeId;
            }
            set
            {
                _rotationTypeId = value;
            }
        }

        public System.Int32? CenterId
        {
            get
            {
                return _centerId;
            }
            set
            {
                _centerId = value;
            }
        }

        public System.String Note
        {
            get
            {
                return _note;
            }
            set
            {
                _note = value;
            }
        }

        public System.Int32? CreatedBy
        {
            get
            {
                return _createdBy;
            }
            set
            {
                _createdBy = value;
            }
        }

        public System.DateTime? CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        public System.Int32? ModifiedBy
        {
            get
            {
                return _modifiedBy;
            }
            set
            {
                _modifiedBy = value;
            }
        }

        public System.DateTime? ModifiedDate
        {
            get
            {
                return _modifiedDate;
            }
            set
            {
                _modifiedDate = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("RotationId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ResidentId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("DepartmentId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("StartDate", typeof(System.DateTime));
            ds.Tables[TABLE_NAME].Columns.Add("EndDate", typeof(System.DateTime));
            ds.Tables[TABLE_NAME].Columns.Add("RotationTypeId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("CenterId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("Note", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("CreatedBy", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime));
            ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (RotationId == null)
                dr["RotationId"] = DBNull.Value;
            else
                dr["RotationId"] = RotationId;

            if (ResidentId == null)
                dr["ResidentId"] = DBNull.Value;
            else
                dr["ResidentId"] = ResidentId;

            if (DepartmentId == null)
                dr["DepartmentId"] = DBNull.Value;
            else
                dr["DepartmentId"] = DepartmentId;

            if (StartDate == null)
                dr["StartDate"] = DBNull.Value;
            else
                dr["StartDate"] = StartDate;

            if (EndDate == null)
                dr["EndDate"] = DBNull.Value;
            else
                dr["EndDate"] = EndDate;

            if (RotationTypeId == null)
                dr["RotationTypeId"] = DBNull.Value;
            else
                dr["RotationTypeId"] = RotationTypeId;

            if (CenterId == null)
                dr["CenterId"] = DBNull.Value;
            else
                dr["CenterId"] = CenterId;

            if (Note == null)
                dr["Note"] = DBNull.Value;
            else
                dr["Note"] = Note;

            if (CreatedBy == null)
                dr["CreatedBy"] = DBNull.Value;
            else
                dr["CreatedBy"] = CreatedBy;

            if (CreatedDate == null)
                dr["CreatedDate"] = DBNull.Value;
            else
                dr["CreatedDate"] = CreatedDate;

            if (ModifiedBy == null)
                dr["ModifiedBy"] = DBNull.Value;
            else
                dr["ModifiedBy"] = ModifiedBy;

            if (ModifiedDate == null)
                dr["ModifiedDate"] = DBNull.Value;
            else
                dr["ModifiedDate"] = ModifiedDate;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            RotationId = dr["RotationId"] != DBNull.Value ? Convert.ToInt32(dr["RotationId"]) : RotationId = null;
            ResidentId = dr["ResidentId"] != DBNull.Value ? Convert.ToInt32(dr["ResidentId"]) : ResidentId = null;
            DepartmentId = dr["DepartmentId"] != DBNull.Value ? Convert.ToInt32(dr["DepartmentId"]) : DepartmentId = null;
            StartDate = dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : StartDate = null;
            EndDate = dr["EndDate"] != DBNull.Value ? Convert.ToDateTime(dr["EndDate"]) : EndDate = null;
            RotationTypeId = dr["RotationTypeId"] != DBNull.Value ? Convert.ToInt32(dr["RotationTypeId"]) : RotationTypeId = null;
            CenterId = dr["CenterId"] != DBNull.Value ? Convert.ToInt32(dr["CenterId"]) : CenterId = null;
            Note = dr["Note"] != DBNull.Value ? Convert.ToString(dr["Note"]) : Note = null;
            CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : CreatedBy = null;
            CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
            ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
            ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
        }

        public static ResRotations[] MapFrom(DataSet ds)
        {
            List<ResRotations> objects;


            // Initialise Collection.
            objects = new List<ResRotations>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[res_Rotations] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[res_Rotations] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                ResRotations instance = new ResRotations();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static ResRotations Get(System.Int32 rotationId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            ResRotations instance;


            instance = new ResRotations();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResRotations_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, rotationId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get ResRotations ID:" + rotationId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.Int32? residentId, System.Int32? departmentId, System.DateTime? startDate, System.DateTime? endDate, System.Int32? rotationTypeId, System.Int32? centerId, System.String note, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResRotations_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, residentId, departmentId, startDate, endDate, rotationTypeId, centerId, note, createdBy, createdDate, modifiedBy, modifiedDate);

            if (transaction == null)
                this.RotationId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.RotationId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.Int32? residentId, System.Int32? departmentId, System.DateTime? startDate, System.DateTime? endDate, System.Int32? rotationTypeId, System.Int32? centerId, System.String note, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
        {
            Insert(residentId, departmentId, startDate, endDate, rotationTypeId, centerId, note, createdBy, createdDate, modifiedBy, modifiedDate, null);
        }
        /// <summary>
        /// Insert current ResRotations to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(ResidentId, DepartmentId, StartDate, EndDate, RotationTypeId, CenterId, Note, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, transaction);
        }

        /// <summary>
        /// Insert current ResRotations to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? rotationId, System.Int32? residentId, System.Int32? departmentId, System.DateTime? startDate, System.DateTime? endDate, System.Int32? rotationTypeId, System.Int32? centerId, System.String note, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResRotations_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@rotationId"].Value = rotationId;
            dbCommand.Parameters["@residentId"].Value = residentId;
            dbCommand.Parameters["@departmentId"].Value = departmentId;
            dbCommand.Parameters["@startDate"].Value = startDate;
            dbCommand.Parameters["@endDate"].Value = endDate;
            dbCommand.Parameters["@rotationTypeId"].Value = rotationTypeId;
            dbCommand.Parameters["@centerId"].Value = centerId;
            dbCommand.Parameters["@note"].Value = note;
            dbCommand.Parameters["@createdBy"].Value = createdBy;
            dbCommand.Parameters["@createdDate"].Value = createdDate;
            dbCommand.Parameters["@modifiedBy"].Value = modifiedBy;
            dbCommand.Parameters["@modifiedDate"].Value = modifiedDate;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? rotationId, System.Int32? residentId, System.Int32? departmentId, System.DateTime? startDate, System.DateTime? endDate, System.Int32? rotationTypeId, System.Int32? centerId, System.String note, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
        {
            Update(rotationId, residentId, departmentId, startDate, endDate, rotationTypeId, centerId, note, createdBy, createdDate, modifiedBy, modifiedDate, null);
        }

        public static void Update(ResRotations resRotations)
        {
            resRotations.Update();
        }

        public static void Update(ResRotations resRotations, DbTransaction transaction)
        {
            resRotations.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResRotations_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@rotationId"].SourceColumn = "RotationId";
            dbCommand.Parameters["@residentId"].SourceColumn = "ResidentId";
            dbCommand.Parameters["@departmentId"].SourceColumn = "DepartmentId";
            dbCommand.Parameters["@startDate"].SourceColumn = "StartDate";
            dbCommand.Parameters["@endDate"].SourceColumn = "EndDate";
            dbCommand.Parameters["@rotationTypeId"].SourceColumn = "RotationTypeId";
            dbCommand.Parameters["@centerId"].SourceColumn = "CenterId";
            dbCommand.Parameters["@note"].SourceColumn = "Note";
            dbCommand.Parameters["@createdBy"].SourceColumn = "CreatedBy";
            dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
            dbCommand.Parameters["@modifiedBy"].SourceColumn = "ModifiedBy";
            dbCommand.Parameters["@modifiedDate"].SourceColumn = "ModifiedDate";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? rotationId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResRotations_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, rotationId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? rotationId)
        {
            Delete(
            rotationId);
        }

        /// <summary>
        /// Delete current ResRotations from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResRotations_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, RotationId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.RotationId = null;
        }

        /// <summary>
        /// Delete current ResRotations from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static ResRotations[] Search(System.Int32? rotationId, System.Int32? residentId, System.Int32? departmentId, System.DateTime? startDate, System.DateTime? endDate, System.Int32? rotationTypeId, System.Int32? centerId, System.String note, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResRotations_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, rotationId, residentId, departmentId, startDate, endDate, rotationTypeId, centerId, note, createdBy, createdDate, modifiedBy, modifiedDate);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return ResRotations.MapFrom(ds);
        }


        public static ResRotations[] Search(ResRotations searchObject)
        {
            return Search(searchObject.RotationId, searchObject.ResidentId, searchObject.DepartmentId, searchObject.StartDate, searchObject.EndDate, searchObject.RotationTypeId, searchObject.CenterId, searchObject.Note, searchObject.CreatedBy, searchObject.CreatedDate, searchObject.ModifiedBy, searchObject.ModifiedDate);
        }

        /// <summary>
        /// Returns all ResRotations objects.
        /// </summary>
        /// <returns>List of all ResRotations objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static ResRotations[] Search()
        {
            return Search(null, null, null, null, null, null, null, null, null, null, null, null);
        }

        #endregion


        #endregion


        #endregion
	
	
        #region Custom

        [Serializable]
        public class Rotations
        {
            public int rotationId { get; set; }
            public int departmentId { get; set; }
            public string departmentName { get; set; }
            public DateTime startDate { get; set; }
            public string showStartDate { get; set; }
            public DateTime endDate { get; set; }
            public string showEndDate { get; set; }
            public int rotationTypeId { get; set; }
            public string rotationType { get; set; }
            public string rotationNote { get; set; }
            public int centerId { get; set; }
        }

        public static IList<Rotations> GetByResidentId(int residentId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            Rotations instance;
            IList<Rotations> llist = new List<Rotations>();

            instance = new Rotations();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "ResRotations_GetByResidentId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, residentId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new Rotations();
                instance.rotationId = (int)item["RotationId"];
                instance.departmentId = (int)item["DepartmentId"];
                instance.departmentName = (string)item["DepartmentName"];
                instance.startDate = (DateTime)item["StartDate"];
                instance.showStartDate = (string)item["ShowStartDate"];
                instance.endDate = (DateTime)item["EndDate"];
                instance.showEndDate = (string)item["showEndDate"];
                instance.rotationTypeId = (int)item["RotationTypeId"];
                instance.rotationType = (string)item["RotationType"];
                instance.rotationNote = (string)item["Note"];
                instance.centerId = (int)item["CenterId"];
                llist.Add(instance);
            }
            return llist;
        }

        //public static void DeleteByGeneralId(int generalId)
        //{
        //    Database db;
        //    string sqlCommand;
        //    DbCommand dbCommand;

        //    db = DatabaseFactory.CreateDatabase(DataConnection);
        //    sqlCommand = "[dbo].GenGeneralPhones_DELETE";
        //    dbCommand = db.GetStoredProcCommand(sqlCommand, generalId);

        //    // Execute.
        //    db.ExecuteNonQuery(dbCommand);
        //}

        #endregion


    }
}
