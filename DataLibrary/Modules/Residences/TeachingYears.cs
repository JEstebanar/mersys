﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Residences.TeachingYears
{


    [DataObject]
    [Serializable]
    public partial class ResTeachingYears
    {
        
        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[res_TeachingYears]";
        private static readonly string DataConnection = "DataConnection";

        #endregion

        #region Custom

        [Serializable]
        public class TeachingYears
        {
            public int teachingYearId { get; set; }
            public string teachingYear { get; set; }
        }

        public static IList<TeachingYears> GetTeachingYears(System.Int32 centerId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            TeachingYears instance;
            IList<TeachingYears> llist = new List<TeachingYears>();

            instance = new TeachingYears();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "ResTeachingYears_Get";
            dbCommand = db.GetStoredProcCommand(sqlCommand, centerId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new TeachingYears();
                instance.teachingYearId = (int)item["TeachingYearId"];
                instance.teachingYear = (string)item["TeachingYear"];
                llist.Add(instance);
            }
            return llist;
        }

        public static int GetNext(System.Int32 teachingYearId)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResTeachingYears_GetNext";
            dbCommand = db.GetStoredProcCommand(sqlCommand, teachingYearId);

            return Convert.ToInt32(db.ExecuteScalar(dbCommand));
        }


        //public static bool CheckDateRange(DateTime date)
        //{
        //    Database db;
        //    string sqlCommand;
        //    DbCommand dbCommand;

        //    db = DatabaseFactory.CreateDatabase(DataConnection);
        //    sqlCommand = "[dbo].ResTeachingYears_CheckDateRange";
        //    dbCommand = db.GetStoredProcCommand(sqlCommand, date);

        //    return Convert.ToBoolean(db.ExecuteScalar(dbCommand));
        //}
        #endregion
    }
}