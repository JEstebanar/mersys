﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Residences.Transfers
{

    [DataObject]
    [Serializable]
    public partial class ResTransfers
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[res_Transfers]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _transferId;
        private System.Int32? _residentId;
        private System.Int32? _toCenterId;
        private System.String _transferDocument;
        private System.Boolean? _transferred;
        private System.Int32? _createdBy;
        private System.DateTime? _createdDate;

        #endregion


        #region Properties
        public System.Int32? TransferId
        {
            get
            {
                return _transferId;
            }
            set
            {
                _transferId = value;
            }
        }

        public System.Int32? ResidentId
        {
            get
            {
                return _residentId;
            }
            set
            {
                _residentId = value;
            }
        }

        public System.Int32? ToCenterId
        {
            get
            {
                return _toCenterId;
            }
            set
            {
                _toCenterId = value;
            }
        }

        public System.String TransferDocument
        {
            get
            {
                return _transferDocument;
            }
            set
            {
                _transferDocument = value;
            }
        }

        public System.Boolean? Transferred
        {
            get
            {
                return _transferred;
            }
            set
            {
                _transferred = value;
            }
        }

        public System.Int32? CreatedBy
        {
            get
            {
                return _createdBy;
            }
            set
            {
                _createdBy = value;
            }
        }

        public System.DateTime? CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("TransferId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ResidentId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ToCenterId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("TransferDocument", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("Transferred", typeof(System.Boolean));
            ds.Tables[TABLE_NAME].Columns.Add("CreatedBy", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (TransferId == null)
                dr["TransferId"] = DBNull.Value;
            else
                dr["TransferId"] = TransferId;

            if (ResidentId == null)
                dr["ResidentId"] = DBNull.Value;
            else
                dr["ResidentId"] = ResidentId;

            if (ToCenterId == null)
                dr["ToCenterId"] = DBNull.Value;
            else
                dr["ToCenterId"] = ToCenterId;

            if (TransferDocument == null)
                dr["TransferDocument"] = DBNull.Value;
            else
                dr["TransferDocument"] = TransferDocument;

            if (Transferred == null)
                dr["Transferred"] = DBNull.Value;
            else
                dr["Transferred"] = Transferred;

            if (CreatedBy == null)
                dr["CreatedBy"] = DBNull.Value;
            else
                dr["CreatedBy"] = CreatedBy;

            if (CreatedDate == null)
                dr["CreatedDate"] = DBNull.Value;
            else
                dr["CreatedDate"] = CreatedDate;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            TransferId = dr["TransferId"] != DBNull.Value ? Convert.ToInt32(dr["TransferId"]) : TransferId = null;
            ResidentId = dr["ResidentId"] != DBNull.Value ? Convert.ToInt32(dr["ResidentId"]) : ResidentId = null;
            ToCenterId = dr["ToCenterId"] != DBNull.Value ? Convert.ToInt32(dr["ToCenterId"]) : ToCenterId = null;
            TransferDocument = dr["TransferDocument"] != DBNull.Value ? Convert.ToString(dr["TransferDocument"]) : TransferDocument = null;
            Transferred = dr["Transferred"] != DBNull.Value ? Convert.ToBoolean(dr["Transferred"]) : Transferred = null;
            CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : CreatedBy = null;
            CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
        }

        public static ResTransfers[] MapFrom(DataSet ds)
        {
            List<ResTransfers> objects;


            // Initialise Collection.
            objects = new List<ResTransfers>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[res_Transfers] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[res_Transfers] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                ResTransfers instance = new ResTransfers();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static ResTransfers Get(System.Int32 transferId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            ResTransfers instance;


            instance = new ResTransfers();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResTransfers_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, transferId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get ResTransfers ID:" + transferId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.Int32? residentId, System.Int32? toCenterId, System.String transferDocument, System.Boolean? transferred, System.Int32? createdBy, System.DateTime? createdDate, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResTransfers_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, residentId, toCenterId, transferDocument, transferred, createdBy, createdDate);

            if (transaction == null)
                this.TransferId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.TransferId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.Int32? residentId, System.Int32? toCenterId, System.String transferDocument, System.Boolean? transferred, System.Int32? createdBy, System.DateTime? createdDate)
        {
            Insert(residentId, toCenterId, transferDocument, transferred, createdBy, createdDate, null);
        }
        /// <summary>
        /// Insert current ResTransfers to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(ResidentId, ToCenterId, TransferDocument, Transferred, CreatedBy, CreatedDate, transaction);
        }

        /// <summary>
        /// Insert current ResTransfers to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? transferId, System.Int32? residentId, System.Int32? toCenterId, System.String transferDocument, System.Boolean? transferred, System.Int32? createdBy, System.DateTime? createdDate, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResTransfers_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@transferId"].Value = transferId;
            dbCommand.Parameters["@residentId"].Value = residentId;
            dbCommand.Parameters["@toCenterId"].Value = toCenterId;
            dbCommand.Parameters["@transferDocument"].Value = transferDocument;
            dbCommand.Parameters["@transferred"].Value = transferred;
            dbCommand.Parameters["@createdBy"].Value = createdBy;
            dbCommand.Parameters["@createdDate"].Value = createdDate;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? transferId, System.Int32? residentId, System.Int32? toCenterId, System.String transferDocument, System.Boolean? transferred, System.Int32? createdBy, System.DateTime? createdDate)
        {
            Update(transferId, residentId, toCenterId, transferDocument, transferred, createdBy, createdDate, null);
        }

        public static void Update(ResTransfers resTransfers)
        {
            resTransfers.Update();
        }

        public static void Update(ResTransfers resTransfers, DbTransaction transaction)
        {
            resTransfers.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResTransfers_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@transferId"].SourceColumn = "TransferId";
            dbCommand.Parameters["@residentId"].SourceColumn = "ResidentId";
            dbCommand.Parameters["@toCenterId"].SourceColumn = "ToCenterId";
            dbCommand.Parameters["@transferDocument"].SourceColumn = "TransferDocument";
            dbCommand.Parameters["@transferred"].SourceColumn = "Transferred";
            dbCommand.Parameters["@createdBy"].SourceColumn = "CreatedBy";
            dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? transferId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResTransfers_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, transferId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? transferId)
        {
            Delete(
            transferId);
        }

        /// <summary>
        /// Delete current ResTransfers from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResTransfers_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, TransferId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.TransferId = null;
        }

        /// <summary>
        /// Delete current ResTransfers from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static ResTransfers[] Search(System.Int32? transferId, System.Int32? residentId, System.Int32? toCenterId, System.String transferDocument, System.Boolean? transferred, System.Int32? createdBy, System.DateTime? createdDate)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResTransfers_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, transferId, residentId, toCenterId, transferDocument, transferred, createdBy, createdDate);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return ResTransfers.MapFrom(ds);
        }


        public static ResTransfers[] Search(ResTransfers searchObject)
        {
            return Search(searchObject.TransferId, searchObject.ResidentId, searchObject.ToCenterId, searchObject.TransferDocument, searchObject.Transferred, searchObject.CreatedBy, searchObject.CreatedDate);
        }

        /// <summary>
        /// Returns all ResTransfers objects.
        /// </summary>
        /// <returns>List of all ResTransfers objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static ResTransfers[] Search()
        {
            return Search(null, null, null, null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


    }


}