﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Residences.ResidentStatus
{

    [DataObject]
    [Serializable]
    public partial class ResResidentStatus
    {

        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[res_ResidentStatus]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Custom

        [Serializable]
        public class ResidentStatus
        {
            public int statusId { get; set; }
            public string statusName { get; set; }
        }

        public static IList<ResidentStatus> GetResidentsStatus()
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            ResidentStatus instance;
            IList<ResidentStatus> llist = new List<ResidentStatus>();

            instance = new ResidentStatus();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "ResResidentStatus_Get";
            dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new ResidentStatus();
                instance.statusId = (int)item["StatusId"];
                instance.statusName = (string)item["StatusName"];
                llist.Add(instance);
            }
            return llist;
        }

        [Serializable]
        public class Especialties
        {
            public string residenceName { get; set; }
            public string gradeName { get; set; }
            public string startDate { get; set; }
            public string endDate { get; set; }
        }

        #endregion
    }
}
