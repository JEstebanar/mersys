﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Residences.ResidentInfo
{
    [DataObject]
    [Serializable]
    public partial class ResResidentInfo
    {

        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[res_Residents]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _residentId;
        private System.Int32? _generalId;
        private System.Int32? _universityId;
        private System.Int32? _residentStatusId;
        private System.Int32? _residenceId;
        private System.String _preResidence;
        private System.Int32? _gradeId;
        private System.Int32? _centreId;
        private System.DateTime? _startDate;

        private System.String _enrollment;
        private System.String _exequatur;

        private System.Int32? _rotationId;

        private System.Int32? _relationshipId;
        private System.String _name;
        private System.String _middleName;
        private System.String _firstLastName;
        private System.String _secondLastName;
        private System.String _allergies;
        private System.String _drugs;

        #endregion


        #region Properties
        public System.Int32? ResidentId
        {
            get
            {
                return _residentId;
            }
            set
            {
                _residentId = value;
            }
        }

        public System.Int32? GeneralId
        {
            get
            {
                return _generalId;
            }
            set
            {
                _generalId = value;
            }
        }

        public System.Int32? UniversityId
        {
            get
            {
                return _universityId;
            }
            set
            {
                _universityId = value;
            }
        }

        public System.Int32? ResidentStatusId
        {
            get
            {
                return _residentStatusId;
            }
            set
            {
                _residentStatusId = value;
            }
        }

        public System.Int32? ResidenceId
        {
            get
            {
                return _residenceId;
            }
            set
            {
                _residenceId = value;
            }
        }

        public System.String PreResidence
        {
            get
            {
                return _preResidence;
            }
            set
            {
                _preResidence = value;
            }
        }

        public System.Int32? GradeId
        {
            get
            {
                return _gradeId;
            }
            set
            {
                _gradeId = value;
            }
        }

        public System.DateTime? StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value;
            }
        }

        public System.Int32? CentreId
        {
            get
            {
                return _centreId;
            }
            set
            {
                _centreId = value;
            }
        }


        public System.String Enrollment
        {
            get
            {
                return _enrollment;
            }
            set
            {
                _enrollment = value;
            }
        }

        public System.String Exequatur
        {
            get
            {
                return _exequatur;
            }
            set
            {
                _exequatur = value;
            }
        }


        public System.Int32? RotationId
        {
            get
            {
                return _rotationId;
            }
            set
            {
                _rotationId = value;
            }
        }


        public System.Int32? RelationshipId
        {
            get
            {
                return _relationshipId;
            }
            set
            {
                _relationshipId = value;
            }
        }

        public System.String Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public System.String MiddleName
        {
            get
            {
                return _middleName;
            }
            set
            {
                _middleName = value;
            }
        }

        public System.String FirstLastName
        {
            get
            {
                return _firstLastName;
            }
            set
            {
                _firstLastName = value;
            }
        }

        public System.String SecondLastName
        {
            get
            {
                return _secondLastName;
            }
            set
            {
                _secondLastName = value;
            }
        }

        public System.String Allergies
        {
            get
            {
                return _allergies;
            }
            set
            {
                _allergies = value;
            }
        }

        public System.String Drugs
        {
            get
            {
                return _drugs;
            }
            set
            {
                _drugs = value;
            }
        }
        #endregion


        #region Methods


        #region Mapping Methods

        //protected void MapTo(DataSet ds)
        //{
        //    DataRow dr;


        //    if (ds == null)
        //        ds = new DataSet();

        //    if (ds.Tables["TABLE_NAME"] == null)
        //        ds.Tables.Add(TABLE_NAME);

        //    ds.Tables[TABLE_NAME].Columns.Add("ResidentId", typeof(System.Int32));
        //    ds.Tables[TABLE_NAME].Columns.Add("GeneralId", typeof(System.Int32));
        //    ds.Tables[TABLE_NAME].Columns.Add("UniversityId", typeof(System.Int32));
        //    ds.Tables[TABLE_NAME].Columns.Add("ResidentStatusId", typeof(System.Int32));
        //    dr = ds.Tables[TABLE_NAME].NewRow();

        //    if (ResidentId == null)
        //        dr["ResidentId"] = DBNull.Value;
        //    else
        //        dr["ResidentId"] = ResidentId;

        //    if (GeneralId == null)
        //        dr["GeneralId"] = DBNull.Value;
        //    else
        //        dr["GeneralId"] = GeneralId;
           
        //    if (UniversityId == null)
        //        dr["UniversityId"] = DBNull.Value;
        //    else
        //        dr["UniversityId"] = UniversityId;

        //               if (ResidentStatusId == null)
        //        dr["ResidentStatusId"] = DBNull.Value;
        //    else
        //        dr["ResidentStatusId"] = ResidentStatusId;

        //               ds.Tables[TABLE_NAME].Rows.Add(dr);

        //}

        protected void MapFrom(DataRow dr)
        {
            ResidentId = dr["ResidentId"] != DBNull.Value ? Convert.ToInt32(dr["ResidentId"]) : ResidentId = null;
            GeneralId = dr["GeneralId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralId"]) : GeneralId = null;
            UniversityId = dr["UniversityId"] != DBNull.Value ? Convert.ToInt32(dr["UniversityId"]) : UniversityId = null;
            ResidentStatusId = dr["ResidentStatusId"] != DBNull.Value ? Convert.ToInt32(dr["ResidentStatusId"]) : ResidentStatusId = null;
            ResidenceId = dr["ResidenceId"] != DBNull.Value ? Convert.ToInt32(dr["ResidenceId"]) : ResidenceId = null;
            PreResidence = dr["CurrentResidence"] != DBNull.Value ? Convert.ToString(dr["CurrentResidence"]) : PreResidence = null;
            GradeId = dr["GradeId"] != DBNull.Value ? Convert.ToInt32(dr["GradeId"]) : GradeId = null;
            StartDate = dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : StartDate = null;
            CentreId = dr["CentreId"] != DBNull.Value ? Convert.ToInt32(dr["CentreId"]) : CentreId = null;

            Enrollment = dr["Enrollment"] != DBNull.Value ? Convert.ToString(dr["Enrollment"]) : Name = null;
            Exequatur = dr["Exequatur"] != DBNull.Value ? Convert.ToString(dr["Exequatur"]) : Name = null;

            RotationId = dr["RotationId"] != DBNull.Value ? Convert.ToInt32(dr["RotationId"]) : RotationId = null;

            RelationshipId = dr["RelationshipId"] != DBNull.Value ? Convert.ToInt32(dr["RelationshipId"]) : RelationshipId = null;
            Name = dr["Name"] != DBNull.Value ? Convert.ToString(dr["Name"]) : Name = null;
            MiddleName = dr["MiddleName"] != DBNull.Value ? Convert.ToString(dr["MiddleName"]) : MiddleName = null;
            FirstLastName = dr["FirstLastName"] != DBNull.Value ? Convert.ToString(dr["FirstLastName"]) : FirstLastName = null;
            SecondLastName = dr["SecondLastName"] != DBNull.Value ? Convert.ToString(dr["SecondLastName"]) : SecondLastName = null;
            Allergies = dr["Allergies"] != DBNull.Value ? Convert.ToString(dr["Allergies"]) : Allergies = null;
            Drugs = dr["Drugs"] != DBNull.Value ? Convert.ToString(dr["Drugs"]) : Drugs = null;
        }

        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static ResResidentInfo Get(System.Int32 residentId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            ResResidentInfo instance;


            instance = new ResResidentInfo();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResResidents_GetInfoByResidentId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, residentId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get ResResidents ID:" + residentId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }
        

        #endregion


        #endregion
    }
}
