﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Residences.ResidentStatusComments
{


    [DataObject]
    [Serializable]
    public partial class ResResidentStatusComments
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[res_ResidentStatusComments]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _commentId;
        private System.Int32? _residentId;
        private System.Int32? _statusId;
        private System.String _comment;
        private System.Int32? _createdBy;
        private System.DateTime? _createdDate;

        #endregion


        #region Properties
        public System.Int32? CommentId
        {
            get
            {
                return _commentId;
            }
            set
            {
                _commentId = value;
            }
        }

        public System.Int32? ResidentId
        {
            get
            {
                return _residentId;
            }
            set
            {
                _residentId = value;
            }
        }

        public System.Int32? StatusId
        {
            get
            {
                return _statusId;
            }
            set
            {
                _statusId = value;
            }
        }

        public System.String Comment
        {
            get
            {
                return _comment;
            }
            set
            {
                _comment = value;
            }
        }

        public System.Int32? CreatedBy
        {
            get
            {
                return _createdBy;
            }
            set
            {
                _createdBy = value;
            }
        }

        public System.DateTime? CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("CommentId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ResidentId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("StatusId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("Comment", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("CreatedBy", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (CommentId == null)
                dr["CommentId"] = DBNull.Value;
            else
                dr["CommentId"] = CommentId;

            if (ResidentId == null)
                dr["ResidentId"] = DBNull.Value;
            else
                dr["ResidentId"] = ResidentId;

            if (StatusId == null)
                dr["StatusId"] = DBNull.Value;
            else
                dr["StatusId"] = StatusId;

            if (Comment == null)
                dr["Comment"] = DBNull.Value;
            else
                dr["Comment"] = Comment;

            if (CreatedBy == null)
                dr["CreatedBy"] = DBNull.Value;
            else
                dr["CreatedBy"] = CreatedBy;

            if (CreatedDate == null)
                dr["CreatedDate"] = DBNull.Value;
            else
                dr["CreatedDate"] = CreatedDate;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            CommentId = dr["CommentId"] != DBNull.Value ? Convert.ToInt32(dr["CommentId"]) : CommentId = null;
            ResidentId = dr["ResidentId"] != DBNull.Value ? Convert.ToInt32(dr["ResidentId"]) : ResidentId = null;
            StatusId = dr["StatusId"] != DBNull.Value ? Convert.ToInt32(dr["StatusId"]) : StatusId = null;
            Comment = dr["Comment"] != DBNull.Value ? Convert.ToString(dr["Comment"]) : Comment = null;
            CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : CreatedBy = null;
            CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
        }

        public static ResResidentStatusComments[] MapFrom(DataSet ds)
        {
            List<ResResidentStatusComments> objects;


            // Initialise Collection.
            objects = new List<ResResidentStatusComments>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[res_ResidentStatusComments] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[res_ResidentStatusComments] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                ResResidentStatusComments instance = new ResResidentStatusComments();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static ResResidentStatusComments Get(System.Int32 commentId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            ResResidentStatusComments instance;


            instance = new ResResidentStatusComments();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResResidentStatusComments_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, commentId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get ResResidentStatusComments ID:" + commentId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.Int32? residentId, System.Int32? statusId, System.String comment, System.Int32? createdBy, System.DateTime? createdDate, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResResidentStatusComments_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, residentId, statusId, comment, createdBy, createdDate);

            if (transaction == null)
                this.CommentId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.CommentId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.Int32? residentId, System.Int32? statusId, System.String comment, System.Int32? createdBy, System.DateTime? createdDate)
        {
            Insert(residentId, statusId, comment, createdBy, createdDate, null);
        }
        /// <summary>
        /// Insert current ResResidentStatusComments to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(ResidentId, StatusId, Comment, CreatedBy, CreatedDate, transaction);
        }

        /// <summary>
        /// Insert current ResResidentStatusComments to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? commentId, System.Int32? residentId, System.Int32? statusId, System.String comment, System.Int32? createdBy, System.DateTime? createdDate, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResResidentStatusComments_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@commentId"].Value = commentId;
            dbCommand.Parameters["@residentId"].Value = residentId;
            dbCommand.Parameters["@statusId"].Value = statusId;
            dbCommand.Parameters["@comment"].Value = comment;
            dbCommand.Parameters["@createdBy"].Value = createdBy;
            dbCommand.Parameters["@createdDate"].Value = createdDate;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? commentId, System.Int32? residentId, System.Int32? statusId, System.String comment, System.Int32? createdBy, System.DateTime? createdDate)
        {
            Update(commentId, residentId, statusId, comment, createdBy, createdDate, null);
        }

        public static void Update(ResResidentStatusComments resResidentStatusComments)
        {
            resResidentStatusComments.Update();
        }

        public static void Update(ResResidentStatusComments resResidentStatusComments, DbTransaction transaction)
        {
            resResidentStatusComments.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResResidentStatusComments_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@commentId"].SourceColumn = "CommentId";
            dbCommand.Parameters["@residentId"].SourceColumn = "ResidentId";
            dbCommand.Parameters["@statusId"].SourceColumn = "StatusId";
            dbCommand.Parameters["@comment"].SourceColumn = "Comment";
            dbCommand.Parameters["@createdBy"].SourceColumn = "CreatedBy";
            dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? commentId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResResidentStatusComments_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, commentId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? commentId)
        {
            Delete(
            commentId);
        }

        /// <summary>
        /// Delete current ResResidentStatusComments from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResResidentStatusComments_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, CommentId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.CommentId = null;
        }

        /// <summary>
        /// Delete current ResResidentStatusComments from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static ResResidentStatusComments[] Search(System.Int32? commentId, System.Int32? residentId, System.Int32? statusId, System.String comment, System.Int32? createdBy, System.DateTime? createdDate)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].ResResidentStatusComments_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, commentId, residentId, statusId, comment, createdBy, createdDate);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return ResResidentStatusComments.MapFrom(ds);
        }


        public static ResResidentStatusComments[] Search(ResResidentStatusComments searchObject)
        {
            return Search(searchObject.CommentId, searchObject.ResidentId, searchObject.StatusId, searchObject.Comment, searchObject.CreatedBy, searchObject.CreatedDate);
        }

        /// <summary>
        /// Returns all ResResidentStatusComments objects.
        /// </summary>
        /// <returns>List of all ResResidentStatusComments objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static ResResidentStatusComments[] Search()
        {
            return Search(null, null, null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


        #region Custom
        [Serializable]
        public class ResidentStatusComments
        {
            public int commentId { get; set; }
            public string comment { get; set; }
            public string statusName { get; set; }
            public DateTime createdDate { get; set; }
        }

        public static IList<ResidentStatusComments> GetByResidentId(int residentId, int? specificStatusId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            ResidentStatusComments instance;
            IList<ResidentStatusComments> llist = new List<ResidentStatusComments>();

            instance = new ResidentStatusComments();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "ResResidentStatusComments_GetByResidentId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, residentId, specificStatusId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new ResidentStatusComments();
                instance.commentId = (int)item["CommentId"];
                instance.comment = (string)item["Comment"];
                instance.statusName = (string)item["StatusName"];
                instance.createdDate = (DateTime)item["CreatedDate"];
                llist.Add(instance);
            }
            return llist;
        }

        //[Serializable]
        //public class ResidentTransferComments
        //{
        //    public string comment { get; set; }
        //}

        //public static IList<ResidentTransferComments> GetTransferCommentByResidentId(int residentId)
        //{
        //    DataSet ds;
        //    string sqlCommand;
        //    DbCommand dbCommand;
        //    ResidentTransferComments instance;
        //    IList<ResidentTransferComments> llist = new List<ResidentTransferComments>();

        //    instance = new ResidentTransferComments();

        //    Database db = DatabaseFactory.CreateDatabase("DataConnection");
        //    sqlCommand = "ResResidentStatusComments_GetTransferCommentByResidentId";
        //    dbCommand = db.GetStoredProcCommand(sqlCommand, residentId);

        //    // Get results.
        //    ds = db.ExecuteDataSet(dbCommand);

        //    // Return results.
        //    ds.Tables[0].TableName = TABLE_NAME;

        //    foreach (DataRow item in ds.Tables[0].Rows)
        //    {
        //        instance = new ResidentTransferComments();
        //        instance.comment = (string)item["Comment"];
        //        llist.Add(instance);
        //    }
        //    return llist;
        //}

        #endregion
    }
}