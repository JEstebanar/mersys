﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Modules.Residences.Universities
{

[DataObject]
[Serializable]
public partial class ResUniversities
{
	
	
	#region Constants
    private static readonly string TABLE_NAME = "[dbo].[res_Universities]";
    private static readonly string DataConnection = "DataConnection";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _universityId;
	private System.String _universityName;
	private System.String _universityAcronym;
	private System.Boolean? _universityStatus;
	private System.Int32? _createdBY;
	private System.DateTime? _createdDate;
	private System.Int32? _modifiedBy;
	private System.DateTime? _modifiedDate;
	
	#endregion
	
	
	#region Properties
	public System.Int32? UniversityId
	{
		get
		{
			return _universityId;
		}
		set
		{
			_universityId = value;
		}
	}
	
	public System.String UniversityName
	{
		get
		{
			return _universityName;
		}
		set
		{
			_universityName = value;
		}
	}
	
	public System.String UniversityAcronym
	{
		get
		{
			return _universityAcronym;
		}
		set
		{
			_universityAcronym = value;
		}
	}
	
	public System.Boolean? UniversityStatus
	{
		get
		{
			return _universityStatus;
		}
		set
		{
			_universityStatus = value;
		}
	}
	
	public System.Int32? CreatedBY
	{
		get
		{
			return _createdBY;
		}
		set
		{
			_createdBY = value;
		}
	}
	
	public System.DateTime? CreatedDate
	{
		get
		{
			return _createdDate;
		}
		set
		{
			_createdDate = value;
		}
	}
	
	public System.Int32? ModifiedBy
	{
		get
		{
			return _modifiedBy;
		}
		set
		{
			_modifiedBy = value;
		}
	}
	
	public System.DateTime? ModifiedDate
	{
		get
		{
			return _modifiedDate;
		}
		set
		{
			_modifiedDate = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("UniversityId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("UniversityName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("UniversityAcronym", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("UniversityStatus", typeof(System.Boolean) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedBY", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (UniversityId == null)
		dr["UniversityId"] = DBNull.Value;
		else
		dr["UniversityId"] = UniversityId;
		
		if (UniversityName == null)
		dr["UniversityName"] = DBNull.Value;
		else
		dr["UniversityName"] = UniversityName;
		
		if (UniversityAcronym == null)
		dr["UniversityAcronym"] = DBNull.Value;
		else
		dr["UniversityAcronym"] = UniversityAcronym;
		
		if (UniversityStatus == null)
		dr["UniversityStatus"] = DBNull.Value;
		else
		dr["UniversityStatus"] = UniversityStatus;
		
		if (CreatedBY == null)
		dr["CreatedBY"] = DBNull.Value;
		else
		dr["CreatedBY"] = CreatedBY;
		
		if (CreatedDate == null)
		dr["CreatedDate"] = DBNull.Value;
		else
		dr["CreatedDate"] = CreatedDate;
		
		if (ModifiedBy == null)
		dr["ModifiedBy"] = DBNull.Value;
		else
		dr["ModifiedBy"] = ModifiedBy;
		
		if (ModifiedDate == null)
		dr["ModifiedDate"] = DBNull.Value;
		else
		dr["ModifiedDate"] = ModifiedDate;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		UniversityId = dr["UniversityId"] != DBNull.Value ? Convert.ToInt32(dr["UniversityId"]) : UniversityId = null;
		UniversityName = dr["UniversityName"] != DBNull.Value ? Convert.ToString(dr["UniversityName"]) : UniversityName = null;
		UniversityAcronym = dr["UniversityAcronym"] != DBNull.Value ? Convert.ToString(dr["UniversityAcronym"]) : UniversityAcronym = null;
		UniversityStatus = dr["UniversityStatus"] != DBNull.Value ? Convert.ToBoolean(dr["UniversityStatus"]) : UniversityStatus = null;
		CreatedBY = dr["CreatedBY"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBY"]) : CreatedBY = null;
		CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
		ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
		ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
	}
	
	public static ResUniversities[] MapFrom(DataSet ds)
	{
		List<ResUniversities> objects;
		
		
		// Initialise Collection.
		objects = new List<ResUniversities>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[res_Universities] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[res_Universities] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			ResUniversities instance = new ResUniversities();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static ResUniversities Get(System.Int32 universityId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		ResUniversities instance;
		
		
		instance = new ResUniversities();
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].ResUniversities_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, universityId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get ResUniversities ID:" + universityId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.String universityName, System.String universityAcronym, System.Boolean? universityStatus, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].ResUniversities_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, universityName, universityAcronym, universityStatus, createdBY, createdDate, modifiedBy, modifiedDate);
		
		if (transaction == null)
		this.UniversityId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.UniversityId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.String universityName, System.String universityAcronym, System.Boolean? universityStatus, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Insert(universityName, universityAcronym, universityStatus, createdBY, createdDate, modifiedBy, modifiedDate, null);
	}
	/// <summary>
	/// Insert current ResUniversities to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(UniversityName, UniversityAcronym, UniversityStatus, CreatedBY, CreatedDate, ModifiedBy, ModifiedDate, transaction);
	}
	
	/// <summary>
	/// Insert current ResUniversities to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? universityId, System.String universityName, System.String universityAcronym, System.Boolean? universityStatus, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].ResUniversities_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@universityId"].Value = universityId;
		dbCommand.Parameters["@universityName"].Value = universityName;
		dbCommand.Parameters["@universityAcronym"].Value = universityAcronym;
		dbCommand.Parameters["@universityStatus"].Value = universityStatus;
		dbCommand.Parameters["@createdBY"].Value = createdBY;
		dbCommand.Parameters["@createdDate"].Value = createdDate;
		dbCommand.Parameters["@modifiedBy"].Value = modifiedBy;
		dbCommand.Parameters["@modifiedDate"].Value = modifiedDate;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? universityId, System.String universityName, System.String universityAcronym, System.Boolean? universityStatus, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Update(universityId, universityName, universityAcronym, universityStatus, createdBY, createdDate, modifiedBy, modifiedDate, null);
	}
	
	public static void Update(ResUniversities resUniversities)
	{
		resUniversities.Update();
	}
	
	public static void Update(ResUniversities resUniversities, DbTransaction transaction)
	{
		resUniversities.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].ResUniversities_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@universityId"].SourceColumn = "UniversityId";
		dbCommand.Parameters["@universityName"].SourceColumn = "UniversityName";
		dbCommand.Parameters["@universityAcronym"].SourceColumn = "UniversityAcronym";
		dbCommand.Parameters["@universityStatus"].SourceColumn = "UniversityStatus";
		dbCommand.Parameters["@createdBY"].SourceColumn = "CreatedBY";
		dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
		dbCommand.Parameters["@modifiedBy"].SourceColumn = "ModifiedBy";
		dbCommand.Parameters["@modifiedDate"].SourceColumn = "ModifiedDate";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? universityId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].ResUniversities_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, universityId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? universityId)
	{
		Delete(
		universityId);
	}
	
	/// <summary>
	/// Delete current ResUniversities from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].ResUniversities_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, UniversityId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.UniversityId = null;
	}
	
	/// <summary>
	/// Delete current ResUniversities from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static ResUniversities[] Search(System.Int32? universityId, System.String universityName, System.String universityAcronym, System.Boolean? universityStatus, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase(DataConnection);
		sqlCommand = "[dbo].ResUniversities_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, universityId, universityName, universityAcronym, universityStatus, createdBY, createdDate, modifiedBy, modifiedDate);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return ResUniversities.MapFrom(ds);
	}
	
	
	public static ResUniversities[] Search(ResUniversities searchObject)
	{
		return Search ( searchObject.UniversityId, searchObject.UniversityName, searchObject.UniversityAcronym, searchObject.UniversityStatus, searchObject.CreatedBY, searchObject.CreatedDate, searchObject.ModifiedBy, searchObject.ModifiedDate);
	}
	
	/// <summary>
	/// Returns all ResUniversities objects.
	/// </summary>
	/// <returns>List of all ResUniversities objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static ResUniversities[] Search()
	{
		return Search ( null, null, null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion


    #region Custom

    [Serializable]
    public class Universities
    {
        public int universityId { get; set; }
        public string universityName { get; set; }
    }

    public static IList<Universities> GetUniversities()
    {
        DataSet ds;
        string sqlCommand;
        DbCommand dbCommand;
        Universities instance;
        IList<Universities> llist = new List<Universities>();

        instance = new Universities();

        Database db = DatabaseFactory.CreateDatabase("DataConnection");
        sqlCommand = "ResUniversities_Get";
        dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Get results.
        ds = db.ExecuteDataSet(dbCommand);

        // Return results.
        ds.Tables[0].TableName = TABLE_NAME;

        foreach (DataRow item in ds.Tables[0].Rows)
        {
            instance = new Universities();
            instance.universityId = (int)item["UniversityId"];
            instance.universityName = (string)item["UniversityAcronym"];
            llist.Add(instance);
        }
        return llist;
    }
    #endregion
}


}
