﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Billing.InvoiceDetails
{

    [DataObject]
    [Serializable]
    public partial class BilInvoiceDetails
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[bil_InvoiceDetails]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _detailId;
        private System.Int32? _invoiceId;
        private System.Int32? _serviceRequestId;
        private System.Boolean? _detailStatus;

        #endregion


        #region Properties
        public System.Int32? DetailId
        {
            get
            {
                return _detailId;
            }
            set
            {
                _detailId = value;
            }
        }

        public System.Int32? InvoiceId
        {
            get
            {
                return _invoiceId;
            }
            set
            {
                _invoiceId = value;
            }
        }

        public System.Int32? ServiceRequestId
        {
            get
            {
                return _serviceRequestId;
            }
            set
            {
                _serviceRequestId = value;
            }
        }

        public System.Boolean? DetailStatus
        {
            get
            {
                return _detailStatus;
            }
            set
            {
                _detailStatus = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("DetailId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("InvoiceId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ServiceRequestId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("DetailStatus", typeof(System.Boolean));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (DetailId == null)
                dr["DetailId"] = DBNull.Value;
            else
                dr["DetailId"] = DetailId;

            if (InvoiceId == null)
                dr["InvoiceId"] = DBNull.Value;
            else
                dr["InvoiceId"] = InvoiceId;

            if (ServiceRequestId == null)
                dr["ServiceRequestId"] = DBNull.Value;
            else
                dr["ServiceRequestId"] = ServiceRequestId;

            if (DetailStatus == null)
                dr["DetailStatus"] = DBNull.Value;
            else
                dr["DetailStatus"] = DetailStatus;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            DetailId = dr["DetailId"] != DBNull.Value ? Convert.ToInt32(dr["DetailId"]) : DetailId = null;
            InvoiceId = dr["InvoiceId"] != DBNull.Value ? Convert.ToInt32(dr["InvoiceId"]) : InvoiceId = null;
            ServiceRequestId = dr["ServiceRequestId"] != DBNull.Value ? Convert.ToInt32(dr["ServiceRequestId"]) : ServiceRequestId = null;
            DetailStatus = dr["DetailStatus"] != DBNull.Value ? Convert.ToBoolean(dr["DetailStatus"]) : DetailStatus = null;
        }

        public static BilInvoiceDetails[] MapFrom(DataSet ds)
        {
            List<BilInvoiceDetails> objects;


            // Initialise Collection.
            objects = new List<BilInvoiceDetails>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[bil_InvoiceDetails] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[bil_InvoiceDetails] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                BilInvoiceDetails instance = new BilInvoiceDetails();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static BilInvoiceDetails Get(System.Int32 detailId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            BilInvoiceDetails instance;


            instance = new BilInvoiceDetails();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].BilInvoiceDetails_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, detailId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get BilInvoiceDetails ID:" + detailId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.Int32? invoiceId, System.Int32? serviceRequestId, System.Boolean? detailStatus, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].BilInvoiceDetails_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, invoiceId, serviceRequestId, detailStatus);

            if (transaction == null)
                this.DetailId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.DetailId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.Int32? invoiceId, System.Int32? serviceRequestId, System.Boolean? detailStatus)
        {
            Insert(invoiceId, serviceRequestId, detailStatus, null);
        }
        /// <summary>
        /// Insert current BilInvoiceDetails to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(InvoiceId, ServiceRequestId, DetailStatus, transaction);
        }

        /// <summary>
        /// Insert current BilInvoiceDetails to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? detailId, System.Int32? invoiceId, System.Int32? serviceRequestId, System.Boolean? detailStatus, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].BilInvoiceDetails_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@detailId"].Value = detailId;
            dbCommand.Parameters["@invoiceId"].Value = invoiceId;
            dbCommand.Parameters["@serviceRequestId"].Value = serviceRequestId;
            dbCommand.Parameters["@detailStatus"].Value = detailStatus;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? detailId, System.Int32? invoiceId, System.Int32? serviceRequestId, System.Boolean? detailStatus)
        {
            Update(detailId, invoiceId, serviceRequestId, detailStatus, null);
        }

        public static void Update(BilInvoiceDetails bilInvoiceDetails)
        {
            bilInvoiceDetails.Update();
        }

        public static void Update(BilInvoiceDetails bilInvoiceDetails, DbTransaction transaction)
        {
            bilInvoiceDetails.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].BilInvoiceDetails_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@detailId"].SourceColumn = "DetailId";
            dbCommand.Parameters["@invoiceId"].SourceColumn = "InvoiceId";
            dbCommand.Parameters["@serviceRequestId"].SourceColumn = "ServiceRequestId";
            dbCommand.Parameters["@detailStatus"].SourceColumn = "DetailStatus";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? detailId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].BilInvoiceDetails_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, detailId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? detailId)
        {
            Delete(
            detailId);
        }

        /// <summary>
        /// Delete current BilInvoiceDetails from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].BilInvoiceDetails_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, DetailId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.DetailId = null;
        }

        /// <summary>
        /// Delete current BilInvoiceDetails from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static BilInvoiceDetails[] Search(System.Int32? detailId, System.Int32? invoiceId, System.Int32? serviceRequestId, System.Boolean? detailStatus)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].BilInvoiceDetails_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, detailId, invoiceId, serviceRequestId, detailStatus);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return BilInvoiceDetails.MapFrom(ds);
        }


        public static BilInvoiceDetails[] Search(BilInvoiceDetails searchObject)
        {
            return Search(searchObject.DetailId, searchObject.InvoiceId, searchObject.ServiceRequestId, searchObject.DetailStatus);
        }

        /// <summary>
        /// Returns all BilInvoiceDetails objects.
        /// </summary>
        /// <returns>List of all BilInvoiceDetails objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static BilInvoiceDetails[] Search()
        {
            return Search(null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


    }


}