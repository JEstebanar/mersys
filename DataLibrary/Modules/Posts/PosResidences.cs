﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Posts.Residences
{
    [DataObject]
    [Serializable]
    public partial class PosPostResidences
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[pos_PostResidences]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _postId;
        private System.Int32? _residenceId;

        #endregion


        #region Properties
        public System.Int32? PostId
        {
            get
            {
                return _postId;
            }
            set
            {
                _postId = value;
            }
        }

        public System.Int32? ResidenceId
        {
            get
            {
                return _residenceId;
            }
            set
            {
                _residenceId = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("PostId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ResidenceId", typeof(System.Int32));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (PostId == null)
                dr["PostId"] = DBNull.Value;
            else
                dr["PostId"] = PostId;

            if (ResidenceId == null)
                dr["ResidenceId"] = DBNull.Value;
            else
                dr["ResidenceId"] = ResidenceId;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            PostId = dr["PostId"] != DBNull.Value ? Convert.ToInt32(dr["PostId"]) : PostId = null;
            ResidenceId = dr["ResidenceId"] != DBNull.Value ? Convert.ToInt32(dr["ResidenceId"]) : ResidenceId = null;
        }

        public static PosPostResidences[] MapFrom(DataSet ds)
        {
            List<PosPostResidences> objects;


            // Initialise Collection.
            objects = new List<PosPostResidences>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[pos_PostResidences] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[pos_PostResidences] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                PosPostResidences instance = new PosPostResidences();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods


        #region INSERT
        public void Insert(System.Int32? postId, System.Int32? residenceId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].PosPostResidences_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, postId, residenceId);

            if (transaction == null)
                db.ExecuteScalar(dbCommand);
            else
                db.ExecuteScalar(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.Int32? postId, System.Int32? residenceId)
        {
            Insert(postId, residenceId, null);
        }
        /// <summary>
        /// Insert current PosPostResidences to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(PostId, ResidenceId, transaction);
        }

        /// <summary>
        /// Insert current PosPostResidences to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion




        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static PosPostResidences[] Search(System.Int32? postId, System.Int32? residenceId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].PosPostResidences_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, postId, residenceId);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return PosPostResidences.MapFrom(ds);
        }


        public static PosPostResidences[] Search(PosPostResidences searchObject)
        {
            return Search(searchObject.PostId, searchObject.ResidenceId);
        }

        /// <summary>
        /// Returns all PosPostResidences objects.
        /// </summary>
        /// <returns>List of all PosPostResidences objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static PosPostResidences[] Search()
        {
            return Search(null, null);
        }

        #endregion


        #endregion


        #endregion


    }

}