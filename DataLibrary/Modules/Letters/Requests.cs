﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Letters.Requests
{

    [DataObject]
    [Serializable]
    public partial class LetRequests
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[let_Requests]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _requestId;
        private System.Int32? _letterId;
        private System.Int32? _requestStatusId;

        #endregion


        #region Properties
        public System.Int32? RequestId
        {
            get
            {
                return _requestId;
            }
            set
            {
                _requestId = value;
            }
        }

        public System.Int32? LetterId
        {
            get
            {
                return _letterId;
            }
            set
            {
                _letterId = value;
            }
        }

        public System.Int32? RequestStatusId
        {
            get
            {
                return _requestStatusId;
            }
            set
            {
                _requestStatusId = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("RequestId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("LetterId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("RequestStatusId", typeof(System.Int32));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (RequestId == null)
                dr["RequestId"] = DBNull.Value;
            else
                dr["RequestId"] = RequestId;

            if (LetterId == null)
                dr["LetterId"] = DBNull.Value;
            else
                dr["LetterId"] = LetterId;

            if (RequestStatusId == null)
                dr["RequestStatusId"] = DBNull.Value;
            else
                dr["RequestStatusId"] = RequestStatusId;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            RequestId = dr["RequestId"] != DBNull.Value ? Convert.ToInt32(dr["RequestId"]) : RequestId = null;
            LetterId = dr["LetterId"] != DBNull.Value ? Convert.ToInt32(dr["LetterId"]) : LetterId = null;
            RequestStatusId = dr["RequestStatusId"] != DBNull.Value ? Convert.ToInt32(dr["RequestStatusId"]) : RequestStatusId = null;
        }

        public static LetRequests[] MapFrom(DataSet ds)
        {
            List<LetRequests> objects;


            // Initialise Collection.
            objects = new List<LetRequests>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[let_Requests] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[let_Requests] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                LetRequests instance = new LetRequests();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static LetRequests Get(System.Int32 requestId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            LetRequests instance;


            instance = new LetRequests();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].LetRequests_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, requestId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get LetRequests ID:" + requestId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public int Insert(System.Int32? letterId, System.Int32? requestStatusId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].LetRequests_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, letterId, requestStatusId);

            if (transaction == null)
                this.RequestId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.RequestId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return Convert.ToInt32(this.RequestId);
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public int Insert(System.Int32? letterId, System.Int32? requestStatusId)
        {
            return Insert(letterId, requestStatusId, null);
        }
        /// <summary>
        /// Insert current LetRequests to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public int Insert(DbTransaction transaction)
        {
            return Insert(LetterId, RequestStatusId, transaction);
        }

        /// <summary>
        /// Insert current LetRequests to database.
        /// </summary>
        public int Insert()
        {
            return this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? requestId, System.Int32? letterId, System.Int32? requestStatusId, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].LetRequests_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@requestId"].Value = requestId;
            dbCommand.Parameters["@letterId"].Value = letterId;
            dbCommand.Parameters["@requestStatusId"].Value = requestStatusId;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? requestId, System.Int32? letterId, System.Int32? requestStatusId)
        {
            Update(requestId, letterId, requestStatusId, null);
        }

        public static void Update(LetRequests letRequests)
        {
            letRequests.Update();
        }

        public static void Update(LetRequests letRequests, DbTransaction transaction)
        {
            letRequests.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].LetRequests_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@requestId"].SourceColumn = "RequestId";
            dbCommand.Parameters["@letterId"].SourceColumn = "LetterId";
            dbCommand.Parameters["@requestStatusId"].SourceColumn = "RequestStatusId";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? requestId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].LetRequests_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, requestId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? requestId)
        {
            Delete(
            requestId);
        }

        /// <summary>
        /// Delete current LetRequests from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].LetRequests_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, RequestId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.RequestId = null;
        }

        /// <summary>
        /// Delete current LetRequests from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static LetRequests[] Search(System.Int32? requestId, System.Int32? letterId, System.Int32? requestStatusId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].LetRequests_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, requestId, letterId, requestStatusId);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return LetRequests.MapFrom(ds);
        }


        public static LetRequests[] Search(LetRequests searchObject)
        {
            return Search(searchObject.RequestId, searchObject.LetterId, searchObject.RequestStatusId);
        }

        /// <summary>
        /// Returns all LetRequests objects.
        /// </summary>
        /// <returns>List of all LetRequests objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static LetRequests[] Search()
        {
            return Search(null, null, null);
        }

        #endregion


        #endregion


        #endregion

        #region Custom
        [Serializable]
        public class Requests
        {
            public int invoiceDetailId { get; set; }
            public int requestId { get; set; }
            public string createdDate { get; set; }
            public string letterName { get; set; }
            public string fullName { get; set; }
            public int generalId { get; set; }
            public string letterStatus { get; set; }
        }

        public static IList<Requests> GetByStatusId(int statusId, int centerId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            Requests instance;
            IList<Requests> llist = new List<Requests>();

            instance = new Requests();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "LetRequests_GetByStatusId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, statusId, centerId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new Requests();
                instance.invoiceDetailId = (int)item["DetailId"];
                instance.requestId = (int)item["RequestId"];
                instance.createdDate = (string)item["CreatedDate"];
                instance.letterName = (string)item["LetterName"];
                instance.fullName = (string)item["FullName"];
                instance.generalId = (int)item["GeneralId"];
                llist.Add(instance);
            }
            return llist;
        }

        public static bool IsLetterRequested(int generalId, int letterId)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].LetRequests_IsLetterRequested";
            dbCommand = db.GetStoredProcCommand(sqlCommand, generalId, letterId);

            return Convert.ToBoolean(db.ExecuteScalar(dbCommand));
        }

        public static IList<Requests> GetRequestStatusByGeneralId(int generalId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            Requests instance;
            IList<Requests> llist = new List<Requests>();

            instance = new Requests();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "LetRequests_GetRequestStatusByGeneralId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, generalId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new Requests();
                instance.createdDate = (string)item["CreatedDate"];
                instance.letterName = (string)item["LetterName"];
                instance.letterStatus = (string)item["StatusName"];
                llist.Add(instance);
            }
            return llist;
        }
        #endregion
    }
}