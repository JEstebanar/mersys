﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SystemSecurity.Roles
{

    [DataObject]
    [Serializable]
    public partial class SysRoles
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[sys_Roles]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _roleId;
        private System.String _roleName;
        private System.String _roleDescription;
        private System.Int32? _roleSecurityId;
        private System.Boolean? _roleStatus;
        private System.Decimal? _roleOrder;

        #endregion


        #region Properties
        public System.Int32? RoleId
        {
            get
            {
                return _roleId;
            }
            set
            {
                _roleId = value;
            }
        }

        public System.String RoleName
        {
            get
            {
                return _roleName;
            }
            set
            {
                _roleName = value;
            }
        }

        public System.String RoleDescription
        {
            get
            {
                return _roleDescription;
            }
            set
            {
                _roleDescription = value;
            }
        }

        public System.Int32? RoleSecurityId
        {
            get
            {
                return _roleSecurityId;
            }
            set
            {
                _roleSecurityId = value;
            }
        }

        public System.Boolean? RoleStatus
        {
            get
            {
                return _roleStatus;
            }
            set
            {
                _roleStatus = value;
            }
        }

        public System.Decimal? RoleOrder
        {
            get
            {
                return _roleOrder;
            }
            set
            {
                _roleOrder = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("RoleId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("RoleName", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("RoleDescription", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("RoleSecurityId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("RoleStatus", typeof(System.Boolean));
            ds.Tables[TABLE_NAME].Columns.Add("RoleOrder", typeof(System.Decimal));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (RoleId == null)
                dr["RoleId"] = DBNull.Value;
            else
                dr["RoleId"] = RoleId;

            if (RoleName == null)
                dr["RoleName"] = DBNull.Value;
            else
                dr["RoleName"] = RoleName;

            if (RoleDescription == null)
                dr["RoleDescription"] = DBNull.Value;
            else
                dr["RoleDescription"] = RoleDescription;

            if (RoleSecurityId == null)
                dr["RoleSecurityId"] = DBNull.Value;
            else
                dr["RoleSecurityId"] = RoleSecurityId;

            if (RoleStatus == null)
                dr["RoleStatus"] = DBNull.Value;
            else
                dr["RoleStatus"] = RoleStatus;

            if (RoleOrder == null)
                dr["RoleOrder"] = DBNull.Value;
            else
                dr["RoleOrder"] = RoleOrder;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            RoleId = dr["RoleId"] != DBNull.Value ? Convert.ToInt32(dr["RoleId"]) : RoleId = null;
            RoleName = dr["RoleName"] != DBNull.Value ? Convert.ToString(dr["RoleName"]) : RoleName = null;
            RoleDescription = dr["RoleDescription"] != DBNull.Value ? Convert.ToString(dr["RoleDescription"]) : RoleDescription = null;
            RoleSecurityId = dr["RoleSecurityId"] != DBNull.Value ? Convert.ToInt32(dr["RoleSecurityId"]) : RoleSecurityId = null;
            RoleStatus = dr["RoleStatus"] != DBNull.Value ? Convert.ToBoolean(dr["RoleStatus"]) : RoleStatus = null;
            RoleOrder = dr["RoleOrder"] != DBNull.Value ? Convert.ToDecimal(dr["RoleOrder"]) : RoleOrder = null;
        }

        public static SysRoles[] MapFrom(DataSet ds)
        {
            List<SysRoles> objects;


            // Initialise Collection.
            objects = new List<SysRoles>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[sys_Roles] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[sys_Roles] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                SysRoles instance = new SysRoles();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysRoles Get(System.Int32 roleId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            SysRoles instance;


            instance = new SysRoles();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysRoles_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, roleId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysRoles ID:" + roleId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.String roleName, System.String roleDescription, System.Int32? roleSecurityId, System.Boolean? roleStatus, System.Decimal? roleOrder, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysRoles_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, roleName, roleDescription, roleSecurityId, roleStatus, roleOrder);

            if (transaction == null)
                this.RoleId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.RoleId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.String roleName, System.String roleDescription, System.Int32? roleSecurityId, System.Boolean? roleStatus, System.Decimal? roleOrder)
        {
            Insert(roleName, roleDescription, roleSecurityId, roleStatus, roleOrder, null);
        }
        /// <summary>
        /// Insert current SysRoles to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(RoleName, RoleDescription, RoleSecurityId, RoleStatus, RoleOrder, transaction);
        }

        /// <summary>
        /// Insert current SysRoles to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? roleId, System.String roleName, System.String roleDescription, System.Int32? roleSecurityId, System.Boolean? roleStatus, System.Decimal? roleOrder, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysRoles_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@roleId"].Value = roleId;
            dbCommand.Parameters["@roleName"].Value = roleName;
            dbCommand.Parameters["@roleDescription"].Value = roleDescription;
            dbCommand.Parameters["@roleSecurityId"].Value = roleSecurityId;
            dbCommand.Parameters["@roleStatus"].Value = roleStatus;
            dbCommand.Parameters["@roleOrder"].Value = roleOrder;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? roleId, System.String roleName, System.String roleDescription, System.Int32? roleSecurityId, System.Boolean? roleStatus, System.Decimal? roleOrder)
        {
            Update(roleId, roleName, roleDescription, roleSecurityId, roleStatus, roleOrder, null);
        }

        public static void Update(SysRoles sysRoles)
        {
            sysRoles.Update();
        }

        public static void Update(SysRoles sysRoles, DbTransaction transaction)
        {
            sysRoles.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysRoles_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@roleId"].SourceColumn = "RoleId";
            dbCommand.Parameters["@roleName"].SourceColumn = "RoleName";
            dbCommand.Parameters["@roleDescription"].SourceColumn = "RoleDescription";
            dbCommand.Parameters["@roleSecurityId"].SourceColumn = "RoleSecurityId";
            dbCommand.Parameters["@roleStatus"].SourceColumn = "RoleStatus";
            dbCommand.Parameters["@roleOrder"].SourceColumn = "RoleOrder";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? roleId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysRoles_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, roleId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? roleId)
        {
            Delete(
            roleId);
        }

        /// <summary>
        /// Delete current SysRoles from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysRoles_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, RoleId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.RoleId = null;
        }

        /// <summary>
        /// Delete current SysRoles from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysRoles[] Search(System.Int32? roleId, System.String roleName, System.String roleDescription, System.Int32? roleSecurityId, System.Boolean? roleStatus, System.Decimal? roleOrder)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysRoles_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, roleId, roleName, roleDescription, roleSecurityId, roleStatus, roleOrder);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return SysRoles.MapFrom(ds);
        }


        public static SysRoles[] Search(SysRoles searchObject)
        {
            return Search(searchObject.RoleId, searchObject.RoleName, searchObject.RoleDescription, searchObject.RoleSecurityId, searchObject.RoleStatus, searchObject.RoleOrder);
        }

        /// <summary>
        /// Returns all SysRoles objects.
        /// </summary>
        /// <returns>List of all SysRoles objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static SysRoles[] Search()
        {
            return Search(null, null, null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


        #region Custom

        [Serializable]
        public class Roles
        {
            public int roleId { get; set; }
            public string roleName { get; set; }
        }

        public static IList<Roles> GetRoles()
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            Roles instance;
            IList<Roles> llist = new List<Roles>();

            instance = new Roles();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysRoles_Get";
            dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new Roles();
                instance.roleId = (int)item["RoleId"];
                instance.roleName = (string)item["RoleName"];
                llist.Add(instance);
            }
            return llist;
        }

        #endregion
    }
}
