﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SystemSecurity.UserRoles
{

    [DataObject]
    [Serializable]
    public partial class SysUserRoles
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[sys_UserRoles]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _userRoleId;
        private System.Int32? _userCenterId;
        private System.Int32? _roleId;
        private System.Boolean? _userRoleStatus;

        #endregion


        #region Properties
        public System.Int32? UserRoleId
        {
            get
            {
                return _userRoleId;
            }
            set
            {
                _userRoleId = value;
            }
        }

        public System.Int32? UserCenterId
        {
            get
            {
                return _userCenterId;
            }
            set
            {
                _userCenterId = value;
            }
        }

        public System.Int32? RoleId
        {
            get
            {
                return _roleId;
            }
            set
            {
                _roleId = value;
            }
        }

        public System.Boolean? UserRoleStatus
        {
            get
            {
                return _userRoleStatus;
            }
            set
            {
                _userRoleStatus = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("UserRoleId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("UserCenterId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("RoleId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("UserRoleStatus", typeof(System.Boolean));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (UserRoleId == null)
                dr["UserRoleId"] = DBNull.Value;
            else
                dr["UserRoleId"] = UserRoleId;

            if (UserCenterId == null)
                dr["UserCenterId"] = DBNull.Value;
            else
                dr["UserCenterId"] = UserCenterId;

            if (RoleId == null)
                dr["RoleId"] = DBNull.Value;
            else
                dr["RoleId"] = RoleId;

            if (UserRoleStatus == null)
                dr["UserRoleStatus"] = DBNull.Value;
            else
                dr["UserRoleStatus"] = UserRoleStatus;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            UserRoleId = dr["UserRoleId"] != DBNull.Value ? Convert.ToInt32(dr["UserRoleId"]) : UserRoleId = null;
            UserCenterId = dr["UserCenterId"] != DBNull.Value ? Convert.ToInt32(dr["UserCenterId"]) : UserCenterId = null;
            RoleId = dr["RoleId"] != DBNull.Value ? Convert.ToInt32(dr["RoleId"]) : RoleId = null;
            UserRoleStatus = dr["UserRoleStatus"] != DBNull.Value ? Convert.ToBoolean(dr["UserRoleStatus"]) : UserRoleStatus = null;
        }

        public static SysUserRoles[] MapFrom(DataSet ds)
        {
            List<SysUserRoles> objects;


            // Initialise Collection.
            objects = new List<SysUserRoles>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[sys_UserRoles] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[sys_UserRoles] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                SysUserRoles instance = new SysUserRoles();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysUserRoles Get(System.Int32 userRoleId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            SysUserRoles instance;


            instance = new SysUserRoles();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserRoles_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userRoleId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysUserRoles ID:" + userRoleId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.Int32? userCenterId, System.Int32? roleId, System.Boolean? userRoleStatus, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserRoles_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterId, roleId, userRoleStatus);

            if (transaction == null)
                this.UserRoleId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.UserRoleId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.Int32? userCenterId, System.Int32? roleId, System.Boolean? userRoleStatus)
        {
            Insert(userCenterId, roleId, userRoleStatus, null);
        }
        /// <summary>
        /// Insert current SysUserRoles to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(UserCenterId, RoleId, UserRoleStatus, transaction);
        }

        /// <summary>
        /// Insert current SysUserRoles to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? userRoleId, System.Int32? userCenterId, System.Int32? roleId, System.Boolean? userRoleStatus, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserRoles_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@userRoleId"].Value = userRoleId;
            dbCommand.Parameters["@userCenterId"].Value = userCenterId;
            dbCommand.Parameters["@roleId"].Value = roleId;
            dbCommand.Parameters["@userRoleStatus"].Value = userRoleStatus;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? userRoleId, System.Int32? userCenterId, System.Int32? roleId, System.Boolean? userRoleStatus)
        {
            Update(userRoleId, userCenterId, roleId, userRoleStatus, null);
        }

        public static void Update(SysUserRoles sysUserRoles)
        {
            sysUserRoles.Update();
        }

        public static void Update(SysUserRoles sysUserRoles, DbTransaction transaction)
        {
            sysUserRoles.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserRoles_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@userRoleId"].SourceColumn = "UserRoleId";
            dbCommand.Parameters["@userCenterId"].SourceColumn = "UserCenterId";
            dbCommand.Parameters["@roleId"].SourceColumn = "RoleId";
            dbCommand.Parameters["@userRoleStatus"].SourceColumn = "UserRoleStatus";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? userRoleId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserRoles_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userRoleId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? userRoleId)
        {
            Delete(
            userRoleId);
        }

        /// <summary>
        /// Delete current SysUserRoles from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserRoles_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, UserRoleId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.UserRoleId = null;
        }

        /// <summary>
        /// Delete current SysUserRoles from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysUserRoles[] Search(System.Int32? userRoleId, System.Int32? userCenterId, System.Int32? roleId, System.Boolean? userRoleStatus)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserRoles_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userRoleId, userCenterId, roleId, userRoleStatus);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return SysUserRoles.MapFrom(ds);
        }


        public static SysUserRoles[] Search(SysUserRoles searchObject)
        {
            return Search(searchObject.UserRoleId, searchObject.UserCenterId, searchObject.RoleId, searchObject.UserRoleStatus);
        }

        /// <summary>
        /// Returns all SysUserRoles objects.
        /// </summary>
        /// <returns>List of all SysUserRoles objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static SysUserRoles[] Search()
        {
            return Search(null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


        #region Custom

        [Serializable]
        public class UserRoles
        {
            public int userRoleId { get; set; }
            public int userCenterId { get; set; }
            public int roleId { get; set; }
            public string roleName { get; set; }
            public bool userRoleStatus { get; set; }
        }

        public static IList<UserRoles> GetByUserId(int userId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            UserRoles instance;
            IList<UserRoles> llist = new List<UserRoles>();

            instance = new UserRoles();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysUserRoles_GetByUserId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new UserRoles();
                instance.userRoleId = (int)item["UserRoleId"];
                instance.userCenterId = (int)item["UserCenterId"];
                instance.roleId = (int)item["RoleId"];
                instance.roleName = (string)item["RoleName"];
                instance.userRoleStatus = Convert.ToBoolean(item["UserRoleStatus"]);
                llist.Add(instance);
            }
            return llist;
        }

        public static IList<UserRoles> GetRoles()
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            UserRoles instance;
            IList<UserRoles> llist = new List<UserRoles>();

            instance = new UserRoles();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysUserRoles_GetRoles";
            dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new UserRoles();
                instance.userRoleId = (int)item["UserRoleId"];
                instance.userCenterId = (int)item["UserCenterId"];
                instance.roleId = (int)item["RoleId"];
                instance.roleName = (string)item["RoleName"];
                instance.userRoleStatus = Convert.ToBoolean(item["UserRoleStatus"]);
                llist.Add(instance);
            }
            return llist;
        }

        public static IList<UserRoles> GetRolesToAssign()
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            UserRoles instance;
            IList<UserRoles> llist = new List<UserRoles>();

            instance = new UserRoles();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysUserRoles_GetRolesToAssign";
            dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new UserRoles();
                instance.userRoleId = (int)item["UserRoleId"];
                instance.userCenterId = (int)item["UserCenterId"];
                instance.roleId = (int)item["RoleId"];
                instance.roleName = (string)item["RoleName"];
                instance.userRoleStatus = Convert.ToBoolean(item["UserRoleStatus"]);
                llist.Add(instance);
            }
            return llist;
        }

        public static int IfExists(System.Int32 userRoleId, System.Int32? userCenterId, System.Int32? roleId, System.Boolean? userRoleStatus)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserRoles_IfExists";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userRoleId, userCenterId, roleId, userRoleStatus);

            try
            {
                return Convert.ToInt32(db.ExecuteScalar(dbCommand));
            }
            catch (ArgumentNullException)
            {
                return 0;
            }
        }

        /// <summary>
        /// this is important for access.aspx
        /// </summary>
        /// <param name="userCenterId"></param>
        /// <returns>role list</returns>
        public static IList<UserRoles> GetByUserCenterId(int userCenterId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            UserRoles instance;
            IList<UserRoles> llist = new List<UserRoles>();

            instance = new UserRoles();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysUserRoles_GetByUserCenterId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new UserRoles();
                //instance.userRoleId = (int)item["UserRoleId"];
                //instance.userCenterId = (int)item["UserCenterId"];
                instance.roleId = (int)item["RoleId"];
                instance.roleName = (string)item["RoleName"];
                //instance.userRoleStatus = Convert.ToBoolean(item["UserRoleStatus"]);
                llist.Add(instance);
            }
            return llist;
        }

        #endregion

    }

}