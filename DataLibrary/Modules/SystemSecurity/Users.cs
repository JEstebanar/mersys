﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using SystemSecurity.UserCenters;
using General.GeneralCommons;
using SystemSecurity.CentersInfo;
using SystemSecurity.UserRoles;
using System.Security;
using SystemSecurity.Utilities;
using General.EMails;

namespace SystemSecurity.SysUsers
{

    [DataObject]
    [Serializable]
    public partial class SysUsers
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[sys_Users]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _userId;
        private System.String _userName;
        private System.String _userPassword;
        private System.String _confirmationCode;
        private System.Boolean? _userStatus;
        private System.Int32? _generalId;

        #endregion


        #region Properties
        public System.Int32? UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        public System.String UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }

        public System.String UserPassword
        {
            get
            {
                return _userPassword;
            }
            set
            {
                _userPassword = value;
            }
        }

        public System.String ConfirmationCode
        {
            get
            {
                return _confirmationCode;
            }
            set
            {
                _confirmationCode = value;
            }
        }

        public System.Boolean? UserStatus
        {
            get
            {
                return _userStatus;
            }
            set
            {
                _userStatus = value;
            }
        }

        public System.Int32? GeneralId
        {
            get
            {
                return _generalId;
            }
            set
            {
                _generalId = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("UserId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("UserName", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("UserPassword", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("ConfirmationCode", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("UserStatus", typeof(System.Boolean));
            ds.Tables[TABLE_NAME].Columns.Add("GeneralId", typeof(System.Int32));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (UserId == null)
                dr["UserId"] = DBNull.Value;
            else
                dr["UserId"] = UserId;

            if (UserName == null)
                dr["UserName"] = DBNull.Value;
            else
                dr["UserName"] = UserName;

            if (UserPassword == null)
                dr["UserPassword"] = DBNull.Value;
            else
                dr["UserPassword"] = UserPassword;

            if (ConfirmationCode == null)
                dr["ConfirmationCode"] = DBNull.Value;
            else
                dr["ConfirmationCode"] = ConfirmationCode;

            if (UserStatus == null)
                dr["UserStatus"] = DBNull.Value;
            else
                dr["UserStatus"] = UserStatus;

            if (GeneralId == null)
                dr["GeneralId"] = DBNull.Value;
            else
                dr["GeneralId"] = GeneralId;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            UserId = dr["UserId"] != DBNull.Value ? Convert.ToInt32(dr["UserId"]) : UserId = null;
            UserName = dr["UserName"] != DBNull.Value ? Convert.ToString(dr["UserName"]) : UserName = null;
            UserPassword = dr["UserPassword"] != DBNull.Value ? Convert.ToString(dr["UserPassword"]) : UserPassword = null;
            ConfirmationCode = dr["ConfirmationCode"] != DBNull.Value ? Convert.ToString(dr["ConfirmationCode"]) : ConfirmationCode = null;
            UserStatus = dr["UserStatus"] != DBNull.Value ? Convert.ToBoolean(dr["UserStatus"]) : UserStatus = null;
            GeneralId = dr["GeneralId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralId"]) : GeneralId = null;
        }

        public static SysUsers[] MapFrom(DataSet ds)
        {
            List<SysUsers> objects;


            // Initialise Collection.
            objects = new List<SysUsers>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[sys_Users] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[sys_Users] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                SysUsers instance = new SysUsers();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysUsers Get(System.Int32 userId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            SysUsers instance;


            instance = new SysUsers();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUsers_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysUsers ID:" + userId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public int Insert(System.String userName, System.String userPassword, System.String confirmationCode, System.Boolean? userStatus, System.Int32? generalId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUsers_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userName, userPassword, confirmationCode, userStatus, generalId);

            if (transaction == null)
                this.UserId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.UserId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return Convert.ToInt32(UserId);
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public int Insert(System.String userName, System.String userPassword, System.String confirmationCode, System.Boolean? userStatus, System.Int32? generalId)
        {
            return Insert(userName, userPassword, confirmationCode, userStatus, generalId, null);
        }
        /// <summary>
        /// Insert current SysUsers to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public int Insert(DbTransaction transaction)
        {
            return Insert(UserName, UserPassword, ConfirmationCode, UserStatus, GeneralId, transaction);
        }

        /// <summary>
        /// Insert current SysUsers to database.
        /// </summary>
        public int Insert()
        {
            return this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? userId, System.String userName, System.String userPassword, System.String confirmationCode, System.Boolean? userStatus, System.Int32? generalId, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUsers_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@userId"].Value = userId;
            dbCommand.Parameters["@userName"].Value = userName;
            dbCommand.Parameters["@userPassword"].Value = userPassword;
            dbCommand.Parameters["@confirmationCode"].Value = confirmationCode;
            dbCommand.Parameters["@userStatus"].Value = userStatus;
            dbCommand.Parameters["@generalId"].Value = generalId;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? userId, System.String userName, System.String userPassword, System.String confirmationCode, System.Boolean? userStatus, System.Int32? generalId)
        {
            Update(userId, userName, userPassword, confirmationCode, userStatus, generalId, null);
        }

        public static void Update(SysUsers sysUsers)
        {
            sysUsers.Update();
        }

        public static void Update(SysUsers sysUsers, DbTransaction transaction)
        {
            sysUsers.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUsers_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@userId"].SourceColumn = "UserId";
            dbCommand.Parameters["@userName"].SourceColumn = "UserName";
            dbCommand.Parameters["@userPassword"].SourceColumn = "UserPassword";
            dbCommand.Parameters["@confirmationCode"].SourceColumn = "ConfirmationCode";
            dbCommand.Parameters["@userStatus"].SourceColumn = "UserStatus";
            dbCommand.Parameters["@generalId"].SourceColumn = "GeneralId";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? userId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUsers_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? userId)
        {
            Delete(
            userId);
        }

        /// <summary>
        /// Delete current SysUsers from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUsers_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, UserId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.UserId = null;
        }

        /// <summary>
        /// Delete current SysUsers from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysUsers[] Search(System.Int32? userId, System.String userName, System.String userPassword, System.String confirmationCode, System.Boolean? userStatus, System.Int32? generalId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUsers_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userId, userName, userPassword, confirmationCode, userStatus, generalId);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return SysUsers.MapFrom(ds);
        }


        public static SysUsers[] Search(SysUsers searchObject)
        {
            return Search(searchObject.UserId, searchObject.UserName, searchObject.UserPassword, searchObject.ConfirmationCode, searchObject.UserStatus, searchObject.GeneralId);
        }

        /// <summary>
        /// Returns all SysUsers objects.
        /// </summary>
        /// <returns>List of all SysUsers objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static SysUsers[] Search()
        {
            return Search(null, null, null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


        #region Custom
        public static int CreateUser(System.Int32 generalId, System.String userName, System.String personName, System.Int32 centerId, System.Int32 entityTypeId)
        {
            string confirmationCode = SysUtilities.CreateConfirmationCode(); //Guid.NewGuid().ToString().Replace("-", "");

            SysUsers nUser = new SysUsers();
            Random random = new Random();
            int randomNumber = random.Next(0, 99999) + 100000;
            nUser.UserName = userName;
            nUser.UserPassword = (randomNumber - 50000).ToString();
            nUser.ConfirmationCode = confirmationCode;
            nUser.UserStatus = false;
            nUser.GeneralId = generalId;
            int _userId = nUser.Insert();

            SysUserCenters nUserCenter = new SysUserCenters();
            nUserCenter.UserId = _userId;
            nUserCenter.CenterId = centerId;
            nUserCenter.UserCenterStatus = true;
            int _userCenterId = nUserCenter.Insert();

            if (entityTypeId != (int)GeneralCommon.EntityTypes.Employee)
            {
                SysUserRoles nUserRole = new SysUserRoles();
                nUserRole.UserCenterId = _userCenterId;
                nUserRole.RoleId = SysUtilities.GetRoleIdByEntityTypeId(entityTypeId);
                nUserRole.UserRoleStatus = true;
                nUserRole.Insert();
            }

            //
            //SysCentersInfo cCenter = SysCentersInfo.Get(centerId);

            GenEMails.SignUp(userName, personName, confirmationCode, centerId);

            //GenSentMail nMail = new GenSentMail();
            //nMail.SentTo = userName;
            //nMail.PersonName = personName;
            //nMail.Confirmation = confirmationCode;
            //nMail.MailType = (int)GeneralCommon.MailType.SignUp;
            //nMail.CenterName = cCenter.CenterName;
            //nMail.CenterPhone = cCenter.PhoneNumber;
            //nMail.Sent();

            return _userId;
        }

        public static void UpdateUserName(System.Int32 userId, System.String userName, System.String personName, System.Int32 centerId)
        {
            string confirmationCode = SysUtilities.CreateConfirmationCode();

            SysUsers cUser = SysUsers.Get(userId);
            cUser.UserName = userName;
            cUser.ConfirmationCode = confirmationCode;
            cUser.UserStatus = false;
            cUser.Update();

            GenEMails.SignUp(userName, personName, confirmationCode, centerId);
        }

        public static void ResetPassword(System.Int32 userId)
        {
            SysUsers cUser = SysUsers.Get(userId);
            //call method
            //generar temp pass (method)
            //generar confirmation
            //desactivate user
            //save
            //send msg with confirmation
        }

        public static void ForwardConfirmation(System.Int32 userId)
        {
            SysUsers cUser = SysUsers.Get(userId);
            //call method
            //generar temp pass (method)
            //generar confirmation
            //desactivate user
            //save
            //send msg with confirmation
        }

        public static string GetUserNameByGeneralId(System.Int32 generalId)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUsers_GetUserNameByGeneralId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, generalId);

            return db.ExecuteScalar(dbCommand).ToString();
        }

        public static bool IfExistsUserName(System.String userName, System.Int32? generalId)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUsers_IfExistsUserName";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userName, generalId);

            return Convert.ToBoolean(db.ExecuteScalar(dbCommand));
        }

        public class UserInfo
        {
            public int userId { get; set; }
            //public string userName { get; set; }
            public string fullName { get; set; }
        }

        public static UserInfo GetByUserName(string userName)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            UserInfo instance;
            //IList<UserInfo> llist = new List<UserInfo>();

            instance = new UserInfo();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysUsers_GetUserIdByUserName";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userName);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new UserInfo();
                instance.userId = (int)item["UserId"];
                instance.fullName = (string)item["FullName"];
                //llist.Add(instance);
            }
            return instance;// llist;
        }
        #endregion
    }


}
