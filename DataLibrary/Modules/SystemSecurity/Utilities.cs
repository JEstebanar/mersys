﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using General.GeneralCommons;

namespace SystemSecurity.Utilities
{
    public class SysUtilities
    {
        public static int GetRoleIdByEntityTypeId(int entityTypeId)
        {
            int RoleType = 0;
            switch (entityTypeId)
            {
                case (int)GeneralCommon.EntityTypes.Resident:
                    RoleType = (int)GeneralCommon.SystemRoles.Resident;
                    break;
                case (int)GeneralCommon.EntityTypes.Coordinator:
                    RoleType = (int)GeneralCommon.SystemRoles.Coordinator;
                    break;
                case (int)GeneralCommon.EntityTypes.Administrator:
                    RoleType = (int)GeneralCommon.SystemRoles.Administrator;
                    break;
                case (int)GeneralCommon.EntityTypes.Teacher:
                    RoleType = (int)GeneralCommon.SystemRoles.Teacher;
                    break;
                case (int)GeneralCommon.EntityTypes.HeadTraining:
                    RoleType = (int)GeneralCommon.SystemRoles.HeadTraining;
                    break;
            }
            return RoleType;
            //EvaluationSecretariat = Employee
            //TeachingSecretariant = Employee
        }

        public static GeneralCommon.SystemRoles GetRoleByEntityType(GeneralCommon.EntityTypes entityType)
        {
            GeneralCommon.SystemRoles RoleType = 0;
            switch (entityType)
            {
                case GeneralCommon.EntityTypes.Resident:
                    RoleType = GeneralCommon.SystemRoles.Resident;
                    break;
                case GeneralCommon.EntityTypes.Coordinator:
                    RoleType = GeneralCommon.SystemRoles.Coordinator;
                    break;
                case GeneralCommon.EntityTypes.Administrator:
                    RoleType = GeneralCommon.SystemRoles.Administrator;
                    break;
                case GeneralCommon.EntityTypes.Teacher:
                    RoleType = GeneralCommon.SystemRoles.Teacher;
                    break;
                case GeneralCommon.EntityTypes.HeadTraining:
                    RoleType = GeneralCommon.SystemRoles.HeadTraining;
                    break;
            }
            return RoleType;
            //EvaluationSecretariat = Employee
            //TeachingSecretariant = Employee
        }

        public static string CreateConfirmationCode()
        {
            return Guid.NewGuid().ToString().Replace("-", "");
        }

        //public static string GetModuleName(GeneralCommon.Modules moduleId){
        //    string moduleName =string.Empty;
        //    switch(moduleId)
        //    {
        //        case GeneralCommon.Modules.Residences:
        //            moduleName="Residencias"

            //        Residences = 1,
            //Evaluation = 2,
            //Security = 3,
            //Account = 4,
            //Reportes = 5,
            //Letters = 6,
            //Administration = 7
    }
}
