﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace SystemSecurity.Evaluations
{
    public class SysEvaluations
    {
        public void AllowScoresInMonth(ref GridView grv)
        {

        }

        //si
        public static int GetMonthNumber(string month)
        {
            int monthNumber = 0;
            switch (month)
            {
                case "actionJul":
                    monthNumber = 7;
                    break;
                case "actionAug":
                    monthNumber = 8;
                    break;
                case "actionSep":
                    monthNumber = 9;
                    break;
                case "actionOct":
                    monthNumber = 10;
                    break;
                case "actionNov":
                    monthNumber = 11;
                    break;
                case "actionDec":
                    monthNumber = 12;
                    break;
                case "actionJan":
                    monthNumber = 1;
                    break;
                case "actionFeb":
                    monthNumber = 2;
                    break;
                case "actionMar":
                    monthNumber = 3;
                    break;
                case "actionApr":
                    monthNumber = 4;
                    break;
                case "actionMay":
                    monthNumber = 5;
                    break;
                case "actionJun":
                    monthNumber = 6;
                    break;
            }
            return monthNumber;
        }

        //no
        public static int GetCurrentQuarter1()
        {
            int Q = 0;
            if (DateTime.Now.Month >= 7 && DateTime.Now.Month <= 10)
                Q = 1;
            else if (DateTime.Now.Month >= 11 || DateTime.Now.Month <= 2)
                Q = 2;
            else
                Q = 3;
            return Q;
        }

        //no
        public static int GetCurrentQuarterByMonthNumber1(int monthNumber)
        {
            int Q;
            if (monthNumber >= 7 && monthNumber <= 10)
                Q = 1;
            else if (monthNumber >= 11 || monthNumber <= 2)
                Q = 2;
            else
                Q = 3;
            return Q;
        }

        //no
        //only the month <= actual month
        public static bool AllowToSetScore1(string month)
        {
            if (GetMonthNumber(month) > DateTime.Now.Month && GetMonthNumber(month) < DateTime.Now.Month)
                return false;
            else
                return true;
        }
    }
}
   
