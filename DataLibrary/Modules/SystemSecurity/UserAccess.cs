﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SystemSecurity.UserAccess
{

    [DataObject]
    [Serializable]
    public partial class SysUserAccess
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[sys_UserAccess]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _userId;
        private System.Int32? _userCenterId;
        private System.Int32? _centerId;
        private System.Int32? _teachingYearId;
        private System.DateTime? _startDate;
        private System.DateTime? _endDate;
        private System.String _teachingYear;
        private System.String _fullName;

        #endregion


        #region Properties
        public System.Int32? UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        public System.Int32? UserCenterId
        {
            get
            {
                return _userCenterId;
            }
            set
            {
                _userCenterId = value;
            }
        }

        public System.Int32? CenterId
        {
            get
            {
                return _centerId;
            }
            set
            {
                _centerId = value;
            }
        }

        public System.Int32? TeachingYearId
        {
            get
            {
                return _teachingYearId;
            }
            set
            {
                _teachingYearId = value;
            }
        }

        public System.DateTime? StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value;
            }
        }

        public System.DateTime? EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                _endDate = value;
            }
        }

        public System.String TeachingYear
        {
            get
            {
                return _teachingYear;
            }
            set
            {
                _teachingYear = value;
            }
        }

        public System.String FullName
        {
            get
            {
                return _fullName;
            }
            set
            {
                _fullName = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods
        
        protected void MapFrom(DataRow dr)
        {
            UserId = dr["UserId"] != DBNull.Value ? Convert.ToInt32(dr["UserId"]) : UserId = null;
            UserCenterId = dr["UserCenterId"] != DBNull.Value ? Convert.ToInt32(dr["UserCenterId"]) : UserCenterId = null;
            CenterId = dr["CenterId"] != DBNull.Value ? Convert.ToInt32(dr["CenterId"]) : CenterId = null;
            TeachingYearId = dr["TeachingYearId"] != DBNull.Value ? Convert.ToInt32(dr["TeachingYearId"]) : TeachingYearId = null;
            StartDate = dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : StartDate = null;
            EndDate = dr["EndDate"] != DBNull.Value ? Convert.ToDateTime(dr["EndDate"]) : EndDate = null;
            TeachingYear = dr["TeachingYear"] != DBNull.Value ? Convert.ToString(dr["TeachingYear"]) : TeachingYear = null;
            FullName = dr["FullName"] != DBNull.Value ? Convert.ToString(dr["FullName"]) : FullName = null;
        }

        public static SysUserAccess[] MapFrom(DataSet ds)
        {
            List<SysUserAccess> objects;


            // Initialise Collection.
            objects = new List<SysUserAccess>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[sys_UserAccess] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[sys_UserAccess] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                SysUserAccess instance = new SysUserAccess();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysUserAccess GetAccess(System.Int32 userId, System.Int32 userCenterId, System.Int32 centerId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            SysUserAccess instance;

            instance = new SysUserAccess();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysUserAccess_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userId, userCenterId, centerId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get ID:" + userId.ToString() + " and " + userId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #endregion


        #endregion
    }
}