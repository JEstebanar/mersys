﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SystemSecurity.RoleSecurities
{

    [DataObject]
    [Serializable]
    public partial class SysRoleSecurities
    {

        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[sys_UserRoles]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Custom

        [Serializable]
        public class SecurityInRoles
        {
            public int roleSecurityId { get; set; }
            public int minCharacters { get; set; }
            public int maxCharacters { get; set; }
            public bool allowNumbers { get; set; }
            public bool allowLetters { get; set; }
            public bool allowCapitalLetters { get; set; }
            public int maxAttempts { get; set; }
        }

        public static IList<SecurityInRoles> GetByUserCenterId(int userCenterId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            SecurityInRoles instance;
            IList<SecurityInRoles> llist = new List<SecurityInRoles>();

            instance = new SecurityInRoles();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysRoleSecurity_GetByUserCenterId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new SecurityInRoles();
                instance.minCharacters = (int)item["MinCharacters"];
                instance.maxCharacters = (int)item["MaxCharacters"];
                instance.allowNumbers = Convert.ToBoolean(item["AllowNumbers"]);
                instance.allowLetters = Convert.ToBoolean(item["AllowLetters"]);
                instance.allowCapitalLetters = Convert.ToBoolean(item["AllowCapitalLetters"]);
                instance.maxAttempts = (int)item["MaxAttempts"];
                llist.Add(instance);
            }
            return llist;
        }

        #endregion
    }
}
