﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SystemSecurity.SysError
{

    [DataObject]
    [Serializable]
    public partial class SysErrors
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[sys_Errors]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _errorId;
        private System.String _errorForm;
        private System.String _errorEvent;
        private System.String _errorSource;
        private System.String _errorExceptionMessage;
        private System.String _errorInnerExceptionMessage;
        private System.Int32? _errorUserCenterId;
        private System.DateTime? _errorDate;

        #endregion


        #region Properties
        public System.Int32? ErrorId
        {
            get
            {
                return _errorId;
            }
            set
            {
                _errorId = value;
            }
        }

        public System.String ErrorForm
        {
            get
            {
                return _errorForm;
            }
            set
            {
                _errorForm = value;
            }
        }

        public System.String ErrorEvent
        {
            get
            {
                return _errorEvent;
            }
            set
            {
                _errorEvent = value;
            }
        }

        public System.String ErrorSource
        {
            get
            {
                return _errorSource;
            }
            set
            {
                _errorSource = value;
            }
        }

        public System.String ErrorExceptionMessage
        {
            get
            {
                return _errorExceptionMessage;
            }
            set
            {
                _errorExceptionMessage = value;
            }
        }

        public System.String ErrorInnerExceptionMessage
        {
            get
            {
                return _errorInnerExceptionMessage;
            }
            set
            {
                _errorInnerExceptionMessage = value;
            }
        }

        public System.Int32? ErrorUserCenterId
        {
            get
            {
                return _errorUserCenterId;
            }
            set
            {
                _errorUserCenterId = value;
            }
        }

        public System.DateTime? ErrorDate
        {
            get
            {
                return _errorDate;
            }
            set
            {
                _errorDate = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("ErrorId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ErrorForm", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("ErrorEvent", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("ErrorSource", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("ErrorExceptionMessage", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("ErrorInnerExceptionMessage", typeof(System.String));
            ds.Tables[TABLE_NAME].Columns.Add("ErrorUserCenterId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ErrorDate", typeof(System.DateTime));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (ErrorId == null)
                dr["ErrorId"] = DBNull.Value;
            else
                dr["ErrorId"] = ErrorId;

            if (ErrorForm == null)
                dr["ErrorForm"] = DBNull.Value;
            else
                dr["ErrorForm"] = ErrorForm;

            if (ErrorEvent == null)
                dr["ErrorEvent"] = DBNull.Value;
            else
                dr["ErrorEvent"] = ErrorEvent;

            if (ErrorSource == null)
                dr["ErrorSource"] = DBNull.Value;
            else
                dr["ErrorSource"] = ErrorSource;

            if (ErrorExceptionMessage == null)
                dr["ErrorExceptionMessage"] = DBNull.Value;
            else
                dr["ErrorExceptionMessage"] = ErrorExceptionMessage;

            if (ErrorInnerExceptionMessage == null)
                dr["ErrorInnerExceptionMessage"] = DBNull.Value;
            else
                dr["ErrorInnerExceptionMessage"] = ErrorInnerExceptionMessage;

            if (ErrorUserCenterId == null)
                dr["ErrorUserCenterId"] = DBNull.Value;
            else
                dr["ErrorUserCenterId"] = ErrorUserCenterId;

            if (ErrorDate == null)
                dr["ErrorDate"] = DBNull.Value;
            else
                dr["ErrorDate"] = ErrorDate;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            ErrorId = dr["ErrorId"] != DBNull.Value ? Convert.ToInt32(dr["ErrorId"]) : ErrorId = null;
            ErrorForm = dr["ErrorForm"] != DBNull.Value ? Convert.ToString(dr["ErrorForm"]) : ErrorForm = null;
            ErrorEvent = dr["ErrorEvent"] != DBNull.Value ? Convert.ToString(dr["ErrorEvent"]) : ErrorEvent = null;
            ErrorSource = dr["ErrorSource"] != DBNull.Value ? Convert.ToString(dr["ErrorSource"]) : ErrorSource = null;
            ErrorExceptionMessage = dr["ErrorExceptionMessage"] != DBNull.Value ? Convert.ToString(dr["ErrorExceptionMessage"]) : ErrorExceptionMessage = null;
            ErrorInnerExceptionMessage = dr["ErrorInnerExceptionMessage"] != DBNull.Value ? Convert.ToString(dr["ErrorInnerExceptionMessage"]) : ErrorInnerExceptionMessage = null;
            ErrorUserCenterId = dr["ErrorUserCenterId"] != DBNull.Value ? Convert.ToInt32(dr["ErrorUserCenterId"]) : ErrorUserCenterId = null;
            ErrorDate = dr["ErrorDate"] != DBNull.Value ? Convert.ToDateTime(dr["ErrorDate"]) : ErrorDate = null;
        }

        public static SysErrors[] MapFrom(DataSet ds)
        {
            List<SysErrors> objects;


            // Initialise Collection.
            objects = new List<SysErrors>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[sys_Errors] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[sys_Errors] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                SysErrors instance = new SysErrors();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysErrors Get(System.Int32 errorId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            SysErrors instance;


            instance = new SysErrors();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysErrors_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, errorId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysErrors ID:" + errorId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.String errorForm, System.String errorEvent, System.String errorSource, System.String errorExceptionMessage, System.String errorInnerExceptionMessage, System.Int32? errorUserCenterId, System.DateTime? errorDate, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysErrors_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, errorForm, errorEvent, errorSource, errorExceptionMessage, errorInnerExceptionMessage, errorUserCenterId, errorDate);

            if (transaction == null)
                this.ErrorId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.ErrorId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.String errorForm, System.String errorEvent, System.String errorSource, System.String errorExceptionMessage, System.String errorInnerExceptionMessage, System.Int32? errorUserCenterId, System.DateTime? errorDate)
        {
            Insert(errorForm, errorEvent, errorSource, errorExceptionMessage, errorInnerExceptionMessage, errorUserCenterId, errorDate, null);
        }
        /// <summary>
        /// Insert current SysErrors to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(ErrorForm, ErrorEvent, ErrorSource, ErrorExceptionMessage, ErrorInnerExceptionMessage, ErrorUserCenterId, ErrorDate, transaction);
        }

        /// <summary>
        /// Insert current SysErrors to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? errorId, System.String errorForm, System.String errorEvent, System.String errorSource, System.String errorExceptionMessage, System.String errorInnerExceptionMessage, System.Int32? errorUserCenterId, System.DateTime? errorDate, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysErrors_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@errorId"].Value = errorId;
            dbCommand.Parameters["@errorForm"].Value = errorForm;
            dbCommand.Parameters["@errorEvent"].Value = errorEvent;
            dbCommand.Parameters["@errorSource"].Value = errorSource;
            dbCommand.Parameters["@errorExceptionMessage"].Value = errorExceptionMessage;
            dbCommand.Parameters["@errorInnerExceptionMessage"].Value = errorInnerExceptionMessage;
            dbCommand.Parameters["@errorUserCenterId"].Value = errorUserCenterId;
            dbCommand.Parameters["@errorDate"].Value = errorDate;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? errorId, System.String errorForm, System.String errorEvent, System.String errorSource, System.String errorExceptionMessage, System.String errorInnerExceptionMessage, System.Int32? errorUserCenterId, System.DateTime? errorDate)
        {
            Update(errorId, errorForm, errorEvent, errorSource, errorExceptionMessage, errorInnerExceptionMessage, errorUserCenterId, errorDate, null);
        }

        public static void Update(SysErrors sysErrors)
        {
            sysErrors.Update();
        }

        public static void Update(SysErrors sysErrors, DbTransaction transaction)
        {
            sysErrors.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysErrors_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@errorId"].SourceColumn = "ErrorId";
            dbCommand.Parameters["@errorForm"].SourceColumn = "ErrorForm";
            dbCommand.Parameters["@errorEvent"].SourceColumn = "ErrorEvent";
            dbCommand.Parameters["@errorSource"].SourceColumn = "ErrorSource";
            dbCommand.Parameters["@errorExceptionMessage"].SourceColumn = "ErrorExceptionMessage";
            dbCommand.Parameters["@errorInnerExceptionMessage"].SourceColumn = "ErrorInnerExceptionMessage";
            dbCommand.Parameters["@errorUserCenterId"].SourceColumn = "ErrorUserCenterId";
            dbCommand.Parameters["@errorDate"].SourceColumn = "ErrorDate";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? errorId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysErrors_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, errorId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? errorId)
        {
            Delete(
            errorId);
        }

        /// <summary>
        /// Delete current SysErrors from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysErrors_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, ErrorId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.ErrorId = null;
        }

        /// <summary>
        /// Delete current SysErrors from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysErrors[] Search(System.Int32? errorId, System.String errorForm, System.String errorEvent, System.String errorSource, System.String errorExceptionMessage, System.String errorInnerExceptionMessage, System.Int32? errorUserCenterId, System.DateTime? errorDate)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysErrors_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, errorId, errorForm, errorEvent, errorSource, errorExceptionMessage, errorInnerExceptionMessage, errorUserCenterId, errorDate);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return SysErrors.MapFrom(ds);
        }


        public static SysErrors[] Search(SysErrors searchObject)
        {
            return Search(searchObject.ErrorId, searchObject.ErrorForm, searchObject.ErrorEvent, searchObject.ErrorSource, searchObject.ErrorExceptionMessage, searchObject.ErrorInnerExceptionMessage, searchObject.ErrorUserCenterId, searchObject.ErrorDate);
        }

        /// <summary>
        /// Returns all SysErrors objects.
        /// </summary>
        /// <returns>List of all SysErrors objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static SysErrors[] Search()
        {
            return Search(null, null, null, null, null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


        #region Custom
        public static void SetSystemError(System.String errorForm, System.String errorEvent, System.Int32? userCenterId, ref Exception ex)
        {
            if (userCenterId != null)
            {
                SysErrors nError = new SysErrors();

                nError.ErrorForm = errorForm;
                nError.ErrorEvent = errorEvent;
                nError.ErrorSource = ex.Source;
                nError.ErrorExceptionMessage = ex.Message;
                nError.ErrorUserCenterId = userCenterId;
                nError.ErrorDate = DateTime.Now;

                if (ex.InnerException != null)
                    nError.ErrorInnerExceptionMessage = ex.InnerException.Message;

                nError.Insert();
            }
        }
        #endregion
    }
}