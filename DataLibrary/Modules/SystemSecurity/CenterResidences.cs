﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SystemSecurity.CenterResidences
{

    [DataObject]
    [Serializable]
    public partial class SysCenterResidences
    {
        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[sys_CenterResidences]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _centerResidenceId;
        private System.Int32? _centerId;
        private System.Int32? _residenceId;
        private System.Boolean? _centerResidenceStatus;
        private System.Int32? _teachingYearId;

        #endregion


        #region Properties
        public System.Int32? CenterResidenceId
        {
            get
            {
                return _centerResidenceId;
            }
            set
            {
                _centerResidenceId = value;
            }
        }

        public System.Int32? CenterId
        {
            get
            {
                return _centerId;
            }
            set
            {
                _centerId = value;
            }
        }

        public System.Int32? ResidenceId
        {
            get
            {
                return _residenceId;
            }
            set
            {
                _residenceId = value;
            }
        }

        public System.Boolean? CenterResidenceStatus
        {
            get
            {
                return _centerResidenceStatus;
            }
            set
            {
                _centerResidenceStatus = value;
            }
        }

        public System.Int32? TeachingYearId
        {
            get
            {
                return _teachingYearId;
            }
            set
            {
                _teachingYearId = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("CenterResidenceId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("CenterId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ResidenceId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("CenterResidenceStatus", typeof(System.Boolean));
            ds.Tables[TABLE_NAME].Columns.Add("TeachingYearId", typeof(System.Int32));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (CenterResidenceId == null)
                dr["CenterResidenceId"] = DBNull.Value;
            else
                dr["CenterResidenceId"] = CenterResidenceId;

            if (CenterId == null)
                dr["CenterId"] = DBNull.Value;
            else
                dr["CenterId"] = CenterId;

            if (ResidenceId == null)
                dr["ResidenceId"] = DBNull.Value;
            else
                dr["ResidenceId"] = ResidenceId;

            if (CenterResidenceStatus == null)
                dr["CenterResidenceStatus"] = DBNull.Value;
            else
                dr["CenterResidenceStatus"] = CenterResidenceStatus;

            if (TeachingYearId == null)
                dr["TeachingYearId"] = DBNull.Value;
            else
                dr["TeachingYearId"] = TeachingYearId;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            CenterResidenceId = dr["CenterResidenceId"] != DBNull.Value ? Convert.ToInt32(dr["CenterResidenceId"]) : CenterResidenceId = null;
            CenterId = dr["CenterId"] != DBNull.Value ? Convert.ToInt32(dr["CenterId"]) : CenterId = null;
            ResidenceId = dr["ResidenceId"] != DBNull.Value ? Convert.ToInt32(dr["ResidenceId"]) : ResidenceId = null;
            CenterResidenceStatus = dr["CenterResidenceStatus"] != DBNull.Value ? Convert.ToBoolean(dr["CenterResidenceStatus"]) : CenterResidenceStatus = null;
            TeachingYearId = dr["TeachingYearId"] != DBNull.Value ? Convert.ToInt32(dr["TeachingYearId"]) : TeachingYearId = null;
        }

        public static SysCenterResidences[] MapFrom(DataSet ds)
        {
            List<SysCenterResidences> objects;


            // Initialise Collection.
            objects = new List<SysCenterResidences>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[sys_CenterResidences] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[sys_CenterResidences] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                SysCenterResidences instance = new SysCenterResidences();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysCenterResidences Get(System.Int32 centerResidenceId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            SysCenterResidences instance;


            instance = new SysCenterResidences();

            db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "[dbo].SysCenterResidences_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, centerResidenceId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysCenterResidences ID:" + centerResidenceId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.Int32? centerId, System.Int32? residenceId, System.Boolean? centerResidenceStatus, System.Int32? teachingYearId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "[dbo].SysCenterResidences_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, centerId, residenceId, centerResidenceStatus, teachingYearId);

            if (transaction == null)
                this.CenterResidenceId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.CenterResidenceId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.Int32? centerId, System.Int32? residenceId, System.Boolean? centerResidenceStatus, System.Int32? teachingYearId)
        {
            Insert(centerId, residenceId, centerResidenceStatus, teachingYearId, null);
        }
        /// <summary>
        /// Insert current SysCenterResidences to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(CenterId, ResidenceId, CenterResidenceStatus, TeachingYearId, transaction);
        }

        /// <summary>
        /// Insert current SysCenterResidences to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? centerResidenceId, System.Int32? centerId, System.Int32? residenceId, System.Boolean? centerResidenceStatus, System.Int32? teachingYearId, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "[dbo].SysCenterResidences_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@centerResidenceId"].Value = centerResidenceId;
            dbCommand.Parameters["@centerId"].Value = centerId;
            dbCommand.Parameters["@residenceId"].Value = residenceId;
            dbCommand.Parameters["@centerResidenceStatus"].Value = centerResidenceStatus;
            dbCommand.Parameters["@teachingYearId"].Value = teachingYearId;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? centerResidenceId, System.Int32? centerId, System.Int32? residenceId, System.Boolean? centerResidenceStatus, System.Int32? teachingYearId)
        {
            Update(centerResidenceId, centerId, residenceId, centerResidenceStatus, teachingYearId, null);
        }

        public static void Update(SysCenterResidences sysCenterResidences)
        {
            sysCenterResidences.Update();
        }

        public static void Update(SysCenterResidences sysCenterResidences, DbTransaction transaction)
        {
            sysCenterResidences.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "[dbo].SysCenterResidences_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@centerResidenceId"].SourceColumn = "CenterResidenceId";
            dbCommand.Parameters["@centerId"].SourceColumn = "CenterId";
            dbCommand.Parameters["@residenceId"].SourceColumn = "ResidenceId";
            dbCommand.Parameters["@centerResidenceStatus"].SourceColumn = "CenterResidenceStatus";
            dbCommand.Parameters["@teachingYearId"].SourceColumn = "TeachingYearId";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? centerResidenceId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "[dbo].SysCenterResidences_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, centerResidenceId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? centerResidenceId)
        {
            Delete(
            centerResidenceId);
        }

        /// <summary>
        /// Delete current SysCenterResidences from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "[dbo].SysCenterResidences_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, CenterResidenceId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.CenterResidenceId = null;
        }

        /// <summary>
        /// Delete current SysCenterResidences from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysCenterResidences[] Search(System.Int32? centerResidenceId, System.Int32? centerId, System.Int32? residenceId, System.Boolean? centerResidenceStatus, System.Int32? teachingYearId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "[dbo].SysCenterResidences_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, centerResidenceId, centerId, residenceId, centerResidenceStatus, teachingYearId);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return SysCenterResidences.MapFrom(ds);
        }


        public static SysCenterResidences[] Search(SysCenterResidences searchObject)
        {
            return Search(searchObject.CenterResidenceId, searchObject.CenterId, searchObject.ResidenceId, searchObject.CenterResidenceStatus, searchObject.TeachingYearId);
        }

        /// <summary>
        /// Returns all SysCenterResidences objects.
        /// </summary>
        /// <returns>List of all SysCenterResidences objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static SysCenterResidences[] Search()
        {
            return Search(null, null, null, null, null);
        }

        #endregion


        #endregion


        #endregion

        #region Custom
        [Serializable]
        public class CenterResidences
        {
            public int userCenterResidenceId { get; set; }
            public int userRoleId { get; set; }
            public int centerResidenceId { get; set; }
            public string preResidenceName { get; set; }
            public string residenceName { get; set; }
            public bool userResidenceStatus { get; set; }
        }

        public static IList<CenterResidences> GetByParentId(int parentId, int centerId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            CenterResidences instance;
            IList<CenterResidences> llist = new List<CenterResidences>();

            instance = new CenterResidences();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "ResResidences_GetbyParentId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, parentId, centerId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new CenterResidences();
                instance.centerResidenceId = (int)item["CenterResidenceId"];
                llist.Add(instance);
            }
            return llist;
        }
        #endregion
    }
}
