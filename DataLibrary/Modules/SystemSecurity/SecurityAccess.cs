﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using SystemSecurity.Screens;
using General.GeneralCommons;

namespace SystemSecurity.SecurityAccess
{
    public partial class SysSecurityAccess
    {

        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[sys_UserRoles]";
        private static readonly string DataConnection = "DataConnection";

        #endregion

        #region Custom
        ////old version 
        //public static bool UserAccess(System.Int32 userCenterId, System.Int32 screenId)
        //{
        //    Database db;
        //    string sqlCommand;
        //    DbCommand dbCommand;

        //    db = DatabaseFactory.CreateDatabase(DataConnection);
        //    sqlCommand = "[dbo].SysScreens_GetAccess";
        //    dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterId, screenId);

        //    return Convert.ToBoolean(db.ExecuteScalar(dbCommand));
        //}

        //new
        public static bool UserAccess(List<SysScreens.Screens> screenAccess, GeneralCommon.SystemScreens screenId)
        {
            bool YesOrNot = false;
            if (screenAccess != null)
            {
                foreach (SysScreens.Screens screenInList in screenAccess)
                {
                    if (screenInList.screenId == (int)screenId)
                        YesOrNot = true;
                }
            }
            else
                YesOrNot = true;

            return YesOrNot;
        }

        public static int ConfirmationCode(System.String validatioCode)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUsers_ConfirmationCode";
            dbCommand = db.GetStoredProcCommand(sqlCommand, validatioCode);

            return Convert.ToInt32(db.ExecuteScalar(dbCommand));
        }

        public static void SetUserActive(System.Int32 userId)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUsers_SetUserActive";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userId);

            db.ExecuteScalar(dbCommand);
        }

        public static DateTime GetCurrentSQLServerDateTime()
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].GetCurrentDateTime";
            dbCommand = db.GetStoredProcCommand(sqlCommand);

            return Convert.ToDateTime(db.ExecuteScalar(dbCommand));
        }
        #endregion
    }
}
