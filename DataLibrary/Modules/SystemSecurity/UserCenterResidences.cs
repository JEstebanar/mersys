﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SystemSecurity.UserCenterResidences
{

    [DataObject]
    [Serializable]
    public partial class SysUserCenterResidences
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[sys_UserCenterResidences]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _userCenterResidenceId;
        private System.Int32? _userRoleId;
        private System.Int32? _centerResidenceId;
        private System.Boolean? _userResidenceStatus;

        #endregion


        #region Properties
        public System.Int32? UserCenterResidenceId
        {
            get
            {
                return _userCenterResidenceId;
            }
            set
            {
                _userCenterResidenceId = value;
            }
        }

        public System.Int32? UserRoleId
        {
            get
            {
                return _userRoleId;
            }
            set
            {
                _userRoleId = value;
            }
        }

        public System.Int32? CenterResidenceId
        {
            get
            {
                return _centerResidenceId;
            }
            set
            {
                _centerResidenceId = value;
            }
        }

        public System.Boolean? UserResidenceStatus
        {
            get
            {
                return _userResidenceStatus;
            }
            set
            {
                _userResidenceStatus = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("UserCenterResidenceId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("UserRoleId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("CenterResidenceId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("UserResidenceStatus", typeof(System.Boolean));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (UserCenterResidenceId == null)
                dr["UserCenterResidenceId"] = DBNull.Value;
            else
                dr["UserCenterResidenceId"] = UserCenterResidenceId;

            if (UserRoleId == null)
                dr["UserRoleId"] = DBNull.Value;
            else
                dr["UserRoleId"] = UserRoleId;

            if (CenterResidenceId == null)
                dr["CenterResidenceId"] = DBNull.Value;
            else
                dr["CenterResidenceId"] = CenterResidenceId;

            if (UserResidenceStatus == null)
                dr["UserResidenceStatus"] = DBNull.Value;
            else
                dr["UserResidenceStatus"] = UserResidenceStatus;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            UserCenterResidenceId = dr["UserCenterResidenceId"] != DBNull.Value ? Convert.ToInt32(dr["UserCenterResidenceId"]) : UserCenterResidenceId = null;
            UserRoleId = dr["UserRoleId"] != DBNull.Value ? Convert.ToInt32(dr["UserRoleId"]) : UserRoleId = null;
            CenterResidenceId = dr["CenterResidenceId"] != DBNull.Value ? Convert.ToInt32(dr["CenterResidenceId"]) : CenterResidenceId = null;
            UserResidenceStatus = dr["UserResidenceStatus"] != DBNull.Value ? Convert.ToBoolean(dr["UserResidenceStatus"]) : UserResidenceStatus = null;
        }

        public static SysUserCenterResidences[] MapFrom(DataSet ds)
        {
            List<SysUserCenterResidences> objects;


            // Initialise Collection.
            objects = new List<SysUserCenterResidences>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[sys_UserCenterResidences] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[sys_UserCenterResidences] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                SysUserCenterResidences instance = new SysUserCenterResidences();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysUserCenterResidences Get(System.Int32 userCenterResidenceId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            SysUserCenterResidences instance;


            instance = new SysUserCenterResidences();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenterResidences_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterResidenceId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysUserCenterResidences ID:" + userCenterResidenceId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.Int32? userRoleId, System.Int32? centerResidenceId, System.Boolean? userResidenceStatus, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenterResidences_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userRoleId, centerResidenceId, userResidenceStatus);

            if (transaction == null)
                this.UserCenterResidenceId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.UserCenterResidenceId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.Int32? userRoleId, System.Int32? centerResidenceId, System.Boolean? userResidenceStatus)
        {
            Insert(userRoleId, centerResidenceId, userResidenceStatus, null);
        }
        /// <summary>
        /// Insert current SysUserCenterResidences to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(UserRoleId, CenterResidenceId, UserResidenceStatus, transaction);
        }

        /// <summary>
        /// Insert current SysUserCenterResidences to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? userCenterResidenceId, System.Int32? userRoleId, System.Int32? centerResidenceId, System.Boolean? userResidenceStatus, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenterResidences_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@userCenterResidenceId"].Value = userCenterResidenceId;
            dbCommand.Parameters["@userRoleId"].Value = userRoleId;
            dbCommand.Parameters["@centerResidenceId"].Value = centerResidenceId;
            dbCommand.Parameters["@userResidenceStatus"].Value = userResidenceStatus;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? userCenterResidenceId, System.Int32? userRoleId, System.Int32? centerResidenceId, System.Boolean? userResidenceStatus)
        {
            Update(userCenterResidenceId, userRoleId, centerResidenceId, userResidenceStatus, null);
        }

        public static void Update(SysUserCenterResidences sysUserCenterResidences)
        {
            sysUserCenterResidences.Update();
        }

        public static void Update(SysUserCenterResidences sysUserCenterResidences, DbTransaction transaction)
        {
            sysUserCenterResidences.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenterResidences_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@userCenterResidenceId"].SourceColumn = "UserCenterResidenceId";
            dbCommand.Parameters["@userRoleId"].SourceColumn = "UserRoleId";
            dbCommand.Parameters["@centerResidenceId"].SourceColumn = "CenterResidenceId";
            dbCommand.Parameters["@userResidenceStatus"].SourceColumn = "UserResidenceStatus";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? userCenterResidenceId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenterResidences_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterResidenceId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? userCenterResidenceId)
        {
            Delete(
            userCenterResidenceId);
        }

        /// <summary>
        /// Delete current SysUserCenterResidences from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenterResidences_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, UserCenterResidenceId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.UserCenterResidenceId = null;
        }

        /// <summary>
        /// Delete current SysUserCenterResidences from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysUserCenterResidences[] Search(System.Int32? userCenterResidenceId, System.Int32? userRoleId, System.Int32? centerResidenceId, System.Boolean? userResidenceStatus)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenterResidences_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterResidenceId, userRoleId, centerResidenceId, userResidenceStatus);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return SysUserCenterResidences.MapFrom(ds);
        }


        public static SysUserCenterResidences[] Search(SysUserCenterResidences searchObject)
        {
            return Search(searchObject.UserCenterResidenceId, searchObject.UserRoleId, searchObject.CenterResidenceId, searchObject.UserResidenceStatus);
        }

        /// <summary>
        /// Returns all SysUserCenterResidences objects.
        /// </summary>
        /// <returns>List of all SysUserCenterResidences objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static SysUserCenterResidences[] Search()
        {
            return Search(null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


        #region Custom

        [Serializable]
        public class UserCenterResidences
        {
            public int userCenterResidenceId { get; set; }
            public int userRoleId { get; set; }
            public int centerResidenceId { get; set; }
            public string preResidenceName { get; set; }
            public string residenceName { get; set; }
            public bool userResidenceStatus { get; set; }
        }

        public static IList<UserCenterResidences> GetByUserId(int userId, int teachingYearId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            UserCenterResidences instance;
            IList<UserCenterResidences> llist = new List<UserCenterResidences>();

            instance = new UserCenterResidences();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysUserCenterResidences_GetByUserId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userId, teachingYearId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new UserCenterResidences();
                instance.userCenterResidenceId = (int)item["UserCenterResidenceId"];
                instance.userRoleId = (int)item["UserRoleId"];
                instance.centerResidenceId = (int)item["CenterResidenceId"];
                instance.preResidenceName = (string)item["PreResidenceName"];
                instance.residenceName = (string)item["ResidenceName"];
                instance.userResidenceStatus = Convert.ToBoolean(item["UserResidenceStatus"]);
                llist.Add(instance);
            }
            return llist;
        }

        [Serializable]
        public class UserCenterResidencesList
        {
            public int residenceId { get; set; }
            public string residenceName { get; set; }
        }

        //sin uso//
        public static IList<UserCenterResidencesList> GetByUserCenterId(int userCenterId, int teachingYearId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            UserCenterResidencesList instance;
            IList<UserCenterResidencesList> llist = new List<UserCenterResidencesList>();

            instance = new UserCenterResidencesList();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysUserCenterResidences_GetByUserCenterId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterId, teachingYearId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new UserCenterResidencesList();
                instance.residenceId = (int)item["ResidenceId"];
                instance.residenceName = (string)item["ResidenceName"];
                llist.Add(instance);
            }
            return llist;
        }

        public static IList<UserCenterResidences> GetResidences(int centerId, int teachingYearId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            UserCenterResidences instance;
            IList<UserCenterResidences> llist = new List<UserCenterResidences>();

            instance = new UserCenterResidences();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysUserCenterResidences_GetResidences";
            dbCommand = db.GetStoredProcCommand(sqlCommand, centerId, teachingYearId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new UserCenterResidences();
                instance.userCenterResidenceId = (int)item["UserCenterResidenceId"];
                instance.userRoleId = (int)item["UserRoleId"];
                instance.centerResidenceId = (int)item["CenterResidenceId"];
                instance.preResidenceName = (string)item["PreResidenceName"];
                instance.residenceName = (string)item["ResidenceName"];
                instance.userResidenceStatus = Convert.ToBoolean(item["UserResidenceStatus"]);
                llist.Add(instance);
            }
            return llist;
        }

        //public static void IfExists(System.Int32? userCenterResidenceId, System.Int32? userRoleId, System.Int32? centerResidenceId, System.Boolean? userResidenceStatus)
        public static void IfExists(System.Int32? userId, System.Int32? centerId, System.Int32? centerResidenceId)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenterResidences_IfExists";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userId, centerId, centerResidenceId);

            db.ExecuteScalar(dbCommand);

            return;
        }

        public static int GetParentByUserId(System.Int32? userId, System.Int32 centerId)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenterResidences_GetParentByUserId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userId, centerId);

            return Convert.ToInt32(db.ExecuteScalar(dbCommand));
        }
        #endregion

    }

}