﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SystemSecurity.RoleScreens
{
    [DataObject]
    [Serializable]
    public partial class SysRoleScreens
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[sys_RoleScreens]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _roleScreenId;
        private System.Int32? _roleId;
        private System.Int32? _screenId;
        private System.Boolean? _roleScreenStatus;

        #endregion


        #region Properties
        public System.Int32? RoleScreenId
        {
            get
            {
                return _roleScreenId;
            }
            set
            {
                _roleScreenId = value;
            }
        }

        public System.Int32? RoleId
        {
            get
            {
                return _roleId;
            }
            set
            {
                _roleId = value;
            }
        }

        public System.Int32? ScreenId
        {
            get
            {
                return _screenId;
            }
            set
            {
                _screenId = value;
            }
        }

        public System.Boolean? RoleScreenStatus
        {
            get
            {
                return _roleScreenStatus;
            }
            set
            {
                _roleScreenStatus = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("RoleScreenId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("RoleId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ScreenId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("RoleScreenStatus", typeof(System.Boolean));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (RoleScreenId == null)
                dr["RoleScreenId"] = DBNull.Value;
            else
                dr["RoleScreenId"] = RoleScreenId;

            if (RoleId == null)
                dr["RoleId"] = DBNull.Value;
            else
                dr["RoleId"] = RoleId;

            if (ScreenId == null)
                dr["ScreenId"] = DBNull.Value;
            else
                dr["ScreenId"] = ScreenId;

            if (RoleScreenStatus == null)
                dr["RoleScreenStatus"] = DBNull.Value;
            else
                dr["RoleScreenStatus"] = RoleScreenStatus;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            RoleScreenId = dr["RoleScreenId"] != DBNull.Value ? Convert.ToInt32(dr["RoleScreenId"]) : RoleScreenId = null;
            RoleId = dr["RoleId"] != DBNull.Value ? Convert.ToInt32(dr["RoleId"]) : RoleId = null;
            ScreenId = dr["ScreenId"] != DBNull.Value ? Convert.ToInt32(dr["ScreenId"]) : ScreenId = null;
            RoleScreenStatus = dr["RoleScreenStatus"] != DBNull.Value ? Convert.ToBoolean(dr["RoleScreenStatus"]) : RoleScreenStatus = null;
        }

        public static SysRoleScreens[] MapFrom(DataSet ds)
        {
            List<SysRoleScreens> objects;


            // Initialise Collection.
            objects = new List<SysRoleScreens>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[sys_RoleScreens] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[sys_RoleScreens] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                SysRoleScreens instance = new SysRoleScreens();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysRoleScreens Get(System.Int32 roleScreenId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            SysRoleScreens instance;


            instance = new SysRoleScreens();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysRoleScreens_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, roleScreenId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysRoleScreens ID:" + roleScreenId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.Int32? roleId, System.Int32? screenId, System.Boolean? roleScreenStatus, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysRoleScreens_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, roleId, screenId, roleScreenStatus);

            if (transaction == null)
                this.RoleScreenId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.RoleScreenId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.Int32? roleId, System.Int32? screenId, System.Boolean? roleScreenStatus)
        {
            Insert(roleId, screenId, roleScreenStatus, null);
        }
        /// <summary>
        /// Insert current SysRoleScreens to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(RoleId, ScreenId, RoleScreenStatus, transaction);
        }

        /// <summary>
        /// Insert current SysRoleScreens to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? roleScreenId, System.Int32? roleId, System.Int32? screenId, System.Boolean? roleScreenStatus, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysRoleScreens_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@roleScreenId"].Value = roleScreenId;
            dbCommand.Parameters["@roleId"].Value = roleId;
            dbCommand.Parameters["@screenId"].Value = screenId;
            dbCommand.Parameters["@roleScreenStatus"].Value = roleScreenStatus;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? roleScreenId, System.Int32? roleId, System.Int32? screenId, System.Boolean? roleScreenStatus)
        {
            Update(roleScreenId, roleId, screenId, roleScreenStatus, null);
        }

        public static void Update(SysRoleScreens sysRoleScreens)
        {
            sysRoleScreens.Update();
        }

        public static void Update(SysRoleScreens sysRoleScreens, DbTransaction transaction)
        {
            sysRoleScreens.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysRoleScreens_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@roleScreenId"].SourceColumn = "RoleScreenId";
            dbCommand.Parameters["@roleId"].SourceColumn = "RoleId";
            dbCommand.Parameters["@screenId"].SourceColumn = "ScreenId";
            dbCommand.Parameters["@roleScreenStatus"].SourceColumn = "RoleScreenStatus";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? roleScreenId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysRoleScreens_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, roleScreenId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? roleScreenId)
        {
            Delete(
            roleScreenId);
        }

        /// <summary>
        /// Delete current SysRoleScreens from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysRoleScreens_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, RoleScreenId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.RoleScreenId = null;
        }

        /// <summary>
        /// Delete current SysRoleScreens from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysRoleScreens[] Search(System.Int32? roleScreenId, System.Int32? roleId, System.Int32? screenId, System.Boolean? roleScreenStatus)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysRoleScreens_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, roleScreenId, roleId, screenId, roleScreenStatus);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return SysRoleScreens.MapFrom(ds);
        }


        public static SysRoleScreens[] Search(SysRoleScreens searchObject)
        {
            return Search(searchObject.RoleScreenId, searchObject.RoleId, searchObject.ScreenId, searchObject.RoleScreenStatus);
        }

        /// <summary>
        /// Returns all SysRoleScreens objects.
        /// </summary>
        /// <returns>List of all SysRoleScreens objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static SysRoleScreens[] Search()
        {
            return Search(null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


        #region Custom
        [Serializable]
        public class RoleScreens
        {
            public int screenId { get; set; }
            public string screenName { get; set; }
            public bool assigned { get; set; }
        }

        public static IList<RoleScreens> GetByRoleId(int roleId, int moduleId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            RoleScreens instance;
            IList<RoleScreens> llist = new List<RoleScreens>();

            instance = new RoleScreens();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysRoleScreens_GetByRoleId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, roleId, moduleId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new RoleScreens();
                instance.screenId = (int)item["ScreenId"];
                instance.screenName = (string)item["ScreenName"];
                instance.assigned = (bool)item["Assigned"];
                llist.Add(instance);
            }
            return llist;
        }

        public void InsertOrUpdate(System.Int32? roleId, System.Int32? screenId, System.Boolean? roleScreenStatus, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysRoleScreens_InsertOrUpdate";
            dbCommand = db.GetStoredProcCommand(sqlCommand, roleId, screenId, roleScreenStatus);

            if (transaction == null)
                db.ExecuteScalar(dbCommand);
            else
                db.ExecuteScalar(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void InsertOrUpdate(System.Int32? roleId, System.Int32? screenId, System.Boolean? roleScreenStatus)
        {
            InsertOrUpdate(roleId, screenId, roleScreenStatus, null);
        }
        /// <summary>
        /// Insert current SysCenterScreens to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void InsertOrUpdate(DbTransaction transaction)
        {
            InsertOrUpdate(RoleId, ScreenId, RoleScreenStatus, transaction);
        }

        /// <summary>
        /// Insert current SysCenterScreens to database.
        /// </summary>
        public void InsertOrUpdate()
        {
            this.InsertOrUpdate((DbTransaction)null);
        }
        #endregion
    }
}