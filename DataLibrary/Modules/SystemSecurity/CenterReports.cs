﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SystemSecurity.CenterReports
{
    [DataObject]
    [Serializable]
    public partial class SysCenterReports
    {

        #region Fields
        private System.Int32? _centerReportId;
        private System.Int32? _centerId;
        private System.Int32? _reportId;
        private System.Boolean? _centerReportStatus;

        #endregion


        #region Properties
        public System.Int32? CenterReportId
        {
            get
            {
                return _centerReportId;
            }
            set
            {
                _centerReportId = value;
            }
        }

        public System.Int32? CenterId
        {
            get
            {
                return _centerId;
            }
            set
            {
                _centerId = value;
            }
        }

        public System.Int32? ReportId
        {
            get
            {
                return _reportId;
            }
            set
            {
                _reportId = value;
            }
        }

        public System.Boolean? CenterReportStatus
        {
            get
            {
                return _centerReportStatus;
            }
            set
            {
                _centerReportStatus = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("CenterReportId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("CenterId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("ReportId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("CenterReportStatus", typeof(System.Boolean));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (CenterReportId == null)
                dr["CenterReportId"] = DBNull.Value;
            else
                dr["CenterReportId"] = CenterReportId;

            if (CenterId == null)
                dr["CenterId"] = DBNull.Value;
            else
                dr["CenterId"] = CenterId;

            if (ReportId == null)
                dr["ReportId"] = DBNull.Value;
            else
                dr["ReportId"] = ReportId;

            if (CenterReportStatus == null)
                dr["CenterReportStatus"] = DBNull.Value;
            else
                dr["CenterReportStatus"] = CenterReportStatus;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            CenterReportId = dr["CenterReportId"] != DBNull.Value ? Convert.ToInt32(dr["CenterReportId"]) : CenterReportId = null;
            CenterId = dr["CenterId"] != DBNull.Value ? Convert.ToInt32(dr["CenterId"]) : CenterId = null;
            ReportId = dr["ReportId"] != DBNull.Value ? Convert.ToInt32(dr["ReportId"]) : ReportId = null;
            CenterReportStatus = dr["CenterReportStatus"] != DBNull.Value ? Convert.ToBoolean(dr["CenterReportStatus"]) : CenterReportStatus = null;
        }

        public static SysCenterReports[] MapFrom(DataSet ds)
        {
            List<SysCenterReports> objects;


            // Initialise Collection.
            objects = new List<SysCenterReports>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[sys_CenterReports] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[sys_CenterReports] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                SysCenterReports instance = new SysCenterReports();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysCenterReports Get(System.Int32 centerReportId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            SysCenterReports instance;


            instance = new SysCenterReports();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysCenterReports_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, centerReportId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysCenterReports ID:" + centerReportId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public void Insert(System.Int32? centerId, System.Int32? reportId, System.Boolean? centerReportStatus, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysCenterReports_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, centerId, reportId, centerReportStatus);

            if (transaction == null)
                this.CenterReportId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.CenterReportId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void Insert(System.Int32? centerId, System.Int32? reportId, System.Boolean? centerReportStatus)
        {
            Insert(centerId, reportId, centerReportStatus, null);
        }
        /// <summary>
        /// Insert current SysCenterReports to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Insert(DbTransaction transaction)
        {
            Insert(CenterId, ReportId, CenterReportStatus, transaction);
        }

        /// <summary>
        /// Insert current SysCenterReports to database.
        /// </summary>
        public void Insert()
        {
            this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? centerReportId, System.Int32? centerId, System.Int32? reportId, System.Boolean? centerReportStatus, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysCenterReports_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@centerReportId"].Value = centerReportId;
            dbCommand.Parameters["@centerId"].Value = centerId;
            dbCommand.Parameters["@reportId"].Value = reportId;
            dbCommand.Parameters["@centerReportStatus"].Value = centerReportStatus;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? centerReportId, System.Int32? centerId, System.Int32? reportId, System.Boolean? centerReportStatus)
        {
            Update(centerReportId, centerId, reportId, centerReportStatus, null);
        }

        public static void Update(SysCenterReports sysCenterReports)
        {
            sysCenterReports.Update();
        }

        public static void Update(SysCenterReports sysCenterReports, DbTransaction transaction)
        {
            sysCenterReports.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysCenterReports_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@centerReportId"].SourceColumn = "CenterReportId";
            dbCommand.Parameters["@centerId"].SourceColumn = "CenterId";
            dbCommand.Parameters["@reportId"].SourceColumn = "ReportId";
            dbCommand.Parameters["@centerReportStatus"].SourceColumn = "CenterReportStatus";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? centerReportId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysCenterReports_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, centerReportId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? centerReportId)
        {
            Delete(
            centerReportId);
        }

        /// <summary>
        /// Delete current SysCenterReports from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysCenterReports_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, CenterReportId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.CenterReportId = null;
        }

        /// <summary>
        /// Delete current SysCenterReports from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysCenterReports[] Search(System.Int32? centerReportId, System.Int32? centerId, System.Int32? reportId, System.Boolean? centerReportStatus)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysCenterReports_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, centerReportId, centerId, reportId, centerReportStatus);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return SysCenterReports.MapFrom(ds);
        }


        public static SysCenterReports[] Search(SysCenterReports searchObject)
        {
            return Search(searchObject.CenterReportId, searchObject.CenterId, searchObject.ReportId, searchObject.CenterReportStatus);
        }

        /// <summary>
        /// Returns all SysCenterReports objects.
        /// </summary>
        /// <returns>List of all SysCenterReports objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static SysCenterReports[] Search()
        {
            return Search(null, null, null, null);
        }

        #endregion


        #endregion


        #endregion


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[sys_CenterReports]";
        private static readonly string DataConnection = "DataConnection";

        #endregion

        #region Custom
        [Serializable]
        public class CenterReports
        {
            public int reportId { get; set; }
            public string reportName { get; set; }
            public bool assigned { get; set; }
        }

        public static IList<CenterReports> GetByCenterId(int centerId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            CenterReports instance;
            IList<CenterReports> llist = new List<CenterReports>();

            instance = new CenterReports();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysCenterReports_GetByCenterId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, centerId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new CenterReports();
                instance.reportId = (int)item["ReportId"];
                instance.reportName = (string)item["ReportName"];
                instance.assigned = (bool)item["Assigned"];
                llist.Add(instance);
            }
            return llist;
        }

        public void InsertOrUpdate(System.Int32? centerId, System.Int32? reportId, System.Boolean? centerReportStatus, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysCenterReports_InsertOrUpdate";
            dbCommand = db.GetStoredProcCommand(sqlCommand, centerId, reportId, centerReportStatus);

            if (transaction == null)
                db.ExecuteScalar(dbCommand);
            else
                db.ExecuteScalar(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public void InsertOrUpdate(System.Int32? centerId, System.Int32? reportId, System.Boolean? centerReportStatus)
        {
            InsertOrUpdate(centerId, reportId, centerReportStatus, null);
        }
        /// <summary>
        /// Insert current SysCenterReports to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void InsertOrUpdate(DbTransaction transaction)
        {
            InsertOrUpdate(CenterId, ReportId, CenterReportStatus, transaction);
        }

        /// <summary>
        /// Insert current SysCenterReports to database.
        /// </summary>
        public void InsertOrUpdate()
        {
            this.InsertOrUpdate((DbTransaction)null);
        }
        #endregion
    }
}
