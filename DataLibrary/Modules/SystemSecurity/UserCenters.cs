﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SystemSecurity.UserCenters
{

    [DataObject]
    [Serializable]
    public partial class SysUserCenters
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[sys_UserCenters]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _userCenterId;
        private System.Int32? _userId;
        private System.Int32? _centerId;
        private System.Boolean? _userCenterStatus;

        #endregion


        #region Properties
        public System.Int32? UserCenterId
        {
            get
            {
                return _userCenterId;
            }
            set
            {
                _userCenterId = value;
            }
        }

        public System.Int32? UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        public System.Int32? CenterId
        {
            get
            {
                return _centerId;
            }
            set
            {
                _centerId = value;
            }
        }

        public System.Boolean? UserCenterStatus
        {
            get
            {
                return _userCenterStatus;
            }
            set
            {
                _userCenterStatus = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        protected void MapTo(DataSet ds)
        {
            DataRow dr;


            if (ds == null)
                ds = new DataSet();

            if (ds.Tables["TABLE_NAME"] == null)
                ds.Tables.Add(TABLE_NAME);

            ds.Tables[TABLE_NAME].Columns.Add("UserCenterId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("UserId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("CenterId", typeof(System.Int32));
            ds.Tables[TABLE_NAME].Columns.Add("UserCenterStatus", typeof(System.Boolean));

            dr = ds.Tables[TABLE_NAME].NewRow();

            if (UserCenterId == null)
                dr["UserCenterId"] = DBNull.Value;
            else
                dr["UserCenterId"] = UserCenterId;

            if (UserId == null)
                dr["UserId"] = DBNull.Value;
            else
                dr["UserId"] = UserId;

            if (CenterId == null)
                dr["CenterId"] = DBNull.Value;
            else
                dr["CenterId"] = CenterId;

            if (UserCenterStatus == null)
                dr["UserCenterStatus"] = DBNull.Value;
            else
                dr["UserCenterStatus"] = UserCenterStatus;


            ds.Tables[TABLE_NAME].Rows.Add(dr);

        }

        protected void MapFrom(DataRow dr)
        {
            UserCenterId = dr["UserCenterId"] != DBNull.Value ? Convert.ToInt32(dr["UserCenterId"]) : UserCenterId = null;
            UserId = dr["UserId"] != DBNull.Value ? Convert.ToInt32(dr["UserId"]) : UserId = null;
            CenterId = dr["CenterId"] != DBNull.Value ? Convert.ToInt32(dr["CenterId"]) : CenterId = null;
            UserCenterStatus = dr["UserCenterStatus"] != DBNull.Value ? Convert.ToBoolean(dr["UserCenterStatus"]) : UserCenterStatus = null;
        }

        public static SysUserCenters[] MapFrom(DataSet ds)
        {
            List<SysUserCenters> objects;


            // Initialise Collection.
            objects = new List<SysUserCenters>();

            // Validation.
            if (ds == null)
                throw new ApplicationException("Cannot map to dataset null.");
            else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
                return objects.ToArray();

            if (ds.Tables[TABLE_NAME] == null)
                throw new ApplicationException("Cannot find table [dbo].[sys_UserCenters] in DataSet.");

            if (ds.Tables[TABLE_NAME].Rows.Count < 1)
                throw new ApplicationException("Table [dbo].[sys_UserCenters] is empty.");

            // Map DataSet to Instance.
            foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
            {
                SysUserCenters instance = new SysUserCenters();
                instance.MapFrom(dr);
                objects.Add(instance);
            }

            // Return collection.
            return objects.ToArray();
        }


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysUserCenters Get(System.Int32 userCenterId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            SysUserCenters instance;


            instance = new SysUserCenters();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenters_SELECT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysUserCenters ID:" + userCenterId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        #region INSERT
        public int Insert(System.Int32? userId, System.Int32? centerId, System.Boolean? userCenterStatus, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenters_INSERT";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userId, centerId, userCenterStatus);

            if (transaction == null)
                this.UserCenterId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            else
                this.UserCenterId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
            return Convert.ToInt32(UserCenterId);
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        public int Insert(System.Int32? userId, System.Int32? centerId, System.Boolean? userCenterStatus)
        {
            return Insert(userId, centerId, userCenterStatus, null);
        }
        /// <summary>
        /// Insert current SysUserCenters to database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public int Insert(DbTransaction transaction)
        {
            return Insert(UserId, CenterId, UserCenterStatus, transaction);
        }

        /// <summary>
        /// Insert current SysUserCenters to database.
        /// </summary>
        public int Insert()
        {
            return this.Insert((DbTransaction)null);
        }
        #endregion


        #region UPDATE
        public static void Update(System.Int32? userCenterId, System.Int32? userId, System.Int32? centerId, System.Boolean? userCenterStatus, DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenters_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@userCenterId"].Value = userCenterId;
            dbCommand.Parameters["@userId"].Value = userId;
            dbCommand.Parameters["@centerId"].Value = centerId;
            dbCommand.Parameters["@userCenterStatus"].Value = userCenterStatus;

            if (transaction == null)
                db.ExecuteNonQuery(dbCommand);
            else
                db.ExecuteNonQuery(dbCommand, transaction);
            return;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        public static void Update(System.Int32? userCenterId, System.Int32? userId, System.Int32? centerId, System.Boolean? userCenterStatus)
        {
            Update(userCenterId, userId, centerId, userCenterStatus, null);
        }

        public static void Update(SysUserCenters sysUserCenters)
        {
            sysUserCenters.Update();
        }

        public static void Update(SysUserCenters sysUserCenters, DbTransaction transaction)
        {
            sysUserCenters.Update(transaction);
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Update(DbTransaction transaction)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenters_UPDATE";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@userCenterId"].SourceColumn = "UserCenterId";
            dbCommand.Parameters["@userId"].SourceColumn = "UserId";
            dbCommand.Parameters["@centerId"].SourceColumn = "CenterId";
            dbCommand.Parameters["@userCenterStatus"].SourceColumn = "UserCenterStatus";

            ds = new DataSet();
            this.MapTo(ds);
            ds.AcceptChanges();
            ds.Tables[0].Rows[0].SetModified();
            if (transaction == null)
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
            else
                db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
            return;
        }

        /// <summary>
        /// Updates changes to the database.
        /// </summary>
        public void Update()
        {
            this.Update((DbTransaction)null);
        }
        #endregion


        #region DELETE
        [DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        public static void Delete(System.Int32? userCenterId, DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenters_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        public static void Delete(System.Int32? userCenterId)
        {
            Delete(
            userCenterId);
        }

        /// <summary>
        /// Delete current SysUserCenters from database.
        /// </summary>
        /// <param name="transaction">optional SQL Transaction</param>
        public void Delete(DbTransaction transaction)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenters_DELETE";
            dbCommand = db.GetStoredProcCommand(sqlCommand, UserCenterId);

            // Execute.
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
            }
            this.UserCenterId = null;
        }

        /// <summary>
        /// Delete current SysUserCenters from database.
        /// </summary>
        public void Delete()
        {
            this.Delete((DbTransaction)null);
        }

        #endregion


        #region SEARCH
        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysUserCenters[] Search(System.Int32? userCenterId, System.Int32? userId, System.Int32? centerId, System.Boolean? userCenterStatus)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenters_SEARCH";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterId, userId, centerId, userCenterStatus);

            ds = db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = TABLE_NAME;
            return SysUserCenters.MapFrom(ds);
        }


        public static SysUserCenters[] Search(SysUserCenters searchObject)
        {
            return Search(searchObject.UserCenterId, searchObject.UserId, searchObject.CenterId, searchObject.UserCenterStatus);
        }

        /// <summary>
        /// Returns all SysUserCenters objects.
        /// </summary>
        /// <returns>List of all SysUserCenters objects. </returns>
        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static SysUserCenters[] Search()
        {
            return Search(null, null, null, null);
        }

        #endregion

        
        #endregion


        #endregion


        #region Custom

        [Serializable]
        public class UserCenters
        {
            public int userCenterId { get; set; }
            public int userId { get; set; }
            public int centerId { get; set; }
            public string centerName { get; set; }
            public bool userCenterStatus { get; set; }
        }

        //access
        public static IList<UserCenters> GetByUserId(int userId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            UserCenters instance;
            IList<UserCenters> llist = new List<UserCenters>();

            instance = new UserCenters();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysUserCenters_GetByUserId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new UserCenters();
                instance.userCenterId = (int)item["UserCenterId"];
                instance.userId = (int)item["UserId"];
                instance.centerId = (int)item["CenterId"];
                instance.centerName = (string)item["CenterName"];
                instance.userCenterStatus = Convert.ToBoolean(item["UserCenterStatus"]);
                llist.Add(instance);
            }
            return llist;
        }

        public static IList<UserCenters> GetByUserCenterId(int userCenterId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            UserCenters instance;
            IList<UserCenters> llist = new List<UserCenters>();

            instance = new UserCenters();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysUserCenters_GetByUserCenterId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new UserCenters();
                instance.userCenterId = (int)item["UserCenterId"];
                instance.userId = (int)item["UserId"];
                instance.centerId = (int)item["CenterId"];
                instance.centerName = (string)item["CenterName"];
                instance.userCenterStatus = Convert.ToBoolean(item["UserCenterStatus"]);
                llist.Add(instance);
            }
            return llist;
        }

        public static IList<UserCenters> GetUnAsignedByUserId(int userId)
        {
            DataSet ds;
            string sqlCommand;
            DbCommand dbCommand;
            UserCenters instance;
            IList<UserCenters> llist = new List<UserCenters>();

            instance = new UserCenters();

            Database db = DatabaseFactory.CreateDatabase("DataConnection");
            sqlCommand = "SysUserCenters_GetUnAsignedByUserId";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);

            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                instance = new UserCenters();
                instance.userCenterId = (int)item["UserCenterId"];
                instance.userId = (int)item["UserId"];
                instance.centerId = (int)item["CenterId"];
                instance.centerName = (string)item["CenterName"];
                instance.userCenterStatus = Convert.ToBoolean(item["UserCenterStatus"]);
                llist.Add(instance);
            }
            return llist;
        }

        public static int IfExists(System.Int32? userCenterId, System.Int32? userId, System.Int32? centerId, System.Boolean? userCenterStatus)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenters_IfExists";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterId, userId, centerId, userCenterStatus);

            try
            {
                return Convert.ToInt32(db.ExecuteScalar(dbCommand));
            }
            catch (ArgumentNullException)
            {
                return 0;
            }
        }

        public static void SetPermissionsInactives(System.Int32? userId)
        {
            Database db;
            string sqlCommand;
            DbCommand dbCommand;


            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysUserCenters_SetPermissionsInactives";
            dbCommand = db.GetStoredProcCommand(sqlCommand, userId);

            db.ExecuteNonQuery(dbCommand);

            return;
        }
        #endregion
    }

}