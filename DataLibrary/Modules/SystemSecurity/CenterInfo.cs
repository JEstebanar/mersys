﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SystemSecurity.CentersInfo
{

    [DataObject]
    [Serializable]
    public partial class SysCentersInfo
    {


        #region Constants
        private static readonly string TABLE_NAME = "[dbo].[sys_CentersInfo]";
        private static readonly string DataConnection = "DataConnection";

        #endregion


        #region Fields
        private System.Int32? _centerId;
        private System.String _centerName;
        private System.String _headTraining;
        private System.String _centerAddress;
        private System.String _cityName;
        private System.String _stateName;
        private System.String _countryName;
        private System.String _phoneNumber;

        #endregion


        #region Properties
        public System.Int32? CenterId
        {
            get
            {
                return _centerId;
            }
            set
            {
                _centerId = value;
            }
        }

        public System.String CenterName
        {
            get
            {
                return _centerName;
            }
            set
            {
                _centerName = value;
            }
        }

        public System.String HeadTraining
        {
            get
            {
                return _headTraining;
            }
            set
            {
                _headTraining = value;
            }
        }

        public System.String CenterAddress
        {
            get
            {
                return _centerAddress;
            }
            set
            {
                _centerAddress = value;
            }
        }

        public System.String CityName
        {
            get
            {
                return _cityName;
            }
            set
            {
                _cityName = value;
            }
        }

        public System.String StateName
        {
            get
            {
                return _stateName;
            }
            set
            {
                _stateName = value;
            }
        }

        public System.String CountryName
        {
            get
            {
                return _countryName;
            }
            set
            {
                _countryName = value;
            }
        }

        public System.String PhoneNumber
        {
            get
            {
                return _phoneNumber;
            }
            set
            {
                _phoneNumber = value;
            }
        }

        #endregion


        #region Methods


        #region Mapping Methods

        //protected void MapTo(DataSet ds)
        //{
        //    DataRow dr;


        //    if (ds == null)
        //        ds = new DataSet();

        //    if (ds.Tables["TABLE_NAME"] == null)
        //        ds.Tables.Add(TABLE_NAME);

        //    ds.Tables[TABLE_NAME].Columns.Add("CenterId", typeof(System.Int32));
        //    ds.Tables[TABLE_NAME].Columns.Add("CenterName", typeof(System.String));
        //    ds.Tables[TABLE_NAME].Columns.Add("HeadTraining", typeof(System.String));
        //    ds.Tables[TABLE_NAME].Columns.Add("CenterAddress", typeof(System.String));
        //    ds.Tables[TABLE_NAME].Columns.Add("CityName", typeof(System.String));
        //    ds.Tables[TABLE_NAME].Columns.Add("StateName", typeof(System.String));
        //    ds.Tables[TABLE_NAME].Columns.Add("CountryName", typeof(System.String));
        //    ds.Tables[TABLE_NAME].Columns.Add("PhoneNumber", typeof(System.String));

        //    dr = ds.Tables[TABLE_NAME].NewRow();

        //    if (CenterId == null)
        //        dr["CenterId"] = DBNull.Value;
        //    else
        //        dr["CenterId"] = CenterId;

        //    if (CenterName == null)
        //        dr["CenterName"] = DBNull.Value;
        //    else
        //        dr["CenterName"] = CenterName;

        //    if (HeadTraining == null)
        //        dr["HeadTraining"] = DBNull.Value;
        //    else
        //        dr["HeadTraining"] = HeadTraining;

        //    if (CenterAddress == null)
        //        dr["CenterAddress"] = DBNull.Value;
        //    else
        //        dr["CenterAddress"] = CenterAddress;

        //    if (CityName == null)
        //        dr["CityName"] = DBNull.Value;
        //    else
        //        dr["CityName"] = CityName;

        //    if (StateName == null)
        //        dr["StateName"] = DBNull.Value;
        //    else
        //        dr["StateName"] = StateName;

        //    if (CountryName == null)
        //        dr["CountryName"] = DBNull.Value;
        //    else
        //        dr["CountryName"] = CountryName;

        //    if (PhoneNumber == null)
        //        dr["PhoneNumber"] = DBNull.Value;
        //    else
        //        dr["PhoneNumber"] = PhoneNumber;


        //    ds.Tables[TABLE_NAME].Rows.Add(dr);

        //}

        protected void MapFrom(DataRow dr)
        {
            CenterId = dr["CenterId"] != DBNull.Value ? Convert.ToInt32(dr["CenterId"]) : CenterId = null;
            CenterName = dr["CenterName"] != DBNull.Value ? Convert.ToString(dr["CenterName"]) : CenterName = null;
            HeadTraining = dr["HeadTraining"] != DBNull.Value ? Convert.ToString(dr["HeadTraining"]) : HeadTraining = null;
            CenterAddress = dr["CenterAddress"] != DBNull.Value ? Convert.ToString(dr["CenterAddress"]) : CenterAddress = null;
            CityName = dr["CityName"] != DBNull.Value ? Convert.ToString(dr["CityName"]) : CityName = null;
            StateName = dr["StateName"] != DBNull.Value ? Convert.ToString(dr["StateName"]) : StateName = null;
            CountryName = dr["CountryName"] != DBNull.Value ? Convert.ToString(dr["CountryName"]) : CountryName = null;
            PhoneNumber = dr["PhoneNumber"] != DBNull.Value ? Convert.ToString(dr["PhoneNumber"]) : PhoneNumber = null;
        }

        //public static SysCentersInfo[] MapFrom(DataSet ds)
        //{
        //    List<SysCentersInfo> objects;


        //    // Initialise Collection.
        //    objects = new List<SysCentersInfo>();

        //    // Validation.
        //    if (ds == null)
        //        throw new ApplicationException("Cannot map to dataset null.");
        //    else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
        //        return objects.ToArray();

        //    if (ds.Tables[TABLE_NAME] == null)
        //        throw new ApplicationException("Cannot find table [dbo].[sys_CentersInfo] in DataSet.");

        //    if (ds.Tables[TABLE_NAME].Rows.Count < 1)
        //        throw new ApplicationException("Table [dbo].[sys_CentersInfo] is empty.");

        //    // Map DataSet to Instance.
        //    foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
        //    {
        //        SysCentersInfo instance = new SysCentersInfo();
        //        instance.MapFrom(dr);
        //        objects.Add(instance);
        //    }

        //    // Return collection.
        //    return objects.ToArray();
        //}


        #endregion


        #region CRUD Methods

        [DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        public static SysCentersInfo Get(System.Int32 centerId)
        {
            DataSet ds;
            Database db;
            string sqlCommand;
            DbCommand dbCommand;
            SysCentersInfo instance;


            instance = new SysCentersInfo();

            db = DatabaseFactory.CreateDatabase(DataConnection);
            sqlCommand = "[dbo].SysCenters_GetInfo";
            dbCommand = db.GetStoredProcCommand(sqlCommand, centerId);

            // Get results.
            ds = db.ExecuteDataSet(dbCommand);
            // Verification.
            if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysCentersInfo ID:" + centerId.ToString() + " from Database.");
            // Return results.
            ds.Tables[0].TableName = TABLE_NAME;

            instance.MapFrom(ds.Tables[0].Rows[0]);
            return instance;
        }

        //#region INSERT
        //public void Insert(System.String centerName, System.String headTraining, System.String centerAddress, System.String cityName, System.String stateName, System.String countryName, System.String phoneNumber, DbTransaction transaction)
        //{
        //    Database db;
        //    string sqlCommand;
        //    DbCommand dbCommand;


        //    db = DatabaseFactory.CreateDatabase(DataConnection);
        //    sqlCommand = "[dbo].SysCentersInfo_INSERT";
        //    dbCommand = db.GetStoredProcCommand(sqlCommand, centerName, headTraining, centerAddress, cityName, stateName, countryName, phoneNumber);

        //    if (transaction == null)
        //        this.CenterId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
        //    else
        //        this.CenterId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
        //    return;
        //}

        //[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
        //public void Insert(System.String centerName, System.String headTraining, System.String centerAddress, System.String cityName, System.String stateName, System.String countryName, System.String phoneNumber)
        //{
        //    Insert(centerName, headTraining, centerAddress, cityName, stateName, countryName, phoneNumber, null);
        //}
        ///// <summary>
        ///// Insert current SysCentersInfo to database.
        ///// </summary>
        ///// <param name="transaction">optional SQL Transaction</param>
        //public void Insert(DbTransaction transaction)
        //{
        //    Insert(CenterName, HeadTraining, CenterAddress, CityName, StateName, CountryName, PhoneNumber, transaction);
        //}

        ///// <summary>
        ///// Insert current SysCentersInfo to database.
        ///// </summary>
        //public void Insert()
        //{
        //    this.Insert((DbTransaction)null);
        //}
        //#endregion


        //#region UPDATE
        //public static void Update(System.Int32? centerId, System.String centerName, System.String headTraining, System.String centerAddress, System.String cityName, System.String stateName, System.String countryName, System.String phoneNumber, DbTransaction transaction)
        //{
        //    DataSet ds;
        //    Database db;
        //    string sqlCommand;
        //    DbCommand dbCommand;


        //    db = DatabaseFactory.CreateDatabase(DataConnection);
        //    sqlCommand = "[dbo].SysCentersInfo_UPDATE";
        //    dbCommand = db.GetStoredProcCommand(sqlCommand);
        //    db.DiscoverParameters(dbCommand);
        //    dbCommand.Parameters["@centerId"].Value = centerId;
        //    dbCommand.Parameters["@centerName"].Value = centerName;
        //    dbCommand.Parameters["@headTraining"].Value = headTraining;
        //    dbCommand.Parameters["@centerAddress"].Value = centerAddress;
        //    dbCommand.Parameters["@cityName"].Value = cityName;
        //    dbCommand.Parameters["@stateName"].Value = stateName;
        //    dbCommand.Parameters["@countryName"].Value = countryName;
        //    dbCommand.Parameters["@phoneNumber"].Value = phoneNumber;

        //    if (transaction == null)
        //        db.ExecuteNonQuery(dbCommand);
        //    else
        //        db.ExecuteNonQuery(dbCommand, transaction);
        //    return;
        //}

        //[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
        //public static void Update(System.Int32? centerId, System.String centerName, System.String headTraining, System.String centerAddress, System.String cityName, System.String stateName, System.String countryName, System.String phoneNumber)
        //{
        //    Update(centerId, centerName, headTraining, centerAddress, cityName, stateName, countryName, phoneNumber, null);
        //}

        //public static void Update(SysCentersInfo sysCentersInfo)
        //{
        //    sysCentersInfo.Update();
        //}

        //public static void Update(SysCentersInfo sysCentersInfo, DbTransaction transaction)
        //{
        //    sysCentersInfo.Update(transaction);
        //}

        ///// <summary>
        ///// Updates changes to the database.
        ///// </summary>
        ///// <param name="transaction">optional SQL Transaction</param>
        //public void Update(DbTransaction transaction)
        //{
        //    DataSet ds;
        //    Database db;
        //    string sqlCommand;
        //    DbCommand dbCommand;


        //    db = DatabaseFactory.CreateDatabase(DataConnection);
        //    sqlCommand = "[dbo].SysCentersInfo_UPDATE";
        //    dbCommand = db.GetStoredProcCommand(sqlCommand);
        //    db.DiscoverParameters(dbCommand);
        //    dbCommand.Parameters["@centerId"].SourceColumn = "CenterId";
        //    dbCommand.Parameters["@centerName"].SourceColumn = "CenterName";
        //    dbCommand.Parameters["@headTraining"].SourceColumn = "HeadTraining";
        //    dbCommand.Parameters["@centerAddress"].SourceColumn = "CenterAddress";
        //    dbCommand.Parameters["@cityName"].SourceColumn = "CityName";
        //    dbCommand.Parameters["@stateName"].SourceColumn = "StateName";
        //    dbCommand.Parameters["@countryName"].SourceColumn = "CountryName";
        //    dbCommand.Parameters["@phoneNumber"].SourceColumn = "PhoneNumber";

        //    ds = new DataSet();
        //    this.MapTo(ds);
        //    ds.AcceptChanges();
        //    ds.Tables[0].Rows[0].SetModified();
        //    if (transaction == null)
        //        db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
        //    else
        //        db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
        //    return;
        //}

        ///// <summary>
        ///// Updates changes to the database.
        ///// </summary>
        //public void Update()
        //{
        //    this.Update((DbTransaction)null);
        //}
        //#endregion


        //#region DELETE
        //[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
        //public static void Delete(System.Int32? centerId, DbTransaction transaction)
        //{
        //    Database db;
        //    string sqlCommand;
        //    DbCommand dbCommand;


        //    db = DatabaseFactory.CreateDatabase(DataConnection);
        //    sqlCommand = "[dbo].SysCentersInfo_DELETE";
        //    dbCommand = db.GetStoredProcCommand(sqlCommand, centerId);

        //    // Execute.
        //    if (transaction != null)
        //    {
        //        db.ExecuteNonQuery(dbCommand, transaction);
        //    }
        //    else
        //    {
        //        db.ExecuteNonQuery(dbCommand);
        //    }
        //}

        //[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
        //public static void Delete(System.Int32? centerId)
        //{
        //    Delete(
        //    centerId);
        //}

        ///// <summary>
        ///// Delete current SysCentersInfo from database.
        ///// </summary>
        ///// <param name="transaction">optional SQL Transaction</param>
        //public void Delete(DbTransaction transaction)
        //{
        //    Database db;
        //    string sqlCommand;
        //    DbCommand dbCommand;


        //    db = DatabaseFactory.CreateDatabase(DataConnection);
        //    sqlCommand = "[dbo].SysCentersInfo_DELETE";
        //    dbCommand = db.GetStoredProcCommand(sqlCommand, CenterId);

        //    // Execute.
        //    if (transaction != null)
        //    {
        //        db.ExecuteNonQuery(dbCommand, transaction);
        //    }
        //    else
        //    {
        //        db.ExecuteNonQuery(dbCommand);
        //    }
        //    this.CenterId = null;
        //}

        ///// <summary>
        ///// Delete current SysCentersInfo from database.
        ///// </summary>
        //public void Delete()
        //{
        //    this.Delete((DbTransaction)null);
        //}

        //#endregion


        //#region SEARCH
        //[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
        //public static SysCentersInfo[] Search(System.Int32? centerId, System.String centerName, System.String headTraining, System.String centerAddress, System.String cityName, System.String stateName, System.String countryName, System.String phoneNumber)
        //{
        //    DataSet ds;
        //    Database db;
        //    string sqlCommand;
        //    DbCommand dbCommand;


        //    db = DatabaseFactory.CreateDatabase(DataConnection);
        //    sqlCommand = "[dbo].SysCentersInfo_SEARCH";
        //    dbCommand = db.GetStoredProcCommand(sqlCommand, centerId, centerName, headTraining, centerAddress, cityName, stateName, countryName, phoneNumber);

        //    ds = db.ExecuteDataSet(dbCommand);
        //    ds.Tables[0].TableName = TABLE_NAME;
        //    return SysCentersInfo.MapFrom(ds);
        //}


        //public static SysCentersInfo[] Search(SysCentersInfo searchObject)
        //{
        //    return Search(searchObject.CenterId, searchObject.CenterName, searchObject.HeadTraining, searchObject.CenterAddress, searchObject.CityName, searchObject.StateName, searchObject.CountryName, searchObject.PhoneNumber);
        //}

        ///// <summary>
        ///// Returns all SysCentersInfo objects.
        ///// </summary>
        ///// <returns>List of all SysCentersInfo objects. </returns>
        //[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        //public static SysCentersInfo[] Search()
        //{
        //    return Search(null, null, null, null, null, null, null, null);
        //}

        //#endregion


        #endregion


        #endregion


    }
}