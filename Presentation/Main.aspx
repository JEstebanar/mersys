﻿<%@ Page Title="Docencia Médica" Language="C#" MasterPageFile="~/SiteGeneral.master"
    AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="Presentation.Main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<div class="row-fluid">
        <div class="span4">
        </div>
        <div class="span4">
        </div>
        <div class="span4">
        </div>
    </div>--%>

    <div id="divPosts" runat="server" class="row-fluid" >
        <ul class="thumbnails">
            <li class="span4">
                <asp:Repeater ID="rptPostsLeft" runat="server">
                    <ItemTemplate>
                        <div class="thumbnail">
                            <%--<img alt="300x200" style="width: 300px; height: 200px;" src="medicenter/images/samples/310x200/image_04.jpg">--%>
                            <a class="post_image" href="<%# Eval("postURL")%>" title="<%# Eval("title")%>">
                                <%--<img src="<%# Eval("postImage")%>" alt="" />--%>
                                <asp:Image ID="newsImage" runat="server" alt="300x200" Style="width: 300px; height: 200px;"
                                    Visible='<%# Eval("postImage").ToString() != "" %>' ImageUrl='<%# Eval("postImage") %>' />
                            </a>
                            <div class="caption">
                                <h3>
                                    <a href="<%# Eval("postURL")%>" title="<%# Eval("title")%>">
                                        <%# Eval("title")%>
                                    </a>
                                </h3>
                                <%#Eval("body").ToString().Trim().Length <= 500 ? Eval("body") : Eval("body").ToString().Remove(500) %>
                                <p>
                                    <a href="<%# Eval("postURL")%>" class="btn btn-link">Leer más >></a></p>
                            </div>
                        </div>
                        <br />
                    </ItemTemplate>
                </asp:Repeater>
            </li>
            <li class="span4">
                <asp:Repeater ID="rptPostsRight" runat="server">
                    <ItemTemplate>
                        <div class="thumbnail">
                            <%--<img alt="300x200" style="width: 300px; height: 200px;" src="medicenter/images/samples/310x200/image_04.jpg">--%>
                            <a class="post_image" href="<%# Eval("postURL")%>" title="<%# Eval("title")%>">
                                <%--<img src="<%# Eval("postImage")%>" alt="" />--%>
                                <asp:Image ID="newsImage" runat="server" alt="300x200" Style="width: 300px; height: 200px;"
                                    Visible='<%# Eval("postImage").ToString() != "" %>' ImageUrl='<%# Eval("postImage") %>' />
                            </a>
                            <div class="caption">
                                <h3>
                                    <a href="<%# Eval("postURL")%>" title="<%# Eval("title")%>">
                                        <%# Eval("title")%>
                                    </a>
                                </h3>
                                <%#Eval("body").ToString().Trim().Length <= 500 ? Eval("body") : Eval("body").ToString().Remove(500) %>
                                <p>
                                    <a href="<%# Eval("postURL")%>" class="btn btn-link">Leer más >></a></p>
                            </div>
                        </div>
                        <br />
                    </ItemTemplate>
                </asp:Repeater>
            </li>
            <li class="span4">
                <asp:Repeater ID="rptPostsSide" runat="server">
                    <ItemTemplate>
                        <div class="thumbnail">
                            <%--<img alt="300x200" style="width: 300px; height: 200px;" src="medicenter/images/samples/310x200/image_04.jpg">--%>
                            <a class="post_image" href="<%# Eval("postURL")%>" title="<%# Eval("title")%>">
                                <%--<img src="<%# Eval("postImage")%>" alt="" />--%>
                                <asp:Image ID="newsImage" runat="server" alt="300x200" Style="width: 300px; height: 200px;"
                                    Visible='<%# Eval("postImage").ToString() != "" %>' ImageUrl='<%# Eval("postImage") %>' />
                            </a>
                            <div class="caption">
                                <h3>
                                    <a href="<%# Eval("postURL")%>" title="<%# Eval("title")%>">
                                        <%# Eval("title")%>
                                    </a>
                                </h3>
                                <%#Eval("body").ToString().Trim().Length <= 500 ? Eval("body") : Eval("body").ToString().Remove(500) %>
                                <p>
                                    <a href="<%# Eval("postURL")%>" class="btn btn-link">Leer más >></a></p>
                            </div>
                        </div>
                        <br />
                    </ItemTemplate>
                </asp:Repeater>
            </li>
        </ul>

        <!-- pagination -->
        <div class="pagination pagination-large pagination-centered">
            <ul>
                <%--<li>
                    <asp:LinkButton ID="lnkBtnPrev" runat="server" Font-Underline="False" OnClick="lnkBtnPrev_Click"
                        Font-Bold="True"><< Anterior </asp:LinkButton></li>--%>
                <li>
                    <input id="txtHidden" style="width: 28px" type="hidden" value="1" runat="server" />
                </li>
                <%--<li>
                    <asp:LinkButton ID="lnkBtnNext" runat="server" Font-Underline="False" OnClick="lnkBtnNext_Click"
                        Font-Bold="True">Siguiente >></asp:LinkButton></li>--%>
                <%--<li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">»</a></li>--%>
            </ul>
        </div>
    </div>

        <!-- Post Modal -->
        <div id="pstModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×</button>
            <h3 id="myModalLabel">
                <label id="lblTitle" runat="server"></label></h3>
        </div>
        <div class="modal-body" id="divBody" runat="server">
            <%--<p>
                ¿Esta seguro que desea salir?</p>--%>
        </div>
        <div class="modal-footer">
            <%--<asp:Button ID="btnExit" runat="server" class="btn btn-primary" Text="Aceptar" OnClick="btnExit_Click" />--%>
            <button class="btn" data-dismiss="modal" aria-hidden="true">
                Cancelar</button>
        </div>
    </div>
</asp:Content>
