﻿<%@ Page Title="Artículos" Language="C#" MasterPageFile="~/medicenter.Master" AutoEventWireup="true"
    CodeBehind="Articulos.aspx.cs" Inherits="Presentation.Articulos" %>

<%@ Register Src="~/Entradas/UserControls/wucSidebar.ascx" TagName="wucSidebar" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server"> 
    <div class="page relative">
        <div class="page_layout page_margin_top clearfix">
            <div class="page_header clearfix">
                <div class="page_header_left">
                    <h1 class="page_title">
                        Artículos</h1>
                    <%--<ul class="bread_crumb">
                        <li><a href="home.aspx" title="Home">Inicio </a></li>
                        <li class="separator icon_small_arrow right_gray">&nbsp; </li>
                        <li>Artículos </li>
                    </ul>--%>
                </div>
                <%--                <div class="page_header_right">
                    <form class="search">
                    <input class="search_input" type="text" value="To search type and hit enter..." placeholder="To search type and hit enter..." />
                    </form>
                </div>--%>
            </div>
            <div class="page_left">
                <ul class="blog clearfix">
                    <%--                    <li class="post">
                        <ul class="comment_box">
                            <li class="date clearfix animated_element animation-slideRight">
                                <div class="value">
                                    12 DEC 12</div>
                                <div class="arrow_date">
                                </div>
                            </li>
                            <li class="comments_number animated_element animation-slideUp duration-300 delay-500">
                                <a href="post.html#comments_list" title="2 comments">2 comments </a></li>
                        </ul>
                        <div class="post_content">
                            <a class="post_image" href="post.html" title="Lorem ipsum dolor sit amat velum">
                                <img src="images/samples/520x240/image_03.jpg" alt="" />
                            </a>
                            <h2>
                                <a href="post.html" title="Lorem ipsum dolor sit amat velum">Lorem ipsum dolor sit amat
                                    velum </a>
                            </h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut volutpat rutrum eros
                                amet sollicitudin interdum. Suspendisse pulvinar, velit nec pharetra interdum, ante
                                tellus ornare mi, et mollis tellus neque vitae elit. Mauris adipiscing mauris fringilla
                                turpis interdum sed pulvinar nisi malesuada. Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit.
                            </p>
                            <p>
                                Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id
                                nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus
                                posuere velit aliquet. Duis mollis, est non commodo luctus, nisi erat porttitor
                                ligula. Mauris sit amet neque nec nunc gravida.
                            </p>
                            <a title="Read more" href="post.html" class="more">Read more &rarr; </a>
                            <div class="post_footer clearfix">
                                <ul class="post_footer_details">
                                    <li>Posted in </li>
                                    <li><a href="#" title="General">General, </a></li>
                                    <li><a href="#" title="Dental clinic">Dental clinic </a></li>
                                </ul>
                                <ul class="post_footer_details">
                                    <li>Posted by </li>
                                    <li><a href="#" title="John Doe">John Doe </a></li>
                                </ul>
                                <ul class="post_footer_details">
                                    <li>Post type </li>
                                    <li><a href="#" title="Image">Image </a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="post">
                        <ul class="comment_box clearfix">
                            <li class="date clearfix animated_element animation-slideRight">
                                <div class="value">
                                    11 DEC 12</div>
                                <div class="arrow_date">
                                </div>
                            </li>
                            <li class="comments_number animated_element animation-slideUp duration-300 delay-500">
                                <a href="post.html#comments_list" title="8 comments">8 comments </a></li>
                        </ul>
                        <div class="post_content">
                            <a class="post_image" href="post.html" title="Lorem ipsum dolor sit amat velum">
                                <img src="images/samples/520x240/image_08.jpg" alt="" />
                            </a>
                            <h2>
                                <a href="post.html" title="Lorem ipsum dolor sit amat velum">Lorem ipsum dolor sit amat
                                    velum </a>
                            </h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut volutpat rutrum eros
                                amet sollicitudin interdum. Suspendisse pulvinar, velit nec pharetra interdum, ante
                                tellus ornare mi, et mollis tellus neque vitae elit. Mauris adipiscing mauris fringilla
                                turpis interdum sed pulvinar nisi malesuada. Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit.
                            </p>
                            <a title="Read more" href="post.html" class="more">Read more &rarr; </a>
                            <div class="post_footer clearfix">
                                <ul class="post_footer_details">
                                    <li>Posted in</li>
                                    <li><a href="#" title="General">General, </a></li>
                                    <li><a href="#" title="Outpatient surgery">Outpatient surgery </a></li>
                                </ul>
                                <ul class="post_footer_details">
                                    <li>Posted by </li>
                                    <li><a href="#" title="John Doe">John Doe </a></li>
                                </ul>
                                <ul class="post_footer_details">
                                    <li>Post type </li>
                                    <li><a href="#" title="Video">Video </a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="post">
                        <ul class="comment_box clearfix">
                            <li class="date clearfix animated_element animation-slideRight">
                                <div class="value">
                                    11 DEC 12</div>
                                <div class="arrow_date">
                                </div>
                            </li>
                            <li class="comments_number animated_element animation-slideUp duration-300 delay-500">
                                <a href="post.html#comments_list" title="3 comments">3 comments </a></li>
                        </ul>
                        <div class="post_content">
                            <h2>
                                <a href="post.html" title="Lorem ipsum dolor sit amat velum">Lorem ipsum dolor sit amat
                                    velum </a>
                            </h2>
                            <blockquote>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut volutpat rutrum eros
                                amet sollicitudin interdum. Suspendisse pulvinar, velit nec pharetra interdum, ante
                                tellus ornare mi, et mollis tellus neque vitae elit volutpat rutrum eros.
                            </blockquote>
                            <a title="Read more" href="post.html" class="more">Read more &rarr; </a>
                            <div class="post_footer clearfix">
                                <ul class="post_footer_details">
                                    <li>Posted in</li>
                                    <li><a href="#" title="General">General, </a></li>
                                    <li><a href="#" title="Outpatient surgery">Outpatient surgery </a></li>
                                </ul>
                                <ul class="post_footer_details">
                                    <li>Posted by </li>
                                    <li><a href="#" title="John Doe">John Doe </a></li>
                                </ul>
                                <ul class="post_footer_details">
                                    <li>Post type </li>
                                    <li><a href="#" title="Quote">Quote </a></li>
                                </ul>
                            </div>
                        </div>
                    </li>--%>
                    <asp:Repeater ID="rptPosts" runat="server">
                        <ItemTemplate>
                            <li class="post">
                                <ul class="comment_box">
                                    <li class="date clearfix animated_element animation-slideRight">
                                        <div class="value">
                                            <%# Eval("createdDate")%></div>
                                        <div class="arrow_date">
                                        </div>
                                    </li>
                                    <%--<li class="comments_number animated_element animation-slideUp duration-300 delay-500">
                                    <a href="<%# Eval("postURL")%>#comments_list"
                                        title=<%# Eval("comments")%>><%# Eval("comments")%> </a></li>--%>
                                    <li class="comments_number animated_element animation-slideDown duration-500 delay-700">
                                        <a href="<%# Eval("postURL")%>#comments_list" title='<%# Eval("visits")%>'>
                                            <%# Eval("visits")%>
                                        </a></li>
                                </ul>
                                <div class="post_content">
                                    <a class="post_image" href="<%# Eval("postURL")%>" title="<%# Eval("title")%>">
                                        <%--<img src="<%# Eval("postImage")%>" alt="" />--%>
                                        <asp:Image ID="newsImage" runat="server" Visible='<%# Eval("postImage").ToString() != "" %>'
                                            ImageUrl='<%# Eval("postImage") %>' />
                                    </a>
                                    <h2>
                                        <a href="<%# Eval("postURL")%>" title="<%# Eval("title")%>">
                                            <%# Eval("title")%>
                                        </a>
                                    </h2>
                                    <p>
                                        <%#Eval("body").ToString().Trim().Length <= 500 ? Eval("body") : Eval("body").ToString().Remove(500) %>
                                    </p>
                                    <a title="Leer más" href="<%# Eval("postURL")%>" class="more">Leer más &rarr; </a>
                                    <%--<div class="post_footer clearfix">
                                <ul class="post_footer_details">
                                    <li>Posted in </li>
                                    <li><a href="#" title="General">General, </a></li>
                                    <li><a href="#" title="Dental clinic">Dental clinic </a></li>
                                </ul>
                                <ul class="post_footer_details">
                                    <li>Posted by </li>
                                    <li><a href="#" title="John Doe">John Doe </a></li>
                                </ul>
                                <ul class="post_footer_details">
                                    <li>Post type </li>
                                    <li><a href="#" title="Image">Image </a></li>
                                </ul>
                            </div>--%>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
                <ul class="pagination page_margin_top">
                    <%--<li class="selected"><a href="#" title="">1 </a></li>
                    <li><a href="#" title="">2 </a></li>
                    <li><a href="#" title="">3 </a></li>--%>
                    <li>
                        <asp:LinkButton ID="lnkBtnPrev" runat="server" Font-Underline="False" OnClick="lnkBtnPrev_Click"
                            Font-Bold="True"><< Anterior </asp:LinkButton></li>
                    <li>
                        <input id="txtHidden" style="width: 28px" type="hidden" value="1" runat="server" />
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnNext" runat="server" Font-Underline="False" OnClick="lnkBtnNext_Click"
                            Font-Bold="True">Siguiente >></asp:LinkButton></li>
                </ul>
            </div>
            <div class="page_right">
                <uc1:wucSidebar ID="wucSidebar1" runat="server" />
                



            </div>
        </div>
    </div>
</asp:Content>
