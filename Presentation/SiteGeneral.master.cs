﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SystemSecurity.Screens;
using System.Web.UI.HtmlControls;
using SystemSecurity.SysUsers;
using SystemSecurity.UserAccess;
using System.Web.Security;
using SystemSecurity.SysError;
using System.IO;
using System.Reflection;
using Residences.TeachingYears;
using SystemSecurity.UserRoles;
using General.GeneralCommons;

namespace Presentation
{
    public partial class SiteGeneral : System.Web.UI.MasterPage
    {
        private IList<SysScreens.Screens> CurrentScreens { get { return (IList<SysScreens.Screens>)ViewState["currentScreens"]; } set { ViewState["currentScreens"] = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    //Initializate();
                    //ddlTeachingYears.SelectedValue = ((SysUserAccess)Session["access"]).TeachingYearId.ToString();
                    lblTeachingYear.InnerText = ((SysUserAccess)Session["access"]).TeachingYear.ToString();
                   
                    lblUserFullName.InnerText = ((SysUserAccess)Session["access"]).FullName;

                    //business logic
                    divMsgCenter.Visible = false;
                    //foreach (SysUserRoles.UserRoles roleInList in (List<SysUserRoles.UserRoles>)Session["roles"])
                    //{
                    //    if (roleInList.roleId != (int)GeneralCommon.SystemRoles.Administrator
                    //        && roleInList.roleId != (int)GeneralCommon.SystemRoles.Resident
                    //        && roleInList.roleId != (int)GeneralCommon.SystemRoles.Coordinator)
                    //    {
                    //        divMsgCenter.InnerHtml = "Para poder seguir brindando un buen servicio con esta plataforma de evaluación"+
                    //            "es necesario que el <strong>Director Docente</strong> se comunique con el <strong>Administrador</strong> de este sistema.";
                    //        divMsgCenter.Visible = true;
                    //    }
                    //}                    
                }
                LoadMenuNavigation();
                Message("", "", 0);
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        #region Methods
        private void LoadMenuNavigation()
        {
            if (Session["menu"] == null)
            {
                CurrentScreens = SysScreens.GetByUserCenterId((int)((SysUserAccess)Session["access"]).UserCenterId);
                Session["menu"] = CurrentScreens;
            }
            else
                CurrentScreens = (List<SysScreens.Screens>)Session["menu"];

            //CurrentScreens = SysScreens.GetByUserCenterId((int)((SysUserAccess)Session["access"]).UserCenterId);
            int beforeModuleId = -1;
            foreach (SysScreens.Screens screensInList in CurrentScreens)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");

                if (beforeModuleId != screensInList.moduleId)
                {
                    li.Attributes.Add("class", "nav-header");
                    li.InnerText = screensInList.screenName;
                    NavList.Controls.Add(li);
                }
                else
                {
                    NavList.Controls.Add(li);
                    HtmlGenericControl anchor = new HtmlGenericControl("a");
                    anchor.Attributes.Add("href", screensInList.screenUrl);
                    anchor.InnerText = screensInList.screenName;
                    li.Controls.Add(anchor);
                }

                beforeModuleId = screensInList.moduleId;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">Message you want to display</param>
        /// <param name="header">Message header</param>
        /// <param name="type">1 - Warning Message, || 2 - Error Message, || 3 - Message of Success, || 4 - Message Information</param>
        public void Message(System.String message, System.String header, int? type)
        {
            HtmlGenericControl h4 = new HtmlGenericControl("h4");
            //HtmlGenericControl p = new HtmlGenericControl("p");

            if (type == null || type > 4 || type <= 0)
            {
                divMessage.Controls.Remove(h4);

                //divMessage.Controls.Remove(p);

                divMessage.Attributes.Add("class", "");
                divMessage.Visible = false;
            }
            else
            {
                divMessage.Visible = true;

                switch (type)
                {
                    case 1:         //warning
                        divMessage.Attributes.Add("class", "alert alert-block");
                        h4.InnerText = "Advertencia";
                        break;
                    case 2:         //error
                        divMessage.Attributes.Add("class", "alert alert-error");
                        h4.InnerText = "Algo esta mal";
                        break;
                    case 3:         //success
                        divMessage.Attributes.Add("class", "alert alert-success");
                        h4.InnerText = "¡Bien Hecho!";
                        break;
                    case 4:         //info
                        divMessage.Attributes.Add("class", "alert alert-info");
                        h4.InnerText = "¡Algo esta mal!";
                        break;
                    default:
                        divMessage.Attributes.Add("class", "");
                        h4.InnerText = "";
                        break;
                }

                //h4.InnerText = header;
                divMessage.Controls.Add(h4);

                //p.InnerText = message;
                divMessage.InnerText = message;
            }
            upMessage.Update();
        }

        public void PersonInfo(System.String residentName, System.String residenceName, System.String gradeName)
        {
            if (residenceName != string.Empty)
            {
                lblFullName.Text = residentName;
                lblResidence.Text = " || " + residenceName;
                lblGrade.Text = " || " + gradeName;
            }
            else
            {
                lblFullName.Text = string.Empty;
                lblResidence.Text = string.Empty;
                lblGrade.Text = string.Empty;
            }
            upPersonInfo.Update();
        }

        public void PersonInfo(System.String personName)
        {
            if (personName != string.Empty)
                lblFullName.Text = personName;
            else
                lblFullName.Text = string.Empty;
        }

        private void Initializate()
        {
            //ddlTeachingYears.Items.Clear();
            //ddlTeachingYears.DataTextField = "teachingYear";
            //ddlTeachingYears.DataValueField = "teachingYearId";
            //ddlTeachingYears.DataSource = ResTeachingYears.GetTeachingYears();
            //ddlTeachingYears.DataBind();
        }
        #endregion

        #region Buttons

        protected void btnExit_Click(object sender, EventArgs e)
        {
            Session["user"] = null;
            Session["access"] = null;
            Session["menu"] = null;
            Session["roles"] = null;

            Session.RemoveAll();
            Session.Abandon();

            FormsAuthentication.SignOut();
            Response.Redirect("/Account/LogIn.aspx", true);
        }

        protected void btnHelp_Click(object sender, EventArgs e)
        {
            Response.Write("<script language=javascript>child=window.open('/Files/UserGuide.pdf');</script>");
        }

        #endregion

        protected void ddlTeachingYears_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}