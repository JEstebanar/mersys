﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SystemSecurity.SysUsers;
using System.Web.UI.HtmlControls;

namespace Presentation
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


            } Message("", "", 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">Message you want to display</param>
        /// <param name="header">Message header</param>
        /// <param name="type">1 - Warning Message, || 2 - Error Message, || 3 - Message of Success, || 4 - Message Information</param>
        public void Message(System.String message, System.String header, int? type)
        {
            HtmlGenericControl h4 = new HtmlGenericControl("h4");
            //HtmlGenericControl p = new HtmlGenericControl("p");

            if (type == null || type > 4 || type <= 0)
            {
                divMessage.Controls.Remove(h4);

                //divMessage.Controls.Remove(p);

                divMessage.Attributes.Add("class", "");
                divMessage.Visible = false;
                return;
            }
            else
            {
                divMessage.Visible = true;
                
                switch (type)
                {
                    case 1:         //warning
                        divMessage.Attributes.Add("class", "alert alert-block");
                        h4.InnerText = "Advertencia";
                        break;
                    case 2:         //error
                        divMessage.Attributes.Add("class", "alert alert-error");
                        h4.InnerText = "Algo esta mal";
                        break;
                    case 3:         //success
                        divMessage.Attributes.Add("class", "alert alert-success");
                        h4.InnerText = "¡Bien Hecho!";
                        break;
                    case 4:         //info
                        divMessage.Attributes.Add("class", "alert alert-info");
                        h4.InnerText = "¡Algo esta mal!";
                        break;
                    default:
                        divMessage.Attributes.Add("class", "");
                        h4.InnerText = "";
                        break;
                }

                //h4.InnerText = header;
                divMessage.Controls.Add(h4);

                //p.InnerText = message;
                divMessage.InnerText=message;
            }
            upMessage.Update();
        }

        //public void modal()
        //{
        //    //HtmlGenericControl p = new HtmlGenericControl("p");
        //    //p.InnerText = "algo...............................";
        //    //divContent.Controls.Add(p);
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal(); });", true);
        //}
    }
}
