﻿<%@  Language="C#" MasterPageFile="~/medicenter.Master" AutoEventWireup="true" CodeBehind="Post.aspx.cs"
    Inherits="Presentation.Entradas.Post" %>
<%--<%@ MasterType VirtualPath="~/medicenter.Master" %>--%>
<%@ Register Src="~/Entradas/UserControls/wucSidebar.ascx" TagName="wucSidebar" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page relative">
        <div class="page_layout page_margin_top clearfix">
            <div class="page_header clearfix">
                <div class="page_header_left">
                    <h1 class="page_title">
                        Artículos</h1>
                    <ul class="bread_crumb">
                        <%--<li><a href="../home.aspx" title="Inicio">Inicio </a></li>
                        <li class="separator icon_small_arrow right_gray">&nbsp; </li>--%>
                        <li><a href="../../../Articulos2.aspx" title="Artículos">Artículos </a></li>
                        <li class="separator icon_small_arrow right_gray">&nbsp; </li>
                        <li>
                            <label id="lblTitlePath" runat="server">
                            </label>
                        </li>
                    </ul>
                </div>
                <%--<div class="page_header_right">
							<form class="search">
								<input class="search_input" type="text" value="To search type and hit enter..." placeholder="To search type and hit enter..." />
							</form>
						</div>--%>
            </div>
            <div class="page_left">
                <ul class="blog clearfix">
                    <li class="post single">
                        <ul class="comment_box">
                            <li class="date clearfix animated_element animation-slideRight">
                                <div class="value">
                                    <label id="lblDate" runat="server">
                                    </label>
                                </div>
                                <div class="arrow_date">
                                </div>
                            </li>
                            <%--<li class="comments_number animated_element animation-slideUp duration-300 delay-500">
                                <a href="#comments_list">
                                    <label id="lblComments" runat="server">
                                    </label>
                                </a></li>--%>
                            <li class="comments_number animated_element animation-slideDown duration-500 delay-700">
                                <a href="#comments_list">
                                    <label id="lblVisits" runat="server">
                                    </label>
                                </a></li>
                        </ul>
                        <div class="post_content">
                            <div class="gallery_box">
                                <%--<img src="/Files/Posts/images/MSP-y-LMD-unen-esfuerzos-para-prevenir-Chikungunya.JPG"
                                    alt="" />--%>
                                <a class="post_image" href="#" title="">
                                    <asp:Image ID="imgPost" runat="server" /></a>
                            </div>
                            <div class="ads_h70">
                                <h3>
                                    Espacio disponible, 100% x 70px</h3>
                                <p>
                                    hola@residenciasmedicasrd.com</p>
                            </div>
                            <h2>
                                <label id="lblTitle" runat="server">
                                </label>
                            </h2>
                            <div id="divContent" runat="server">
                                <%--<a title="Leave a reply" href="#comment_form" class="more reply_button">
										Leave a reply &rarr;
									</a>--%>
                            </div>
                            <div class="post_footer clearfix">
                                <%--<ul class="post_footer_details">
											<li>Posted in </li>
											<li>
												<a href="#" title="General">
													General,
												</a>
											</li>
											<li>
												<a href="#" title="Dental clinic">
													Dental clinic
												</a>
											</li>
										</ul>--%>
                                <ul class="post_footer_details">
                                    <li>Publicado por </li>
                                    <li>
                                        <label id="lblAuthor" runat="server">
                                        </label>
                                    </li>
                                </ul>
                                <%--<ul class="post_footer_details">
											<li>Post type </li>
											<li>
												<a href="#" title="Image">
													Image
												</a>
											</li>
										</ul>--%>
                            </div>
                            <div id="divSocial" runat="server">
                            </div>
                            <br />
                            <div id="divComments" runat="server">
                            </div>
                        </div>
                    </li>
                </ul>
                
                <%--<div class="comments clearfix page_margin_top">
							<div class="comment_box">
								<div class="comments_number animated_element animation-slideRight">
									<a href="post.html#comments_list" title="2 comments">
										2 comments
									</a>
									<div class="arrow_comments"></div>
								</div>
							</div>
							<div id="comments_list">
								<ul>
									<li class="comment clearfix">
										<div class="comment_author_avatar">
											&nbsp;
										</div>
										<div class="comment_details">
											<div class="posted_by">
												Posted by <a class="author" href="#" title="Jonh Doe">John Doe</a> on 16 Feb 2012, 2.24 pm
											</div>
											<p>
												Donec ipsum diam, pretium mollis dapibus risus. Nullam tindun pulvinar at interdum eget, suscipit eget felis. Pellentesque est faucibus tincidunt risus id interdum primis orci cubilla.
											</p>
											<a class="more reply_button" href="#comment_form">
												Reply &rarr;
											</a>
										</div>
									</li>
									<li class="comment clearfix">
										<div class="comment_author_avatar">
											&nbsp;
										</div>
										<div class="comment_details">
											<div class="posted_by">
												Posted by <a class="author" href="#" title="Jonh Doe">John Doe</a> on 16 Feb 2012, 2.24 pm
											</div>
											<p>
												Donec ipsum diam, pretium mollis dapibus risus. Nullam tindun pulvinar at interdum eget, suscipit eget felis. Pellentesque est faucibus tincidunt risus id interdum primis orci cubilla.
											</p>
											<a class="more reply_button" href="#comment_form">
												Reply &rarr;
											</a>
										</div>
										<ul class="children">
											<li class="comment clearfix">
												<div class="comment_author_avatar">
													&nbsp;
												</div>
												<div class="comment_details">
													<div class="posted_by">
														Posted by <a class="author" href="#" title="Jonh Doe">John Doe</a> on 16 Feb 2012, 2.24 pm
													</div>
													<p>
														Donec ipsum diam, pretium mollis dapibus risus. Nullam tindun pulvinar at interdum eget, suscipit eget felis. Pellentesque est faucibus tincidunt risus.
													</p>
													<a class="more reply_button" href="#comment_form">
														Reply &rarr;
													</a>
												</div>
											</li>
											<li class="comment clearfix">
												<div class="comment_author_avatar">
													&nbsp;
												</div>
												<div class="comment_details">
													<div class="posted_by">
														Posted by <a class="author" href="#" title="Jonh Doe">John Doe</a> on 16 Feb 2012, 2.24 pm
													</div>
													<p>
														Donec ipsum diam, pretium mollis dapibus risus. Nullam tindun pulvinar at interdum eget, suscipit eget felis. Pellentesque est faucibus tincidunt risus.
													</p>
													<a class="more reply_button" href="#comment_form">
														Reply &rarr;
													</a>
												</div>
											</li>
										</ul>
									</li>
								</ul>
								<ul class="pagination">
									<li class="selected">
										<a href="#" title="">
											1
										</a>
									</li>
									<li>
										<a href="#" title="">
											2
										</a>
									</li>
									<li>
										<a href="#" title="">
											3
										</a>
									</li>
								</ul>
							</div>
							<div class="comment_form_container">
								<h3 class="box_header">
									Leave a reply
								</h3>
								<form class="comment_form" id="comment_form" method="post" action="post.html">
									<fieldset class="left">
										<label class="first">Your Name</label>
										<input class="text_input" name="name" type="text" value="" />
										<label>Your Email</label>
										<input class="text_input" name="email" type="text" value="" />
									</fieldset>
									<fieldset class="right">
										<label class="first">Message</label>
										<textarea name="message"></textarea>
										<input type="submit" value="Send" class="more blue" />
										<a href="#cancel" id="cancel_comment" title="Cancel reply">Cancel reply</a>
									</fieldset>
								</form>
							</div>
						</div>--%>
            </div>
            <div class="page_right">
                <uc1:wucSidebar ID="wucSidebar1" runat="server" />
            </div>
        </div>
    </div>
    <script type="text/javascript">
        window.___gcfg = { lang: 'es' };

        (function () {
            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            po.src = 'https://apis.google.com/js/platform.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
        })();
    </script>
</asp:Content>
