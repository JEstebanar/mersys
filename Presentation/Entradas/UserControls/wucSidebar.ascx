﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="wucSidebar.ascx.cs"
    Inherits="Presentation.Entradas.UserControls.wucSidebar" %>
<!-- Categories -->
<%--<h3 class="box_header margin">
    Categorías
</h3>
<div class="sidebar_box first">
    <ul class="categories clearfix page_margin_top">
        <li><a href="#" title="General">General </a></li>
        <li><a href="#" title="News">News </a></li>
        <li><a href="#" title="Primary health">Primary health </a></li>
        <li><a href="#" title="Pediatric clinic">Pediatric clinic </a></li>
        <li><a href="#" title="Outpatient surgery">Outpatient surgery </a></li>
        <li><a href="#" title="Cardiac clinic">Cardiac clinic </a></li>
        <li><a href="#" title="Laryngological clinic">Laryngological clinic </a></li>
        <li><a href="#" title="Health">Health </a></li>
        <li><a href="#" title="Dental clinic">Dental clinic </a></li>
        <li><a href="#" title="Ophthalmology clinic">Ophthalmology clinic </a></li>
    </ul>
</div>--%>

<!-- Hottest Posts -->
<div class="sidebar_box first" style="margin-top: 27px;">
    <asp:Repeater ID="rptHottestPost" runat="server">
        <HeaderTemplate>
            <div class="clearfix">
                <div class="header_left">
                    <h3 class="box_header">
                        Más Vistas esta Semana
                    </h3>
                </div>
                <div class="header_right">
                    <%--<a href="#" id="most_viewed_this_week_prev" class="scrolling_list_control_left icon_small_arrow left_black"></a>
                    <a href="#" id="most_viewed_this_week_next" class="scrolling_list_control_right icon_small_arrow right_black"></a>--%>

                    <a href="#" id="scrolling_list_0_prev" class="scrolling_list_control_left icon_small_arrow left_black"></a>
					<a href="#" id="scrolling_list_0_next" class="scrolling_list_control_right icon_small_arrow right_black"></a>
                    
                </div>
            </div>
            <div class="scrolling_list_wrapper">
                <%--<ul class="scrolling_list most_viewed">--%>
                <ul class="scrolling_list scrolling_list_0">
        </HeaderTemplate>
        <ItemTemplate>
            <li class="icon_small_arrow right_black"><a href="<%# Eval("postURL")%>" class="clearfix">
                <span class="left">
                    <%# Eval("title")%></span><span class="number"><%# Eval("visits")%>
                    </span></a>
                <abbr title="<%# Eval("createdDate")%>" class="timeago">
                    <%# Eval("createdDate")%></abbr>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul> </div>
        </FooterTemplate>
    </asp:Repeater>
</div>

<!-- Latest Posts -->
<div class="sidebar_box">
    <asp:Repeater ID="rptLatestPosts" runat="server">
        <HeaderTemplate>
            <div class="clearfix">
                <div class="header_left">
                    <h3 class="box_header">
                        Últimas Noticias
                    </h3>
                </div>
                <div class="header_right">
                    <a href="#" id="most_commented_prev" class="scrolling_list_control_left icon_small_arrow left_black">
                    </a><a href="#" id="most_commented_next" class="scrolling_list_control_right icon_small_arrow right_black">
                    </a>
                </div>
            </div>
            <div class="scrolling_list_wrapper">
                <ul class="scrolling_list most_commented">
        </HeaderTemplate>
        <ItemTemplate>
            <li class="icon_small_arrow right_black"><a href="<%# Eval("postURL")%>" class="clearfix">
                <span class="left">
                    <%# Eval("title")%>
                </span><%--<span class="number">
                    <%# Eval("postComments")%>
                </span>--%></a>
                <abbr title="<%# Eval("createdDate")%>" class="timeago">
                    <%# Eval("createdDate")%></abbr>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul> </div></FooterTemplate>
    </asp:Repeater>
</div>

<!-- Most Comments -->
<!--<div class="sidebar_box">
    <asp:Repeater ID="rptPostsMostCommented" runat="server">
        <HeaderTemplate>
            <div class="clearfix">
                <div class="header_left">
                    <h3 class="box_header">
                        Más Comentados
                    </h3>
                </div>
                <%--        <div class="header_right">
            <a href="#" id="most_commented_prev" class="scrolling_list_control_left icon_small_arrow left_black">
            </a><a href="#" id="most_commented_next" class="scrolling_list_control_right icon_small_arrow right_black">
            </a>
        </div>--%>
            </div>
            <div class="scrolling_list_wrapper">
                <ul class="scrolling_list most_commented">
        </HeaderTemplate>
        <ItemTemplate>
            <li class="icon_small_arrow right_black"><a href="<%# Eval("postURL")%>" class="clearfix">
                <span class="left">
                    <%# Eval("title")%>
                </span><span class="number">
                    <%# Eval("postComments")%>
                </span></a>
                <abbr title="<%# Eval("createdDate")%>" class="timeago">
                    <%# Eval("createdDate")%></abbr>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul> </div></FooterTemplate>
    </asp:Repeater>
</div>-->

<!-- Ads -->
<div class="sidebar_box">
<script type="text/javascript"><!--
    google_ad_client = "ca-pub-3240367647985224";
    /* Articles_300x250 */
    google_ad_slot = "5763111595";
    google_ad_width = 300;
    google_ad_height = 250;
//-->
</script>
<script type="text/javascript"
src="//pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</div>

<!-- Most Visited -->
<div class="sidebar_box">
    <asp:Repeater ID="rptPostsMostVisited" runat="server">
        <HeaderTemplate>
            <div class="clearfix">
                <div class="header_left">
                    <h3 class="box_header">
                        Más Vistas
                    </h3>
                </div>
                <div class="header_right">
                    <a href="#" id="most_viewed_prev" class="scrolling_list_control_left icon_small_arrow left_black">
                    </a><a href="#" id="most_viewed_next" class="scrolling_list_control_right icon_small_arrow right_black">
                    </a>
                </div>
            </div>
            <div class="scrolling_list_wrapper">
                <ul class="scrolling_list most_viewed">
        </HeaderTemplate>
        <ItemTemplate>
            <li class="icon_small_arrow right_black"><a href="<%# Eval("postURL")%>" class="clearfix">
                <span class="left">
                    <%# Eval("title")%></span><span class="number"><%# Eval("visits")%>
                    </span></a>
                <abbr title="<%# Eval("createdDate")%>" class="timeago">
                    <%# Eval("createdDate")%></abbr>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul> </div>
        </FooterTemplate>
    </asp:Repeater>
</div>

<!-- Photostream -->
<%--<div class="sidebar_box">
    <h3 class="box_header">
        Photostream
    </h3>
    <ul class="photostream clearfix">
        <li class="gallery_box">
            <img src="images/samples/75x75/image_01.jpg" alt="" />
            <ul class="controls">
                <li><a href="images/samples/image_01.jpg" rel="photostream" class="fancybox open_lightbox">
                </a></li>
            </ul>
        </li>
        <li class="gallery_box">
            <img src="images/samples/75x75/image_02.jpg" alt="" />
            <ul class="controls">
                <li><a href="images/samples/image_02.jpg" rel="photostream" class="fancybox open_lightbox">
                </a></li>
            </ul>
        </li>
        <li class="gallery_box">
            <img src="images/samples/75x75/image_03.jpg" alt="" />
            <ul class="controls">
                <li><a href="images/samples/image_03.jpg" rel="photostream" class="fancybox open_lightbox">
                </a></li>
            </ul>
        </li>
        <li class="gallery_box">
            <img src="images/samples/75x75/image_04.jpg" alt="" />
            <ul class="controls">
                <li><a href="images/samples/image_04.jpg" rel="photostream" class="fancybox open_lightbox">
                </a></li>
            </ul>
        </li>
        <li class="gallery_box">
            <img src="images/samples/75x75/image_05.jpg" alt="" />
            <ul class="controls">
                <li><a href="images/samples/image_05.jpg" rel="photostream" class="fancybox open_lightbox">
                </a></li>
            </ul>
        </li>
        <li class="gallery_box">
            <img src="images/samples/75x75/image_06.jpg" alt="" />
            <ul class="controls">
                <li><a href="images/samples/image_06.jpg" rel="photostream" class="fancybox open_lightbox">
                </a></li>
            </ul>
        </li>
        <li class="gallery_box">
            <img src="images/samples/75x75/image_07.jpg" alt="" />
            <ul class="controls">
                <li><a href="images/samples/image_07.jpg" rel="photostream" class="fancybox open_lightbox">
                </a></li>
            </ul>
        </li>
        <li class="gallery_box">
            <img src="images/samples/75x75/image_08.jpg" alt="" />
            <ul class="controls">
                <li><a href="images/samples/image_08.jpg" rel="photostream" class="fancybox open_lightbox">
                </a></li>
            </ul>
        </li>
    </ul>
</div>--%>
<!-- Archives -->
<div class="sidebar_box">
    <h3 class="box_header">
        Archivos
    </h3>
    <ul class="columns list clearfix">
        <li class="column_left icon_small_arrow right_black"><a href="#" title="Diciembre 2013">
            Diciembre 2013 </a></li>
        <li class="column_right icon_small_arrow right_black"><a href="#" title="Enero 2014">
            Enero 2014 </a></li>
        <li class="column_left icon_small_arrow right_black"><a href="#" title="Febrero 2012">
            Febrero 2014 </a></li>
        <li class="column_right icon_small_arrow right_black"><a href="#" title="Marzo 2014">
            Marzo 2014 </a></li>
        <li class="column_left icon_small_arrow right_black"><a href="#" title="Abril 2014">
            Abril 2014 </a></li>
        <li class="column_right icon_small_arrow right_black"><a href="#" title="Mayo 2014">
            Mayo 2014 </a></li>
    </ul>
</div>

<!-- Regulations -->
<div class="sidebar_box">
    <h3 class="box_header">
        Reglamentos
    </h3>
    <p>
        Los programas de Residencias Médicas, dirigidos al personal médico, tienen su punto
        de inicio en el 1968 desde el interior de los hospitales públicos, dependen...
    </p>
    <a title="Leer más" href="/Files/Reglamentos-Consejo-Nac-Residencia-Med.pdf"
        target="_blank">Leer más → </a>
</div>
