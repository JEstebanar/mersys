﻿using System;
using Posts.Posts;

namespace Presentation.Entradas.UserControls
{
    public partial class wucSidebar : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptHottestPost.DataSource = PosPosts.GetMostVisitedthisWeek();
                rptHottestPost.DataBind();

                rptLatestPosts.DataSource = PosPosts.GetLatestNews();
                rptLatestPosts.DataBind();

                rptPostsMostVisited.DataSource = PosPosts.GetMostVisited();
                rptPostsMostVisited.DataBind();

                //rptPostsMostCommented.DataSource = PosPosts.GetMostCommented();
                //rptPostsMostCommented.DataBind();

            }
        }
    }
}