﻿using System;
using General.Utilities;
using Posts.Posts;
using System.Web.UI.HtmlControls;
using General.GeneralCommons;

namespace Presentation.Entradas
{
    public partial class Post : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Request.QueryString.Get("id") != null)
            {
                int postId = Convert.ToInt32(Request.QueryString.Get("id"));
                PosPosts cPost = new PosPosts();
                PosPosts.SetPostVisit(postId);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString.Get("id") != null)
                {
                    int id = Convert.ToInt32(Request.QueryString.Get("id"));
                    DisplayBlog(id);
                }
            }
        }

        private void DisplayBlog(int id)
        {
            PosPosts.ResidentsPosts cPost = PosPosts.GetByPostId(id);

            Page.Title = cPost.title;
            Page.MetaDescription = cPost.title;
            Page.MetaKeywords = cPost.tags;

            lblTitlePath.InnerText = cPost.title;
            lblTitle.InnerText = cPost.title;
            divContent.InnerHtml = cPost.body;
            if (cPost.postImage == "")
                imgPost.Visible = false;
            else
                imgPost.ImageUrl = cPost.postImage;
            lblDate.InnerText = cPost.createdDate;
            //lblComments.InnerText = cPost.comments;
            lblVisits.InnerText = cPost.visits;
            lblAuthor.InnerText = cPost.author;
            //tags

            string url = "http://residenciasmedicasrd.com" + GenUtilities.GenerateURL(cPost.title, cPost.postId, GeneralCommon.PostTypes.Articles);
            HtmlGenericControl divfb = new HtmlGenericControl("div");
            divfb.Attributes.Add("class", "fb-like");
            divfb.Attributes.Add("data-href", url);
            divfb.Attributes.Add("data-layout", "button_count");
            divfb.Attributes.Add("data-action", "like");
            divfb.Attributes.Add("data-show-faces", "true");
            divfb.Attributes.Add("data-share", "true");
            divSocial.Controls.Add(divfb);
            
            HtmlGenericControl divfbcomments = new HtmlGenericControl("div");
            divfbcomments.Attributes.Add("class", "fb-comments");
            divfbcomments.Attributes.Add("data-href", url);
            divfbcomments.Attributes.Add("data-numposts", "5");
            divfbcomments.Attributes.Add("data-colorscheme", "light");
            divComments.Controls.Add(divfbcomments);

            //<div class="fb-comments" data-href="http://residenciasmedicasrd.com/Entradas/Articulos/111/cuanto-viven-los-espermatozoides-despues-de-la-eyaculacion" 
            //data-numposts="5" data-colorscheme="light"></div>

            //HtmlGenericControl divg1 = new HtmlGenericControl("div");
            //divg1.Attributes.Add("class", "g-plusone");
            ////divg1.Attributes.Add("data-size", "medium");
            //divSocial.Controls.Add(divg1);

            //<div><div class="fb-like" data-href="http://residenciasmedicasrd.com/" data-layout="button"
            //                        data-action="like" data-show-faces="true" data-share="true">
            //                    </div>

            //<div class="fb-like" data-href="http://residenciasmedicasrd.com/Entradas/Articulos/15/salud-p%C3%BAblica-extiende-hasta-las-7-pm-consultas-de-unap-para-atender-casos-febriles"
            //                    data-layout="button" data-action="like" data-show-faces="true" data-share="true">
            //                </div>


            //BlogDAL objBlogDAL = new BlogDAL();
            //Blog objBlog = objBlogDAL.GetBlogById(id);
            //divTitle.InnerHtml = objBlog.Title;
            //spVisitor.InnerHtml = "Total Visits " + objBlog.Visit;
            //divContent.InnerHtml = objBlog.Content;
        }
    }
}