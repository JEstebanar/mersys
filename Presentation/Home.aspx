﻿<%@ Page Title="Inicio" Language="C#" MasterPageFile="~/medicenter.Master" AutoEventWireup="true"
    CodeBehind="Home.aspx.cs" Inherits="Presentation.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- slider -->
    <ul class="slider">
        <li style="background-image: url('medicenter/images/slider/img1.jpg');">&nbsp; </li>
        <li style="background-image: url('medicenter/images/slider/img2.jpg');">&nbsp; </li>
        <li style="background-image: url('medicenter/images/slider/img3.jpg');">&nbsp; </li>
    </ul>
    <div class="page relative noborder">
        <!--<div class="top_hint">
					Give us a call: +123 356 123 124
				</div>-->
        <!-- slider content -->
        <div class="slider_content_box clearfix">
            <div class="slider_content" style="display: block;">
                <h1 class="title">
                    Enfócate<br />
                    en lo que<br />
                    quieres
                </h1>
                <h2 class="subtitle">
                    No existen límites para tus sueños
                    <br />
                </h2>
            </div>
            <div class="slider_content">
                <h1 class="title">
                    Interés por<br />
                    las personas
                </h1>
                <h2 class="subtitle">
                    Muestra lo profesional que eres
                    <br />
                    con tus pacientes
                </h2>
            </div>
            <div class="slider_content">
                <h1 class="title">
                    Transmite
                    <br />
                    tu pasión
                </h1>
                <h2 class="subtitle">
                    El camino es largo pero tus deseos
                    <br />
                    y sueños son más fuertes
                </h2>
            </div>
            <!--<div class="slider_navigation">
						<div class="slider_bar"></div>
						<a class="slider_control" id="slide_1_control" href="#" title="1">
							<span class="top_border"></span>
							<span class="slider_control_bar"></span>
							1
						</a>
						<a class="slider_control" id="slide_2_control" href="#" title="2">
							<span class="top_border"></span>
							<span class="slider_control_bar"></span>
							2
						</a>
						<a class="slider_control" id="slide_3_control" href="#" title="3">
							<span class="top_border"></span>
							<span class="slider_control_bar"></span>
							3
						</a>
					</div>-->
        </div>
        <!-- home box -->
        <ul class="home_box_container clearfix">
            <li class="home_box light_blue">
                <h2>
                    <a href="#" title="Especialidades">Especialidades </a>
                </h2>
                <div class="news clearfix">
                    <p class="text">
                        Si ya eres medico general con deseos de especializarse en alguna especialidad, aquí
                        puedes encontrar la información necesaria.
                    </p>
                    <a class="more light icon_small_arrow margin_right_white" href="/Residencies.aspx" title="Leer más">
                        Leer más</a>
                </div>
            </li>
            <li class="home_box blue">
                <h2>
                    <a href="#" title="Casos de Emergencias">Sub Especialidades </a>
                </h2>
                <div class="news clearfix">
                    <p class="text">
                        Conoce todo lo que necesitas saber para hacer una sub especialidad en uno de los
                        centros de residencies del país.
                    </p>
                    <a class="more light icon_small_arrow margin_right_white" href="/Residencies.aspx" title="Leer más">
                        Leer más</a>
                </div>
            </li>
            <li class="home_box dark_blue">
                <h2>
                    <a href="#" title="Casos de Emergencias">Récord de Notas </a>
                </h2>
                <div class="news clearfix">
                    <p class="text">
                        Si ya hiciste tu especialidad en alguno de los centros afiliados a esta herramienta,
                        puedes visualizar tu récord de notas completo.
                    </p>
                    <a class="more light icon_small_arrow margin_right_white" href="/Main.aspx" title="Leer más">
                        Ver Récord de Notas</a>
                </div>
            </li>
        </ul>
        <div class="page_layout page_margin_top_section clearfix">
            <div class="page_left">
                <h3 class="box_header">
                    Últimas Noticias
                </h3>
                <ul class="blog clearfix">
                    <%--                    <li class="post">
                        <ul class="comment_box">
                            <li class="date clearfix animated_element animation-slideRight">
                                <div class="value">
                                    02 MAY 14</div>
                                <div class="arrow_date">
                                </div>
                            </li>
                            <li class="comments_number animated_element animation-slideUp duration-300 delay-500">
                                <a href="Articulos/MSP-y-LMD-unen-esfuerzos-para-prevenir-Chikungunya-1#comments_list" title="0 comentarios">0 comentarios </a></li>
                        </ul>
                        <div class="post_content">
                            <a class="post_image" href="Articulos/MSP-y-LMD-unen-esfuerzos-para-prevenir-Chikungunya-1" title="MSP y LMD unen esfuerzos para prevenir Chikungunya">
                                <img src="Files/Posts/images/MSP-y-LMD-unen-esfuerzos-para-prevenir-Chikungunya.JPG" alt="" />
                            </a>
                            <h2>
                                <a href="Articulos/MSP-y-LMD-unen-esfuerzos-para-prevenir-Chikungunya-1" title="MSP y LMD unen esfuerzos para prevenir Chikungunya">MSP y LMD unen esfuerzos para prevenir Chikungunya </a>
                            </h2>
                            <p>
                                El Ministerio de Salud Pública y Asistencia Social y la Liga Municipal Dominicana
                                (LMD) anunciaron esfuerzos conjuntos para prevenir y contener en el país el virus
                                Chikungunya. 
                            </p>
                            <p>
                                El Ministro de Salud Pública, doctor Freddy Hidalgo Núñez, y el secretario
                                de la Liga Municipal Dominicana, Ingeniero Johnny Jonnes, informaron la integración
                                coordinada de los ayuntamientos a las jornadas de eliminación de criaderos de mosquitos
                                y la ejecución de las medidas de contención de brotes.
                            </p>
                            <a title="Leer más" href="Articulos/1/MSP-y-LMD-unen-esfuerzos-para-prevenir-Chikungunya" class="more">Leer más &rarr; </a>--%>
                    <%--<div class="post_footer clearfix">
                                <ul class="post_footer_details">
                                    <li>Posted in </li>
                                    <li><a href="#" title="General">General, </a></li>
                                    <li><a href="#" title="Dental clinic">Dental clinic </a></li>
                                </ul>
                                <ul class="post_footer_details">
                                    <li>Posted by </li>
                                    <li><a href="#" title="John Doe">John Doe </a></li>
                                </ul>
                                <ul class="post_footer_details">
                                    <li>Post type </li>
                                    <li><a href="#" title="Image">Image </a></li>
                                </ul>
                            </div>--%>
                    <%--</div>
                    </li>--%>
                    <asp:Repeater ID="rptPosts" runat="server">
                        <ItemTemplate>
                        <li class="post">
                            <ul class="comment_box">
                                <li class="date clearfix animated_element animation-slideRight">
                                    <div class="value">
                                        <%# Eval("createdDate")%></div>
                                    <div class="arrow_date">
                                    </div>
                                </li>
                                <%--<li class="comments_number animated_element animation-slideUp duration-300 delay-500">
                                    <a href="<%# Eval("postURL")%>#comments_list"
                                        title=<%# Eval("comments")%>><%# Eval("comments")%> </a></li>--%>
                                <li class="comments_number animated_element animation-slideDown duration-500 delay-700">
                                    <a href="<%# Eval("postURL")%>#comments_list"
                                        title=<%# Eval("visits")%>><%# Eval("visits")%> </a></li>
                            </ul>
                            <div class="post_content">
                                <a class="post_image" href="<%# Eval("postURL")%>"
                                    title="<%# Eval("title")%>">
                                   
                                    <%--<img src="<%# Eval("postImage")%>" alt="" />--%>
                                   
                                    <asp:Image ID="newsImage" runat="server" Visible='<%# Eval("postImage").ToString() != "" %>'  ImageUrl='<%# Eval("postImage") %>' />
                                </a>
                                <h2>
                                    <a href="<%# Eval("postURL")%>" title="<%# Eval("title")%>">
                                        <%# Eval("title")%> </a>
                                </h2>
                               
                                    <%#Eval("body").ToString().Trim().Length <= 500 ? Eval("body") : Eval("body").ToString().Remove(500) %>
                                
                                <a title="Leer más" href="<%# Eval("postURL")%>"
                                    class="more">Leer más &rarr; </a>
                                <%--<div class="post_footer clearfix">
                                <ul class="post_footer_details">
                                    <li>Posted in </li>
                                    <li><a href="#" title="General">General, </a></li>
                                    <li><a href="#" title="Dental clinic">Dental clinic </a></li>
                                </ul>
                                <ul class="post_footer_details">
                                    <li>Posted by </li>
                                    <li><a href="#" title="John Doe">John Doe </a></li>
                                </ul>
                                <ul class="post_footer_details">
                                    <li>Post type </li>
                                    <li><a href="#" title="Image">Image </a></li>
                                </ul>
                            </div>--%>
                            </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%--<li class="post">
                        <ul class="comment_box clearfix">
                            <li class="date clearfix animated_element animation-slideRight">
                                <div class="value">
                                    10 DEC 12</div>
                                <div class="arrow_date">
                                </div>
                            </li>
                            <li class="comments_number animated_element animation-slideUp duration-300 delay-500">
                                <a href="post.html#comments_list" title="5 comments">5 comments </a></li>
                        </ul>
                        <div class="post_content">
                            <h2>
                                <a target="_blank" href="http://themeforest.net/item/medicenter-responsive-medical-health-template/4000598?ref=QuanticaLabs"
                                    title="Lorem ipsum dolor sit amat velum">Lorem ipsum dolor sit amat velum </a>
                            </h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut volutpat rutrum eros
                                amet sollicitudin interdum. Suspendisse pulvinar, velit nec pharetra interdum, ante
                                tellus ornare mi, et mollis tellus neque vitae elit. Mauris adipiscing mauris fringilla
                                turpis interdum sed pulvinar nisi malesuada. Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit.
                            </p>
                            <a target="_blank" title="Visit link" href="http://themeforest.net/item/medicenter-responsive-medical-health-template/4000598?ref=QuanticaLabs"
                                class="more" target="_blank">Visit link &rarr; </a>
                            <div class="post_footer clearfix">
                                <ul class="post_footer_details">
                                    <li>Posted in</li>
                                    <li><a href="#" title="General">General, </a></li>
                                    <li><a href="#" title="Outpatient surgery">Outpatient surgery </a></li>
                                </ul>
                                <ul class="post_footer_details">
                                    <li>Posted by </li>
                                    <li><a href="#" title="John Doe">John Doe </a></li>
                                </ul>
                                <ul class="post_footer_details">
                                    <li>Post type </li>
                                    <li><a href="#" title="Link">Link </a></li>
                                </ul>
                            </div>
                        </div>
                    </li>--%>
                </ul>
                <div class="show_all clearfix">
                    <a class="more" href="Articulos2.aspx" title="Show all">Mostrar todo &rarr; </a>
                </div>
            </div>
            <div class="page_right">
                <div class="sidebar_box first">
                    <h3 class="box_header">
                        Especialidades
                    </h3>
                    <ul class="accordion">
                        <li>
                            <div id="accordion-pathology">
                                <h3>
                                    Anatomía Patológica</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" title="Anatomía Patológica">
                                        <img src="medicenter/images/samples/75x75/image_08.jpg" alt="" />
                                    </a>
                                    <p>
                                        La Anatomía Patológica es la rama de la Medicina que se ocupa del estudio, por medio
                                        de técnicas morfológicas, de las causas, desarrollo y consecuencias de las enfermedades.
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white" href="Residencies.aspx#pathology"
                                        title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-anesthesiology">
                                <h3>
                                    Anestesiología</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Pediatric Clinic">
                                        <img src="medicenter/images/samples/75x75/image_05.jpg" alt="" />
                                    </a>
                                    <p>
                                        La anestesiología es la especialidad médica dedicada a la atención y cuidados especiales
                                        de los pacientes durante las intervenciones quirúrgicas u otros procesos que puedan
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white" href="Residencies.aspx#anesthesiology"
                                        title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-cardiology">
                                <h3>
                                    Cardiología</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Cardiología">
                                        <img src="medicenter/images/samples/75x75/image_07.jpg" alt="" />
                                    </a>
                                    <p>
                                        La cardiología es la rama de la medicina interna, encargada de las enfermedades
                                        del corazón y del aparato circulatorio. Se incluye dentro de las especialidades
                                        médicas quirúrgicas.
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white" href="Residencies.aspx#cardiology"
                                        title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-cervico-buco-maxilo-facial">
                                <h3>
                                    Cervico Buco Maxilo Facial</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Laryngological Clinic">
                                        <img src="medicenter/images/samples/75x75/image_08.jpg" alt="" />
                                    </a>
                                    <p>
                                        Cervico Buco Maxilo Facial es una especialidad quirúrgica que incluye el diagnóstico,
                                        cirugía y tratamientos relacionados de un gran espectro de enfermedades, heridas
                                        y aspectos estéticos de la boca, dientes, cara, cabeza y cuello.
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white" href="Residencies.aspx#cervico-buco-maxilo-facial"
                                        title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-general-surgery">
                                <h3>
                                    Cirugía General</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Cirugía General">
                                        <img src="medicenter/images/samples/75x75/image_01.jpg" alt="" />
                                    </a>
                                    <p>
                                        La cirugía general es la especialidad médica de clase quirúrgica que abarca las
                                        operaciones del aparato digestivo; incluyendo el tracto gastrointestinal y el sistema
                                        hepato-bilio-pancreático,
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white" href="Residencies.aspx#general-surgery"
                                        title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-intensive-care">
                                <h3>
                                    Cuidados Intensivos</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Cuidados Intensivos">
                                        <img src="medicenter/images/samples/75x75/image_04.jpg" alt="" />
                                    </a>
                                    <p>
                                        La medicina intensiva es una especialidad médica dedicada al suministro de soporte
                                        vital o de soporte a los sistemas orgánicos en los pacientes que están críticamente
                                        enfermos,
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white" href="Residencies.aspx#intensive-care"
                                        title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-emergenciologia">
                                <h3>
                                    Emergenciología</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Ophthalmology Clinic">
                                        <img src="medicenter/images/samples/75x75/image_06.jpg" alt="" />
                                    </a>
                                    <p>
                                        La medicina de emergencia, emergenciología, emergentología o medicina de urgencias
                                        es la que actúa sobre una emergencia médica o urgencia médica o sobre cualquier
                                        enfermedad en su momento agudo,
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white" href="Residencies.aspx#emergenciologia"
                                        title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
<%--                        <li>
                            <div id="accordion-physiatry">
                                <h3>
                                    Fisiatría</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Fisiatría">
                                        <img src="medicenter/images/samples/75x75/image_03.jpg" alt="" />
                                    </a>
                                    <p>
                                        La medicina física y rehabilitación, también llamada fisiatría, es una especialidad
                                        de la medicina y de las ciencias de la salud, configurada por un cuerpo doctrinal
                                        complejo, constituido por
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white" href="Residencies.aspx#physiatry"
                                        title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>--%>
                        <li>
                            <div id="accordion-geriatria">
                                <h3>
                                    Geriatría</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Geriatría">
                                        <img src="medicenter/images/samples/75x75/image_05.jpg" alt="" />
                                    </a>
                                    <p>
                                        La Geriatría es una especialidad médica dedicada al estudio de la prevención, el
                                        diagnóstico, el tratamiento y la rehabilitación de las enfermedades en la senectud.
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white" href="residencies.aspx#geriatria"
                                        title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-gineco-obstetricia">
                                <h3>
                                    Gineco-Obstetricia</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Gineco-Obstetricia">
                                        <img src="medicenter/images/samples/75x75/image_05.jpg" alt="" />
                                    </a>
                                    <p>
                                        La Gineco-Obstetricia es la especialidad médica dedicada a los campos de la obstetricia y la ginecología
                                        a través de un único programa de formación académica. Esta formación combinada convierte
                                        a los practicantes en 
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white"
                                            href="residencies.aspx#gineco-obstetricia" title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-hematologia">
                                <h3>
                                    Hematología</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Hematología">
                                        <img src="medicenter/images/samples/75x75/image_03.jpg" alt="" />
                                    </a>
                                    <p>
                                        es la especialidad médica que se dedica al tratamiento de los pacientes con enfermedades
                                        hematológicas, para ello se encarga del estudio e investigación de la sangre y los
                                        órganos hematopoyéticos
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white" href="Residencies.aspx#hematologia"
                                        title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>

                        <%--                            <li>
                                <div id="accordion-outpatient-rehabilitation">
                                    <h3>
                                        Outpatient Rehabilitation</h3>
                                </div>
                                <div class="clearfix">
                                    <div class="item_content clearfix">
                                        <a class="thumb_image" href="#" title="Outpatient Rehabilitation">
                                            <img src="medicenter/images/samples/75x75/image_05.jpg" alt="" />
                                        </a>
                                        <p>
                                            Mauris adisciping fringila turpis intend tellus ornare etos pelim. Pulvunar est
                                            cardio neque vitae elit. Lorem vulputat paentra nunc gravida.
                                        </p>
                                    </div>
                                    <div class="item_footer clearfix">
                                        <a class="more blue icon_small_arrow margin_right_white" href="timetable.html#outpatient-rehabilitation"
                                            title="Timetable">Timetable</a> <a class="more blue icon_small_arrow margin_right_white"
                                                href="departments.html#outpatient-rehabilitation" title="Details">Details</a>
                                    </div>
                                </div>
                            </li>--%>
                    </ul>
                </div>
                <div class="sidebar_box">
                    <h3 class="box_header">
                        Reglamentos
                    </h3>
                    <p>
                        Los programas de Residencias Médicas, dirigidos al personal médico, tienen su punto
                        de inicio en el 1968 desde el interior de los hospitales públicos, dependen...
                    </p>
                    <a title="Leer más" href="/Files/Reglamentos-Consejo-Nac-Residencia-Med.pdf" target="_blank">
                        Leer más → </a>
                </div>
                <!--Ads-->
                <div class="sidebar_box">
                    <script type="text/javascript"><!--
                        google_ad_client = "ca-pub-3240367647985224";
                        /* Articles_300x250 */
                        google_ad_slot = "5763111595";
                        google_ad_width = 300;
                        google_ad_height = 250;
//-->
</script>
<script type="text/javascript"
src="//pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
