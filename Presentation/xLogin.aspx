﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="xLogin.aspx.cs" Inherits="Presentation.Login" %>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <title></title>
    <link href="~/Styles/Login.css" rel="stylesheet" type="text/css" />
    <%--    <!-- Le styles -->
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">--%>

</head>
<body>
    <div class="container">
        <section id="content">
            <form action="" runat="server">
            <h1>
                Iniciar Sesión</h1>
            <div>
                <input type="text" runat="server" placeholder="E-Mail" required="" id="username" />
                <%--<asp:TextBox ID="txtUserName" runat="server" required="" placeholder="E-Mail" type="text"></asp:TextBox>--%>
            </div>
            <div>
                <input type="password" runat="server" placeholder="Password" required="" id="password" />
                <%--<asp:TextBox ID="txtPassword" runat="server" required="" placeholder="Contraseña"
                    type="password"></asp:TextBox>--%>
            </div>
            <div>
                <asp:Button ID="btnLogIn" runat="server" Text="Iniciar Sesión" type="sumit" 
                    onclick="btnLogIn_Click" />
                <%--<input type="submit" value="Iniciar Sesión" runat="server" id="btnLogIn" />--%>
                <%--<a href="#">Lost your password?</a> <a href="#">Register</a>--%>
            </div>
            </form>
            <!-- form -->
        </section><!-- content -->
    </div>
    <!-- container -->
</body>
</html>
