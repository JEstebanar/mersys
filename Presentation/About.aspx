﻿<%@ Page Title="Nosotros" Language="C#" MasterPageFile="~/medicenter.Master" AutoEventWireup="true"
    CodeBehind="About.aspx.cs" Inherits="Presentation.About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page relative">
        <div class="page_layout page_margin_top clearfix">
            <div class="page_header clearfix">
                <div class="page_header_left">
                    <h1 class="page_title">
                        Nosotros</h1>
                    <ul class="bread_crumb">
                        <li><a href="Home.aspx" title="Home">Inicio </a></li>
                        <li class="separator icon_small_arrow right_gray">&nbsp; </li>
                        <li>Nosotros </li>
                    </ul>
                </div>
                <%--<div class="page_header_right">
                    <form class="search">
                    <input class="search_input" type="text" value="To search type and hit enter..." placeholder="To search type and hit enter..." />
                    </form>
                </div>--%>
            </div>
            <div class="clearfix">
                <div class="gallery_item_details_list clearfix page_margin_top">
                    <div class="gallery_item_details clearfix">
                        <div class="columns no_width">
                            <div class="column_left">
                                <div class="gallery_box">
                                    <ul class="image_carousel">
                                        <li class="current_slide">
                                            <img src="medicenter/images/samples/480x300/image_08.jpg" alt="" />
                                            <ul class="controls">
                                                <li><a href="medicenter/images/samples/image_08.jpg" rel="gallery" class="fancybox open_lightbox">
                                                </a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <img src="medicenter/images/samples/480x300/image_05.jpg" alt="" />
                                            <ul class="controls">
                                                <li><a href="medicenter/images/samples/image_05.jpg" rel="gallery" class="fancybox open_lightbox">
                                                </a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="column_right">
                                <div class="details_box">
                                    <h2>
                                        Bienvenidos a MeSys
                                    </h2>
                                    <p>
                                        MeSys es el sistema de evaluación de médicos residentes que permitirá a su centro
                                        tener un control estable de las evaluaciones hechas a sus médicos residentes. Este
                                        sistema es único en su tipo ya que permite el acceso a la información de manera
                                        rápida, segura y fácil.
                                        <br />
                                        <br />
                                        El acceso es único y exclusivo para los médicos residentes que se encuentran o hicieron
                                        una especialidad o sub especialidad y las autoridades autorizadas del departamento
                                        de docencia médica.
                                        <br />
                                        <br />
                                        Su departamento de docencia tendrá un mejor control de la información de sus médicos
                                        residentes y de los servicios que les brinda a los mismos.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="columns page_margin_top full_width clearfix">
                    <div class="column_left">
                        <h3 class="box_header">
                            ¿Qué Ofrecemos?
                        </h3>
                        <!--<h3 class="sentence">
                            There's lots of people in this world who spend so much time watching their health
                            that they haven't the time to enjoy it.
                        </h3>
                        <div class="clearfix">
                            <span class="sentence_author">&#8212;&nbsp;&nbsp;Josh Billings</span>
                        </div>-->
                        <ul class="accordion medium page_margin_top clearfix">
                            <li>
                                <div id="accordion-easy-access">
                                    <h3>
                                        Fácil Acceso</h3>
                                </div>
                                <ul class="clearfix">
                                    <li class="item_content clearfix"><a class="features_image" href="#" title="">
                                        <img src="medicenter/images/features_large/mobile.png" alt="" class="animated_element animation-scale" />
                                    </a>
                                        <div class="text">
                                            No importa el dispositivo que utilice para acceder, MeSys es flexible para ajustarse
                                            a (Computadoras, Tabletas y Teléfonos Inteligentes) garantizando un acceso rápido
                                            y amigable.
                                            <br />
                                            <br />
                                            No importa el lugar donde se encuentre siempre que tenga una conexión a internet
                                            podrá visualizar todo el contenido disponible.
                                        </div>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <div id="accordion-innovation">
                                    <h3>
                                        Innovación</h3>
                                </div>
                                <ul class="clearfix">
                                    <li class="item_content clearfix"><a class="features_image" title="">
                                        <img src="medicenter/images/features_large/form.png" alt="" class="animated_element animation-scale" />
                                    </a>
                                        <div class="text">
                                            A medida que los tiempos van cambiando y con el la medicina, la información no se
                                            puede quedar atrás. Día a día este software va evolucionando para facilitar una
                                            interacción amigable con los usuarios que dispongan de su uso.
                                        </div>
                                    </li>                                    
                                </ul>
                            </li>
                            
                            <li>
                                <div id="accordion-aenean-faucibus">
                                    <h3>
                                        Aenean faucibus sapien a odio varius?</h3>
                                </div>
                                <ul class="clearfix">
                                    <li class="item_content clearfix"><a class="features_image" href="#" title="">
                                        <img src="images/features_large/document.png" alt="" class="animated_element animation-scale" />
                                    </a>
                                        <div class="text">
                                            Donec ipsum diam, pretium mollis dapibus risus. Nullam dolor nibh pulvinar at interdum
                                            eget, suscipit id felis. Pellentesque est faucibus tincidunt risus.
                                            <div class="item_footer clearfix">
                                                <a title="Read more" href="about.html" class="more">Read more &rarr; </a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item_content clearfix"><a class="features_image" href="#" title="">
                                        <img src="images/features_large/firstaid.png" alt="" class="animated_element animation-scale" />
                                    </a>
                                        <div class="text">
                                            Donec ipsum diam, pretium mollis dapibus risus. Nullam dolor nibh pulvinar at interdum
                                            eget, suscipit id felis. Pellentesque est faucibus tincidunt risus.
                                            <div class="item_footer clearfix">
                                                <a title="Read more" href="about.html" class="more">Read more &rarr; </a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <div id="accordion-donec-pilvinar">
                                    <h3>
                                        Donec pulvinar lectus quis laoreet vestibulum?</h3>
                                </div>
                                <ul class="clearfix">
                                    <li class="item_content clearfix"><a class="features_image" href="#" title="">
                                        <img src="images/features_large/heart.png" alt="" class="animated_element animation-scale" />
                                    </a>
                                        <div class="text">
                                            Donec ipsum diam, pretium mollis dapibus risus. Nullam dolor nibh pulvinar at interdum
                                            eget, suscipit id felis. Pellentesque est faucibus tincidunt risus.
                                            <div class="item_footer clearfix">
                                                <a title="Read more" href="about.html" class="more">Read more &rarr; </a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item_content clearfix"><a class="features_image" href="#" title="">
                                        <img src="images/features_large/healthcare.png" alt="" class="animated_element animation-scale" />
                                    </a>
                                        <div class="text">
                                            Donec ipsum diam, pretium mollis dapibus risus. Nullam dolor nibh pulvinar at interdum
                                            eget, suscipit id felis. Pellentesque est faucibus tincidunt risus.
                                            <div class="item_footer clearfix">
                                                <a title="Read more" href="about.html" class="more">Read more &rarr; </a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="column_right">
                        <h3 class="box_header">
                            Why Choose Medicenter?
                        </h3>
                        <ul class="page_margin_top clearfix">
                            <li class="item_content clearfix"><a class="thumb_image" href="#" title="">
                                <img src="images/samples/75x75/image_05.jpg" alt="" />
                            </a>
                                <div class="text">
                                    Donec ipsum diam, pretium mollis dapibus risus. Nullam id dolor id nibh pulvinar
                                    at interdum eget, suscipit eget felis. Pellentesque est faucibus tincidunt risus
                                    id interdum primis.
                                    <div class="item_footer clearfix">
                                        <a title="Read more" href="about.html" class="more">Read more &rarr; </a>
                                    </div>
                                </div>
                            </li>
                            <li class="item_content clearfix"><a class="thumb_image" href="#" title="">
                                <img src="images/samples/75x75/image_04.jpg" alt="" />
                            </a>
                                <div class="text">
                                    Donec ipsum diam, pretium mollis dapibus risus. Nullam id dolor id nibh pulvinar
                                    at interdum eget, suscipit eget felis. Pellentesque est faucibus tincidunt risus
                                    id interdum primis.
                                    <div class="item_footer clearfix">
                                        <a title="Read more" href="about.html" class="more">Read more &rarr; </a>
                                    </div>
                                </div>
                            </li>
                            <li class="item_content clearfix"><a class="thumb_image" href="#" title="">
                                <img src="images/samples/75x75/image_07.jpg" alt="" />
                            </a>
                                <div class="text">
                                    Donec ipsum diam, pretium mollis dapibus risus. Nullam id dolor id nibh pulvinar
                                    at interdum eget, suscipit eget felis. Pellentesque est faucibus tincidunt risus
                                    id interdum primis.
                                    <div class="item_footer clearfix">
                                        <a title="Read more" href="about.html" class="more">Read more &rarr; </a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>--%>
                <h3 class="box_header page_margin_top slide">
                    Características
                </h3>
                <div class="columns clearfix no_width page_margin_top">
                    <ul class="column_left">
                        <!--Apariencia Amigable-->
                        <li class="item_content clearfix"><a class="features_image" href="#" title="">
                            <img src="medicenter/images/features_large/mobile.png" alt="">
                        </a>
                            <div class="text">
                                <h3>
                                    Apariencia Amigable</h3>
                                No importa el dispositivo que utilice para acceder, MeSys es flexible para ajustarse
                                a (Computadoras, Tabletas y Teléfonos Inteligentes) garantizando un acceso rápido
                                y amigable.
                            </div>
                        </li>                        
                        <!--Innovación-->
                        <li class="item_content clearfix"><a class="features_image" href="#" title="">
                            <img src="medicenter/images/features_large/healthcare.png" alt="">
                        </a>
                            <div class="text">
                                <h3>
                                    Innovación</h3>
                                A medida que los tiempos van cambiando y con el la medicina, la información no se
                                puede quedar atrás. Día a día este software va evolucionando para facilitar una
                                interacción amigable con los usuarios que dispongan de su uso.
                            </div>
                        </li>
                        <!--Reportes de Calificaciones y Informativos-->
                        <li class="item_content clearfix"><a class="features_image" href="#" title="">
                            <img src="medicenter/images/features_large/document.png" alt="">
                        </a>
                            <div class="text">
                                <h3>
                                    Reportes de Calificaciones y Informativos</h3>
                                Su departamento de docencia puede generar todos sus reportes sin complicaciones,
                                desde donde desee y tenga acceso. Todos sus reportes pueden ser generados no importa
                                el año de docencia siempre que los datos estén registrados.
                            </div>
                        </li>
                        <!--Estadísticas de Rendimiento-->
                        <li class="item_content clearfix"><a class="features_image" href="#" title="">
                            <img src="medicenter/images/features_large/stats.png" alt="">
                        </a>
                            <div class="text">
                                <h3>
                                    Estadísticas de Rendimiento</h3>
                                Puede visualizar estadísticas del rendimiento de sus médicos residentes de acuerdo
                                a las calificaciones obtenidas en los distintos periodos evaluados. Estas calificaciones
                                pueden ser comparadas a nivel de año y residencia en incluso con las demás residencias
                                diferentes.
                            </div>
                        </li>
                         <!--Solicitar Servicios-->
                        <li class="item_content clearfix"><a class="features_image" href="#" title="">
                            <img src="medicenter/images/features_large/briefcase.png" alt="">
                        </a>
                            <div class="text">
                                <h3>
                                    Solicitar Servicios</h3>
                                Todos sus médicos residentes que están o estuvieron haciendo alguna especialidad
                                pueden solicitar servicios informativos sin la necesidad de hacer una visita de
                                forma presencial al departamento de docencia.
                            </div>
                        </li>
                    </ul>
                    <ul class="column_right">
                        <!--Fácil Acceso-->
                        <li class="item_content clearfix"><a class="features_image" href="#" title="">
                            <img src="medicenter/images/features_large/leaves.png" alt="">
                        </a>
                            <div class="text">
                                <h3>
                                    Fácil Acceso</h3>
                                Tan sencillo como tener conexión a Internet desde un dispositivo portátil(teléfono
                                inteligente, tableta, laptop) o computadora de escritorio.
                            </div>
                        </li>
                        <!--Actualización de Actividades-->
                        <li class="item_content clearfix"><a class="features_image" href="#" title="">
                            <img src="medicenter/images/features_large/chat.png" alt="">
                        </a>
                            <div class="text">
                                <h3>
                                    Actualización de Actividades</h3>
                                Manténgase siempre al día con las actividades realizadas en su centro por medio
                                de notificaciones instantáneas que notifican sobre toda su información a los médicos
                                residentes autorizados por medio de correos electrónicos.
                            </div>
                        </li>
                        <!--Información disponible-->
                        <li class="item_content clearfix"><a class="features_image" href="#" title="">
                            <img src="medicenter/images/features_large/form.png" alt="">
                        </a>
                            <div class="text">
                                <h3>
                                    Información disponible</h3>
                                Información disponible Todos sus médicos residentes que actualmente hacen o hicieron
                                alguna especialidad o sub especialidad podrán visualizar contenidos de interés como
                                lo son las calificaciones mensuales.
                            </div>
                        </li>
                        <!--Invitación a Actividades-->
                        <li class="item_content clearfix"><a class="features_image" href="#" title="">
                            <img src="medicenter/images/features_large/mail.png" alt="">
                        </a>
                            <div class="text">
                                <h3>
                                    Invitación a Actividades</h3>
                                Una vez que sus médicos residentes se encuentren registrados será mucho más fácil
                                invitar a los mismos a actividades de interés profesional. Estas actividades pueden
                                ser confirmadas por los invitados.
                            </div>
                        </li>
                        <!--Debates Médicos-->
                        <li class="item_content clearfix"><a class="features_image" href="#" title="">
                            <img src="medicenter/images/features_large/people.png" alt="">
                        </a>
                            <div class="text">
                                <h3>
                                    Debates Médicos</h3>
                                Todos sus médicos residentes que están o estuvieron haciendo alguna especialidad
                                y les gusta mostrar su opinión en debates médicos profesionales pueden hacerlo mediante
                                foros exclusivos para los mismos, ganando reputación dentro del mismo foro.
                            </div>
                        </li>                        
                    </ul>
                </div>
                <div class="clearfix">
					
					<div class="clearfix page_margin_top_section">
						<div class="header_left">
							<h3 class="box_header">
								Centros
							</h3>
						</div>
						<div class="header_right">
							<a href="#" id="our_clinic_prev" class="scrolling_list_control_left icon_small_arrow left_black"></a>
							<a href="#" id="our_clinic_next" class="scrolling_list_control_right icon_small_arrow right_black"></a>
						</div>
					</div>
					<ul class="gallery horizontal_carousel our_clinic">
						<li class="gallery_box">
							<%--<img src="medicenter/images/samples/225x150/image_01.jpg" alt="" />--%>
                            <img src="Styles/Images/logos/225x150/cabral.gif" alt="" />
							<div class="description">
								<h3>
									Hospital Cabral y Báez
								</h3>
								<h5>
									Santiago
								</h5>
							</div>
							<%--<ul class="controls">
								<li>
									<a href="medicenter/images/samples/image_01.jpg" rel="our_clinic" class="fancybox open_lightbox"></a>
								</li>
							</ul>--%>
						</li>
					</ul>
					<div class="announcement page_margin_top_section clearfix">
						<ul class="columns no_width">
                            <li class="column_left">
                                <h1>
                                    No permita que su centro siga gestionando la información <br />con los antiguos mecanismo
                                </h1>
                                <p>
                                    Comuniquese con nosotros.</p>
                            </li>
							<li class="column_right">
								<div class="vertical_align">
									<div class="vertical_align_cell">
										<a title="Make an Appointment" href="Contact.aspx" class="more blue medium animated_element animation-slideLeft">Contactar</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
            </div>
        </div>
    </div>
</asp:Content>
