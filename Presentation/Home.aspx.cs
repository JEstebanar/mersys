﻿using System;
using System.Collections.Generic;
using General.GeneralCommons;
using Posts.Posts;

namespace Presentation
{
    public partial class Home : System.Web.UI.Page
    {
        private IList<PosPosts.ResidentsPosts> PostList { get { return (IList<PosPosts.ResidentsPosts>)ViewState["postList"]; } set { ViewState["postList"] = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PostList = PosPosts.GetByPublic(1, (int)GeneralCommon.PageSize.Home);
                rptPosts.DataSource = PostList;
                rptPosts.DataBind();
            }
        }
    }
}