﻿<%@ Page Title="Algo salió mal" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="Presentation.Error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="page relative">
        <div class="page_layout page_margin_top clearfix">
            <div class="page_header clearfix">
                <div class="page_header_left">
                    <h1 class="page_title">
                        Página no Encontrada</h1>
                    <ul class="bread_crumb">
                        <li><a href="/Main.aspx" title="Docencia Médica">Docencia Médica</a></li>
                    </ul>
                </div>
            </div>
            <div class="clearfix">
                <h1 class="not_found page_margin_top_section">
                    Algo salió mal</h1>
                <h1 class="page_margin_top_section">
                    Problema no Identificado</h1>
                <p style="font-size: 14px; padding: 0;" class="page_margin_top">
                    Se han encontrado problemas en la página solicitada.<br />
                    Notifique a su centro de cabecera y vuelva a intentarlo más tarde.</p>
                <a title="Docencia Médica" href="/Main.aspx" class="more blue medium page_margin_top_section">
                    Volver atrás</a>
            </div>
        </div>
    </div>
</asp:Content>
