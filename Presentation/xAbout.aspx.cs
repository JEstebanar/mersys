﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using General.States;
using System.IO;
using General.Forms;
using General.Utilities;
using Evaluation.DetailScores;

namespace Presentation
{
    public partial class xAbout : System.Web.UI.Page
    {
        private IList<EvaDetailScores.ScoreUpdated> ScoresUpdated { get { return (IList<EvaDetailScores.ScoreUpdated>)ViewState["scoresUpdated"]; } set { ViewState["scoresUpdated"] = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            //    IList<EvaDetailScores.ScoreDetails> SelectedScoreDetails = new List<EvaDetailScores.ScoreDetails>();
            //foreach (EvaDetailScores.ScoreDetails evaDetailInList in CurrentScoreDetails)
            //{
            //    if (evaDetailInList.evaluationId == evaluationId && evaDetailInList.monthNumber == monthNumber)
            //    {
            //        SelectedScoreDetails.Add(evaDetailInList);
            //    }
            //}
                List<EvaDetailScores.ScoreUpdated> tempList = new List<EvaDetailScores.ScoreUpdated>();
               

                EvaDetailScores.ScoreUpdated temp = new EvaDetailScores.ScoreUpdated();
                //temp.jan = false;
                //temp.feb = false;
                //temp.mar = false;
                //temp.apr = false;

                tempList.Add(temp);

                ScoresUpdated = tempList;


                //foreach (EvaDetailScores.ScoreUpdated inList in ScoresUpdated)
                //{
                //    inList.jan = false;
                //    inList.feb = false;
                //    inList.mar = false;
                //    inList.apr = false;
                //}

                //Uri MyUrl = Request.UrlReferrer;
                //txtPrueba.Text = MyUrl.ToString();

                //txtNombre.Text = Request.ServerVariables["http_referer"].ToString();
                //txtSegment0.Text = HttpContext.Current.Request.UrlReferrer.Segments[0];
                //txtSegment1.Text = HttpContext.Current.Request.UrlReferrer.Segments[1];

                //ScriptManager scripManager = ScriptManager.GetCurrent(this.Page);

                //scripManager.RegisterPostBackControl(btnUpload);
            }
        }

        protected void btnSession_Click(object sender, EventArgs e)
        {
            switch (Convert.ToInt32(txtNumber.Text))
            {
                case 1:
                    var jan = ScoresUpdated.FirstOrDefault(s => s.jan == false);
                    jan.jan = true;
                    break;
                case 2:
                    var feb = ScoresUpdated.FirstOrDefault(s => s.feb == false);
                    feb.feb = true;
                    break;
                case 3:
                    var mar = ScoresUpdated.FirstOrDefault(s => s.mar == false);
                    mar.mar = true;
                    break;
                case 4:
                    var apr = ScoresUpdated.FirstOrDefault(s => s.apr == false);
                    apr.apr = true;
                    break;
            }

            foreach (EvaDetailScores.ScoreUpdated updatedInList in ScoresUpdated)
            {
                if (updatedInList.jan)
                    txtValidEMail.Text = "1";

                else if (updatedInList.feb)
                    txtValidEMail.Text = "2";
                        
                else if (updatedInList.mar)
                    txtValidEMail.Text = "3";
                
                else if (updatedInList.apr)
                    txtValidEMail.Text = "4";
            }
        }

        protected void btnEMail_Click(object sender, EventArgs e)
        {
            if (GenUtilities.IsValidEmail(txtEMail.Text))
                txtValidEMail.Text = "SI";
            else
                txtValidEMail.Text = "NO";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
//text1.Text = "123";
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            //DateTime dt = Convert.ToDateTime(txt.Value);
        }

        protected void Provision_PortedTelcoChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click2(object sender, EventArgs e)
        {
            //YourLink.HRef = "#myModal";
        }

        protected void Button1_Click3(object sender, EventArgs e)
        {
            
        }

        protected void btnAlert_Click(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Call my function", "MyFunction()", true);

            //ScriptManager.RegisterStartupScript(this, typeof(Page), "invocarfuncion", "Alerta()", true);

            //Master.Message();

            //divAlerta.Attributes.Add("class", "alert alert-error");
            //TextBox mpLabel = (TextBox)Master.FindControl("txtTexto");
            //if (mpLabel != null)
            //{
            //    txtNombre.Text= mpLabel.Text;
            //}
        }

        protected void btn1_Click(object sender, EventArgs e)
        {
           // Master.modal();
        }

        //protected void btnUpload_Click(object sender, EventArgs e)
        //{
        //    string docName = SaveFileInPath(ref fuDocuments, GenForms.GetTransferDocumentsPath());
        //    txtDocName.Text = docName;
        //}

        //protected void btnDownLoad_Click(object sender, EventArgs e)
        //{
        //    img.ImageUrl = GenForms.GetTransferDocumentsPath() + txtDocName.Text;
        //}
        //private string SaveFileInPath(ref FileUpload ObjUpload, string filePath)
        //{
        //    //string fileName = Path.GetFileName(fileUp.PostedFile.FileName);
        //    string fileEextensio = Path.GetExtension(ObjUpload.FileName);
        //    string newFileName = Guid.NewGuid().ToString() + fileEextensio;

        //    ObjUpload.SaveAs(Server.MapPath(filePath + newFileName));

        //    return newFileName;
        //}

        protected void btnSentMail_Click(object sender, EventArgs e)
        {
            //GenSentMail.Sent(1, "xpboy1@hotmail.com");
            string identificadorUnico = Guid.NewGuid().ToString();
            Response.Redirect("~/WebForm1.aspx?flag="+identificadorUnico.Replace("-",""), true);
        }




    }
}
