﻿using System;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysUsers;
using General.GeneralCommons;
using System.Reflection;

namespace Presentation.Account
{
    public partial class ConfirmAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] == null)
                {
                    if (!IsPostBack)
                    {
                        wucConfirmPassword1.ConfirmationType = GeneralCommon.FormConfirmationTypes.SignUp;
                    }
                }
                else
                    Response.Redirect("/Main.aspx");
            }
            catch (Exception)
            {
                throw; // SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }
    }
}