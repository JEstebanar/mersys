﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SystemSecurity.UserCenters;
using SystemSecurity.SysUsers;
using SystemSecurity.UserRoles;
using SystemSecurity.UserAccess;
using SystemSecurity.SysError;
using System.Reflection;
using System.IO;
using General.GeneralCommons;

namespace Presentation.Account
{
    public partial class Access : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    //fill centers 
                    grvCenters.DataSource = SysUserCenters.GetByUserId((int)((SysUsers)Session["user"]).UserId);
                    grvCenters.DataBind();

                    if (grvCenters.Rows.Count == 1)
                    {
                        //int _centerId = 0;
                        foreach (GridViewRow row in grvCenters.Rows)
                        {
                            //_centerId = Convert.ToInt32(grvCenters.DataKeys[row.RowIndex].Values["centerId"]);
                            //FillRoles(Convert.ToInt32(grvCenters.DataKeys[row.RowIndex].Values["userCenterId"]), _centerId, grvCenters.DataKeys[row.RowIndex].Values["centerName"].ToString());
                            AllowAccess(Convert.ToInt32(grvCenters.DataKeys[row.RowIndex].Values["userCenterId"]), Convert.ToInt32(grvCenters.DataKeys[row.RowIndex].Values["centerId"]));
                        }
                    }
                    else if (grvCenters.Rows.Count > 1)
                    {
                        divAccessCenters.Visible = true;
                        //divPublicity.Visible = false;
                    }
                    else
                    {
                        Master.Message("El usuario <strong>" + ((SysUsers)Session["user"]).UserName + "</strong> no tiene centros asignados.", "", 1);
                        return;
                    }
                }
            }
            catch (Exception)
            {
                throw; // SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        #region Methods
        //private void FillRoles(int userCenterId, int _centerId, string _centerName)
        //{
        //    grvRoles.DataSource = SysUserRoles.GetByUserCenterId(userCenterId);
        //    grvRoles.DataBind();

        //    if (grvRoles.Rows.Count == 1)
        //    {
        //        int _roleId = 0;
        //        foreach (GridViewRow row in grvRoles.Rows)
        //            _roleId = Convert.ToInt32(grvRoles.DataKeys[row.RowIndex].Values["roleId"]);

        //        AllowAccess(_centerId, _roleId);
        //    }
        //    else if (grvRoles.Rows.Count > 1)
        //    {
        //        divAccessRoles.Visible = true;
        //        //divPublicity.Visible = false;
        //    }
        //    else
        //    {
        //        Master.Message("El usuario <strong>" + ((SysUsers)Session["user"]).UserName + "</strong> no tiene permisos asignados para el centro <strong>" + _centerName + "</strong>.", "", 1);
        //        return;
        //    }
        //}

        private void AllowAccess(int userCenterId, int centerId)
        {
            //load access
            //SysUserAccess UserSelect = new SysUserAccess();
            //UserSelect.UserId = (int)((SysUsers)Session["user"]).UserId;
            //UserSelect.UserCenterId = userCenterId;
            //UserSelect.CenterId = centerId;
            //SysUserAccess[] Access = SysUserAccess.Search(UserSelect);
            //SysUserAccess access = Access[0];

            Session["access"] = SysUserAccess.GetAccess((int)((SysUsers)Session["user"]).UserId, userCenterId, centerId);

            //load roles by userCenterId
            Session["roles"] = SysUserRoles.GetByUserCenterId((int)((SysUserAccess)Session["access"]).UserCenterId);

            ////business logic
            //foreach (SysUserRoles.UserRoles roleInList in (List<SysUserRoles.UserRoles>)Session["roles"])
            //{
            //    if (roleInList.roleId == (int)GeneralCommon.SystemRoles.Resident)
            //    {

            //    }
            //}
            Response.Redirect("/Main.aspx", true);
        }
        #endregion

        #region GridViews
        protected void grvCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //FillRoles(Convert.ToInt32(grvCenters.SelectedDataKey[0]), Convert.ToInt32(grvCenters.SelectedDataKey[1]), grvCenters.SelectedDataKey[2].ToString());
                AllowAccess(Convert.ToInt32(grvCenters.SelectedDataKey[0]), Convert.ToInt32(grvCenters.SelectedDataKey[1]));
            }
            catch (Exception)
            {
                throw; //SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        //protected void grvRoles_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    AllowAccess(Convert.ToInt32(grvCenters.SelectedDataKey[1]), Convert.ToInt32(grvRoles.SelectedDataKey[0]));
        //}
        #endregion
    }
}