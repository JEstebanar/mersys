﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using General.GeneralCommons;
using System.Reflection;
using System.IO;
using SystemSecurity.SysError;

namespace Presentation.Account
{
    public partial class Recover : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] == null)
                {
                    if (!IsPostBack)
                    {
                        wucConfirmPassword1.ConfirmationType = GeneralCommon.FormConfirmationTypes.RecoverPassword;
                    }
                }
                else
                    Response.Redirect("/Main.aspx");
            }
            catch (Exception)
            {
                throw; //SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }
    }
}