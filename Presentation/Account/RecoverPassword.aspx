﻿<%@ Page Title="Recuperar Contraseña" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="RecoverPassword.aspx.cs" Inherits="Presentation.Account.RecoverPassword" %>
    <%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="span6 center">
        <div class="well">
            <fieldset>
                <legend>
                    <h2>
                        Recuperar Contraseña</h2>
                </legend>
                <blockquote>
                    <p>
                        <strong>¿Olvidaste tu Contraseña?</strong><br />
                        <small>No te preocupes, es bien sencillo recuperar tu contraseña.</small>
                    </p>
                </blockquote>
                <div class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label">
                            Correo Electrónico</label>
                        <div class="controls">
                            <asp:TextBox ID="txtEmail" runat="server" type="email" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <asp:Button ID="btnConfirm" runat="server" Text="Recuperar Contraseña" class="btn btn-primary"
                        OnClick="btnConfirm_Click" />
                    <%--<asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" />--%>
                    <a href="/" role="button" class="btn">Cancelar</a>
                </div>
<%--                <blockquote>
                    <p>
                        <strong>¿Aun no recibes un correo de confirmación?</strong> <small>Este proceso puede tardar
                            varios minutos.</small>
                    </p>
                </blockquote>--%>
            </fieldset>
        </div>
    </div>
</asp:Content>
