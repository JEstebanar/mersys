﻿using System;
using System.Web;
using SystemSecurity.SysUsers;
using System.Web.Security;
using System.Reflection;
using System.IO;
using SystemSecurity.SysError;

namespace Presentation.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //RegisterHyperLink.NavigateUrl = "SignUp.aspx?ReturnUrl=" + HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            lnkForgotPassword.NavigateUrl = "RecoverPassword.aspx";
            //lnkRegister.NavigateUrl = "SignUp.aspx";
            Master.Message("", "", 0);

            if (Session["access"] != null)
                Response.Redirect("/Main.aspx", true);
        }

        protected void btnLogIn_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtEmail.Text == string.Empty && txtPassword.Text == string.Empty)
                {
                    Master.Message("Por favor introduzca un correo electrónico y contraseña valida.", "", 2);
                    txtPassword.Text = string.Empty;
                    return;
                }

                SysUsers UserLogIn = new SysUsers();
                UserLogIn.UserName = txtEmail.Text;
                UserLogIn.UserPassword = txtPassword.Text;
                SysUsers[] Users = SysUsers.Search(UserLogIn);

                if (Users.Length != 1)
                {
                    Master.Message("Por favor introduzca un correo electrónico y contraseña valida.", "", 2);
                    txtPassword.Text = string.Empty;
                    return;
                }

                SysUsers User = Users[0];

                if (User == null)
                {
                    Master.Message("Correo Electrónico o Contraseña no validos.", "", 2);
                    txtPassword.Text = string.Empty;
                    return;
                }

                if (txtPassword.Text != User.UserPassword)
                {
                    Master.Message("Correo Electrónico o Contraseña no validos.", "", 2);
                    txtPassword.Text = string.Empty;
                    //user.Update();  //Update With A Failed Login
                    return;
                }

                Session["user"] = User;
                FormsAuthentication.SetAuthCookie(txtEmail.Text, true);
                FormsAuthentication.SetAuthCookie(txtPassword.Text, false);
                Response.Redirect("/Account/Access.aspx", true);
            }
            catch (Exception)
            {
                throw; // SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }
    }
}
