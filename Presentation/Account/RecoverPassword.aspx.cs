﻿using System;
using SystemSecurity.SysUsers;
using General.EMails;
using SystemSecurity.Utilities;

namespace Presentation.Account
{
    public partial class RecoverPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (!SysUsers.IfExistsUserName(txtEmail.Text, null))
            {
                Master.Message("No tenemos registrado este correo en nuestra base de datos. " +
                    "Si esto es un error y su correo se encuentra registrado en su centro de residencia, " +
                    "favor diríjase a docencia médica para resolver esta situación.", "", 2);
                return;
            }

            string confirmationCode = SysUtilities.CreateConfirmationCode(); //Guid.NewGuid().ToString().Replace("-", "");
            string userName = txtEmail.Text;

            SysUsers.UserInfo info = SysUsers.GetByUserName(userName);
            string fullName = info.fullName;

            SysUsers cUser = SysUsers.Get(info.userId);
            //Random random = new Random();
            //int randomNumber = random.Next(0, 99999) + 100000;
            //cUser.UserPassword = (randomNumber - 50000).ToString();
            cUser.ConfirmationCode = confirmationCode;
            //cUser.UserStatus = false;
            cUser.Update();

            GenEMails.RecoverPassword(userName, fullName, confirmationCode);
            Master.Message("Enviamos la confirmación a tu correo electrónico, favor revisar y seguir las instrucciones.", "", 3); 
        }
    }
}