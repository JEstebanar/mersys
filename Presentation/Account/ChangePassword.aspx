﻿<%@ Page Title="Cambiar Contraseña" Language="C#" MasterPageFile="~/SiteGeneral.Master" AutoEventWireup="true"
    CodeBehind="ChangePassword.aspx.cs" Inherits="Presentation.Account.ChangePassword1" %>

<%@ MasterType VirtualPath="~/SiteGeneral.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>
            <h2>
                Cambiar Contraseña</h2>
        </legend>
        <div class="form-horizontal">
            <div class="control-group">
                <div class="controls">
                    <ul id="ulMessage" runat="server" class="text-error">
                    </ul>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">
                    Contraseña Actual</label>
                <div class="controls">
                    <asp:TextBox ID="txtCurrentPassword" runat="server" type="password"></asp:TextBox></div>
            </div>
            <div class="control-group">
                <label class="control-label">
                    Nueva Contraseña</label>
                <div class="controls">
                    <asp:TextBox ID="txtNewPassword" runat="server" type="password"></asp:TextBox></div>
            </div>
            <div class="control-group">
                <label class="control-label">
                    Confirmar Contraseña</label>
                <div class="controls">
                    <asp:TextBox ID="txtConfirmNewPassword" runat="server" type="password"></asp:TextBox></div>
            </div>
            <div class="form-actions">
                <asp:Button ID="btnSave" runat="server" Text="Cambiar Contraseña" class="btn btn-primary"
                    OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
            </div>
        </div>
    </fieldset>
</asp:Content>
