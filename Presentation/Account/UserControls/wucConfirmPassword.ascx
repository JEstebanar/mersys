﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="wucConfirmPassword.ascx.cs"
    Inherits="Presentation.Account.UserControls.wucConfirmPassword" %>
<!-- Le styles -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../../../bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="../../../bootstrap/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="../../../Default/style/reset.css" />
<link rel="stylesheet" type="text/css" href="../../../Default/style/superfish.css" />
<link rel="stylesheet" type="text/css" href="../../../Default/style/style.css" />
<link rel="stylesheet" type="text/css" href="../../../Default/style/responsive.css" />
<link rel="shortcut icon" href="../../../Default/images/favicon.ico" />
<link rel="stylesheet" href="../../../Styles/Site.css" type="text/css" />
<!-- end styles -->
<asp:UpdatePanel runat="server" ID="upMessage" UpdateMode="Conditional">
    <ContentTemplate>
        <div id="divMessage" runat="server" class="alert alert-block" visible="false">
            <button type="button" class="close" data-dismiss="alert">
                &times;</button>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="container-fluid">
    <div class="span6">
        <div class="well">
            <fieldset>
                <legend>
                    <h2>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h2>
                </legend>
                <div class="form-horizontal">
                    <div class="control-group">
                        <div class="controls">
                            <ul id="ulMessage" runat="server" class="text-error">
                            </ul>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            Contraseña</label>
                        <div class="controls">
                            <asp:TextBox ID="txtPassword" runat="server" type="password" placeholder=""></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            Confirmar Contraseña</label>
                        <div class="controls">
                            <asp:TextBox ID="txtPasswordConfirm" runat="server" type="password" placeholder=""></asp:TextBox>
                        </div>
                    </div>
                    <div class="controls">
                        <asp:Button ID="btnConfirm" runat="server" class="btn btn-large btn-success" OnClick="btnConfirm_Click" />
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
