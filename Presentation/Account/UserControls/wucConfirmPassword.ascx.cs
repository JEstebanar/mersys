﻿using System;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysUsers;
using System.Web.UI.HtmlControls;
using General.GeneralCommons;

namespace Presentation.Account.UserControls
{
    public partial class wucConfirmPassword : System.Web.UI.UserControl
    {
        #region Properties
        public GeneralCommon.FormConfirmationTypes ConfirmationType
        {
            get { return (GeneralCommon.FormConfirmationTypes)ViewState["ConfirmationType"]; }
            set { ViewState["ConfirmationType"] = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                switch (ConfirmationType)
                {
                    case GeneralCommon.FormConfirmationTypes.SignUp:
                        lblHeader.Text = "Confirmación de Cuenta";
                        btnConfirm.Text = "Regístrar";
                        break;
                    case GeneralCommon.FormConfirmationTypes.RecoverPassword:
                        lblHeader.Text = "Recuperar Contraseña";
                        btnConfirm.Text = "Recuperar";
                        break;
                }

                txtPassword.Text = string.Empty;
                txtPasswordConfirm.Text = string.Empty;

                if (Request.QueryString["conf"] != null)
                {
                    if (SysSecurityAccess.ConfirmationCode(Request.QueryString["conf"]) > 0)
                    {
                        btnConfirm.Visible = true;
                    }
                    else
                    {
                        Message("Esta confirmación no es válida. Pongase en contacto con su centro para que se le vueva a emitir otro código de confirmación.", "", 2);
                        btnConfirm.Visible = false;
                        return;
                    }
                }
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text.Length < 6)
            {
                Message("Las Contraseñas deben tener mínimo 6 caracteres.", "", 2);
                return;
            }

            if (txtPassword.Text != txtPasswordConfirm.Text)
            {
                Message("Las Contraseñas deben de ser iguales.", "", 2);
                return;
            }

            SysUsers cUser = SysUsers.Get(SysSecurityAccess.ConfirmationCode(Request.QueryString["conf"]));
            cUser.UserPassword = txtPassword.Text;
            cUser.ConfirmationCode = null;
            cUser.UserStatus = true;
            cUser.Update();

            //SysSecurityAccess.SetUserActive(SysSecurityAccess.ConfirmationCode(Request.QueryString["conf"]));
            Response.Redirect("/Account/LogIn.aspx", true);
        }

        private bool Validation(string txt)
        {
            bool correct = true;
            int minCharacters = 6;
            int maxCharacters = 12;
            if (minCharacters > txt.Length)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                li.InnerText = "Debe de contener " + minCharacters + " caracteres mínimo.";
                ulMessage.Controls.Add(li);
                correct = false;
            }

            if (maxCharacters < txt.Length)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                li.InnerText = "Debe de contener " + maxCharacters + " caracteres máximo.";
                ulMessage.Controls.Add(li);
                correct = false;
            }
            return correct;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">Message you want to display</param>
        /// <param name="header">Message header</param>
        /// <param name="type">1 - Warning Message, || 2 - Error Message, || 3 - Message of Success, || 4 - Message Information</param>
        public void Message(System.String message, System.String header, int? type)
        {
            HtmlGenericControl h4 = new HtmlGenericControl("h4");
            //HtmlGenericControl p = new HtmlGenericControl("p");

            if (type == null || type > 4 || type <= 0)
            {
                divMessage.Controls.Remove(h4);

                //divMessage.Controls.Remove(p);

                divMessage.Attributes.Add("class", "");
                divMessage.Visible = false;
            }
            else
            {
                divMessage.Visible = true;

                switch (type)
                {
                    case 1:         //warning
                        divMessage.Attributes.Add("class", "alert alert-block");
                        h4.InnerText = "Advertencia";
                        break;
                    case 2:         //error
                        divMessage.Attributes.Add("class", "alert alert-error");
                        h4.InnerText = "Algo esta mal";
                        break;
                    case 3:         //success
                        divMessage.Attributes.Add("class", "alert alert-success");
                        h4.InnerText = "¡Bien Hecho!";
                        break;
                    case 4:         //info
                        divMessage.Attributes.Add("class", "alert alert-info");
                        h4.InnerText = "¡Algo esta mal!";
                        break;
                    default:
                        divMessage.Attributes.Add("class", "");
                        h4.InnerText = "";
                        break;
                }

                //h4.InnerText = header;
                divMessage.Controls.Add(h4);

                //p.InnerText = message;
                divMessage.InnerText = message;
            }
            upMessage.Update();
        }
    }
}