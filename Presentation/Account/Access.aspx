﻿<%@ Page Title="Centros Autorizados" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Access.aspx.cs" Inherits="Presentation.Account.Access" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">
        <div class="row-fluid">
            <div id="divAccessCenters" runat="server" class="span4" visible="false">
                <div class="well">
                    <fieldset>
                        <legend>Centros Permitidos</legend>
                        <div class="controls">
                            <asp:GridView ID="grvCenters" runat="server" Width="100%" class="table table-striped table-bordered"
                                AutoGenerateColumns="False" DataKeyNames="userCenterId,centerId,centerName" ShowHeader="false"
                                OnSelectedIndexChanged="grvCenters_SelectedIndexChanged">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="centerSelect" runat="server" CommandArgument='<%# Eval("userCenterId") %>'
                                                CommandName="Select" ToolTip="Seleccionar Centro" Text='<%# Eval("centerName") %>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
