﻿<%@ Page Title="Iniciar Sesión" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="LogIn.aspx.cs" Inherits="Presentation.Account.Login" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span8">
            </div>
            <div class="span4">
                <div class="well text-center">
                    <fieldset>
                        <legend>Iniciar Sesión</legend>
                        <img src="../Styles/Images/avatar_2x.png" class="img-circle">
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-envelope"></i></span>
                                <asp:TextBox ID="txtEmail" runat="server" type="email" placeholder="Correo Electrónico"
                                    autocomplete="off"></asp:TextBox>
                            </div>
                        </div>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-lock"></i></span>
                                <asp:TextBox ID="txtPassword" runat="server" type="password" placeholder="Contraseña"
                                    autocomplete="off"></asp:TextBox>
                            </div>
                        </div>
                        <div class="controls">
                            <asp:Button ID="btnLogIn" runat="server" Text="Iniciar Sesión" class="btn btn-large btn-primary"
                                OnClick="btnLogIn_Click" />
                        </div>
                        <br />
                        <p>
                            <asp:HyperLink ID="lnkForgotPassword" runat="server" EnableViewState="false">¿Olvidaste tu Contraseña?</asp:HyperLink>
                            <br />
                            ¿Eres Médico Residente y No estas Registrado?
                            <asp:HyperLink ID="lnkRegister" runat="server" EnableViewState="false">Rigistrate</asp:HyperLink>
                        </p>
                    </fieldset>
                </div>
            </div>
            <div id="divPublicity" runat="server" class="span8" visible="false">
                <div class="well">
                    <!-- Carousel
    ================================================== -->
                    <div id="myCarousel" class="carousel slide">
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="../Default/images/slider/img1.jpg" alt="" />
                                <div class="container">
                                    <%--<div class="carousel-caption">
              <h1>Example headline.</h1>
              <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <a class="btn btn-large btn-primary" href="#">Sign up today</a>
            </div>--%>
                                </div>
                            </div>
                            <div class="item">
                                <img src="../Default/images/slider/img2.jpg" alt="" />
                                <div class="container">
                                    <%--<div class="carousel-caption">
              <h1>Another example headline.</h1>
              <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <a class="btn btn-large btn-primary" href="#">Learn more</a>
            </div>--%>
                                </div>
                            </div>
                            <div class="item">
                                <img src="../Default/images/slider/img3.jpg" alt="" />
                                <div class="container">
                                    <%--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <a class="btn btn-large btn-primary" href="#">Browse gallery</a>
            </div>--%>
                                </div>
                            </div>
                        </div>
                        <%--      <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>--%>
                    </div>
                    <!-- /.carousel -->
                </div>
            </div>
        </div>
    </div>
</asp:Content>
