﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SystemSecurity.SysUsers;
using SystemSecurity.RoleSecurities;
using SystemSecurity.UserAccess;
using General.Utilities;
using System.Web.UI.HtmlControls;
using General.GeneralCommons;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using System.IO;
using System.Reflection;

namespace Presentation.Account
{
    public partial class ChangePassword1 : System.Web.UI.Page
    {
        private IList<SysRoleSecurities.SecurityInRoles> AsignedSecurity { get { return (IList<SysRoleSecurities.SecurityInRoles>)ViewState["asignedSecurity"]; } set { ViewState["asignedSecurity"] = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    //if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.ChangePassword))
                    //{
                        if (!IsPostBack)
                        {
                            AsignedSecurity = SysRoleSecurities.GetByUserCenterId((int)((SysUserAccess)Session["access"]).UserCenterId);
                        }
                    //}
                    //else
                    //    Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private bool Validation(string txt)
        {
            bool correct = true;
            foreach (SysRoleSecurities.SecurityInRoles security in AsignedSecurity)
            {
                if (security.minCharacters > txt.Length)
                {
                    HtmlGenericControl li = new HtmlGenericControl("li");
                    li.InnerText = "Debe de contener " + security.minCharacters + " caracteres mínimo.";
                    ulMessage.Controls.Add(li);
                    correct = false;
                }

                if (security.maxCharacters < txt.Length)
                {
                    HtmlGenericControl li = new HtmlGenericControl("li");
                    li.InnerText = "Debe de contener " + security.maxCharacters + " caracteres máximo.";
                    ulMessage.Controls.Add(li);
                    correct = false;
                }

                //if (security.allowNumbers == true && security.allowLetters)
                //{
                //    if (!GenUtilities.IsAlphaNumeric(txt))
                //    {
                //        HtmlGenericControl li = new HtmlGenericControl("li");
                //        li.InnerText = "Debe de contener letras.";
                //        ulMessage.Controls.Add(li);

                //        // HtmlGenericControl li = new HtmlGenericControl("li");
                //        li.InnerText = "Debe de contener números.";
                //        ulMessage.Controls.Add(li);
                //        correct = false;
                //    }
                //}
            }
                        
            return correct;
            //    if (security.allowNumbers==true)
            //    {

            //        if (!GenUtilities.AllowNumbers(txt))
            //        {
            //            HtmlGenericControl li = new HtmlGenericControl("li");
            //            li.InnerText = "Debe de contener números.";
            //            ulMessage.Controls.Add(li);
            //        }
            //    }

            //    if (security.allowLetters == true)
            //    {
            //        if (!GenUtilities.AllowLetters(txt))
            //        {
            //            HtmlGenericControl li = new HtmlGenericControl("li");
            //            li.InnerText = "Debe de contener letras.";
            //            ulMessage.Controls.Add(li);
            //        }
            //    }

            //    if (security.allowCapitalLetters == true)
            //    {
            //        if (!GenUtilities.AllowCapitalLetters(txt))
            //        {
            //            HtmlGenericControl li = new HtmlGenericControl("li");
            //            li.InnerText = "Debe de contener letras mayúsculas.";
            //            ulMessage.Controls.Add(li);
            //        }
            //    }
            //}           

            //if (msg == string.Empty)
            //    return true;
            //else                
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (((SysUsers)Session["user"]).UserPassword != txtCurrentPassword.Text)
                {
                    Master.Master.Message("La contraseña especificado como actual no coincide.", "", 2);
                    return;
                }

                if (txtNewPassword.Text == string.Empty)
                {
                    Master.Master.Message("La confirmación de contraseña no es correcta.", "", 2);
                    return;
                }

                if (txtNewPassword.Text != txtConfirmNewPassword.Text)
                {
                    Master.Master.Message("La confirmación de contraseña no es correcta.", "", 2);
                    return;
                }

                if (!Validation(txtNewPassword.Text))
                    return;

                SysUsers user;
                user = SysUsers.Get((int)((SysUsers)Session["user"]).UserId);
                user.UserPassword = txtNewPassword.Text;
                user.Update();
                
                Response.Redirect("/Main.aspx", true);
                Master.Master.Message("¡Datos Guardados Exitosamente!", "", 3);
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Main.aspx", true);
        }
    }
}