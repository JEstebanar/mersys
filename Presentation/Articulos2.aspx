﻿<%@ Page Title="Artículos" Language="C#" MasterPageFile="~/medicenter.Master" AutoEventWireup="true"
    CodeBehind="Articulos2.aspx.cs" Inherits="Presentation.Articulos2" %>

<%@ Register Src="~/Entradas/UserControls/wucSidebar.ascx" TagName="wucSidebar" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page relative">
        <div class="page_layout page_margin_top clearfix">
            <div class="page_header clearfix">
                <div class="page_header_left">
                    <h1 class="page_title">
                        Artículos</h1>
                    <%--<ul class="bread_crumb">
                        <li><a href="home.aspx" title="Home">Inicio </a></li>
                        <li class="separator icon_small_arrow right_gray">&nbsp; </li>
                        <li>Artículos </li>
                    </ul>--%>
                </div>
                <%--                <div class="page_header_right">
                    <form class="search">
                    <input class="search_input" type="text" value="To search type and hit enter..." placeholder="To search type and hit enter..." />
                    </form>
                </div>--%>
            </div>
            <div class="page_left">
                <div class="columns clearfix">
                    <ul class="blog column_left">
                    <asp:Repeater ID="rptPostLeft" runat="server">
                        <ItemTemplate>
                        <li class="post">
                            <ul class="comment_box clearfix">
                                <li class="date">
                                    <div class="value">
                                        <%# Eval("createdDate")%></div>
                                    <div class="arrow_date">
                                    </div>
                                </li>
                                <li class="comments_number"><a href="<%# Eval("postURL")%>#comments_list" title="<%# Eval("visits")%>">
                                <%# Eval("visits")%></a>
                                </li>
                            </ul>
                            <div class="post_content">
                                <a class="post_image" href="<%# Eval("postURL")%>" title="<%# Eval("title")%>">
                                    <asp:Image ID="newsImage" runat="server" Visible='<%# Eval("postImage").ToString() != "" %>'
                                            ImageUrl='<%# Eval("postImage") %>' />
                                </a>
                                <h2>
                                    <a href="<%# Eval("postURL")%>" title="<%# Eval("title")%>">
                                            <%# Eval("title")%>
                                        </a>
                                </h2>
                               
                                    <%#Eval("body").ToString().Trim().Length <= 500 ? Eval("body") : Eval("body").ToString().Remove(500) %>
                                
                                <a title="Leer más" href="<%# Eval("postURL")%>" class="more">Leer más &rarr; </a>
                                <%--<div class="post_footer">
                                    <ul class="post_footer_details">
                                        <li>Posted in </li>
                                        <li><a href="#" title="General">General, </a></li>
                                        <li><a href="#" title="Dental clinic">Dental clinic </a></li>
                                    </ul>
                                </div>--%>
                            </div>
                        </li>
                        </ItemTemplate>
                    </asp:Repeater>
                    </ul>
                    <ul class="blog column_right">
                    <asp:Repeater ID="rptPostRight" runat="server">
                        <ItemTemplate>
                        <li class="post">
                            <ul class="comment_box clearfix">
                                <li class="date">
                                    <div class="value">
                                        <%# Eval("createdDate")%></div>
                                    <div class="arrow_date">
                                    </div>
                                </li>
                                <li class="comments_number"><a href="<%# Eval("postURL")%>#comments_list" title="<%# Eval("visits")%>">
                                <%# Eval("visits")%></a>
                                </li>
                            </ul>
                            <div class="post_content">
                                <a class="post_image" href="<%# Eval("postURL")%>" title="<%# Eval("title")%>">
                                    <asp:Image ID="newsImage" runat="server" Visible='<%# Eval("postImage").ToString() != "" %>'
                                            ImageUrl='<%# Eval("postImage") %>' />
                                </a>
                                <h2>
                                    <a href="<%# Eval("postURL")%>" title="<%# Eval("title")%>">
                                            <%# Eval("title")%>
                                        </a>
                                </h2>
                                
                                    <%#Eval("body").ToString().Trim().Length <= 500 ? Eval("body") : Eval("body").ToString().Remove(500) %>
                                
                                <a title="Leer más" href="<%# Eval("postURL")%>" class="more">Leer más &rarr; </a>
                                <%--<div class="post_footer">
                                    <ul class="post_footer_details">
                                        <li>Posted in </li>
                                        <li><a href="#" title="General">General, </a></li>
                                        <li><a href="#" title="Dental clinic">Dental clinic </a></li>
                                    </ul>
                                </div>--%>
                            </div>
                        </li>
                        </ItemTemplate>
                    </asp:Repeater>
                    </ul>
                </div>
                <ul class="pagination page_margin_top">
                    <%--<li class="selected"><a href="#" title="">1 </a></li>
                    <li><a href="#" title="">2 </a></li>
                    <li><a href="#" title="">3 </a></li>--%>
                    <li>
                        <asp:LinkButton ID="lnkBtnPrev" runat="server" Font-Underline="False" OnClick="lnkBtnPrev_Click"
                            Font-Bold="True"><< Anterior </asp:LinkButton></li>
                    <li>
                        <input id="txtHidden" style="width: 28px" type="hidden" value="1" runat="server" />
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnNext" runat="server" Font-Underline="False" OnClick="lnkBtnNext_Click"
                            Font-Bold="True">Siguiente >></asp:LinkButton></li>
                </ul>
            </div>
            <div class="page_right">
                <uc1:wucSidebar ID="wucSidebar1" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
