﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SystemSecurity.SysUsers;
using General.GeneralCommons;
using SystemSecurity.UserAccess;
using SystemSecurity.SysError;
using System.IO;
using System.Reflection;
using Posts.Posts;
using System.Web.UI.HtmlControls;
using General.Utilities;

namespace Presentation
{
    public partial class Main : System.Web.UI.Page
    {
        private IList<PosPosts.ResidentsPosts> PostsList { get { return (IList<PosPosts.ResidentsPosts>)ViewState["postsList"]; } set { ViewState["postsList"] = value; } }
        private IList<PosPosts.ResidentsPosts> PostsLeft { get { return (IList<PosPosts.ResidentsPosts>)ViewState["postsLeft"]; } set { ViewState["postsLeft"] = value; } }
        private IList<PosPosts.ResidentsPosts> PostsRight { get { return (IList<PosPosts.ResidentsPosts>)ViewState["postsRight"]; } set { ViewState["postsRight"] = value; } }
        private IList<PosPosts.ResidentsPosts> PostsSide { get { return (IList<PosPosts.ResidentsPosts>)ViewState["postsSide"]; } set { ViewState["postsSide"] = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (!IsPostBack)
                    {
                        bindrepeater(1);
                        txtHidden.Value = "1";

                        //FillPosts();
                    }
                }
                else
                    Response.Redirect("~/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void lnkBtnPrev_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(txtHidden.Value) <= 2)
                return;

            txtHidden.Value = Convert.ToString(Convert.ToInt32(txtHidden.Value) - 2);
            bindrepeater(Convert.ToInt32(txtHidden.Value));
        }

        protected void lnkBtnNext_Click(object sender, EventArgs e)
        {
            txtHidden.Value = Convert.ToString(Convert.ToInt32(txtHidden.Value) + 2);
            bindrepeater(Convert.ToInt32(txtHidden.Value));
        }

        public void bindrepeater(int newPage)
        {
            if (newPage == 0)
                txtHidden.Value = Convert.ToString(Convert.ToInt32(txtHidden.Value) + 1);

            //PostsList = PosPosts.GetByPublic(newPage, (int)GeneralCommon.PageSize.Articles);

            //int postNum=0;
            //foreach (PosPosts.ResidentsPosts postsInList in PostsList)
            //{
            //    if(postNum%2==0)

            //}
            PostsLeft = PosPosts.GetByPublic(newPage, (int)GeneralCommon.PageSize.Articles);
            PostsRight = PosPosts.GetByPublic(newPage + 1, (int)GeneralCommon.PageSize.Articles);
            PostsSide = PosPosts.GetByPublic(newPage + 2, (int)GeneralCommon.PageSize.Articles);

            rptPostsLeft.DataSource = PostsLeft;
            rptPostsLeft.DataBind();

            rptPostsRight.DataSource = PostsRight;
            rptPostsRight.DataBind();

            rptPostsSide.DataSource = PostsSide;
            rptPostsSide.DataBind();
            //pageno(1);
        }

        public void pageno(int totItems)
        {
            // Calculate total numbers of pages
            int pgCount = totItems / (int)GeneralCommon.PageSize.Articles + totItems % (int)GeneralCommon.PageSize.Articles;

            //// Display Next>> button
            //if (pgCount - 1 > Convert.ToInt16(txtHidden.Value))
            //    lnkBtnNext.Visible = true;
            //else
            //    lnkBtnNext.Visible = false;

            //// Display <<Prev button
            //if ((Convert.ToInt16(txtHidden.Value)) > 1)
            //    lnkBtnPrev.Visible = true;
            //else
            //    lnkBtnPrev.Visible = false;
        }

        //private void FillPosts()
        //{
        //    PostList = PosPosts.GetByResident(0, 0, (int)((SysUserAccess)Session["access"]).CenterId, GeneralCommon.PostTypes.Articles);

        //    HtmlGenericControl ul = new HtmlGenericControl("ul");
        //    ul.Attributes.Add("class", "thumbnails");
        //    divPosts.Controls.Add(ul);
        //    int items = 0;

        //    foreach (PosPosts.ResidentsPosts postsInList in PostList)
        //    {
        //        if (items == 3)
        //        {
        //            ul = new HtmlGenericControl("ul");
        //            ul.Attributes.Add("class", "thumbnails");
        //            divPosts.Controls.Add(ul);
        //            items = 0;
        //        }                

        //        HtmlGenericControl li = new HtmlGenericControl("li");
        //        li.Attributes.Add("class", "span4");
        //        ul.Controls.Add(li);

        //        HtmlGenericControl div_thumbnail = new HtmlGenericControl("div");
        //        div_thumbnail.Attributes.Add("class", "thumbnail");
        //        li.Controls.Add(div_thumbnail);

        //        //<img alt="300x200" style="width: 300px; height: 200px;" src="bootstrap/img/300x200.png">
        //        HtmlGenericControl img = new HtmlGenericControl("img");
        //        img.Attributes.Add("alt", "300x200");
        //        img.Attributes.Add("style", "width: 300px; height: 200px;");
        //        img.Attributes.Add("src", "bootstrap/img/300x200.png");
        //        div_thumbnail.Controls.Add(img);

        //        HtmlGenericControl div_caption = new HtmlGenericControl("div");
        //        div_caption.Attributes.Add("class", "caption");
        //        div_thumbnail.Controls.Add(div_caption);

        //        HtmlGenericControl h3 = new HtmlGenericControl("h3");
        //        h3.InnerText = postsInList.title;
        //        div_caption.Controls.Add(h3);

        //        HtmlGenericControl p_body = new HtmlGenericControl("p");
        //        p_body.InnerHtml = postsInList.body.Trim().Remove(150) + "...";
        //        div_caption.Controls.Add(p_body);

        //        HtmlGenericControl p = new HtmlGenericControl("p");
        //        div_caption.Controls.Add(p);

        //        //HtmlGenericControl p_a = new HtmlGenericControl("p");
        //        HtmlGenericControl a = new HtmlGenericControl("a");
                
        //        //a.Attributes.Add("href", postsInList.postURL);
        //        //<a href="#exitModal" role="button" data-toggle="modal">Salir</a>
        //        //a.Attributes.Add("href", "#postModal");
        //        //a.Attributes.Add("role", "button");
        //        //a.Attributes.Add("data-toggle", "modal");
        //        a.Attributes.Add("class", "btn");
        //        a.InnerText = "Leer >>";

        //        p.Controls.Add(a);

        //        items++;

        //    }            
        //}
    }
}