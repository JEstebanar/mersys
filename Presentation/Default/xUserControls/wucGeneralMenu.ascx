﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="wucGeneralMenu.ascx.cs"
    Inherits="Presentation.Default.UserControls.wucGeneralMenu" %>
<!--style-->
<%--<link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Volkhov:400italic' rel='stylesheet'
    type='text/css'>--%>
<%--<link rel="stylesheet" type="text/css" href="~/Default/style/reset.css" />
<link rel="stylesheet" type="text/css" href="~/Default/style/superfish.css" />
<link rel="stylesheet" type="text/css" href="~/Default/style/fancybox/jquery.fancybox.css" />
<link rel="stylesheet" type="text/css" href="~/Default/style/jquery.qtip.css" />
<link rel="stylesheet" type="text/css" href="~/Default/style/jquery-ui-1.9.2.custom.css" />--%>
<link rel="stylesheet" type="text/css" href="../style/style.css" />
<link rel="stylesheet" type="text/css" href="../style/responsive.css" />
<link rel="shortcut icon" href="../images/favicon.ico" />
<!-- end styles -->
    <!-- js -->
    <script type="text/javascript" src="~/Default/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="~/Default/js/jquery.ba-bbq.min.js"></script>
    <script type="text/javascript" src="~/Default/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="~/Default/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="~/Default/js/jquery.carouFredSel-5.6.4-packed.js"></script>
    <script type="text/javascript" src="~/Default/js/jquery.sliderControl.js"></script>
    <script type="text/javascript" src="~/Default/js/jquery.timeago.js"></script>
    <script type="text/javascript" src="~/Default/js/jquery.hint.js"></script>
    <script type="text/javascript" src="~/Default/js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="~/Default/js/jquery.isotope.masonry.js"></script>
    <script type="text/javascript" src="~/Default/js/jquery.fancybox-1.3.4.pack.js"></script>
    <script type="text/javascript" src="~/Default/js/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="~/Default/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="~/Default/js/main.js"></script>

<div class="header_container">
    <div class="header clearfix">
        <div class="header_left">
            <a href="Home.aspx" title="Residencias Médicas">
                <img src="images/header_logo.png" alt="logo" />
                <span class="logo">Residencias Médicas</span> </a>
        </div>
        <ul class="sf-menu header_right">
            <li><a href="Home.aspx" title="Inicio">INICIO </a></li>
            <li class="submenu wide"><a href="#" title="Residencias">RESIDENCIAS </a>
                <ul>
                    <li><a href="Residences.aspx#primary-health-care" title="Primary Health Care">Primary
                        Health Care </a></li>
                    <li><a href="Residences.aspx#laryngological-clinic" title="Laryngological Clinic">Laryngological
                        Clinic </a></li>
                    <li><a href="Residences.aspx#pediatric-clinic" title="Pediatric Clinic">Pediatric Clinic
                    </a></li>
                    <li><a href="Residences.aspx#ophthalmology-clinic" title="Ophthalmology Clinic">Ophthalmology
                        Clinic </a></li>
                    <li><a href="Residences.aspx#outpatient-surgery" title="Outpatient Surgery">Outpatient
                        Surgery </a></li>
                    <li><a href="Residences.aspx#dental-clinic" title="Dental Clinic">Dental Clinic </a>
                    </li>
                    <li><a href="Residences.aspx#gynaecological-clinic" title="Gynaecological Clinic">Gynaecological
                        Clinic </a></li>
                    <li><a href="Residences.aspx#outpatient-rehabilitation" title="Outpatient Rehabilitation">
                        Outpatient Rehabilitation </a></li>
                    <li><a href="Residences.aspx#cardiac-clinic" title="Cardiac Clinic">Cardiac Clinic </a>
                    </li>
                </ul>
            </li>
            <li><a href="About.aspx" title="Nosotros">NOSOTROS </a></li>
            <li><a href="Contact.aspx" title="Contacto">CONTACTO </a></li>
        </ul>
    </div>
</div>