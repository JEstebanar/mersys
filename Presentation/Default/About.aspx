﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default/Site.Master" AutoEventWireup="true"
    CodeBehind="About.aspx.cs" Inherits="Presentation.Default.About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page relative">
        <div class="page_layout page_margin_top clearfix">
            <div class="page_header clearfix">
                <div class="page_header_left">
                    <h1 class="page_title">
                        About</h1>
                    <ul class="bread_crumb">
                        <li><a href="Home.aspx" title="Home">Home </a></li>
                        <li class="separator icon_small_arrow right_gray">&nbsp; </li>
                        <li>About </li>
                    </ul>
                </div>
                <div class="page_header_right">
                    <form class="search">
                    <input class="search_input" type="text" value="To search type and hit enter..." placeholder="To search type and hit enter..." />
                    </form>
                </div>
            </div>
            <div class="clearfix">
                <div class="gallery_item_details_list clearfix page_margin_top">
                    <div class="gallery_item_details clearfix">
                        <div class="columns no_width">
                            <div class="column_left">
                                <div class="gallery_box">
                                    <ul class="image_carousel">
                                        <li class="current_slide">
                                            <img src="images/samples/480x300/image_01.jpg" alt="" />
                                            <ul class="controls">
                                                <li><a href="images/samples/image_01.jpg" rel="gallery" class="fancybox open_lightbox">
                                                </a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <img src="images/samples/480x300/image_02.jpg" alt="" />
                                            <ul class="controls">
                                                <li><a href="images/samples/image_02.jpg" rel="gallery" class="fancybox open_lightbox">
                                                </a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="column_right">
                                <div class="details_box">
                                    <h2>
                                        Welcome to medicenter
                                    </h2>
                                    <p>
                                        Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id
                                        nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus
                                        posuere velit aliquet. Duis mollis, est non commodo luctus, nisi erat porttitor.
                                    </p>
                                    <h3 class="box_header margin_top_10">
                                        Our Motto
                                    </h3>
                                    <h3 class="sentence">
                                        The mind has great influence over the body and maladies often have their origin
                                        there.
                                    </h3>
                                    <span class="sentence_author">&#8212;&nbsp;&nbsp;John Doe Molicere</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h3 class="box_header page_margin_top_section">
                    Meet The Team
                </h3>
                <ul class="gallery">
                    <li class="gallery_box">
                        <img src="images/samples/225x150/image_03.jpg" alt="" />
                        <div class="description">
                            <h3>
                                Ann Blyumin, Prof.
                            </h3>
                            <h5>
                                Pediatric Clinic
                            </h5>
                        </div>
                        <div class="item_details">
                            Donec sed odio dui. Nulla vitae elit libero, a pharetra augue.Duis mollis, est non
                            commodo luctus.
                        </div>
                        <ul class="social_icons clearfix">
                            <li><a class="social_icon facebook" href="http://facebook.com/QuanticaLabs" title="">
                                &nbsp; </a></li>
                            <li><a class="social_icon googleplus" href="#" title="">&nbsp; </a></li>
                            <li><a class="social_icon mail" href="mailto:medicenter@mail.com" title="">&nbsp; </a>
                            </li>
                            <li><a class="social_icon forrst" href="#" title="">&nbsp; </a></li>
                        </ul>
                        <ul class="controls">
                            <li><a href="doctors.html#gallery-details-1" class="open_details"></a></li>
                            <li><a href="images/samples/image_03.jpg" rel="team" class="fancybox open_lightbox">
                            </a></li>
                        </ul>
                    </li>
                    <li class="gallery_box">
                        <img src="images/samples/225x150/image_01.jpg" alt="" />
                        <div class="description">
                            <h3>
                                Robert Brown, Prof.
                            </h3>
                            <h5>
                                Primary Health Care
                            </h5>
                        </div>
                        <div class="item_details">
                            Donec sed odio dui. Nulla vitae elit libero, a pharetra augue.Duis mollis, est non
                            commodo luctus.
                        </div>
                        <ul class="social_icons clearfix">
                            <li><a class="social_icon facebook" href="http://facebook.com/QuanticaLabs" title="">
                                &nbsp; </a></li>
                            <li><a class="social_icon googleplus" href="#" title="">&nbsp; </a></li>
                            <li><a class="social_icon linkedin" href="#" title="">&nbsp; </a></li>
                            <li><a class="social_icon xing" href="#" title="">&nbsp; </a></li>
                        </ul>
                        <ul class="controls">
                            <li><a href="doctors.html#gallery-details-2" class="open_details"></a></li>
                            <li><a href="images/samples/image_01.jpg" rel="team" class="fancybox open_lightbox">
                            </a></li>
                        </ul>
                    </li>
                    <li class="gallery_box">
                        <img src="images/samples/225x150/image_02.jpg" alt="" />
                        <div class="description">
                            <h3>
                                Clare Mitchell, Prof.
                            </h3>
                            <h5>
                                Cardiac Clinic
                            </h5>
                        </div>
                        <div class="item_details">
                            Donec sed odio dui. Nulla vitae elit libero, a pharetra augue.Duis mollis, est non
                            commodo luctus.
                        </div>
                        <ul class="social_icons clearfix">
                            <li><a class="social_icon twitter" href="https://twitter.com/QuanticaLabs" title="">
                                &nbsp; </a></li>
                            <li><a class="social_icon mail" href="mailto:medicenter@mail.com" title="">&nbsp; </a>
                            </li>
                            <li><a class="social_icon myspace" href="#" title="">&nbsp; </a></li>
                        </ul>
                        <ul class="controls">
                            <li><a href="doctors.html#gallery-details-3" class="open_details"></a></li>
                            <li><a href="images/samples/image_02.jpg" rel="team" class="fancybox open_lightbox">
                            </a></li>
                        </ul>
                    </li>
                    <li class="gallery_box">
                        <img src="images/samples/225x150/image_04.jpg" alt="" />
                        <div class="description">
                            <h3>
                                Earlene Milone, Prof.
                            </h3>
                            <h5>
                                Pediatric Clinic
                            </h5>
                        </div>
                        <div class="item_details">
                            Donec sed odio dui. Nulla vitae elit libero, a pharetra augue.Duis mollis, est non
                            commodo luctus.
                        </div>
                        <ul class="social_icons clearfix">
                            <li><a class="social_icon facebook" href="http://www.facebook.com/QuanticaLabs" title="">
                                &nbsp; </a></li>
                            <li><a class="social_icon twitter" href="https://twitter.com/QuanticaLabs" title="">
                                &nbsp; </a></li>
                        </ul>
                        <ul class="controls">
                            <li><a href="doctors.html#gallery-details-4" class="open_details"></a></li>
                            <li><a href="images/samples/image_04.jpg" rel="team" class="fancybox open_lightbox">
                            </a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>
