﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default/Site.Master" AutoEventWireup="true"
    CodeBehind="Home.aspx.cs" Inherits="Presentation.Default.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- slider -->
    <ul class="slider">
        <li style="background-image: url('images/slider/img1.jpg');">&nbsp; </li>
        <li style="background-image: url('images/slider/img2.jpg');">&nbsp; </li>
        <li style="background-image: url('images/slider/img3.jpg');">&nbsp; </li>
    </ul>
    <div class="page relative noborder">
        <!--<div class="top_hint">
					Give us a call: +123 356 123 124
				</div>-->
        <!-- slider content -->
        <div class="slider_content_box clearfix">
            <div class="slider_content" style="display: block;">
                <h1 class="title">
                    Enfócate<br />
                    en lo que<br />
                    quieres
                </h1>
                <h2 class="subtitle">
                    No existen límites para tus sueños <br />
                </h2>
            </div>            
            <div class="slider_content">
                <h1 class="title">
                    Interés por<br />
                    las personas
                </h1>
                <h2 class="subtitle">
                    Muestra lo profesional que eres <br />
                    con tus pacientes
                </h2>
            </div>        
            <div class="slider_content">
                <h1 class="title">
                    Transmite <br />
                    tu pasión
                </h1>
                <h2 class="subtitle">
                    El camino es largo pero tus deseos <br />
                    y sueños son más fuertes
                </h2>
            </div>    
            <!--<div class="slider_navigation">
						<div class="slider_bar"></div>
						<a class="slider_control" id="slide_1_control" href="#" title="1">
							<span class="top_border"></span>
							<span class="slider_control_bar"></span>
							1
						</a>
						<a class="slider_control" id="slide_2_control" href="#" title="2">
							<span class="top_border"></span>
							<span class="slider_control_bar"></span>
							2
						</a>
						<a class="slider_control" id="slide_3_control" href="#" title="3">
							<span class="top_border"></span>
							<span class="slider_control_bar"></span>
							3
						</a>
					</div>-->
        </div>
        <!-- home box -->
        <ul class="home_box_container clearfix">
            <li class="home_box light_blue">
                <h2>
                    <a href="Contact.aspx" title="Casos de Emergencias">Casos de Emergencias </a>
                </h2>
                <div class="news clearfix">
                    <p class="text">
                        If you need a doctor urgently outside of medicenter opening hours, call emergency
                        appointment number for emergency service.
                    </p>
                    <a class="more light icon_small_arrow margin_right_white" href="Contact.aspx" title="Read more">
                        Leer Más</a>
                </div>
            </li>
            <li class="home_box blue">
                <h2>
                    <a href="#" title="Especialidades">Especialidades </a>
                </h2>
                <div class="news clearfix">
                    <p class="text">
                        Here at medicenter we have individual doctor's lists. Click read more below to see
                        services and current timetable for our doctors.
                    </p>
                    <a class="more light icon_small_arrow margin_right_white" href="#" title="Read more">
                        Leer más</a>
                </div>
            </li>
            <%--<li class="home_box dark_blue">
                <h2>
                    Opening Hours
                </h2>
                <ul class="items_list thin dark_blue opening_hours">
                    <li class="clearfix"><span>Monday - Friday </span>
                        <div class="value">
                            8.00 - 17.00
                        </div>
                    </li>
                    <li class="clearfix"><span>Saturday </span>
                        <div class="value">
                            9.30 - 17.30
                        </div>
                    </li>
                    <li class="clearfix"><span>Sunday </span>
                        <div class="value">
                            9.30 - 15.00
                        </div>
                    </li>
                </ul>
            </li>--%>
        </ul>
        <div class="page_layout page_margin_top clearfix">
            <div class="page_left">
                <h3 class="box_header">
                    Ultimas Noticias
                </h3>
                <div class="columns clearfix">
                    <ul class="blog column_left">
                        <li class="post">
                            <ul class="comment_box clearfix">
                                <li class="date">
                                    <div class="value">
                                        12 DEC 12</div>
                                    <div class="arrow_date">
                                    </div>
                                </li>
                                <li class="comments_number"><a href="post.html#comments_list" title="2 comments">2 </a>
                                </li>
                            </ul>
                            <div class="post_content">
                                <a class="post_image" href="post.html" title="Lorem ipsum dolor sit amat velum">
                                    <img src="images/samples/480x300/image_03.jpg" alt="" />
                                </a>
                                <h2>
                                    <a href="post.html" title="Lorem ipsum dolor sit amat velum">Lorem ipsum dolor sit amat
                                        velum </a>
                                </h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut volutpat rutrum eros
                                    sit amet sollicitudin. Suspendisse pulvinar, velit nec pharetra interdum, ante tellus
                                    ornare mi, et mollis tellus neque vitae elit. Mauris adipiscing mauris.
                                </p>
                                <div class="post_footer">
                                    <ul class="post_footer_details">
                                        <li>Posted in </li>
                                        <li><a href="#" title="General">General, </a></li>
                                        <li><a href="#" title="Dental clinic">Dental clinic </a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="post">
                            <ul class="comment_box clearfix">
                                <li class="date">
                                    <div class="value">
                                        11 DEC 12</div>
                                    <div class="arrow_date">
                                    </div>
                                </li>
                                <li class="comments_number"><a href="post.html#comments_list" title="8 comments">8 </a>
                                </li>
                            </ul>
                            <div class="post_content">
                                <h2>
                                    <a href="post.html" title="Lorem ipsum dolor sit amat velum">Lorem ipsum dolor sit amat
                                        velum </a>
                                </h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut volutpat rutrum eros
                                    sit amet sollicitudin. Suspendisse pulvinar, velit nec pharetra interdum, ante tellus
                                    ornare mi, et mollis tellus neque vitae elit. Mauris adipiscing mauris.
                                </p>
                                <div class="post_footer">
                                    <ul class="post_footer_details">
                                        <li>Posted in</li>
                                        <li><a href="#" title="General">General, </a></li>
                                        <li><a href="#" title="Outpatient surgery">Outpatient surgery </a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul class="blog column_right">
                        <li class="post">
                            <ul class="comment_box clearfix">
                                <li class="date">
                                    <div class="value">
                                        12 DEC 12</div>
                                    <div class="arrow_date">
                                    </div>
                                </li>
                                <li class="comments_number"><a href="post.html#comments_list" title="3 comments">3 </a>
                                </li>
                            </ul>
                            <div class="post_content">
                                <h2>
                                    <a href="post.html" title="Lorem ipsum dolor sit amat velum">Lorem ipsum dolor sit amat
                                        velum </a>
                                </h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut volutpat rutrum eros
                                    sit amet sollicitudin. Suspendisse pulvinar, velit nec pharetra interdum, ante tellus
                                    ornare mi, et mollis tellus neque vitae elit. Mauris adipiscing mauris.
                                </p>
                                <div class="post_footer">
                                    <ul class="post_footer_details">
                                        <li>Posted in </li>
                                        <li><a href="#" title="General">General, </a></li>
                                        <li><a href="#" title="Dental clinic">Dental clinic </a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="post">
                            <ul class="comment_box clearfix">
                                <li class="date">
                                    <div class="value">
                                        11 DEC 12</div>
                                    <div class="arrow_date">
                                    </div>
                                </li>
                                <li class="comments_number"><a href="post.html#comments_list" title="5 comments">5 </a>
                                </li>
                            </ul>
                            <div class="post_content">
                                <a class="post_image" href="post.html" title="Lorem ipsum dolor sit amat velum">
                                    <img src="images/samples/480x300/image_02.jpg" alt="" />
                                </a>
                                <h2>
                                    <a href="post.html" title="Lorem ipsum dolor sit amat velum">Lorem ipsum dolor sit amat
                                        velum </a>
                                </h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut volutpat rutrum eros
                                    sit amet sollicitudin. Suspendisse pulvinar, velit nec pharetra interdum, ante tellus
                                    ornare mi, et mollis tellus neque vitae elit. Mauris adipiscing mauris.
                                </p>
                                <div class="post_footer">
                                    <ul class="post_footer_details">
                                        <li>Posted in</li>
                                        <li><a href="#" title="General">General, </a></li>
                                        <li><a href="#" title="Outpatient surgery">Outpatient surgery </a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="show_all clearfix">
                    <a class="more" href="blog.html" title="Show all">Ver todo &rarr; </a>
                </div>
            </div>
            <div class="page_right">
                <div class="sidebar_box first">
                    <h3 class="box_header">
                        Residencias
                    </h3>
                    <ul class="accordion">
                        <li>
                            <div id="accordion-GeneralSurgery">
                                <h3>
                                    Cirugía General</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Cirugía General">
                                        <img src="images/samples/75x75/image_08.jpg" alt="" />
                                    </a>
                                    <p>
                                        Mauris adisciping fringila turpis intend tellus ornare etos pelim. Pulvunar est
                                        cardio neque vitae elit. Lorem vulputat paentra nunc gravida.
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white"
                                            href="#" title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-pediatric-clinic">
                                <h3>
                                    Cuidados Intensivos</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Cuidados Intensivos">
                                        <img src="images/samples/75x75/image_05.jpg" alt="" />
                                    </a>
                                    <p>
                                        Mauris adisciping fringila turpis intend tellus ornare etos pelim. Pulvunar est
                                        cardio neque vitae elit. Lorem vulputat paentra nunc gravida.
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white"
                                            href="#" title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-outpatient-surgery">
                                <h3>
                                    Medicina Interna</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Medicina Interna">
                                        <img src="images/samples/75x75/image_07.jpg" alt="" />
                                    </a>
                                    <p>
                                        Mauris adisciping fringila turpis intend tellus ornare etos pelim. Pulvunar est
                                        cardio neque vitae elit. Lorem vulputat paentra nunc gravida.
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white"
                                            href="#" title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-gynaecological-clinic">
                                <h3>
                                    Anatomía Patológica</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Anatomía Patológica">
                                        <img src="images/samples/75x75/image_01.jpg" alt="" />
                                    </a>
                                    <p>
                                        Mauris adisciping fringila turpis intend tellus ornare etos pelim. Pulvunar est
                                        cardio neque vitae elit. Lorem vulputat paentra nunc gravida.
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                     <a class="more blue icon_small_arrow margin_right_white"
                                            href="#" title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-cardiac-clinic">
                                <h3>
                                    Medicina Materno Fetal</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Medicina Materno Fetal">
                                        <img src="images/samples/75x75/image_04.jpg" alt="" />
                                    </a>
                                    <p>
                                        Mauris adisciping fringila turpis intend tellus ornare etos pelim. Pulvunar est
                                        cardio neque vitae elit. Lorem vulputat paentra nunc gravida.
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                     <a class="more blue icon_small_arrow margin_right_white"
                                            href="#" title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-laryngological-clinic">
                                <h3>
                                    Ortopedia y Trauma</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Ortopedia y Trauma">
                                        <img src="images/samples/75x75/image_08.jpg" alt="" />
                                    </a>
                                    <p>
                                        Mauris adisciping fringila turpis intend tellus ornare etos pelim. Pulvunar est
                                        cardio neque vitae elit. Lorem vulputat paentra nunc gravida.
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white"
                                            href="#" title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-ophthalmology-clinic">
                                <h3>
                                    Anestesiología</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Anestesiología">
                                        <img src="images/samples/75x75/image_06.jpg" alt="" />
                                    </a>
                                    <p>
                                        Mauris adisciping fringila turpis intend tellus ornare etos pelim. Pulvunar est
                                        cardio neque vitae elit. Lorem vulputat paentra nunc gravida.
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                     <a class="more blue icon_small_arrow margin_right_white"
                                            href="#" title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-dental-clinic">
                                <h3>
                                    Gineco-Obstetricia</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Gineco-Obstetricia">
                                        <img src="images/samples/75x75/image_03.jpg" alt="" />
                                    </a>
                                    <p>
                                        Mauris adisciping fringila turpis intend tellus ornare etos pelim. Pulvunar est
                                        cardio neque vitae elit. Lorem vulputat paentra nunc gravida.
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                     <a class="more blue icon_small_arrow margin_right_white"
                                            href="#" title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div id="accordion-outpatient-rehabilitation">
                                <h3>
                                    Radiología</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Radiología">
                                        <img src="images/samples/75x75/image_05.jpg" alt="" />
                                    </a>
                                    <p>
                                        Mauris adisciping fringila turpis intend tellus ornare etos pelim. Pulvunar est
                                        cardio neque vitae elit. Lorem vulputat paentra nunc gravida.
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white"
                                            href="#" title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>

                        <li>
                            <div id="accordion-">
                                <h3>
                                    Geriátrica</h3>
                            </div>
                            <div class="clearfix">
                                <div class="item_content clearfix">
                                    <a class="thumb_image" href="#" title="Geriátrica">
                                        <img src="images/samples/75x75/image_05.jpg" alt="" />
                                    </a>
                                    <p>
                                        Mauris adisciping fringila turpis intend tellus ornare etos pelim. Pulvunar est
                                        cardio neque vitae elit. Lorem vulputat paentra nunc gravida.
                                    </p>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more blue icon_small_arrow margin_right_white"
                                            href="#" title="Detalles">Detalles</a>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>
                <div class="sidebar_box">
                    <h3 class="box_header">
                        Make An Appointment
                    </h3>
                    <p class="info">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut volutpat rutrum eros
                        sit amet sollicitudin.
                    </p>
                    <ul class="contact_data">
                        <li class="clearfix"><span class="social_icon phone"></span>
                            <p class="value">
                                by phone: <strong>1-800-643-4300</strong>
                            </p>
                        </li>
                        <li class="clearfix"><span class="social_icon mail"></span>
                            <p class="value">
                                by e-mail: <a href="mailto:medicenter@mail.com">medicenter@mail.com</a>
                            </p>
                        </li>
                        <li class="clearfix"><span class="social_icon form"></span>
                            <p class="value">
                                or <a href="Contact.aspx" title="Contact form">fill in the form</a> on our contact
                                page
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
