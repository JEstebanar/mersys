﻿<%@ Page Title="Residencias" Language="C#" MasterPageFile="~/medicenter.Master" AutoEventWireup="true"
    CodeBehind="Residencies.aspx.cs" Inherits="Presentation.Residencies" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="page relative">
        <div class="page_layout page_margin_top clearfix">
            <div class="page_header clearfix">
                <div class="page_header_left">
                    <h1 class="page_title">
                        Especialidades</h1>
                    <ul class="bread_crumb">
                        <li><a href="Home.aspx" title="Home">Inicio </a></li>
                        <li class="separator icon_small_arrow right_gray">&nbsp; </li>
                        <li>Residencias </li>
                    </ul>
                </div>
                <%--<div class="page_header_right">
                    <form class="search">
                    <input class="search_input" type="text" value="To search type and hit enter..." placeholder="To search type and hit enter..." />
                    </form>
                </div>--%>
            </div>
            <div class="page_left page_margin_top">
                <ul class="accordion wide">
                    <li>
                        <div id="accordion-pathology">
                            <h3>
                                Anatomía Patológica</h3>
                        </div>
                        <div class="clearfix tabs">
                            <%--<ul class="tabs_navigation clearfix">
                                <li><a href="#pathology-general" title="Información General">Información General </a></li>
                                <li><a href="#pathology-doctors" title="Doctors list">Doctors list </a></li>
                            </ul>--%>
                            <div id="pathology-general">
                                <p>
                                    La Anatomía Patológica es la rama de la Medicina que se ocupa del estudio, por medio
                                    de técnicas morfológicas, de las causas, desarrollo y consecuencias de las enfermedades.
                                    El fin último es el diagnóstico correcto de biopsias, piezas quirúrgicas, citologías
                                    y autopsias. En el caso de la Medicina, el ámbito fundamental son las enfermedades
                                    humanas. La Anatomía Patológica es una especialidad médica que posee un cuerpo doctrinal
                                    de carácter básico que hace que sea, por una parte, una disciplina académica autónoma
                                    y, por otra, una unidad funcional en la asistencia médica.
                                </p>
                            </div>
                            <div id="pathology-doctors">
                                <ul class="doctors_list">
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="medicenter/images/samples/480x300/image_03.jpg" alt="" />
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Información General
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Avalado Por
                                                            </label>
                                                            <div class="text">
                                                                PUCMM
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Pre Requisito
                                                            </label>
                                                            <div class="text">
                                                                Médico General
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Duración
                                                            </label>
                                                            <div class="text">
                                                                4 Años
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div id="accordion-anesthesiology">
                            <h3>
                                Anestesiología</h3>
                        </div>
                        <div class="clearfix tabs">
                            <%--<ul class="tabs_navigation clearfix">
                                <li><a href="#anesthesiology-general" title="Información General">Información General </a></li>
                                <li><a href="#anesthesiology-doctors" title="Doctors list">Doctors list </a></li>
                            </ul>--%>
                            <div id="anesthesiology-general">
                                <p>
                                    La anestesiología es la especialidad médica dedicada a la atención y cuidados especiales
                                    de los pacientes durante las intervenciones quirúrgicas u otros procesos que puedan
                                    resultar molestos o dolorosos (endoscopia, radiología intervencionista, etc.). Asimismo,
                                    tiene a su cargo el tratamiento del dolor agudo o crónico de causa extraquirúrgica.
                                    Ejemplos de estos últimos son la analgesia durante el trabajo de parto y el alivio
                                    del dolor en pacientes con cáncer. La especialidad recibe el nombre de anestesiología
                                    y reanimación, dado que abarca el tratamiento del paciente crítico en distintas
                                    áreas como lo son la recuperación postoperatoria y la emergencia, así como el cuidado
                                    del paciente crítico en las unidades de cuidados intensivos o de reanimación postoperatoria.
                                    La especialidad médica de la medicina intensiva es un brazo más de la anestesiología.
                                </p>
                            </div>
                            <div id="anesthesiology-doctors">
                                <ul class="doctors_list">
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="medicenter/images/samples/480x300/image_03.jpg" alt="" />
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Información General
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Avalado Por
                                                            </label>
                                                            <div class="text">
                                                                PUCMM
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Pre Requisito
                                                            </label>
                                                            <div class="text">
                                                                Médico General
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Duración
                                                            </label>
                                                            <div class="text">
                                                                4 Años
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div id="accordion-cardiology">
                            <h3>
                                Cardiología</h3>
                        </div>
                        <div class="clearfix tabs">
                            <%--<ul class="tabs_navigation clearfix">
                                <li><a href="#cardiology-general" title="General info">General info </a>
                                <li><a href="#cardiology-doctors" title="Doctors list">Doctors list </a>
                            </ul>--%>
                            <div id="cardiology-general">
                                <p>
                                    La cardiología es la rama de la medicina interna, encargada de las enfermedades
                                    del corazón y del aparato circulatorio. Se incluye dentro de las especialidades
                                    médicas quirúrgicas.<br />
                                    La cardiología es un campo complejo, por eso muchos cardiólogos se subespecializan
                                    en diferentes áreas, como son la electrofisiólogia, cardiología intervencionista,
                                    cardiología nuclear, ecocardiografía, rehabilitación cardíaca, terapia intensiva
                                    cardiológica y unidad coronaria.
                                </p>
                            </div>
                            <div id="cardiology-doctors">
                                <ul class="doctors_list">
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="medicenter/images/samples/480x300/image_03.jpg" alt="" />
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Información General
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Avalado Por
                                                            </label>
                                                            <div class="text">
                                                                PUCMM
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Pre Requisito
                                                            </label>
                                                            <div class="text">
                                                                3 Años de Medicina Interna
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Duración
                                                            </label>
                                                            <div class="text">
                                                                2 Años
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div id="accordion-cervico-buco-maxilo-facial">
                            <h3>
                                Cervico Buco Maxilo Facial</h3>
                        </div>
                        <div class="clearfix tabs">
                            <%--<ul class="tabs_navigation clearfix">
                                <li><a href="#cervico-buco-maxilo-facial-general" title="General info">General info </a>
                                <li><a href="#cervico-buco-maxilo-facial-doctors" title="Doctors list">Doctors list </a>
                            </ul>--%>
                            <div id="cervico-buco-maxilo-facial-general">
                                <h3 class="sentence">
                                    Cervico Buco Maxilo Facial es una especialidad quirúrgica que incluye el diagnóstico,
                                    cirugía y tratamientos relacionados de un gran espectro de enfermedades, heridas
                                    y aspectos estéticos de la boca, dientes, cara, cabeza y cuello.
                                </h3>
                            </div>
                            <div id="cervico-buco-maxilo-facial-doctors">
                                <ul class="doctors_list">
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="medicenter/images/samples/480x300/image_03.jpg" alt="" />
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Información General
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Avalado Por
                                                            </label>
                                                            <div class="text">
                                                                PUCMM
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Pre Requisito
                                                            </label>
                                                            <div class="text">
                                                                Médico General
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Duración
                                                            </label>
                                                            <div class="text">
                                                                4 Años
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div id="accordion-general-surgery">
                            <h3>
                                Cirugía General</h3>
                        </div>
                        <div class="clearfix tabs">
                            <%--<ul class="tabs_navigation clearfix">
                                <li><a href="#general-surgery-general" title="General info">General info </a>
                                <li><a href="#general-surgery-doctors" title="Doctors list">Doctors list </a>
                            </ul>--%>
                            <div id="general-surgery-general">
                                <p>
                                    La cirugía general es la especialidad médica de clase quirúrgica que abarca las
                                    operaciones del aparato digestivo; incluyendo el tracto gastrointestinal y el sistema
                                    hepato-bilio-pancreático, el sistema endocrino; incluyendo las glándulas suprarrenales,
                                    tiroides, paratiroides y otras glándulas incluidas en el aparato digestivo. Asimismo
                                    incluye la reparación de hernias y eventraciones de la pared abdominal. En estas
                                    áreas de la cirugía no se precisa un especialista aunque el cirujano general puede
                                    especializarse en alguna de ellas. Esto no es igual en todos los países ya que en
                                    algunos es considerada una especialidad más y se entiende por súper especialización
                                    la profundización en una de sus ramas quirúrgicas.
                                </p>
                            </div>
                            <div id="general-surgery-doctors">
                                <ul class="doctors_list">
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="medicenter/images/samples/480x300/image_01.jpg" alt="" />
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Información General
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Avalado Por
                                                            </label>
                                                            <div class="text">
                                                                PUCMM
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Pre Requisito
                                                            </label>
                                                            <div class="text">
                                                                Medico General
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Duración
                                                            </label>
                                                            <div class="text">
                                                                4 Años
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div id="accordion-intensive-care">
                            <h3>
                                Cuidados Intensivos</h3>
                        </div>
                        <div class="clearfix tabs">
                            <%--<ul class="tabs_navigation clearfix">
                                <li><a href="#intensive-care-general" title="General info">General info </a></li>
                                <li><a href="#intensive-care-doctors" title="Doctors list">Doctors list </a></li>
                            </ul>--%>
                            <div id="intensive-care-general">
                                <p>
                                    La medicina intensiva es una especialidad médica dedicada al suministro de soporte
                                    vital o de soporte a los sistemas orgánicos en los pacientes que están críticamente
                                    enfermos, quienes generalmente también requieren supervisión y monitorización intensiva.
                                    Los pacientes que requieren cuidados intensivos, por lo general también necesitan
                                    soporte para la inestabilidad hemodinámica (hipotensión o hipertensión), para las
                                    vías aéreas o el compromiso respiratorio o el fracaso renal, y a menudo los tres.
                                    Los pacientes admitidos en las unidadades de cuidados intensivos (UCI), también
                                    llamadas unidades de vigilancia intensiva (UVI), que no requieren soporte para lo
                                    antedicho, generalmente son admitidos para la supervisión intensiva/invasora, habitualmente
                                    después de cirugía mayor.</p>
                            </div>
                            <div id="intensive-care-doctors">
                                <ul class="doctors_list">
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="medicenter/images/samples/480x300/image_02.jpg" alt="" />
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Información General
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Pre Requisisto
                                                            </label>
                                                            <div class="text">
                                                                3 Años de Medicina Interna
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Pre Requisisto
                                                            </label>
                                                            <div class="text">
                                                                Medico General
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Duración
                                                            </label>
                                                            <div class="text">
                                                                3 Años
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Duración
                                                            </label>
                                                            <div class="text">
                                                                6 Años
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div id="accordion-emergenciologia">
                            <h3>
                                Emergenciología</h3>
                        </div>
                        <div class="clearfix tabs">
                            <%--<ul class="tabs_navigation clearfix">
                                <li><a href="#emergenciologia-general" title="General info">General info </a>
                                <li><a href="#emergenciologia-doctors" title="Doctors list">Doctors list </a>
                            </ul>--%>
                            <div id="emergenciologia-general">
                                <h3 class="sentence">
                                    La medicina de emergencia, emergenciología, emergentología o medicina de urgencias
                                    es la que actúa sobre una emergencia médica o urgencia médica o sobre cualquier
                                    enfermedad en su momento agudo, definida como una lesión o enfermedad que plantean
                                    una amenaza inmediata para la vida de una persona y cuya asistencia no puede ser
                                    demorada.
                                </h3>
                            </div>
                            <div id="emergenciologia-doctors">
                                <ul class="doctors_list">
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="medicenter/images/samples/480x300/image_03.jpg" alt="" />
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Información General
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Avalado Por
                                                            </label>
                                                            <div class="text">
                                                                PUCMM
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Pre Requisito
                                                            </label>
                                                            <div class="text">
                                                                Médico General
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Duración
                                                            </label>
                                                            <div class="text">
                                                                4 Años
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <!--<li>
                        <div id="accordion-physiatry">
                            <h3>
                                Fisiatría</h3>
                        </div>
                        <div class="clearfix tabs">
                            <%-- <ul class="tabs_navigation clearfix">
                                <li><a href="#physiatry-general" title="General info">General info </a></li>
                                <li><a href="#physiatry-doctors" title="Doctors list">Doctors list </a></li>
                            </ul>--%>
                            <div id="physiatry-general">
                                <h3 class="sentence">
                                    La medicina física y rehabilitación, también llamada fisiatría, es una especialidad
                                    de la medicina y de las ciencias de la salud, configurada por un cuerpo doctrinal
                                    complejo, constituido por la agrupación de conocimientos y experiencias relativas
                                    a la naturaleza de los agentes físicos no ionizantes, a los fenómenos derivados
                                    de su interacción con el organismo y su aplicación diagnóstica, terapéutica y preventiva.
                                    Comprende el estudio, detección y diagnóstico, prevención y tratamiento clínico
                                    o quirúrgico de los enfermos con procesos discapacitantes.
                                </h3>
                            </div>
                            <div id="physiatry-doctors">
                                <ul class="doctors_list">
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="medicenter/images/samples/480x300/image_03.jpg" alt="" />
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Información General
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Avalado Por
                                                            </label>
                                                            <div class="text">
                                                                PUCMM
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Pre Requisito
                                                            </label>
                                                            <div class="text">
                                                                Médico General
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Duración
                                                            </label>
                                                            <div class="text">
                                                                3 Años
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>-->
                    <li>
                        <div id="accordion-geriatria">
                            <h3>
                                Geriatría</h3>
                        </div>
                        <div class="clearfix tabs">
                            <%-- <ul class="tabs_navigation clearfix">
                                <li><a href="#geriatria-general" title="General info">General info </a></li>
                                <li><a href="#geriatria-doctors" title="Doctors list">Doctors list </a></li>
                            </ul>--%>
                            <div id="geriatria-general">
                                <h3 class="sentence">
                                    La Geriatría es una especialidad médica dedicada al estudio de la prevención, el
                                    diagnóstico, el tratamiento y la rehabilitación de las enfermedades en la senectud.
                                    La Geriatría resuelve los problemas de salud de los ancianos; sin embargo, la Gerontología
                                    estudia los aspectos psicológicos, educativos, sociales, económicos y demográficos
                                    de la tercera edad.
                                </h3>
                            </div>
                            <div id="geriatria-doctors">
                                <ul class="doctors_list">
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="medicenter/images/samples/480x300/image_04.jpg" alt="" />
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Información General
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Avalado Por
                                                            </label>
                                                            <div class="text">
                                                                UNPHU
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Pre Requisito
                                                            </label>
                                                            <div class="text">
                                                                Médico General
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Duración
                                                            </label>
                                                            <div class="text">
                                                                4 Años
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div id="accordion-gineco-obstetricia">
                            <h3>
                                Gineco-Obstetricia</h3>
                        </div>
                        <div class="clearfix tabs">
                            <%--<ul class="tabs_navigation clearfix">
                                <li><a href="#gineco-obstetricia-general" title="General info">Información General </a>
                                </li>
                            </ul>--%>
                            <div id="gineco-obstetricia-general">
                                <h3 class="sentence">
                                    La Gineco-Obstetricia es la especialidad médica dedicada a los campos de la obstetricia y la ginecología
                                    a través de un único programa de formación académica. Esta formación combinada convierte
                                    a los practicantes en expertos en el cuidado de la salud de los órganos reproductores
                                    femeninos y en el manejo de complicaciones obstétricas, incluso a través de intervenciones
                                    quirúrgicas.
                                </h3>
                            </div>
                            <div id="gineco-obstetricia-doctors">
                                <ul class="doctors_list">
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="medicenter/images/samples/480x300/image_03.jpg" alt="" />
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Información General
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Avalado Por
                                                            </label>
                                                            <div class="text">
                                                                PUCMM
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Pre Requisito
                                                            </label>
                                                            <div class="text">
                                                                Médico General
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Duración
                                                            </label>
                                                            <div class="text">
                                                                4 Años
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div id="accordion-hematologia">
                            <h3>
                                Hematología</h3>
                        </div>
                        <div class="clearfix tabs">
                            <%-- <ul class="tabs_navigation clearfix">
                                <li><a href="#hematologia-general" title="General info">General info </a></li>
                                <li><a href="#hematologia-doctors" title="Doctors list">Doctors list </a></li>
                            </ul>--%>
                            <div id="hematologia-general">
                                <h3 class="sentence">
                                    La Hematología es la especialidad médica que se dedica al tratamiento de los pacientes
                                    con enfermedades hematológicas, para ello se encarga del estudio e investigación
                                    de la sangre y los órganos hematopoyéticos (médula ósea, ganglios linfáticos, bazo,
                                    etc) tanto sanos como enfermos. Se encarga del estudio de los elementos formes de
                                    la sangre y sus precursores, así como de los trastornos estructurales y bioquímicos
                                    de estos elementos, que puedan conducir a una enfermedad.
                                </h3>
                            </div>
                            <div id="hematologia-doctors">
                                <ul class="doctors_list">
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="medicenter/images/samples/480x300/image_03.jpg" alt="" />
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Información General
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Avalado Por
                                                            </label>
                                                            <div class="text">
                                                                PUCMM
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Pre Requisito
                                                            </label>
                                                            <div class="text">
                                                                4 Años de Medicina Interna
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Duración
                                                            </label>
                                                            <div class="text">
                                                                2 Años
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <%--                    <li>
                        <div id="accordion-outpatient-rehabilitation">
                            <h3>
                                Outpatient Rehabilitation</h3>
                        </div>
                        <div class="clearfix tabs">
                            <ul class="tabs_navigation clearfix">
                                <li><a href="#outpatient-rehabilitation-general" title="General info">General info </a>
                                </li>
                                <li><a href="#outpatient-rehabilitation-services" title="Services">Services </a>
                                </li>
                                <li><a href="#outpatient-rehabilitation-doctors" title="Doctors list">Doctors list </a>
                                </li>
                                <li><a href="http://quanticalabs.com/Medicenter/Template/timetable.html#outpatient-rehabilitation"
                                    title="Timetable">Timetable </a></li>
                            </ul>
                            <div id="outpatient-rehabilitation-general">
                                <h3 class="sentence">
                                    An average heart beats 100,000 times a day, pumping some 2,000 gallons of blood
                                    through its chambers. Over a 70-year life span, that adds up to more than 2.5 billion
                                    heartbeats.
                                </h3>
                                <div class="columns page_margin_top clearfix">
                                    <div class="column_left">
                                        <h3 class="box_header">
                                            Investigations
                                        </h3>
                                        <ul class="items_list page_margin_top clearfix">
                                            <li class="clearfix"><span>Colonoscopy </span>
                                                <div class="value">
                                                    $250
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Gastroscopy </span>
                                                <div class="value">
                                                    $235
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Allergy testing </span>
                                                <div class="value">
                                                    $135
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>CT scan </span>
                                                <div class="value">
                                                    $575
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="item_footer clearfix">
                                            <a class="more" href="#outpatient-rehabilitation-services">Full list of services &rarr;
                                            </a>
                                        </div>
                                    </div>
                                    <div class="column_right">
                                        <h3 class="box_header">
                                            Treatments
                                        </h3>
                                        <ul class="items_list page_margin_top clearfix">
                                            <li class="clearfix"><span>Bronchoscopy </span>
                                                <div class="value">
                                                    $100 - $150
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Cardiac ablation </span>
                                                <div class="value">
                                                    $150 - $200
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Sport injuries </span>
                                                <div class="value">
                                                    $135
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Women's health </span>
                                                <div class="value">
                                                    $135
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="item_footer clearfix">
                                            <a class="more" href="#outpatient-rehabilitation-services">Full list of services &rarr;
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="box_header page_margin_top">
                                    Doctors
                                </h3>
                                <div class="columns page_margin_top clearfix">
                                    <div class="column_left">
                                        <ul class="items_list clearfix">
                                            <li class="clearfix"><span>Ann Blyumin </span>
                                                <div class="value">
                                                    Pediatrist
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Robert Brown </span>
                                                <div class="value">
                                                    General Medicine
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="column_right">
                                        <ul class="items_list clearfix">
                                            <li class="clearfix"><span>Clare Mitchell </span>
                                                <div class="value">
                                                    Family Doctor
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more" href="#outpatient-rehabilitation-doctors">Full doctors list &rarr;
                                    </a>
                                </div>
                            </div>
                            <div id="outpatient-rehabilitation-services">
                                <h3 class="sentence">
                                    Quick naps not only improve your alertness, but they also help in decision making,
                                    creativity and sensory perception.
                                </h3>
                                <div class="columns page_margin_top clearfix">
                                    <div class="column_left">
                                        <h3 class="box_header">
                                            Investigations
                                        </h3>
                                        <ul class="items_list page_margin_top clearfix">
                                            <li class="clearfix"><span>Colonoscopy </span>
                                                <div class="value">
                                                    $250
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Gastroscopy </span>
                                                <div class="value">
                                                    $235
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Allergy testing </span>
                                                <div class="value">
                                                    $135
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>CT scan </span>
                                                <div class="value">
                                                    $575
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Cardiac CT scan </span>
                                                <div class="value">
                                                    $275
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Paratryroid scan </span>
                                                <div class="value">
                                                    $195
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Captopril renogram </span>
                                                <div class="value">
                                                    $165
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="column_right">
                                        <h3 class="box_header">
                                            Treatments
                                        </h3>
                                        <ul class="items_list page_margin_top clearfix">
                                            <li class="clearfix"><span>Bronchoscopy </span>
                                                <div class="value">
                                                    $100 - $150
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Cardiac ablation </span>
                                                <div class="value">
                                                    $150 - $200
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Sport injuries </span>
                                                <div class="value">
                                                    $135
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Women's health </span>
                                                <div class="value">
                                                    $135
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Orthotics </span>
                                                <div class="value">
                                                    $175
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div id="outpatient-rehabilitation-doctors">
                                <ul class="doctors_list">
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="images/samples/480x300/image_03.jpg" alt="" />
                                                    <div class="description">
                                                        <h3>
                                                            Ann Blyumin, Prof.
                                                        </h3>
                                                        <h5>
                                                            Pediatrist
                                                        </h5>
                                                    </div>
                                                    <ul class="controls">
                                                        <li><a href="images/samples/image_03.jpg" rel="outpatient-rehabilitation-doctors"
                                                            class="fancybox open_lightbox"></a></li>
                                                    </ul>
                                                    <ul class="social_icons clearfix">
                                                        <li><a class="social_icon facebook" href="http://facebook.com/QuanticaLabs" title="">
                                                            &nbsp; </a></li>
                                                        <li><a class="social_icon googleplus" href="#" title="">&nbsp; </a></li>
                                                        <li><a class="social_icon mail" href="mailto:medicenter@mail.com" title="">&nbsp; </a>
                                                        </li>
                                                        <li><a class="social_icon forrst" href="#" title="">&nbsp; </a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Ann Blyumin, Prof.
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Speciality
                                                            </label>
                                                            <div class="text">
                                                                Pediatrist
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Degrees
                                                            </label>
                                                            <div class="text">
                                                                M.D. of Medicine
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Training
                                                            </label>
                                                            <div class="text">
                                                                Donec commodo oido. Nulla vitae elit libero, a pharetra augue. Mauris id Integer
                                                                posuere erat ante dapibus posuere velit aliquet.
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Office
                                                            </label>
                                                            <div class="text">
                                                                367, Hall A
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Work days
                                                            </label>
                                                            <div class="text">
                                                                Monday, Thursday, Friday
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <div class="item_footer clearfix">
                                                        <a title="Timetable" href="timetable.html" class="more">Timetable &rarr; </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="images/samples/480x300/image_01.jpg" alt="" />
                                                    <div class="description">
                                                        <h3>
                                                            Robert Brown, Prof.
                                                        </h3>
                                                        <h5>
                                                            General Medicine
                                                        </h5>
                                                    </div>
                                                    <ul class="controls">
                                                        <li><a href="images/samples/image_01.jpg" rel="outpatient-rehabilitation-doctors"
                                                            class="fancybox open_lightbox"></a></li>
                                                    </ul>
                                                    <ul class="social_icons clearfix">
                                                        <li><a class="social_icon facebook" href="http://facebook.com/QuanticaLabs" title="">
                                                            &nbsp; </a></li>
                                                        <li><a class="social_icon googleplus" href="#" title="">&nbsp; </a></li>
                                                        <li><a class="social_icon linkedin" href="#" title="">&nbsp; </a></li>
                                                        <li><a class="social_icon xing" href="#" title="">&nbsp; </a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Robert Brown, Prof.
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Speciality
                                                            </label>
                                                            <div class="text">
                                                                General Medicine
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Degrees
                                                            </label>
                                                            <div class="text">
                                                                M.D. of Medicine
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Training
                                                            </label>
                                                            <div class="text">
                                                                Donec commodo oido. Nulla vitae elit libero, a pharetra augue. Mauris id Integer
                                                                posuere erat ante dapibus posuere velit aliquet.
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Office
                                                            </label>
                                                            <div class="text">
                                                                367, Hall A
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Work days
                                                            </label>
                                                            <div class="text">
                                                                Monday, Thursday, Friday
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <div class="item_footer clearfix">
                                                        <a title="Timetable" href="timetable.html" class="more">Timetable &rarr; </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="images/samples/480x300/image_02.jpg" alt="" />
                                                    <div class="description">
                                                        <h3>
                                                            Clare Mitchell, Prof.
                                                        </h3>
                                                        <h5>
                                                            Family Doctor
                                                        </h5>
                                                    </div>
                                                    <ul class="controls">
                                                        <li><a href="images/samples/image_02.jpg" rel="outpatient-rehabilitation-doctors"
                                                            class="fancybox open_lightbox"></a></li>
                                                    </ul>
                                                    <ul class="social_icons clearfix">
                                                        <li><a class="social_icon twitter" href="https://twitter.com/QuanticaLabs" title="">
                                                            &nbsp; </a></li>
                                                        <li><a class="social_icon mail" href="mailto:medicenter@mail.com" title="">&nbsp; </a>
                                                        </li>
                                                        <li><a class="social_icon myspace" href="#" title="">&nbsp; </a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Clare Mitchell, Prof.
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Speciality
                                                            </label>
                                                            <div class="text">
                                                                Family Doctor
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Degrees
                                                            </label>
                                                            <div class="text">
                                                                M.D. of Medicine
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Training
                                                            </label>
                                                            <div class="text">
                                                                Donec commodo oido. Nulla vitae elit libero, a pharetra augue. Mauris id Integer
                                                                posuere erat ante dapibus posuere velit aliquet.
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Office
                                                            </label>
                                                            <div class="text">
                                                                367, Hall A
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Work days
                                                            </label>
                                                            <div class="text">
                                                                Monday, Thursday, Friday
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <div class="item_footer clearfix">
                                                        <a title="Timetable" href="timetable.html" class="more">Timetable &rarr; </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>--%>
                    <%--                    <li>
                        <div id="accordion-outpatient-rehabilitation">
                            <h3>
                                Outpatient Rehabilitation</h3>
                        </div>
                        <div class="clearfix tabs">
                            <ul class="tabs_navigation clearfix">
                                <li><a href="#outpatient-rehabilitation-general" title="General info">General info </a>
                                </li>
                                <li><a href="#outpatient-rehabilitation-services" title="Services">Services </a>
                                </li>
                                <li><a href="#outpatient-rehabilitation-doctors" title="Doctors list">Doctors list </a>
                                </li>
                                <li><a href="http://quanticalabs.com/Medicenter/Template/timetable.html#outpatient-rehabilitation"
                                    title="Timetable">Timetable </a></li>
                            </ul>
                            <div id="outpatient-rehabilitation-general">
                                <h3 class="sentence">
                                    An average heart beats 100,000 times a day, pumping some 2,000 gallons of blood
                                    through its chambers. Over a 70-year life span, that adds up to more than 2.5 billion
                                    heartbeats.
                                </h3>
                                <div class="columns page_margin_top clearfix">
                                    <div class="column_left">
                                        <h3 class="box_header">
                                            Investigations
                                        </h3>
                                        <ul class="items_list page_margin_top clearfix">
                                            <li class="clearfix"><span>Colonoscopy </span>
                                                <div class="value">
                                                    $250
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Gastroscopy </span>
                                                <div class="value">
                                                    $235
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Allergy testing </span>
                                                <div class="value">
                                                    $135
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>CT scan </span>
                                                <div class="value">
                                                    $575
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="item_footer clearfix">
                                            <a class="more" href="#outpatient-rehabilitation-services">Full list of services &rarr;
                                            </a>
                                        </div>
                                    </div>
                                    <div class="column_right">
                                        <h3 class="box_header">
                                            Treatments
                                        </h3>
                                        <ul class="items_list page_margin_top clearfix">
                                            <li class="clearfix"><span>Bronchoscopy </span>
                                                <div class="value">
                                                    $100 - $150
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Cardiac ablation </span>
                                                <div class="value">
                                                    $150 - $200
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Sport injuries </span>
                                                <div class="value">
                                                    $135
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Women's health </span>
                                                <div class="value">
                                                    $135
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="item_footer clearfix">
                                            <a class="more" href="#outpatient-rehabilitation-services">Full list of services &rarr;
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="box_header page_margin_top">
                                    Doctors
                                </h3>
                                <div class="columns page_margin_top clearfix">
                                    <div class="column_left">
                                        <ul class="items_list clearfix">
                                            <li class="clearfix"><span>Ann Blyumin </span>
                                                <div class="value">
                                                    Pediatrist
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Robert Brown </span>
                                                <div class="value">
                                                    General Medicine
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="column_right">
                                        <ul class="items_list clearfix">
                                            <li class="clearfix"><span>Clare Mitchell </span>
                                                <div class="value">
                                                    Family Doctor
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item_footer clearfix">
                                    <a class="more" href="#outpatient-rehabilitation-doctors">Full doctors list &rarr;
                                    </a>
                                </div>
                            </div>
                            <div id="outpatient-rehabilitation-services">
                                <h3 class="sentence">
                                    Quick naps not only improve your alertness, but they also help in decision making,
                                    creativity and sensory perception.
                                </h3>
                                <div class="columns page_margin_top clearfix">
                                    <div class="column_left">
                                        <h3 class="box_header">
                                            Investigations
                                        </h3>
                                        <ul class="items_list page_margin_top clearfix">
                                            <li class="clearfix"><span>Colonoscopy </span>
                                                <div class="value">
                                                    $250
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Gastroscopy </span>
                                                <div class="value">
                                                    $235
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Allergy testing </span>
                                                <div class="value">
                                                    $135
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>CT scan </span>
                                                <div class="value">
                                                    $575
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Cardiac CT scan </span>
                                                <div class="value">
                                                    $275
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Paratryroid scan </span>
                                                <div class="value">
                                                    $195
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Captopril renogram </span>
                                                <div class="value">
                                                    $165
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="column_right">
                                        <h3 class="box_header">
                                            Treatments
                                        </h3>
                                        <ul class="items_list page_margin_top clearfix">
                                            <li class="clearfix"><span>Bronchoscopy </span>
                                                <div class="value">
                                                    $100 - $150
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Cardiac ablation </span>
                                                <div class="value">
                                                    $150 - $200
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Sport injuries </span>
                                                <div class="value">
                                                    $135
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Women's health </span>
                                                <div class="value">
                                                    $135
                                                </div>
                                            </li>
                                            <li class="clearfix"><span>Orthotics </span>
                                                <div class="value">
                                                    $175
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div id="outpatient-rehabilitation-doctors">
                                <ul class="doctors_list">
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="images/samples/480x300/image_03.jpg" alt="" />
                                                    <div class="description">
                                                        <h3>
                                                            Ann Blyumin, Prof.
                                                        </h3>
                                                        <h5>
                                                            Pediatrist
                                                        </h5>
                                                    </div>
                                                    <ul class="controls">
                                                        <li><a href="images/samples/image_03.jpg" rel="outpatient-rehabilitation-doctors"
                                                            class="fancybox open_lightbox"></a></li>
                                                    </ul>
                                                    <ul class="social_icons clearfix">
                                                        <li><a class="social_icon facebook" href="http://facebook.com/QuanticaLabs" title="">
                                                            &nbsp; </a></li>
                                                        <li><a class="social_icon googleplus" href="#" title="">&nbsp; </a></li>
                                                        <li><a class="social_icon mail" href="mailto:medicenter@mail.com" title="">&nbsp; </a>
                                                        </li>
                                                        <li><a class="social_icon forrst" href="#" title="">&nbsp; </a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Ann Blyumin, Prof.
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Speciality
                                                            </label>
                                                            <div class="text">
                                                                Pediatrist
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Degrees
                                                            </label>
                                                            <div class="text">
                                                                M.D. of Medicine
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Training
                                                            </label>
                                                            <div class="text">
                                                                Donec commodo oido. Nulla vitae elit libero, a pharetra augue. Mauris id Integer
                                                                posuere erat ante dapibus posuere velit aliquet.
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Office
                                                            </label>
                                                            <div class="text">
                                                                367, Hall A
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Work days
                                                            </label>
                                                            <div class="text">
                                                                Monday, Thursday, Friday
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <div class="item_footer clearfix">
                                                        <a title="Timetable" href="timetable.html" class="more">Timetable &rarr; </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="images/samples/480x300/image_01.jpg" alt="" />
                                                    <div class="description">
                                                        <h3>
                                                            Robert Brown, Prof.
                                                        </h3>
                                                        <h5>
                                                            General Medicine
                                                        </h5>
                                                    </div>
                                                    <ul class="controls">
                                                        <li><a href="images/samples/image_01.jpg" rel="outpatient-rehabilitation-doctors"
                                                            class="fancybox open_lightbox"></a></li>
                                                    </ul>
                                                    <ul class="social_icons clearfix">
                                                        <li><a class="social_icon facebook" href="http://facebook.com/QuanticaLabs" title="">
                                                            &nbsp; </a></li>
                                                        <li><a class="social_icon googleplus" href="#" title="">&nbsp; </a></li>
                                                        <li><a class="social_icon linkedin" href="#" title="">&nbsp; </a></li>
                                                        <li><a class="social_icon xing" href="#" title="">&nbsp; </a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Robert Brown, Prof.
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Speciality
                                                            </label>
                                                            <div class="text">
                                                                General Medicine
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Degrees
                                                            </label>
                                                            <div class="text">
                                                                M.D. of Medicine
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Training
                                                            </label>
                                                            <div class="text">
                                                                Donec commodo oido. Nulla vitae elit libero, a pharetra augue. Mauris id Integer
                                                                posuere erat ante dapibus posuere velit aliquet.
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Office
                                                            </label>
                                                            <div class="text">
                                                                367, Hall A
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Work days
                                                            </label>
                                                            <div class="text">
                                                                Monday, Thursday, Friday
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <div class="item_footer clearfix">
                                                        <a title="Timetable" href="timetable.html" class="more">Timetable &rarr; </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item clearfix">
                                        <div class="columns no_width">
                                            <div class="column_left">
                                                <div class="gallery_box">
                                                    <img src="images/samples/480x300/image_02.jpg" alt="" />
                                                    <div class="description">
                                                        <h3>
                                                            Clare Mitchell, Prof.
                                                        </h3>
                                                        <h5>
                                                            Family Doctor
                                                        </h5>
                                                    </div>
                                                    <ul class="controls">
                                                        <li><a href="images/samples/image_02.jpg" rel="outpatient-rehabilitation-doctors"
                                                            class="fancybox open_lightbox"></a></li>
                                                    </ul>
                                                    <ul class="social_icons clearfix">
                                                        <li><a class="social_icon twitter" href="https://twitter.com/QuanticaLabs" title="">
                                                            &nbsp; </a></li>
                                                        <li><a class="social_icon mail" href="mailto:medicenter@mail.com" title="">&nbsp; </a>
                                                        </li>
                                                        <li><a class="social_icon myspace" href="#" title="">&nbsp; </a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="column_right">
                                                <div class="details_box">
                                                    <h3 class="box_header">
                                                        Clare Mitchell, Prof.
                                                    </h3>
                                                    <ul class="info_list page_margin_top">
                                                        <li class="clearfix">
                                                            <label>
                                                                Speciality
                                                            </label>
                                                            <div class="text">
                                                                Family Doctor
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Degrees
                                                            </label>
                                                            <div class="text">
                                                                M.D. of Medicine
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Training
                                                            </label>
                                                            <div class="text">
                                                                Donec commodo oido. Nulla vitae elit libero, a pharetra augue. Mauris id Integer
                                                                posuere erat ante dapibus posuere velit aliquet.
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Office
                                                            </label>
                                                            <div class="text">
                                                                367, Hall A
                                                            </div>
                                                        </li>
                                                        <li class="clearfix">
                                                            <label>
                                                                Work days
                                                            </label>
                                                            <div class="text">
                                                                Monday, Thursday, Friday
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <div class="item_footer clearfix">
                                                        <a title="Timetable" href="timetable.html" class="more">Timetable &rarr; </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>--%>
                </ul>
            </div>
            <div class="page_right page_margin_top">
                <ul>
                    <li class="home_box light_blue animated_element animation-fadeIn duration-500" style="z-index: 3;">
                        <h2>
                            <a href="#" title="Especialidades">Especialidades </a>
                        </h2>
                        <div class="news clearfix">
                            <p class="text">
                                Si ya eres medico general con deseos de especializarse en alguna especialidad, aquí
                                puedes encontrar la información necesaria.
                            </p>
                            <a class="more light icon_small_arrow margin_right_white" href="#" title="Leer más">
                                Leer más</a>
                        </div>
                    </li>
                    <li class="home_box blue animated_element animation-slideDown duration-800 delay-250"
                        style="z-index: 2;">
                        <h2>
                            <a href="#" title="Casos de Emergencias">Sub Especialidades </a>
                        </h2>
                        <div class="news clearfix">
                            <p class="text">
                                Conoce todo lo que necesitas saber para hacer una sub especialidad en uno de los
                                centros de residencies del país.
                            </p>
                            <a class="more light icon_small_arrow margin_right_white" href="#" title="Leer más">
                                Leer más</a>
                        </div>
                    </li>
                    <li class="home_box dark_blue animated_element animation-slideDown200 duration-800 delay-500"
                        style="z-index: 1;">
                        <h2>
                            <a href="#" title="Casos de Emergencias">Récord de Notas </a>
                        </h2>
                        <div class="news clearfix">
                            <p class="text">
                                Si ya hiciste tu especialidad en alguno de los centros afiliados a esta herramienta,
                                puedes visualizar tu récord de notas completo.
                            </p>
                            <a class="more light icon_small_arrow margin_right_white" href="/Main.aspx" title="Leer más">
                                Ver Récord de Notas</a>
                        </div>
                    </li>
                    <%--<li class="sidebar_box">
                        <h3 class="box_header">
                            Make An Appointment
                        </h3>
                        <p class="info">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut volutpat rutrum eros
                            sit amet sollicitudin.
                        </p>
                        <ul class="contact_data">
                            <li class="clearfix"><span class="social_icon phone"></span>
                                <p class="value">
                                    by phone: <strong>1-800-643-4300</strong>
                                </p>
                            </li>
                            <li class="clearfix"><span class="social_icon mail"></span>
                                <p class="value">
                                    by e-mail: <a href="mailto:medicenter@mail.com">medicenter@mail.com</a>
                                </p>
                            </li>
                            <li class="clearfix"><span class="social_icon form"></span>
                                <p class="value">
                                    or <a href="contact.html" title="Contact form">fill in the form</a> on our contact
                                    page
                                </p>
                            </li>
                        </ul>
                    </li>--%>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>
