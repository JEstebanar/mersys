﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using General.EMails;

namespace Presentation
{
    public partial class Contact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnContact_Click(object sender, EventArgs e)
        {
            if (txtName.Text != string.Empty && txtPhone.Text != string.Empty)
            {
                GenEMails.Contact(txtName.Text,
                    txtCenter.Text,
                    txtPhone.Text,
                    txtEMail.Text,
                    txtComment.Text);
            }
        }
    }
}