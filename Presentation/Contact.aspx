﻿<%@ Page Title="Contactos" Language="C#" MasterPageFile="~/medicenter.Master" AutoEventWireup="true"
    CodeBehind="Contact.aspx.cs" Inherits="Presentation.Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page relative">
        <div class="page_layout page_margin_top clearfix">
            <div class="page_header clearfix">
                <div class="page_header_left">
                    <h1 class="page_title">
                        Contacto</h1>
                    <ul class="bread_crumb">
                        <li><a href="Home.aspx" title="Inicio">Home </a></li>
                        <li class="separator icon_small_arrow right_gray">&nbsp; </li>
                        <li>Contacto </li>
                    </ul>
                </div>
                <%--<div class="page_header_right">
                    <form class="search">
                    <input class="search_input" type="text" value="To search type and hit enter..." placeholder="To search type and hit enter..." />
                    </form>
                </div>--%>
            </div>
            <div class="clearfix">
                <div class="contact_map page_margin_top html" id="map">
                </div>
                <div class="page_margin_top clearfix">
                    <div class="page_left">
                        <h3 class="box_header slide">
                            Formulario de Contacto
                        </h3>
                        <div class="contact_form" id="contact_form" method="post">
                            <fieldset class="left">
                                <label>
                                    Nombre</label>
                                <div class="block">
                                    <asp:TextBox ID="txtName" runat="server" class="text_input"></asp:TextBox>
                                </div>
                                <label>
                                    Teléfono</label>
                                <div class="block">
                                    <asp:TextBox ID="txtPhone" runat="server" class="text_input"></asp:TextBox>
                                </div>
                            </fieldset>
                            <fieldset class="right">
                                <label>
                                    Centro de Especialidad</label>
                                <div class="block">
                                    <asp:TextBox ID="txtCenter" runat="server" class="text_input"></asp:TextBox>
                                </div>
                                <label>
                                    Correo Electrónico</label>
                                <div class="block">
                                    <asp:TextBox ID="txtEMail" runat="server" class="text_input"></asp:TextBox>
                                </div>
                            </fieldset>
                            <fieldset style="clear: both;">
                                <label>
                                    Comentario</label>
                                <div class="block">
                                    <asp:TextBox ID="txtComment" runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                </div>
                                <asp:Button ID="btnContact" runat="server" class="more blue" Text="Contactar" OnClick="btnContact_Click" />
                            </fieldset>
                        </div>
                    </div>
                    <div class="page_right">
                        <h3 class="box_header slide">
                            MeSys
                        </h3>
                        <ul class="contact_data page_margin_top">
                            <li class="clearfix"><span class="social_icon phone"></span>
                                <p class="value">
                                    Teléfono: <strong>809-613-7615</strong>
                                </p>
                            </li>
                            <li class="clearfix"><span class="social_icon mail"></span>
                                <p class="value">
                                    Correo: <a href="mailto:medicenter@mail.com">hola@residenciasmedicasrd.com</a>
                                </p>
                            </li>
                            <%--<li class="clearfix"><span class="social_icon facebook"></span>
                                <p class="value">
                                    <a href="http://facebook.com/" title="Facebook" target="_blank">facebook.com/</a>
                                </p>
                            </li>
                            <li class="clearfix"><span class="social_icon twitter"></span>
                                <p class="value">
                                    <a href="http://twitter.com/" title="Twitter" target="_blank">twitter.com/</a>
                                </p>
                            </li>
                            <li class="clearfix"><span class="social_icon googleplus"></span>
                                <p class="value">
                                    <a href="http://.com" title="Google +" target="_blank">google.com/medicenter</a>
                                </p>
                            </li>--%>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
