﻿<%@ Page Title="MSP y LMD unen esfuerzos para prevenir Chikungunya" Language="C#" MasterPageFile="~/medicenter.Master" AutoEventWireup="true" CodeBehind="Post.aspx.cs" Inherits="Presentation.Post" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString.Get("id") != null)
                {
                    int id = Convert.ToInt32(Request.QueryString.Get("id"));
                    //DisplayBlog(id);
                }
            }
        }
</script>
<div class="page relative">
        <div class="page_layout page_margin_top clearfix">
            <div class="page_header clearfix">
                <div class="page_header_left">
                    <h1 class="page_title">
                        Artículos</h1>
                    <ul class="bread_crumb">
                        <li><a href="home.aspx" title="Inicio">Inicio </a></li>
                        <li class="separator icon_small_arrow right_gray">&nbsp; </li>
                        <li><a href="#" title="Artículos">Artículos </a></li>
                        <li class="separator icon_small_arrow right_gray">&nbsp; </li>
                        <li>MSP y LMD unen esfuerzos para prevenir Chikungunya </li>
                    </ul>
                </div>
                <%--<div class="page_header_right">
							<form class="search">
								<input class="search_input" type="text" value="To search type and hit enter..." placeholder="To search type and hit enter..." />
							</form>
						</div>--%>
            </div>
            <div class="page_left">
                <ul class="blog clearfix">
                    <li class="post single">
                        <ul class="comment_box">
                            <li class="date clearfix animated_element animation-slideRight">
                                <div class="value">
                                    02 MAY 14</div>
                                <div class="arrow_date">
                                </div>
                            </li>
                            <li class="comments_number animated_element animation-slideUp duration-300 delay-500">
                                <a href="post.aspx#comments_list" title="0 comentarios">0 comentarios </a></li>
                        </ul>
                        <div class="post_content">
                            <div class="gallery_box">
                                <img src="/Files/Posts/images/MSP-y-LMD-unen-esfuerzos-para-prevenir-Chikungunya.JPG"
                                    alt="" />
                            </div>
                            <h2>
                                <a href="post.aspx" title="Lorem ipsum dolor sit amat velum">MSP y LMD unen esfuerzos
                                    para prevenir Chikungunya </a>
                            </h2>
                            <p>
                                El Ministerio de Salud Pública y Asistencia Social y la Liga Municipal Dominicana
                                (LMD) anunciaron esfuerzos conjuntos para prevenir y contener en el país el virus
                                Chikungunya.</p>
                            <p>
                                El Ministro de Salud Pública, doctor Freddy Hidalgo Núñez, y el secretario de la
                                Liga Municipal Dominicana, Ingeniero Johnny Jonnes, informaron la integración coordinada
                                de los ayuntamientos a las jornadas de eliminación de criaderos de mosquitos y la
                                ejecución de las medidas de contención de brotes.</p>
                            <p>
                                Hidalgo Núñez, explicó que los técnicos del Ministerio de Salud presentaron al secretario
                                de la LMD la situación de la enfermedad en el país y la metodología de trabajo para
                                la integración de los ayuntamientos.</p>
                            <p>
                                De su lado, el ingeniero Jonnes anunció para el próximo martes la convocatoria del
                                Comité Ejecutivo de la LMD que la forman 40 alcaldes para dejar iniciados los trabajos
                                junto al Ministerio de Salud.
                            </p>
                            <p>
                                “Haremos participes a todos los cabildos del país a los fines de que se integren
                                a las jornadas de movilización para eliminar criaderos y adoptar las medidas de
                                contención de la transmisión de la Chikungunya”, garantizó, Johnny Jonnes.</p>
                            <%--<a title="Leave a reply" href="#comment_form" class="more reply_button">
										Leave a reply &rarr;
									</a>--%>
                            <div class="post_footer clearfix">
                                <%--<ul class="post_footer_details">
											<li>Posted in </li>
											<li>
												<a href="#" title="General">
													General,
												</a>
											</li>
											<li>
												<a href="#" title="Dental clinic">
													Dental clinic
												</a>
											</li>
										</ul>--%>
                                <ul class="post_footer_details">
                                    <li>Publicado por </li>
                                    <li><a href="#" title="John Doe">Prensa | Ministerio de Salud Pública </a></li>
                                </ul>
                                <%--<ul class="post_footer_details">
											<li>Post type </li>
											<li>
												<a href="#" title="Image">
													Image
												</a>
											</li>
										</ul>--%>
                            </div>
                        </div>
                    </li>
                </ul>
                <%--<div class="comments clearfix page_margin_top">
							<div class="comment_box">
								<div class="comments_number animated_element animation-slideRight">
									<a href="post.html#comments_list" title="2 comments">
										2 comments
									</a>
									<div class="arrow_comments"></div>
								</div>
							</div>
							<div id="comments_list">
								<ul>
									<li class="comment clearfix">
										<div class="comment_author_avatar">
											&nbsp;
										</div>
										<div class="comment_details">
											<div class="posted_by">
												Posted by <a class="author" href="#" title="Jonh Doe">John Doe</a> on 16 Feb 2012, 2.24 pm
											</div>
											<p>
												Donec ipsum diam, pretium mollis dapibus risus. Nullam tindun pulvinar at interdum eget, suscipit eget felis. Pellentesque est faucibus tincidunt risus id interdum primis orci cubilla.
											</p>
											<a class="more reply_button" href="#comment_form">
												Reply &rarr;
											</a>
										</div>
									</li>
									<li class="comment clearfix">
										<div class="comment_author_avatar">
											&nbsp;
										</div>
										<div class="comment_details">
											<div class="posted_by">
												Posted by <a class="author" href="#" title="Jonh Doe">John Doe</a> on 16 Feb 2012, 2.24 pm
											</div>
											<p>
												Donec ipsum diam, pretium mollis dapibus risus. Nullam tindun pulvinar at interdum eget, suscipit eget felis. Pellentesque est faucibus tincidunt risus id interdum primis orci cubilla.
											</p>
											<a class="more reply_button" href="#comment_form">
												Reply &rarr;
											</a>
										</div>
										<ul class="children">
											<li class="comment clearfix">
												<div class="comment_author_avatar">
													&nbsp;
												</div>
												<div class="comment_details">
													<div class="posted_by">
														Posted by <a class="author" href="#" title="Jonh Doe">John Doe</a> on 16 Feb 2012, 2.24 pm
													</div>
													<p>
														Donec ipsum diam, pretium mollis dapibus risus. Nullam tindun pulvinar at interdum eget, suscipit eget felis. Pellentesque est faucibus tincidunt risus.
													</p>
													<a class="more reply_button" href="#comment_form">
														Reply &rarr;
													</a>
												</div>
											</li>
											<li class="comment clearfix">
												<div class="comment_author_avatar">
													&nbsp;
												</div>
												<div class="comment_details">
													<div class="posted_by">
														Posted by <a class="author" href="#" title="Jonh Doe">John Doe</a> on 16 Feb 2012, 2.24 pm
													</div>
													<p>
														Donec ipsum diam, pretium mollis dapibus risus. Nullam tindun pulvinar at interdum eget, suscipit eget felis. Pellentesque est faucibus tincidunt risus.
													</p>
													<a class="more reply_button" href="#comment_form">
														Reply &rarr;
													</a>
												</div>
											</li>
										</ul>
									</li>
								</ul>
								<ul class="pagination">
									<li class="selected">
										<a href="#" title="">
											1
										</a>
									</li>
									<li>
										<a href="#" title="">
											2
										</a>
									</li>
									<li>
										<a href="#" title="">
											3
										</a>
									</li>
								</ul>
							</div>
							<div class="comment_form_container">
								<h3 class="box_header">
									Leave a reply
								</h3>
								<form class="comment_form" id="comment_form" method="post" action="post.html">
									<fieldset class="left">
										<label class="first">Your Name</label>
										<input class="text_input" name="name" type="text" value="" />
										<label>Your Email</label>
										<input class="text_input" name="email" type="text" value="" />
									</fieldset>
									<fieldset class="right">
										<label class="first">Message</label>
										<textarea name="message"></textarea>
										<input type="submit" value="Send" class="more blue" />
										<a href="#cancel" id="cancel_comment" title="Cancel reply">Cancel reply</a>
									</fieldset>
								</form>
							</div>
						</div>--%>
            </div>
            <div class="page_right">
                <%--<h3 class="box_header margin">
							Categorías
						</h3>
						<div class="sidebar_box first">
							<ul class="categories clearfix page_margin_top">
								<li>
									<a href="#" title="General">
										General
									</a>
								</li>
								<li>
									<a href="#" title="News">
										News
									</a>
								</li>
								<li>
									<a href="#" title="Primary health">
										Primary health
									</a>
								</li>
								<li>
									<a href="#" title="Pediatric clinic">
										Pediatric clinic
									</a>
								</li>
								<li>
									<a href="#" title="Outpatient surgery">
										Outpatient surgery
									</a>
								</li>
								<li>
									<a href="#" title="Cardiac clinic">
										Cardiac clinic
									</a>
								</li>
								<li>
									<a href="#" title="Laryngological clinic">
										Laryngological clinic
									</a>
								</li>
								<li>
									<a href="#" title="Health">
										Health
									</a>
								</li>
								<li>
									<a href="#" title="Dental clinic">
										Dental clinic
									</a>
								</li>
								<li>
									<a href="#" title="Ophthalmology clinic">
										Ophthalmology clinic
									</a>
								</li>
							</ul>
						</div>
						<div class="sidebar_box">
							<div class="clearfix">
								<div class="header_left">
									<h3 class="box_header">
										Most Commented
									</h3>
								</div>
								<div class="header_right">
									<a href="#" id="most_commented_prev" class="scrolling_list_control_left icon_small_arrow left_black"></a>
									<a href="#" id="most_commented_next" class="scrolling_list_control_right icon_small_arrow right_black"></a>
								</div>
							</div>
							<div class="scrolling_list_wrapper">
								<ul class="scrolling_list most_commented">
									<li class="icon_small_arrow right_black">
										<a href="post.html" class="clearfix">
											<span class="left">
												Mauris adipiscing mauris fringilla turpis interdum sed pulvinar nisi malesuada.
											</span>
											<span class="number">
												18
											</span>
										</a>
										<abbr title="29 May 2012" class="timeago">29 May 2012</abbr>
									</li>
									<li class="icon_small_arrow right_black">
										<a href="post.html" class="clearfix">
											<span class="left">
												Lorem ipsum dolor sit amat velum.
											</span>
											<span class="number">
												16
											</span>
										</a>
										<abbr title="04 Apr 2012" class="timeago">04 Apr 2012</abbr>
									</li>
									<li class="icon_small_arrow right_black">
										<a href="post.html" class="clearfix">
											<span class="left">
												Mauris adipiscing mauris fringilla turpis interdum sed pulvinar nisi malesuada.
											</span>
											<span class="number">
												9
											</span>
										</a>
										<abbr title="02 Feb 2012" class="timeago">02 Feb 2012</abbr>
									</li>
									<li class="icon_small_arrow right_black">
										<a href="post.html" class="clearfix">
											<span class="left">
												Lorem ipsum dolor sit amat velum, consectetur adipiscing elit.
											</span>
											<span class="number">
												7
											</span>
										</a>
										<abbr title="24 Jan 2011" class="timeago">24 Jan 2011</abbr>
									</li>
								</ul>
							</div>
						</div>
						<div class="sidebar_box">
							<div class="clearfix">
								<div class="header_left">
									<h3 class="box_header">
										Most Viewed
									</h3>
								</div>
								<div class="header_right">
									<a href="#" id="most_viewed_prev" class="scrolling_list_control_left icon_small_arrow left_black"></a>
									<a href="#" id="most_viewed_next" class="scrolling_list_control_right icon_small_arrow right_black"></a>
								</div>
							</div>
							<div class="scrolling_list_wrapper">
								<ul class="scrolling_list most_viewed">
									<li class="icon_small_arrow right_black">
										<a href="post.html" class="clearfix">
											<span class="left">
												Mauris adipiscing mauris fringilla turpis interdum sed pulvinar nisi malesuada.
											</span>
											<span class="number">
												423
											</span>
										</a>
										<abbr title="29 May 2012" class="timeago">29 May 2012</abbr>
									</li>
									<li class="icon_small_arrow right_black">
										<a href="post.html" class="clearfix">
											<span class="left">
												Lorem ipsum dolor sit amat velum.
											</span>
											<span class="number">
												231
											</span>
										</a>
										<abbr title="04 Apr 2012" class="timeago">04 Apr 2012</abbr>
									</li>
									<li class="icon_small_arrow right_black">
										<a href="post.html" class="clearfix">
											<span class="left">
												Mauris adipiscing mauris fringilla turpis interdum sed pulvinar nisi malesuada.
											</span>
											<span class="number">
												184
											</span>
										</a>
										<abbr title="02 Feb 2012" class="timeago">02 Feb 2012</abbr>
									</li>
									<li class="icon_small_arrow right_black">
										<a href="post.html" class="clearfix">
											<span class="left">
												Lorem ipsum dolor sit amat velum, consectetur adipiscing elit.
											</span>
											<span class="number">
												97
											</span>
										</a>
										<abbr title="24 Jan 2011" class="timeago">24 Jan 2011</abbr>
									</li>
								</ul>
							</div>
						</div>
						<div class="sidebar_box">
							<h3 class="box_header">
								Photostream
							</h3>
							<ul class="photostream clearfix">
								<li class="gallery_box">
									<img src="images/samples/75x75/image_01.jpg" alt=""/>
									<ul class="controls">
										<li>
											<a href="images/samples/image_01.jpg" rel="photostream" class="fancybox open_lightbox"></a>
										</li>
									</ul>
								</li>
								<li class="gallery_box">
									<img src="images/samples/75x75/image_02.jpg" alt=""/>
									<ul class="controls">
										<li>
											<a href="images/samples/image_02.jpg" rel="photostream" class="fancybox open_lightbox"></a>
										</li>
									</ul>
								</li>
								<li class="gallery_box">
									<img src="images/samples/75x75/image_03.jpg" alt=""/>
									<ul class="controls">
										<li>
											<a href="images/samples/image_03.jpg" rel="photostream" class="fancybox open_lightbox"></a>
										</li>
									</ul>
								</li>
								<li class="gallery_box">
									<img src="images/samples/75x75/image_04.jpg" alt=""/>
									<ul class="controls">
										<li>
											<a href="images/samples/image_04.jpg" rel="photostream" class="fancybox open_lightbox"></a>
										</li>
									</ul>
								</li>
								<li class="gallery_box">
									<img src="images/samples/75x75/image_05.jpg" alt=""/>
									<ul class="controls">
										<li>
											<a href="images/samples/image_05.jpg" rel="photostream" class="fancybox open_lightbox"></a>
										</li>
									</ul>
								</li>
								<li class="gallery_box">
									<img src="images/samples/75x75/image_06.jpg" alt=""/>
									<ul class="controls">
										<li>
											<a href="images/samples/image_06.jpg" rel="photostream" class="fancybox open_lightbox"></a>
										</li>
									</ul>
								</li>
								<li class="gallery_box">
									<img src="images/samples/75x75/image_07.jpg" alt=""/>
									<ul class="controls">
										<li>
											<a href="images/samples/image_07.jpg" rel="photostream" class="fancybox open_lightbox"></a>
										</li>
									</ul>
								</li>
								<li class="gallery_box">
									<img src="images/samples/75x75/image_08.jpg" alt=""/>
									<ul class="controls">
										<li>
											<a href="images/samples/image_08.jpg" rel="photostream" class="fancybox open_lightbox"></a>
										</li>
									</ul>
								</li>
							</ul>
						</div>--%>
                <div class="sidebar_box">
                    <h3 class="box_header">
                        Archivos
                    </h3>
                    <ul class="columns list clearfix">
                        <li class="column_left icon_small_arrow right_black"><a href="#" title="Diciembre 2013">
                            Diciembre 2013 </a></li>
                        <li class="column_right icon_small_arrow right_black"><a href="#" title="Enero 2014">
                            Enero 2014 </a></li>
                        <li class="column_left icon_small_arrow right_black"><a href="#" title="Febrero 2012">
                            Febrero 2014 </a></li>
                        <li class="column_right icon_small_arrow right_black"><a href="#" title="Marzo 2014">
                            Marzo 2014 </a></li>
                        <li class="column_left icon_small_arrow right_black"><a href="#" title="Abril 2014">
                            Abril 2014 </a></li>
                        <li class="column_right icon_small_arrow right_black"><a href="#" title="Mayo 2014">
                            Mayo 2014 </a></li>
                    </ul>
                </div>
                <div class="sidebar_box">
                    <h3 class="box_header">
                        Reglamentos
                    </h3>
                    <p>
                        Los programas de Residencias Médicas, dirigidos al personal médico, tienen su punto
                        de inicio en el 1968 desde el interior de los hospitales públicos, dependen...
                    </p>
                    <a title="Leer más" href="/Files/Reglamentos-Consejo-Nac-Residencia-Med.pdf" target="_blank">
                        Leer más → </a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
