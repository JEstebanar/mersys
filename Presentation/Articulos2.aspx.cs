﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using General.GeneralCommons;
using Posts.Posts;

namespace Presentation
{
    public partial class Articulos2 : System.Web.UI.Page
    {
        private IList<PosPosts.ResidentsPosts> PostsList { get { return (IList<PosPosts.ResidentsPosts>)ViewState["postsList"]; } set { ViewState["postsList"] = value; } }
        private IList<PosPosts.ResidentsPosts> PostsLeft { get { return (IList<PosPosts.ResidentsPosts>)ViewState["postsLeft"]; } set { ViewState["postsLeft"] = value; } }
        private IList<PosPosts.ResidentsPosts> PostsRight { get { return (IList<PosPosts.ResidentsPosts>)ViewState["postsRight"]; } set { ViewState["postsRight"] = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindrepeater(1);
                txtHidden.Value = "1";
            }
        }

        protected void lnkBtnPrev_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(txtHidden.Value) <= 2)
                return;

            txtHidden.Value = Convert.ToString(Convert.ToInt32(txtHidden.Value) - 2);
            bindrepeater(Convert.ToInt32(txtHidden.Value));
        }

        protected void lnkBtnNext_Click(object sender, EventArgs e)
        {
            txtHidden.Value = Convert.ToString(Convert.ToInt32(txtHidden.Value) + 2);
            bindrepeater(Convert.ToInt32(txtHidden.Value));
        }

        public void bindrepeater(int newPage)
        {
            if (newPage == 0)
                txtHidden.Value = Convert.ToString(Convert.ToInt32(txtHidden.Value) + 1);

            //PostsList = PosPosts.GetByPublic(newPage, (int)GeneralCommon.PageSize.Articles);

            //int postNum=0;
            //foreach (PosPosts.ResidentsPosts postsInList in PostsList)
            //{
            //    if(postNum%2==0)

            //}
            PostsLeft = PosPosts.GetByPublic(newPage, (int)GeneralCommon.PageSize.Articles);
            PostsRight = PosPosts.GetByPublic(newPage + 1, (int)GeneralCommon.PageSize.Articles);

            rptPostLeft.DataSource = PostsLeft;
            rptPostLeft.DataBind();

            rptPostRight.DataSource = PostsRight;
            rptPostRight.DataBind();
            //pageno(1);
        }

        public void pageno(int totItems)
        {
            // Calculate total numbers of pages
            int pgCount = totItems / (int)GeneralCommon.PageSize.Articles + totItems % (int)GeneralCommon.PageSize.Articles;

            // Display Next>> button
            if (pgCount - 1 > Convert.ToInt16(txtHidden.Value))
                lnkBtnNext.Visible = true;
            else
                lnkBtnNext.Visible = false;

            // Display <<Prev button
            if ((Convert.ToInt16(txtHidden.Value)) > 1)
                lnkBtnPrev.Visible = true;
            else
                lnkBtnPrev.Visible = false;
        }
    }
}