﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Posts.Posts;
using General.GeneralCommons;

namespace Presentation
{
    public partial class Articulos : System.Web.UI.Page
    {
        private IList<PosPosts.ResidentsPosts> PostList { get { return (IList<PosPosts.ResidentsPosts>)ViewState["postList"]; } set { ViewState["postList"] = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindrepeater(1);
                txtHidden.Value = "1";
            }
        }

        protected void lnkBtnPrev_Click(object sender, EventArgs e)
        {
            txtHidden.Value = Convert.ToString(Convert.ToInt32(txtHidden.Value) - 1);
            bindrepeater(Convert.ToInt32(txtHidden.Value));
        }

        protected void lnkBtnNext_Click(object sender, EventArgs e)
        {
            txtHidden.Value = Convert.ToString(Convert.ToInt32(txtHidden.Value) + 1);
            bindrepeater(Convert.ToInt32(txtHidden.Value));
        }

        public void bindrepeater(int newPage)
        {
            if (newPage==0)
                txtHidden.Value = Convert.ToString(Convert.ToInt32(txtHidden.Value) + 1);

            PostList = PosPosts.GetByPublic(newPage, (int)GeneralCommon.PageSize.Articles);
            rptPosts.DataSource = PostList;
            rptPosts.DataBind();
            //pageno(1);
        }

        public void pageno(int totItems)
        {
            // Calculate total numbers of pages
            int pgCount = totItems / (int)GeneralCommon.PageSize.Articles + totItems % (int)GeneralCommon.PageSize.Articles;

            // Display Next>> button
            if (pgCount - 1 > Convert.ToInt16(txtHidden.Value))
                lnkBtnNext.Visible = true;
            else
                lnkBtnNext.Visible = false;

            // Display <<Prev button
            if ((Convert.ToInt16(txtHidden.Value)) > 1)
                lnkBtnPrev.Visible = true;
            else
                lnkBtnPrev.Visible = false;
        }
    }
}