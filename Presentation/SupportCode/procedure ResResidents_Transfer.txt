alter procedure ResResidents_Transfer (
	@residentId int
	,@userId int 
) as
declare @generalId as int, @newGeneralId as int, @personId as int, @newPersonId as int, @generalId2 as int, @newGeneralId2 as int, @personId2 as int, @newPersonId2 as int, @emergencyContactId int, @newEmergencyContactId as int, @contactId as int, @newContactId as int
	,@newResidentId as int, @centerId as int, @newCenterId as int

set @newCenterId = (select ToCenterId from res_Transfers where ResidentId = @residentId and Transferred is null)

select @generalId = res_Residents.GeneralId, @personId = PersonId, @centerId = CentreId 
from gen_Persons
	left join gen_Generals on gen_Persons.GeneralId = gen_generals.GeneralId
	left join res_Residents on gen_generals.GeneralId = res_Residents.GeneralId
	where ResidentId = @residentId
	select @generalId, @personId, @centerId
	
--create new general record
insert into gen_Generals 
SELECT [NationalityId]
      ,[PhotoPath]
  FROM [gen_Generals]
  where	GeneralId = @generalId
set @newGeneralId = (select SCOPE_IDENTITY())

--create new person record
insert into gen_Persons 
SELECT @newGeneralId
      ,[Name]
      ,[MiddleName]
      ,[FirstLastName]
      ,[SecondLastName]
      --,[FullName]
      ,[GenderId]
      ,[MaritalStatusId]
      ,[Birthday]
      ,null
      ,null
  FROM [gen_Persons]
  where GeneralId = @generalId
  set @newPersonId = (select SCOPE_IDENTITY())

--create new entity record
insert into gen_Entities 
SELECT @newGeneralId
      ,[EntityTypeId]
      ,@newCenterId
      ,'true'
      ,@userId
      ,(GETDATE())
      ,null
      ,null
  FROM [gen_Entities]
  where GeneralId = @generalId and CenterId = @centerId and EntityTypeId = 1 --Resident

--create new general record
--========================================================
declare cIdentityDocuments cursor for
select IdentityId from gen_GeneralIdentityDocuments where GeneralId = @generalId

declare @cIdentityId int
open cIdentityDocuments

fetch next from cIdentityDocuments 
into @cIdentityId

while @@fetch_status = 0
begin

insert into gen_GeneralIdentityDocuments 
SELECT @newGeneralId
      ,[IdentityNumber]
      ,[IdentityTypeId]
      ,[ExpirationDate]
      ,[IdentityPath]
      ,[CreatedBY]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
  FROM [gen_GeneralIdentityDocuments]
  where IdentityId = @cIdentityId
    
fetch next from cIdentityDocuments
into @cIdentityId

end 
close cIdentityDocuments 
deallocate cIdentityDocuments 
--========================================================

--create new general phones record
--========================================================
declare cGeneralPhones cursor for
select GeneralPhoneId from gen_GeneralPhones where GeneralId = @generalId

declare @cGeneralPhoneId int
open cGeneralPhones

fetch next from cGeneralPhones 
into @cGeneralPhoneId

while @@fetch_status = 0
begin

insert into gen_GeneralPhones 
SELECT @newGeneralId
      ,[PhoneNumber]
      ,[PhoneTypeId]
      ,[PhoneStatus]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
  FROM [gen_GeneralPhones]
  where GeneralPhoneId = @cGeneralPhoneId

fetch next from cGeneralPhones
into @cGeneralPhoneId

end 
close cGeneralPhones
deallocate cGeneralPhones 
--========================================================

--create new address record
--========================================================
declare cAddresses cursor for
select AddressId from gen_GeneralAddresses where GeneralId = @generalId

declare @cAddressId int
open cAddresses

fetch next from cAddresses 
into @cAddressId

while @@fetch_status = 0
begin

insert into gen_GeneralAddresses 
SELECT @newGeneralId
      ,[Address1]
      ,[Address2]
      ,[CityId]
      ,[AddressStatus]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
  FROM [gen_GeneralAddresses]
  where Address1 = @cAddressId

fetch next from cAddresses
into @cAddressId

end 
close cAddresses
deallocate cAddresses 
--========================================================

--create new resident documents record
--========================================================
declare cResidentDocuments cursor for
select ResidentDocumentId from res_ResidentDocuments where GeneralId = @generalId
declare @cResidentDocumentId int

open cResidentDocuments

fetch next from cResidentDocuments 
into @cResidentDocumentId

while @@fetch_status = 0
begin

insert into res_ResidentDocuments 
SELECT @newResidentId
      ,[DocumentNumber]
      ,[DocumentTypeId]
      ,[ExpirationDate]
      ,[DocumentPath]
      ,[CreatedBY]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
  FROM [res_ResidentDocuments]
  where ResidentDocumentId = @cResidentDocumentId

fetch next from cResidentDocuments
into @cResidentDocumentId

end 
close cResidentDocuments
deallocate cResidentDocuments 
--========================================================  

--for emergency contact
select @generalId2 = GeneralId, @contactId = ContactId, @emergencyContactId = EmergencyContactId
 from vw_EmergencyContacts where PersonId = @personId
 
--create new emergency contact record
insert into gen_Generals 
SELECT [NationalityId]
      ,[PhotoPath]
  FROM [gen_Generals]
  where	GeneralId = @generalId2
set @newGeneralId2 = (select SCOPE_IDENTITY())

insert into gen_Persons 
SELECT @newGeneralId2
      ,[Name]
      ,[MiddleName]
      ,[FirstLastName]
      ,[SecondLastName]
      --,[FullName]
      ,[GenderId]
      ,[MaritalStatusId]
      ,[Birthday]
      ,null
      ,null
  FROM [gen_Persons]
  where PersonId = @contactId
  set @newContactId = (select SCOPE_IDENTITY())

--create new person emergency contact record
insert into gen_PersonEmergencyContacts 
SELECT @newPersonId
      ,@newContactId
      ,[RelationshipId]
      ,[Allergies]
      ,[Drugs]
      ,[CreatedBy]
      ,[CreatedDate]
      ,null
      ,null
  FROM [gen_PersonEmergencyContacts]
  where EmergencyContactId = @emergencyContactId

insert into gen_Entities 
SELECT @newGeneralId2
      ,[EntityTypeId]
      ,@newCenterId
      ,'true'
      ,@userId
      ,(GETDATE())
      ,null
      ,null
  FROM [gen_Entities]
  where GeneralId = @generalId2  
  
-- create new resident record
--==========================================================================
declare cResidents cursor for
select ResidentId from res_Residents where GeneralId = @generalId

declare @cResidentId int
open cResidents

fetch next from cResidents 
into @cResidentId

while @@fetch_status = 0
begin

insert into res_Residents 
SELECT @newGeneralId
      ,[ResidenceId]
      ,[SubResidence]
      ,[GradeId]
      ,[UniversityId]
      ,[TeachingYearId]
      ,case when [EndDate] IS NULL then (GETDATE()) else [StartDate] end	--actual year start now
      ,[EndDate]
      ,1 --Active
      ,@newCenterId
      ,case when [EndDate] IS NULL then @userId else [CreatedBY] end
      ,case when [EndDate] IS NULL then (GETDATE()) else [CreatedDate] end
      ,null
      ,null
  FROM [res_Residents]
  where ResidentId = @cResidentId  
  set @newResidentId = (select SCOPE_IDENTITY())	

--create new residents transfer record
insert into res_Transfers 
SELECT @newResidentId
      ,[ToCenterId]
      ,[TransferDocument]
      ,'true'
      ,[CreatedBy]
      ,[CreatedDate]
  FROM [res_Transfers]
  where ResidentId = @cResidentId
  
--create new resident status comments record
insert into res_ResidentStatusComments 
SELECT @newResidentId
      ,[StatusId]
      ,[Comment]
      ,[CreatedBy]
      ,[CreatedDate]
  FROM [res_ResidentStatusComments]
  where ResidentId = @cResidentId

--create new resident rotation record
insert into res_Rotations 
SELECT @newResidentId
      ,[DepartmentId]
      ,[StartDate]
      ,[EndDate]
      ,[RotationTypeId]
      ,[CenterId]
      ,[Note]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
  FROM [res_Rotations]
  where ResidentId = @cResidentId

--create new evaluation score
insert into eva_DetailScores 
SELECT [DetailId]
      ,@newResidentId
      ,[July]
      ,[August]
      ,[September]
      ,[October]
      --,[Quarter1]
      ,[November]
      ,[December]
      ,[January]
      ,[February]
      --,[Quarter2]
      ,[March]
      ,[April]
      ,[May]
      ,[June]
      --,[Quarter3]
      --,[Average]
      ,[LastActionBy]
  FROM [eva_DetailScores]  
  where ResidentId = @cResidentId
  
	fetch next from cResidents
	into @cResidentId

end 
close cResidents
deallocate cResidents

--===============================================================

--security system
declare @residentUserId as int, @userCenterId as int, @newUserCenterId as int, @userRoleId as int

select @residentUserId = sys_Users.UserId, @userCenterId = sys_UserCenters.UserCenterId, @userRoleId = UserRoleId 
from sys_Users 
left join sys_UserCenters on sys_Users.UserId = sys_UserCenters.UserId
left join sys_UserRoles on sys_UserCenters.UserCenterId = sys_UserRoles.UserCenterId
where GeneralId = @generalId
	and RoleId = 5	--Resident
	and CenterId = @centerId
--select @residentUserId, @userCenterId, @userRoleId

insert into sys_UserCenters values (@residentUserId, @newCenterId, 'true')
set @newUserCenterId = (select SCOPE_IDENTITY())
update sys_UserCenters set UserCenterStatus = 'false' where UserCenterId = @userCenterId
  
insert into sys_UserRoles values (@newUserCenterId, 5, 'true') --5 = Resident
update sys_UserRoles set UserRoleStatus = 'false' where UserRoleId = @userRoleId

update res_Transfers set Transferred = 'true' 
	where ResidentId = @residentId 
		and Transferred is null
		
----complete transfer
--exec ResResidents_SetTransfer @residentId, @newResidentId, 'true'
go