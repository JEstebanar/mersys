﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Text.RegularExpressions;

namespace Presentation
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            string origionalpath = Request.Url.ToString();
            string subPath = string.Empty;
            string articleId = string.Empty;
            int id = 0;
            if (origionalpath.Contains("/Articulos/"))
            {
                if (origionalpath.Length >= 31)
                {
                    subPath = origionalpath.Substring(22);
                    if (subPath.Length >= 1)
                    {
                        articleId = Regex.Match(subPath, @"\d+").Value;
                        bool isValid = Int32.TryParse(articleId, out id);
                        if (isValid)
                        {
                            Context.RewritePath("../../Post.aspx?id=" + id);
                        }
                    }
                }
            }

            if (origionalpath.Contains("/Modules/Posts/"))
            {
                if (origionalpath.Length >= 31)
                {
                    subPath = origionalpath.Substring(22);
                    if (subPath.Length >= 1)
                    {
                        articleId = Regex.Match(subPath, @"\d+").Value;
                        bool isValid = Int32.TryParse(articleId, out id);
                        if (isValid)
                        {
                            Context.RewritePath("../Post.aspx?id=" + id);
                        }
                    }
                }
            }
        }
    }
}
