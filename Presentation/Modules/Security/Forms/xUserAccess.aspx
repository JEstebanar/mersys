﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/Security/Security.master"
    AutoEventWireup="true" CodeBehind="xUserAccess.aspx.cs" Inherits="Presentation.Modules.Security.Forms.UserAccess" %>

<%@ MasterType VirtualPath="~/Modules/Security/Security.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Accesos a Residencias</legend>
        <div class="accordion" id="Accordion">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                        Usuarios </a>
                </div>
                <div id="collapseOne" class="accordion-body collapse ">
                    <div class="accordion-inner">
                        <asp:UpdatePanel ID="upanelSearch" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <div class="control-group">
                                    <div class="form-search">
                                        <asp:TextBox ID="txtSearchName" runat="server" CssClass="textEntry" placeholder="Nombre"
                                            OnTextChanged="txtSearchName_TextChanged"></asp:TextBox>
                                        <asp:Button ID="btnSearch" runat="server" Text="Buscar" class="btn" OnClick="btnSearch_Click" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <asp:GridView ID="grvPersons" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                                        AutoGenerateColumns="False" DataKeyNames="userId,generalId,personId,fullName"
                                        ShowHeader="False" OnSelectedIndexChanged="grvPersons_SelectedIndexChanged" AllowPaging="True"
                                        OnPageIndexChanging="grvPersons_PageIndexChanging" PageSize="7">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="personSelect" runat="server" CommandArgument='<%# Eval("userId") %>'
                                                        CommandName="Select" ToolTip="Seleccionar" Text='<%# Eval("fullName") %>'>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="txtSearchName" EventName="TextChanged" />
                                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-horizontal">
            <div class="control-group">
                <label class="control-label">
                    01 Jul 2013 - 30 Jun 2014</label>
                <div class="controls">
                        <asp:DropDownList id="ddlResidences3" runat="server"></asp:DropDownList>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">
                    01 Jul 2014 - 30 Jun 2015</label>
                <div class="controls">
                        <asp:DropDownList id="ddlResidences4" runat="server"></asp:DropDownList>
                </div>
            </div>
        </div>
    </fieldset>
</asp:Content>
