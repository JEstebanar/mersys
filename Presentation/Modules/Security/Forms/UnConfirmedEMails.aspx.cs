﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI.WebControls;
using General.EMails;
using General.Forms;
using General.GeneralCommons;
using Residences.Residents;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.SysUsers;
using SystemSecurity.UserAccess;
using SystemSecurity.Utilities;

namespace Presentation.Modules.Security.Forms
{
    public partial class UnConfirmedEMails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.UnConfirmedEMails))
                    {
                        if (!IsPostBack)
                        {
                            FillResidents();
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillResidents()
        {
            grvResidentEmails.DataSource = ResResidents.GetUnConfirmedEMails((int)((SysUserAccess)Session["access"]).CenterId, (int)((SysUserAccess)Session["access"]).TeachingYearId);
            grvResidentEmails.DataBind();

            lblCount.Text = grvResidentEmails.Rows.Count.ToString();
        }

        protected void chkResidentHeader_CheckedChanged(object sender, EventArgs e)
        {
            GenForms.CheckItemsInGridView(ref grvResidentEmails, "chkResidentHeader", "chkEMail");
        }

        protected void chkEMail_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Master.Master.Message("Este proceso puede tardar, por favor espere la confirmación. <br / > Reenviando confirmaciones ...", "", 4);
            foreach (GridViewRow row in grvResidentEmails.Rows)
            {
                if ((row.FindControl("chkEMail") as CheckBox).Checked)
                {
                    string confirmationCode = SysUtilities.CreateConfirmationCode(); //Guid.NewGuid().ToString().Replace("-", "");
                    string userName = grvResidentEmails.DataKeys[row.RowIndex].Values["userName"].ToString();
                    string fullName = grvResidentEmails.DataKeys[row.RowIndex].Values["fullName"].ToString();

                    SysUsers cUser = SysUsers.Get((int)grvResidentEmails.DataKeys[row.RowIndex].Values["userId"]);
                    Random random = new Random();
                    int randomNumber = random.Next(0, 99999) + 100000;
                    cUser.UserPassword = (randomNumber - 50000).ToString();
                    cUser.ConfirmationCode = confirmationCode;
                    cUser.UserStatus = false;
                    cUser.Update();

                    GenEMails.ReSendConfirmation(userName, fullName, confirmationCode, (int)((SysUserAccess)Session["access"]).CenterId);
                }
            }
            FillResidents();
            Master.Master.Message("¡Confirmaciones enviadas con éxito!", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            GenForms.UnCheckItemsInGridView(ref grvResidentEmails, "chkResidentHeader", "chkEMail");
        }
    }
}