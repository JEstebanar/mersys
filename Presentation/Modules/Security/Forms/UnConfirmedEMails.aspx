﻿<%@ Page Title="Correos Electrónicos sin Confirmar" Language="C#" MasterPageFile="~/Modules/Security/Security.master"
    AutoEventWireup="true" CodeBehind="UnConfirmedEMails.aspx.cs" Inherits="Presentation.Modules.Security.Forms.UnConfirmedEMails" %>
    
<%@ MasterType VirtualPath="~/Modules/Security/Security.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Correos Electrónicos sin Confirmar (Residentes)</legend>
        <asp:Label ID="lblCount" runat="server"></asp:Label>
        <asp:GridView ID="grvResidentEmails" runat="server" class="table table-condensed table-striped table-bordered"
            AutoGenerateColumns="False" DataKeyNames="userId,userName,fullName">
            <Columns>
                <asp:BoundField DataField="fullName" HeaderText="Residente" SortExpression="fullName" />
                <asp:BoundField DataField="userName" HeaderText="Correo" SortExpression="userName" />
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkResidentHeader" runat="server"
                            AutoPostBack="true" oncheckedchanged="chkResidentHeader_CheckedChanged" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkEMail" runat="server" 
                            oncheckedchanged="chkEMail_CheckedChanged" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <div class="form-actions">
            <asp:Button ID="btnSave" runat="server" Text="Reenviar Confirmación" 
                class="btn btn-success" onclick="btnSave_Click" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" 
                onclick="btnCancel_Click" />
        </div>
    </fieldset>
</asp:Content>
