﻿<%@ Page Title="Usuario" Language="C#" MasterPageFile="~/Modules/Security/Security.master"
    AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="Presentation.Modules.Security.Forms.User" %>

<%@ MasterType VirtualPath="~/Modules/Security/Security.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="accordion" id="Accordion">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                    Usuarios </a>
            </div>
            <div id="collapseOne" class="accordion-body collapse ">
                <div class="accordion-inner">
                <asp:UpdatePanel ID="upanelSearch" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                    <div class="control-group">
                        <div class="form-search">
                            <asp:TextBox ID="txtSearchName" runat="server" CssClass="textEntry" 
                                placeholder="Nombre" ontextchanged="txtSearchName_TextChanged"></asp:TextBox>
                            <asp:Button ID="btnSearch" runat="server" Text="Buscar" class="btn" 
                                onclick="btnSearch_Click" />
                        </div>
                    </div>
                    <div class="control-group">
                        <asp:GridView ID="grvPersons" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                            AutoGenerateColumns="False" DataKeyNames="userId,generalId,personId,fullName"
                            ShowHeader="False" 
                            OnSelectedIndexChanged="grvPersons_SelectedIndexChanged" AllowPaging="True" 
                            onpageindexchanging="grvPersons_PageIndexChanging" PageSize="7">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="personSelect" runat="server" CommandArgument='<%# Eval("userId") %>'
                                            CommandName="Select" ToolTip="Seleccionar" Text='<%# Eval("fullName") %>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="txtSearchName" EventName="TextChanged" />
                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#UserInfo" data-toggle="tab">Usuario</a></li>
            <li><a href="#Rols" data-toggle="tab">Permisos de Acceso</a></li>
        </ul>
        <div class="tab-content">
            <div id="UserInfo" class="tab-pane active">
                <asp:UpdatePanel ID="upanelUserInfo" runat="server">
                    <ContentTemplate>
                        <fieldset>
                            <legend>Información de Usuario</legend>
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label">
                                        Correo Electrónico</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtUserName" runat="server" type="email" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="controls">
                                        <asp:Label ID="lblConfirmed" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="controls">
                                    <asp:RadioButtonList ID="rbOptions" runat="server">
                                        <asp:ListItem>Cambiar Contraseña</asp:ListItem>
                                        <asp:ListItem>Re Enviar Confirmación </asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </fieldset>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="Rols" class="tab-pane">
                <asp:UpdatePanel ID="upanelRoles" runat="server">
                    <ContentTemplate>
                        <fieldset>
                            <legend>Permisos</legend>
                            <table style="width: 100%">
                                <tr style="vertical-align: top">
                                    <td>
                                        <asp:GridView ID="grvCenters" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                                            AutoGenerateColumns="False" DataKeyNames="centerId,centerName">
                                            <Columns>
                                                <asp:BoundField DataField="centerName" HeaderText="Centros" ReadOnly="True" SortExpression="centerName" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkCenter" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                    <td>
                                        <div class="control-group">
                                            <div class="controls">
                                                <asp:Button ID="btnAddCenter" runat="server" Text="+>>" class="btn btn-large btn-link"
                                                    ToolTip="Agregar Centro" OnClick="btnAddCenter_Click" />
                                            </div>
                                            <div class="controls">
                                                <asp:Button ID="btnRemoveCenter" runat="server" Text="<<-" class="btn btn-large btn-link"
                                                    type="submit" ToolTip="Quitar Centro" OnClick="btnRemoveCenter_Click"/>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <asp:GridView ID="grvSelectedCenters" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                                            AutoGenerateColumns="False" 
                                            DataKeyNames="centerId,centerName,userCenterId" 
                                            OnRowCommand="grvSelectedCenters_RowCommand">
                                            <Columns>
                                                <asp:BoundField DataField="centerName" HeaderText="Centros" ReadOnly="True" SortExpression="centerName" />
                                                <%--<asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelectedCenter" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="centerSelect" runat="server" CommandArgument='<%# Eval("userCenterId") %>'
                                                            CommandName="Select" class="icon-edit" ToolTip="Seleccionar" ImageUrl="../../../Styles/Images/blue/pencil.png">
                                                        </asp:ImageButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <p>
                                            <strong>Los permisos para el centro seleccionado se muestran de bajo.</strong></p>
                                    </td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td style="padding-top: 10px; width: 50%">
                                        <asp:GridView ID="grvRoles" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                                            AutoGenerateColumns="False" DataKeyNames="roleId,roleName">
                                            <Columns>
                                                <asp:BoundField DataField="roleName" HeaderText="Permisos" ReadOnly="true" SortExpression="roleName" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRole" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                    <td>
                                        <div class="control-group">
                                            <div class="controls">
                                                <asp:Button ID="btnAddRole" runat="server" Text="+>>" class="btn btn-large btn-link"
                                                    ToolTip="Agregar Permiso" OnClick="btnAddRole_Click" />
                                            </div>
                                            <div class="controls">
                                                <asp:Button ID="btnRemoveRole" runat="server" Text="<<-" class="btn btn-large btn-link"
                                                    ToolTip="Quitar Permiso" OnClick="btnRemoveRole_Click" />
                                            </div>
                                        </div>
                                    </td>
                                    <td style="padding-top: 10px; width: 50%">
                                        <asp:GridView ID="grvSelectedRoles" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                                            AutoGenerateColumns="false" DataKeyNames="roleId,roleName,userRoleId,userCenterId"
                                            OnRowCommand="grvSelectedRoles_RowCommand">
                                            <Columns>
                                                <asp:BoundField DataField="roleName" HeaderText="Permiso" ReadOnly="true" SortExpression="roleName" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelectedRole" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="roleSelect" runat="server" CommandArgument='<%# Eval("userRoleId") %>'
                                                            CommandName="Select" class="icon-edit" ToolTip="Seleccionar" ImageUrl="../../../Styles/Images/blue/pencil.png">
                                                        </asp:ImageButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <p>
                                            <strong>Las residencias para el centro seleccionado se muestran de bajo.</strong>
                                        </p>
                                    </td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td>
                                        <asp:GridView ID="grvResidences" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                                            AutoGenerateColumns="false" DataKeyNames="userCenterResidenceId,userRoleId,centerResidenceId,residenceName,preResidenceName">
                                            <Columns>
                                                <asp:BoundField DataField="preResidenceName" HeaderText="Pre-Requisito" ReadOnly="true"
                                                    SortExpression="preResidenceName" />
                                                <asp:BoundField DataField="residenceName" HeaderText="Residencias" ReadOnly="true"
                                                    SortExpression="residenceName" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkResidence" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                    <td>
                                        <div class="control-group">
                                            <div class="controls">
                                                <asp:Button ID="btnAddResidence" runat="server" Text="+>>" class="btn btn-large btn-link"
                                                    ToolTip="Agregar Residencia" OnClick="btnAddResidence_Click" />
                                            </div>
                                            <div class="controls">
                                                <asp:Button ID="btnRemoveResidence" runat="server" Text="<<-" class="btn btn-large btn-link"
                                                    ToolTip="Quitar Residencia" OnClick="btnRemoveResidence_Click" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <asp:GridView ID="grvSelectedResidences" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                                            AutoGenerateColumns="false" DataKeyNames="centerResidenceId,residenceName,preResidenceName">
                                            <Columns>
                                                <asp:BoundField DataField="preResidenceName" HeaderText="Pre-Requisito" ReadOnly="true"
                                                    SortExpression="preResidenceName" />
                                                <asp:BoundField DataField="residenceName" HeaderText="Residencias" ReadOnly="true"
                                                    SortExpression="residenceName" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelectedResidence" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
    </div>
</asp:Content>
