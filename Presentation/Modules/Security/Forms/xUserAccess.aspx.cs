﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using General.Persons;
using SystemSecurity.SysError;
using System.IO;
using System.Reflection;
using SystemSecurity.UserAccess;
using Residences.Residences;
using General.GeneralCommons;
using SystemSecurity.SysUsers;

namespace Presentation.Modules.Security.Forms
{
    public partial class UserAccess : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlResidences3.Items.Clear();
                ddlResidences3.DataTextField = "residenceName";
                ddlResidences3.DataValueField = "residenceId";
                ddlResidences3.DataSource = ResResidences.GetResidences((int)((SysUserAccess)Session["access"]).CenterId, 3);//(int)((SysUserAccess)Session["access"]).TeachingYearId);
                ddlResidences3.DataBind();
                ddlResidences3.Items.Insert(0, new ListItem("-- Todas las Residencias --", "0"));

                ddlResidences4.Items.Clear();
                ddlResidences4.DataTextField = "residenceName";
                ddlResidences4.DataValueField = "residenceId";
                ddlResidences4.DataSource = ResResidences.GetResidences((int)((SysUserAccess)Session["access"]).CenterId, (int)((SysUserAccess)Session["access"]).TeachingYearId);
                ddlResidences4.DataBind();
                ddlResidences4.Items.Insert(0, new ListItem("-- Todas las Residencias --", "0"));
            }
        }

        #region Buttons

        protected void txtSearchName_TextChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ////Users by entity type
                grvPersons.DataSource = GenPersons.GetPersonsByEntityType(null, (int)((SysUserAccess)Session["access"]).CenterId, true, txtSearchName.Text);
                grvPersons.DataBind();
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }
        #endregion

        #region GridViews
        protected void grvPersons_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Master.Master.PersonInfo(grvPersons.SelectedDataKey[3].ToString(), "", "");

                //fill centers lists
                SysUsers cUser = SysUsers.Get(Convert.ToInt32(grvPersons.SelectedDataKey[0]));
                
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void grvPersons_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvPersons.PageIndex = e.NewPageIndex;
            grvPersons.SelectedIndex = -1;
            btnSearch_Click(null, null);
        }
        #endregion
    }
}