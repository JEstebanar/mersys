﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.UI.WebControls;
using General.GeneralCommons;
using General.Persons;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.SysUsers;
using SystemSecurity.UserAccess;
using SystemSecurity.UserCenterResidences;
using SystemSecurity.UserCenters;
using SystemSecurity.UserRoles;

namespace Presentation.Modules.Security.Forms
{
    public partial class User : System.Web.UI.Page
    {
        private IList<SysUserCenters.UserCenters> CenterList { get { return (IList<SysUserCenters.UserCenters>)ViewState["centerList"]; } set { ViewState["centerList"] = value; } }
        private IList<SysUserCenters.UserCenters> AsignedCenters { get { return (IList<SysUserCenters.UserCenters>)ViewState["asignedCenters"]; } set { ViewState["asignedCenters"] = value; } }
        private IList<SysUserRoles.UserRoles> RoleList { get { return (IList<SysUserRoles.UserRoles>)ViewState["roleList"]; } set { ViewState["roleList"] = value; } }
        //private IList<SysUserRoles.UserRoles> UnSelectedRoles { get { return (IList<SysUserRoles.UserRoles>)ViewState["unSelectedRoles"]; } set { ViewState["unSelectedRoles"] = value; } }
        private IList<SysUserRoles.UserRoles> AsignedRoles { get { return (IList<SysUserRoles.UserRoles>)ViewState["asignedRoles"]; } set { ViewState["asignedRoles"] = value; } }
        private IList<SysUserCenterResidences.UserCenterResidences> ResidenceList { get { return (IList<SysUserCenterResidences.UserCenterResidences>)ViewState["residenceList"]; } set { ViewState["residenceList"] = value; } }
        private IList<SysUserCenterResidences.UserCenterResidences> AsignedResidences { get { return (IList<SysUserCenterResidences.UserCenterResidences>)ViewState["asignedResidences"]; } set { ViewState["asignedResidences"] = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.User))
                    {
                        if (!IsPostBack)
                        {
                            Initialize();
                            SetButtonsAction(false);
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
            //string x =Guid.NewGuid();
            //string f = Guid.NewGuid().ToString("N");
            //long t = DateTime.Now.ToFileTime();
            //int myIntValue = unchecked((int)DateTime.Now.ToFileTime());

            //DateTime centuryBegin = new DateTime(2001, 1, 1);
            //DateTime currentDate = DateTime.Now;

            //long elapsedTicks = currentDate.Ticks - centuryBegin.Ticks;
            //TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
            //int c = Convert.ToInt32(elapsedSpan.TotalMinutes);            
        }

        #region Methods

        private void Initialize()
        {
            rbOptions.Visible = false;

            ////centers allow for session user
            //CenterList = SysUserCenters.GetByUserCenterId((int)((SysUserAccess)Session["access"]).UserCenterId);
            //grvCenters.DataSource = CenterList;
            //grvCenters.DataBind();
        }

        private void MakeClean()
        {
            Master.Master.PersonInfo("", "", "");
            grvPersons.SelectedIndex = -1;
            txtUserName.Text = string.Empty;
            txtUserName.Enabled = false;
            rbOptions.SelectedIndex = -1;
            rbOptions.Visible = false;
            SetButtonsAction(false);
            grvSelectedCenters.DataSource = null;
            grvSelectedCenters.DataBind();
            grvRoles.DataSource = null;
            grvRoles.DataBind();
            grvSelectedRoles.DataSource = null;
            grvSelectedRoles.DataBind();
            grvResidences.DataSource = null;
            grvResidences.DataBind();
            grvSelectedResidences.DataSource = null;
            grvSelectedResidences.DataBind();

        }

        private void SetButtonsAction(bool Allow)
        {
            btnAddCenter.Visible = false; //Allow;
            btnRemoveCenter.Visible = false; //Allow;
            btnAddRole.Visible = Allow;
            btnRemoveRole.Visible = Allow;
            btnAddResidence.Visible = Allow;
            btnRemoveResidence.Visible = Allow;
        }

        private void HideOptionInSelectedRoles()
        {
            //foreach (GridViewRow row in grvSelectedRoles.Rows)
            //{
            //    if ((int)grvSelectedRoles.DataKeys[row.RowIndex].Values["roleId"] == (int)GeneralCommon.SystemRoles.Coordinator)
            //        (row.FindControl("roleSelect") as ImageButton).Visible = true;
            //}
        }

        #endregion

        #region Buttons

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ////Users by entity type
                grvPersons.DataSource = GenPersons.GetPersonsByEntityType(null, (int)((SysUserAccess)Session["access"]).CenterId, true, txtSearchName.Text);
                grvPersons.DataBind();
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void txtSearchName_TextChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }

        protected void btnAddCenter_Click(object sender, EventArgs e)
        {
            try
            {
                IList<SysUserCenters.UserCenters> centersToRemove = new List<SysUserCenters.UserCenters>();
                List<SysUserCenters.UserCenters> tempCenters = new List<SysUserCenters.UserCenters>();
                if (AsignedCenters != null)
                    tempCenters = AsignedCenters.ToList();

                foreach (GridViewRow rowCenter in grvCenters.Rows)
                {
                    CheckBox check = rowCenter.FindControl("chkCenter") as CheckBox;
                    if (check.Checked)
                    {
                        SysUserCenters.UserCenters newCenter = new SysUserCenters.UserCenters();
                        //newCenter.userCenterId = unchecked((int)DateTime.Now.ToFileTime());
                        newCenter.userCenterId = int.Parse(-1 + grvPersons.SelectedDataKey[0].ToString() + grvCenters.DataKeys[rowCenter.RowIndex].Values["centerId"].ToString());
                        newCenter.userId = Convert.ToInt32(grvPersons.SelectedDataKey[0]);
                        newCenter.centerId = (int)grvCenters.DataKeys[rowCenter.RowIndex].Values["centerId"];
                        newCenter.userCenterStatus = true;
                        newCenter.centerName = grvCenters.DataKeys[rowCenter.RowIndex].Values["centerName"].ToString();
                        tempCenters.Add(newCenter);
                        centersToRemove.Add(CenterList.ElementAt(rowCenter.RowIndex));
                    }
                }

                AsignedCenters = tempCenters;
                grvSelectedCenters.DataSource = AsignedCenters;
                grvSelectedCenters.DataBind();

                foreach (SysUserCenters.UserCenters centersInList in centersToRemove)
                    CenterList.Remove(centersInList);

                grvCenters.DataSource = CenterList;
                grvCenters.DataBind();
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void btnRemoveCenter_Click(object sender, EventArgs e)
        {
            try
            {
                IList<SysUserCenters.UserCenters> centersToRemove = new List<SysUserCenters.UserCenters>();
                List<SysUserCenters.UserCenters> tempCenters = new List<SysUserCenters.UserCenters>();
                if (CenterList != null)
                    tempCenters = CenterList.ToList();

                foreach (GridViewRow rowCenter in grvSelectedCenters.Rows)
                {
                    CheckBox check = rowCenter.FindControl("chkSelectedCenter") as CheckBox;
                    if (check.Checked)
                    {
                        SysUserCenters.UserCenters newCenter = new SysUserCenters.UserCenters();
                        newCenter.userCenterId = (int)grvSelectedCenters.DataKeys[rowCenter.RowIndex].Values["userCenterId"];
                        newCenter.userId = Convert.ToInt32(grvPersons.SelectedDataKey[0]);
                        newCenter.centerId = (int)grvSelectedCenters.DataKeys[rowCenter.RowIndex].Values["centerId"];
                        newCenter.userCenterStatus = false;
                        newCenter.centerName = grvSelectedCenters.DataKeys[rowCenter.RowIndex].Values["centerName"].ToString();
                        tempCenters.Add(newCenter);
                        centersToRemove.Add(AsignedCenters.ElementAt(rowCenter.RowIndex));
                    }
                }

                CenterList = tempCenters;
                grvCenters.DataSource = CenterList;
                grvCenters.DataBind();

                foreach (SysUserCenters.UserCenters centersInList in centersToRemove)
                    AsignedCenters.Remove(centersInList);

                grvSelectedCenters.DataSource = AsignedCenters;
                grvSelectedCenters.DataBind();

                ////clear grv roles
                grvRoles.DataSource = null;
                grvRoles.DataBind();

                grvSelectedRoles.DataSource = null;
                grvSelectedRoles.DataBind();
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void btnAddRole_Click(object sender, EventArgs e)
        {
            try
            {
                //IList<SysUserRoles.UserRoles> rolesToRemove = new List<SysUserRoles.UserRoles>();
                IList<SysUserRoles.UserRoles> SelectedRoles = new List<SysUserRoles.UserRoles>();
                IList<SysUserRoles.UserRoles> NoSelectedRoles = new List<SysUserRoles.UserRoles>();
                if (AsignedRoles != null)
                    SelectedRoles = AsignedRoles.ToList();

                int nUserCenterId = Convert.ToInt32(grvSelectedCenters.SelectedDataKey[2]);
                foreach (GridViewRow row in grvRoles.Rows)
                {
                    CheckBox check = row.FindControl("chkRole") as CheckBox;
                    if (check.Checked)
                    {
                        SysUserRoles.UserRoles newRole = new SysUserRoles.UserRoles();
                        newRole.userRoleId = int.Parse(nUserCenterId + grvRoles.DataKeys[row.RowIndex].Values["roleId"].ToString());
                        newRole.userCenterId = nUserCenterId;
                        newRole.roleId = (int)grvRoles.DataKeys[row.RowIndex].Values["roleId"];
                        newRole.userRoleStatus = true;
                        newRole.roleName = grvRoles.DataKeys[row.RowIndex].Values["roleName"].ToString();
                        SelectedRoles.Add(newRole);
                        //rolesToRemove.Add(RoleList.ElementAt(row.RowIndex));                        
                    }
                }

                AsignedRoles = SelectedRoles;
                grvSelectedRoles.DataSource = AsignedRoles.Where(r => r.userCenterId == nUserCenterId);
                grvSelectedRoles.DataBind();

                List<int> Asigned = new List<int>();
                List<int> NoAsigned = new List<int>();

                foreach (SysUserRoles.UserRoles roleInList in SelectedRoles.Where(r => r.userCenterId == nUserCenterId))
                    Asigned.Add(roleInList.roleId);

                foreach (SysUserRoles.UserRoles roleInList in RoleList)
                    NoAsigned.Add(roleInList.roleId);

                var result = NoAsigned.Except(Asigned);

                foreach (SysUserRoles.UserRoles roleInList in RoleList)
                {
                    foreach (int IdInList in result)
                    {
                        if (roleInList.roleId == IdInList)
                            NoSelectedRoles.Add(roleInList);
                    }
                }

                //UnSelectedRoles = NoSelectedRoles;
                grvRoles.DataSource = NoSelectedRoles;
                grvRoles.DataBind();
                HideOptionInSelectedRoles();

                //var query = from firstItem in RoleList
                //            join secondItem in tempRoles
                //            on firstItem.roleId equals secondItem.roleId
                //            select firstItem;

                //var inter = (from i in RoleList
                //             select i.roleId).Except((from o in tempRoles
                //                                      select o.roleId));

                //NoSelectedRoles = inter.ToList();

                //foreach (SysUserRoles.UserRoles roleInList in RoleList)
                // NoSelectedRoles = RoleList.Where(r => r.roleId = result);

                //foreach (SysUserRoles.UserRoles roleInList in rolesToRemove)
                //    RoleList.Remove(roleInList);
                //IList<SysUserRoles.UserRoles> UnSelectedRoles = new List<SysUserRoles.UserRoles>();
                //foreach (SysUserRoles.UserRoles selectedRoles in AsignedRoles.Where(s => s.userCenterId == nUserCenterId))
                //{
                //    //foreach (SysUserRoles.UserRoles roleInList in RoleList.Where(r => selectedRoles.roleId != r.roleId))
                //    //    UnSelectedRoles.Add(roleInList);
                //    UnSelectedRoles = UnSelectedRoles.Where(x => !UnSelectedRoles.Contains(x.roleId)).ToList();
                //}
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void btnRemoveRole_Click(object sender, EventArgs e)
        {
            try
            {
                IList<SysUserRoles.UserRoles> rolesToRemove = new List<SysUserRoles.UserRoles>();
                IList<SysUserRoles.UserRoles> SelectedRoles = new List<SysUserRoles.UserRoles>();
                IList<SysUserRoles.UserRoles> NoSelectedRoles = new List<SysUserRoles.UserRoles>();
                if (AsignedRoles != null)
                    SelectedRoles = AsignedRoles.ToList();

                int nUserCenterId = Convert.ToInt32(grvSelectedCenters.SelectedDataKey[2]);
                foreach (GridViewRow row in grvSelectedRoles.Rows)
                {
                    CheckBox check = row.FindControl("chkSelectedRole") as CheckBox;
                    if (check.Checked)
                    {
                        //SysUserRoles.UserRoles newRole = new SysUserRoles.UserRoles();
                        //newRole.userRoleId = (int)grvSelectedRoles.DataKeys[row.RowIndex].Values["userRoleId"];
                        //newRole.userCenterId = (int)grvSelectedRoles.DataKeys[row.RowIndex].Values["userCenterId"];
                        //newRole.roleId = (int)grvSelectedRoles.DataKeys[row.RowIndex].Values["roleId"];
                        //newRole.userRoleStatus = true;
                        //newRole.roleName = grvSelectedRoles.DataKeys[row.RowIndex].Values["roleName"].ToString();
                        //tempRoles.Add(newRole);
                        //rolesToRemove.Add(AsignedRoles.ElementAt(row.RowIndex));
                        var foundId = SelectedRoles.FirstOrDefault(c => c.roleId == (int)grvSelectedRoles.DataKeys[row.RowIndex].Values["roleId"] && c.userCenterId == (int)grvSelectedRoles.DataKeys[row.RowIndex].Values["userCenterId"]);
                        foundId.userCenterId = 0;
                    }
                    //else
                    //{
                    //    //foreach (SysUserRoles.UserRoles roleInList in RoleList.Where(r => r.roleId == (int)grvSelectedRoles.DataKeys[row.RowIndex].Values["roleId"]))
                    //    //{
                    //        roleInList.userRoleId = (int)grvSelectedRoles.DataKeys[row.RowIndex].Values["userRoleId"];
                    //        roleInList.userCenterId = (int)grvSelectedRoles.DataKeys[row.RowIndex].Values["userCenterId"];
                    //        roleInList.roleId = (int)grvSelectedRoles.DataKeys[row.RowIndex].Values["roleId"];
                    //        roleInList.userRoleStatus = true;
                    //        roleInList.roleName = grvSelectedRoles.DataKeys[row.RowIndex].Values["roleName"].ToString();
                    //        SelectedRoles.Add(roleInList);
                    //    //}
                    //}
                }

                //SelectedRoles.RemoveAll(item => item.roleId == 1);

                //foreach (SysUserRoles.UserRoles roleInList in rolesToRemove)
                //    SelectedRoles.Remove(roleInList);


                List<int> Asigned = new List<int>();
                List<int> NoAsigned = new List<int>();

                foreach (SysUserRoles.UserRoles roleInList in SelectedRoles.Where(r => r.userCenterId == nUserCenterId))
                    Asigned.Add(roleInList.roleId);

                foreach (SysUserRoles.UserRoles roleInList in RoleList)
                    NoAsigned.Add(roleInList.roleId);

                var result = NoAsigned.Except(Asigned);

                foreach (SysUserRoles.UserRoles roleInList in RoleList)
                {
                    foreach (int IdInList in result)
                    {
                        if (roleInList.roleId == IdInList)
                            NoSelectedRoles.Add(roleInList);
                    }
                }

                AsignedRoles = SelectedRoles;
                grvSelectedRoles.DataSource = AsignedRoles.Where(r => r.userCenterId == nUserCenterId);
                grvSelectedRoles.DataBind();

                //UnSelectedRoles = tempRoles;
                grvRoles.DataSource = NoSelectedRoles;
                grvRoles.DataBind();

                ////clear grv residences
                grvResidences.DataSource = null;
                grvResidences.DataBind();

                grvSelectedResidences.DataSource = null;
                grvSelectedResidences.DataBind();

                HideOptionInSelectedRoles();
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void btnAddResidence_Click(object sender, EventArgs e)
        {
            try
            {
                //IList<SysUserCenterResidences.UserCenterResidences> residenceToRemove = new List<SysUserCenterResidences.UserCenterResidences>();
                IList<SysUserCenterResidences.UserCenterResidences> SelectedResidences = new List<SysUserCenterResidences.UserCenterResidences>();
                IList<SysUserCenterResidences.UserCenterResidences> NoSelectedResidences = new List<SysUserCenterResidences.UserCenterResidences>();
                //IList<SysUserCenterResidences.UserCenterResidences> tempResidenceList = ResidenceList;
                if (AsignedResidences != null)
                    SelectedResidences = AsignedResidences.ToList();

                int nUserRoleId = Convert.ToInt32(grvSelectedRoles.SelectedDataKey[2]);
                foreach (GridViewRow row in grvResidences.Rows)
                {
                    CheckBox check = row.FindControl("chkResidence") as CheckBox;
                    if (check.Checked)
                    {
                        SysUserCenterResidences.UserCenterResidences newResidence = new SysUserCenterResidences.UserCenterResidences();
                        newResidence.userCenterResidenceId = int.Parse(nUserRoleId + grvResidences.DataKeys[row.RowIndex].Values["centerResidenceId"].ToString());
                        newResidence.userRoleId = nUserRoleId;
                        newResidence.centerResidenceId = (int)grvResidences.DataKeys[row.RowIndex].Values["centerResidenceId"];
                        newResidence.userResidenceStatus = true;
                        newResidence.residenceName = grvResidences.DataKeys[row.RowIndex].Values["residenceName"].ToString();
                        newResidence.preResidenceName = grvResidences.DataKeys[row.RowIndex].Values["preResidenceName"].ToString();
                        SelectedResidences.Add(newResidence);
                        //residenceToRemove.Add(tempResidenceList.ElementAt(row.RowIndex));
                    }
                }

                AsignedResidences = SelectedResidences;
                grvSelectedResidences.DataSource = AsignedResidences.Where(r => r.userRoleId == nUserRoleId);
                grvSelectedResidences.DataBind();

                List<int> Asigned = new List<int>();
                List<int> NoAsigned = new List<int>();

                foreach (SysUserCenterResidences.UserCenterResidences residenceInList in SelectedResidences.Where(r => r.userRoleId == nUserRoleId))
                    Asigned.Add(residenceInList.centerResidenceId);

                foreach (SysUserCenterResidences.UserCenterResidences residenceInList in ResidenceList)
                    NoAsigned.Add(residenceInList.centerResidenceId);

                var result = NoAsigned.Except(Asigned);

                foreach (SysUserCenterResidences.UserCenterResidences residenceInList in ResidenceList)
                {
                    foreach (int IdInList in result)
                    {
                        if (residenceInList.centerResidenceId == IdInList)
                            NoSelectedResidences.Add(residenceInList);
                    }
                }

                grvResidences.DataSource = NoSelectedResidences;
                grvResidences.DataBind();

                //foreach (SysUserCenterResidences.UserCenterResidences residenceInList in residenceToRemove)
                //{
                //    //tempResidenceList.Remove(residenceInList);
                //}

                //grvResidences.DataSource = tempResidenceList;
                //grvResidences.DataBind();

                /////// && selectedResidencesInList.userRoleId == Convert.ToInt32(grvSelectedRoles.SelectedDataKey[2])
                //IList<SysUserCenterResidences.UserCenterResidences> UnSelectedResidences = new List<SysUserCenterResidences.UserCenterResidences>(); 
                //    //= ResidenceList;
                //foreach (SysUserCenterResidences.UserCenterResidences selectedResidencesInList in AsignedResidences)
                //{
                //    foreach (SysUserCenterResidences.UserCenterResidences residencesInlist in ResidenceList)
                //    {
                //        if (selectedResidencesInList.centerResidenceId != residencesInlist.centerResidenceId)
                //            UnSelectedResidences.Add(residencesInlist);
                //       // UnSelectedResidences.Where(r => 5 != selectedResidencesInList.centerResidenceId);
                //    }
                //}

                //if (AsignedResidences.Count == 0)
                //    UnSelectedResidences = ResidenceList;

                //grvResidences.DataSource = UnSelectedResidences;
                //grvResidences.DataBind();
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void btnRemoveResidence_Click(object sender, EventArgs e)
        {
            try
            {
                IList<SysUserCenterResidences.UserCenterResidences> residenceToRemove = new List<SysUserCenterResidences.UserCenterResidences>();
                List<SysUserCenterResidences.UserCenterResidences> SelectedResidences = new List<SysUserCenterResidences.UserCenterResidences>();
                List<SysUserCenterResidences.UserCenterResidences> NoSelectedResidences = new List<SysUserCenterResidences.UserCenterResidences>();
                //IList<SysUserCenterResidences.UserCenterResidences> tempResidenceLists = ResidenceList;
                if (AsignedResidences != null)
                    SelectedResidences = AsignedResidences.ToList();

                int nUserRoleId = Convert.ToInt32(grvSelectedRoles.SelectedDataKey[2]);
                foreach (GridViewRow row in grvSelectedResidences.Rows)
                {
                    CheckBox check = row.FindControl("chkSelectedResidence") as CheckBox;
                    if (check.Checked)
                    {
                        //SysUserCenterResidences.UserCenterResidences newResidence = new SysUserCenterResidences.UserCenterResidences();
                        //newResidence.userCenterResidenceId = (int)grvSelectedResidences.DataKeys[row.RowIndex].Values["userCenterResidenceId"];
                        //newResidence.userRoleId = (int)grvSelectedResidences.DataKeys[row.RowIndex].Values["userRoleId"];
                        //newResidence.centerResidenceId = (int)grvSelectedResidences.DataKeys[row.RowIndex].Values["centerResidenceId"];
                        //newResidence.userResidenceStatus = false;
                        //newResidence.residenceName = grvSelectedResidences.DataKeys[row.RowIndex].Values["residenceName"].ToString();
                        //newResidence.preResidenceName = grvSelectedResidences.DataKeys[row.RowIndex].Values["preResidenceName"].ToString();
                        //tempResidences.Add(newResidence);
                        //residenceToRemove.Add(AsignedResidences.ElementAt(row.RowIndex));
                        var foundId = SelectedResidences.FirstOrDefault(c => c.centerResidenceId == (int)grvSelectedResidences.DataKeys[row.RowIndex].Values["centerResidenceId"] && c.userRoleId == nUserRoleId);
                        foundId.userRoleId = 0;
                    }
                }

                //foreach (SysUserCenterResidences.UserCenterResidences residenceInList in residenceToRemove)
                //    SelectedResidences.Remove(residenceInList);


                List<int> Asigned = new List<int>();
                List<int> NoAsigned = new List<int>();

                foreach (SysUserCenterResidences.UserCenterResidences residenceInList in SelectedResidences.Where(r => r.userRoleId == nUserRoleId))
                    Asigned.Add(residenceInList.centerResidenceId);

                foreach (SysUserCenterResidences.UserCenterResidences residenceInList in ResidenceList)
                    NoAsigned.Add(residenceInList.centerResidenceId);

                var result = NoAsigned.Except(Asigned);

                foreach (SysUserCenterResidences.UserCenterResidences residenceInList in ResidenceList)
                {
                    foreach (int IdInList in result)
                    {
                        if (residenceInList.centerResidenceId == IdInList)
                            NoSelectedResidences.Add(residenceInList);
                    }
                }

                AsignedResidences = SelectedResidences;
                grvSelectedResidences.DataSource = AsignedResidences.Where(r => r.userRoleId == nUserRoleId);
                grvSelectedResidences.DataBind();

                //UnSelectedRoles = tempRoles;
                grvResidences.DataSource = NoSelectedResidences;
                grvResidences.DataBind();

                //tempResidenceLists = tempResidences;
                //grvResidences.DataSource = tempResidenceLists;
                //grvResidences.DataBind();

                //foreach (SysUserCenterResidences.UserCenterResidences residenceInList in residenceToRemove)
                //    AsignedResidences.Remove(residenceInList);

                //grvSelectedResidences.DataSource = AsignedResidences;
                //grvSelectedResidences.DataBind();
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Master.Master.Message("¡Not in service!", "", 1);
            return;

            if (grvPersons.SelectedIndex > -1)
            {
                int cUserId = Convert.ToInt32(grvPersons.SelectedDataKey[0]);

                switch (Convert.ToInt32(rbOptions.SelectedIndex))
                {
                    case 0:
                        SysUsers.ResetPassword(cUserId);
                        break;
                    case 1:
                        SysUsers.ForwardConfirmation(cUserId);
                        break;
                }

                //if (nUserId == 0)
                //    nUserId = SysUsers.CreateUser(Convert.ToInt32(grvPersons.SelectedDataKey[1]), txtUserName.Text, (int)((SysUserAccess)Session["access"]).CenterId);

                SysUserCenters.SetPermissionsInactives(Convert.ToInt32(grvPersons.SelectedDataKey[0]));
                int nUserCenterId;
                int nUserRoleId;
                foreach (SysUserCenters.UserCenters centersInList in AsignedCenters)
                {
                    nUserCenterId = SysUserCenters.IfExists(centersInList.userCenterId, cUserId, centersInList.centerId, centersInList.userCenterStatus);

                    foreach (SysUserRoles.UserRoles rolesInList in AsignedRoles.Where(r => r.userCenterId == centersInList.userCenterId && r.userCenterId != 0))
                    {
                        if (nUserCenterId == 0)
                            nUserCenterId = rolesInList.userCenterId;

                        nUserRoleId = SysUserRoles.IfExists(rolesInList.userRoleId, nUserCenterId, rolesInList.roleId, rolesInList.userRoleStatus);

                        foreach (SysUserCenterResidences.UserCenterResidences residencesInList in AsignedResidences.Where(r => r.userRoleId == rolesInList.userRoleId && r.userRoleId != 0))
                        {
                            if (nUserRoleId == 0)
                                nUserRoleId = residencesInList.userRoleId;

                            //SysUserCenterResidences.IfExists(residencesInList.userCenterResidenceId, nUserRoleId, residencesInList.centerResidenceId, residencesInList.userResidenceStatus);
                        }
                    }
                }
                Master.Master.Message("¡Datos Guardados Exitosamente!", "", 3);
                MakeClean();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            SetButtonsAction(false);
            MakeClean();
        }

        #endregion

        #region GridViews
        protected void grvPersons_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Master.Master.PersonInfo(grvPersons.SelectedDataKey[3].ToString(), "", "");

                //fill centers lists
                SysUsers cUser = SysUsers.Get(Convert.ToInt32(grvPersons.SelectedDataKey[0]));
                txtUserName.Text = cUser.UserName;
                if (cUser.ConfirmationCode == null)
                {
                    txtUserName.Enabled = false;
                    rbOptions.Visible = true;
                    lblConfirmed.Text = "Confirmado";
                }
                else
                {
                    txtUserName.Enabled = true;
                    rbOptions.Visible = false;
                    lblConfirmed.Text = "Sin Confirmar";
                }
                rbOptions.Visible = false;

                //CenterList = SysUserCenters.GetUnAsignedByUserId(Convert.ToInt32(grvPersons.SelectedDataKey[0]));
                AsignedCenters = SysUserCenters.GetByUserId(Convert.ToInt32(grvPersons.SelectedDataKey[0]));
                grvSelectedCenters.DataSource = AsignedCenters;
                grvSelectedCenters.DataBind();
                //AsignedCenters = CenterList;

                SetButtonsAction(true);

                RoleList = SysUserRoles.GetRolesToAssign();
                AsignedRoles = SysUserRoles.GetByUserId(Convert.ToInt32(grvPersons.SelectedDataKey[0]));

                ResidenceList = SysUserCenterResidences.GetResidences((int)((SysUserAccess)Session["access"]).CenterId, (int)((SysUserAccess)Session["access"]).TeachingYearId);
                AsignedResidences = SysUserCenterResidences.GetByUserId(Convert.ToInt32(grvPersons.SelectedDataKey[0]), (int)((SysUserAccess)Session["access"]).TeachingYearId);

                grvRoles.DataSource = null;
                grvRoles.DataBind();

                grvSelectedRoles.DataSource = null;
                grvSelectedRoles.DataBind();

                grvResidences.DataSource = null;
                grvResidences.DataBind();

                grvSelectedResidences.DataSource = null;
                grvSelectedResidences.DataBind();

                //grvSelectedCenters.DataSource = CenterList;
                //grvSelectedCenters.DataBind();

                ////fill centers grv
                //IList<SysUserCenters.UserCenters> SelectedCenters = new List<SysUserCenters.UserCenters>();
                //IList<SysUserCenters.UserCenters> NoSelectedCenters = new List<SysUserCenters.UserCenters>();
                //foreach (SysUserCenters.UserCenters selectedCentersInList in AsignedCenters)
                //    SelectedCenters.Add(selectedCentersInList);

                //grvSelectedCenters.DataSource = SelectedCenters;
                //grvSelectedCenters.DataBind();

                //List<int> Asigned = new List<int>();
                //List<int> NoAsigned = new List<int>();

                //foreach (SysUserCenters.UserCenters centerInList in SelectedCenters)
                //    Asigned.Add(centerInList.centerId);

                //foreach (SysUserCenters.UserCenters centerInList in CenterList)
                //    NoAsigned.Add(centerInList.centerId);

                //var result = NoAsigned.Except(Asigned);

                //foreach (SysUserCenters.UserCenters centerInList in CenterList)
                //{
                //    foreach (int IdInList in result)
                //    {
                //        if (centerInList.centerId == IdInList)
                //            NoSelectedCenters.Add(centerInList);
                //    }
                //}

                //grvCenters.DataSource = NoSelectedCenters;
                //grvCenters.DataBind();
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void grvPersons_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvPersons.PageIndex = e.NewPageIndex;
            grvPersons.SelectedIndex = -1;
            btnSearch_Click(null, null);
        }

        protected void grvSelectedCenters_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Select")
                {
                    int nUserCenterId = Convert.ToInt32(e.CommandArgument);

                    IList<SysUserRoles.UserRoles> SelectedRoles = new List<SysUserRoles.UserRoles>();
                    IList<SysUserRoles.UserRoles> NoSelectedRoles = new List<SysUserRoles.UserRoles>();
                    foreach (SysUserRoles.UserRoles selectedRolesInList in AsignedRoles.Where(r => r.userCenterId == nUserCenterId))
                        SelectedRoles.Add(selectedRolesInList);

                    grvSelectedRoles.DataSource = SelectedRoles;
                    grvSelectedRoles.DataBind();
                    HideOptionInSelectedRoles();

                    List<int> Asigned = new List<int>();
                    List<int> NoAsigned = new List<int>();

                    foreach (SysUserRoles.UserRoles roleInList in SelectedRoles.Where(r => r.userCenterId == nUserCenterId))
                        Asigned.Add(roleInList.roleId);

                    foreach (SysUserRoles.UserRoles roleInList in RoleList)
                        NoAsigned.Add(roleInList.roleId);

                    var result = NoAsigned.Except(Asigned);

                    foreach (SysUserRoles.UserRoles roleInList in RoleList)
                    {
                        foreach (int IdInList in result)
                        {
                            if (roleInList.roleId == IdInList)
                                NoSelectedRoles.Add(roleInList);
                        }
                    }

                    grvRoles.DataSource = NoSelectedRoles;
                    grvRoles.DataBind();


                    //IList<SysUserRoles.UserRoles> NoSelectedRoles = new List<SysUserRoles.UserRoles>();
                    ////NoSelectedRoles = RoleList;
                    ////foreach (SysUserRoles.UserRoles selectedRolesInList in SelectedRoles)
                    ////{
                    ////    //NoSelectedRoles.FirstOrDefault(r => r.roleId != selectedRolesInList.roleId && r.userCenterId == selectedRolesInList.userCenterId);
                    ////    if (selectedRolesInList.userCenterId == UserCenterId)
                    ////        NoSelectedRoles.Add(selectedRolesInList);
                    ////}

                    //if (UnSelectedRoles != null)
                    //{
                    //    foreach (SysUserRoles.UserRoles rolesInList in UnSelectedRoles.Where(r => r.userCenterId == UserCenterId))
                    //        NoSelectedRoles.Add(rolesInList);
                    //}

                    //if (NoSelectedRoles.Count == 0)
                    //    NoSelectedRoles = RoleList;

                    //grvRoles.DataSource = NoSelectedRoles;
                    //grvRoles.DataBind();

                    grvResidences.DataSource = null;
                    grvResidences.DataBind();

                    grvSelectedResidences.DataSource = null;
                    grvSelectedResidences.DataBind();
                }
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void grvSelectedRoles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Select")
                {
                    int nUserRoleId = Convert.ToInt32(e.CommandArgument);

                    IList<SysUserCenterResidences.UserCenterResidences> SelectedResidences = new List<SysUserCenterResidences.UserCenterResidences>();
                    IList<SysUserCenterResidences.UserCenterResidences> NoSelectedResidences = new List<SysUserCenterResidences.UserCenterResidences>();
                    foreach (SysUserCenterResidences.UserCenterResidences selectedresidencesInList in AsignedResidences.Where(r => r.userRoleId == nUserRoleId))
                        SelectedResidences.Add(selectedresidencesInList);

                    grvSelectedResidences.DataSource = SelectedResidences;
                    grvSelectedResidences.DataBind();

                    List<int> Asigned = new List<int>();
                    List<int> NoAsigned = new List<int>();

                    foreach (SysUserCenterResidences.UserCenterResidences residenceInList in SelectedResidences.Where(r => r.userRoleId == nUserRoleId))
                        Asigned.Add(residenceInList.centerResidenceId);

                    foreach (SysUserCenterResidences.UserCenterResidences residenceInList in ResidenceList)
                        NoAsigned.Add(residenceInList.centerResidenceId);

                    var result = NoAsigned.Except(Asigned);

                    foreach (SysUserCenterResidences.UserCenterResidences residenceInList in ResidenceList)
                    {
                        foreach (int IdInList in result)
                        {
                            if (residenceInList.centerResidenceId == IdInList)
                                NoSelectedResidences.Add(residenceInList);
                        }
                    }

                    grvResidences.DataSource = NoSelectedResidences;
                    grvResidences.DataBind();

                    //IList<SysUserCenterResidences.UserCenterResidences> UnSelectedResidences = new List<SysUserCenterResidences.UserCenterResidences>();
                    ////UnSelectedResidences = ResidenceList;
                    //foreach (SysUserCenterResidences.UserCenterResidences selectedResidencesInList in SelectedResidences)
                    //{
                    //    foreach (SysUserCenterResidences.UserCenterResidences residencesInList in ResidenceList)
                    //    {
                    //        if (selectedResidencesInList.centerResidenceId != residencesInList.centerResidenceId && selectedResidencesInList.userRoleId == UserRoleId)
                    //            UnSelectedResidences.Add(residencesInList);
                    //    }
                    //    //UnSelectedResidences.Where(r => r.centerResidenceId != selectedResidencesInList.centerResidenceId && selectedResidencesInList.userRoleId == UserRoleId);
                    //}

                    //if (SelectedResidences.Count == 0)
                    //    UnSelectedResidences = ResidenceList;

                    //grvResidences.DataSource = UnSelectedResidences;
                    //grvResidences.DataBind();
                }
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }
        #endregion
    }
}