﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using General.Gender;
using General.MaritalStatus;
using General.PhoneType;
using General.States;
using General.Nationalities;
using General.IdentityDocumentTypes;
using General.GeneralCommons;
using General.Cities;

namespace Presentation.Modules.General.Forms
{
    public partial class GeneralRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //Initialize();
            }
        }

        protected void btnCreateResident_Click(object sender, EventArgs e)
        {
            //clean form
            MadeClean();


        }

        private void MadeClean()
        {

        }

        //private void Initialize()
        //{
        //    ////personal information
        //    ddlGenders.DataTextField = "genderName";
        //    ddlGenders.DataValueField = "genderId";
        //    ddlGenders.DataSource = GenGenders.GetGenders();
        //    ddlGenders.DataBind();
        //    ddlGenders.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

        //    ddlMaritalStatuses.DataTextField = "maritalStatusName";
        //    ddlMaritalStatuses.DataValueField = "maritalStatusId";
        //    ddlMaritalStatuses.DataSource = GenMaritalStatus.GetMaritalStatuses();
        //    ddlMaritalStatuses.DataBind();
        //    ddlMaritalStatuses.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

        //    ////localization
        //    ddlPhoneTipes.DataTextField = "typeName";
        //    ddlPhoneTipes.DataValueField = "typeId";
        //    ddlPhoneTipes.DataSource = GenPhoneTypes.GetPhoneTypes();
        //    ddlPhoneTipes.DataBind();
        //    ddlPhoneTipes.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

        //    ddlStates.DataTextField = "stateName";
        //    ddlStates.DataValueField = "stateId";
        //    ddlStates.DataSource = GenStates.GetStatesByCountryId(null);
        //    ddlStates.DataBind();
        //    ddlStates.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

        //    ddlNationality.DataTextField = "nationalityName";
        //    ddlNationality.DataValueField = "nationalityId";
        //    ddlNationality.DataSource = GenNationalities.GetNationalities();
        //    ddlNationality.DataBind();
        //    ddlNationality.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

        //    ////Identification
        //    ddlIndentificationTypes.DataTextField = "TypeName";
        //    ddlIndentificationTypes.DataValueField = "TypeId";
        //    ddlIndentificationTypes.DataSource = GenIdentityDocumentTypes.GetIdentityDocumentTypeByEntityTypeId((int)GeneralCommon.EntityTypes.Resident, 1);
        //    ddlIndentificationTypes.DataBind();
        //    ddlIndentificationTypes.Items.Insert(0, new ListItem("-- Seleccione --", "0"));
        //}

        //protected void ddlStates_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ddlCities.DataTextField = "cityName";
        //    ddlCities.DataValueField = "cityId";
        //    ddlCities.DataSource = GenCities.GetCitiesByStateId(Convert.ToInt32(ddlStates.SelectedValue));
        //    ddlCities.DataBind();
        //    ddlCities.Items.Insert(0, new ListItem("-- Seleccione --", "0"));
        //}

        //protected void btnAddIdentification_Click(object sender, EventArgs e)
        //{

        //}

        //protected void btnAddPhone_Click(object sender, EventArgs e)
        //{
        //    ddlCities.DataTextField = "cityName";
        //    ddlCities.DataValueField = "cityId";
        //    ddlCities.DataSource = GenCities.GetCitiesByStateId(Convert.ToInt32(ddlStates.SelectedValue));
        //    ddlCities.DataBind();
        //    ddlCities.Items.Insert(0, new ListItem("-- Seleccione --", "0"));
        //}
    }
}

