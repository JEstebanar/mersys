﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using General.Generals;
using General.Persons;
using General.Entities;
using General.Gender;
using General.MaritalStatus;
using General.States;
using General.Nationalities;
using General.Cities;
using General.GeneralCommons;
using General.GeneralPhones;
using General.GeneralIdentityDocuments;
using General.PersonInfo;
using General.Address;
using SystemSecurity.SysUsers;
using SystemSecurity.UserAccess;
using System.Web.UI.HtmlControls;
using Residences.Residents;
using General.Forms;
using General.Utilities;
using SystemSecurity.SysError;
using System.IO;
using System.Reflection;
using General.EMails;
using SystemSecurity.Utilities;
using Residences.Residences;
using SystemSecurity.CenterResidences;
using SystemSecurity.UserCenterResidences;

namespace Presentation.Modules.General.UserControls
{
    public partial class wucGeneralRegister : System.Web.UI.UserControl
    {
        #region Properties
        public int ModuleId
        {
            get { return (int)ViewState["moduleId"]; }
            set { ViewState["moduleId"] = value; }
        }
        public int ScreenId
        {
            get { return (int)ViewState["screenId"]; }
            set { ViewState["screenId"] = value; }
        }
        public int EntityTypeId
        {
            get { return (int)ViewState["entityId"]; }
            set { ViewState["entityId"] = value; }
        }

        private IList<GenGeneralPhones.Phones> AsignedPhones { get { return (IList<GenGeneralPhones.Phones>)ViewState["asignedPhones"]; } set { ViewState["asignedPhones"] = value; } }
        private IList<GenGeneralIdentityDocuments.Identifications> AsignedIdentifications { get { return (IList<GenGeneralIdentityDocuments.Identifications>)ViewState["asignedIdentifications"]; } set { ViewState["asignedIdentifications"] = value; } }
        private IList<SysCenterResidences.CenterResidences> CenterResidences { get { return (IList<SysCenterResidences.CenterResidences>)ViewState["centerResidences"]; } set { ViewState["centerResidences"] = value; } }
        //private IList<GenEntities.Entities> AsignedEntities { get { return (IList<GenEntities.Entities>)ViewState["asignedEntities"]; } set { ViewState["asignedEntities"] = value; } }
        //private IList<GenEntities.Entities> NewsEntities { get { return (IList<GenEntities.Entities>)ViewState["newsEntities"]; } set { ViewState["newsEntities"] = value; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialize();
            }
            Message("", "", 0);
        }

        #region Methods

        private void Initialize()
        {
            ////personal information
            ddlGenders.DataTextField = "genderName";
            ddlGenders.DataValueField = "genderId";
            ddlGenders.DataSource = GenGenders.GetGenders();
            ddlGenders.DataBind();
            ddlGenders.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

            ddlMaritalStatuses.DataTextField = "maritalStatusName";
            ddlMaritalStatuses.DataValueField = "maritalStatusId";
            ddlMaritalStatuses.DataSource = GenMaritalStatus.GetMaritalStatuses();
            ddlMaritalStatuses.DataBind();
            ddlMaritalStatuses.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

            btnResendEmailConfirmation.Visible = false;
            btnResetPassword.Visible = false;

            //////localization
            //ddlPhoneTypes.DataTextField = "typeName";
            //ddlPhoneTypes.DataValueField = "typeId";
            //ddlPhoneTypes.DataSource = GenPhoneTypes.GetPhoneTypes();
            //ddlPhoneTypes.DataBind();
            //ddlPhoneTypes.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

            ddlStates.DataTextField = "stateName";
            ddlStates.DataValueField = "stateId";
            ddlStates.DataSource = GenStates.GetStatesByCountryId(null);
            ddlStates.DataBind();
            ddlStates.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

            ddlNationalities.DataTextField = "nationalityName";
            ddlNationalities.DataValueField = "nationalityId";
            ddlNationalities.DataSource = GenNationalities.GetNationalities();
            ddlNationalities.DataBind();
            ddlNationalities.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

            //////Identification
            //ddlIndentificationTypes.DataTextField = "TypeName";
            //ddlIndentificationTypes.DataValueField = "TypeId";
            //ddlIndentificationTypes.DataSource = GenIdentityDocumentTypes.GetIdentityDocumentTypeByEntityTypeId(EntityTypeId, (int)((SysUserAccess)Session["access"]).CenterId);
            //ddlIndentificationTypes.DataBind();
            //ddlIndentificationTypes.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

            //coordinators
            if (EntityTypeId == (int)GeneralCommon.EntityTypes.Coordinator)
            {
                divResidences.Visible = true;
                ddlResidences.Items.Clear();
                ddlResidences.DataTextField = "residenceName";
                ddlResidences.DataValueField = "residenceId";
                ddlResidences.DataSource = ResResidences.GetParents((int)((SysUserAccess)Session["access"]).CenterId, (int)((SysUserAccess)Session["access"]).TeachingYearId);
                ddlResidences.DataBind();
                ddlResidences.Items.Insert(0, new ListItem("-- Seleccionar --", "0"));
            }
            else
                divResidences.Visible = false;
        }

        private bool ValidateResidentTransferred()
        {
            if (EntityTypeId == (int)GeneralCommon.EntityTypes.Resident)
            {
                if (ResResidents.GetCurrentStatus(Convert.ToInt32(grvPersons.SelectedDataKey[3])) != (int)GeneralCommon.ResidentStatuses.Transferred)
                {
                    btnSave.Visible = true;
                    btnCancel.Visible = true;
                    return true;
                }
                else
                {
                    //if the resident has been transferred
                    Message("Este médico residente ha sido transferido a otro centro y sus datos no pueden ser modificados.", "", 4);
                    btnSave.Visible = false;
                    btnCancel.Visible = false;
                    return false;
                }
            }
            else
                return true;
        }

        private void MakeClean()
        {
            PersonInformation("", "", "");
            txtName.Text = string.Empty;
            txtMiddleName.Text = string.Empty;
            txtFirstLastName.Text = string.Empty;
            txtSecondLastName.Text = string.Empty;
            txtBirthday.Text = string.Empty;
            ddlGenders.SelectedValue = "0";
            ddlMaritalStatuses.SelectedValue = "0";
            txtHomePhone.Text = string.Empty;
            txtCellPhone.Text = string.Empty;
            txtUserName.Text = string.Empty;
            txtUserName.Enabled = true;
            btnResendEmailConfirmation.Visible = false;
            btnResetPassword.Visible = false;
            txtAddress1.Text = string.Empty;
            txtAddress2.Text = string.Empty;
            ddlStates.SelectedValue = "0";
            //ddlCities.SelectedValue = "0";
            ddlNationalities.SelectedValue = "0";
            txtIdentificationCard.Text = string.Empty;
            txtIdentCardExDate.Text = string.Empty;
            txtPassport.Text = string.Empty;
            txtPassportExDate.Text = string.Empty;
            ddlResidences.SelectedValue = "0";

            AsignedPhones = null;
            //grvPhones.DataSource = AsignedPhones;
            //grvPhones.DataBind();

            AsignedIdentifications = null;
            //grvIdentifications.DataSource = AsignedIdentifications;
            //grvIdentifications.DataBind();

            grvEntities.DataSource = null;
            grvEntities.DataBind();
        }

        private void FillCities(int stateId)
        {
            ddlCities.DataTextField = "cityName";
            ddlCities.DataValueField = "cityId";
            ddlCities.DataSource = GenCities.GetCitiesByStateId(stateId);
            ddlCities.DataBind();
            ddlCities.Items.Insert(0, new ListItem("-- Seleccione --", "0"));
        }

        private void IfExistsPhones(int generalId, string phoneNumber, int phoneTypeId)
        {
            bool exists = false;
            int phoneId = 0;
            if (AsignedPhones != null)
            {
                foreach (GenGeneralPhones.Phones phoneInList in AsignedPhones)
                {
                    if (phoneInList.phoneTypeId == phoneTypeId)
                    {
                        exists = true;
                        phoneId = phoneInList.phoneId;
                    }
                }
            }

            GenGeneralPhones cPhones;
            if (exists)
                cPhones = GenGeneralPhones.Get(phoneId);
            else
                cPhones = new GenGeneralPhones();


            cPhones.GeneralId = generalId;
            cPhones.PhoneNumber = phoneNumber;
            cPhones.PhoneTypeId = phoneTypeId;
            cPhones.PhoneStatus = true;

            if (exists)
            {
                if (phoneNumber == string.Empty)
                    cPhones.Delete();
                else
                {
                    cPhones.ModifiedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                    cPhones.ModifiedDate = DateTime.Now;
                    cPhones.Update();
                }
            }
            else
            {
                if (phoneNumber != string.Empty)
                {
                    cPhones.CreatedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                    cPhones.CreatedDate = DateTime.Now;
                    cPhones.Insert();
                }
            }
        }

        private void IfExistsIdentifications(int generalId, string identificationNumber, int identificationTypeId, DateTime expirationDate, ref FileUpload fUpload)
        {
            bool exists = false;
            int identificationId = 0;
            if (AsignedIdentifications != null)
            {
                foreach (GenGeneralIdentityDocuments.Identifications identificationInList in AsignedIdentifications)
                {
                    if (identificationInList.identificationTypeId == identificationTypeId)
                    {
                        exists = true;
                        identificationId = identificationInList.identificationId;
                    }
                }
            }

            GenGeneralIdentityDocuments cIdentification;
            if (exists)
                cIdentification = GenGeneralIdentityDocuments.Get(identificationId);
            else
                cIdentification = new GenGeneralIdentityDocuments();


            cIdentification.GeneralId = generalId;
            cIdentification.IdentityNumber = identificationNumber;
            cIdentification.IdentityTypeId = identificationTypeId;
            cIdentification.ExpirationDate = expirationDate;
            if (fUpload.HasFile)
                cIdentification.IdentityPath = GenUtilities.SaveFileInPath(ref fUpload, GeneralCommon.TypeFilePaths.Identification, "");

            if (exists)
            {
                if (identificationNumber == string.Empty)
                    cIdentification.Delete();
                else
                {
                    cIdentification.ModifiedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                    cIdentification.ModifiedDate = DateTime.Now;
                    cIdentification.Update();
                }
            }
            else
            {
                if (identificationNumber != string.Empty)
                {
                    cIdentification.CreatedBY = ((SysUserAccess)Session["access"]).UserCenterId;
                    cIdentification.CreatedDate = DateTime.Now;
                    cIdentification.Insert();
                }
            }
        }

        private void HideOptionsInEntities()
        {
            foreach (GridViewRow row in grvEntities.Rows)
            {
                CheckBox chk = row.FindControl("chkEntity") as CheckBox;
                chk.Visible = false;
                if (Convert.ToInt32(grvEntities.DataKeys[row.RowIndex].Values["entityId"]) == EntityTypeId)
                {
                    chk.Visible = true;
                    //all residents are set in Resident.aspx
                    if (EntityTypeId == (int)GeneralCommon.EntityTypes.Resident)
                        chk.Enabled = false;
                }
            }
        }

        private bool Validations()
        {
            bool StopValidation = false;
            ////phones
            if (txtHomePhone.Text== string.Empty && txtCellPhone.Text== string.Empty)
            {
                Message("Un número de teléfono es requerido.", "", 2);
                StopValidation = true;
            }

            if (txtHomePhone.MaxLength < 10 || txtCellPhone.MaxLength < 10)
            {
                Message("Número de teléfono incorrecto.", "", 2);
                StopValidation = true;
            }

            ////email
            if (!GenUtilities.IsValidEmail(txtUserName.Text))
            {
                Message("Correo Electrónico Invalido.", "", 2);
                StopValidation = true;
            }

            ////identity card
            int? _generalId;
            if (grvPersons.SelectedIndex > -1)
                _generalId = Convert.ToInt32(grvPersons.SelectedDataKey[0]);
            else
                _generalId = null;

            if (GenGeneralIdentityDocuments.IfExistsDocuemnt(txtIdentificationCard.Text, _generalId))
            {
                Message("Este número de Cédula ya se encuentra asignado a otra persona.", "", 2);
                StopValidation = true;
            }

            ////passport
            if (GenGeneralIdentityDocuments.IfExistsDocuemnt(txtPassport.Text, _generalId))
            {
                Message("Este número de Pasaporte ya se encuentra asignado a otra persona.", "", 2);
                StopValidation = true;
            }

            if (txtPassport.Text != string.Empty && txtPassportExDate.Text == string.Empty)
            {
                Message("Fecha de Vencimineto para el Pasaporte es requerida.", "", 2);
                StopValidation = true;
            }

            //coordinators
            if (EntityTypeId == (int)GeneralCommon.EntityTypes.Coordinator)
            {
                if (ddlResidences.SelectedValue == "0")
                {
                    Message("Favor indicar una residencia para este coordinador", "", 2);
                    StopValidation = true;
                }
            }

            ////else
            ////    divMessage.Visible = false;

            return StopValidation;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">Message you want to display</param>
        /// <param name="header">Message header</param>
        /// <param name="type">1 - Warning Message, || 2 - Error Message, || 3 - Message of Success, || 4 - Message Information</param>
        public void Message(System.String message, System.String header, int? type)
        {
            HtmlGenericControl h4 = new HtmlGenericControl("h4");

            if (type == null || type > 4 || type <= 0)
            {
                divMessage.Controls.Remove(h4);

                divMessage.Attributes.Add("class", "");
                divMessage.Visible = false;
                return;
            }
            else
            {
                divMessage.Visible = true;

                switch (type)
                {
                    case 1:         //warning
                        divMessage.Attributes.Add("class", "alert alert-block");
                        h4.InnerText = "Advertencia";
                        break;
                    case 2:         //error
                        divMessage.Attributes.Add("class", "alert alert-error");
                        h4.InnerText = "Algo esta mal";
                        break;
                    case 3:         //success
                        divMessage.Attributes.Add("class", "alert alert-success");
                        h4.InnerText = "¡Bien Hecho!";
                        break;
                    case 4:         //info
                        divMessage.Attributes.Add("class", "alert alert-info");
                        h4.InnerText = "¡Algo esta mal!";
                        break;
                    default:
                        divMessage.Attributes.Add("class", "");
                        h4.InnerText = "";
                        break;
                }

                divMessage.Controls.Add(h4);

                divMessage.InnerText = message;
            }
            upMessage.Update();
        }

        public void PersonInformation(System.String residentName, System.String residenceName, System.String gradeName)
        {
            if (residentName != string.Empty)
            {
                lblFullName.Text = residentName;
                lblResidence.Text = " || " + residenceName;
                lblGrade.Text = " || " + gradeName;
            }
            else
            {
                lblFullName.Text = string.Empty;
                lblResidence.Text = string.Empty;
                lblGrade.Text = string.Empty;
            }
            upPersonInfo.Update();
        }

        #endregion

        #region Buttons
        
        protected void txtSearchName_TextChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //clean list after save -> note

            if (EntityTypeId == (int)GeneralCommon.EntityTypes.Resident)
            {
                grvPersons.DataSource = ResResidents.GetByCenterId((int)((SysUserAccess)Session["access"]).CenterId, txtSearchName.Text);
                grvPersons.DataBind();
            }
            else
            {
                grvPersons.DataSource = GenPersons.GetPersonsByEntityType(EntityTypeId, (int)((SysUserAccess)Session["access"]).CenterId, null, txtSearchName.Text);
                grvPersons.DataBind();
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            //clean form
            MakeClean();
            grvPersons.SelectedIndex = -1;
        }
        /*
        protected void btnAddPhone_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(ddlPhoneTypes.SelectedValue) < 1 || txtPhone.Text == string.Empty || txtPhone.Text.Length < 10)
                {
                    divPhoneError.Visible = true;
                    return;
                }
                else
                    divPhoneError.Visible = false;

                //IList<GenGeneralPhones.Phone> AsignedPhones = (IList<GenGeneralPhones.Phone>)ViewState["tempPhones"];

                List<GenGeneralPhones.Phone> tempPhones = new List<GenGeneralPhones.Phone>();
                if (AsignedPhones != null)
                    tempPhones = AsignedPhones.ToList();

                GenGeneralPhones.Phone newPhone = new GenGeneralPhones.Phone();
                newPhone.phoneId = 0;
                newPhone.phoneNumber = txtPhone.Text;
                newPhone.phoneType = ddlPhoneTypes.SelectedItem.ToString();
                newPhone.phoneTypeId = Convert.ToInt32(ddlPhoneTypes.SelectedValue);

                foreach (GenGeneralPhones.Phone phoneInList in tempPhones.ToList())
                {
                    if (phoneInList.phoneTypeId == newPhone.phoneTypeId || phoneInList.phoneNumber == newPhone.phoneNumber)
                    {
                        return;
                    }
                }
                tempPhones.Add(newPhone);
                //ViewState["tempPhones"] = tempPhones;
                AsignedPhones = tempPhones;

                grvPhones.DataSource = tempPhones;
                grvPhones.DataBind();

                txtPhone.Text = string.Empty;
                ddlPhoneTypes.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void btnAddIdentification_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(ddlIndentificationTypes.SelectedValue) < 1 || txtIdentification.Text == string.Empty || txtExpirationDate.Text == string.Empty)
                {
                    divIdentificationError.Visible = true;
                    return;
                }
                else
                    divIdentificationError.Visible = false;

                if (Convert.ToDateTime(txtExpirationDate.Text).Date <= DateTime.Now.Date)
                {
                    Message("Fecha de expiración es inválida.", "", 2);
                    return;
                }

                int _generalId = 0;
                if (grvPersons.SelectedIndex > -1)
                    _generalId = (int)grvPersons.SelectedDataKey[0];

                if (GenGeneralIdentityDocuments.CompareIdentification(_generalId, txtIdentification.Text.ToString()))
                {
                    Message("El número de identificación >> " + txtIdentification.Text.ToString() + " << se encuentra enlazado a otra persona. Favor verificar y inténtelo de nuevo", "", 2);
                    //divMessage.Visible = true;
                    return;
                }

                //IList<GenGeneralIdentityDocuments.Identification> AsignedIdentifications = (IList<GenGeneralIdentityDocuments.Identification>)ViewState["tempIdentifications"];

                List<GenGeneralIdentityDocuments.Identification> tempIdentifications = new List<GenGeneralIdentityDocuments.Identification>();
                if (AsignedIdentifications != null)
                    tempIdentifications = AsignedIdentifications.ToList();

                GenGeneralIdentityDocuments.Identification newIdentification = new GenGeneralIdentityDocuments.Identification();
                newIdentification.identificationNumber = txtIdentification.Text;
                newIdentification.identificationType = ddlIndentificationTypes.SelectedItem.ToString();
                newIdentification.identificationTypeId = Convert.ToInt32(ddlIndentificationTypes.SelectedValue);
                newIdentification.expirationDate = Convert.ToDateTime(txtExpirationDate.Text);
                //newIdentification.path = "-";        

                foreach (GenGeneralIdentityDocuments.Identification identificationInList in tempIdentifications.ToList())
                {
                    if (identificationInList.identificationTypeId == newIdentification.identificationTypeId)
                    {
                        return;
                    }
                }
                tempIdentifications.Add(newIdentification);
                //ViewState["tempIdentifications"] = tempIdentifications;
                AsignedIdentifications = tempIdentifications;

                grvIdentifications.DataSource = tempIdentifications;
                grvIdentifications.DataBind();
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }
        */
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ////just for phones and documents
                if (Validations())
                    return;

                int GeneralEntityId = 0;

                GenGenerals GeneralEntity;
                if (grvPersons.SelectedIndex > -1)
                {
                    if (!ValidateResidentTransferred())
                        return;

                    if (SysUsers.IfExistsUserName(txtUserName.Text, Convert.ToInt32(grvPersons.SelectedDataKey[0])))
                    {
                        Message("Este Correo Electrónico se encuentra en uso por otra persona.", "", 2);
                        return;
                    }

                    GeneralEntityId = (int)grvPersons.SelectedDataKey[0];
                    GeneralEntity = GenGenerals.Get(GeneralEntityId);
                }
                else
                {
                    if (SysUsers.IfExistsUserName(txtUserName.Text, null))
                    {
                        Message("Este Correo Electrónico se encuentra en uso por otra persona.", "", 2);
                        return;
                    }
                    GeneralEntity = new GenGenerals();
                }

                if (ddlNationalities.SelectedValue != "0")
                    GeneralEntity.NationalityId = Convert.ToInt32(ddlNationalities.SelectedValue);
                //photo missing

                if (grvPersons.SelectedIndex > -1)
                {
                    GeneralEntity.Update();
                    if (txtUserName.Enabled == true)
                        SysUsers.UpdateUserName(Convert.ToInt32(grvPersons.SelectedDataKey[4]), txtUserName.Text, txtName.Text, (int)((SysUserAccess)Session["access"]).CenterId);
                }
                else
                {
                    GeneralEntityId = GeneralEntity.Insert();

                    //create user
                    SysUsers.CreateUser(GeneralEntityId, txtUserName.Text, txtName.Text, (int)((SysUserAccess)Session["access"]).CenterId, EntityTypeId);
                }

                //***||***\\
                GenPersons Person;
                GenGeneralAddresses Address;
                if (grvPersons.SelectedIndex > -1)
                {
                    Person = GenPersons.Get((int)grvPersons.SelectedDataKey[1]);
                    Address = GenGeneralAddresses.Get((int)grvPersons.SelectedDataKey[0]);
                }
                else
                {
                    Person = new GenPersons();
                    Address = new GenGeneralAddresses();
                }

                Person.GeneralId = GeneralEntityId;
                Person.Name = txtName.Text;
                Person.MiddleName = txtMiddleName.Text;
                Person.FirstLastName = txtFirstLastName.Text;
                Person.SecondLastName = txtSecondLastName.Text;
                Person.Birthday = Convert.ToDateTime(txtBirthday.Text);
                Person.GenderId = Convert.ToInt32(ddlGenders.SelectedValue);
                if (ddlMaritalStatuses.SelectedIndex != 0)
                    Person.MaritalStatusId = Convert.ToInt32(ddlMaritalStatuses.SelectedValue);
                else
                    Person.MaritalStatusId = null;

                IfExistsPhones(GeneralEntityId, txtHomePhone.Text, (int)GeneralCommon.PhoneTypes.HomePhone);
                IfExistsPhones(GeneralEntityId, txtCellPhone.Text, (int)GeneralCommon.PhoneTypes.CellPhone);
                
                Address.Address1 = txtAddress1.Text;
                Address.Address2 = txtAddress2.Text;
                Address.GeneralId = GeneralEntityId;
                Address.CityId = Convert.ToInt32(ddlCities.SelectedValue);
                Address.AddressStatus = true;

                IfExistsIdentifications(GeneralEntityId, txtIdentificationCard.Text, (int)GeneralCommon.IdentificationTypes.IdentificationCard, Convert.ToDateTime(txtIdentCardExDate.Text), ref fupIdentificationCard);
                if(txtPassport.Text!=string.Empty)
                    IfExistsIdentifications(GeneralEntityId, txtPassport.Text, (int)GeneralCommon.IdentificationTypes.Passport, Convert.ToDateTime(txtPassportExDate.Text), ref fupPassport);

                //// grv < 0 -> new entity
                if (grvEntities.Rows.Count > 0)
                {
                    foreach (GridViewRow row in grvEntities.Rows)
                    {
                        CheckBox chk = row.FindControl("chkEntity") as CheckBox;
                        if (Convert.ToInt32(grvEntities.DataKeys[row.RowIndex].Values["entityId"]) == EntityTypeId)
                            GenEntities.SetEntities(0, GeneralEntityId, EntityTypeId, (int)((SysUserAccess)Session["access"]).CenterId, chk.Checked, (int)((SysUserAccess)Session["access"]).UserCenterId, (int)((SysUserAccess)Session["access"]).UserCenterId);
                    }
                }
                else
                    GenEntities.SetEntities(0, GeneralEntityId, EntityTypeId, (int)((SysUserAccess)Session["access"]).CenterId, true, (int)((SysUserAccess)Session["access"]).UserCenterId, (int)((SysUserAccess)Session["access"]).UserCenterId);

                //coordinators
                if (EntityTypeId == (int)GeneralCommon.EntityTypes.Coordinator)
                {
                    CenterResidences = SysCenterResidences.GetByParentId(Convert.ToInt32(ddlResidences.SelectedValue), (int)((SysUserAccess)Session["access"]).CenterId);

                    foreach (SysCenterResidences.CenterResidences residenceInList in CenterResidences)
                    {
                        SysUserCenterResidences.IfExists(Convert.ToInt32(grvPersons.SelectedDataKey[4]), (int)((SysUserAccess)Session["access"]).CenterId, residenceInList.centerResidenceId);
                    }
                }

                if (grvPersons.SelectedIndex > -1)
                {
                    Person.ModifiedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                    Person.ModifiedDate = DateTime.Now;
                    Person.Update();

                    Address.ModifiedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                    Address.ModifiedDate = DateTime.Now;
                    Address.Update();

                    MakeClean();
                    btnSearch_Click(null, null);
                    Message("Los datos han sido guardados exitosamente.", "", 3);
                }
                else
                {
                    Person.Insert();

                    Address.CreatedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                    Address.CreatedDate = DateTime.Now;
                    Address.Insert();

                    if (EntityTypeId == (int)GeneralCommon.EntityTypes.Resident)
                        Response.Redirect("~/Modules/Residences/Forms/Resident.aspx?flag=1", true);
                    //else
                    //    Response.Redirect("~/Modules/Security/Forms/User.aspx?flag=1", true);
                }
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            MakeClean();
            grvPersons.SelectedIndex = -1;
        }

        protected void btnResendEmailConfirmation_Click(object sender, EventArgs e)
        {
            if (grvPersons.SelectedIndex > -1 && txtUserName.Enabled == true)
            {
                string confirmationCode = SysUtilities.CreateConfirmationCode();
                string fullName = grvPersons.SelectedDataKey[2].ToString();

                SysUsers cUser = SysUsers.Get(Convert.ToInt32(grvPersons.SelectedDataKey[4]));
                Random random = new Random();
                int randomNumber = random.Next(0, 99999) + 100000;
                cUser.UserPassword = (randomNumber - 50000).ToString();
                cUser.ConfirmationCode = confirmationCode;
                cUser.UserStatus = false;
                cUser.Update();

                GenEMails.ReSendConfirmation(txtUserName.Text, fullName, confirmationCode, (int)((SysUserAccess)Session["access"]).CenterId);

                Message("¡Confirmación enviada con éxito!", "", 3);
            }
        }

        protected void btnResetPassword_Click(object sender, EventArgs e)
        {

        }
        #endregion

        #region DropDownLists

        protected void ddlStates_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillCities(Convert.ToInt32(ddlStates.SelectedValue));
            uppanelAddress.Update();
        }

        #endregion

        #region GridViews

        protected void grvPersons_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                MakeClean();
                Message("", "", 0);
                GenPersonInfo PersonInfo = GenPersonInfo.Get(Convert.ToInt32(grvPersons.SelectedDataKey[1]));
                txtName.Text = PersonInfo.Name;
                txtMiddleName.Text = PersonInfo.MiddleName;
                txtFirstLastName.Text = PersonInfo.FirstLastName;
                txtSecondLastName.Text = PersonInfo.SecondLastName;
                txtBirthday.Text = PersonInfo.Birthday.ToString();
                ddlGenders.SelectedValue = PersonInfo.GenderId.ToString();
                if (PersonInfo.MaritalStatusId != null)
                    ddlMaritalStatuses.SelectedValue = PersonInfo.MaritalStatusId.ToString();

                PersonInformation(PersonInfo.Name + " " + PersonInfo.MiddleName + " " + PersonInfo.FirstLastName + " " + PersonInfo.SecondLastName, "", "");

                AsignedPhones = GenGeneralPhones.GetByGeneralId(Convert.ToInt32(grvPersons.SelectedDataKey[0]));
                foreach (GenGeneralPhones.Phones phoneInList in AsignedPhones)
                {
                    switch (phoneInList.phoneTypeId)
                    {
                        case (int)GeneralCommon.PhoneTypes.HomePhone:
                            txtHomePhone.Text = phoneInList.phoneNumber;
                            break;
                        case (int)GeneralCommon.PhoneTypes.CellPhone:
                            txtCellPhone.Text = phoneInList.phoneNumber;
                            break;
                    }
                }
                //grvPhones.DataSource = AsignedPhones;
                //grvPhones.DataBind();

                SysUsers UserInfo = SysUsers.Get(Convert.ToInt32(grvPersons.SelectedDataKey[4]));
                txtUserName.Text = UserInfo.UserName;
                if (UserInfo.ConfirmationCode == null)
                {
                    txtUserName.Enabled = false;
                    btnResendEmailConfirmation.Visible = false;
                    //btnResetPassword.Visible = false;
                }
                else
                {
                    txtUserName.Enabled = true;
                    btnResendEmailConfirmation.Visible = true;
                    //btnResetPassword.Visible = true;
                }

                txtAddress1.Text = PersonInfo.Address1;
                txtAddress2.Text = PersonInfo.Address2;
                ddlStates.SelectedValue = PersonInfo.StateId.ToString();
                FillCities((int)PersonInfo.StateId);
                ddlCities.SelectedValue = PersonInfo.CityId.ToString();
                if (PersonInfo.NationalityId != null)
                    ddlNationalities.SelectedValue = PersonInfo.NationalityId.ToString();
                else
                    ddlNationalities.SelectedValue = "0";
                uppanelAddress.Update();

                AsignedIdentifications = GenGeneralIdentityDocuments.GetByGeneralId(Convert.ToInt32(grvPersons.SelectedDataKey[0]));
                foreach (GenGeneralIdentityDocuments.Identifications identificationInList in AsignedIdentifications)
                {
                    switch (identificationInList.identificationTypeId)
                    {
                        case (int)GeneralCommon.IdentificationTypes.IdentificationCard:
                            txtIdentificationCard.Text = identificationInList.identificationNumber;
                            txtIdentCardExDate.Text = identificationInList.expirationDate.ToString();
                            GenUtilities.SetImagenToPopUp(ref imgIdentificationCard, GeneralCommon.TypeFilePaths.Identification, identificationInList.identificationPath);
                            break;
                        case (int)GeneralCommon.IdentificationTypes.Passport:
                            txtPassport.Text = identificationInList.identificationNumber;
                            txtIdentCardExDate.Text = identificationInList.expirationDate.ToString();
                            GenUtilities.SetImagenToPopUp(ref imgPassport, GeneralCommon.TypeFilePaths.Identification, identificationInList.identificationPath);
                            break;
                    }
                }
                //grvIdentifications.DataSource = AsignedIdentifications;
                //grvIdentifications.DataBind();

                grvEntities.DataSource = GenEntities.GetByGeneralId(Convert.ToInt32(grvPersons.SelectedDataKey[0]), (int)((SysUserAccess)Session["access"]).CenterId);
                grvEntities.DataBind();

                if (EntityTypeId == (int)GeneralCommon.EntityTypes.Coordinator)
                {
                    ddlResidences.SelectedValue = SysUserCenterResidences.GetParentByUserId(Convert.ToInt32(grvPersons.SelectedDataKey[4]), (int)((SysUserAccess)Session["access"]).CenterId).ToString();
                }

                HideOptionsInEntities();

                ValidateResidentTransferred();
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void grvPersons_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvPersons.PageIndex = e.NewPageIndex;
            grvPersons.SelectedIndex = -1;
            btnSearch_Click(null, null);
        }

        /*
        protected void grvPersons_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (e.CommandName == "Selecting")
                //{

                MakeClean();

                GenPersonInfo PersonInfo = GenPersonInfo.Get(Convert.ToInt32(grvPersons.SelectedDataKey[1]));
                txtName.Text = PersonInfo.Name;
                txtMiddleName.Text = PersonInfo.MiddleName;
                txtFirstLastName.Text = PersonInfo.FirstLastName;
                txtSecondLastName.Text = PersonInfo.SecondLastName;
                txtBirthday.Text = PersonInfo.Birthday.ToString();
                ddlGenders.SelectedValue = PersonInfo.GenderId.ToString();
                if (PersonInfo.MaritalStatusId != null)
                    ddlMaritalStatuses.SelectedValue = PersonInfo.MaritalStatusId.ToString();

                AsignedPhones = GenGeneralPhones.GetByGeneralId(Convert.ToInt32(grvPersons.SelectedDataKey[1]));
                grvPhones.DataSource = AsignedPhones;
                grvPhones.DataBind();

                txtAddress1.Text = PersonInfo.Address1;
                txtAddress2.Text = PersonInfo.Address2;
                ddlStates.SelectedValue = PersonInfo.StateId.ToString();
                FillCities((int)PersonInfo.StateId);
                ddlCities.SelectedValue = PersonInfo.CityId.ToString();
                ddlNationalities.SelectedValue = PersonInfo.NationalityId.ToString();

                AsignedIdentifications = GenGeneralIdentityDocuments.GetByGeneralId(Convert.ToInt32(grvPersons.SelectedDataKey[1]));
                grvIdentifications.DataSource = AsignedIdentifications;
                grvIdentifications.DataBind();
            }
            //}
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void grvPhones_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Editing")
            {
                IList<GenGeneralPhones.Phone> asignedPhoneList = AsignedPhones;

                foreach (GenGeneralPhones.Phone phoneInList in asignedPhoneList.ToList())
                {
                    if (phoneInList.phoneTypeId == Convert.ToInt32(e.CommandArgument))
                    {
                        txtPhone.Text = phoneInList.phoneNumber;
                        ddlPhoneTypes.SelectedValue = phoneInList.phoneTypeId.ToString();
                        return;
                    }
                }
            }

            if (e.CommandName == "Deleting")
            {
                IList<GenGeneralPhones.Phone> asignedPhoneList = AsignedPhones;

                foreach (GenGeneralPhones.Phone phoneInList in asignedPhoneList.ToList())
                {
                    if (phoneInList.phoneTypeId == Convert.ToInt32(e.CommandArgument))
                    {
                        asignedPhoneList.Remove(phoneInList);
                        divPhoneError.Visible = false;
                        grvPhones.DataSource = asignedPhoneList;
                        grvPhones.DataBind();
                        return;
                    }
                }
            }
        }

        protected void grvIdentifications_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Editing")
            {
                IList<GenGeneralIdentityDocuments.Identification> asignedIdentificatioList = AsignedIdentifications; //= (IList<GenGeneralIdentityDocuments.Identification>)ViewState["tempIdentifications"];

                foreach (GenGeneralIdentityDocuments.Identification identificationInList in asignedIdentificatioList.ToList())
                {
                    if (identificationInList.identificationTypeId == Convert.ToInt32(e.CommandArgument))
                    {
                        txtIdentification.Text = identificationInList.identificationNumber;
                        ddlIndentificationTypes.SelectedValue = identificationInList.identificationTypeId.ToString();
                        txtExpirationDate.Text = identificationInList.expirationDate.ToString();
                        return;
                    }
                }
            }
            if (e.CommandName == "Deleting")
            {
                IList<GenGeneralIdentityDocuments.Identification> asignedIdentificationList = AsignedIdentifications;// (IList<GenGeneralIdentityDocuments.Identification>)ViewState["tempIdentifications"];

                foreach (GenGeneralIdentityDocuments.Identification identificationInList in asignedIdentificationList.ToList())
                {
                    if (identificationInList.identificationTypeId == Convert.ToInt32(e.CommandArgument))
                    {
                        asignedIdentificationList.Remove(identificationInList);
                        divIdentificationError.Visible = false;
                        grvIdentifications.DataSource = asignedIdentificationList;
                        grvIdentifications.DataBind();
                        return;
                    }
                }
            }
        }
        */
        #endregion
    }
}