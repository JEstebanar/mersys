﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="wucGeneralRegister.ascx.cs"
    Inherits="Presentation.Modules.General.UserControls.wucGeneralRegister" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!-- Le styles -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../../../bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="../../../bootstrap/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="../../../medicenter/style/reset.css" />
<link rel="stylesheet" type="text/css" href="../../../medicenter/style/superfish.css" />
<link rel="stylesheet" type="text/css" href="../../../medicenter/style/style.css" />
<link rel="stylesheet" type="text/css" href="../../../medicenter/style/responsive.css" />
<link rel="shortcut icon" href="../../../medicenter/images/favicon.ico" />
<link rel="stylesheet" href="../../../Styles/Site.css" type="text/css" />

<!-- end styles -->
<div class="control-group">
    <asp:UpdatePanel runat="server" ID="upPersonInfo" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="form-inline">
                <strong>
                    <asp:Label ID="lblFullName" runat="server"></asp:Label>
                    <asp:Label ID="lblResidence" runat="server"></asp:Label>
                    <asp:Label ID="lblGrade" runat="server"></asp:Label></strong>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<asp:UpdatePanel runat="server" ID="upMessage" UpdateMode="Conditional">
    <ContentTemplate>
        <div id="divMessage" runat="server" class="alert alert-block" visible="false">
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="accordion" id="Accordion">
    <div class="accordion-group">
        <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                Personas </a>
        </div>
        <div id="collapseOne" class="accordion-body collapse ">
            <div class="accordion-inner">
                <asp:UpdatePanel ID="upanelSearch" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <div class="control-group">
                            <div class="form-search">
                                <asp:TextBox ID="txtSearchName" runat="server" CssClass="textEntry" placeholder="Nombre"
                                    AutoPostBack="True" OnTextChanged="txtSearchName_TextChanged"></asp:TextBox>
                                <asp:Button ID="btnSearch" runat="server" Text="Buscar" class="btn" OnClick="btnSearch_Click" />
                                <asp:Button ID="btnCreate" runat="server" Text="Crear Nuevo" class="btn btn-info"
                                    OnClick="btnCreate_Click" />
                            </div>
                        </div>
                        <div class="control-group">
                            <asp:GridView ID="grvPersons" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                                AutoGenerateColumns="False" DataKeyNames="generalId,personId,fullName,residentId,userId"
                                ShowHeader="False" 
                                OnSelectedIndexChanged="grvPersons_SelectedIndexChanged" AllowPaging="True" 
                                onpageindexchanging="grvPersons_PageIndexChanging" PageSize="7">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="personSelect" runat="server" CommandArgument='<%# Eval("personId") %>'
                                                CommandName="Select" ToolTip="Seleccionar" Text='<%# Eval("fullName") %>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="txtSearchName" EventName="TextChanged" />
                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                        <asp:PostBackTrigger ControlID="btnCreate" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</div>
<asp:ValidationSummary ID="ResidentValidationSummary" runat="server" CssClass="alert alert-danger"
    ValidationGroup="ResidentValidationGroup" />
<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#Personals" data-toggle="tab">Datos Personales</a></li>
        <li><a href="#Localization" data-toggle="tab">Localización</a></li>
        <li><a href="#Identification" data-toggle="tab">Identificación</a></li>
        <li><a href="#Entities" data-toggle="tab">Entidades</a></li>
    </ul>
    <div class="tab-content">
        <div id="Personals" class="tab-pane active">
            <asp:UpdatePanel ID="upanelPersonal" runat="server">
                <ContentTemplate>
                    <fieldset>
                        <legend>Datos Personales</legend>
                        <div class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">
                                    Nombre</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtName" runat="server" placeholder=""></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="NameRequired" runat="server" ControlToValidate="txtName"
                                        CssClass="failureNotification" ErrorMessage="Nombre es requerido." ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Segundo Nombre</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtMiddleName" runat="server" placeholder=""></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Primer Apellido</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtFirstLastName" runat="server" placeholder=""></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="txtFirstLastNameRequired" runat="server" ControlToValidate="txtFirstLastName"
                                        CssClass="failureNotification" ErrorMessage="Apellido es requerido." ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Segundo Apellido</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtSecondLastName" runat="server" placeholder=""></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Fecha de Nacimiento</label>
                                <div class="controls">
                                    <div class="input-append">
                                        <asp:TextBox ID="txtBirthday" runat="server" size="16" type="text" value=""></asp:TextBox>
                                        <asp:MaskedEditExtender ID="MaskedEditExtendertxtBirthday" runat="server" TargetControlID="txtBirthday"
                                            MaskType="Date" Mask="99/99/9999">
                                        </asp:MaskedEditExtender>
                                        <asp:CalendarExtender ID="CalendarExtendertxtBirthday" runat="server" TargetControlID="txtBirthday"
                                            PopupButtonID="calendartxtBirthday">
                                        </asp:CalendarExtender>
                                        <span class="add-on"><i id="calendartxtBirthday" class="icon-calendar"></i></span>
                                    </div>
                                    <asp:RequiredFieldValidator ID="txtBirthdayRequired" runat="server" ControlToValidate="txtBirthday"
                                        CssClass="failureNotification" ErrorMessage="Fecha de Nacimiento es requerida."
                                        ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Sexo</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlGenders" runat="server">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="ddlGendersRequired" runat="server" ControlToValidate="ddlGenders"
                                        CssClass="failureNotification" ErrorMessage="Sexo es requerido." ValidationGroup="ResidentValidationGroup"
                                        InitialValue="0">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Estado Civil</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlMaritalStatuses" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="Localization" class="tab-pane">
            <asp:UpdatePanel ID="upanelLocalization" runat="server">
                <ContentTemplate>
                    <fieldset>
                        <legend>Teléfonos</legend>
                        <div id="divPhoneError" class="alert alert-error" runat="server" visible="false">
                            <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                            <h4>
                                Algo esta mal!</h4>
                            <label>
                                Complete los campos de identificación personal y inténtelo de nuevo ...</label>
                        </div>
                        <div class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">
                                    Casa</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtHomePhone" runat="server" MaxLength="10"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="txtHomePhoneFilteredTextBoxExtender" runat="server"
                                        TargetControlID="txtHomePhone" ValidChars="0123456789">
                                    </asp:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Celular</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtCellPhone" runat="server" MaxLength="10"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="txtCellPhoneFilteredTextBoxExtender" runat="server"
                                        TargetControlID="txtCellPhone" ValidChars="0123456789">
                                    </asp:FilteredTextBoxExtender>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="uppanelAddress" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <fieldset>
                        <legend>Direccón</legend>
                        <div class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">
                                    Correo Electrónico</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtUserName" runat="server" ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="txtUserNameRequired" runat="server" ControlToValidate="txtUserName"
                                        CssClass="failureNotification" ErrorMessage="Correo Electrónico es requerido."
                                        ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                                    <asp:Button ID="btnResendEmailConfirmation" runat="server" 
                                        Text="Re Enviar Confirmación" class="btn btn-success" 
                                        onclick="btnResendEmailConfirmation_Click" />
                                    <asp:Button ID="btnResetPassword" runat="server" Text="Cambiar Contraseña" 
                                        class="btn btn-warning" onclick="btnResetPassword_Click" Visible="false" />
                                </div>
                                <div class="controls">
                                    <span class="help-inline">Este será el Correo Electrónico con el cual tendra acceso.</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Dirección 1</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddress1" runat="server" placeholder=""></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="txtAddress1Required" runat="server" ControlToValidate="txtAddress1"
                                        CssClass="failureNotification" ErrorMessage="Dirección es requerida." ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Dirección 2</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddress2" runat="server" placeholser=""></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Provincia</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlStates" runat="server" OnSelectedIndexChanged="ddlStates_SelectedIndexChanged"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="ddlStatesRequired" runat="server" ControlToValidate="ddlStates"
                                        CssClass="failureNotification" ErrorMessage="Provincia es requerida." ValidationGroup="ResidentValidationGroup"
                                        InitialValue="0">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Ciudad</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlCities" runat="server">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="ddlCitiesRequired" runat="server" ControlToValidate="ddlCities"
                                        CssClass="failureNotification" ErrorMessage="Ciudad es requerida." ValidationGroup="ResidentValidationGroup"
                                        InitialValue="0">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Nacionalidad</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlNationalities" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlStates" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div id="Identification" class="tab-pane">
            <asp:UpdatePanel ID="upanelIdentification" runat="server">
                <ContentTemplate>
                    <fieldset>
                        <legend>Identificación Personal</legend>
                        <div id="divIdentificationError" class="alert alert-error" runat="server" visible="false">
                            <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                            <h4>
                                Algo esta mal!</h4>
                            <label>
                                Complete los campos de teléfono y inténtelo de nuevo ...</label>
                        </div>
                        <div class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">
                                    Cédula</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtIdentificationCard" runat="server" MaxLength="11"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="txtIdentificationCardFilter" runat="server" TargetControlID="txtIdentificationCard"
                                        ValidChars="0123456789">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="txtIdentificationCardRequired" runat="server" ControlToValidate="txtIdentificationCard"
                                        CssClass="failureNotification" ErrorMessage="Cédula es Requerida." ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Fecha de Vencimiento</label>
                                <div class="controls">
                                    <div class="input-append">
                                        <asp:TextBox ID="txtIdentCardExDate" runat="server" size="16" type="text" value=""></asp:TextBox>
                                        <asp:MaskedEditExtender ID="MaskedEditExtendertxtIdentCardExDate" runat="server"
                                            TargetControlID="txtIdentCardExDate" MaskType="Date" Mask="99/99/9999">
                                        </asp:MaskedEditExtender>
                                        <asp:CalendarExtender ID="CalendarExtendertxtIdentCardExDate" runat="server" TargetControlID="txtIdentCardExDate"
                                            PopupButtonID="calendartxtIdentCardExDate">
                                        </asp:CalendarExtender>
                                        <span class="add-on"><i id="calendartxtIdentCardExDate" class="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <asp:RequiredFieldValidator ID="txtIdentCardExDateRequired" runat="server" ControlToValidate="txtIdentCardExDate"
                                        CssClass="failureNotification" ErrorMessage="Fecha de Vencimiento para la Cédula es Requerida."
                                        ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Adjuntar</label>
                                <div class="controls form-inline">
                                    <asp:FileUpload ID="fupIdentificationCard" runat="server" />
                                    <a id="showIdentificationCard" runat="server" href="#myModal" role="button" class="btn btn-link"
                                        data-toggle="modal">Ver</a>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Pasaporte</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtPassport" runat="server"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="txtPassportFilteredTextBoxExtender" runat="server"
                                        TargetControlID="txtPassport" ValidChars="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz">
                                    </asp:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Fecha de Vencimiento</label>
                                <div class="controls">
                                    <div class="input-append">
                                        <asp:TextBox ID="txtPassportExDate" runat="server" size="16" type="text" value=""></asp:TextBox>
                                        <asp:MaskedEditExtender ID="MaskedEditExtendertxtPassportExDate" runat="server" TargetControlID="txtPassportExDate"
                                            MaskType="Date" Mask="99/99/9999">
                                        </asp:MaskedEditExtender>
                                        <asp:CalendarExtender ID="CalendarExtendertxtPassportExDate" runat="server" TargetControlID="txtPassportExDate"
                                            PopupButtonID="calendartxtPassportExDate">
                                        </asp:CalendarExtender>
                                        <span class="add-on"><i id="calendartxtPassportExDate" class="icon-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Adjuntar</label>
                                <div class="controls form-inline">
                                    <asp:FileUpload ID="fupPassport" runat="server" />
                                    <a id="showPassport" runat="server" href="#myModal" role="button" class="btn btn-link"
                                        data-toggle="modal">Ver</a>
                                </div>
                            </div>
                        </div>
                        <!-- Modal -->
                        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                            aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    ×</button>
                                <h3 id="myModalLabel">
                                    Documento</h3>
                            </div>
                            <div class="modal-body">
                                <asp:Image ID="imgIdentificationCard" runat="server" />
                                <asp:Image ID="imgPassport" runat="server" />
                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                    </fieldset>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="Entities" class="tab-pane">
            <asp:UpdatePanel ID="upanelentities" runat="server">
                <ContentTemplate>
                    <fieldset>
                        <legend>Entidades</legend>
                        <div class="form-horizontal">
                            <div id="divResidences" runat="server" class="control-group">
                                <label class="control-label">
                                    Residencia</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlResidences" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <asp:GridView ID="grvEntities" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                                    AutoGenerateColumns="False" DataKeyNames="entityId,entityStatus">
                                    <Columns>
                                        <asp:BoundField DataField="entityName" HeaderText="Entidades" ReadOnly="True" SortExpression="entityName" />
                                        <%--<asp:BoundField DataField="entityStatus" FooterText="Estatus" ReadOnly="True" SortExpression="entityStatus" />--%>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkEntity" runat="server" Checked='<%# Eval("entityStatus") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>

                        </div>
                    </fieldset>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
<div class="form-actions">
    <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnSave_Click"
        ValidationGroup="ResidentValidationGroup" />
    <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
</div>
