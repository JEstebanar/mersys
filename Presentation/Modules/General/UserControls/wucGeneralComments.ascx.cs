﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using General.Comments;
using General.GeneralCommons;
using SystemSecurity.SysUsers;

namespace Presentation.Modules.General.UserControls
{
    public partial class wucComments : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Methods

        public void FillCommentsByProcessId(int processId, GeneralCommon.CommentProcessType processTypeId)
        {
            CleanForm();
            grvComments.DataSource = GenComments.GetByProcessId(processId, processTypeId);
            grvComments.DataBind();
        }

        public void SaveCommentByProcessId(int processId, GeneralCommon.CommentProcessType processTypeId)
        {
            if (txtComment.Text != string.Empty)
            {
                GenComments nComment = new GenComments();
                nComment.ProcessId = processId;
                nComment.ProcessTypeId = (int)processTypeId;
                nComment.Comment = txtComment.Text;
                nComment.GeneralId = ((SysUsers)Session["user"]).GeneralId;
                nComment.CreatedDate = DateTime.Now;
                nComment.Insert();
            }
            CleanForm();
        }

        public void CleanForm()
        {
            grvComments.DataSource = null;
            grvComments.DataBind();

            txtComment.Text = string.Empty;
        }
        #endregion
    }
}