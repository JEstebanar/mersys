﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using General.GeneralCommons;
using General.Utilities;
using Residences.Residents;
using Residences.ResidentStatusComments;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Residences.Forms
{
    public partial class Transfers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.Transfers))
                    {
                        if (!IsPostBack)
                        {
                            FillResidents();
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        #region Methods
        private void FillResidents()
        {
            grvResidents.DataSource = ResResidents.GetToTransfer((int)((SysUserAccess)Session["access"]).CenterId);
            grvResidents.DataBind();
            //grvResidents.SelectedIndex = -1;
            divDetail.Visible = false;
        }
        #endregion

        #region Buttons
        protected void btnSave_Click(object sender, EventArgs e)
        {   
            if (grvResidents.SelectedIndex > -1)
            {
                if (txtComment.Text == string.Empty)
                {
                    Master.Master.Message("Debe de comentar para terminar la transferencia.", "", 2);
                    return;
                }

                ResResidents.Transfer(Convert.ToInt32(grvResidents.SelectedDataKey[0]), (int)((SysUserAccess)Session["access"]).UserCenterId);

                ResResidentStatusComments nComment = new ResResidentStatusComments();
                nComment.ResidentId = Convert.ToInt32(grvResidents.SelectedDataKey[0]);
                nComment.StatusId = (int)GeneralCommon.ResidentStatuses.Active;
                nComment.Comment = txtComment.Text;
                nComment.CreatedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                nComment.CreatedDate = DateTime.Now;
                nComment.Insert();

                FillResidents();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ResResidents.SetTransfer(Convert.ToInt32(grvResidents.SelectedDataKey[0]), false);
            FillResidents();
        }
        #endregion

        protected void grvResidents_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtResidentName.Text = grvResidents.SelectedDataKey[1].ToString();
            txtResidence.Text = grvResidents.SelectedDataKey[2].ToString();
            txtGrade.Text = grvResidents.SelectedDataKey[3].ToString();
            txtCenter.Text = grvResidents.SelectedDataKey[4].ToString();
            //imgDocument.ImageUrl = GenForms.GetTransferDocumentsPath() + grvResidents.SelectedDataKey[5].ToString();
            GenUtilities.SetImagenToPopUp(ref imgDocument, GeneralCommon.TypeFilePaths.TransferDocuments, grvResidents.SelectedDataKey[5].ToString());
            grvComments.DataSource = ResResidentStatusComments.GetByResidentId((int)grvResidents.SelectedDataKey[0], (int)GeneralCommon.ResidentStatuses.Transferred);
            grvComments.DataBind();
            divDetail.Visible = true;
        }
    }
}