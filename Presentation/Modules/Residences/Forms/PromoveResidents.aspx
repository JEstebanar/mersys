﻿<%@ Page Title="Promover" Language="C#" MasterPageFile="~/Modules/Residences/Residences.master"
    AutoEventWireup="true" CodeBehind="PromoveResidents.aspx.cs" Inherits="Presentation.Modules.Residences.Forms.ChangeResidentsStatus" %>

<%@ MasterType VirtualPath="~/Modules/Residences/Residences.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Promever Médicos Residentes</legend>
        <div class="form-search">
            <asp:DropDownList ID="ddlTeachingYears" runat="server">
            </asp:DropDownList>
            <asp:DropDownList ID="ddlResidences" runat="server">
            </asp:DropDownList>
            <asp:DropDownList ID="ddlGrades" runat="server">
            </asp:DropDownList>
            <asp:Button ID="btnSearch" runat="server" Text="Buscar" class="btn btn-info" OnClick="btnSearch_Click" />
        </div>
        <br />
        <div class="control-group">
            <asp:GridView ID="grvResidents" runat="server" class="table table-condensed table-striped table-bordered"
                AutoGenerateColumns="False" DataKeyNames="residentId">
                <Columns>
                    <asp:BoundField DataField="residentName" HeaderText="Residente" SortExpression="residentName" />
                    <asp:BoundField DataField="gradeName" HeaderText="Año" SortExpression="gradeName" />
                    <asp:TemplateField SortExpression="selected">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkResidentHeader" runat="server"
                                AutoPostBack="true" oncheckedchanged="chkResidentHeader_CheckedChanged" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkResident" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </fieldset>
    <div class="form-actions">
        <asp:Button ID="btnSave" runat="server" Text="Promover" class="btn btn-warning" 
            onclick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" 
            onclick="btnCancel_Click" />
    </div>
</asp:Content>
