﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using General.GeneralCommons;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Residences.Forms
{
    public partial class ResidentRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.ResidentRegister))
                    {
                        if (!IsPostBack)
                        {
                            wucGeneralRegister1.ModuleId = (int)GeneralCommon.Modules.Residences;
                            wucGeneralRegister1.ScreenId = (int)GeneralCommon.SystemScreens.ResidentRegister;
                            wucGeneralRegister1.EntityTypeId = (int)GeneralCommon.EntityTypes.Resident;
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        //protected void btnCreateResident_Click(object sender, EventArgs e)
        //{
        //    //clean form
        //    MadeClean();


        //}

        //private void MadeClean()
        //{

        //}

        //private void Initialize()
        //{
        //    ////personal information
        //    ddlGenders.DataTextField = "genderName";
        //    ddlGenders.DataValueField = "genderId";
        //    ddlGenders.DataSource = GenGenders.GetGenders();
        //    ddlGenders.DataBind();
        //    ddlGenders.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

        //    ddlMaritalStatuses.DataTextField = "maritalStatusName";
        //    ddlMaritalStatuses.DataValueField = "maritalStatusId";
        //    ddlMaritalStatuses.DataSource = GenMaritalStatus.GetMaritalStatuses();
        //    ddlMaritalStatuses.DataBind();
        //    ddlMaritalStatuses.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

        //    ////localization
        //    ddlPhoneTipes.DataTextField = "typeName";
        //    ddlPhoneTipes.DataValueField = "typeId";
        //    ddlPhoneTipes.DataSource = GenPhoneTypes.GetPhoneTypes();
        //    ddlPhoneTipes.DataBind();
        //    ddlPhoneTipes.Items.Insert(0, new ListItem("-- Seleccione --", "0"));            

        //    ddlStates.DataTextField = "stateName";
        //    ddlStates.DataValueField = "stateId";
        //    ddlStates.DataSource = GenStates.GetStatesByCountryId(null);
        //    ddlStates.DataBind();
        //    ddlStates.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

        //    ddlNationality.DataTextField = "nationalityName";
        //    ddlNationality.DataValueField = "nationalityId";
        //    ddlNationality.DataSource = GenNationalities.GetNationalities();
        //    ddlNationality.DataBind();
        //    ddlNationality.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

        //    ////Identification
        //    ddlIndentificationTypes.DataTextField = "TypeName";
        //    ddlIndentificationTypes.DataValueField = "TypeId";
        //    ddlIndentificationTypes.DataSource = GenIdentityDocumentTypes.GetIdentityDocumentTypeByEntityTypeId((int)GeneralCommon.EntityTypes.Resident, 1);
        //    ddlIndentificationTypes.DataBind();
        //    ddlIndentificationTypes.Items.Insert(0, new ListItem("-- Seleccione --", "0"));
        //}

        //protected void ddlStates_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ddlCities.DataTextField = "cityName";
        //    ddlCities.DataValueField = "cityId";
        //    ddlCities.DataSource = GenCities.GetCitiesByStateId(Convert.ToInt32(ddlStates.SelectedValue));
        //    ddlCities.DataBind();
        //    ddlCities.Items.Insert(0, new ListItem("-- Seleccione --", "0"));
        //}

        //protected void btnAddIdentification_Click(object sender, EventArgs e)
        //{

        //}

        //protected void btnAddPhone_Click(object sender, EventArgs e)
        //{
        //    ddlCities.DataTextField = "cityName";
        //    ddlCities.DataValueField = "cityId";
        //    ddlCities.DataSource = GenCities.GetCitiesByStateId(Convert.ToInt32(ddlStates.SelectedValue));
        //    ddlCities.DataBind();
        //    ddlCities.Items.Insert(0, new ListItem("-- Seleccione --", "0"));
        //}
    }
}

