﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.UI.WebControls;
using General.Entities;
using General.GeneralCommons;
using General.GeneralPhones;
using General.Generals;
using General.PersonEmergencyContacts;
using General.Persons;
using General.Utilities;
using Modules.General.RelationshipTypes;
using Modules.Residences.Departments;
using Modules.Residences.ResidentDocuments;
using Modules.Residences.Rotations;
using Modules.Residences.RotationTypes;
using Modules.Residences.Universities;
using Residences.Grades;
using Residences.Residences;
using Residences.ResidentInfo;
using Residences.Residents;
using Residences.ResidentStatusComments;
using Residences.Transfers;
using SystemSecurity.Centers;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Residences.Forms
{
    public partial class Resident : System.Web.UI.Page
    {
        //private IList<ResResidents.Especialties> AsignedEspecialties { get { return (IList<ResResidents.Especialties>)ViewState["asignedEspecialties"]; } set { ViewState["asignedEspecialties"] = value; } }
        //private IList<ResResidents.Especialties> AsignedSubEspecialties { get { return (IList<ResResidents.Especialties>)ViewState["asignedSubEspecialties"]; } set { ViewState["asignedSubEspecialties"] = value; } }
        private IList<ResRotations.Rotations> AsignedRotations { get { return (IList<ResRotations.Rotations>)ViewState["asignedRotations"]; } set { ViewState["asignedRotations"] = value; } }
        //private IList<ResRotations.Rotations> DeletedRotations { get { return (IList<ResRotations.Rotations>)ViewState["deletedRotations"]; } set { ViewState["deletedRotations"] = value; } }
        private IList<GenGeneralPhones.Phones> AsignedPhones { get { return (IList<GenGeneralPhones.Phones>)ViewState["asignedPhones"]; } set { ViewState["asignedPhones"] = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.Resident))
                    {
                        if (!IsPostBack)
                        {
                            Initialize();
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        #region Methods
        private void Initialize()
        {
            ddlUniversities.DataTextField = "universityName";
            ddlUniversities.DataValueField = "universityId";
            ddlUniversities.DataSource = ResUniversities.GetUniversities();
            ddlUniversities.DataBind();
            ddlUniversities.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

            ////especialty  
            ddlResidences.Items.Clear();
            ddlResidences.DataTextField = "residenceName";
            ddlResidences.DataValueField = "residenceId";
            ddlResidences.DataSource = ResResidences.GetResidencesByGrade(null, null, (int)((SysUserAccess)Session["access"]).CenterId, (int)((SysUserAccess)Session["access"]).TeachingYearId);
            ddlResidences.DataBind();
            ddlResidences.Items.Insert(0, new ListItem("-- Seleccione Residencia --", "0")); 

            ddlGrade.DataTextField = "gradeName";
            ddlGrade.DataValueField = "gradeId";
            ddlGrade.DataSource = ResGrades.GetGrades();
            ddlGrade.DataBind();
            ddlGrade.Items.Insert(0, new ListItem("-- Seleccione Grado --", "0"));

            ////centers
            ddlRotationCenters.DataTextField = "centerName";
            ddlRotationCenters.DataValueField = "centerId";
            ddlRotationCenters.DataSource = SysCenters.GetCenters();
            ddlRotationCenters.DataBind();
            ddlRotationCenters.Items.Insert(0, new ListItem("-- Extranjero --", "0"));

            ////department
            ddlDepartments.DataTextField = "departmentName";
            ddlDepartments.DataValueField = "departmentId";
            ddlDepartments.DataSource = ResDepartments.GetDepartments();
            ddlDepartments.DataBind();
            ddlDepartments.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

            ////rotation tipe
            ddlRotationType.DataTextField = "typeName";
            ddlRotationType.DataValueField = "typeId";
            ddlRotationType.DataSource = ResRotationTypes.GetRotationTypes();
            ddlRotationType.DataBind();
            ddlRotationType.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

            ////emergency info
            ddlRelationship.DataTextField = "typeName";
            ddlRelationship.DataValueField = "typeId";
            ddlRelationship.DataSource = GenPersonRelationshipType.GetRelationshipTypes();
            ddlRelationship.DataBind();
            ddlRelationship.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

            //status 
            ddlStatus.Items.Clear();
            ddlStatus.DataTextField = "statusName";
            ddlStatus.DataValueField = "statusId";
            ddlStatus.DataSource = ResResidents.GetStatus();
            ddlStatus.DataBind();
            ddlStatus.Items.Insert(0, new ListItem("-- Seleccione un Estado --", "0"));

            ////centers *upgrade perform
            ddlTransferCenters.DataTextField = "centerName";
            ddlTransferCenters.DataValueField = "centerId";
            ddlTransferCenters.DataSource = SysCenters.GetCenters();
            ddlTransferCenters.DataBind();
            ddlTransferCenters.Items.Insert(0, new ListItem("-- Seleccione --", "0"));
        }

        private bool ValidateResidentTransferred()
        {
            if (Convert.ToInt32(grvResidents.SelectedDataKey[0]) > 0) //first time the id come 0
            {
                if (((ResResidentInfo)Session["resident"]).ResidentStatusId != (int)GeneralCommon.ResidentStatuses.Transferred)
                {
                    return true;
                }
                else
                {
                    //if the resident has been transferred
                    Master.Master.Message("Este médico residente ha sido transferido a otro centro y sus datos no pueden ser modificados.", "", 4);
                    return false;
                }
            }
            else
                return true;
        }

        private void MakeClean()
        {
            Master.Master.PersonInfo("", "", "");
            txtExequatur.Text = string.Empty;
            txtEnrollment.Text = string.Empty;
            ddlUniversities.SelectedIndex = 0;
            //AsignedEspecialties = null;
            grvResidences.DataSource = null;
            grvResidences.DataBind();
            ddlResidences.SelectedIndex = -1;
            lblCurrentResidence.Text = string.Empty;
            ddlGrade.SelectedIndex = 0;
            ddlGrade.Enabled = true;
            txtStartDateResidence.Text = string.Empty;
            //AsignedEspecialties = null;
            ddlRotationCenters.SelectedIndex = 0;
            ddlDepartments.SelectedIndex = 0;
            txtStartDate.Text = string.Empty;
            txtEndDate.Text = string.Empty;
            ddlRotationType.SelectedIndex = 0;
            txtRotationNote.Text = string.Empty;
            AsignedRotations = null;
            grvRotations.DataSource = null;
            grvRotations.DataBind();
            txtName.Text = string.Empty;
            txtMiddleName.Text = string.Empty;
            txtFirstLastName.Text = string.Empty;
            txtSecondLastName.Text = string.Empty;
            ddlRelationship.SelectedIndex = 0;
            txtCellPhone.Text = string.Empty;
            txtHomePhone.Text = string.Empty;
            txtAllergies.Text = string.Empty;
            txtDrugs.Text = string.Empty;
            ddlStatus.SelectedIndex = 0;
            ddlStatus.Enabled = false;
            txtComment.Text = string.Empty;
            lblStatus.Text = string.Empty;
            ddlTransferCenters.Enabled = false;
            ddlTransferCenters.SelectedIndex = 0;
            fuTransferDocument.Enabled = false;
            grvComments.DataSource = null;
            grvComments.DataBind();
            ddlDepartments.Enabled = true;
            ddlRotationCenters.Enabled = true;
            txtStartDate.Enabled = true;
            txtEndDate.Enabled = true;
            ddlRotationType.Enabled = true;
            txtRotationNote.Enabled = true;
            AsignedPhones = null;
            //btnAddRotation.Visible = true;
            //btnSave.Visible = true;
            //btnCancel.Visible = true;
        }

        //private void HideOptionsInRotations()
        //{
        //    foreach (GridViewRow row in grvRotations.Rows)
        //    {
        //        ImageButton obj = row.FindControl("rotationSelect") as ImageButton;
        //        obj.Visible = false;
        //        if (Convert.ToDateTime(grvRotations.DataKeys[row.RowIndex].Values["startDate"]) >= DateTime.Now)
        //            obj.Visible = true;
        //        //else
        //        //    btnAddRotation.Visible = true;
        //    }
        //    ddlRotationType.SelectedIndex = 0;
        //    divCenter.Visible = false;
        //    ddlRotationCenters.SelectedIndex = 0;
        //    ddlDepartments.SelectedIndex = 0;
        //    txtStartDate.Text = string.Empty;
        //    txtEndDate.Text = string.Empty;
        //    txtRotationNote.Text = string.Empty;
        //}

        private bool Validations()
        {
            bool StopValidation = false;

            //////Exequátur
            //if (ResResidentDocuments.IfExistsDocuemnt(txtExequatur.Text, Convert.ToInt32(grvResidents.SelectedDataKey[2])))
            //{
            //    Master.Master.Message("Este Exequátur ya se encuentra asignado a otro médico residente.", "", 2);
            //    StopValidation = true;
            //}

            ////Matrícula
            if (ResResidentDocuments.IfExistsDocuemnt(txtEnrollment.Text, Convert.ToInt32(grvResidents.SelectedDataKey[2])))
            {
                Master.Master.Message("Esta Matrícula ya se encuentra asignada a otro médico residente.", "", 2);
                StopValidation = true;
            }

            ////rotations
            if (Convert.ToInt32(ddlRotationType.SelectedValue) == 0 || Convert.ToInt32(ddlDepartments.SelectedValue) == 0 || txtStartDate.Text == string.Empty || txtEndDate.Text == string.Empty)
            {
                if (((ResResidentInfo)Session["resident"]).RotationId > 0)
                {
                    Master.Master.Message("Complete los campos de rotación y inténtelo de nuevo.", "", 2);
                    StopValidation = true;
                }
            }
            else
            {
                DateTime _startDate = Convert.ToDateTime(txtStartDate.Text);
                DateTime _endDate = Convert.ToDateTime(txtEndDate.Text);
                if (_startDate > _endDate)
                {
                    Master.Master.Message("La fecha de inicio de la rotación debe de ser menor que la fecha de fin.", "", 2);
                    StopValidation = true;
                }

                TimeSpan differentInDates = _endDate - _startDate;
                ////1 month -> 30 days || 6 month -> 30*6=180+3 (+3 cause month with 31 days
                if (differentInDates.Days < 30 || differentInDates.Days > 183)
                {
                    Master.Master.Message("El Tiempo de duración de las rotaciones debe de ser de mínimo un (1) mes y máximo seis (6) mese.", "", 2);
                    StopValidation = true;
                }

                ////has to be >= now
                if (_startDate.Date < DateTime.Now.Date)
                {
                    Master.Master.Message("La fecha de inicio no es válida.", "", 2);
                    StopValidation = true;
                }

                if (Convert.ToInt32(ddlRotationType.SelectedValue) == (int)GeneralCommon.RotationTypes.External && txtRotationNote.Text == string.Empty)
                {
                    Master.Master.Message("La rotación es Externa, favor agregar alguna nota.", "", 2);
                    StopValidation = true;
                }
            }

            ////start date
            //if (Convert.ToDateTime(txtStartDate.Text) < ((SysUserAccess)Session["access"]).StartDate || Convert.ToDateTime(txtStartDate.Text) > ((SysUserAccess)Session["access"]).EndDate)
            //{
            //    Master.Master.Message("La fecha de inicio en la residencia no se encuentra en el año docente actual.", "", 2);
            //    StopValidation = true;
            //}

            ////phones
            if (txtHomePhone.Text == string.Empty && txtCellPhone.Text == string.Empty)
            {
                Master.Master.Message("Un número de teléfono es requerido en los datos de emergencia.", "", 2);
                StopValidation = true;
            }
            return StopValidation;
        }

        private void IfExistsPhones(int generalId, string phoneNumber, int phoneTypeId)
        {
            bool exists = false;
            int phoneId = 0;
            if (AsignedPhones != null)
            {
                foreach (GenGeneralPhones.Phones phoneInList in AsignedPhones)
                {
                    if (phoneInList.phoneTypeId == phoneTypeId)
                    {
                        exists = true;
                        phoneId = phoneInList.phoneId;
                    }
                }
            }

            GenGeneralPhones cPhones;
            if (exists)
                cPhones = GenGeneralPhones.Get(phoneId);
            else
                cPhones = new GenGeneralPhones();


            cPhones.GeneralId = generalId;
            cPhones.PhoneNumber = phoneNumber;
            cPhones.PhoneTypeId = phoneTypeId;
            cPhones.PhoneStatus = true;

            if (exists)
            {
                if (phoneNumber == string.Empty)
                    cPhones.Delete();
                else
                {
                    cPhones.ModifiedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                    cPhones.ModifiedDate = DateTime.Now;
                    cPhones.Update();
                }
            }
            else
            {
                if (phoneNumber != string.Empty)
                {
                    cPhones.CreatedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                    cPhones.CreatedDate = DateTime.Now;
                    cPhones.Insert();
                }
            }
        }

        #endregion

        #region Buttons

        protected void txtSearchName_TextChanged(object sender, EventArgs e)
        {
            btnSearchResidents_Click(null, null);
        }

        protected void btnSearchResidents_Click(object sender, EventArgs e)
        {
            //clean list after save -> note

            ////Persons by entity type
            grvResidents.DataSource = ResResidents.GetByCenterId((int)((SysUserAccess)Session["access"]).CenterId, txtSearchName.Text);
            grvResidents.DataBind();
        }

        //protected void btnCreateResident_Click(object sender, EventArgs e)
        //{
        //    MakeClean();
        //}
        
        //protected void btnAddRotation_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (grvResidents.SelectedIndex == -1)
        //        {
        //            Master.Master.Message("Debe de seleccionar un médico residente.", "", 2);
        //            return;
        //        }

        //        Master.Master.Message("", "", 0);
        //        if (Convert.ToInt32(ddlDepartments.SelectedValue) < 1 || txtStartDate.Text == string.Empty || txtEndDate.Text == string.Empty || Convert.ToInt32(ddlRotationType.SelectedValue) < 1)
        //        {
        //            Master.Master.Message("Complete los campos de rotación y inténtelo de nuevo.", "", 2);
        //            return;
        //        }

        //        DateTime _startDate = Convert.ToDateTime(txtStartDate.Text);
        //        DateTime _endDate = Convert.ToDateTime(txtEndDate.Text);
        //        if (_startDate > _endDate)
        //        {
        //            Master.Master.Message("La fecha de inicio de la rotación debe de ser menor que la fecha de fin.", "", 2);
        //            return;
        //        }

        //        TimeSpan differentIdDates = _endDate - _startDate;
        //        ////1 month -> 30 days || 6 month -> 30*6=180+3 (+3 cause month with 31 days
        //        if (differentIdDates.Days < 30 || differentIdDates.Days > 183)
        //        {
        //            Master.Master.Message("El Tiempo de duración de las rotaciones debe de ser de mínimo un (1) mes y máximo seix (6) mese.", "", 2);
        //            return;
        //        }

        //        ////has to be >= now
        //        if (_startDate.Date < DateTime.Now.Date)
        //        {
        //            Master.Master.Message("La fecha de inicio no es válida.", "", 2);
        //            return;
        //        }

        //        if (Convert.ToInt32(ddlRotationType.SelectedValue) == (int)GeneralCommon.RotationTypes.External && txtRotationNote.Text == string.Empty)
        //        {
        //            Master.Master.Message("La rotación es Externa, favor agregar alguna nota.", "", 2);
        //            return;
        //        }

        //        ////dont allow more than 1 rotation in date
        //        if (btnAddRotation.Visible == true && Convert.ToInt32(grvResidents.SelectedDataKey[0])>0)
        //        {
        //            foreach (ResRotations.Rotations rotationInList in AsignedRotations.Where(r => r.endDate.Date > DateTime.Now.Date))
        //            {
        //                rotationInList.departmentId = Convert.ToInt32(ddlDepartments.SelectedValue);
        //                rotationInList.departmentName = ddlDepartments.SelectedItem.ToString();
        //                rotationInList.startDate = Convert.ToDateTime(txtStartDate.Text);
        //                rotationInList.endDate = Convert.ToDateTime(txtEndDate.Text);
        //                rotationInList.rotationTypeId = Convert.ToInt32(ddlRotationType.SelectedValue);
        //                rotationInList.rotationType = ddlRotationType.SelectedItem.ToString();
        //                rotationInList.rotationNote = txtRotationNote.Text;
        //                rotationInList.centerId = Convert.ToInt32(ddlRotationCenters.SelectedValue);
        //                grvRotations.DataSource = AsignedRotations.OrderByDescending(p => p.startDate).ToList();
        //                grvRotations.DataBind();
        //                HideOptionsInRotations();
        //                return;
        //            }
        //        }
        //        else
        //        {
        //            if (Convert.ToInt32(grvResidents.SelectedDataKey[0]) > 0)
        //            {
        //                foreach (ResRotations.Rotations rotationInList in AsignedRotations)
        //                {
        //                    if (rotationInList.endDate.Date > DateTime.Now.Date)
        //                    {
        //                        Master.Master.Message("Tiene una rotación sin finalizar. No puede agregar otra rotación.", "", 2);
        //                        return;
        //                    }
        //                }
        //            }
        //        }


        //        List<ResRotations.Rotations> tempRotations = new List<ResRotations.Rotations>();
        //        if (AsignedRotations != null)
        //            tempRotations = AsignedRotations.ToList();

        //        ResRotations.Rotations newRotation = new ResRotations.Rotations();
        //        if (grvRotations.SelectedIndex > -1)
        //            newRotation.rotationId = (int)grvRotations.SelectedDataKey[0];
        //        else
        //            newRotation.rotationId = 0;

        //        newRotation.departmentId = Convert.ToInt32(ddlDepartments.SelectedValue);
        //        newRotation.departmentName = ddlDepartments.SelectedItem.ToString();
        //        newRotation.startDate = Convert.ToDateTime(txtStartDate.Text);
        //        newRotation.endDate = Convert.ToDateTime(txtEndDate.Text);
        //        newRotation.rotationTypeId = Convert.ToInt32(ddlRotationType.SelectedValue);
        //        newRotation.rotationType = ddlRotationType.SelectedItem.ToString();
        //        newRotation.rotationNote = txtRotationNote.Text;
        //        newRotation.centerId = Convert.ToInt32(ddlRotationCenters.SelectedValue);

        //        tempRotations.Add(newRotation);
        //        AsignedRotations = tempRotations.OrderByDescending(p => p.startDate).ToList();

        //        grvRotations.DataSource = AsignedRotations;
        //        grvRotations.DataBind();
        //        HideOptionsInRotations();
        //    }
        //    catch (Exception ex)
        //    {
        //        SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
        //    }
        //}

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (grvResidents.SelectedIndex < 0)
                {
                    Master.Master.Message("Debe de seleccionar un médico residente.", "", 2);
                    return;
                }

                //if the resident has been transferred
                if (!ValidateResidentTransferred())
                    return;

                if (Validations())
                    return;

                if (ddlStatus.SelectedIndex > 0 && txtComment.Text == string.Empty)
                {
                    Master.Master.Message("Si desea cambiar el estado del médico residente actual debe de hacer un comentario del cambio.", "", 2);
                    return;
                }

                if (Convert.ToInt32(ddlStatus.SelectedValue) == (int)GeneralCommon.ResidentStatuses.Transferred)
                {
                    //if (Convert.ToInt32(ddlTransferCenters.SelectedIndex) == -1)
                    //{
                    //    Master.Master.Message("Debe seleccionar el centro al cual desea hacer el traslado.", "", 2);
                    //    return;
                    //}
                    if (((ResResidentInfo)Session["resident"]).ResidentStatusId == (int)GeneralCommon.ResidentStatuses.Rotating)
                    {
                        Master.Master.Message("Este médico residente se encuentra rotando y no puede ser trasladado.", "", 2);
                        return;
                    }
                }

                //GeneralEntityId = emergency contact general id
                int GeneralEntityId = 0;
                ResResidents ActualResident;
                //ResResidentDocuments ResidentDocuments;
                //ResRotations ResidentRotations;  
                //GenGenerals GeneralEntity;
                //GenEntities Entity;

                GenPersonEmergencyContacts Contact;
                GenPersons EmergencyContact;
                if ((int)grvResidents.SelectedDataKey[0] > 0)
                {
                    ActualResident = ResResidents.Get((int)grvResidents.SelectedDataKey[0]);
                    //ResidentDocuments = ResResidentDocuments.Get((int)grvResidents.SelectedDataKey[0]);
                    //ResidentRotations = ResRotations.Get((int)grvResidents.SelectedDataKey[0]);
                    //GeneralEntity = GenGenerals.Get((int)grvResidents.SelectedDataKey[2]);
                    //Entity = GenEntities.Get((int)grvResidents.SelectedDataKey[2], (int)GeneralCommon.EntityTypes.Resident);
                    //EmergencyContact =GenPersons.Get((int)grvResidents.SelectedDataKey[3]);
                    Contact = GenPersonEmergencyContacts.Get((int)grvResidents.SelectedDataKey[3]);
                    EmergencyContact = GenPersons.Get((int)Contact.ContactId);
                }
                else
                {
                    ActualResident = new ResResidents();
                    //ResidentDocuments = new ResResidentDocuments();
                    //ResidentRotations = new ResRotations();
                    //GeneralEntity = new GenGenerals();
                    //Entity = new GenEntities();

                    Contact = new GenPersonEmergencyContacts();
                    EmergencyContact = new GenPersons();
                }

                ////resident
                //ResResidents ActualResident = new ResResidents();
                int _generalId=(int)grvResidents.SelectedDataKey[2];
                ActualResident.GeneralId =_generalId;
                ActualResident.UniversityId = Convert.ToInt32(ddlUniversities.SelectedValue);
                ActualResident.ResidenceId = Convert.ToInt32(ddlResidences.SelectedValue);
                ActualResident.GradeId = Convert.ToInt32(ddlGrade.SelectedValue);
                ActualResident.StartDate = Convert.ToDateTime(txtStartDateResidence.Text);
                ActualResident.CentreId = (int)((SysUserAccess)Session["access"]).CenterId;

                ////resident documents
                ResResidentDocuments ResidentDocuemnts = new ResResidentDocuments();
                
                ////emergency contact info
                //GenPersons EmergencyContact = new GenPersons();
                //EmergencyContact.GeneralId = (int)grvResidents.SelectedDataKey[2];
                EmergencyContact.Name = txtName.Text;
                EmergencyContact.MiddleName = txtMiddleName.Text;
                EmergencyContact.FirstLastName = txtFirstLastName.Text;
                EmergencyContact.SecondLastName = txtSecondLastName.Text;

                ////emergency contact
                //GenPersonEmergencyContacts Contact = new GenPersonEmergencyContacts();                               
                Contact.RelationshipId = Convert.ToInt32(ddlRelationship.SelectedValue);
                Contact.Allergies = txtAllergies.Text;
                Contact.Drugs = txtDrugs.Text;

                int _residentId = 0;
                //if (((ResResidentInfo)Session["resident"]).ResidentStatusId == (int)GeneralCommon.ResidentStatuses.Active)
                //{
                if ((int)grvResidents.SelectedDataKey[0] > 0)
                {
                    if (ddlStatus.SelectedIndex > 0)
                        ActualResident.ResidentStatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                    ActualResident.ResidentId = (int)grvResidents.SelectedDataKey[0];
                    ActualResident.ModifiedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                    ActualResident.ModifiedDate = DateTime.Now;
                    ActualResident.Update();
                    _residentId = Convert.ToInt32(grvResidents.SelectedDataKey[0]);
                }
                else
                {
                    ActualResident.TeachingYearId = (int)((SysUserAccess)Session["access"]).TeachingYearId;
                    ActualResident.ResidentStatusId = (int)GeneralCommon.ResidentStatuses.Active;
                    ActualResident.CreatedBY = ((SysUserAccess)Session["access"]).UserCenterId;
                    ActualResident.CreatedDate = DateTime.Now;
                    _residentId = ActualResident.Insert();
                }

                if ((int)grvResidents.SelectedDataKey[0] != 0)
                {
                    //EmergencyContact.PersonId = (int)grvResidents.SelectedDataKey[3];
                    //general id for emergeny contact
                    GeneralEntityId = (int)EmergencyContact.GeneralId;
                    EmergencyContact.ModifiedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                    EmergencyContact.ModifiedDate = DateTime.Now;
                    EmergencyContact.Update();

                    //Contact.ContactId = (int)grvResidents.SelectedDataKey[3];
                    Contact.ModifiedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                    Contact.ModifiedDate = DateTime.Now;
                    Contact.Update();
                }
                else
                {
                    ////general for emergency contact
                    GenGenerals GeneralEntity = new GenGenerals();
                    GeneralEntityId = GeneralEntity.Insert();
                                        
                    //// emergency contact entity
                    GenEntities EmergencyEntity = new GenEntities();
                    EmergencyEntity.GeneralId = GeneralEntityId;
                    EmergencyEntity.EntityTypeId = (int)GeneralCommon.EntityTypes.EmergencyContact;
                    EmergencyEntity.CenterId = ((SysUserAccess)Session["access"]).CenterId;
                    EmergencyEntity.EntityStatus = true;
                    EmergencyEntity.CreatedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                    EmergencyEntity.CreatedDate = DateTime.Now;
                    EmergencyEntity.Insert();

                    EmergencyContact.GeneralId = GeneralEntityId;

                    Contact.ContactId = EmergencyContact.Insert();
                    Contact.PersonId = (int)grvResidents.SelectedDataKey[1];
                    Contact.CreatedBY = ((SysUserAccess)Session["access"]).UserCenterId;
                    Contact.CreatedDate = DateTime.Now;
                    Contact.Insert();
                }

                IfExistsPhones(GeneralEntityId, txtHomePhone.Text, (int)GeneralCommon.PhoneTypes.HomePhone);
                IfExistsPhones(GeneralEntityId, txtCellPhone.Text, (int)GeneralCommon.PhoneTypes.CellPhone);

                ////residnet documents       
                ResidentDocuemnts.GeneralId = _generalId;
                ResidentDocuemnts.CreatedBY = ((SysUserAccess)Session["access"]).UserCenterId;
                ResidentDocuemnts.CreatedDate = DateTime.Now;
                ResidentDocuemnts.ModifiedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                ResidentDocuemnts.ModifiedDate = DateTime.Now;
                if ((int)grvResidents.SelectedDataKey[0] > 0)
                {
                    ResidentDocuemnts.DocumentNumber = txtEnrollment.Text;
                    ResidentDocuemnts.DocumentTypeId = (int)GeneralCommon.ResidentDocTypes.Enrollment;
                    ResidentDocuemnts.Update();

                    ResidentDocuemnts.DocumentNumber = txtExequatur.Text;
                    ResidentDocuemnts.DocumentTypeId = (int)GeneralCommon.ResidentDocTypes.Exequatur;
                    ResidentDocuemnts.Update();
                }
                else
                {
                    ResidentDocuemnts.DocumentNumber = txtEnrollment.Text;
                    ResidentDocuemnts.DocumentTypeId = (int)GeneralCommon.ResidentDocTypes.Enrollment;
                    ResidentDocuemnts.Insert();

                    ResidentDocuemnts.DocumentNumber = txtExequatur.Text;
                    ResidentDocuemnts.DocumentTypeId = (int)GeneralCommon.ResidentDocTypes.Exequatur;
                    ResidentDocuemnts.Insert();
                }

                ////rotation
                //ResidentRotations.ResidentId = _residentId;
                //just for the firs rotation in top
                //foreach (ResRotations.Rotations rotationsInList in AsignedRotations.OrderByDescending(p => p.startDate).ToList())
                //{
                    //ResidentRotations.RotationId = rotationsInList.rotationId;
                    //ResidentRotations.DepartmentId = rotationsInList.departmentId;
                    //ResidentRotations.StartDate = rotationsInList.startDate;
                    //ResidentRotations.EndDate = rotationsInList.endDate;
                    //ResidentRotations.RotationTypeId = rotationsInList.rotationTypeId;
                    //ResidentRotations.Note = rotationsInList.rotationNote;
                    //if (rotationsInList.centerId != 0)
                    //    ResidentRotations.CenterId = rotationsInList.centerId;
                    //else
                    //    ResidentRotations.CenterId = null;
                    //break;
                //}
                
                ////resident rotations
                if (Convert.ToInt32(ddlRotationType.SelectedValue) > 0 && Convert.ToInt32(ddlDepartments.SelectedValue) > 0 && txtStartDate.Text != string.Empty && txtEndDate.Text != string.Empty)
                {
                    ResRotations ResidentRotation;
                    if (((ResResidentInfo)Session["resident"]).RotationId > 0)
                        ResidentRotation = ResRotations.Get((int)((ResResidentInfo)Session["resident"]).RotationId);
                    else
                        ResidentRotation = new ResRotations();

                    ResidentRotation.ResidentId = _residentId;
                    ResidentRotation.DepartmentId = Convert.ToInt32(ddlDepartments.SelectedValue);
                    ResidentRotation.StartDate = Convert.ToDateTime(txtStartDate.Text);
                    ResidentRotation.EndDate = Convert.ToDateTime(txtEndDate.Text);
                    ResidentRotation.RotationTypeId = Convert.ToInt32(ddlRotationType.SelectedValue);
                    ResidentRotation.Note = txtRotationNote.Text;

                    if (Convert.ToInt32(ddlRotationType.SelectedValue) == (int)GeneralCommon.RotationTypes.External)
                    {
                        if (Convert.ToInt32(ddlRotationCenters.SelectedValue) > 0)
                            ResidentRotation.CenterId = Convert.ToInt32(ddlRotationCenters.SelectedValue);
                        else
                            ResidentRotation.CenterId = null;
                    }
                    else
                        ResidentRotation.CenterId = null;

                    if (((ResResidentInfo)Session["resident"]).RotationId > 0)
                    {
                        ResidentRotation.ModifiedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                        ResidentRotation.ModifiedDate = DateTime.Now;
                        ResidentRotation.Update();
                    }
                    else
                    {
                        ResidentRotation.CreatedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                        ResidentRotation.CreatedDate = DateTime.Now;
                        ResidentRotation.Insert();
                    }
                }
                
                //comments in status
                if (ddlStatus.SelectedIndex > 0)
                {
                    ResResidentStatusComments nComment = new ResResidentStatusComments();
                    nComment.ResidentId = _residentId;
                    nComment.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                    nComment.Comment = txtComment.Text;
                    nComment.CreatedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                    nComment.CreatedDate = DateTime.Now;
                    nComment.Insert();

                    if (Convert.ToInt32(ddlStatus.SelectedValue) == (int)GeneralCommon.ResidentStatuses.Transferred)
                    {
                        ResTransfers nTransfer = new ResTransfers();
                        nTransfer.ResidentId = _residentId;
                        nTransfer.ToCenterId = Convert.ToInt32(ddlTransferCenters.SelectedValue);
                        if (fuTransferDocument.HasFile)
                            nTransfer.TransferDocument = GenUtilities.SaveFileInPath(ref fuTransferDocument, GeneralCommon.TypeFilePaths.TransferDocuments, "");
                        nTransfer.CreatedBy = ((SysUserAccess)Session["access"]).UserCenterId;
                        nTransfer.CreatedDate = DateTime.Now;
                        nTransfer.Insert();
                    }
                }

                MakeClean();
                btnSearchResidents_Click(null, null);
                grvResidents.SelectedIndex = -1;
                Master.Master.Message("Los datos para este Médico Residente se han guardado exitosamente.", "", 3);
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            MakeClean();
        }
        #endregion

        #region GridViews
        protected void grvResidents_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                MakeClean();

                ResResidentInfo ResidentInfo;
                if ((int)grvResidents.SelectedDataKey[0] != 0)
                    ResidentInfo = ResResidentInfo.Get((int)grvResidents.SelectedDataKey[0]);
                else
                    ResidentInfo = new ResResidentInfo();

                if (ResidentInfo.UniversityId != null)
                    ddlUniversities.SelectedValue = ResidentInfo.UniversityId.ToString();

                Session["resident"] = ResidentInfo;

                Master.Master.PersonInfo(grvResidents.SelectedDataKey[4].ToString(), grvResidents.SelectedDataKey[5].ToString(), grvResidents.SelectedDataKey[6].ToString());

                ////especialty  
                ddlResidences.Items.Clear();
                ddlResidences.DataTextField = "residenceName";
                ddlResidences.DataValueField = "residenceId";
                ddlResidences.DataSource = ResResidences.GetResidencesByGrade(ResidentInfo.ResidenceId, ResidentInfo.GradeId, (int)((SysUserAccess)Session["access"]).CenterId, (int)((SysUserAccess)Session["access"]).TeachingYearId);
                ddlResidences.DataBind();
                ddlResidences.Items.Insert(0, new ListItem("-- Seleccione Residencia --", "0"));

                lblCurrentResidence.Text = ResidentInfo.PreResidence;

                upanelRotation.Update();
                if ((int)grvResidents.SelectedDataKey[0] != 0)
                {
                    txtExequatur.Text = ResidentInfo.Exequatur;
                    txtEnrollment.Text = ResidentInfo.Enrollment;
                    ddlResidences.SelectedValue = ResidentInfo.ResidenceId.ToString();
                    txtStartDateResidence.Text = ResidentInfo.StartDate.ToString();

                    if (ResidentInfo.ResidentStatusId == (int)GeneralCommon.ResidentStatuses.Promoted)
                        ddlGrade.SelectedValue = (ResidentInfo.GradeId + 1).ToString();
                    else
                        ddlGrade.SelectedValue = ResidentInfo.GradeId.ToString();

                    grvResidences.DataSource = ResResidents.GetByGeneralId((int)grvResidents.SelectedDataKey[2]);
                    grvResidences.DataBind();
                    
                    AsignedRotations = ResRotations.GetByResidentId((int)grvResidents.SelectedDataKey[0]);
                    foreach (ResRotations.Rotations rotationInList in AsignedRotations)
                    {
                        if (rotationInList.rotationId == ResidentInfo.RotationId)
                        {
                            ddlDepartments.SelectedValue = rotationInList.departmentId.ToString();
                            txtStartDate.Text = rotationInList.startDate.ToString();
                            txtEndDate.Text = rotationInList.endDate.ToString();
                            ddlRotationType.SelectedValue = rotationInList.rotationTypeId.ToString();
                            txtRotationNote.Text = rotationInList.rotationNote;
                        }

                        if (rotationInList.startDate < DateTime.Now && rotationInList.endDate > DateTime.Now)
                        {
                            ddlDepartments.Enabled = false;
                            ddlRotationCenters.Enabled = false;
                            txtStartDate.Enabled = false;
                            txtEndDate.Enabled = false;
                            ddlRotationType.Enabled = false;
                            txtRotationNote.Enabled = false;
                        }
                    }

                    grvRotations.DataSource = AsignedRotations.Where(r => r.rotationId != ResidentInfo.RotationId).OrderByDescending(p => p.startDate).ToList();
                    grvRotations.DataBind();

                    upanelRotation.Update();

                    //HideOptionsInRotations();

                    txtName.Text = ResidentInfo.Name;
                    txtMiddleName.Text = ResidentInfo.MiddleName;
                    txtFirstLastName.Text = ResidentInfo.FirstLastName;
                    txtSecondLastName.Text = ResidentInfo.SecondLastName;
                    ddlRelationship.SelectedValue = ResidentInfo.RelationshipId.ToString();
                    txtAllergies.Text = ResidentInfo.Allergies;
                    txtDrugs.Text = ResidentInfo.Drugs;
                }

                //phones
                AsignedPhones = GenGeneralPhones.GetByGeneralId(Convert.ToInt32(grvResidents.SelectedDataKey[7])); //EmergencyGeneralId
                foreach (GenGeneralPhones.Phones phoneInList in AsignedPhones)
                {
                    switch (phoneInList.phoneTypeId)
                    {
                        case (int)GeneralCommon.PhoneTypes.HomePhone:
                            txtHomePhone.Text = phoneInList.phoneNumber;
                            break;
                        case (int)GeneralCommon.PhoneTypes.CellPhone:
                            txtCellPhone.Text = phoneInList.phoneNumber;
                            break;
                    }
                }

                ddlStatus.Enabled = true;
                if (ResidentInfo.ResidentStatusId == (int)GeneralCommon.ResidentStatuses.Rotating)
                    lblStatus.Text = "No es posible cambiar el estado en este momento. Este médico residente se encuentra rotando.";
                else
                    lblStatus.Text = string.Empty;

                //coments in status
                grvComments.DataSource = ResResidentStatusComments.GetByResidentId((int)grvResidents.SelectedDataKey[0], null);
                grvComments.DataBind();
                //upanelStatus.Update();

                ValidateResidentTransferred();
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }


        protected void grvResidents_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvResidents.PageIndex = e.NewPageIndex;
            grvResidents.SelectedIndex = -1;
            btnSearchResidents_Click(null, null);
        }

        //protected void grvRotations_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName == "Editing")
        //        {
        //            IList<ResRotations.Rotations> asignedRotationList = AsignedRotations;

        //            foreach (ResRotations.Rotations rotationInList in asignedRotationList.ToList())
        //            {
        //                if (rotationInList.rotationId == Convert.ToInt32(e.CommandArgument))
        //                {
        //                    ddlDepartments.SelectedValue = rotationInList.departmentId.ToString();
        //                    txtStartDate.Text = rotationInList.startDate.ToString();
        //                    txtEndDate.Text = rotationInList.endDate.ToString();
        //                    ddlRotationType.SelectedValue = rotationInList.rotationTypeId.ToString();
        //                    txtRotationNote.Text = rotationInList.rotationNote;
        //                    btnAddRotation.Visible = true;
        //                    return;
        //                }
        //            }
        //        }

        //        //if (e.CommandName == "Deleting")
        //        //{
        //        //    IList<ResRotations.Rotations> asignedRotationList = AsignedRotations;
        //        //    IList<ResRotations.Rotations> deletedRotationList = DeletedRotations;

        //        //    foreach (ResRotations.Rotations rotationInList in asignedRotationList.ToList())
        //        //    {
        //        //        if (rotationInList.rotationId == Convert.ToInt32(e.CommandArgument))
        //        //        {
        //        //            asignedRotationList.Remove(rotationInList);
        //        //            deletedRotationList.Add(rotationInList);
        //        //            grvRotations.DataSource = asignedRotationList;
        //        //            grvRotations.DataBind();
        //        //            return;
        //        //        }
        //        //    }
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
        //    }
        //}
        #endregion

        #region DropDownLists
        protected void ddlRotationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDepartments.Enabled = true;
            txtStartDate.Enabled = true;
            txtEndDate.Enabled = true;
            txtRotationNote.Enabled = true;
            switch (Convert.ToInt32(ddlRotationType.SelectedValue))
            {
                case (int)GeneralCommon.RotationTypes.Internal:
                    divCenter.Visible = false;
                    break;
                case (int)GeneralCommon.RotationTypes.External:
                    divCenter.Visible = true;
                    break;
                case 0:
                    ddlDepartments.Enabled = false;
                    txtStartDate.Enabled = false;
                    txtEndDate.Enabled = false;
                    txtRotationNote.Enabled = false;
                    break;
            }
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlStatus.SelectedValue) == (int)GeneralCommon.ResidentStatuses.Transferred)
            {
                ddlTransferCenters.Enabled = true;
                fuTransferDocument.Enabled = true;
            }
            else
            {
                ddlTransferCenters.Enabled = false;
                fuTransferDocument.Enabled = false;
            }
            //upanelStatus.Update();
        }
        #endregion
    }
}