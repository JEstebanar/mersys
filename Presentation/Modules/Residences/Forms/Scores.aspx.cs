﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using General.GeneralCommons;
using Residences.Residents;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.SysUsers;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Residences.Forms
{
    public partial class Scores : System.Web.UI.Page
    {
        private IList<ResResidents.Scores> ScoresHistory { get { return (IList<ResResidents.Scores>)ViewState["scoresHistory"]; } set { ViewState["scoresHistory"] = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.ResidentScores))
                    {
                        if (!IsPostBack)
                        {
                            FillScores();
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillScores()
        {
            ScoresHistory = ResResidents.GetScoresByGeneralId((int)((SysUsers)Session["user"]).GeneralId);

            grvQ1.DataSource = ScoresHistory;
            grvQ1.DataBind();

            grvQ2.DataSource = ScoresHistory;
            grvQ2.DataBind();

            grvQ3.DataSource = ScoresHistory;
            grvQ3.DataBind();

            grvAvg.DataSource = ScoresHistory;
            grvAvg.DataBind();
        }
    }
}