﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI.WebControls;
using General.GeneralCommons;
using Residences.Grades;
using Residences.Residences;
using Residences.Residents;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;
using General.Forms;
using Residences.TeachingYears;

namespace Presentation.Modules.Residences.Forms
{
    public partial class ChangeResidentsStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.PromoteResidnet))
                    {
                        if (!IsPostBack)
                        {
                            ddlTeachingYears.Items.Clear();
                            ddlTeachingYears.DataTextField = "teachingYear";
                            ddlTeachingYears.DataValueField = "teachingYearId";
                            ddlTeachingYears.DataSource = ResTeachingYears.GetTeachingYears((int)((SysUserAccess)Session["access"]).CenterId);
                            ddlTeachingYears.DataBind();
                            
                            ddlGrades.DataTextField = "gradeName";
                            ddlGrades.DataValueField = "gradeId";
                            ddlGrades.DataSource = ResGrades.GetGrades();
                            ddlGrades.DataBind();
                            ddlGrades.Items.Insert(0, new ListItem("-- Todos los Grados --", "0"));

                            Initialize();
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        #region Methods
        private void Initialize()
        {
            //ddlResidences.Items.Clear();
            ddlResidences.DataTextField = "residenceName";
            ddlResidences.DataValueField = "residenceId";
            ddlResidences.DataSource = ResResidences.GetResidences((int)((SysUserAccess)Session["access"]).CenterId, Convert.ToInt32(ddlTeachingYears.SelectedValue));
            ddlResidences.DataBind();
            ddlResidences.Items.Insert(0, new ListItem("-- Todas las Residencias --", "0"));
        }
        #endregion

        #region Buttons
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            grvResidents.DataSource = ResResidents.GetToPromoved(Convert.ToInt32(ddlResidences.SelectedValue), Convert.ToInt32(ddlGrades.SelectedValue), (int)((SysUserAccess)Session["access"]).CenterId, Convert.ToInt32(ddlTeachingYears.SelectedValue));
            grvResidents.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //if (ddlStatus.SelectedIndex == 0)
            //{
            //    Master.Master.Message("Seleccione el estado al cual quiere cambiar los médicos residentes seleccionados", "", 2);
            //    return;
            //}

            if (grvResidents.Rows.Count == 0)
            {
                Master.Master.Message("Seleccione los médicos residentes a los cuales desea promover", "", 2);
                return;
            }

            int count = 0;
            ResResidents cResident;
            ResResidences cResidence;
            foreach (GridViewRow row in grvResidents.Rows)
            {
                if ((row.FindControl("chkResident") as CheckBox).Checked)
                {
                    cResident = ResResidents.Get(Convert.ToInt32(grvResidents.DataKeys[row.RowIndex].Values["residentId"]));
                    cResidence = ResResidences.Get((int)cResident.ResidenceId);
                    if (cResidence.ResidenceMaxGradeId == cResident.GradeId)
                    {
                        cResident.ResidentStatusId = (int)GeneralCommon.ResidentStatuses.Graduate;
                    }
                    else
                    {
                        cResident.ResidentStatusId = (int)GeneralCommon.ResidentStatuses.Promoted;

                        ResResidents nResident = new ResResidents();
                        nResident.GeneralId = cResident.GeneralId;
                        nResident.ResidenceId = cResident.ResidenceId;
                        nResident.GradeId = cResident.GradeId + 1;
                        nResident.UniversityId = cResident.UniversityId;
                        nResident.TeachingYearId = ResTeachingYears.GetNext((int)cResident.TeachingYearId);// (int)((SysUserAccess)Session["access"]).TeachingYearId);
                        nResident.StartDate = DateTime.Now;
                        nResident.ResidentStatusId = (int)GeneralCommon.ResidentStatuses.Active;
                        nResident.CentreId = cResident.CentreId;
                        nResident.CreatedBY = (int)((SysUserAccess)Session["access"]).UserCenterId;
                        nResident.CreatedDate = DateTime.Now;
                        nResident.Insert();
                    }
                    cResident.EndDate = ((SysUserAccess)Session["access"]).EndDate;
                    cResident.ModifiedBy = (int)((SysUserAccess)Session["access"]).UserCenterId;
                    cResident.ModifiedDate = DateTime.Now;
                    cResident.Update();
                    count++;
                }
            }

            if (count == 0)
                Master.Master.Message("Seleccione los médicos residentes a los cuales desea promover.", "", 2);
            else
            {
                Master.Master.Message("Los Médicos Residentes seleccionados han sido promovidos con éxito.", "", 3);
                btnCancel_Click(null, null);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            grvResidents.DataSource = null;
            grvResidents.DataBind();
        }
        #endregion

        protected void chkResidentHeader_CheckedChanged(object sender, EventArgs e)
        {
            GenForms.CheckItemsInGridView(ref grvResidents, "chkResidentHeader", "chkResident");
        }
    }
}