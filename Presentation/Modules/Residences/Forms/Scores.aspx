﻿<%@ Page Title="Calificaciones" Language="C#" MasterPageFile="~/Modules/Residences/Residences.master"
    AutoEventWireup="true" CodeBehind="Scores.aspx.cs" Inherits="Presentation.Modules.Residences.Forms.Scores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Calificaciones</h2>
    <fieldset>
        <legend>Julio - Octubre</legend>
        <asp:GridView ID="grvQ1" runat="server" class="table table-condensed table-striped table-bordered"
            AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="residenceName" HeaderText="Residencia" SortExpression="residenceName" />
                <asp:BoundField DataField="gradeName" HeaderText="Año" SortExpression="gradeName" />
                <asp:BoundField DataField="jul" HeaderText="Jul" SortExpression="jul" />
                <asp:BoundField DataField="aug" HeaderText="Ago" SortExpression="aug" />
                <asp:BoundField DataField="Sep" HeaderText="Sep" SortExpression="Sep" />
                <asp:BoundField DataField="oct" HeaderText="Oct" SortExpression="oct" />
                <asp:BoundField DataField="q1" HeaderText="Promedio" SortExpression="q1" />
            </Columns>
        </asp:GridView>
    </fieldset>
    <fieldset>
        <legend>Noviembre - Diciembre</legend>
        <asp:GridView ID="grvQ2" runat="server" class="table table-condensed table-striped table-bordered"
            AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="residenceName" HeaderText="Residencia" SortExpression="residenceName" />
                <asp:BoundField DataField="gradeName" HeaderText="Año" SortExpression="gradeName" />
                <asp:BoundField DataField="nov" HeaderText="Nov" SortExpression="nov" />
                <asp:BoundField DataField="dec" HeaderText="Dic" SortExpression="dec" />
                <asp:BoundField DataField="jan" HeaderText="Ene" SortExpression="jan" />
                <asp:BoundField DataField="feb" HeaderText="Feb" SortExpression="feb" />
                <asp:BoundField DataField="q2" HeaderText="Promedio" SortExpression="q2" />
            </Columns>
        </asp:GridView>
    </fieldset>
    <fieldset>
        <legend>Marzo - Junio</legend>
        <asp:GridView ID="grvQ3" runat="server" class="table table-condensed table-striped table-bordered"
            AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="residenceName" HeaderText="Residencia" SortExpression="residenceName" />
                <asp:BoundField DataField="gradeName" HeaderText="Año" SortExpression="gradeName" />
                <asp:BoundField DataField="mar" HeaderText="Mar" SortExpression="mar" />
                <asp:BoundField DataField="apr" HeaderText="Abr" SortExpression="apr" />
                <asp:BoundField DataField="may" HeaderText="May" SortExpression="may" />
                <asp:BoundField DataField="jun" HeaderText="Jun" SortExpression="jun" />
                <asp:BoundField DataField="q3" HeaderText="Promedio" SortExpression="q3" />
            </Columns>
        </asp:GridView>
    </fieldset>
    <fieldset>
        <legend>Promedio General</legend>
        <asp:GridView ID="grvAvg" runat="server" class="table table-condensed table-striped table-bordered"
            AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="residenceName" HeaderText="Residencia" SortExpression="residenceName" />
                <asp:BoundField DataField="gradeName" HeaderText="Año" SortExpression="gradeName" />
                <asp:BoundField DataField="teachingYear" HeaderText="Periodo" SortExpression="teachingYear" />
                <asp:BoundField DataField="average" HeaderText="Promedio" SortExpression="average" />
            </Columns>
        </asp:GridView>
    </fieldset>
</asp:Content>
