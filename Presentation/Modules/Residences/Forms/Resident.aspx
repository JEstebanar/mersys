﻿<%@ Page Title="Residente" Language="C#" MasterPageFile="~/Modules/Residences/Residences.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeBehind="Resident.aspx.cs" Inherits="Presentation.Modules.Residences.Forms.Resident" %>

<%@ MasterType VirtualPath="~/Modules/Residences/Residences.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="accordion" id="Accordion">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                    Residentes </a>
            </div>
            <div id="collapseOne" class="accordion-body collapse">
                <div class="accordion-inner">
                    <asp:UpdatePanel ID="upanelSearch" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <div class="control-group">
                                <div class="form-search">
                                    <asp:TextBox ID="txtSearchName" runat="server" CssClass="textEntry" placeholder="Nombre"
                                        AutoPostBack="True" OnTextChanged="txtSearchName_TextChanged"></asp:TextBox>
                                    <asp:Button ID="btnSearchResidents" runat="server" Text="Buscar" class="btn" OnClick="btnSearchResidents_Click" />
                                    <%--<asp:Button ID="btnCreateResident" runat="server" Text="Crear Nuevo" class="btn btn-info"
                                        OnClick="btnCreateResident_Click" />--%>
                                </div>
                            </div>
                            <div class="control-group">
                                <asp:GridView ID="grvResidents" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                                    AutoGenerateColumns="False" DataKeyNames="residentId,personId,generalId,emergencyContactId,fullName,residenceName,gradeName,emergencyGeneralId"
                                    OnSelectedIndexChanged="grvResidents_SelectedIndexChanged" 
                                    ShowHeader="False" AllowPaging="True" 
                                    onpageindexchanging="grvResidents_PageIndexChanging" PageSize="7">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="personSelect" runat="server" CommandArgument='<%# Eval("residentId") %>'
                                                    CommandName="Select" ToolTip="Seleccionar" Text='<%# Eval("fullName") %>'>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="txtSearchName" EventName="TextChanged" />
                            <asp:AsyncPostBackTrigger ControlID="btnSearchResidents" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <asp:ValidationSummary ID="ResidentValidationSummary" runat="server" CssClass="alert alert-danger"
        ValidationGroup="ResidentValidationGroup" />
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#Resident" data-toggle="tab">Residente</a></li>
            <%--<li><a href="#Rotation" data-toggle="tab">Rotación</a></li>--%>
            <li><a href="#EmergencyInfo" data-toggle="tab">Datos de Emergencia</a></li>
            <li><a href="#Status" data-toggle="tab">Estado del Residente</a></li>
        </ul>
        <div class="tab-content">
            <div id="Resident" class="tab-pane active">
                <asp:UpdatePanel ID="upanelResident" runat="server">
                    <ContentTemplate>
                        <fieldset>
                            <legend>Especificaciones</legend>
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label">
                                        Exequátur</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtExequatur" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ExequaturRequired" runat="server" ControlToValidate="txtExequatur"
                                            CssClass="failureNotification" ErrorMessage="Exequátur es requerido." ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Matrícula</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtEnrollment" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="EnrollmentRequired" runat="server" ControlToValidate="txtenrollment"
                                            CssClass="failureNotification" ErrorMessage="Matrícula es requerida." ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                                        <asp:FilteredTextBoxExtender ID="txtEnrollmentFilteredTextBoxExtender" runat="server"
                                            TargetControlID="txtEnrollment" ValidChars="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz">
                                        </asp:FilteredTextBoxExtender>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Especialidad</legend>
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label">
                                        Universidad</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlUniversities" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="UniversityRequired" runat="server" ControlToValidate="ddlUniversities"
                                            CssClass="failureNotification" ErrorMessage="Universidad es requerida." ValidationGroup="ResidentValidationGroup"
                                            InitialValue="0">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="control-label">
                                        Especialidad</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlResidences" runat="server">
                                        </asp:DropDownList>
                                        
                                        <asp:RequiredFieldValidator ID="ddlResidencesRequired" runat="server" ControlToValidate="ddlResidences"
                                            CssClass="failureNotification" ErrorMessage="Especialidad es requerida." ValidationGroup="ResidentValidationGroup"
                                            InitialValue="0">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Residencia Actual</label>
                                    <div class="controls">
                                        <span class="label label-success"><asp:Label ID="lblCurrentResidence" runat="server"></asp:Label></span>                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Grado Actual</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlGrade" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="ddlGradeRequired" runat="server" ControlToValidate="ddlGrade"
                                            CssClass="failureNotification" ErrorMessage="Grado es requerido." ValidationGroup="ResidentValidationGroup"
                                            InitialValue="0">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Fecha de Inicio</label>
                                    <div class="controls">
                                        <div class="input-append">
                                            <asp:TextBox ID="txtStartDateResidence" runat="server" size="16" type="text" value=""></asp:TextBox>
                                            <asp:MaskedEditExtender ID="MaskedEditExtenderStartDateResidence" runat="server"
                                                TargetControlID="txtStartDateResidence" MaskType="Date" Mask="99/99/9999">
                                            </asp:MaskedEditExtender>
                                            <asp:CalendarExtender ID="CalendarExtenderStartDateResidence" runat="server" TargetControlID="txtStartDateResidence"
                                                PopupButtonID="calendarStartDateResidence">
                                            </asp:CalendarExtender>
                                            <%--<span class="add-on"><i class="icon-remove"></i></span>--%>
                                            <span class="add-on"><i id="calendarStartDateResidence" class="icon-calendar"></i>
                                            </span>
                                        <asp:RequiredFieldValidator ID="txtStartDateResidenceRequired" runat="server" ControlToValidate="txtStartDateResidence"
                                            CssClass="failureNotification" ErrorMessage="Fecha de Inicio es requerida." ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <asp:GridView ID="grvResidences" runat="server" Width="100%" AutoGenerateColumns="False"
                                    class="table table-striped table-bordered">
                                    <Columns>
                                        <asp:BoundField DataField="residenceName" HeaderText="Residencia" ReadOnly="True"
                                            SortExpression="residenceName" />
                                        <asp:BoundField DataField="gradeName" HeaderText="Año" ReadOnly="True" SortExpression="gradeName" />
                                        <asp:BoundField DataField="startDate" HeaderText="Fecha de Inicio" ReadOnly="True"
                                            SortExpression="startDate" />
                                        <asp:BoundField DataField="endDate" HeaderText="Fecha de Fin" ReadOnly="true" SortExpression="endDate" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </fieldset>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="Rotation" class="tab-pane">
                <asp:UpdatePanel ID="upanelRotation" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <fieldset>
                            <legend>Rotaciones</legend>
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label">
                                        Tipo de Rotación</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlRotationType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRotationType_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="divCenter" runat="server" class="control-group">
                                    <label class="control-label">
                                        Centro</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlRotationCenters" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Departamento</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlDepartments" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Fecha de Inicio</label>
                                    <div class="controls">
                                        <div class="input-append">
                                            <asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox>
                                            <asp:MaskedEditExtender ID="MaskedEditExtenderStartDate" runat="server" TargetControlID="txtStartDate"
                                                MaskType="Date" Mask="99/99/9999">
                                            </asp:MaskedEditExtender>
                                            <asp:CalendarExtender ID="CalendarExtenderStartDate" runat="server" TargetControlID="txtStartDate"
                                                PopupButtonID="calendarStartDate">
                                            </asp:CalendarExtender>
                                            <%--<span class="add-on"><i class="icon-remove"></i></span>--%>
                                            <span class="add-on"><i id="calendarStartDate" class="icon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Fecha de Fin</label>
                                    <div class="controls">
                                        <div class="input-append">
                                            <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
                                            <asp:MaskedEditExtender ID="MaskedEditExtenderEndDate" runat="server" TargetControlID="txtEndDate"
                                                MaskType="Date" Mask="99/99/9999">
                                            </asp:MaskedEditExtender>
                                            <asp:CalendarExtender ID="CalendarExtenderEndDate" runat="server" TargetControlID="txtEndDate"
                                                PopupButtonID="calendarEndDate">
                                            </asp:CalendarExtender>
                                            <span class="add-on"><i id="calendarEndDate" class="icon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Nota</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtRotationNote" runat="server" TextMode="MultiLine" Rows="3" Width="98%"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="control-group">
                                <asp:GridView ID="grvRotations" runat="server" Width="100%" AutoGenerateColumns="false"
                                    class="table table-striped table-bordered" DataKeyNames="rotationId,startDate,endDate">
                                    <Columns>
                                        <asp:BoundField DataField="departmentName" HeaderText="Departamento" ReadOnly="True"
                                            SortExpression="departmentName" />
                                        <asp:BoundField DataField="showStartDate" HeaderText="Fecha de Inicio" ReadOnly="True"
                                            SortExpression="showStartDate" />
                                        <asp:BoundField DataField="showEndDate" HeaderText="Fecha de Fin" ReadOnly="True" SortExpression="showEndDate" />
                                        <asp:BoundField DataField="rotationNote" HeaderText="Nota" ReadOnly="True" SortExpression="rotationNote" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            </label>
                        </fieldset>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlRotationType" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div id="EmergencyInfo" class="tab-pane">
                <asp:UpdatePanel ID="upanelEmergencyInfo" runat="server">
                    <ContentTemplate>
                        <fieldset>
                            <legend>Datos de Emergencia</legend>
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label">
                                        Primer Nombre</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtName" runat="server" placeholder=""></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="NameRequired" runat="server" ControlToValidate="txtName"
                                            CssClass="failureNotification" ErrorMessage="Nombre es requerido." ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Segundo Nombre</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtMiddleName" runat="server" placeholder=""></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Primer Apellido</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtFirstLastName" runat="server" placeholder=""></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="txtFirstLastNameRequired" runat="server" ControlToValidate="txtFirstLastName"
                                            CssClass="failureNotification" ErrorMessage="Apellido es requerido." ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Segundo Apellido</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtSecondLastName" runat="server" placeholder=""></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Parentesco</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlRelationship" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="ddlRelationshipRequired" runat="server" ControlToValidate="ddlRelationship"
                                            CssClass="failureNotification" ErrorMessage="Parentesco es requerido." ValidationGroup="ResidentValidationGroup"
                                            InitialValue="0">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                <label class="control-label">
                                    Casa</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtHomePhone" runat="server" MaxLength="10"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="txtHomePhoneFilteredTextBoxExtender" runat="server"
                                        TargetControlID="txtHomePhone" ValidChars="0123456789">
                                    </asp:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Celular</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtCellPhone" runat="server" MaxLength="10"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="txtCellPhoneFilteredTextBoxExtender" runat="server"
                                        TargetControlID="txtCellPhone" ValidChars="0123456789">
                                    </asp:FilteredTextBoxExtender>
                                </div>
                            </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Informacón Médica</legend>
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label">
                                        Alergías</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtAllergies" runat="server" TextMode="MultiLine" Rows="3" Width="97%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Medicamentos</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtDrugs" runat="server" TextMode="MultiLine" Rows="3" Width="97%"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="Status" class="tab-pane">
                <asp:UpdatePanel ID="upanelStatus" runat="server">
                    <ContentTemplate>
                        <fieldset>
                            <legend>Estado</legend>
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label">
                                        Estado</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlStatus" runat="server" Enabled="false" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="controls">
                                        <asp:Label ID="lblStatus" runat="server" class="text-info"></asp:Label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Centro</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlTransferCenters" runat="server" Enabled="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Documento</label>
                                    <div class="controls">
                                        <asp:FileUpload ID="fuTransferDocument" runat="server" Enabled="false" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Comentario</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Rows="3" Width="98%"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <asp:GridView ID="grvComments" runat="server" class="table table-condensed table-striped table-bordered"
                                    AutoGenerateColumns="False" DataKeyNames="commentId">
                                    <Columns>
                                        <asp:BoundField DataField="comment" HeaderText="Comentario" ReadOnly="True" SortExpression="comment" />
                                        <asp:BoundField DataField="statusName" HeaderText="Cambiado a" SortExpression="statusName" />
                                        <asp:BoundField DataField="createdDate" HeaderText="Fecha" SortExpression="createdDate" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </fieldset>
                    </ContentTemplate>
                    <%--<Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlStatus" EventName="SelectedIndexChanged" />
                    </Triggers>--%>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnSave_Click"
            ValidationGroup="ResidentValidationGroup" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
    </div>
</asp:Content>
