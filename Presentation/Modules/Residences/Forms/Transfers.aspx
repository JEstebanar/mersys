﻿<%@ Page Title="Transferencias" Language="C#" MasterPageFile="~/Modules/Residences/Residences.master"
    AutoEventWireup="true" CodeBehind="Transfers.aspx.cs" Inherits="Presentation.Modules.Residences.Forms.Transfers" %>

<%@ MasterType VirtualPath="~/Modules/Residences/Residences.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upanelTransfers" runat="server">
        <ContentTemplate>
            <fieldset>
                <legend>Transferencias Pendientes</legend>
                <div class="controls">
                    <asp:GridView ID="grvResidents" runat="server" class="table table-condensed table-striped table-bordered"
                        AutoGenerateColumns="False" DataKeyNames="residentId,residentName,residenceName,gradeName,centerName,transferDocument"
                        OnSelectedIndexChanged="grvResidents_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="residentName" HeaderText="Residente" SortExpression="residentName" />
                            <asp:BoundField DataField="residenceName" HeaderText="Residencia" SortExpression="residenceName" />
                            <asp:BoundField DataField="gradeName" HeaderText="Grado" SortExpression="gradeName" />
                            <asp:BoundField DataField="centerName" HeaderText="Procedente de" SortExpression="centerName" />
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgSelect" runat="server" CausesValidation="False" CommandName="Select"
                                        class="icon-edit" ImageUrl="~/Styles/Images/blue/pencil.png" ToolTip="Seleccionar"
                                        Text="Editar" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="divDetail" runat="server">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">
                                Residente</label>
                            <div class="controls">
                                <asp:TextBox ID="txtResidentName" runat="server" disabled="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Residencia</label>
                            <div class="controls">
                                <asp:TextBox ID="txtResidence" runat="server" disabled="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Grado</label>
                            <div class="controls">
                                <asp:TextBox ID="txtGrade" runat="server" disabled="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Procedente de</label>
                            <div class="controls">
                                <asp:TextBox ID="txtCenter" runat="server" disabled="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Documento</label>
                            <div class="controls">
                                <a id="showDocument" runat="server" href="#myModal" role="button" class="btn btn-link"
                                    data-toggle="modal">Ver Documento Adjunto</a>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Comentario</label>
                            <div class="controls">
                                <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Rows="3" Width="98%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Comentario</label>
                            <div class="controls">
                                <asp:GridView ID="grvComments" runat="server" class="table table-condensed table-striped table-bordered"
                                    AutoGenerateColumns="False" ShowHeader="false" DataKeyNames="commentId">
                                    <Columns>
                                        <asp:BoundField DataField="comment" HeaderText="Comentario" ReadOnly="True" SortExpression="comment" />
                                        <asp:BoundField DataField="createdDate" HeaderText="Fecha" SortExpression="createdDate" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <asp:Button ID="btnSave" runat="server" Text="Trasferir" class="btn btn-warning"
                            OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
                    </div>
                </div>
            </fieldset>
            <!-- Modal -->
            <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×</button>
                    <h3 id="myModalLabel">
                        Documento</h3>
                </div>
                <div class="modal-body">
                    <asp:Image ID="imgDocument" runat="server" />
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="grvResidents" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
