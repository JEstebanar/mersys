﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using General.Entities;
using General.GeneralCommons;
using General.GeneralPhones;
using General.Generals;
using SystemSecurity.Centers;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Administration.Forms
{
    public partial class Centers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.Centers))
                    {
                        if (!IsPostBack)
                        {
                            FillCenters();
                            divDetails.Visible = false;
                            chkStatus.Checked = true;
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillCenters()
        {
            grvCenters.DataSource = SysCenters.Search();
            grvCenters.DataBind();
            divDetails.Visible = false;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            grvCenters.SelectedIndex = -1;
            divDetails.Visible = true;
            txtCenterName.Text = string.Empty;
            txtPhone.Text = string.Empty;
            chkStatus.Checked = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int nGeneralId = 0;
            SysCenters nCenter;
            GenGeneralPhones nPhone;
            if (grvCenters.SelectedIndex > -1)
            {
                nCenter = SysCenters.Get(Convert.ToInt32(grvCenters.SelectedDataKey[0]));
                nPhone = GenGeneralPhones.GetByTheOnlyGeneralId(Convert.ToInt32(grvCenters.SelectedDataKey[1]));
            }
            else
            {
                GenGenerals nGeneral = new GenGenerals();
                nGeneral.NationalityId = (int)GeneralCommon.Nationalities.Dominican;
                nGeneralId = nGeneral.Insert();

                GenEntities nEntity = new GenEntities();
                nEntity.GeneralId = nGeneralId;
                nEntity.EntityTypeId = (int)GeneralCommon.EntityTypes.Center;
                nEntity.CenterId = null;
                nEntity.EntityStatus = true;
                nEntity.CreatedBy = (int)((SysUserAccess)Session["access"]).UserCenterId;
                nEntity.CreatedDate = DateTime.Now;
                nEntity.Insert();

                nCenter = new SysCenters();
                nCenter.GeneralId = nGeneralId;

                nPhone = new GenGeneralPhones();
                nPhone.GeneralId = nGeneralId;
            }

            nCenter.CenterName = txtCenterName.Text;
            nCenter.CenterStatus = chkStatus.Checked;

            nPhone.PhoneNumber=txtPhone.Text;
            nPhone.PhoneTypeId = (int)GeneralCommon.PhoneTypes.Work;
            nPhone.PhoneStatus = true;

            if (grvCenters.SelectedIndex > -1)
            {
                nCenter.Update();

                nPhone.Update();
                nPhone.ModifiedBy = (int)((SysUserAccess)Session["access"]).UserCenterId;
                nPhone.ModifiedDate = DateTime.Now;
            }
            else
            {
                nCenter.Insert();

                nPhone.CreatedBy = (int)((SysUserAccess)Session["access"]).UserCenterId;
                nPhone.CreatedDate = DateTime.Now;
                nPhone.Insert();
            }

            FillCenters();
            Master.Master.Message("¡Datos guardados exitosamente!.", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divDetails.Visible = false;
        }

        protected void grvCenter_SelectedIndexChanged(object sender, EventArgs e)
        {
            divDetails.Visible = true;
            SysCenters cCenter = SysCenters.Get(Convert.ToInt32(grvCenters.SelectedDataKey[0]));
            txtCenterName.Text = cCenter.CenterName;

            GenGeneralPhones cPhone = GenGeneralPhones.GetByTheOnlyGeneralId(Convert.ToInt32(grvCenters.SelectedDataKey[1]));
            txtPhone.Text = cPhone.PhoneNumber;

            chkStatus.Checked = Convert.ToBoolean(cCenter.CenterStatus);
        }
    }
}