﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using General.GeneralCommons;
using Letters.Letters;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Administration.Forms
{
    public partial class Letters : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.Letters))
                    {
                        if (!IsPostBack)
                        {
                            FillLetters();
                            divDetail.Visible = false;
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillLetters()
        {
            grvLetters.DataSource = LetLetters.Search();
            grvLetters.DataBind();
            divDetail.Visible = false;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            grvLetters.SelectedIndex = -1;
            divDetail.Visible = true;
            txtName.Text = string.Empty;
            chkStatus.Checked = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            LetLetters nLetter;
            if (grvLetters.SelectedIndex > -1)
                nLetter = LetLetters.Get(Convert.ToInt32(grvLetters.SelectedDataKey[0]));
            else
                nLetter = new LetLetters();

            nLetter.LetterName = txtName.Text;
            nLetter.LetterStatus = chkStatus.Checked;

            if (grvLetters.SelectedIndex > -1)
                nLetter.Update();
            else
                nLetter.Insert();

            FillLetters();
            Master.Master.Message("¡Datos guardados exitosamente!.", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divDetail.Visible = false;
        }

        protected void grvLetter_SelectedIndexChanged(object sender, EventArgs e)
        {
            divDetail.Visible = true;
            LetLetters cLetter = LetLetters.Get(Convert.ToInt32(grvLetters.SelectedDataKey[0]));
            txtName.Text = cLetter.LetterName;
            chkStatus.Checked = Convert.ToBoolean(cLetter.LetterStatus);
        }
    }
}