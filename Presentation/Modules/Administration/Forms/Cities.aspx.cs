﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI.WebControls;
using General.Cities;
using General.GeneralCommons;
using General.States;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Administration.Forms
{
    public partial class Cities : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.States))
                    {
                        if (!IsPostBack)
                        {
                            FillCities();
                            divDetail.Visible = false;

                            ddlStates.DataTextField = "stateName";
                            ddlStates.DataValueField = "stateId";
                            ddlStates.DataSource = GenStates.GetStatesByCountryId(null);
                            ddlStates.DataBind();
                            ddlStates.Items.Insert(0, new ListItem("-- Seleccione --", "0"));
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillCities()
        {
            grvCities.DataSource = GenCities.Search();
            grvCities.DataBind();
            divDetail.Visible = false;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            grvCities.SelectedIndex = -1;
            divDetail.Visible = true;
            txtCityName.Text = string.Empty;
            ddlStates.SelectedIndex = -1;
            chkStatus.Checked = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            GenCities nCity;
            if (grvCities.SelectedIndex > -1)
                nCity = GenCities.Get(Convert.ToInt32(grvCities.SelectedDataKey[0]));
            else
                nCity = new GenCities();

            nCity.CityName = txtCityName.Text;
            nCity.CityStatus = chkStatus.Checked;
            nCity.StateId = Convert.ToInt32(ddlStates.SelectedValue);

            if (grvCities.SelectedIndex > -1)
                nCity.Update();
            else
                nCity.Insert();

            FillCities();
            Master.Master.Message("¡Datos guardados exitosamente!.", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divDetail.Visible = false;
        }

        protected void grvCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            divDetail.Visible = true;
            GenCities cCity = GenCities.Get(Convert.ToInt32(grvCities.SelectedDataKey[0]));
            txtCityName.Text = cCity.CityName;
            ddlStates.SelectedValue = cCity.StateId.ToString();
            chkStatus.Checked = Convert.ToBoolean(cCity.CityStatus);
        }
    }
}