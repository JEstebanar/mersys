﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI.WebControls;
using General.Forms;
using General.GeneralCommons;
using SystemSecurity.CenterModules;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Administration.Forms
{
    public partial class CenterModules : System.Web.UI.Page
    {
        private IList<SysCenterModules.CenterModules> AssignedModules { get { return (IList<SysCenterModules.CenterModules>)ViewState["assignedModules"]; } set { ViewState["assignedModules"] = value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.CenterModules))
                    {
                        if (!IsPostBack)
                        {
                            AssignedModules = SysCenterModules.GetByCenterId((int)((SysUserAccess)Session["access"]).CenterId);
                            FillCenterModules();
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillCenterModules()
        {
            grvCenterModules.DataSource = AssignedModules;
            grvCenterModules.DataBind();

            GenForms.DisableCheckInGridview(grvCenterModules, "moduleId", (int)GeneralCommon.Modules.Administration, "chkAssigned");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in grvCenterModules.Rows)
            {
                SysCenterModules CenterModule = new SysCenterModules();
                CenterModule.CenterId = (int)((SysUserAccess)Session["access"]).CenterId;
                CenterModule.ModuleId = (int)grvCenterModules.DataKeys[row.RowIndex].Values["moduleId"];
                CenterModule.CenterModuleStatus = (row.FindControl("chkAssigned") as CheckBox).Checked;
                CenterModule.InsertOrUpdate();
            }

            AssignedModules = SysCenterModules.GetByCenterId((int)((SysUserAccess)Session["access"]).CenterId);
            FillCenterModules();
            Master.Master.Message("¡Datos guardados exitosamente!.", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            FillCenterModules();
        }
    }
}