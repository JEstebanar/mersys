﻿<%@ Page Title="Registro de Módulos" Language="C#" MasterPageFile="~/Modules/Administration/Administration.master"
    AutoEventWireup="true" CodeBehind="ModuleRegister.aspx.cs" Inherits="Presentation.Modules.Administration.Forms.ModuleRegister" %>

<%@ MasterType VirtualPath="~/Modules/Administration/Administration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Módulos</legend>
        <div class="form-search">
            <asp:Button ID="btnNew" runat="server" Text="Nuevo" class="btn" 
                onclick="btnNew_Click" />
        </div>
        <br />
        <div class="control-group">
            <asp:GridView ID="grvModules" runat="server" class="table table-condensed table-striped table-bordered"
                AutoGenerateColumns="False" DataKeyNames="moduleId" 
                onselectedindexchanged="grvModules_SelectedIndexChanged">
                <Columns>
                    <asp:BoundField DataField="moduleName" HeaderText="Modulo" SortExpression="moduleName" />
                    <asp:BoundField DataField="moduleDescription" HeaderText="Descripción" SortExpression="moduleDescription" />
                    <asp:BoundField DataField="moduleOrder" HeaderText="Orden" SortExpression="moduleOrder" />
                    <asp:BoundField DataField="moduleStatus" HeaderText="Estado" SortExpression="moduleStatus" />
                    <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
                </Columns>
            </asp:GridView>
        </div>
    </fieldset>
    <div id="divDetail" runat="server">
        <fieldset>
            <legend>Detalles</legend>
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">
                        Nombre</label>
                    <div class="controls">
                        <asp:TextBox ID="txtModuleName" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Descripción</label>
                    <div class="controls">
                        <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Orden</label>
                    <div class="controls">
                        <asp:TextBox ID="txtOrder" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <asp:CheckBox ID="chkStatus" runat="server" Text="Activo" />
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="form-actions">
        <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
    </div>
</asp:Content>
