﻿<%@ Page Title="Reportes" Language="C#" MasterPageFile="~/Modules/Administration/Administration.master"
    AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="Presentation.Modules.Administration.Forms.Reports" %>

<%@ MasterType VirtualPath="~/Modules/Administration/Administration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Reportes</legend>
        <div class="form-inline">
            <asp:Button ID="btnNew" runat="server" Text="Nuevo" class="btn" OnClick="btnNew_Click" />
        </div>
        <br />
        <asp:GridView ID="grvReports" runat="server" class="table table-condensed table-striped table-bordered"
            AutoGenerateColumns="False" DataKeyNames="reportId" OnSelectedIndexChanged="grvReport_SelectedIndexChanged">
            <Columns>
                <asp:BoundField DataField="reportName" HeaderText="Reporte" SortExpression="reportName" />
                <asp:BoundField DataField="reportStatus" HeaderText="Estado" SortExpression="reportStatus" />
                <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
            </Columns>
        </asp:GridView>
    </fieldset>
    <div id="divDetail" runat="server">
        <fieldset>
            <legend>Detalles</legend>
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">
                        Nombre</label>
                    <div class="controls">
                        <asp:TextBox ID="txtReportName" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Tipo</label>
                    <div class="controls">
                        <asp:DropDownList ID="ddlReportType" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <asp:CheckBox ID="chkStatus" runat="server" Text="Activo" />
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="form-actions">
        <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
    </div>
</asp:Content>
