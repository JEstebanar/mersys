﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/Administration/Administration.master"
    AutoEventWireup="true" CodeBehind="Countries.aspx.cs" Inherits="Presentation.Modules.Administration.Forms.Countries" %>

<%@ MasterType VirtualPath="~/Modules/Administration/Administration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Países</legend>
        <div class="form-inline">
            <asp:Button ID="btnNew" runat="server" Text="Nuevo" class="btn" OnClick="btnNew_Click" />
        </div>
        <br />
        <asp:GridView ID="grvCountries" runat="server" class="table table-condensed table-striped table-bordered"
            AutoGenerateColumns="False" DataKeyNames="countryId" OnSelectedIndexChanged="grvCountry_SelectedIndexChanged">
            <Columns>
                <asp:BoundField DataField="CountryName" HeaderText="País" SortExpression="CountryName" />
                <asp:BoundField DataField="countryStatus" HeaderText="Estado" SortExpression="countryStatus" />
                <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
            </Columns>
        </asp:GridView>
    </fieldset>
    <div id="divDetail" runat="server">
        <fieldset>
            <legend>Detalles</legend>
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">
                        Nombre</label>
                    <div class="controls">
                        <asp:TextBox ID="txtReportName" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <asp:CheckBox ID="chkStatus" runat="server" Text="Activo" />
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="form-actions">
        <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
    </div>
</asp:Content>
