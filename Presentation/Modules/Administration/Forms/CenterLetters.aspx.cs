﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SystemSecurity.SecurityAccess;
using SystemSecurity.Screens;
using General.GeneralCommons;
using SystemSecurity.CenterReports;
using SystemSecurity.UserAccess;
using SystemSecurity.SysError;
using System.IO;
using System.Reflection;
using SystemSecurity.CenterLetters;
using Letters.Letters;

namespace Presentation.Modules.Administration.Forms
{
    public partial class CenterLetters : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.CenterLetters))
                    {
                        if (!IsPostBack)
                        {
                            FillCenterLetters();
                            FillLetters();
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillCenterLetters()
        {
            grvCenterLetters.DataSource = SysCenterLetters.GetByCenterId((int)((SysUserAccess)Session["access"]).CenterId);
            grvCenterLetters.DataBind();
            divDetail.Visible = false;
        }

        private void FillLetters()
        {
            ddlLetters.Items.Clear();
            ddlLetters.DataTextField = "letterName";
            ddlLetters.DataValueField = "letterId";
            ddlLetters.DataSource = LetLetters.GetLetters();
            ddlLetters.DataBind();
            ddlLetters.Items.Insert(0, new ListItem("-- Cartas --", "0"));
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            grvCenterLetters.SelectedIndex = -1;
            divDetail.Visible = true;
            ddlLetters.SelectedIndex = -1;
            txtPrice.Text = "0";
            chkStatus.Checked = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (ddlLetters.SelectedIndex == -1)
            {
                Master.Master.Message("Seleccione una Carta.", "", 2);
                return;
            }
            
            SysCenterLetters CenterLetter;
            if (grvCenterLetters.SelectedIndex > -1)
                CenterLetter = SysCenterLetters.Get(Convert.ToInt32(grvCenterLetters.SelectedDataKey[0]));
            else
                CenterLetter = new SysCenterLetters();

            if (txtPrice.Text == string.Empty)
                txtPrice.Text = "0";
            CenterLetter.CenterId = (int)((SysUserAccess)Session["access"]).CenterId;
            

            if (grvCenterLetters.SelectedIndex > -1)
            {
                if (CenterLetter.LetterPrice == Convert.ToDecimal(txtPrice.Text))
                {
                    CenterLetter.CenterLetterStatus = chkStatus.Checked;
                    CenterLetter.Update();
                }
                else
                {
                    CenterLetter.CenterLetterStatus = false;
                    CenterLetter.Update();

                    CenterLetter.VersionNumber++;
                    CenterLetter.LetterPrice = Math.Round(Convert.ToDecimal(txtPrice.Text), 0);
                    CenterLetter.CenterLetterStatus = true;
                    CenterLetter.Insert();
                }
            }
            else
            {
                CenterLetter.LetterId = Convert.ToInt32(ddlLetters.SelectedValue);
                CenterLetter.LetterPrice = Math.Round(Convert.ToDecimal(txtPrice.Text), 0);
                CenterLetter.VersionNumber = 1;
                CenterLetter.CenterLetterStatus = true;
                CenterLetter.Insert();
            }

            FillCenterLetters();
            Master.Master.Message("¡Datos guardados exitosamente!.", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divDetail.Visible = false;
        }

        protected void grvLetter_SelectedIndexChanged(object sender, EventArgs e)
        {
            divDetail.Visible = true;
            SysCenterLetters CenterLetter = SysCenterLetters.Get(Convert.ToInt32(grvCenterLetters.SelectedDataKey[0]));
            ddlLetters.SelectedValue = CenterLetter.LetterId.ToString();
            txtPrice.Text = CenterLetter.LetterPrice.ToString();
            chkStatus.Checked = Convert.ToBoolean(CenterLetter.CenterLetterStatus);
        }
    }
}
