﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código. 
// </generado automáticamente>
//------------------------------------------------------------------------------

namespace Presentation.Modules.Administration.Forms {
    
    
    public partial class EmployeeRegister {
        
        /// <summary>
        /// Control wucGeneralRegister1.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Presentation.Modules.General.UserControls.wucGeneralRegister wucGeneralRegister1;
        
        /// <summary>
        /// Propiedad Master.
        /// </summary>
        /// <remarks>
        /// Propiedad generada automáticamente.
        /// </remarks>
        public new Presentation.Modules.Administration.Administration Master {
            get {
                return ((Presentation.Modules.Administration.Administration)(base.Master));
            }
        }
    }
}
