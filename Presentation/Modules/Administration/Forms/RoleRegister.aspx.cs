﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using General.GeneralCommons;
using SystemSecurity.Roles;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Administration.Forms
{
    public partial class RoleRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.RoleRegister))
                    {
                        if (!IsPostBack)
                        {
                            FillRoles();
                            divDetail.Visible = false;
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillRoles()
        {
            grvRoles.DataSource = SysRoles.Search();
            grvRoles.DataBind();
            divDetail.Visible = false;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            grvRoles.SelectedIndex = -1;
            divDetail.Visible = true;
            txtRoleName.Text = string.Empty;
            txtDescription.Text = string.Empty;
            txtOrder.Text = string.Empty;
            chkStatus.Checked = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SysRoles nRole;
            if (grvRoles.SelectedIndex > -1)
                nRole = SysRoles.Get(Convert.ToInt32(grvRoles.SelectedDataKey[0]));
            else
                nRole = new SysRoles();

            nRole.RoleName = txtRoleName.Text;
            nRole.RoleDescription = txtDescription.Text;
            nRole.RoleOrder = Convert.ToDecimal(txtOrder.Text);
            nRole.RoleSecurityId = 1;
            nRole.RoleStatus = chkStatus.Checked;

            if (grvRoles.SelectedIndex > -1)
                nRole.Update();
            else
                nRole.Insert();

            FillRoles();
            Master.Master.Message("¡Datos guardados exitosamente!.", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divDetail.Visible = false;
        }

        protected void grvRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            divDetail.Visible = true;
            SysRoles cRole = SysRoles.Get(Convert.ToInt32(grvRoles.SelectedDataKey[0]));
            txtRoleName.Text = cRole.RoleName;
            txtDescription.Text = cRole.RoleDescription;
            txtOrder.Text = cRole.RoleOrder.ToString();
            chkStatus.Checked = Convert.ToBoolean(cRole.RoleStatus);
        }
    }
}