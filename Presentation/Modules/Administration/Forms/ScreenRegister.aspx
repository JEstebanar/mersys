﻿<%@ Page Title="Registro de Pantallas" Language="C#" MasterPageFile="~/Modules/Administration/Administration.master"
    AutoEventWireup="true" CodeBehind="ScreenRegister.aspx.cs" Inherits="Presentation.Modules.Administration.Forms.ScreenRegister" %>

<%@ MasterType VirtualPath="~/Modules/Administration/Administration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Pantallas</legend>
        <div class="form-search">
            <asp:Button ID="btnNew" runat="server" Text="Nuevo" class="btn" 
                onclick="btnNew_Click" />
        </div>
        <br />
        <div class="control-group">
            <asp:GridView ID="grvScreens" runat="server" class="table table-condensed table-striped table-bordered"
                AutoGenerateColumns="False" DataKeyNames="screenId" 
                OnSelectedIndexChanged="grvScreens_SelectedIndexChanged" AllowPaging="True" 
                onpageindexchanging="grvScreens_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="screenName" HeaderText="Pantalla" SortExpression="screenName" />
                    <asp:BoundField DataField="moduleName" HeaderText="Módulo" SortExpression="moduleName" />
                    <asp:BoundField DataField="OrderInModule" HeaderText="Orden" SortExpression="OrderInModule" />
                    <asp:BoundField DataField="screenStatus" HeaderText="Estado" SortExpression="screenStatus" />
                    <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
                </Columns>
            </asp:GridView>
        </div>
    </fieldset>
    <div id="divDetail" runat="server">
        <fieldset>
            <legend>Detalles</legend>
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">
                        Módulo</label>
                    <div class="controls">
                        <asp:DropDownList ID="ddlModules" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Padre</label>
                    <div class="controls">
                        <asp:DropDownList ID="ddlParentScreen" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Nombre</label>
                    <div class="controls">
                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Url</label>
                    <div class="controls">
                        <asp:TextBox ID="txtUrl" runat="server" Width="100%"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Orden en Módulo</label>
                    <div class="controls">
                        <asp:TextBox ID="txtOrder" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <asp:CheckBox ID="chkStatus" runat="server" Text="Activo" />
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="form-actions">
        <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
    </div>
</asp:Content>
