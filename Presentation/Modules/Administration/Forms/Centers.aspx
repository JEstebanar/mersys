﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/Administration/Administration.master"
    AutoEventWireup="true" CodeBehind="Centers.aspx.cs" Inherits="Presentation.Modules.Administration.Forms.Centers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Modules/Administration/Administration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Centros</legend>
        <div class="form-search">
            <asp:Button ID="btnNew" runat="server" Text="Nuevo" class="btn" OnClick="btnNew_Click" />
        </div>
        <br />
        <asp:GridView ID="grvCenters" runat="server" class="table table-condensed table-striped table-bordered"
            AutoGenerateColumns="False" DataKeyNames="centerId,generalId" 
            OnSelectedIndexChanged="grvCenter_SelectedIndexChanged">
            <Columns>
                <asp:BoundField DataField="centerName" HeaderText="Centro" SortExpression="centerName" />
                <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
            </Columns>
        </asp:GridView>
    </fieldset>
    <div id="divDetails" runat="server">
        <fieldset>
            <legend>Detalles</legend>
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">
                        Nombre</label>
                    <div class="controls">
                        <asp:TextBox ID="txtCenterName" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Teléfono</label>
                    <div class="controls">
                        <asp:TextBox ID="txtPhone" runat="server" MaxLength="10"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtendertxtPhone" runat="server"
                            TargetControlID="txtPhone" ValidChars="0123456789">
                        </asp:FilteredTextBoxExtender>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <asp:CheckBox ID="chkStatus" runat="server" Text="Activo" />
                    </div>
                </div>
            </div>
        </fieldset>
        <div class="form-actions">
            <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnSave_Click" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
        </div>
    </div>
</asp:Content>
