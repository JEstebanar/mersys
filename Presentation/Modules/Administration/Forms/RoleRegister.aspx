﻿<%@ Page Title="Registro de Roles" Language="C#" MasterPageFile="~/Modules/Administration/Administration.master"
    AutoEventWireup="true" CodeBehind="RoleRegister.aspx.cs" Inherits="Presentation.Modules.Administration.Forms.RoleRegister" %>

<%@ MasterType VirtualPath="~/Modules/Administration/Administration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Roles</legend>
        <div class=form-inline">
            <asp:Button ID="btnNew" runat="server" Text="Nuevo" class="btn" 
                onclick="btnNew_Click" />
        </div>
        <asp:GridView ID="grvRoles" runat="server" class="table table-condensed table-striped table-bordered"
            AutoGenerateColumns="False" DataKeyNames="roleId"
            onselectedindexchanged="grvRoles_SelectedIndexChanged">
            <Columns>
                <asp:BoundField DataField="roleName" HeaderText="Role" SortExpression="roleName" />
                <asp:BoundField DataField="roleDescription" HeaderText="Descripción" SortExpression="roleDescription" />
                <asp:BoundField DataField="roleOrder" HeaderText="Orden" SortExpression="roleOrder" />
                <asp:BoundField DataField="roleStatus" HeaderText="Estado" SortExpression="roleStatus" />
                <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
            </Columns>
        </asp:GridView>
    </fieldset>
    <div id="divDetail" runat="server">
        <fieldset><legend>Detalles</legend>
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">
                        Nombre</label>
                    <div class="controls">
                        <asp:TextBox ID="txtRoleName" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Descripción</label>
                    <div class="controls">
                        <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Orden</label>
                    <div class="controls">
                        <asp:TextBox ID="txtOrder" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <asp:CheckBox ID="chkStatus" runat="server" Text="Activo" />
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="form-actions">
        <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" 
            onclick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" 
            onclick="btnCancel_Click" />
    </div>
</asp:Content>
