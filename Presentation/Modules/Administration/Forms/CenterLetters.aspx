﻿<%@ Page Title="Cartas de Centrlo" Language="C#" MasterPageFile="~/Modules/Administration/Administration.master"
    AutoEventWireup="true" CodeBehind="CenterLetters.aspx.cs" Inherits="Presentation.Modules.Administration.Forms.CenterLetters" %>
    
<%@ MasterType VirtualPath="~/Modules/Administration/Administration.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Cartas de Centro</legend>
        <div class="form-inline">
            <asp:Button ID="btnNew" runat="server" Text="Nuevo" class="btn" OnClick="btnNew_Click" />
        </div>
        <br />
        <asp:GridView ID="grvCenterLetters" runat="server" class="table table-condensed table-striped table-bordered"
            AutoGenerateColumns="False" DataKeyNames="centerLetterId,letterId" OnSelectedIndexChanged="grvLetter_SelectedIndexChanged">
            <Columns>
                <asp:BoundField DataField="letterName" HeaderText="Carta" SortExpression="letterName" />
                <asp:BoundField DataField="letterPrice" HeaderText="Precio" SortExpression="letterPrice" />
                <asp:BoundField DataField="versionNumber" HeaderText="Versión" SortExpression="versionNumber" />
                <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
            </Columns>
        </asp:GridView>
    </fieldset>
    <div id="divDetail" runat="server">
        <fieldset>
            <legend>Detalles</legend>
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">
                        Nombre</label>
                    <div class="controls">
                        <asp:DropDownList ID="ddlLetters" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Precio</label>
                    <div class="controls">
                        <asp:TextBox ID="txtPrice" runat="server"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="txtPriceFilteredTextBoxExtender" runat="server"
                            TargetControlID="txtPrice" ValidChars="0123456789.">
                        </asp:FilteredTextBoxExtender>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <asp:CheckBox ID="chkStatus" runat="server" Text="Activa" />
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="form-actions">
        <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
    </div>
</asp:Content>
