﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI.WebControls;
using General.GeneralCommons;
using SystemSecurity.Roles;
using SystemSecurity.RoleScreens;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;
using SystemSecurity.Modules;

namespace Presentation.Modules.Administration.Forms
{
    public partial class RoleScreens : System.Web.UI.Page
    {
        private IList<SysRoleScreens.RoleScreens> AssignedScreens { get { return (IList<SysRoleScreens.RoleScreens>)ViewState["assignedScreens"]; } set { ViewState["assignedScreens"] = value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.RoleScreens))
                    {
                        if (!IsPostBack)
                        {
                            ddlRoles.DataTextField = "roleName";
                            ddlRoles.DataValueField = "roleId";
                            ddlRoles.DataSource = SysRoles.GetRoles();
                            ddlRoles.DataBind();
                            ddlRoles.Items.Insert(0, new ListItem("-- Seleccione Rol --", "0"));

                            ddlModules.DataTextField = "moduleName";
                            ddlModules.DataValueField = "moduleId";
                            ddlModules.DataSource = SysModules.GetModules();
                            ddlModules.DataBind();
                            ddlModules.Items.Insert(0, new ListItem("-- Seleccione Módulo --", "0"));
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (ddlRoles.SelectedIndex > 0)
            {
                AssignedScreens = SysRoleScreens.GetByRoleId(Convert.ToInt32(ddlRoles.SelectedValue), Convert.ToInt32(ddlModules.SelectedValue));
                grvRoleScreens.DataSource = AssignedScreens;
                grvRoleScreens.DataBind();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in grvRoleScreens.Rows)
            {
                SysRoleScreens RoleScreen = new SysRoleScreens();
                RoleScreen.RoleId = Convert.ToInt32(ddlRoles.SelectedValue);
                RoleScreen.ScreenId = (int)grvRoleScreens.DataKeys[row.RowIndex].Values["screenId"];
                RoleScreen.RoleScreenStatus = (row.FindControl("chkAssigned") as CheckBox).Checked;
                RoleScreen.InsertOrUpdate();
            }

            btnSearch_Click(null,null);
            Master.Master.Message("¡Datos guardados exitosamente!.", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ddlRoles.SelectedIndex = 0;
            btnSearch_Click(null,null);
        }
    }
}