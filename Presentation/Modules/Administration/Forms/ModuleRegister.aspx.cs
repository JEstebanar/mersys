﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using General.GeneralCommons;
using SystemSecurity.Modules;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Administration.Forms
{
    public partial class ModuleRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.ModuleRegister))
                    {
                        if (!IsPostBack)
                        {
                            FillModules();
                            divDetail.Visible = false;
                            chkStatus.Checked = true;
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillModules()
        {
            grvModules.DataSource = SysModules.Search();
            grvModules.DataBind();
            divDetail.Visible = false;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            grvModules.SelectedIndex = -1;
            divDetail.Visible = true;
            txtModuleName.Text = string.Empty;
            txtDescription.Text = string.Empty;
            txtOrder.Text = string.Empty;
            chkStatus.Checked = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SysModules nModule;
            if (grvModules.SelectedIndex > -1)
                nModule = SysModules.Get(Convert.ToInt32(grvModules.SelectedDataKey[0]));
            else
                nModule = new SysModules();

            nModule.ModuleName = txtModuleName.Text;
            nModule.ModuleDescription = txtDescription.Text;
            nModule.ModuleOrder = Convert.ToInt32(txtOrder.Text);
            nModule.ModuleStatus = chkStatus.Checked;

            if (grvModules.SelectedIndex > -1)
                nModule.Update();
            else
                nModule.Insert();

            FillModules();
            Master.Master.Message("¡Datos guardados exitosamente!.", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divDetail.Visible = false;
        }

        protected void grvModules_SelectedIndexChanged(object sender, EventArgs e)
        {
            divDetail.Visible = true;
            SysModules cModule = SysModules.Get(Convert.ToInt32(grvModules.SelectedDataKey[0]));
            txtModuleName.Text = cModule.ModuleName;
            txtDescription.Text = cModule.ModuleDescription;
            txtOrder.Text = cModule.ModuleOrder.ToString();
            chkStatus.Checked = Convert.ToBoolean(cModule.ModuleStatus);
        }
    }
}