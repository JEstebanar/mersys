﻿<%@ Page Title="Ciudades" Language="C#" MasterPageFile="~/Modules/Administration/Administration.master" AutoEventWireup="true" CodeBehind="Cities.aspx.cs" Inherits="Presentation.Modules.Administration.Forms.Cities" %>

<%@ MasterType VirtualPath="~/Modules/Administration/Administration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Ciudades</legend>
        <div class="form-inline">
            <asp:Button ID="btnNew" runat="server" Text="Nuevo" class="btn" 
                onclick="btnNew_Click" />
        </div>
        <br />
        <asp:GridView ID="grvCities" runat="server" class="table table-condensed table-striped table-bordered"
            AutoGenerateColumns="False" DataKeyNames="cityId"
            onselectedindexchanged="grvCity_SelectedIndexChanged">
            <Columns>
                <asp:BoundField DataField="cityName" HeaderText="Ciudad" SortExpression="cityName" />
                <asp:BoundField DataField="cityStatus" HeaderText="Estado" SortExpression="cityStatus" />
                <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
            </Columns>
        </asp:GridView>
    </fieldset>
    <div id="divDetail" runat="server">
        <fieldset>
            <legend>Detalles</legend>
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">
                        Nombre</label>
                    <div class="controls">
                        <asp:TextBox ID="txtCityName" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Provincia</label>
                    <div class="controls">
                        <asp:DropDownList ID="ddlStates" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <asp:CheckBox ID="chkStatus" runat="server" Text="Activo" />
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="form-actions">
        <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" 
            onclick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" 
            onclick="btnCancel_Click" />
    </div>
</asp:Content>
