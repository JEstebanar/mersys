﻿<%@ Page Title="Registro de Módulos en Centro" Language="C#" MasterPageFile="~/Modules/Administration/Administration.master"
    AutoEventWireup="true" CodeBehind="CenterModules.aspx.cs" Inherits="Presentation.Modules.Administration.Forms.CenterModules" %>

<%@ MasterType VirtualPath="~/Modules/Administration/Administration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Módulos de Centro</legend>
        <asp:GridView ID="grvCenterModules" runat="server" class="table table-condensed table-striped table-bordered"
            AutoGenerateColumns="False" DataKeyNames="moduleId">
            <Columns>
                <asp:BoundField DataField="moduleName" HeaderText="Módulo" SortExpression="moduleName" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkAssigned" runat="server" Checked='<%# Eval("assigned") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <div class="form-actions">
            <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnSave_Click" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
        </div>
    </fieldset>
</asp:Content>
