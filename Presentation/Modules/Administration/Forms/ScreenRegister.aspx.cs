﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI.WebControls;
using General.GeneralCommons;
using SystemSecurity.Modules;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Administration.Forms
{
    public partial class ScreenRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.ScreenRegister))
                    {
                        if (!IsPostBack)
                        {
                            FillScreens();
                            divDetail.Visible = false;

                            ddlModules.DataTextField = "moduleName";
                            ddlModules.DataValueField = "moduleId";
                            ddlModules.DataSource = SysModules.GetModules();
                            ddlModules.DataBind();
                            ddlModules.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

                            chkStatus.Checked = true;
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillScreens()
        {
            grvScreens.DataSource = SysScreens.Search();
            grvScreens.DataBind();
            divDetail.Visible = false;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            grvScreens.SelectedIndex = -1;
            divDetail.Visible = true;
            ddlModules.SelectedIndex = -1;
            ddlParentScreen.SelectedIndex = -1;
            txtName.Text = string.Empty;
            txtUrl.Text = string.Empty;
            txtOrder.Text = string.Empty;
            chkStatus.Checked = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (ddlModules.SelectedIndex == -1)
            {
                Master.Master.Message("Seleccione un Módulo", "", 2);
                return;
            }

            SysScreens nScreen;
            if (grvScreens.SelectedIndex > -1)
                nScreen = SysScreens.Get(Convert.ToInt32(grvScreens.SelectedDataKey[0]));
            else
                nScreen = new SysScreens();

            nScreen.ModuleId=Convert.ToInt32(ddlModules.SelectedValue);
            if (ddlParentScreen.SelectedIndex > -1)
                nScreen.ScreenParentId = Convert.ToInt32(ddlParentScreen.SelectedValue);
            else
                nScreen.ScreenParentId = null;
            nScreen.ScreenName = txtName.Text;
            nScreen.ScreenUrl = txtUrl.Text;
            nScreen.OrderInModule = Convert.ToInt32(txtOrder.Text);
            nScreen.ScreenStatus = chkStatus.Checked;

            if (grvScreens.SelectedIndex > -1)
                nScreen.Update();
            else
                nScreen.Insert();

            FillScreens();
            Master.Master.Message("¡Datos guardados exitosamente!.", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divDetail.Visible = false;
        }

        protected void grvScreens_SelectedIndexChanged(object sender, EventArgs e)
        {
            divDetail.Visible = true;
            SysScreens cScreen = SysScreens.Get(Convert.ToInt32(grvScreens.SelectedDataKey[0]));
            ddlModules.SelectedValue = cScreen.ModuleId.ToString();
            ddlParentScreen.SelectedValue = cScreen.ScreenParentId.ToString();
            txtName.Text = cScreen.ScreenName;
            txtUrl.Text = cScreen.ScreenUrl;
            txtOrder.Text = cScreen.OrderInModule.ToString();
            chkStatus.Checked = Convert.ToBoolean(cScreen.ScreenStatus);
        }

        protected void grvScreens_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvScreens.PageIndex = e.NewPageIndex;
            grvScreens.SelectedIndex = -1;
            FillScreens();
        }
    }
}