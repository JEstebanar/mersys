﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI.WebControls;
using General.GeneralCommons;
using SystemSecurity.CenterReports;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Administration.Forms
{
    public partial class CenterReports : System.Web.UI.Page
    {
        private IList<SysCenterReports.CenterReports> AssignedReports { get { return (IList<SysCenterReports.CenterReports>)ViewState["assignedReports"]; } set { ViewState["assignedReports"] = value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.CenterReports))
                    {
                        if (!IsPostBack)
                        {
                            AssignedReports = SysCenterReports.GetByCenterId((int)((SysUserAccess)Session["access"]).CenterId);
                            FillCenterReports();
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillCenterReports()
        {
            grvCenterReports.DataSource = AssignedReports;
            grvCenterReports.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in grvCenterReports.Rows)
            {
                SysCenterReports CenterModule = new SysCenterReports();
                CenterModule.CenterId = (int)((SysUserAccess)Session["access"]).CenterId;
                CenterModule.ReportId = (int)grvCenterReports.DataKeys[row.RowIndex].Values["reportId"];
                CenterModule.CenterReportStatus = (row.FindControl("chkAssigned") as CheckBox).Checked;
                CenterModule.InsertOrUpdate();
            }

            AssignedReports = SysCenterReports.GetByCenterId((int)((SysUserAccess)Session["access"]).CenterId);
            FillCenterReports();
            Master.Master.Message("¡Datos guardados exitosamente!.", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            FillCenterReports();
        }
    }
}