﻿<%@ Page Title="Pantallas de Roles" Language="C#" MasterPageFile="~/Modules/Administration/Administration.master"
    AutoEventWireup="true" CodeBehind="RoleScreens.aspx.cs" Inherits="Presentation.Modules.Administration.Forms.RoleScreens" %>

<%@ MasterType VirtualPath="~/Modules/Administration/Administration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Accesos de Roles</legend>
        <div class="form-search">
            <asp:DropDownList ID="ddlRoles" runat="server">
            </asp:DropDownList>
            <asp:DropDownList ID="ddlModules" runat="server">
            </asp:DropDownList>
            <asp:Button ID="btnSearch" runat="server" Text="Buscar" class="btn btn-info" 
                onclick="btnSearch_Click"/>
        </div>
        <br />
        <asp:GridView ID="grvRoleScreens" runat="server" class="table table-condensed table-striped table-bordered"
            AutoGenerateColumns="False" DataKeyNames="screenId">
            <Columns>
                <asp:BoundField DataField="screenName" HeaderText="Pantalla" SortExpression="screenName" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkAssigned" runat="server" Checked='<%# Eval("assigned") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <div class="form-actions">
            <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnSave_Click" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
        </div>
    </fieldset>
</asp:Content>
