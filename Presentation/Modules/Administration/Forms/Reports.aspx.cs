﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI.WebControls;
using General.GeneralCommons;
using SystemSecurity.Reports;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Administration.Forms
{
    public partial class Reports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.Reports))
                    {
                        if (!IsPostBack)
                        {
                            FillReports();
                            divDetail.Visible = false;

                            ddlReportType.DataTextField = "typeName";
                            ddlReportType.DataValueField = "typeId";
                            ddlReportType.DataSource = SysReports.GetReportTypes();
                            ddlReportType.DataBind();
                            ddlReportType.Items.Insert(0, new ListItem("-- Seleccione Reporte --", "0"));
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillReports()
        {
            grvReports.DataSource = SysReports.Search();
            grvReports.DataBind();
            divDetail.Visible = false;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            grvReports.SelectedIndex = -1;
            divDetail.Visible = true;
            txtReportName.Text = string.Empty;
            ddlReportType.SelectedIndex = -1;
            chkStatus.Checked = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SysReports nReport;
            if (grvReports.SelectedIndex > -1)
                nReport = SysReports.Get(Convert.ToInt32(grvReports.SelectedDataKey[0]));
            else
                nReport = new SysReports();

            nReport.ReportName = txtReportName.Text;
            nReport.ReportTypeId = Convert.ToInt32(ddlReportType.SelectedValue);
            nReport.ReportStatus = chkStatus.Checked;

            if (grvReports.SelectedIndex > -1)
                nReport.Update();
            else
                nReport.Insert();

            FillReports();
            Master.Master.Message("¡Datos guardados exitosamente!.", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divDetail.Visible = false;
        }

        protected void grvReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            divDetail.Visible = true;
            SysReports cReport = SysReports.Get(Convert.ToInt32(grvReports.SelectedDataKey[0]));
            txtReportName.Text = cReport.ReportName;
            ddlReportType.SelectedValue = cReport.ReportTypeId == null ? "0" : cReport.ReportTypeId.ToString();
            chkStatus.Checked = Convert.ToBoolean(cReport.ReportStatus);
        }
    }
}