﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using General.GeneralCommons;
using General.States;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Administration.Forms
{
    public partial class States : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.States))
                    {
                        if (!IsPostBack)
                        {
                            FillStates();
                            divDetail.Visible = false;
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillStates()
        {
            grvStates.DataSource = GenStates.Search();
            grvStates.DataBind();
            divDetail.Visible = false;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            grvStates.SelectedIndex = -1;
            divDetail.Visible = true;
            txtStateName.Text = string.Empty;
            chkStatus.Checked = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            GenStates nState;
            if (grvStates.SelectedIndex > -1)
                nState = GenStates.Get(Convert.ToInt32(grvStates.SelectedDataKey[0]));
            else
                nState = new GenStates();

            nState.StateName = txtStateName.Text;
            nState.StateStatus = chkStatus.Checked;
            nState.CountryId = (int)GeneralCommon.Countries.DominicanRepublic;

            if (grvStates.SelectedIndex > -1)
                nState.Update();
            else
                nState.Insert();

            FillStates();
            Master.Master.Message("¡Datos guardados exitosamente!.", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divDetail.Visible = false;
        }

        protected void grvState_SelectedIndexChanged(object sender, EventArgs e)
        {
            divDetail.Visible = true;
            GenStates cState = GenStates.Get(Convert.ToInt32(grvStates.SelectedDataKey[0]));
            txtStateName.Text = cState.StateName;
            chkStatus.Checked = Convert.ToBoolean(cState.StateStatus);
        }
    }
}