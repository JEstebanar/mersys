﻿<%@ Page Title="Estado de Solicitudes" Language="C#" MasterPageFile="~/Modules/Letters/Letters.master"
    AutoEventWireup="true" CodeBehind="RequestStatus.aspx.cs" Inherits="Presentation.Modules.Letters.Forms.RequestStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Estado de Solicitudes</legend>
        <div class="form-horizontal">
            <div class="control-group">
                <asp:GridView ID="grvRequests" runat="server" class="table table-condensed table-striped table-bordered"
                    AutoGenerateColumns="False" DataKeyNames="requestId">
                    <Columns>
                        <asp:BoundField DataField="createdDate" HeaderText="Fecha" ReadOnly="True" SortExpression="createdDate" />
                        <asp:BoundField DataField="letterName" HeaderText="Cartas" ReadOnly="True" SortExpression="letterName" />                        
                        <asp:BoundField DataField="letterStatus" HeaderText="Estado" ReadOnly="True" SortExpression="letterStatus" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </fieldset>
</asp:Content>
