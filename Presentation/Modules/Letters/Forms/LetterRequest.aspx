﻿<%@ Page Title="Solicitud de Cartas" Language="C#" MasterPageFile="~/Modules/Letters/Letters.master"
    AutoEventWireup="true" CodeBehind="LetterRequest.aspx.cs" Inherits="Presentation.Modules.Letters.Forms.LetterRequest" %>

<%@ MasterType VirtualPath="~/Modules/Letters/Letters.master" %>
<%@ Register Src="~/Modules/General/UserControls/wucGeneralComments.ascx" TagName="wucGeneralComments"
    TagPrefix="uc1" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Solicitud de Cartas</legend>
        <div class="controls">
            <asp:GridView ID="grvLetters" runat="server" class="table table-condensed table-striped table-bordered"
                AutoGenerateColumns="False" DataKeyNames="letterId,letterName,letterPrice" OnSelectedIndexChanged="grvLetters_SelectedIndexChanged">
                <Columns>
                    <asp:BoundField DataField="letterName" HeaderText="Cartas" ReadOnly="True" SortExpression="letterName" />
                    <asp:BoundField DataField="letterPrice" HeaderText="Precios" ReadOnly="True" SortExpression="letterPrice" />
                    <asp:CommandField ShowSelectButton="True" SelectText="Seleccionar">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="divDetail" runat="server">
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">
                        Carta</label>
                    <div class="controls">
                        <asp:TextBox ID="txtLetter" runat="server" class="input-xlarge"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Precio</label>
                    <div class="controls">
                        <asp:TextBox ID="txtPrice" runat="server" class="input-mini"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <uc1:wucGeneralComments ID="wucGeneralComments1" runat="server" />
                </div>
            </div>
            <div class="form-actions">
                <asp:Button ID="btnSave" runat="server" Text="Solicitar" class="btn btn-primary"
                    OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
            </div>
        </div>
    </fieldset>
</asp:Content>
