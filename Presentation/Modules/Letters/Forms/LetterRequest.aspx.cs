﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Billing.InvoiceDetails;
using Billing.Invoices;
using General.GeneralCommons;
using Letters.Letters;
using Letters.Requests;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.SysUsers;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Letters.Forms
{
    public partial class LetterRequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.LetterRequest))
                    {
                        if (!IsPostBack)
                        {
                            Initialize();
                            FillLetterTypes();
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        #region Methods
        private void Initialize()
        {
            divDetail.Visible = false;
            txtLetter.Enabled = false;
            txtPrice.Enabled = false;
            grvLetters.SelectedIndex = -1;
        }

        private void FillLetterTypes()
        {
            grvLetters.DataSource = LetLetters.GetByCenterId((int)((SysUserAccess)Session["access"]).CenterId);
            grvLetters.DataBind();
        }
        #endregion

        #region Buttons
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (grvLetters.SelectedIndex == -1)
            {
                Master.Master.Message("Debe seleccionar una carta.", "", 2);
                return;
            }

            if (LetRequests.IsLetterRequested((int)((SysUsers)Session["user"]).GeneralId, Convert.ToInt32(grvLetters.SelectedDataKey[0])))
            {
                Master.Master.Message("Actualmente tiene esta solicitud en proceso.", "", 2);
                return;
            }

            int nRequestId;
            LetRequests nRequests = new LetRequests();
            nRequests.LetterId=Convert.ToInt32(grvLetters.SelectedDataKey[0]);
            nRequests.RequestStatusId=(int)GeneralCommon.LetterStatus.Requested;
            nRequestId=nRequests.Insert();

            BilInvoices nInvoice = new BilInvoices();
            nInvoice.GeneralId = ((SysUsers)Session["user"]).GeneralId;
            nInvoice.InvoiceRequestTypeId = (int)GeneralCommon.InvoiceRequestTypes.LetterRequest;
            nInvoice.CreatedDate = DateTime.Now;
            nInvoice.InvoiceStatus = true;
            nInvoice.CenterId = (int)((SysUserAccess)Session["access"]).CenterId;
            
            BilInvoiceDetails nInvoiceDetail = new BilInvoiceDetails();
            nInvoiceDetail.InvoiceId = nInvoice.Insert();
            nInvoiceDetail.ServiceRequestId = nRequestId;
            nInvoiceDetail.DetailStatus = true;
            nInvoiceDetail.Insert();

            wucGeneralComments1.SaveCommentByProcessId(nRequestId, GeneralCommon.CommentProcessType.LetterRequest);
            Initialize();
            Master.Master.Message("¡Solicitud Enviada Exitosamente!", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            wucGeneralComments1.CleanForm();
            Initialize();
        }
        #endregion

        protected void grvLetters_SelectedIndexChanged(object sender, EventArgs e)
        {
            divDetail.Visible = true;
            txtLetter.Text = grvLetters.SelectedDataKey[1].ToString();
            txtPrice.Text = grvLetters.SelectedDataKey[2].ToString();
        }
    }
}