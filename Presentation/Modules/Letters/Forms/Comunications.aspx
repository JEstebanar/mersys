﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/Letters/Letters.master"
    AutoEventWireup="true" CodeBehind="Comunications.aspx.cs" Inherits="Presentation.Modules.Letters.Forms.Comunications" %>

<%@ MasterType VirtualPath="~/Modules/Letters/Letters.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Comunicaciones</legend>
        <div class="form-inline">
            <asp:Button ID="btnNew" runat="server" Text="Nuevo" class="btn" />
        </div>
        <asp:GridView ID="grvComunications" runat="server" class="table table-condensed table-striped table-bordered"
                AutoGenerateColumns="False" DataKeyNames="comunicationId"></asp:GridView>
        <div class="form-horizontal">
        </div>
    </fieldset>
</asp:Content>
