﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation.Modules.Letters.UserControls
{
    public partial class wucComments : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Methods

        public void FillCommentsByRequestId(int requestId) 
        {
            CleanForm();

        }

        public void SaveCommentByRequestId(int requestId)
        {

            CleanForm();
        }

        public void CleanForm()
        {
            grvComments.DataSource = null;
            grvComments.DataBind();

            txtComment.Text = string.Empty;
        }

        #endregion
    }
}