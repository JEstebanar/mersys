﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using General.GeneralCommons;
using System.Web.UI.HtmlControls;
using Letters.Requests;
using SystemSecurity.UserAccess;
using Billing.InvoiceDetails;
using General.EMails;

namespace Presentation.Modules.Letters.UserControls
{
    public partial class wucRequests : System.Web.UI.UserControl
    {
        #region Properties
        public int StepNumber
        {
            get { return (int)ViewState["stepNumber"]; }
            set { ViewState["stepNumber"] = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                divDetails.Visible = false;
                txtLetterName.Enabled = false;
                txtApplicantName.Enabled = false;
                switch (StepNumber)
                {
                    case (int)GeneralCommon.LetterSteps.Review:
                        lblTitle.Text = "Revisar Solicitudes";
                        btnSave.Text = "Enviar";
                        btnCancel.Text = "Cancelar";
                        break;

                    case (int)GeneralCommon.LetterSteps.Approve:
                        lblTitle.Text = "Aprobar Solicitudes";
                        btnSave.Text = "Aprobar";
                        btnCancel.Text = "Denegar";
                        break;
                    case (int)GeneralCommon.LetterSteps.Complete:
                        lblTitle.Text = "Completar Solicitudes";
                        btnSave.Text = "Completada";
                        btnCancel.Text = "Cancelada";
                        break;
                }
                FillRequests();
            }
        }

        #region Methods
        private void FillRequests()
        {
            int statusId = 0;
            switch (StepNumber)
            {
                case (int)GeneralCommon.LetterSteps.Review:
                    statusId = (int)GeneralCommon.LetterStatus.Requested;
                    break;
                case (int)GeneralCommon.LetterSteps.Approve:
                    statusId = (int)GeneralCommon.LetterStatus.Sent;
                    break;
                case (int)GeneralCommon.LetterSteps.Complete:
                    statusId = (int)GeneralCommon.LetterStatus.Approved;
                    break;
            }
            grvRequests.DataSource = LetRequests.GetByStatusId(statusId, (int)((SysUserAccess)Session["access"]).CenterId);
            grvRequests.DataBind();
            grvRequests.SelectedIndex = -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">Message you want to display</param>
        /// <param name="header">Message header</param>
        /// <param name="type">1 - Warning Message, || 2 - Error Message, || 3 - Message of Success, || 4 - Message Information</param>
        public void Message(System.String message, System.String header, int? type)
        {
            HtmlGenericControl h4 = new HtmlGenericControl("h4");

            if (type == null || type > 4 || type <= 0)
            {
                divMessage.Controls.Remove(h4);

                divMessage.Attributes.Add("class", "");
                divMessage.Visible = false;
                return;
            }
            else
            {
                divMessage.Visible = true;

                switch (type)
                {
                    case 1:         //warning
                        divMessage.Attributes.Add("class", "alert alert-block");
                        h4.InnerText = "Advertencia";
                        break;
                    case 2:         //error
                        divMessage.Attributes.Add("class", "alert alert-error");
                        h4.InnerText = "Algo esta mal";
                        break;
                    case 3:         //success
                        divMessage.Attributes.Add("class", "alert alert-success");
                        h4.InnerText = "¡Bien Hecho!";
                        break;
                    case 4:         //info
                        divMessage.Attributes.Add("class", "alert alert-info");
                        h4.InnerText = "¡Algo esta mal!";
                        break;
                    default:
                        divMessage.Attributes.Add("class", "");
                        h4.InnerText = "";
                        break;
                }

                divMessage.Controls.Add(h4);

                divMessage.InnerText = message;
            }
            upMessage.Update();
        }

        #endregion

        #region Buttons
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if ((int)grvRequests.SelectedDataKey[0] == -1)
            {
                Message("Debe seleccionar una solicitud.", "", 2);
                return;
            }

            LetRequests cRequest = LetRequests.Get(Convert.ToInt32(grvRequests.SelectedDataKey[0]));
            int statusId = 0;
            switch (StepNumber)
            {
                case (int)GeneralCommon.LetterSteps.Review:
                    statusId = (int)GeneralCommon.LetterStatus.Sent;
                    break;
                case (int)GeneralCommon.LetterSteps.Approve:
                    statusId = (int)GeneralCommon.LetterStatus.Approved;
                    break;
                case (int)GeneralCommon.LetterSteps.Complete:
                    statusId = (int)GeneralCommon.LetterStatus.Completed;
                    //sent mail
                    GenEMails.LetterRequestStatus(Convert.ToInt32(grvRequests.SelectedDataKey[3]), txtApplicantName.Text, GeneralCommon.MailType.LetterRequestCompleted, grvRequests.SelectedDataKey[1].ToString(), null, (int)((SysUserAccess)Session["access"]).CenterId);
                    break;
            }

            cRequest.RequestStatusId = statusId;
            cRequest.Update();

            divDetails.Visible = false;
            wucGeneralComments1.SaveCommentByProcessId((int)grvRequests.SelectedDataKey[0], GeneralCommon.CommentProcessType.LetterRequest);
            FillRequests();
            Message("¡Datos Guardados Exitosamente!", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if ((int)grvRequests.SelectedDataKey[0] == -1)
            {
                Message("Debe seleccionar una solicitud.", "", 2);
                return;
            }

            if (wucGeneralComments1.txtComment.Text == string.Empty)
            {
                Message("Por Favor, Indique la causa de la cancelación.", "", 2);
                return;
            }

            LetRequests cRequest = LetRequests.Get(Convert.ToInt32(grvRequests.SelectedDataKey[0]));
            cRequest.RequestStatusId = (int)GeneralCommon.LetterStatus.Cancel;
            cRequest.Update();

            BilInvoiceDetails cInvoiceDetail = BilInvoiceDetails.Get(Convert.ToInt32(grvRequests.SelectedDataKey[4]));
            cInvoiceDetail.DetailStatus = false;
            cInvoiceDetail.Update();
            //sent mail            
            GenEMails.LetterRequestStatus(Convert.ToInt32(grvRequests.SelectedDataKey[3]), txtApplicantName.Text, GeneralCommon.MailType.LetterRequestCanceled, grvRequests.SelectedDataKey[1].ToString(), wucGeneralComments1.txtComment.Text, (int)((SysUserAccess)Session["access"]).CenterId);
            
            divDetails.Visible = false;
            wucGeneralComments1.SaveCommentByProcessId((int)grvRequests.SelectedDataKey[0], GeneralCommon.CommentProcessType.LetterRequest);
            FillRequests();
            Message("¡Datos Guardados Exitosamente!", "", 3);
        }
        #endregion

        protected void grvRequests_SelectedIndexChanged(object sender, EventArgs e)
        {
            divDetails.Visible = true;
            txtLetterName.Text = grvRequests.SelectedDataKey[1].ToString();
            txtApplicantName.Text = grvRequests.SelectedDataKey[2].ToString();
            wucGeneralComments1.FillCommentsByProcessId((int)grvRequests.SelectedDataKey[0], GeneralCommon.CommentProcessType.LetterRequest);
        }
    }
}