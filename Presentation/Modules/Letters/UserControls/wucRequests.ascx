﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="wucRequests.ascx.cs"
    Inherits="Presentation.Modules.Letters.UserControls.wucRequests" %>
<!-- Le styles -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../../../bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="../../../bootstrap/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="../../../Default/style/reset.css" />
<link rel="stylesheet" type="text/css" href="../../../Default/style/superfish.css" />
<link rel="stylesheet" type="text/css" href="../../../Default/style/style.css" />
<link rel="stylesheet" type="text/css" href="../../../Default/style/responsive.css" />
<link rel="shortcut icon" href="../../../Default/images/favicon.ico" />
<link rel="stylesheet" href="../../../Styles/Site.css" type="text/css" />
<%@ Register Src="~/Modules/General/UserControls/wucGeneralComments.ascx" TagName="wucGeneralComments"
    TagPrefix="uc1" %>
<asp:UpdatePanel runat="server" ID="upMessage" UpdateMode="Conditional">
    <ContentTemplate>
        <div id="divMessage" runat="server" class="alert alert-block" visible="false">
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="upanelRequest" runat="server">
    <ContentTemplate>
        <fieldset>
            <legend>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></legend>
            <div class="form-horizontal">
                <div class="control-group">
                    <asp:GridView ID="grvRequests" runat="server" class="table table-condensed table-striped table-bordered"
                        AutoGenerateColumns="False" DataKeyNames="requestId,letterName,fullName,GeneralId,invoiceDetailId"
                        OnSelectedIndexChanged="grvRequests_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="createdDate" HeaderText="Fecha" ReadOnly="True" SortExpression="createdDate" />
                            <asp:BoundField DataField="letterName" HeaderText="Carta" ReadOnly="True" SortExpression="letterName" />
                            <asp:BoundField DataField="fullName" HeaderText="Solicitante" ReadOnly="True" SortExpression="fullName" />
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <%--<asp:UpdatePanel UpdateMode="Conditional" ID="upanelAugText" runat="server">
                                        <ContentTemplate>--%>
                                            <div class="text-center">
                                                <asp:LinkButton ID="lnkSelect" runat="server" CausesValidation="False" CommandName="Select"
                                                    class="icon-eye-open"></asp:LinkButton></div>
                                        <%--</ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="lnkSelect" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </fieldset>
        <div id="divDetails" runat="server" visible="true">
            <fieldset>
                <legend>Datos de Solicitud</legend>
                <div class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label">
                            Carta</label>
                        <div class="controls">
                            <asp:TextBox ID="txtLetterName" runat="server" class="input-xxlarge"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            Solicitante</label>
                        <div class="controls">
                            <asp:TextBox ID="txtApplicantName" runat="server" class="input-xxlarge"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <uc1:wucGeneralComments ID="wucGeneralComments1" runat="server" />
                    </div>
                </div>
            </fieldset>
            <div class="form-actions">
                <asp:Button ID="btnSave" runat="server" Text="" class="btn btn-success btn-primary"
                    OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="" class="btn btn-danger" OnClick="btnCancel_Click" />
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
