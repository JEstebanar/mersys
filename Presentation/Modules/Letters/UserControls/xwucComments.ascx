﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="xwucComments.ascx.cs"
    Inherits="Presentation.Modules.Letters.UserControls.wucComments" %>
<!-- Le styles -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../../../bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="../../../bootstrap/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="../../../Default/style/reset.css" />
<link rel="stylesheet" type="text/css" href="../../../Default/style/superfish.css" />
<link rel="stylesheet" type="text/css" href="../../../Default/style/style.css" />
<link rel="stylesheet" type="text/css" href="../../../Default/style/responsive.css" />
<link rel="shortcut icon" href="../../../Default/images/favicon.ico" />
<link rel="stylesheet" href="../../../Styles/Site.css" type="text/css" />
<fieldset>
    <legend>Comentario</legend>
    <div class="form-horizontal">
        <div class="control-group">
            <asp:GridView ID="grvComments" runat="server" class="table table-condensed table-striped table-bordered"
                AutoGenerateColumns="False" DataKeyNames="commentId">
                <Columns>
                    <asp:BoundField DataField="comment" HeaderText="Comentarios" ReadOnly="True" SortExpression="comment" />
                    <asp:BoundField DataField="createdBy" HeaderText="Por" ReadOnly="True" SortExpression="createdBy" />
                    <asp:BoundField DataField="createdDate" HeaderText="Fecha" SortExpression="createdDate" />
                </Columns>
            </asp:GridView>
        </div>
        <div class="control-group">
            <label class="control-label">
                Comentario</label>
            <div class="controls">
                <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Rows="3" Width="98%"></asp:TextBox>
            </div>
        </div>
    </div>
</fieldset>
