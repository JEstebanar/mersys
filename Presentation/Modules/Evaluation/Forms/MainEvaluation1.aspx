﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/Evaluation/Evaluation.master"
    AutoEventWireup="true" CodeBehind="MainEvaluation1.aspx.cs" Inherits="Presentation.Modules.Evaluation.Forms.MainEvaluation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="accordion" id="Accordion">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                    Residentes </a>
            </div>
            <div id="collapseOne" class="accordion-body collapse in">
                <div class="accordion-inner">
                    <div class="form-search">
                        <asp:DropDownList ID="ddlResidences" runat="server">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlGRades" runat="server">
                        </asp:DropDownList>
                        <asp:Button ID="btnSearchResidents" runat="server" Text="Buscar" class="btn" />
                    </div>
                    <div class="control-group">
                        <asp:GridView ID="grvResidents" runat="server" Width="100%" class="table-striped">
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-inline">
        <asp:TextBox ID="txtFullName" runat="server" CssClass="textEntry"></asp:TextBox>
        <asp:TextBox ID="txtResidence" runat="server" CssClass="textEntry" Width="250px"></asp:TextBox>
        <asp:TextBox ID="txtGrade" runat="server" CssClass="textEntry" Width="30px"></asp:TextBox>
    </div>
    <div class="control-group">
        <asp:GridView ID="grvEvaluation" runat="server" Width="100%" class="table-striped">
        </asp:GridView>
    </div>
</asp:Content>
