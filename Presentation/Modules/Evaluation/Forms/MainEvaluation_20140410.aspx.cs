﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.UI.WebControls;
using Evaluation.DetailScores;
using General.GeneralCommons;
using Residences.Residents;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;
using SystemSecurity.UserRoles;
using General.Utilities;

namespace Presentation.Modules.Evaluation.Forms
{
    public partial class MainEvaluation1 : System.Web.UI.Page
    {
        private IList<EvaDetailScores.Scores> CurrentScores { get { return (IList<EvaDetailScores.Scores>)ViewState["currentScores"]; } set { ViewState["currentScores"] = value; } }
        private IList<EvaDetailScores.ScoreDetails> CurrentScoreDetails { get { return (IList<EvaDetailScores.ScoreDetails>)ViewState["currentScoreDetails"]; } set { ViewState["currentScoreDetails"] = value; } }
        //private IList<EvaDetailScores.ScoreDetails> SelectedScoreDetails { get { return (IList<EvaDetailScores.ScoreDetails>)ViewState["selectedScoreDetails"]; } set { ViewState["selectedScoreDetails"] = value; } }
        private IList<EvaDetailScores.ScoreDetails> ModifiedScoreDetails { get { return (IList<EvaDetailScores.ScoreDetails>)ViewState["modifiedScoreDetails"]; } set { ViewState["modifiedScoreDetails"] = value; } }


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.MainEvaluation))                   
                    {
                        if (!IsPostBack)
                        {
                            divDetails.Visible = false;
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        #region Methods
        private void Initialize()
        {

        }

        private void FillResidents()
        {
            grvResidents.DataSource = ResResidents.GetByUserCenterId((int)((SysUserAccess)Session["access"]).UserCenterId, txtSearchName.Text);
            grvResidents.DataBind();
        }

        private bool ValidateResidentTransferred()
        {
            if (Convert.ToInt32(Session["ResidentStatusId"]) != (int)GeneralCommon.ResidentStatuses.Transferred)
            {
                grvEvaluation.Enabled = true;
                btnSave.Visible = true;
                btnCancel.Visible = true;
                return true;
            }
            else
            {   
                Master.Master.Message("Este médico residente ha sido transferido a otro centro y sus datos no pueden ser modificados.", "", 4);
                grvEvaluation.Enabled = false;
                btnSave.Visible = false;
                btnCancel.Visible = false;
                return false;
            }
        }

        //private void HideActionInColumns(string actionException, string textException, string footerException)
        //{
        //    foreach (GridViewRow row in grvEvaluation.Rows)
        //    {
        //        bool bit = false;
        //        if ((row.FindControl(actionException) as LinkButton).Visible == false)
        //            bit = true;
        //        else
        //            bit = false;

        //        (row.FindControl("actionJul") as LinkButton).Visible = false;
        //        (row.FindControl("actionAug") as LinkButton).Visible = false;
        //        //(row.FindControl("actionSep") as LinkButton).Visible = false;
        //        //(row.FindControl("actionOct") as LinkButton).Visible = false;
        //        //(row.FindControl("actionNov") as LinkButton).Visible = false;
        //        //(row.FindControl("actionDec") as LinkButton).Visible = false;
        //        //(row.FindControl("actionJan") as LinkButton).Visible = false;
        //        //(row.FindControl("actionFeb") as LinkButton).Visible = false;
        //        //(row.FindControl("actionMar") as LinkButton).Visible = false;
        //        //(row.FindControl("actionApr") as LinkButton).Visible = false;
        //        //(row.FindControl("actionMay") as LinkButton).Visible = false;
        //        //(row.FindControl("actionJun") as LinkButton).Visible = false;
        //        (row.FindControl(actionException) as LinkButton).Visible = bit;

        //        (row.FindControl("txtJul") as TextBox).Enabled = false;
        //        (row.FindControl("txtAug") as TextBox).Enabled = false;
        //        //(row.FindControl("txtSep") as TextBox).Enabled = false;
        //        //(row.FindControl("txtOct") as TextBox).Enabled = false;
        //        //(row.FindControl("txtNov") as TextBox).Enabled = false;
        //        //(row.FindControl("txtDec") as TextBox).Enabled = false;
        //        //(row.FindControl("txtJan") as TextBox).Enabled = false;
        //        //(row.FindControl("txtFeb") as TextBox).Enabled = false;
        //        //(row.FindControl("txtMar") as TextBox).Enabled = false;
        //        //(row.FindControl("txtApr") as TextBox).Enabled = false;
        //        //(row.FindControl("txtMay") as TextBox).Enabled = false;
        //        //(row.FindControl("txtJun") as TextBox).Enabled = false;                
        //        (row.FindControl(textException) as TextBox).Enabled = bit;

        //        (grvEvaluation.FooterRow.FindControl("txtJulFooter") as TextBox).Enabled = false;
        //        (grvEvaluation.FooterRow.FindControl("txtAugFooter") as TextBox).Enabled = false;
        //        //(grvEvaluation.FooterRow.FindControl("txtSepFooter") as TextBox).Enabled = false;
        //        //(grvEvaluation.FooterRow.FindControl("txtOctFooter") as TextBox).Enabled = false;
        //        //(grvEvaluation.FooterRow.FindControl("txtNovFooter") as TextBox).Enabled = false;
        //        //(grvEvaluation.FooterRow.FindControl("txtDecFooter") as TextBox).Enabled = false;
        //        //(grvEvaluation.FooterRow.FindControl("txtJanFooter") as TextBox).Enabled = false;
        //        //(grvEvaluation.FooterRow.FindControl("txtFebFooter") as TextBox).Enabled = false;
        //        //(grvEvaluation.FooterRow.FindControl("txtMarFooter") as TextBox).Enabled = false;
        //        //(grvEvaluation.FooterRow.FindControl("txtAprFooter") as TextBox).Enabled = false;
        //        //(grvEvaluation.FooterRow.FindControl("txtMayFooter") as TextBox).Enabled = false;
        //        //(grvEvaluation.FooterRow.FindControl("txtJunFooter") as TextBox).Enabled = false;
        //        (grvEvaluation.FooterRow.FindControl(footerException) as TextBox).Enabled = bit;
        //    }
        //}

        private void HeaderAction(string txtElement, string linkElement, string footerElement)
        {
            CalculateScores();
            foreach (GridViewRow row in grvEvaluation.Rows)
            {
                bool bit = false;
                if ((row.FindControl(linkElement) as LinkButton).Visible == false)
                    bit = true;
                else                
                    bit = false;

                (row.FindControl("actionJul") as LinkButton).Visible = false;
                (row.FindControl("actionAug") as LinkButton).Visible = false;
                (row.FindControl("actionSep") as LinkButton).Visible = false;
                (row.FindControl("actionOct") as LinkButton).Visible = false;
                (row.FindControl("actionNov") as LinkButton).Visible = false;
                (row.FindControl("actionDec") as LinkButton).Visible = false;
                (row.FindControl("actionJan") as LinkButton).Visible = false;
                (row.FindControl("actionFeb") as LinkButton).Visible = false;
                (row.FindControl("actionMar") as LinkButton).Visible = false;
                (row.FindControl("actionApr") as LinkButton).Visible = false;
                (row.FindControl("actionMay") as LinkButton).Visible = false;
                (row.FindControl("actionJun") as LinkButton).Visible = false;
                (row.FindControl(linkElement) as LinkButton).Visible = bit;

                (row.FindControl("txtJul") as TextBox).Enabled = false;
                (row.FindControl("txtAug") as TextBox).Enabled = false;
                (row.FindControl("txtSep") as TextBox).Enabled = false;
                (row.FindControl("txtOct") as TextBox).Enabled = false;
                (row.FindControl("txtNov") as TextBox).Enabled = false;
                (row.FindControl("txtDec") as TextBox).Enabled = false;
                (row.FindControl("txtJan") as TextBox).Enabled = false;
                (row.FindControl("txtFeb") as TextBox).Enabled = false;
                (row.FindControl("txtMar") as TextBox).Enabled = false;
                (row.FindControl("txtApr") as TextBox).Enabled = false;
                (row.FindControl("txtMay") as TextBox).Enabled = false;
                (row.FindControl("txtJun") as TextBox).Enabled = false;
                (row.FindControl(txtElement) as TextBox).Enabled = bit;

                (grvEvaluation.FooterRow.FindControl("txtJulFooter") as TextBox).Enabled = false;
                (grvEvaluation.FooterRow.FindControl("txtAugFooter") as TextBox).Enabled = false;
                (grvEvaluation.FooterRow.FindControl("txtSepFooter") as TextBox).Enabled = false;
                (grvEvaluation.FooterRow.FindControl("txtOctFooter") as TextBox).Enabled = false;
                (grvEvaluation.FooterRow.FindControl("txtNovFooter") as TextBox).Enabled = false;
                (grvEvaluation.FooterRow.FindControl("txtDecFooter") as TextBox).Enabled = false;
                (grvEvaluation.FooterRow.FindControl("txtJanFooter") as TextBox).Enabled = false;
                (grvEvaluation.FooterRow.FindControl("txtFebFooter") as TextBox).Enabled = false;
                (grvEvaluation.FooterRow.FindControl("txtMarFooter") as TextBox).Enabled = false;
                (grvEvaluation.FooterRow.FindControl("txtAprFooter") as TextBox).Enabled = false;
                (grvEvaluation.FooterRow.FindControl("txtMayFooter") as TextBox).Enabled = false;
                (grvEvaluation.FooterRow.FindControl("txtJunFooter") as TextBox).Enabled = false;
                (grvEvaluation.FooterRow.FindControl(footerElement) as TextBox).Enabled = bit;
            }
            ModifiedScoreDetails = null;
            divDetails.Visible = false;
        }

        private int HeaderAllow()
        {
            bool _jul = false, _aug = false, _sep = false, _oct = false,
                _nov = false, _dec = false, _jan = false, _feb = false,
                _mar = false, _apr = false, _may = false, _jun = false,
                Quarter1 = false, Quarter2 = false, Quarter3 = false,
                tempQuarter1 = false, tempQuarter2 = false, tempQuarter3 = false;
            foreach (EvaDetailScores.Scores scoreInList in CurrentScores)
            {
                if (scoreInList.jul < 1)
                {
                    _jul = true;
                    break;
                }
                if (scoreInList.aug < 1)
                {
                    _aug = true;
                    break;
                }
                if (scoreInList.sep < 1)
                {
                    _sep = true;
                    break;
                }
                if (scoreInList.oct < 1)
                {
                    _oct = true;
                    break;
                }

                if (scoreInList.nov < 1)
                {
                    _nov = true;
                    break;
                }
                if (scoreInList.dec < 1)
                {
                    _dec = true;
                    break;
                }
                if (scoreInList.jan < 1)
                {
                    _jan = true;
                    break;
                }
                if (scoreInList.feb < 1)
                {
                    _feb = true;
                    break;
                }

                if (scoreInList.mar < 1)
                {
                    _mar = true;
                    break;
                }
                if (scoreInList.apr < 1)
                {
                    _apr = true;
                    break;
                }
                if (scoreInList.may < 1)
                {
                    _may = true;
                    break;
                }
                if (scoreInList.jun < 1)
                {
                    _jun = true;
                    break;
                }
            }

            //allow by Quarter
            if (_jul || _aug || _sep || _oct)
                Quarter1 = true;
            if (_nov || _dec || _jan || _feb)
                Quarter2 = true;
            if (_mar || _apr || _may || _jun)
                Quarter3 = true;
            
            tempQuarter1 = Quarter1;
            tempQuarter2 = Quarter2;
            tempQuarter3 = Quarter3;

            //business logic
            foreach (SysUserRoles.UserRoles roleInList in (List<SysUserRoles.UserRoles>)Session["roles"])
            {
                if (roleInList.roleId == (int)GeneralCommon.SystemRoles.Administrator
                    || roleInList.roleId == (int)GeneralCommon.SystemRoles.HeadTraining)
                {
                    tempQuarter1 = true;
                    tempQuarter2 = true;
                    tempQuarter3 = true;
                }

                if (roleInList.roleId == (int)GeneralCommon.SystemRoles.EvaluationSecretary
                    || roleInList.roleId == (int)GeneralCommon.SystemRoles.TeachingSecretary)
                {
                    if (Quarter1)
                    {
                        tempQuarter1 = true; 
                        tempQuarter2 = false;          
                        tempQuarter3 = false;
                    }
                    if (Quarter2)
                    {
                        tempQuarter1 = true;
                        tempQuarter2 = true;
                        tempQuarter3 = false;
                    }
                    if (Quarter3)
                    {
                        tempQuarter1 = true;
                        tempQuarter2 = true;
                        tempQuarter3 = true;
                    }
                }
            }

            //change QuaterX _mon if want to allow by month
            (grvEvaluation.HeaderRow.FindControl("JulHeader") as LinkButton).Enabled = tempQuarter1;
            (grvEvaluation.HeaderRow.FindControl("AugHeader") as LinkButton).Enabled = tempQuarter1;
            (grvEvaluation.HeaderRow.FindControl("SepHeader") as LinkButton).Enabled = tempQuarter1;
            (grvEvaluation.HeaderRow.FindControl("OctHeader") as LinkButton).Enabled = tempQuarter1;

            (grvEvaluation.HeaderRow.FindControl("NovHeader") as LinkButton).Enabled = tempQuarter2;
            (grvEvaluation.HeaderRow.FindControl("DecHeader") as LinkButton).Enabled = tempQuarter2;
            (grvEvaluation.HeaderRow.FindControl("JanHeader") as LinkButton).Enabled = tempQuarter2;
            (grvEvaluation.HeaderRow.FindControl("FebHeader") as LinkButton).Enabled = tempQuarter2;

            (grvEvaluation.HeaderRow.FindControl("MarHeader") as LinkButton).Enabled = tempQuarter3;
            (grvEvaluation.HeaderRow.FindControl("AprHeader") as LinkButton).Enabled = tempQuarter3;
            (grvEvaluation.HeaderRow.FindControl("MayHeader") as LinkButton).Enabled = tempQuarter3;
            (grvEvaluation.HeaderRow.FindControl("JunHeader") as LinkButton).Enabled = tempQuarter3;

            if (Quarter1)
                return 1;
            else if (Quarter2)
                return 2;
            else
                return 3;
        }

        private void DistributeScore(object sender, string txtElement, int month)
        {
            GridViewRow row = ((GridViewRow)((TextBox)sender).NamingContainer);
            TextBox txt = (TextBox)row.FindControl(txtElement);
            
            //if(txt.Text==string.Empty)
            //    txt _julScores = _julScores == 0 ? Convert.ToDecimal(lblPercent.Text) : _julScores;

            //if (!GenUtilities.IsNumeric(txt.Text))
            //{
            //    txt.Text = string.Empty;
            //    Master.Master.Message("Las notas deben de ser en formato numérico.", "", 2);
            //    return;
            //}

            decimal newScore = txt.Text == string.Empty ? 0 : Convert.ToDecimal(txt.Text);
            if (newScore > 100)
            {
                txt.Text = string.Empty;
                Master.Master.Message("Las notas deben de ser igual o menor de 100.", "", 2);
            }
            else
            {
                //Master.Master.Message("", "", 0);
                FillEvaluationFromDetail(Convert.ToInt32(grvEvaluation.DataKeys[row.RowIndex].Values["evaluationId"]), month, newScore);
                foreach (EvaDetailScores.ScoreDetails modifiedInList in CurrentScoreDetails.Where(c => c.evaluationId == Convert.ToInt32(grvEvaluation.DataKeys[row.RowIndex].Values["evaluationId"]) && c.monthNumber == month))
                {
                    if (modifiedInList.maxPoints <= newScore)
                    {
                        modifiedInList.score = modifiedInList.maxPoints;
                        newScore = newScore - modifiedInList.maxPoints;
                    }
                    else
                    {
                        modifiedInList.score = newScore;
                        newScore = 0;
                    }
                }
                //CalculateScores();
            }
        }

        private void DistributeScoreInEvaluations(string footerName, int month)
        {
            foreach (EvaDetailScores.Scores evaluationInList in CurrentScores)
            {
                //if (!GenUtilities.IsNumeric((grvEvaluation.FooterRow.FindControl(footerName) as TextBox).Text))
                //{
                //    (grvEvaluation.FooterRow.FindControl(footerName) as TextBox).Text = string.Empty;
                //    Master.Master.Message("Las notas deben de ser en formato numérico.", "", 2);
                //    return;
                //}

                decimal newScore = (grvEvaluation.FooterRow.FindControl(footerName) as TextBox).Text == string.Empty ? 0 : Convert.ToDecimal((grvEvaluation.FooterRow.FindControl(footerName) as TextBox).Text);

                //decimal newScore = Convert.ToDecimal((grvEvaluation.FooterRow.FindControl(footerName) as TextBox).Text);

                if (newScore > 100)
                {
                    Master.Master.Message("Las notas deben de ser igual o menor de 100.", "", 2);
                    (grvEvaluation.FooterRow.FindControl(footerName) as TextBox).Text = string.Empty;
                    break;
                }
                FillEvaluationFromDetail(evaluationInList.evaluationId, month, newScore);

                foreach (EvaDetailScores.ScoreDetails modifiedInList in CurrentScoreDetails.Where(c => c.monthNumber == month && c.evaluationId == evaluationInList.evaluationId))
                {
                    if (modifiedInList.monthNumber == month)
                    {
                        if (modifiedInList.maxPoints <= newScore)
                        {
                            modifiedInList.score = modifiedInList.maxPoints;
                            newScore = newScore - modifiedInList.maxPoints;
                        }
                        else
                        {
                            modifiedInList.score = newScore;
                            newScore = 0;
                        }
                    }
                }
            }

            grvEvaluation.DataSource = CurrentScores;
            grvEvaluation.DataBind();
            CalculateScores();
            divDetails.Visible = false;
        }

        private void FillDetails(int evaluationId, int monthNumber)
        {
            IList<EvaDetailScores.ScoreDetails> SelectedScoreDetails = new List<EvaDetailScores.ScoreDetails>();
            foreach (EvaDetailScores.ScoreDetails evaDetailInList in CurrentScoreDetails)
            {
                if (evaDetailInList.evaluationId == evaluationId && evaDetailInList.monthNumber == monthNumber)
                {
                    SelectedScoreDetails.Add(evaDetailInList);
                }
            }
            grvDetails.DataSource = SelectedScoreDetails;
            grvDetails.DataBind();

            SumDetailScores();

            divDetails.Visible = true;
        }

        private void FillEvaluationFromDetail(int evaId, int month, decimal nScore)
        {
            //foreach (EvaDetailScores.Scores scoresInList in CurrentScores)
            //{
            if (month == 7)
            {
                var foundScore = CurrentScores.FirstOrDefault(s => s.evaluationId == evaId);
                foundScore.jul = nScore;
            }
            else if (month == 8)
            {
                var foundScore = CurrentScores.FirstOrDefault(s => s.evaluationId == evaId);
                foundScore.aug = nScore;
            }
            else if (month == 9)
            {
                var foundScore = CurrentScores.FirstOrDefault(s => s.evaluationId == evaId);
                foundScore.sep = nScore;
            }
            else if (month == 10)
            {
                var foundScore = CurrentScores.FirstOrDefault(s => s.evaluationId == evaId);
                foundScore.oct = nScore;
            }
            else if (month == 11)
            {
                var foundScore = CurrentScores.FirstOrDefault(s => s.evaluationId == evaId);
                foundScore.nov = nScore;
            }
            else if (month == 12)
            {
                var foundScore = CurrentScores.FirstOrDefault(s => s.evaluationId == evaId);
                foundScore.dec = nScore;
            }
            else if (month == 1)
            {
                var foundScore = CurrentScores.FirstOrDefault(s => s.evaluationId == evaId);
                foundScore.jan = nScore;
            }
            else if (month == 2)
            {
                var foundScore = CurrentScores.FirstOrDefault(s => s.evaluationId == evaId);
                foundScore.feb = nScore;
            }
            else if (month == 3)
            {
                var foundScore = CurrentScores.FirstOrDefault(s => s.evaluationId == evaId);
                foundScore.mar = nScore;
            }
            else if (month == 4)
            {
                var foundScore = CurrentScores.FirstOrDefault(s => s.evaluationId == evaId);
                foundScore.apr = nScore;
            }
            else if (month == 5)
            {
                var foundScore = CurrentScores.FirstOrDefault(s => s.evaluationId == evaId);
                foundScore.may = nScore;
            }
            else if (month == 6)
            {
                var foundScore = CurrentScores.FirstOrDefault(s => s.evaluationId == evaId);
                foundScore.jun = nScore;
            }

            //}
        }

        private void CalculateScores()
        {
            //footers
            decimal julScores = 0, augScores = 0, sepScores = 0, octScores = 0,
                novScores = 0, decScores = 0, janScores = 0, febScores = 0,
                marScores = 0, aprScores = 0, mayScores = 0, junScores = 0;

            decimal _julScores = 0, _augScores = 0, _sepScores = 0, _octScores = 0,
                _novScores = 0, _decScores = 0, _janScores = 0, _febScores = 0,
                _marScores = 0, _aprScores = 0, _mayScores = 0, _junScores = 0;

            int julCount = 0, augCount = 0, sepCount = 0, octCount = 0,
                novCount = 0, decCount = 0, janCount = 0, febCount = 0,
                marCount = 0, aprCount = 0, mayCount = 0, junCount = 0;

            foreach (GridViewRow row in grvEvaluation.Rows)
            {
                TextBox txtJul = row.FindControl("txtJul") as TextBox;
                TextBox txtAug = row.FindControl("txtAug") as TextBox;
                TextBox txtSep = row.FindControl("txtSep") as TextBox;
                TextBox txtOct = row.FindControl("txtOct") as TextBox;
                TextBox txtNov = row.FindControl("txtNov") as TextBox;
                TextBox txtDec = row.FindControl("txtDec") as TextBox;
                TextBox txtJan = row.FindControl("txtJan") as TextBox;
                TextBox txtFeb = row.FindControl("txtFeb") as TextBox;
                TextBox txtMar = row.FindControl("txtMar") as TextBox;
                TextBox txtApr = row.FindControl("txtApr") as TextBox;
                TextBox txtMay = row.FindControl("txtMay") as TextBox;
                TextBox txtJun = row.FindControl("txtJun") as TextBox;
                Label lblPercent = row.FindControl("lblPercent") as Label;

                if (txtJul.Text == string.Empty) txtJul.Text = "0";
                if (txtAug.Text == string.Empty) txtAug.Text = "0";
                if (txtSep.Text == string.Empty) txtSep.Text = "0";
                if (txtOct.Text == string.Empty) txtOct.Text = "0";
                if (txtNov.Text == string.Empty) txtNov.Text = "0";
                if (txtDec.Text == string.Empty) txtDec.Text = "0";
                if (txtJan.Text == string.Empty) txtJan.Text = "0";
                if (txtFeb.Text == string.Empty) txtFeb.Text = "0";
                if (txtMar.Text == string.Empty) txtMar.Text = "0";
                if (txtApr.Text == string.Empty) txtApr.Text = "0";
                if (txtMay.Text == string.Empty) txtMay.Text = "0";
                if (txtJun.Text == string.Empty) txtJun.Text = "0";

                //string.IsNullOrEmpty(tb_cheque.Text) ? 0 : double.Parse(tb_cheque.Text);
                //return x != 0.0 ? Math.Sin(x) / x : 1.0;
                _julScores = (Convert.ToDecimal(txtJul.Text) * Convert.ToDecimal(lblPercent.Text)) / 100;
                _augScores = (Convert.ToDecimal(txtAug.Text) * Convert.ToDecimal(lblPercent.Text)) / 100;
                _sepScores = (Convert.ToDecimal(txtSep.Text) * Convert.ToDecimal(lblPercent.Text)) / 100;
                _octScores = (Convert.ToDecimal(txtOct.Text) * Convert.ToDecimal(lblPercent.Text)) / 100;
                _novScores = (Convert.ToDecimal(txtNov.Text) * Convert.ToDecimal(lblPercent.Text)) / 100;
                _decScores = (Convert.ToDecimal(txtDec.Text) * Convert.ToDecimal(lblPercent.Text)) / 100;
                _janScores = (Convert.ToDecimal(txtJan.Text) * Convert.ToDecimal(lblPercent.Text)) / 100;
                _febScores = (Convert.ToDecimal(txtFeb.Text) * Convert.ToDecimal(lblPercent.Text)) / 100;
                _marScores = (Convert.ToDecimal(txtMar.Text) * Convert.ToDecimal(lblPercent.Text)) / 100;
                _aprScores = (Convert.ToDecimal(txtApr.Text) * Convert.ToDecimal(lblPercent.Text)) / 100;
                _mayScores = (Convert.ToDecimal(txtMay.Text) * Convert.ToDecimal(lblPercent.Text)) / 100;
                _junScores = (Convert.ToDecimal(txtJun.Text) * Convert.ToDecimal(lblPercent.Text)) / 100;

                _julScores = _julScores == 0 ? Convert.ToDecimal(lblPercent.Text) : _julScores;
                _augScores = _augScores == 0 ? Convert.ToDecimal(lblPercent.Text) : _augScores;
                _sepScores = _sepScores == 0 ? Convert.ToDecimal(lblPercent.Text) : _sepScores;
                _octScores = _octScores == 0 ? Convert.ToDecimal(lblPercent.Text) : _octScores;
                _novScores = _novScores == 0 ? Convert.ToDecimal(lblPercent.Text) : _novScores;
                _decScores = _decScores == 0 ? Convert.ToDecimal(lblPercent.Text) : _decScores;
                _janScores = _janScores == 0 ? Convert.ToDecimal(lblPercent.Text) : _janScores;
                _febScores = _febScores == 0 ? Convert.ToDecimal(lblPercent.Text) : _febScores;
                _marScores = _marScores == 0 ? Convert.ToDecimal(lblPercent.Text) : _marScores;
                _aprScores = _aprScores == 0 ? Convert.ToDecimal(lblPercent.Text) : _aprScores;
                _mayScores = _mayScores == 0 ? Convert.ToDecimal(lblPercent.Text) : _mayScores;
                _junScores = _junScores == 0 ? Convert.ToDecimal(lblPercent.Text) : _junScores;

                julScores = julScores + _julScores;
                augScores = augScores + _augScores;
                sepScores = sepScores + _sepScores;
                octScores = octScores + _octScores;
                novScores = novScores + _novScores;
                decScores = decScores + _decScores;
                janScores = janScores + _janScores;
                febScores = febScores + _febScores;
                marScores = marScores + _marScores;
                aprScores = aprScores + _aprScores;
                mayScores = mayScores + _mayScores;
                junScores = junScores + _junScores;

                if (Convert.ToDecimal(txtJul.Text) > 0) julCount++;
                if (Convert.ToDecimal(txtAug.Text) > 0) augCount++;
                if (Convert.ToDecimal(txtSep.Text) > 0) sepCount++;
                if (Convert.ToDecimal(txtOct.Text) > 0) octCount++;
                if (Convert.ToDecimal(txtNov.Text) > 0) novCount++;
                if (Convert.ToDecimal(txtDec.Text) > 0) decCount++;
                if (Convert.ToDecimal(txtJan.Text) > 0) janCount++;
                if (Convert.ToDecimal(txtFeb.Text) > 0) febCount++;
                if (Convert.ToDecimal(txtMar.Text) > 0) marCount++;
                if (Convert.ToDecimal(txtApr.Text) > 0) aprCount++;
                if (Convert.ToDecimal(txtMay.Text) > 0) mayCount++;
                if (Convert.ToDecimal(txtJun.Text) > 0) junCount++;
            }
            ////int evaluations = CurrentScores.Count;
            if (julCount == 0) julScores = 0.00m;
            if (augCount == 0) augScores = 0.00m;
            if (sepCount == 0) sepScores = 0.00m;
            if (octCount == 0) octScores = 0.00m;
            if (novCount == 0) novScores = 0.00m;
            if (decCount == 0) decScores = 0.00m;
            if (janCount == 0) janScores = 0.00m;
            if (febCount == 0) febScores = 0.00m;
            if (marCount == 0) marScores = 0.00m;
            if (aprCount == 0) aprScores = 0.00m;
            if (mayCount == 0) mayScores = 0.00m;
            if (junCount == 0) junScores = 0.00m;

            //(grvEvaluation.FooterRow.FindControl("txtJulFooter") as TextBox).Text = (decimal.Round(julScores / julCount, 2)).ToString();
            //(grvEvaluation.FooterRow.FindControl("txtAugFooter") as TextBox).Text = (decimal.Round(augScores / augCount, 2)).ToString();
            //(grvEvaluation.FooterRow.FindControl("txtSepFooter") as TextBox).Text = (decimal.Round(sepScores / sepCount, 2)).ToString();
            //(grvEvaluation.FooterRow.FindControl("txtOctFooter") as TextBox).Text = (decimal.Round(octScores / octCount, 2)).ToString();
            //(grvEvaluation.FooterRow.FindControl("txtNovFooter") as TextBox).Text = (decimal.Round(novScores / novCount, 2)).ToString();
            //(grvEvaluation.FooterRow.FindControl("txtDecFooter") as TextBox).Text = (decimal.Round(decScores / decCount, 2)).ToString();
            //(grvEvaluation.FooterRow.FindControl("txtJanFooter") as TextBox).Text = (decimal.Round(janScores / janCount, 2)).ToString();
            //(grvEvaluation.FooterRow.FindControl("txtFebFooter") as TextBox).Text = (decimal.Round(febScores / febCount, 2)).ToString();
            //(grvEvaluation.FooterRow.FindControl("txtMarFooter") as TextBox).Text = (decimal.Round(marScores / marCount, 2)).ToString();
            //(grvEvaluation.FooterRow.FindControl("txtAprFooter") as TextBox).Text = (decimal.Round(aprScores / aprCount, 2)).ToString();
            //(grvEvaluation.FooterRow.FindControl("txtMayFooter") as TextBox).Text = (decimal.Round(mayScores / mayCount, 2)).ToString();
            //(grvEvaluation.FooterRow.FindControl("txtJunFooter") as TextBox).Text = (decimal.Round(junScores / junCount, 2)).ToString();

            (grvEvaluation.FooterRow.FindControl("txtJulFooter") as TextBox).Text = (decimal.Round(julScores, 2)).ToString();
            (grvEvaluation.FooterRow.FindControl("txtAugFooter") as TextBox).Text = (decimal.Round(augScores, 2)).ToString();
            (grvEvaluation.FooterRow.FindControl("txtSepFooter") as TextBox).Text = (decimal.Round(sepScores, 2)).ToString();
            (grvEvaluation.FooterRow.FindControl("txtOctFooter") as TextBox).Text = (decimal.Round(octScores, 2)).ToString();
            (grvEvaluation.FooterRow.FindControl("txtNovFooter") as TextBox).Text = (decimal.Round(novScores, 2)).ToString();
            (grvEvaluation.FooterRow.FindControl("txtDecFooter") as TextBox).Text = (decimal.Round(decScores, 2)).ToString();
            (grvEvaluation.FooterRow.FindControl("txtJanFooter") as TextBox).Text = (decimal.Round(janScores, 2)).ToString();
            (grvEvaluation.FooterRow.FindControl("txtFebFooter") as TextBox).Text = (decimal.Round(febScores, 2)).ToString();
            (grvEvaluation.FooterRow.FindControl("txtMarFooter") as TextBox).Text = (decimal.Round(marScores, 2)).ToString();
            (grvEvaluation.FooterRow.FindControl("txtAprFooter") as TextBox).Text = (decimal.Round(aprScores, 2)).ToString();
            (grvEvaluation.FooterRow.FindControl("txtMayFooter") as TextBox).Text = (decimal.Round(mayScores, 2)).ToString();
            (grvEvaluation.FooterRow.FindControl("txtJunFooter") as TextBox).Text = (decimal.Round(junScores, 2)).ToString();

            decimal Q1, Q2, Q3, Average;
            decimal footerAverage = 0, footerActualPercent = 0, footerPercent = 0, control;
            foreach (GridViewRow row in grvEvaluation.Rows)
            {
                foreach (EvaDetailScores.Scores scoreInList in CurrentScores.Where(s => s.evaluationId == Convert.ToInt32(grvEvaluation.DataKeys[row.RowIndex].Values["evaluationId"])))
                {
                    Q1 = (scoreInList.jul + scoreInList.aug + scoreInList.sep + scoreInList.oct) / 4;
                    Q2 = (scoreInList.nov + scoreInList.dec + scoreInList.jan + scoreInList.feb) / 4;
                    Q3 = (scoreInList.mar + scoreInList.apr + scoreInList.may + scoreInList.jun) / 4;
                    Average = (Q1 + Q2 + Q3) / 3;

                    //averages
                    control = decimal.Round(Average, 2);
                    (row.FindControl("lblAverage") as Label).Text = control.ToString();
                    footerAverage = footerAverage + control;

                    //actual percent
                    control = decimal.Round((Average * scoreInList.percent) / 100, 2);
                    (row.FindControl("lblActualPercent") as Label).Text = control.ToString();
                    footerActualPercent = footerActualPercent + control;

                    //percent
                    footerPercent = footerPercent + scoreInList.percent;
                }
            }

            //footer  values
            (grvEvaluation.FooterRow.FindControl("lblAverageFooter") as Label).Text = (footerAverage / grvEvaluation.Rows.Count).ToString();
            (grvEvaluation.FooterRow.FindControl("lblActualPercentFooter") as Label).Text = footerActualPercent.ToString();
            (grvEvaluation.FooterRow.FindControl("lblPercentFooter") as Label).Text = footerPercent.ToString();

            ShowColumnsInEvaluation(HeaderAllow());

            foreach (GridViewRow row in grvEvaluation.Rows)
            {
                TextBox txtJul = row.FindControl("txtJul") as TextBox;
                TextBox txtAug = row.FindControl("txtAug") as TextBox;
                TextBox txtSep = row.FindControl("txtSep") as TextBox;
                TextBox txtOct = row.FindControl("txtOct") as TextBox;
                TextBox txtNov = row.FindControl("txtNov") as TextBox;
                TextBox txtDec = row.FindControl("txtDec") as TextBox;
                TextBox txtJan = row.FindControl("txtJan") as TextBox;
                TextBox txtFeb = row.FindControl("txtFeb") as TextBox;
                TextBox txtMar = row.FindControl("txtMar") as TextBox;
                TextBox txtApr = row.FindControl("txtApr") as TextBox;
                TextBox txtMay = row.FindControl("txtMay") as TextBox;
                TextBox txtJun = row.FindControl("txtJun") as TextBox;

                if (Convert.ToDecimal(txtJul.Text) == 0) txtJul.Text = string.Empty;
                if (Convert.ToDecimal(txtAug.Text) == 0) txtAug.Text = string.Empty;
                if (Convert.ToDecimal(txtSep.Text) == 0) txtSep.Text = string.Empty;
                if (Convert.ToDecimal(txtOct.Text) == 0) txtOct.Text = string.Empty;
                if (Convert.ToDecimal(txtNov.Text) == 0) txtNov.Text = string.Empty;
                if (Convert.ToDecimal(txtDec.Text) == 0) txtDec.Text = string.Empty;
                if (Convert.ToDecimal(txtJan.Text) == 0) txtJan.Text = string.Empty;
                if (Convert.ToDecimal(txtFeb.Text) == 0) txtFeb.Text = string.Empty;
                if (Convert.ToDecimal(txtMar.Text) == 0) txtMar.Text = string.Empty;
                if (Convert.ToDecimal(txtApr.Text) == 0) txtApr.Text = string.Empty;
                if (Convert.ToDecimal(txtMay.Text) == 0) txtMay.Text = string.Empty;
                if (Convert.ToDecimal(txtJun.Text) == 0) txtJun.Text = string.Empty;
            }
        }

        private void SumDetailScores()
        {
            decimal sumPts = 0;
            foreach (GridViewRow row in grvDetails.Rows)
            {
                TextBox txt = row.FindControl("txtDetailScores") as TextBox;
                if (Convert.ToDecimal(txt.Text) > Convert.ToDecimal(grvDetails.DataKeys[row.RowIndex].Values["maxPoints"]))
                {
                    txt.Text = "0.00";
                    //break;
                }
                sumPts = sumPts + Convert.ToDecimal(txt.Text);

                //TextBox txts = row.FindControl("txtDetailPtsFooter") as TextBox;
                //txts.Text = sumPts.ToString();
                //int id = Convert.ToInt32(grvDetails.DataKeys[row.RowIndex].Values["PtsMax"]);
            }

            (grvDetails.FooterRow.FindControl("txtDetailScoresFooter") as TextBox).Text = sumPts.ToString();
            //int cve_rol = Convert.ToInt32((grvDetails.FooterRow.FindControl("txtDetailPtsFooter") as TextBox).Text);

        }

        private void ShowColumnsInEvaluation(int Q)
        {
            int startColumn = 0;
            int endColumn = 0;
            switch (Q)
            {
                case 1:
                    startColumn = 1;
                    endColumn = 4;
                    break;
                case 2:
                    startColumn = 5;
                    endColumn = 8;
                    break;
                case 3:
                    startColumn = 9;
                    endColumn = 12;
                    break;
            }
            for (int i = 1; i <= 12; i++)
                grvEvaluation.Columns[i].Visible = false;

            for (int i = startColumn; i <= endColumn; i++)
                grvEvaluation.Columns[i].Visible = true;
        }
        #endregion

        #region Buttons

        protected void btnModifyDetail_Click(object sender, EventArgs e)
        {
            if (ModifiedScoreDetails != null)
            {
                List<EvaDetailScores.ScoreDetails> tempScoreDetails = new List<EvaDetailScores.ScoreDetails>();
                tempScoreDetails = CurrentScoreDetails.ToList();

                //foreach (EvaDetailScores.ScoreDetails scoreDetailInList in tempScoreDetails)
                //{
                //    foreach (GridViewRow row in grvDetails.Rows)
                //    {
                //        if (scoreDetailInList.detailId == Convert.ToInt32(grvDetails.DataKeys[row.RowIndex].Values["detailId"]))
                //        {

                //        }
                //    }
                //}
                int evaId = 0;
                decimal Score = 0;
                int month = 0;
                foreach (EvaDetailScores.ScoreDetails modifiedInList in ModifiedScoreDetails)
                {
                    var found = tempScoreDetails.FirstOrDefault(c => c.detailId == modifiedInList.detailId && c.monthNumber == modifiedInList.monthNumber);
                    found.score = modifiedInList.score;
                    Score = Score + found.score;
                    evaId = found.evaluationId;
                    month = found.monthNumber;
                }

                CurrentScoreDetails = tempScoreDetails;

                FillEvaluationFromDetail(evaId, month, Score);

                grvEvaluation.DataSource = CurrentScores;
                grvEvaluation.DataBind();
                CalculateScores();
                divDetails.Visible = false;
            }
        }

        protected void btnSearchResidents_Click(object sender, EventArgs e)
        {
            FillResidents();
        }

        protected void btnQ1_Click(object sender, EventArgs e)
        {
            ShowColumnsInEvaluation(1);
        }

        protected void btnQ2_Click(object sender, EventArgs e)
        {
            ShowColumnsInEvaluation(2);
        }

        protected void btnQ3_Click(object sender, EventArgs e)
        {
            ShowColumnsInEvaluation(3);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (grvResidents.SelectedIndex < 0)
                {
                    Master.Master.Message("Debe de seleccionar un médico residente.", "", 2);
                    return;
                }

                //if the resident has been transferred
                if (!ValidateResidentTransferred())
                    return;

                foreach (EvaDetailScores.Scores scoreInList in CurrentScores)
                {
                    EvaDetailScores.Scores obj = new EvaDetailScores.Scores();
                    foreach (EvaDetailScores.ScoreDetails detailInList in CurrentScoreDetails.Where(d => d.evaluationId == scoreInList.evaluationId).OrderBy(d => d.detailId).ThenBy(d => d.monthNumber))
                    {
                        switch (detailInList.monthNumber)
                        {
                            case 7:
                                obj.jul = detailInList.score;
                                break;
                            case 8:
                                obj.aug = detailInList.score;
                                break;
                            case 9:
                                obj.sep = detailInList.score;
                                break;
                            case 10:
                                obj.oct = detailInList.score;
                                break;
                            case 11:
                                obj.nov = detailInList.score;
                                break;
                            case 12:
                                obj.dec = detailInList.score;
                                break;
                            case 1:
                                obj.jan = detailInList.score;
                                break;
                            case 2:
                                obj.feb = detailInList.score;
                                break;
                            case 3:
                                obj.mar = detailInList.score;
                                break;
                            case 4:
                                obj.apr = detailInList.score;
                                break;
                            case 5:
                                obj.may = detailInList.score;
                                break;
                            case 6:
                                obj.jun = detailInList.score;
                                break;
                        }

                        if (detailInList.monthNumber == 12)
                        {
                            EvaDetailScores.IfExists(
                            detailInList.detailId,
                            Convert.ToInt32(grvResidents.SelectedDataKey[0]),
                            obj.jul, obj.aug, obj.sep, obj.oct,
                            obj.nov, obj.dec, obj.jan, obj.feb,
                            obj.mar, obj.apr, obj.may, obj.jun,
                            (int)((SysUserAccess)Session["access"]).UserCenterId);
                        }
                    }
                }
                //clean
                btnCancel_Click(null, null);
                btnSearchResidents_Click(null, null);
                Master.Master.Message("Los datos para este Médico Residente se han guardado exitosamente.", "", 3);
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Master.Master.PersonInfo("", "", "");
            CurrentScores = null;
            CurrentScoreDetails = null;
            ModifiedScoreDetails = null;
            grvEvaluation.DataSource = null;
            grvEvaluation.DataBind();
            grvDetails.DataSource = null;
            grvDetails.DataBind();
            divDetails.Visible = false;
            btnSave.Visible = true;
            btnCancel.Visible = true;
            btnQ1.Visible = true;
            btnQ2.Visible = true;
            btnQ3.Visible = true;
            grvResidents.SelectedIndex = -1;
            Session["ResidentStatusId"] = null;
        }

        #endregion

        #region GridViews

        protected void grvResidents_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Master.Master.PersonInfo(grvResidents.SelectedDataKey[1].ToString(), grvResidents.SelectedDataKey[2].ToString(), grvResidents.SelectedDataKey[3].ToString());

                CurrentScores = EvaDetailScores.GetScores((int)grvResidents.SelectedDataKey[0], (int)((SysUserAccess)Session["access"]).CenterId);
                if (CurrentScores.Count > 0)
                {
                    grvEvaluation.DataSource = CurrentScores;
                    grvEvaluation.DataBind();
                    CalculateScores();

                    //allow just the evaluations that sum 100
                    decimal footerPercent = 0;
                    foreach (EvaDetailScores.Scores scoreInList in CurrentScores)
                        footerPercent = footerPercent + scoreInList.percent;

                    if (footerPercent < 100 || footerPercent > 100)
                    {
                        Master.Master.Message("La suma total de todas la evaluaciones debe de ser igual a cien (100). Favor ponerse en contacto con el Director de Docencia de este centro.", "", 2);
                        ShowColumnsInEvaluation(0);
                        btnQ1.Visible = false;
                        btnQ2.Visible = false;
                        btnQ3.Visible = false;
                        btnSave.Visible = false;
                        btnCancel.Visible = false;
                        return;
                    }
                    else
                    {
                        btnQ1.Visible = true;
                        btnQ2.Visible = true;
                        btnQ3.Visible = true;
                        btnSave.Visible = true;
                        btnCancel.Visible = true;
                    }

                    //if the resident has been transferred
                    Session["ResidentStatusId"] = ResResidents.GetCurrentStatus((int)grvResidents.SelectedDataKey[0]);
                    if (!ValidateResidentTransferred())
                        return;

                    CurrentScoreDetails = EvaDetailScores.GetScoreDetails((int)grvResidents.SelectedDataKey[0], (int)((SysUserAccess)Session["access"]).CenterId);
                }
                else
                {
                    Master.Master.Message("No existen evaluaciones disponibles para este centro.", "", 1);
                    return;
                }
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }
        
        #region July
        protected void JulHeader_Click(object sender, EventArgs e)
        {
            HeaderAction("txtJul", "actionJul", "txtJulFooter");
        }

        protected void actionJul_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "EditEva")
            {
                FillDetails(Convert.ToInt32(e.CommandArgument), 7);
            }
        }

        protected void txtJul_TextChanged(object sender, EventArgs e)
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "DoPostBack", "__doPostBack(sender, e)", true);
            DistributeScore(sender, "txtJul", 7);

            //GridViewRow row = ((GridViewRow)((TextBox)sender).NamingContainer);
            //TextBox txt = (TextBox)row.FindControl("txtJul");

            ////string x = grvEvaluation.DataKeys[row.RowIndex].Values["evaluationId"].ToString();

            //decimal newScore = Convert.ToDecimal(txt.Text);

            ////IList<EvaDetailScores.ScoreDetails> tempScoreDetails = new List<EvaDetailScores.ScoreDetails>();
            ////tempScoreDetails = CurrentScoreDetails.ToList();

            //foreach (EvaDetailScores.ScoreDetails modifiedInList in CurrentScoreDetails.Where(u => u.evaluationId == Convert.ToInt32(grvEvaluation.DataKeys[row.RowIndex].Values["evaluationId"]) && u.monthNumber == 7))
            //{
            //    if (modifiedInList.maxPoints <= newScore)
            //    {
            //        modifiedInList.score = modifiedInList.maxPoints;
            //        newScore = newScore - modifiedInList.maxPoints;
            //    }
            //    else
            //    {
            //        modifiedInList.score = newScore;
            //        newScore = 0;
            //    }
            //}

            //CurrentScoreDetails = tempScoreDetails;
            //foreach (EvaDetailScores.ScoreDetails modifiedInList in CurrentScoreDetails)
            //{
            //    if (modifiedInList.evaluationId == Convert.ToInt32(grvEvaluation.DataKeys[row.RowIndex].Values["evaluationId"]) && modifiedInList.monthNumber == 7)
            //    {
            //        EvaDetailScores.ScoreDetails SelectedScoreDetails = new EvaDetailScores.ScoreDetails();
            //        if (modifiedInList.maxPoints <= newScore)
            //        {
            //            SelectedScoreDetails.score = modifiedInList.maxPoints;
            //            newScore = newScore - modifiedInList.maxPoints;
            //        }
            //        else
            //        {
            //            SelectedScoreDetails.score = newScore;
            //            newScore = 0;
            //        }
            //        SelectedScoreDetails.evaluationId = modifiedInList.evaluationId;
            //        SelectedScoreDetails.detailId = modifiedInList.detailId;
            //        SelectedScoreDetails.detailName = modifiedInList.detailName;
            //        SelectedScoreDetails.maxPoints = modifiedInList.maxPoints;
            //        SelectedScoreDetails.monthNumber = modifiedInList.monthNumber;
            //        tempScoreDetails.Add(SelectedScoreDetails);
            //    }

            //    //var found = tempScoreDetails.FirstOrDefault(c => c.evaluationId == 3 && c.monthNumber == 7);
            //    ////if (found.maxPoints <= newScore)
            //    ////{
            //    ////    found.score = modifiedInList.maxPoints;
            //    ////    newScore = newScore - modifiedInList.maxPoints;
            //    ////}
            //    ////else
            //    ////{
            //    //found.score = modifiedInList.maxPoints;
            //    ////newScore = 5;
            //    ////}
            //}
            //ModifiedScoreDetails = tempScoreDetails;

            //tempScoreDetails = CurrentScoreDetails.ToList();
            //foreach (EvaDetailScores.ScoreDetails modifiedInList in ModifiedScoreDetails)
            //{
            //    var found = tempScoreDetails.FirstOrDefault(c => c.evaluationId == Convert.ToInt32(grvEvaluation.DataKeys[row.RowIndex].Values["evaluationId"]) && c.monthNumber == 7);
            //    found.score = 10;// modifiedInList.score;
            //}


        }

        protected void txtJulFooter_TextChanged(object sender, EventArgs e)
        {
            DistributeScoreInEvaluations("txtJulFooter", 7);
        }
        #endregion

        #region August
        protected void AugHeader_Click(object sender, EventArgs e)
        {
            HeaderAction("txtAug", "actionAug", "txtAugFooter");
        }

        protected void actionAug_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "EditEva")
            {
                FillDetails(Convert.ToInt32(e.CommandArgument), 8);
            }
        }

        protected void txtAug_TextChanged(object sender, EventArgs e)
        {
            DistributeScore(sender, "txtAug", 8);
        }

        protected void txtAugFooter_TextChanged(object sender, EventArgs e)
        {
            DistributeScoreInEvaluations("txtAugFooter", 8);
        }
        #endregion

        #region September
        protected void SepHeader_Click(object sender, EventArgs e)
        {
            HeaderAction("txtSep", "actionSep", "txtSepFooter");
        }

        protected void actionSep_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "EditEva")
            {
                FillDetails(Convert.ToInt32(e.CommandArgument), 9);
            }
        }

        protected void txtSep_TextChanged(object sender, EventArgs e)
        {
            DistributeScore(sender, "txtSep", 9);
        }

        protected void txtSepFooter_TextChanged(object sender, EventArgs e)
        {
            DistributeScoreInEvaluations("txtSepFooter", 9);
        }
        #endregion

        #region October
        protected void OctHeader_Click(object sender, EventArgs e)
        {
            HeaderAction("txtOct", "actionOct", "txtOctFooter");
        }

        protected void actionOct_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "EditEva")
            {
                FillDetails(Convert.ToInt32(e.CommandArgument), 10);
            }
        }

        protected void txtOct_TextChanged(object sender, EventArgs e)
        {
            DistributeScore(sender, "txtOct", 10);
        }

        protected void txtOctFooter_TextChanged(object sender, EventArgs e)
        {
            DistributeScoreInEvaluations("txtOctFooter", 10);
        }
        #endregion

        #region November
        protected void NovHeader_Click(object sender, EventArgs e)
        {
            HeaderAction("txtNov", "actionNov", "txtNovFooter");
        }

        protected void actionNov_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "EditEva")
            {
                FillDetails(Convert.ToInt32(e.CommandArgument), 11);
            }
        }

        protected void txtNov_TextChanged(object sender, EventArgs e)
        {
            DistributeScore(sender, "txtNov", 11);
        }

        protected void txtNovFooter_TextChanged(object sender, EventArgs e)
        {
            DistributeScoreInEvaluations("txtNovFooter", 11);
        }
        #endregion

        #region December
        protected void DecHeader_Click(object sender, EventArgs e)
        {
            HeaderAction("txtDec", "actionDec", "txtDecFooter");
        }

        protected void actionDec_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "EditEva")
            {
                FillDetails(Convert.ToInt32(e.CommandArgument), 12);
            }
        }

        protected void txtDec_TextChanged(object sender, EventArgs e)
        {
            DistributeScore(sender, "txtDec", 12);
        }

        protected void txtDecFooter_TextChanged(object sender, EventArgs e)
        {
            DistributeScoreInEvaluations("txtDecFooter", 12);
        }
        #endregion

        #region January
        protected void JanHeader_Click(object sender, EventArgs e)
        {
            HeaderAction("txtJan", "actionJan", "txtJanFooter");
        }

        protected void actionJan_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "EditEva")
            {
                FillDetails(Convert.ToInt32(e.CommandArgument), 1);
            }
        }

        protected void txtJan_TextChanged(object sender, EventArgs e)
        {
            DistributeScore(sender, "txtJan", 1);
        }

        protected void txtJanFooter_TextChanged(object sender, EventArgs e)
        {
            DistributeScoreInEvaluations("txtJanFooter", 1);
        }
        #endregion

        #region February
        protected void FebHeader_Click(object sender, EventArgs e)
        {
            HeaderAction("txtFeb", "actionFeb", "txtFebFooter");
        }

        protected void actionFeb_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "EditEva")
            {
                FillDetails(Convert.ToInt32(e.CommandArgument), 2);
            }
        }

        protected void txtFeb_TextChanged(object sender, EventArgs e)
        {
            DistributeScore(sender, "txtFeb", 2);
        }

        protected void txtFebFooter_TextChanged(object sender, EventArgs e)
        {
            DistributeScoreInEvaluations("txtFebFooter", 2);
        }
        #endregion

        #region Mar
        protected void MarHeader_Click(object sender, EventArgs e)
        {
            HeaderAction("txtMar", "actionMar", "txtMarFooter");
        }

        protected void actionMar_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "EditEva")
            {
                FillDetails(Convert.ToInt32(e.CommandArgument), 3);
            }
        }

        protected void txtMar_TextChanged(object sender, EventArgs e)
        {
            DistributeScore(sender, "txtMar", 3);
        }

        protected void txtMarFooter_TextChanged(object sender, EventArgs e)
        {
            DistributeScoreInEvaluations("txtMarFooter", 3);
        }
        #endregion

        #region April
        protected void AprHeader_Click(object sender, EventArgs e)
        {
            HeaderAction("txtApr", "actionApr", "txtAprFooter");
        }

        protected void actionApr_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "EditEva")
            {
                FillDetails(Convert.ToInt32(e.CommandArgument), 4);
            }
        }

        protected void txtApr_TextChanged(object sender, EventArgs e)
        {
            DistributeScore(sender, "txtApr", 4);
        }

        protected void txtAprFooter_TextChanged(object sender, EventArgs e)
        {
            DistributeScoreInEvaluations("txtAprFooter", 4);
        }
        #endregion

        #region May
        protected void MayHeader_Click(object sender, EventArgs e)
        {
            HeaderAction("txtMay", "actionMay", "txtMayFooter");
        }

        protected void actionMay_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "EditEva")
            {
                FillDetails(Convert.ToInt32(e.CommandArgument), 5);
            }
        }

        protected void txtMay_TextChanged(object sender, EventArgs e)
        {
            DistributeScore(sender, "txtMay", 5);
        }

        protected void txtMayFooter_TextChanged(object sender, EventArgs e)
        {
            DistributeScoreInEvaluations("txtMayFooter", 5);
        }
        #endregion

        #region June
        protected void JunHeader_Click(object sender, EventArgs e)
        {
            HeaderAction("txtJun", "actionJun", "txtJunFooter");
        }

        protected void actionJun_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "EditEva")
            {
                FillDetails(Convert.ToInt32(e.CommandArgument), 6);
            }
        }

        protected void txtJun_TextChanged(object sender, EventArgs e)
        {
            DistributeScore(sender, "txtJun", 6);
        }

        protected void txtJunFooter_TextChanged(object sender, EventArgs e)
        {
            DistributeScoreInEvaluations("txtJunFooter", 6);
        }
        #endregion

        //detail
        protected void txtDetailScores_TextChanged(object sender, EventArgs e)
        {
            SumDetailScores();
            IList<EvaDetailScores.ScoreDetails> tempScoreDetails = new List<EvaDetailScores.ScoreDetails>();

            foreach (GridViewRow row in grvDetails.Rows)
            {
                EvaDetailScores.ScoreDetails SelectedScoreDetails = new EvaDetailScores.ScoreDetails();
                SelectedScoreDetails.evaluationId = Convert.ToInt32(grvDetails.DataKeys[row.RowIndex].Values["evaluationId"]);
                SelectedScoreDetails.detailId = Convert.ToInt32(grvDetails.DataKeys[row.RowIndex].Values["detailId"]);
                SelectedScoreDetails.detailName = grvDetails.DataKeys[row.RowIndex].Values["detailName"].ToString();
                SelectedScoreDetails.maxPoints = Convert.ToInt32(grvDetails.DataKeys[row.RowIndex].Values["maxPoints"]);
                TextBox txt = row.FindControl("txtDetailScores") as TextBox;
                SelectedScoreDetails.score = Convert.ToDecimal(txt.Text);// Convert.ToDecimal(grvDetails.DataKeys[row.RowIndex].Values["score"]);
                SelectedScoreDetails.monthNumber = Convert.ToInt32(grvDetails.DataKeys[row.RowIndex].Values["monthNumber"]);
                tempScoreDetails.Add(SelectedScoreDetails);
            }
            ModifiedScoreDetails = tempScoreDetails;
            //foreach (GridViewRow row in grvDetails.Rows)
            //{
            //    TextBox txt = row.FindControl("txtDetailScores") as TextBox;
            //    SelectedScoreDetails.ev
            //}
        }
        #endregion

        protected void txtSearchName_TextChanged(object sender, EventArgs e)
        {
            FillResidents();
        }

        protected void grvResidents_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvResidents.PageIndex = e.NewPageIndex;
            grvResidents.SelectedIndex = -1;
            btnSearchResidents_Click(null, null);
        }
    }
}