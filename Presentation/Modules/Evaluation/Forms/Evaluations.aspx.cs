﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI.WebControls;
using Evaluation.Evaluations;
using General.Forms;
using General.GeneralCommons;
using Residences.Grades;
using Residences.Residences;
using Residences.TeachingYears;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Evaluation.Forms
{
    public partial class Evaluations : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.Evaluations))
                    {
                        if (!IsPostBack)
                        {
                            Initialize();
                            FillEvaluations();
                            divGrades.Visible = false;

                            if ((SysSecurityAccess.GetCurrentSQLServerDateTime().Month - Convert.ToDateTime(((SysUserAccess)Session["access"]).StartDate).Month) >= 1)
                            {
                                Master.Master.Message("Estas evaluaciones no pueden ser modificadas debido a que ya ha pasado un mes desde el inicio de docencia.", "", 4);
                                btnCreate.Visible = false;
                            }
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        #region Methods
        private void Initialize()
        {
            try
            {
                ddlResidences.Items.Clear();
                ddlResidences.DataTextField = "residenceName";
                ddlResidences.DataValueField = "residenceId";
                ddlResidences.DataSource = ResResidences.GetResidences((int)((SysUserAccess)Session["access"]).CenterId, (int)((SysUserAccess)Session["access"]).TeachingYearId);
                ddlResidences.DataBind();
                ddlResidences.Items.Insert(0, new ListItem("-- Todas las Residencias --", "0"));

                ddlGrades.DataTextField = "gradeName";
                ddlGrades.DataValueField = "gradeId";
                ddlGrades.DataSource = ResGrades.GetGrades();
                ddlGrades.DataBind();
                ddlGrades.Items.Insert(0, new ListItem("-- Todos los Grados --", "0"));

                chkActives.Checked = true;

                ddlTeachingYears.Items.Clear();
                ddlTeachingYears.DataTextField = "teachingYear";
                ddlTeachingYears.DataValueField = "teachingYearId";
                ddlTeachingYears.DataSource = ResTeachingYears.GetTeachingYears((int)((SysUserAccess)Session["access"]).CenterId);
                ddlTeachingYears.DataBind();

                grvResidences.DataSource = EvaEvaluations.GetEvaluationResidencesTotalPercent((int)((SysUserAccess)Session["access"]).TeachingYearId, (int)((SysUserAccess)Session["access"]).CenterId);
                grvResidences.DataBind();
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillEvaluations()
        {
            try
            {
                int? residenceId = Convert.ToInt32(ddlResidences.SelectedValue);
                int? gradeId = Convert.ToInt32(ddlGrades.SelectedValue);

                if (residenceId == 0)
                    residenceId = null;

                if (gradeId == 0)
                    gradeId = null;

                grvEvaluations.DataSource = EvaEvaluations.GetEvaluations(residenceId, gradeId, chkActives.Checked, (int)((SysUserAccess)Session["access"]).CenterId, (int)((SysUserAccess)Session["access"]).TeachingYearId);
                grvEvaluations.DataBind();

                divDetail.Visible = false;
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillGridViewsInDetail(int? evaluationId)
        {
            try
            {
                grvEvaluationResidences.DataSource = EvaEvaluations.GetEvaluationResidencesByEvaluationId(evaluationId, (int)((SysUserAccess)Session["access"]).CenterId);
                grvEvaluationResidences.DataBind();

                grvEvaluationGrades.DataSource = EvaEvaluations.GetEvaluationGradesByEvaluationId(evaluationId);
                grvEvaluationGrades.DataBind();
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }
        #endregion

        #region Buttons
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillEvaluations();            
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            grvEvaluations.SelectedIndex = -1;
            txtEvaluationName.Text = string.Empty;
            txtPercent.Text = string.Empty;
            //txtVersion.Text = "1";
            chkStatus.Checked = true;
            divDetail.Visible = true;

            FillGridViewsInDetail(null);
            GenForms.UnCheckItemsInGridView(ref grvEvaluationResidences, "chkResidenceHeader", "chkResidence");
            GenForms.UnCheckItemsInGridView(ref grvEvaluationGrades, "chkGradeHeader", "chkGrade");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ////validation
                //decimal _score = Convert.ToDecimal(txtPercent.Text);
                //foreach (GridViewRow row in grvEvaluations.Rows)
                //    _score = _score + Convert.ToDecimal(grvEvaluations.DataKeys[row.RowIndex].Values["evaluationPercent"]);

                //if (_score > 100)
                //{
                //    Master.Master.Message("La suma total de todas las evaluaciones debe de ser igual a cien (100).", "", 2);
                //    return;
                //}

                if ((SysSecurityAccess.GetCurrentSQLServerDateTime().Month - Convert.ToDateTime(((SysUserAccess)Session["access"]).StartDate).Month) >= 1)
                {
                    Master.Master.Message("Estas evaluaciones no pueden ser modificadas debido a que ya ha pasado un mes desde el inicio de docencia.", "", 4);
                    return;
                }

                if (divDetail.Visible == false)
                {
                    Master.Master.Message("Seleccione una evaluación.", "", 2);
                    return;
                }
                
                //(grvEvaluationResidences.HeaderRow.FindControl("chkResidenceHeader") as CheckBox).Checked = true;
                //GenForms.CheckItemsInGridView(ref grvEvaluationResidences, "chkResidenceHeader", "chkResidence");
                if (GenForms.CountSelectedElementsInGridView(ref grvEvaluationResidences, "chkResidence") == 0)
                {
                    Master.Master.Message("Debe elegir la(s) residencia(s) correspondiente(s) para esta evaluación.", "", 2);
                    return;
                }

                (grvEvaluationGrades.HeaderRow.FindControl("chkGradeHeader") as CheckBox).Checked = true;
                GenForms.CheckItemsInGridView(ref grvEvaluationGrades, "chkGradeHeader", "chkGrade");
                if (GenForms.CountSelectedElementsInGridView(ref grvEvaluationGrades, "chkGrade") == 0)
                {
                    Master.Master.Message("Debe elegir el/los grado(s) correspondiente(s) para esta evaluación.", "", 2);
                    return;
                }

                int _evaluationId = 0;
                EvaEvaluations nEvaluation;
                if (grvEvaluations.SelectedIndex > -1)
                {
                    _evaluationId = Convert.ToInt32(grvEvaluations.SelectedDataKey[0]);
                    nEvaluation = EvaEvaluations.Get(_evaluationId);
                }
                else
                    nEvaluation = new EvaEvaluations();

                nEvaluation.EvaluationName = txtEvaluationName.Text.ToUpper();
                nEvaluation.EvaluationPercent = Convert.ToDecimal(txtPercent.Text);
                nEvaluation.TeachingYearId = ((SysUserAccess)Session["access"]).TeachingYearId;

                if (grvEvaluations.SelectedIndex > -1)
                {
                    nEvaluation.EvaluationStatus = chkStatus.Checked;
                    nEvaluation.ModifiedBy = (int)((SysUserAccess)Session["access"]).UserCenterId;
                    nEvaluation.CreatedDate = DateTime.Now;
                    nEvaluation.Update();
                }
                else
                {
                    nEvaluation.CenterId = (int)((SysUserAccess)Session["access"]).CenterId;
                    nEvaluation.EvaluationStatus = true;
                    nEvaluation.CreatedBy = (int)((SysUserAccess)Session["access"]).UserCenterId;
                    nEvaluation.CreatedDate = DateTime.Now;
                    _evaluationId = nEvaluation.Insert();
                }

                foreach (GridViewRow row in grvEvaluationResidences.Rows)
                    EvaEvaluations.EvaluationResidences_IfExists(_evaluationId, Convert.ToInt32(grvEvaluationResidences.DataKeys[row.RowIndex].Values["residenceId"]), (row.FindControl("chkResidence") as CheckBox).Checked);

                foreach (GridViewRow row in grvEvaluationGrades.Rows)
                    EvaEvaluations.EvaluationGrades_IfExists(_evaluationId, Convert.ToInt32(grvEvaluationGrades.DataKeys[row.RowIndex].Values["gradeId"]), (row.FindControl("chkGrade") as CheckBox).Checked);

                Master.Master.Message("La evaluación han sido guardada con éxito.", "", 3);
                FillEvaluations();
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divDetail.Visible = false;
        }
        #endregion

        #region GridViews
        protected void grvEvaluations_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillGridViewsInDetail(Convert.ToInt32(grvEvaluations.SelectedDataKey[0]));
                txtEvaluationName.Text = grvEvaluations.SelectedDataKey[1].ToString();
                txtPercent.Text = grvEvaluations.SelectedDataKey[2].ToString();
                //txtVersion.Text = grvEvaluations.SelectedDataKey[3].ToString();
                chkStatus.Checked = Convert.ToBoolean(grvEvaluations.SelectedDataKey[3]);
                divDetail.Visible = true;
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        protected void grvEvaluations_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvEvaluations.PageIndex = e.NewPageIndex;
            grvEvaluations.SelectedIndex = -1;
            FillEvaluations();
            if (divDetail.Visible == true)
                divDetail.Visible = true;
        }

        protected void chkResidenceHeader_CheckedChanged(object sender, EventArgs e)
        {
            GenForms.CheckItemsInGridView(ref grvEvaluationResidences, "chkResidenceHeader", "chkResidence");

            //bool _bit;
            //if ((grvEvaluationResidences.HeaderRow.FindControl("chkResidenceHeader") as CheckBox).Checked)
            //    _bit = true;
            //else
            //    _bit = false;

            //foreach (GridViewRow row in grvEvaluationResidences.Rows)
            //    (row.FindControl("chkResidence") as CheckBox).Checked = _bit;
        }

        protected void chkGradeHeader_CheckedChanged(object sender, EventArgs e)
        {
            GenForms.CheckItemsInGridView(ref grvEvaluationGrades, "chkGradeHeader", "chkGrade");
        }
        #endregion
    }
}