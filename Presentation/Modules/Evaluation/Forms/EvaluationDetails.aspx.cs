﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI.WebControls;
using Evaluation.EvaluationDetials;
using Evaluation.Evaluations;
using General.Forms;
using General.GeneralCommons;
using Residences.Grades;
using Residences.Residences;
using Residences.TeachingYears;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Evaluation.Forms
{
    public partial class EvaluationDetails : System.Web.UI.Page
    {
        private IList<EvaEvaluationDetails.EvaluationDetails> AsignedDetails { get { return (IList<EvaEvaluationDetails.EvaluationDetails>)ViewState["asignedDetails"]; } set { ViewState["asignedDetails"] = value; } }
        private IList<ResResidences.Residences> AsignedResidences { get { return (IList<ResResidences.Residences>)ViewState["asignedResidences"]; } set { ViewState["asignedResidences"] = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.EvaluationDetails))
                    {
                        if (!IsPostBack)
                        {
                            Initialize();
                            FillEvaluations();
                            divResidences.Visible = false;
                            divGrades.Visible = false;
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        #region Methods
        private void Initialize()
        {
            ddlResidences.Items.Clear();
            ddlResidences.DataTextField = "residenceName";
            ddlResidences.DataValueField = "residenceId";
            ddlResidences.DataSource = ResResidences.GetResidences((int)((SysUserAccess)Session["access"]).CenterId, (int)((SysUserAccess)Session["access"]).TeachingYearId);
            ddlResidences.DataBind();
            ddlResidences.Items.Insert(0, new ListItem("-- Todas las Residencias --", "0"));

            ddlGrades.DataTextField = "gradeName";
            ddlGrades.DataValueField = "gradeId";
            ddlGrades.DataSource = ResGrades.GetGrades();
            ddlGrades.DataBind();
            ddlGrades.Items.Insert(0, new ListItem("-- Todos los Grados --", "0"));

            chkActives.Checked = true;

            ddlTeachingYears.Items.Clear();
            ddlTeachingYears.DataTextField = "teachingYear";
            ddlTeachingYears.DataValueField = "teachingYearId";
            ddlTeachingYears.DataSource = ResTeachingYears.GetTeachingYears((int)((SysUserAccess)Session["access"]).CenterId);
            ddlTeachingYears.DataBind();

            grvResidences.DataSource = EvaEvaluations.GetEvaluationResidencesTotalPercent((int)((SysUserAccess)Session["access"]).TeachingYearId, (int)((SysUserAccess)Session["access"]).CenterId);
            grvResidences.DataBind();
        }

        private void FillEvaluations()
        {
            try
            {
                int? residenceId = Convert.ToInt32(ddlResidences.SelectedValue);
                int? gradeId = Convert.ToInt32(ddlGrades.SelectedValue);

                if (residenceId == 0)
                    residenceId = null;

                if (gradeId == 0)
                    gradeId = null;

                grvEvaluations.DataSource = EvaEvaluations.GetEvaluations(residenceId, gradeId, chkActives.Checked, (int)((SysUserAccess)Session["access"]).CenterId, (int)((SysUserAccess)Session["access"]).TeachingYearId);
                grvEvaluations.DataBind();

                divDetail.Visible = false;
                divDetailDetail.Visible = false;
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillEvaluationDetails(int evaluationId)
        {
            grvEvaluationDetails.DataSource = EvaEvaluationDetails.GetByEvaluationId(evaluationId);
            grvEvaluationDetails.DataBind();

            divDetailDetail.Visible = false;
        }

        private void FillGridViewsInDetail(int? detailId)
        {
            try
            {
                grvDetailResidences.DataSource = EvaEvaluationDetails.GetDetailResidencesByDetailId(detailId, (int)((SysUserAccess)Session["access"]).CenterId);
                grvDetailResidences.DataBind();

                grvDetailGrades.DataSource = EvaEvaluationDetails.GetDetailGradesByDetailId(detailId);
                grvDetailGrades.DataBind();
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }
        #endregion

        #region Buttons
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillEvaluations();
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            grvEvaluationDetails.SelectedIndex = -1;
            txtDetailName.Text = string.Empty;
            txtPoints.Text = string.Empty;
            chkStatus.Checked = true;
            divDetailDetail.Visible = true;

            FillGridViewsInDetail(null);
            GenForms.UnCheckItemsInGridView(ref grvDetailResidences, "chkResidenceHeader", "chkResidence");
            GenForms.UnCheckItemsInGridView(ref grvDetailGrades, "chkGradeHeader", "chkGrade");
        }

        //protected void btnAddDetail_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        List<EvaEvaluationDetails.EvaluationDetails> tempDetails = new List<EvaEvaluationDetails.EvaluationDetails>();
        //        if (AsignedDetails != null)
        //            tempDetails = AsignedDetails.ToList();

        //        EvaEvaluationDetails.EvaluationDetails newDetail = new EvaEvaluationDetails.EvaluationDetails();
        //        newDetail.detailId = 0;
        //        newDetail.detailName = txtDetailName.Text;
        //        newDetail.detailMaxPoints = Convert.ToInt32(txtPoints.Text);
        //        newDetail.detailStatus = chkStatus.Checked;
                                
        //        tempDetails.Add(newDetail);
        //        AsignedDetails = tempDetails;

        //        grvEvaluationDetails.DataSource = tempDetails;
        //        grvEvaluationDetails.DataBind();

        //        txtDetailName.Text = string.Empty;
        //        txtPoints.Text = string.Empty;
        //    }
        //    catch (Exception ex)
        //    {
        //        SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
        //    }
        //}

        //protected void btnDelete_Click(object sender, EventArgs e)
        //{
        //    foreach (GridViewRow row in grvEvaluationDetails.Rows)
        //    {
        //        if ((row.FindControl("chkDetail") as CheckBox).Checked == true)
        //        {
        //            foreach (EvaEvaluationDetails.EvaluationDetails detailInList in AsignedDetails)
        //            {
        //                if (detailInList.detailName == grvEvaluationDetails.DataKeys[row.RowIndex].Values["detailName"].ToString())
        //                {
        //                    AsignedDetails.Remove(detailInList);
        //                    grvEvaluationDetails.DataSource = AsignedDetails;
        //                    grvEvaluationDetails.DataBind();
        //                    divDetailDetail.Visible = false;
        //                    return;
        //                }
        //            }
        //        }
        //    }
        //}

        //protected void btnCancelDetail_Click(object sender, EventArgs e)
        //{
        //    divDetailDetail.Visible = false;
        //}

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //validation
            if (divDetailDetail.Visible == false)
            {
                Master.Master.Message("Seleccione un detalle.", "", 2);
                return;
            }

            int _maxPoints = Convert.ToInt32(txtPoints.Text);
            if (_maxPoints > 100)
            {
                Master.Master.Message("Los puntos deben de ser menor o igual a cien (100).", "", 2);
                return;
            }

            //foreach (GridViewRow row in grvEvaluations.Rows)
            //    _maxPoints = _maxPoints + Convert.ToInt32(grvEvaluationDetails.DataKeys[row.RowIndex].Values["detailMaxPoints"]);

            //if (_maxPoints > 100)
            //{
            //    Master.Master.Message("La suma total de todos los detalles debe de ser igual a cien (100).", "", 2);
            //    return;
            //}

            (grvDetailResidences.HeaderRow.FindControl("chkResidenceHeader") as CheckBox).Checked = true;
            GenForms.CheckItemsInGridView(ref grvDetailResidences, "chkResidenceHeader", "chkResidence");
            if (GenForms.CountSelectedElementsInGridView(ref grvDetailResidences, "chkResidence") == 0)
            {
                Master.Master.Message("Debe elegir la(s) residencia(s) correspondiente(s) a este detalle.", "", 2);
                return;
            }

            (grvDetailGrades.HeaderRow.FindControl("chkGradeHeader") as CheckBox).Checked = true;
            GenForms.CheckItemsInGridView(ref grvDetailGrades, "chkGradeHeader", "chkGrade");
            if (GenForms.CountSelectedElementsInGridView(ref grvDetailGrades, "chkGrade") == 0)
            {
                Master.Master.Message("Debe elegir el/los grado(s) correspondiente(s) a este detalle.", "", 2);
                return;
            }

            int _detailId = 0;
            EvaEvaluationDetails nDetail;
            if (grvEvaluationDetails.SelectedIndex > -1)
            {
                _detailId = Convert.ToInt32(grvEvaluationDetails.SelectedDataKey[0]);
                nDetail = EvaEvaluationDetails.Get(_detailId);
            }
            else
                nDetail = new EvaEvaluationDetails();

            nDetail.EvaluationId = Convert.ToInt32(grvEvaluations.SelectedDataKey[0]);
            nDetail.DetailName = txtDetailName.Text.Trim();
            nDetail.DetailMaxPoints = Convert.ToInt32(txtPoints.Text);
            nDetail.CenterId = (int)((SysUserAccess)Session["access"]).CenterId;

            if (grvEvaluationDetails.SelectedIndex > -1)
            {
                nDetail.DetailStatus = chkStatus.Checked;
                nDetail.ModifiedBy = (int)((SysUserAccess)Session["access"]).UserCenterId;
                nDetail.ModifiedDate = DateTime.Now;
                nDetail.Update();
            }
            else
            {
                nDetail.DetailStatus = true;
                nDetail.CreatedBy = (int)((SysUserAccess)Session["access"]).UserCenterId;
                nDetail.CreatedDate = DateTime.Now;
                _detailId=nDetail.Insert();
            }

            foreach(GridViewRow row in grvDetailResidences.Rows)
                EvaEvaluationDetails.DetailResidences_IfExists(_detailId, Convert.ToInt32(grvDetailResidences.DataKeys[row.RowIndex].Values["residenceId"]), (row.FindControl("chkResidence") as CheckBox).Checked);

            foreach(GridViewRow row in grvDetailGrades.Rows)
                EvaEvaluationDetails.DetailGrades_IfExists(_detailId, Convert.ToInt32(grvDetailGrades.DataKeys[row.RowIndex].Values["gradeId"]), (row.FindControl("chkGrade") as CheckBox).Checked);

            Master.Master.Message("El detalle para la evaluación han sido guardados con éxito.", "", 3);
            FillEvaluationDetails(Convert.ToInt32(grvEvaluations.SelectedDataKey[0]));
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divDetail.Visible = false;
        }
        #endregion

        #region GridViews
        protected void grvEvaluations_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillEvaluationDetails(Convert.ToInt32(grvEvaluations.SelectedDataKey[0]));
            lblEvaluationName.Text = grvEvaluations.SelectedDataKey[1].ToString();
            lblEvaluationPercent.Text = grvEvaluations.SelectedDataKey[2].ToString();
            divDetail.Visible = true;
            divDetailDetail.Visible = false;
        }

        protected void grvEvaluationDetails_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillGridViewsInDetail(Convert.ToInt32(grvEvaluationDetails.SelectedDataKey[0]));
            txtDetailName.Text = grvEvaluationDetails.SelectedDataKey[1].ToString();
            txtPoints.Text = grvEvaluationDetails.SelectedDataKey[2].ToString();
            chkStatus.Checked = Convert.ToBoolean(grvEvaluationDetails.SelectedDataKey[3]);
            divDetailDetail.Visible = true;
        }

        //protected void grvEvaluationDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "Delete")
        //    {
        //        foreach (EvaEvaluationDetails.EvaluationDetails detailInList in AsignedDetails)
        //        {
        //            if (detailInList.detailName == e.CommandArgument.ToString())
        //            {
        //                AsignedDetails.Remove(detailInList);
        //                grvEvaluationDetails.DataSource = null;
        //                grvEvaluationDetails.DataBind();
        //                return;
        //            }
        //        }
        //    }
        //}

        protected void chkResidenceHeader_CheckedChanged(object sender, EventArgs e)
        {
            GenForms.CheckItemsInGridView(ref grvDetailResidences, "chkResidenceHeader", "chkResidence");
        }

        protected void chkGradeHeader_CheckedChanged(object sender, EventArgs e)
        {
            GenForms.CheckItemsInGridView(ref grvDetailGrades, "chkGradeHeader", "chkGrade");
        }

        #endregion
    }
}