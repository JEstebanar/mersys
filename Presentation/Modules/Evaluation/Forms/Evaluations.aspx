﻿<%@ Page Title="Evaluaciones" Language="C#" MasterPageFile="~/Modules/Evaluation/Evaluation.master"
    AutoEventWireup="true" CodeBehind="Evaluations.aspx.cs" Inherits="Presentation.Modules.Evaluation.Forms.Evaluations" %>

<%@ MasterType VirtualPath="~/Modules/Evaluation/Evaluation.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upanelEvaluation" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:ValidationSummary ID="EvaluationValidationSummary" runat="server" CssClass="alert alert-danger"
                ValidationGroup="EvaluationValidationGroup" />
            <fieldset>
                <legend>Evaluaciones</legend>
                <div class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label">
                            Año Docente</label>
                        <div class="controls">
                            <asp:DropDownList ID="ddlTeachingYears" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form-search">
                    <asp:DropDownList ID="ddlResidences" runat="server">
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlGrades" runat="server">
                    </asp:DropDownList>
                    <asp:CheckBox ID="chkActives" runat="server" />
                    <label>
                        Activas</label>
                    <asp:Button ID="btnSearch" runat="server" Text="Buscar" class="btn btn-info" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnCreate" runat="server" Text="Crear" class="btn" OnClick="btnCreate_Click" />
                </div>
                <br />
                <div class="control-group">
                    <asp:GridView ID="grvEvaluations" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                        AutoGenerateColumns="False" DataKeyNames="evaluationId,evaluationName,evaluationPercent,evaluationStatus"
                        OnSelectedIndexChanged="grvEvaluations_SelectedIndexChanged" AllowPaging="True"
                        OnPageIndexChanging="grvEvaluations_PageIndexChanging">
                        <Columns>
                            <asp:BoundField DataField="evaluationName" HeaderText="Nombre de la Evaluacón" SortExpression="evaluationName" />
                            <asp:BoundField DataField="evaluationPercent" HeaderText="Porciento" SortExpression="evaluationPercent" />
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" CommandName="Select"
                                        class="icon-edit" ImageUrl="~/Styles/Images/blue/pencil.png" ToolTip="Editar"
                                        Text="Editar" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="divDetail" runat="server">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">
                                Evaluación</label>
                            <div class="controls">
                                <asp:TextBox ID="txtEvaluationName" runat="server" class="input-xxlarge"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="EvaluationNameRequired" runat="server" ControlToValidate="txtEvaluationName"
                                    CssClass="failureNotification" ErrorMessage="Nombre de la evaluación es requerido."
                                    ValidationGroup="EvaluationValidationGroup">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Porciento</label>
                            <div class="controls">
                                <asp:TextBox ID="txtPercent" runat="server" class="input-mini"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="PercentRequired" runat="server" ControlToValidate="txtPercent"
                                    CssClass="failureNotification" ErrorMessage="Porciento de la evaluación es requerido."
                                    ValidationGroup="EvaluationValidationGroup">*</asp:RequiredFieldValidator>
                                <asp:FilteredTextBoxExtender ID="txtPercentFilteredTextBoxExtender" runat="server"
                                    TargetControlID="txtPercent" ValidChars="0123456789.">
                                </asp:FilteredTextBoxExtender>
                            </div>
                        </div>
                        <%--<div class="control-group">
                        <label class="control-label">
                            Versión</label>
                        <div class="controls">
                            <asp:TextBox ID="txtVersion" runat="server" disabled="true" class="input-mini"></asp:TextBox>
                        </div>
                    </div>--%>
                        <div class="control-group">
                            <label class="control-label">
                                Estatus</label>
                            <div class="controls">
                                <asp:CheckBox ID="chkStatus" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div id="divResidences" runat="server" class="span6">
                            <div class="well">
                                <fieldset>
                                    <legend>Para las Residencias</legend>
                                    <div class="controls">
                                        <asp:GridView ID="grvEvaluationResidences" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                                            AutoGenerateColumns="False" DataKeyNames="residenceId">
                                            <Columns>
                                                <asp:BoundField DataField="preResidenceName" HeaderText="Pre Requisitos" SortExpression="preResidenceName" />
                                                <asp:BoundField DataField="residenceName" HeaderText="Residencias" SortExpression="residenceName" />
                                                <asp:TemplateField SortExpression="selected">
                                                    <HeaderTemplate>
                                                       <%-- <asp:UpdatePanel UpdateMode="Conditional" ID="upanelResidencesHeader" runat="server">
                                                            <ContentTemplate>--%>
                                                                <asp:CheckBox ID="chkResidenceHeader" runat="server" OnCheckedChanged="chkResidenceHeader_CheckedChanged"
                                                                    AutoPostBack="true" />
                                                            <%--</ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="chkResidenceHeader" EventName="CheckedChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkResidence" runat="server" Checked='<%# Bind("selected") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="divGrades" runat="server" class="span6">
                            <div class="well">
                                <fieldset>
                                    <legend>Para los Grados</legend>
                                    <div class="controls">
                                        <asp:GridView ID="grvEvaluationGrades" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                                            AutoGenerateColumns="false" DataKeyNames="gradeId">
                                            <Columns>
                                                <asp:BoundField DataField="gradeName" HeaderText="Grados" SortExpression="gradeName" />
                                                <asp:TemplateField SortExpression="selected">
                                                    <HeaderTemplate>
                                                        <asp:UpdatePanel UpdateMode="Conditional" ID="upanelGradesHeader" runat="server">
                                                            <ContentTemplate>
                                                                <asp:CheckBox ID="chkGradeHeader" runat="server" OnCheckedChanged="chkGradeHeader_CheckedChanged"
                                                                    AutoPostBack="true" />
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="chkGradeHeader" EventName="CheckedChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkGrade" runat="server" Checked='<%# Bind("selected") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="grvEvaluations" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="form-actions">
        <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnSave_Click"
            ValidationGroup="EvaluationValidationGroup" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
    </div>
    <div class="row-fluid">
        <div class="span6">
            <div class="well">
                <fieldset>
                    <legend>Totales por Residencias</legend>
                    <div class="controls">
                        <asp:GridView ID="grvResidences" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                            AutoGenerateColumns="False">
                            <Columns>
                                <asp:BoundField DataField="preResidenceName" HeaderText="Pre Requisitos" SortExpression="preResidenceName" />
                                <asp:BoundField DataField="residenceName" HeaderText="Residencias" SortExpression="residenceName" />
                                <asp:BoundField DataField="totalPercent" HeaderText="Total" SortExpression="totalPercent" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</asp:Content>
