﻿<%@ Page Title="Evaluación" Language="C#" MasterPageFile="~/Modules/Evaluation/Evaluation.master"
    AutoEventWireup="true" CodeBehind="MainEvaluation.aspx.cs" Inherits="Presentation.Modules.Evaluation.Forms.MainEvaluation1" %>

<%@ MasterType VirtualPath="~/Modules/Evaluation/Evaluation.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="accordion" id="Accordion">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                    Residentes </a>
            </div>
            <div id="collapseOne" class="accordion-body collapse ">
                <div class="accordion-inner">
                    <asp:UpdatePanel ID="upanelSearch" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                    <div class="control-group">
                        <div class="form-search">
                            <asp:TextBox ID="txtSearchName" runat="server" CssClass="textEntry" placeholder="Nombre"
                                AutoPostBack="True" ontextchanged="txtSearchName_TextChanged"></asp:TextBox>
                            <asp:Button ID="btnSearchResidents" runat="server" Text="Buscar" class="btn" OnClick="btnSearchResidents_Click" />
                        </div>
                    </div>
                    <div class="control-group">
                        <asp:GridView ID="grvResidents" runat="server" Width="100%" class="table table-condensed table-striped table-bordered"
                            AutoGenerateColumns="False" 
                            DataKeyNames="residentId,fullName,residenceName,gradeName" 
                            ShowHeader="False" 
                            onselectedindexchanged="grvResidents_SelectedIndexChanged" AllowPaging="True" 
                            onpageindexchanging="grvResidents_PageIndexChanging" PageSize="7">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="personSelect" runat="server" CommandArgument='<%# Eval("residentId") %>'
                                            CommandName="Select" ToolTip="Seleccionar" Text='<%# Eval("fullName") %>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="txtSearchName" EventName="TextChanged" />
                            <asp:AsyncPostBackTrigger ControlID="btnSearchResidents" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="upanelEvaluation" runat="server">
        <ContentTemplate>
            
            <div class="control-group text-center">
                <div class="btn-group" data-toggle="buttons-radio">
                    <asp:LinkButton ID="btnQ1" runat="server" Text="Julio - Octubre" class="btn btn-success"
                        OnClick="btnQ1_Click"></asp:LinkButton>
                    <asp:LinkButton ID="btnQ2" runat="server" Text="Noviembre - Febrero" class="btn btn-success"
                        OnClick="btnQ2_Click"></asp:LinkButton>
                    <asp:LinkButton ID="btnQ3" runat="server" Text="Marzo - Junio" class="btn btn-success"
                        OnClick="btnQ3_Click"></asp:LinkButton>
                </div>
            </div>
            <div class="control-group">
                <asp:GridView ID="grvEvaluation" runat="server" class="table table-condensed table-striped table-bordered"
                    AutoGenerateColumns="False" ShowFooter="True" DataKeyNames="evaluationId">
                    <Columns>
                        <asp:BoundField DataField="evaluationName" HeaderText="Evaluación" ReadOnly="True"
                            SortExpression="evaluationName" />
                        <asp:TemplateField HeaderText="Jul" SortExpression="jul">
                            <%--<EditItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("jul") %>'></asp:Label>
                            </EditItemTemplate>--%>
                            <HeaderTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelJulHeader" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:LinkButton ID="JulHeader" runat="server" OnClick="JulHeader_Click">Jul</asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="JulHeader" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelJulText" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <%--<asp:Label ID="lblJul" runat="server" Text='<%# Bind("jul") %>'></asp:Label>--%>
                                            <asp:TextBox ID="txtJul" runat="server" Text='<%# Bind("jul") %>' Width="40px" 
                                                AutoPostBack="true" Enabled="False" MaxLength="6" Height="16" 
                                                OnTextChanged="txtJul_TextChanged"></asp:TextBox>
                                            <asp:LinkButton ID="actionJul" runat="server" CommandArgument='<%# Eval("evaluationId") %>'
                                                PostBack="false" CommandName="EditEva" class="icon-edit" OnCommand="actionJul_Command"
                                                Visible="False"></asp:LinkButton>
                                            <%--<asp:ImageButton ID="actionJul" runat="server" CommandArgument='<%# Eval("evaluationId") %>'
                                        CommandName="EditEva" class="icon-edit" OnCommand="actionJul_Command" Visible="False" PostBack="false"
                                        ToolTip="Ver Detalles" ImageUrl="../../../Styles/Images/blue/pencil.png"></asp:ImageButton>--%>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtJul" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="actionJul" EventName="Command" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelJulFooter" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtJulFooter" runat="server" Width="40px" Height="16"
                                                AutoPostBack="true" Enabled="false" MaxLength="6" OnTextChanged="txtJulFooter_TextChanged"></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtJulFooter" EventName="TextChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </FooterTemplate>                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ago" SortExpression="aug">                            
                            <HeaderTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelAugHeader" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:LinkButton ID="AugHeader" runat="server" OnClick="AugHeader_Click">Ago</asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="AugHeader" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelAugText" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtAug" runat="server" Text='<%# Bind("aug") %>' Width="40px"
                                                AutoPostBack="true" Enabled="False" MaxLength="6" Height="16" OnTextChanged="txtAug_TextChanged"></asp:TextBox>
                                            <asp:LinkButton ID="actionAug" runat="server" CommandArgument='<%# Eval("evaluationId") %>'
                                                PostBack="false" CommandName="EditEva" class="icon-edit" OnCommand="actionAug_Command"
                                                Visible="False"></asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtaug" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="actionAug" EventName="Command" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelAugFooter" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtAugFooter" runat="server" Enabled="false" MaxLength="6" Height="16"
                                                AutoPostBack="true" Width="40px" OnTextChanged="txtAugFooter_TextChanged"></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtAugFooter" EventName="TextChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sep" SortExpression="sep">
                            <HeaderTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelSepHeader" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:LinkButton ID="SepHeader" runat="server" OnClick="SepHeader_Click">Sep</asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="SepHeader" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelSepText" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtSep" runat="server" Text='<%# Bind("sep") %>' Width="40px" 
                                                AutoPostBack="true" Enabled="False" MaxLength="6" Height="16" 
                                                OnTextChanged="txtSep_TextChanged"></asp:TextBox>
                                            <asp:LinkButton ID="actionSep" runat="server" CommandArgument='<%# Eval("evaluationId") %>'
                                                PostBack="false" CommandName="EditEva" class="icon-edit" OnCommand="actionSep_Command"
                                                Visible="False"></asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtSep" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="actionSep" EventName="Command" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelSepFooter" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtSepFooter" runat="server" Width="40px" Height="16"
                                                AutoPostBack="true" Enabled="false" MaxLength="6" OnTextChanged="txtSepFooter_TextChanged"></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtSepFooter" EventName="TextChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </FooterTemplate>                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oct" SortExpression="oct">
                            <HeaderTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelOctHeader" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:LinkButton ID="OctHeader" runat="server" OnClick="OctHeader_Click">Oct</asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="OctHeader" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelOctText" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtOct" runat="server" Text='<%# Bind("oct") %>' Width="40px" 
                                                AutoPostBack="true" Enabled="False" MaxLength="6" Height="16" 
                                                OnTextChanged="txtOct_TextChanged"></asp:TextBox>
                                            <asp:LinkButton ID="actionOct" runat="server" CommandArgument='<%# Eval("evaluationId") %>'
                                                PostBack="false" CommandName="EditEva" class="icon-edit" OnCommand="actionOct_Command"
                                                Visible="False"></asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtOct" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="actionOct" EventName="Command" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelOctFooter" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtOctFooter" runat="server" Width="40px" Height="16"
                                                AutoPostBack="true" Enabled="false" MaxLength="6" OnTextChanged="txtOctFooter_TextChanged"></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtOctFooter" EventName="TextChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </FooterTemplate>                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nov" SortExpression="nov">
                            <HeaderTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelNovHeader" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:LinkButton ID="NovHeader" runat="server" OnClick="NovHeader_Click">Nov</asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="NovHeader" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelNovText" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtNov" runat="server" Text='<%# Bind("nov") %>' Width="40px" 
                                                AutoPostBack="true" Enabled="False" MaxLength="6" Height="16" 
                                                OnTextChanged="txtNov_TextChanged"></asp:TextBox>
                                            <asp:LinkButton ID="actionNov" runat="server" CommandArgument='<%# Eval("evaluationId") %>'
                                                PostBack="false" CommandName="EditEva" class="icon-edit" OnCommand="actionNov_Command"
                                                Visible="False"></asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtNov" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="actionNov" EventName="Command" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelNovFooter" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtNovFooter" runat="server" Width="40px" Height="16"
                                                AutoPostBack="true" Enabled="false" MaxLength="6" OnTextChanged="txtNovFooter_TextChanged"></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtNovFooter" EventName="TextChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </FooterTemplate>                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dic" SortExpression="dec">
                            <HeaderTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelDecHeader" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:LinkButton ID="DecHeader" runat="server" OnClick="DecHeader_Click">Dec</asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="DecHeader" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelDecText" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtDec" runat="server" Text='<%# Bind("dec") %>' Width="40px" 
                                                AutoPostBack="true" Enabled="False" MaxLength="6" Height="16" 
                                                OnTextChanged="txtDec_TextChanged"></asp:TextBox>
                                            <asp:LinkButton ID="actionDec" runat="server" CommandArgument='<%# Eval("evaluationId") %>'
                                                PostBack="false" CommandName="EditEva" class="icon-edit" OnCommand="actionDec_Command"
                                                Visible="False"></asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtDec" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="actionDec" EventName="Command" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelDecFooter" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtDecFooter" runat="server" Width="40px" Height="16"
                                                AutoPostBack="true" Enabled="false" MaxLength="6" OnTextChanged="txtDecFooter_TextChanged"></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtDecFooter" EventName="TextChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </FooterTemplate>                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ene" SortExpression="jan">
                            <HeaderTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelJanHeader" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:LinkButton ID="JanHeader" runat="server" OnClick="JanHeader_Click">Jan</asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="JanHeader" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelJanText" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtJan" runat="server" Text='<%# Bind("jan") %>' Width="40px" 
                                                AutoPostBack="true" Enabled="False" MaxLength="6" Height="16" 
                                                OnTextChanged="txtJan_TextChanged"></asp:TextBox>
                                            <asp:LinkButton ID="actionJan" runat="server" CommandArgument='<%# Eval("evaluationId") %>'
                                                PostBack="false" CommandName="EditEva" class="icon-edit" OnCommand="actionJan_Command"
                                                Visible="False"></asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtJan" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="actionJan" EventName="Command" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelJanFooter" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtJanFooter" runat="server" Width="40px" Height="16"
                                                AutoPostBack="true" Enabled="false" MaxLength="6" OnTextChanged="txtJanFooter_TextChanged"></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtJanFooter" EventName="TextChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </FooterTemplate>                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Feb" SortExpression="feb">
                            <HeaderTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelFebHeader" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:LinkButton ID="FebHeader" runat="server" OnClick="FebHeader_Click">Feb</asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="FebHeader" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelFebText" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtFeb" runat="server" Text='<%# Bind("feb") %>' Width="40px" 
                                                AutoPostBack="true" Enabled="False" MaxLength="6" Height="16" 
                                                OnTextChanged="txtFeb_TextChanged"></asp:TextBox>
                                            <asp:LinkButton ID="actionFeb" runat="server" CommandArgument='<%# Eval("evaluationId") %>'
                                                PostBack="false" CommandName="EditEva" class="icon-edit" OnCommand="actionFeb_Command"
                                                Visible="False"></asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtFeb" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="actionFeb" EventName="Command" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelFebFooter" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtFebFooter" runat="server" Width="40px" Height="16"
                                                AutoPostBack="true" Enabled="false" MaxLength="6" OnTextChanged="txtFebFooter_TextChanged"></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtFebFooter" EventName="TextChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </FooterTemplate>                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mar" SortExpression="mar">
                            <HeaderTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelMarHeader" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:LinkButton ID="MarHeader" runat="server" OnClick="MarHeader_Click">Mar</asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="MarHeader" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelMarText" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtMar" runat="server" Text='<%# Bind("mar") %>' Width="40px" 
                                                AutoPostBack="true" Enabled="False" MaxLength="6" Height="16" 
                                                OnTextChanged="txtMar_TextChanged"></asp:TextBox>
                                            <asp:LinkButton ID="actionMar" runat="server" CommandArgument='<%# Eval("evaluationId") %>'
                                                PostBack="false" CommandName="EditEva" class="icon-edit" OnCommand="actionMar_Command"
                                                Visible="False"></asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtMar" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="actionMar" EventName="Command" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelMarFooter" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtMarFooter" runat="server" Width="40px" Height="16"
                                                AutoPostBack="true" Enabled="false" MaxLength="6" OnTextChanged="txtMarFooter_TextChanged"></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtMarFooter" EventName="TextChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </FooterTemplate>                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Abr" SortExpression="apr">
                            <HeaderTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelAprHeader" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:LinkButton ID="AprHeader" runat="server" OnClick="AprHeader_Click">Apr</asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="AprHeader" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelAprText" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtApr" runat="server" Text='<%# Bind("apr") %>' Width="40px" 
                                                AutoPostBack="true" Enabled="False" MaxLength="6" Height="16" 
                                                OnTextChanged="txtApr_TextChanged"></asp:TextBox>
                                            <asp:LinkButton ID="actionApr" runat="server" CommandArgument='<%# Eval("evaluationId") %>'
                                                PostBack="false" CommandName="EditEva" class="icon-edit" OnCommand="actionApr_Command"
                                                Visible="False"></asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtApr" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="actionApr" EventName="Command" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelAprFooter" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtAprFooter" runat="server" Width="40px" Height="16"
                                                AutoPostBack="true" Enabled="false" MaxLength="6" OnTextChanged="txtAprFooter_TextChanged"></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtAprFooter" EventName="TextChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </FooterTemplate>                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="May" SortExpression="may">
                            <HeaderTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelMayHeader" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:LinkButton ID="MayHeader" runat="server" OnClick="MayHeader_Click">May</asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="MayHeader" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelMayText" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtMay" runat="server" Text='<%# Bind("may") %>' Width="40px" 
                                                AutoPostBack="true" Enabled="False" MaxLength="6" Height="16" 
                                                OnTextChanged="txtMay_TextChanged"></asp:TextBox>
                                            <asp:LinkButton ID="actionMay" runat="server" CommandArgument='<%# Eval("evaluationId") %>'
                                                PostBack="false" CommandName="EditEva" class="icon-edit" OnCommand="actionMay_Command"
                                                Visible="False"></asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtMay" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="actionMay" EventName="Command" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelMayFooter" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtMayFooter" runat="server" Width="40px" Height="16"
                                                AutoPostBack="true" Enabled="false" MaxLength="6" OnTextChanged="txtMayFooter_TextChanged"></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtMayFooter" EventName="TextChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </FooterTemplate>                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Jun" SortExpression="jun">
                            <HeaderTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelJunHeader" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:LinkButton ID="JunHeader" runat="server" OnClick="JunHeader_Click">Jun</asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="JunHeader" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelJunText" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtJun" runat="server" Text='<%# Bind("jun") %>' Width="40px" 
                                                AutoPostBack="true" Enabled="False" MaxLength="6" Height="16" 
                                                OnTextChanged="txtJun_TextChanged"></asp:TextBox>
                                            <asp:LinkButton ID="actionJun" runat="server" CommandArgument='<%# Eval("evaluationId") %>'
                                                PostBack="false" CommandName="EditEva" class="icon-edit" OnCommand="actionJun_Command"
                                                Visible="False"></asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtJun" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="actionJun" EventName="Command" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:UpdatePanel UpdateMode="Conditional" ID="upanelJunFooter" runat="server">
                                    <ContentTemplate>
                                        <div class="form-inline" style="text-align: center">
                                            <asp:TextBox ID="txtJunFooter" runat="server" Width="40px" Height="16"
                                                AutoPostBack="true" Enabled="false" MaxLength="6" OnTextChanged="txtJunFooter_TextChanged"></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtJunFooter" EventName="TextChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </FooterTemplate>                            
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="%" HeaderText="%" ReadOnly="True" SortExpression="%" />--%>
                        <asp:TemplateField HeaderText="Prom" SortExpression="average">
                            <ItemTemplate>
                                <div class="text-right">
                                    <asp:Label ID="lblAverage" runat="server" Text='<%# Bind("average") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                                <div class="text-right">
                                    <asp:Label ID="lblAverageFooter" runat="server"></asp:Label>
                                </div>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Actual">
                            <ItemTemplate>
                                <div class="text-right">
                                    <asp:Label ID="lblActualPercent" runat="server"></asp:Label>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                                <div class="text-right">
                                    <asp:Label ID="lblActualPercentFooter" runat="server"></asp:Label>
                                </div>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="%" SortExpression="percent">
                            <ItemTemplate>
                                <div class="text-right">
                                    <asp:Label ID="lblPercent" runat="server" Text='<%# Bind("percent") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                                <div class="text-right">
                                    <asp:Label ID="lblPercentFooter" runat="server"></asp:Label>
                                </div>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div id="divDetails" runat="server">
                <div class="control-group">
                    <asp:GridView ID="grvDetails" runat="server" Width="100%" class="table table-striped table-bordered"
                        AutoGenerateColumns="False" ShowFooter="True" DataKeyNames="evaluationId,detailId,detailName,maxPoints,score,monthNumber">
                        <Columns>
                            <asp:BoundField DataField="detailName" HeaderText="Detalle de Evaluación" ReadOnly="True"
                                SortExpression="detailName" />
                            <asp:BoundField DataField="maxPoints" HeaderText="Pts. Max." SortExpression="maxPoints"
                                ReadOnly="True" />
                            <asp:TemplateField HeaderText="Pts." SortExpression="score">
                                <ItemTemplate>
                                    <%--<div class="form-inline" style="text-align: center">
                                        <asp:TextBox ID="txtDetailScores" runat="server" Text='<%# Bind("score") %>' Width="30px" 
                                            BorderWidth="1.5" Height="16" AutoPostBack="True" OnTextChanged="txtDetailScores_TextChanged"></asp:TextBox>
                                    </div>--%>
                                    <asp:UpdatePanel UpdateMode="Conditional" ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <div class="form-inline" style="text-align: center">
                                                <%--<asp:Label ID="Label1" runat="server" Text='<%# Bind("Pts") %>'></asp:Label>--%>
                                                <asp:TextBox ID="txtDetailScores" runat="server" Text='<%# Bind("score") %>' Width="30px"
                                                    BorderWidth="1.5" Height="16" AutoPostBack="True" OnTextChanged="txtDetailScores_TextChanged"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="txtDetailScoresFilteredTextBoxExtender" runat="server"
                                                    TargetControlID="txtDetailScores" ValidChars="0123456789.">
                                                </asp:FilteredTextBoxExtender>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="txtDetailScores" EventName="TextChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </ItemTemplate>
                                <%--<EditItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Pts.") %>'></asp:Label>
                            </EditItemTemplate>--%>
                                <FooterTemplate>
                                    <div class="form-inline" style="text-align: center">
                                        <asp:TextBox ID="txtDetailScoresFooter" runat="server" Width="30px" BorderWidth="1.5"
                                            Height="16" Enabled="false"></asp:TextBox>
                                    </div>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="text-right">
                    <asp:Button ID="btnModifyDetail" runat="server" Text="Modificar" 
                        class="btn btn-info" onclick="btnModifyDetail_Click" />
                    <asp:Button ID="btnCancelDetail" runat="server" Text="Cancelar" class="btn" />
                </div>
            </div>
        </ContentTemplate>
                <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnQ1" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnQ2" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnQ3" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="form-actions">
        <asp:Button ID="btnSave" runat="server" Text="Guardar Evaluación" 
            class="btn btn-primary" onclick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" onclick="btnCancel_Click" />
    </div>
</asp:Content>
