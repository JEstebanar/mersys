﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using General.GeneralCommons;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using System.Reflection;
using System.IO;
using SystemSecurity.SysError;
using SystemSecurity.Reports;
using Residences.Residences;
using SystemSecurity.UserAccess;
using Residences.Grades;
using Residences.TeachingYears;
using Microsoft.Reporting.WebForms;

namespace Presentation.Modules.Reports
{
    public partial class BetweenRanges : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.ScoreReport))
                    {
                        if (!IsPostBack)
                        {
                            Initialize();
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        #region Methods
        private void Initialize()
        {
            ddlReportType.DataTextField = "reportName";
            ddlReportType.DataValueField = "reportId";
            ddlReportType.DataSource = SysReports.GetReportsList((int)((SysUserAccess)Session["access"]).CenterId, (int)GeneralCommon.ReportTypes.ScoreReports);
            ddlReportType.DataBind();
            ddlReportType.Items.Insert(0, new ListItem("-- Seleccione Reporte --", "0"));

            ddlResidences.Items.Clear();
            ddlResidences.DataTextField = "residenceName";
            ddlResidences.DataValueField = "residenceId";
            ddlResidences.DataSource = ResResidences.GetResidences((int)((SysUserAccess)Session["access"]).CenterId, (int)((SysUserAccess)Session["access"]).TeachingYearId);
            ddlResidences.DataBind();
            ddlResidences.Items.Insert(0, new ListItem("-- Todas las Residencias --", "0"));

            ddlGrades.DataTextField = "gradeName";
            ddlGrades.DataValueField = "gradeId";
            ddlGrades.DataSource = ResGrades.GetGrades();
            ddlGrades.DataBind();
            ddlGrades.Items.Insert(0, new ListItem("-- Todos los Grados --", "0"));

            ddlTeachingYears.Items.Clear();
            ddlTeachingYears.DataTextField = "teachingYear";
            ddlTeachingYears.DataValueField = "teachingYearId";
            ddlTeachingYears.DataSource = ResTeachingYears.GetTeachingYears((int)((SysUserAccess)Session["access"]).CenterId);
            ddlTeachingYears.DataBind();
        }
        #endregion

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                int centerId = (int)((SysUserAccess)Session["access"]).CenterId;
                int residenceId = Convert.ToInt32(ddlResidences.SelectedValue);
                int gradeId = Convert.ToInt32(ddlGrades.SelectedValue);
                int teachingYearId = Convert.ToInt32(ddlTeachingYears.SelectedValue);
                int from = Convert.ToInt32(txtFrom.Text);
                int to = Convert.ToInt32(txtTo.Text);
                
                //rptReports.LocalReport.DataSources.Clear();
                //ReportDataSource rdsCenter = new ReportDataSource();
                //DataSetReportsTableAdapters.SysCenters_SELECTTableAdapter taReportCenter = new DataSetReportsTableAdapters.SysCenters_SELECTTableAdapter();
                //DataSetReports.SysCenters_SELECTDataTable dtReportCenter = new DataSetReports.SysCenters_SELECTDataTable();

                //rdsCenter.Name = "dsCenterInfo";
                //taReportCenter.Fill(dtReportCenter, centerId);
                //rdsCenter.Value = dtReportCenter;
                //rptReports.LocalReport.DataSources.Add(rdsCenter);

                //ReportDataSource rdsFull = new ReportDataSource();
                //DataSetReportsTableAdapters.ResResidents_ReportEvaluationFullTableAdapter taFull = new DataSetReportsTableAdapters.ResResidents_ReportEvaluationFullTableAdapter();
                //DataSetReports.ResResidents_ReportEvaluationFullDataTable dtFull = new DataSetReports.ResResidents_ReportEvaluationFullDataTable();

                //rdsFull.Name = "dsEvaluationQuarterFull";
                //taFull.Fill(dtFull, centerId, residenceId, gradeId, teachingYearId, from, to);
                //rdsFull.Value = dtFull;
                //rptReports.LocalReport.DataSources.Add(rdsFull);
                //rptReports.LocalReport.ReportPath = Server.MapPath("~/Modules/Reports/Reports/EvaluationFull.rdlc");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }
    }
}