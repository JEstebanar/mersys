﻿<%@ Page Title="Reporte de Ingresos" Language="C#" MasterPageFile="~/SiteGeneral.master"
    AutoEventWireup="true" CodeBehind="IncomeReport.aspx.cs" Inherits="Presentation.Modules.Reports.IncomeReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/SiteGeneral.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upanelReport" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:ValidationSummary ID="ReportValidationSummary" runat="server" CssClass="alert alert-danger"
                ValidationGroup="ReportValidationGroup" />
            <fieldset>
                <legend>Reporte de Ingresos</legend>
                <div class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label">
                            Tipo de Reporte</label>
                        <div class="controls">
                            <asp:DropDownList ID="ddlReportType" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            Desde</label>
                        <div class="controls">
                            <div class="input-append">
                                <asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox>
                                <asp:MaskedEditExtender ID="MaskedEditExtenderStartDate" runat="server" TargetControlID="txtStartDate"
                                    MaskType="Date" Mask="99/99/9999">
                                </asp:MaskedEditExtender>
                                <asp:CalendarExtender ID="CalendarExtenderStartDate" runat="server" TargetControlID="txtStartDate"
                                    PopupButtonID="calendarStartDate">
                                </asp:CalendarExtender>
                                <span class="add-on"><i id="calendarStartDate" class="icon-calendar"></i></span>
                            </div>
                            <asp:RequiredFieldValidator ID="txtStartDateRequired" runat="server" ControlToValidate="txtStartDate"
                                CssClass="failureNotification" ErrorMessage="Fecha Desde es requerida." ValidationGroup="ReportValidationGroup">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            Hasta</label>
                        <div class="controls">
                            <div class="input-append">
                                <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
                                <asp:MaskedEditExtender ID="MaskedEditExtenderEndDate" runat="server" TargetControlID="txtEndDate"
                                    MaskType="Date" Mask="99/99/9999">
                                </asp:MaskedEditExtender>
                                <asp:CalendarExtender ID="CalendarExtenderEndDate" runat="server" TargetControlID="txtEndDate"
                                    PopupButtonID="calendarEndDate">
                                </asp:CalendarExtender>
                                <span class="add-on"><i id="calendarEndDate" class="icon-calendar"></i></span>
                            </div>
                            <asp:RequiredFieldValidator ID="txtEndDateRequired" runat="server" ControlToValidate="txtEndDate"
                                CssClass="failureNotification" ErrorMessage="Fecha Hasta es requerida." ValidationGroup="ReportValidationGroup">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <asp:Button ID="btnSearch" runat="server" Text="Generar" class="btn btn-info" ValidationGroup="ReportValidationGroup"
                                OnClick="btnSearch_Click" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <rsweb:ReportViewer ID="rptReports" runat="server" Width="100%" Height="800px">
                    </rsweb:ReportViewer>
                </div>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
