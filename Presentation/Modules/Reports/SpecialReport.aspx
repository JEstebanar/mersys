﻿<%@ Page Title="Reportes Especiales" Language="C#" MasterPageFile="~/SiteGeneral.master" AutoEventWireup="true"
    CodeBehind="SpecialReport.aspx.cs" Inherits="Presentation.Modules.Reports.SpecialReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ValidationSummary ID="ReportValidationSummary" runat="server" CssClass="alert alert-danger"
        ValidationGroup="ReportValidationGroup" />
    <fieldset>
        <legend>Reportes Especiales</legend>
        <div class="form-horizontal">
            <div class="control-group">
                <label class="control-label">
                    Año Docente</label>
                <div class="controls">
                    <asp:DropDownList ID="ddlTeachingYears" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">
                    Estado</label>
                <div class="controls">
                    <asp:DropDownList ID="ddlResidentsStatus" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="form-search">
                <asp:DropDownList ID="ddlReportType" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="ddlReportTypeRequired" runat="server" ControlToValidate="ddlReportType"
                    CssClass="failureNotification" ErrorMessage="Tipo de Reporte es Requerido." ValidationGroup="ReportValidationGroup"
                    InitialValue="0">*</asp:RequiredFieldValidator>
                <asp:DropDownList ID="ddlResidences" runat="server">
                </asp:DropDownList>
                <asp:DropDownList ID="ddlGrades" runat="server">
                </asp:DropDownList>
                <asp:Button ID="Button1" runat="server" Text="Generar" class="btn btn-info" OnClick="btnSearch_Click"
                    ValidationGroup="ReportValidationGroup" />
            </div>
        </div>
        <div class="control-group">
            <rsweb:ReportViewer ID="rptReport" runat="server" Width="100%" Height="800px">
            </rsweb:ReportViewer>
        </div>
    </fieldset>
</asp:Content>
