﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using SystemSecurity.UserAccess;
using SystemSecurity.SysError;
using System.IO;
using System.Reflection;
using SystemSecurity.SecurityAccess;
using SystemSecurity.Screens;
using General.GeneralCommons;

namespace Presentation.Modules.Reports
{
    public partial class Reports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.SpecialReport))
                    {
                        if (!IsPostBack)
                        {
                            int centerId = (int)((SysUserAccess)Session["access"]).CenterId;
                            int userId = (int)((SysUserAccess)Session["access"]).UserId;

                            rptReports.LocalReport.DataSources.Clear();
                            ReportDataSource rdsCenter = new ReportDataSource();
                            DataSetReportsTableAdapters.SysCenters_SELECTTableAdapter taReportCenter = new DataSetReportsTableAdapters.SysCenters_SELECTTableAdapter();
                            DataSetReports.SysCenters_SELECTDataTable dtReportCenter = new DataSetReports.SysCenters_SELECTDataTable();

                            rdsCenter.Name = "dsCenterInfo";
                            taReportCenter.Fill(dtReportCenter, centerId);
                            rdsCenter.Value = dtReportCenter;
                            rptReports.LocalReport.DataSources.Add(rdsCenter);

                            ReportDataSource rdsAssignedInfo = new ReportDataSource();
                            DataSetReportsTableAdapters.ResResidents_ReportAssignedToCoordinatorsTableAdapter taAssignedInfo = new DataSetReportsTableAdapters.ResResidents_ReportAssignedToCoordinatorsTableAdapter();
                            DataSetReports.ResResidents_ReportAssignedToCoordinatorsDataTable dtAssignedInfo = new DataSetReports.ResResidents_ReportAssignedToCoordinatorsDataTable();

                            rdsAssignedInfo.Name = "dsResidentAssignedToCoordinators";
                            taAssignedInfo.Fill(dtAssignedInfo, centerId, userId);
                            rdsAssignedInfo.Value = dtAssignedInfo;
                            rptReports.LocalReport.DataSources.Add(rdsAssignedInfo);
                            rptReports.LocalReport.ReportPath = Server.MapPath("~/Modules/Reports/Reports/ResidentsAssignedToCoordinators.rdlc");
                        }
                        else
                            Response.Redirect("/Modules/AccessDenied.aspx");
                    }
                    else
                        Response.Redirect("/Account/LogIn.aspx");
                }
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }
    }
}