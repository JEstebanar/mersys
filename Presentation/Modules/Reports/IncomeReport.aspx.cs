﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI.WebControls;
using General.GeneralCommons;
using Microsoft.Reporting.WebForms;
using SystemSecurity.Reports;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Reports
{
    public partial class IncomeReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.IncomeReport))
                    {
                        if (!IsPostBack)
                        {
                            Initialize();
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        #region Methods
        private void Initialize()
        {
            ddlReportType.DataTextField = "reportName";
            ddlReportType.DataValueField = "reportId";
            ddlReportType.DataSource = SysReports.GetReportsList((int)((SysUserAccess)Session["access"]).CenterId, (int)GeneralCommon.ReportTypes.IncomeReports);
            ddlReportType.DataBind();
            ddlReportType.Items.Insert(0, new ListItem("-- Seleccione Reporte --", "0"));
        }
        #endregion

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtStartDate.Text == string.Empty || txtEndDate.Text == string.Empty)
            {
                Master.Message("Complete los campos de fecha correctamente.", "", 2);
                return;
            }

            int centerId = (int)((SysUserAccess)Session["access"]).CenterId;
            DateTime startDate = Convert.ToDateTime(txtStartDate.Text);
            DateTime endDate = Convert.ToDateTime(txtEndDate.Text);

            if (startDate > endDate)
            {
                Master.Message("Fecha Desde debe ser menor que fecha Hasta.", "", 2);
                return;
            }

            rptReports.LocalReport.DataSources.Clear();
            ReportDataSource rdsCenter = new ReportDataSource();
            DataSetReportsTableAdapters.SysCenters_SELECTTableAdapter taReportCenter = new DataSetReportsTableAdapters.SysCenters_SELECTTableAdapter();
            DataSetReports.SysCenters_SELECTDataTable dtReportCenter = new DataSetReports.SysCenters_SELECTDataTable();

            rdsCenter.Name = "dsCenterInfo";
            taReportCenter.Fill(dtReportCenter, centerId);
            rdsCenter.Value = dtReportCenter;
            rptReports.LocalReport.DataSources.Add(rdsCenter);

            switch (Convert.ToInt32(ddlReportType.SelectedValue))
            {
                case (int)GeneralCommon.ReportList.Incomes:
                    ReportDataSource rdsIncome = new ReportDataSource();
                    DataSetReportsTableAdapters.BilInvoices_ReportIncomesTableAdapter taIncome = new DataSetReportsTableAdapters.BilInvoices_ReportIncomesTableAdapter();
                    DataSetReports.BilInvoices_ReportIncomesDataTable dtIncome = new DataSetReports.BilInvoices_ReportIncomesDataTable();

                    rdsIncome.Name = "dsIncomes";
                    taIncome.Fill(dtIncome, centerId, startDate, endDate);
                    rdsIncome.Value = dtIncome;
                    rptReports.LocalReport.DataSources.Add(rdsIncome);
                    rptReports.LocalReport.ReportPath = Server.MapPath("/Modules/Reports/Reports/Incomes.rdlc");
                    rptReports.LocalReport.Refresh();
                    break;
            }            
        }
    }
}