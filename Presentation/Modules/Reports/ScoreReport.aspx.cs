﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI.WebControls;
using General.GeneralCommons;
using Microsoft.Reporting.WebForms;
using Residences.Grades;
using Residences.Residences;
using Residences.TeachingYears;
using SystemSecurity.Reports;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Reports
{
    public partial class ScoreReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.ScoreReport))
                    {
                        if (!IsPostBack)
                        {
                            Initialize();
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        #region Methods
        private void Initialize()
        {
            ddlReportType.DataTextField = "reportName";
            ddlReportType.DataValueField = "reportId";
            ddlReportType.DataSource = SysReports.GetReportsList((int)((SysUserAccess)Session["access"]).CenterId, (int)GeneralCommon.ReportTypes.ScoreReports);
            ddlReportType.DataBind();
            ddlReportType.Items.Insert(0, new ListItem("-- Seleccione Reporte --", "0"));

            ddlGrades.DataTextField = "gradeName";
            ddlGrades.DataValueField = "gradeId";
            ddlGrades.DataSource = ResGrades.GetGrades();
            ddlGrades.DataBind();
            ddlGrades.Items.Insert(0, new ListItem("-- Todos los Grados --", "0"));

            ddlTeachingYears.Items.Clear();
            ddlTeachingYears.DataTextField = "teachingYear";
            ddlTeachingYears.DataValueField = "teachingYearId";
            ddlTeachingYears.DataSource = ResTeachingYears.GetTeachingYears((int)((SysUserAccess)Session["access"]).CenterId);
            ddlTeachingYears.DataBind();
        }
        #endregion

        protected void ddlTeachingYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlResidences.Items.Clear();
            ddlResidences.DataTextField = "residenceName";
            ddlResidences.DataValueField = "residenceId";
            ddlResidences.DataSource = ResResidences.GetResidences((int)((SysUserAccess)Session["access"]).CenterId, Convert.ToInt32(ddlTeachingYears.SelectedValue));
            ddlResidences.DataBind();
            ddlResidences.Items.Insert(0, new ListItem("-- Todas las Residencias --", "0"));
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                int centerId = (int)((SysUserAccess)Session["access"]).CenterId;
                int residenceId = Convert.ToInt32(ddlResidences.SelectedValue);
                int gradeId = Convert.ToInt32(ddlGrades.SelectedValue);
                int teachingYearId = Convert.ToInt32(ddlTeachingYears.SelectedValue);
                string enrollment = txtEnrollment.Text;
                if (txtEnrollment.Text == string.Empty)
                    enrollment = null;

                rptReports.LocalReport.DataSources.Clear();
                ReportDataSource rdsCenter = new ReportDataSource();
                DataSetReportsTableAdapters.SysCenters_SELECTTableAdapter taReportCenter = new DataSetReportsTableAdapters.SysCenters_SELECTTableAdapter();
                DataSetReports.SysCenters_SELECTDataTable dtReportCenter = new DataSetReports.SysCenters_SELECTDataTable();

                rdsCenter.Name = "dsCenterInfo";
                taReportCenter.Fill(dtReportCenter, centerId);
                rdsCenter.Value = dtReportCenter;
                rptReports.LocalReport.DataSources.Add(rdsCenter);

                switch (Convert.ToInt32(ddlReportType.SelectedValue))
                {
                    case (int)GeneralCommon.ReportList.JulyOctober:
                        ReportDataSource rdsQ1 = new ReportDataSource();
                        DataSetReportsTableAdapters.ResResidents_ReportEvaluationQ1TableAdapter taQ1 = new DataSetReportsTableAdapters.ResResidents_ReportEvaluationQ1TableAdapter();
                        DataSetReports.ResResidents_ReportEvaluationQ1DataTable dtQ1 = new DataSetReports.ResResidents_ReportEvaluationQ1DataTable();

                        rdsQ1.Name = "dsEvaluationQuarter1";
                        taQ1.Fill(dtQ1, centerId, residenceId, gradeId, teachingYearId, enrollment);
                        rdsQ1.Value = dtQ1;
                        rptReports.LocalReport.DataSources.Add(rdsQ1);
                        rptReports.LocalReport.ReportPath = Server.MapPath("~/Modules/Reports/Reports/EvaluationQuarter1.rdlc");
                        break;

                    case (int)GeneralCommon.ReportList.NovemberFebruary:
                        ReportDataSource rdsQ2 = new ReportDataSource();
                        DataSetReportsTableAdapters.ResResidents_ReportEvaluationQ2TableAdapter taQ2 = new DataSetReportsTableAdapters.ResResidents_ReportEvaluationQ2TableAdapter();
                        DataSetReports.ResResidents_ReportEvaluationQ2DataTable dtQ2 = new DataSetReports.ResResidents_ReportEvaluationQ2DataTable();

                        rdsQ2.Name = "dsEvaluationQuarter2";
                        taQ2.Fill(dtQ2, centerId, residenceId, gradeId, teachingYearId, enrollment);
                        rdsQ2.Value = dtQ2;
                        rptReports.LocalReport.DataSources.Add(rdsQ2);
                        rptReports.LocalReport.ReportPath = Server.MapPath("~/Modules/Reports/Reports/EvaluationQuarter2.rdlc");
                        break;

                    case (int)GeneralCommon.ReportList.MarchJune:
                        ReportDataSource rdsQ3 = new ReportDataSource();
                        DataSetReportsTableAdapters.ResResidents_ReportEvaluationQ3TableAdapter taQ3 = new DataSetReportsTableAdapters.ResResidents_ReportEvaluationQ3TableAdapter();
                        DataSetReports.ResResidents_ReportEvaluationQ3DataTable dtQ3 = new DataSetReports.ResResidents_ReportEvaluationQ3DataTable();

                        rdsQ3.Name = "dsEvaluationQuarter3";
                        taQ3.Fill(dtQ3, centerId, residenceId, gradeId, teachingYearId, enrollment);
                        rdsQ3.Value = dtQ3;
                        rptReports.LocalReport.DataSources.Add(rdsQ3);
                        rptReports.LocalReport.ReportPath = Server.MapPath("~/Modules/Reports/Reports/EvaluationQuarter3.rdlc");
                        break;

                    case (int)GeneralCommon.ReportList.AllPeriods:
                        ReportDataSource rdsFull = new ReportDataSource();
                        DataSetReportsTableAdapters.ResResidents_ReportEvaluationFullTableAdapter taFull = new DataSetReportsTableAdapters.ResResidents_ReportEvaluationFullTableAdapter();
                        DataSetReports.ResResidents_ReportEvaluationFullDataTable dtFull = new DataSetReports.ResResidents_ReportEvaluationFullDataTable();

                        rdsFull.Name = "dsEvaluationQuarterFull";
                        taFull.Fill(dtFull, centerId, residenceId, gradeId, teachingYearId, enrollment);
                        rdsFull.Value = dtFull;
                        rptReports.LocalReport.DataSources.Add(rdsFull);
                        rptReports.LocalReport.ReportPath = Server.MapPath("~/Modules/Reports/Reports/EvaluationFull.rdlc");
                        break;
                    case (int)GeneralCommon.ReportList.JulyDicember:
                        ReportDataSource rdsS1 = new ReportDataSource();
                        DataSetReportsTableAdapters.ResResidents_ReportEvaluationS1TableAdapter taS1 = new DataSetReportsTableAdapters.ResResidents_ReportEvaluationS1TableAdapter();
                        DataSetReports.ResResidents_ReportEvaluationS1DataTable dtS1 = new DataSetReports.ResResidents_ReportEvaluationS1DataTable();

                        rdsS1.Name = "dsEvaluationSemester1";
                        taS1.Fill(dtS1, centerId, residenceId, gradeId, teachingYearId, enrollment);
                        rdsS1.Value = dtS1;
                        rptReports.LocalReport.DataSources.Add(rdsS1);
                        rptReports.LocalReport.ReportPath = Server.MapPath("~/Modules/Reports/Reports/EvaluationSemester1.rdlc");
                        break;
                    case (int)GeneralCommon.ReportList.JanuaryJune:
                        ReportDataSource rdsS2 = new ReportDataSource();
                        DataSetReportsTableAdapters.ResResidents_ReportEvaluationS2TableAdapter taS2 = new DataSetReportsTableAdapters.ResResidents_ReportEvaluationS2TableAdapter();
                        DataSetReports.ResResidents_ReportEvaluationS2DataTable dtS2 = new DataSetReports.ResResidents_ReportEvaluationS2DataTable();

                        rdsS2.Name = "dsEvaluationSemester2";
                        taS2.Fill(dtS2, centerId, residenceId, gradeId, teachingYearId, enrollment);
                        rdsS2.Value = dtS2;
                        rptReports.LocalReport.DataSources.Add(rdsS2);
                        rptReports.LocalReport.ReportPath = Server.MapPath("~/Modules/Reports/Reports/EvaluationSemester2.rdlc");
                        break;
                }
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }
    }
}