﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI.WebControls;
using General.GeneralCommons;
using Microsoft.Reporting.WebForms;
using Residences.Grades;
using Residences.Residences;
using Residences.ResidentStatus;
using Residences.TeachingYears;
using SystemSecurity.Reports;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Reports
{
    public partial class SpecialReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.SpecialReport))
                    {
                        if (!IsPostBack)
                        {
                            Initialize();
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        #region Methods
        private void Initialize()
        {
            ddlReportType.DataTextField = "reportName";
            ddlReportType.DataValueField = "reportId";
            ddlReportType.DataSource = SysReports.GetReportsList((int)((SysUserAccess)Session["access"]).CenterId, (int)GeneralCommon.ReportTypes.SpecialReports);
            ddlReportType.DataBind();
            ddlReportType.Items.Insert(0, new ListItem("-- Seleccione Reporte --", "0"));

            ddlResidences.Items.Clear();
            ddlResidences.DataTextField = "residenceName";
            ddlResidences.DataValueField = "residenceId";
            ddlResidences.DataSource = ResResidences.GetResidences((int)((SysUserAccess)Session["access"]).CenterId, (int)((SysUserAccess)Session["access"]).TeachingYearId);
            ddlResidences.DataBind();
            ddlResidences.Items.Insert(0, new ListItem("-- Todas las Residencias --", "0"));

            ddlGrades.DataTextField = "gradeName";
            ddlGrades.DataValueField = "gradeId";
            ddlGrades.DataSource = ResGrades.GetGrades();
            ddlGrades.DataBind();
            ddlGrades.Items.Insert(0, new ListItem("-- Todos los Grados --", "0"));

            ddlTeachingYears.Items.Clear();
            ddlTeachingYears.DataTextField = "teachingYear";
            ddlTeachingYears.DataValueField = "teachingYearId";
            ddlTeachingYears.DataSource = ResTeachingYears.GetTeachingYears((int)((SysUserAccess)Session["access"]).CenterId);
            ddlTeachingYears.DataBind();

            ddlResidentsStatus.Items.Clear();
            ddlResidentsStatus.DataTextField = "statusName";
            ddlResidentsStatus.DataValueField = "statusId";
            ddlResidentsStatus.DataSource = ResResidentStatus.GetResidentsStatus();
            ddlResidentsStatus.DataBind();
        }
        #endregion

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int centerId = (int)((SysUserAccess)Session["access"]).CenterId;
            int residenceId = Convert.ToInt32(ddlResidences.SelectedValue);
            int gradeId = Convert.ToInt32(ddlGrades.SelectedValue);
            int teachingYearId = Convert.ToInt32(ddlTeachingYears.SelectedValue);
            int statusId = Convert.ToInt32(ddlResidentsStatus.SelectedValue);

            rptReport.LocalReport.DataSources.Clear();
            ReportDataSource rdsCenter = new ReportDataSource();
            DataSetReportsTableAdapters.SysCenters_SELECTTableAdapter taReportCenter = new DataSetReportsTableAdapters.SysCenters_SELECTTableAdapter();
            DataSetReports.SysCenters_SELECTDataTable dtReportCenter = new DataSetReports.SysCenters_SELECTDataTable();

            rdsCenter.Name = "dsCenterInfo";
            taReportCenter.Fill(dtReportCenter, centerId);
            rdsCenter.Value = dtReportCenter;
            rptReport.LocalReport.DataSources.Add(rdsCenter);

            switch (Convert.ToInt32(ddlReportType.SelectedValue))
            {
                case (int)GeneralCommon.ReportList.ResidentDocumentsInfo:
                    ReportDataSource rdsDocInfo = new ReportDataSource();
                    DataSetReportsTableAdapters.ResResidents_ReportDocumentsInfoTableAdapter taDocInfo = new DataSetReportsTableAdapters.ResResidents_ReportDocumentsInfoTableAdapter();
                    DataSetReports.ResResidents_ReportDocumentsInfoDataTable dtDocInfo = new DataSetReports.ResResidents_ReportDocumentsInfoDataTable();

                    rdsDocInfo.Name = "dsResidentDocumentsInfo";
                    taDocInfo.Fill(dtDocInfo, centerId, residenceId, gradeId, teachingYearId);
                    rdsDocInfo.Value = dtDocInfo;
                    rptReport.LocalReport.DataSources.Add(rdsDocInfo);
                    rptReport.LocalReport.ReportPath = Server.MapPath("~/Modules/Reports/Reports/ResidentDocumentsInfo.rdlc");
                    break;
                case (int)GeneralCommon.ReportList.ResidentStatus:
                    ReportDataSource rdsStatus = new ReportDataSource();
                    DataSetReportsTableAdapters.ResResidents_ReportStatusTableAdapter taStatus = new DataSetReportsTableAdapters.ResResidents_ReportStatusTableAdapter();
                    DataSetReports.ResResidents_ReportStatusDataTable dtStatus = new DataSetReports.ResResidents_ReportStatusDataTable();

                    rdsStatus.Name = "dsResidentsStatus";
                    taStatus.Fill(dtStatus, centerId, residenceId, gradeId, teachingYearId, statusId);
                    rdsStatus.Value = dtStatus;
                    rptReport.LocalReport.DataSources.Add(rdsStatus);
                    rptReport.LocalReport.ReportPath = Server.MapPath("~/Modules/Reports/Reports/ResidentsStatus.rdlc");
                    break;
                case (int)GeneralCommon.ReportList.ResidentsToGraduate:
                    ReportDataSource rdsToGraduate = new ReportDataSource();
                    DataSetReportsTableAdapters.ResResidents_ReportToGraduateTableAdapter taToGraduate = new DataSetReportsTableAdapters.ResResidents_ReportToGraduateTableAdapter();
                    DataSetReports.ResResidents_ReportToGraduateDataTable dtToGraduate = new DataSetReports.ResResidents_ReportToGraduateDataTable();

                    rdsToGraduate.Name = "dsResidentsToGraduate";
                    taToGraduate.Fill(dtToGraduate, centerId, residenceId, gradeId, teachingYearId);
                    rdsToGraduate.Value = dtToGraduate;
                    rptReport.LocalReport.DataSources.Add(rdsToGraduate);
                    rptReport.LocalReport.ReportPath = Server.MapPath("~/Modules/Reports/Reports/ResidentsToGraduate.rdlc");
                    break;
                case (int)GeneralCommon.ReportList.ResidentsPhones:
                    ReportDataSource rdsPhones = new ReportDataSource();
                    DataSetReportsTableAdapters.ResResidents_ReportPhonesTableAdapter taPhones = new DataSetReportsTableAdapters.ResResidents_ReportPhonesTableAdapter();
                    DataSetReports.ResResidents_ReportPhonesDataTable dtPhones = new DataSetReports.ResResidents_ReportPhonesDataTable();

                    rdsPhones.Name = "dsResidentsPhones";
                    taPhones.Fill(dtPhones, centerId, residenceId, gradeId, teachingYearId);
                    rdsPhones.Value = dtPhones;
                    rptReport.LocalReport.DataSources.Add(rdsPhones);
                    rptReport.LocalReport.ReportPath = Server.MapPath("~/Modules/Reports/Reports/ResidentsPhones.rdlc");
                    break;
            }
        }
    }
}