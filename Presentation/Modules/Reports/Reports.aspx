﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteGeneral.master" AutoEventWireup="true"
    CodeBehind="Reports.aspx.cs" Inherits="Presentation.Modules.Reports.Reports" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <rsweb:ReportViewer ID="rptReports" runat="server" Width="100%" Height="800px">
    </rsweb:ReportViewer>
</asp:Content>
