﻿<%@ Page Title="Acceso Denegado" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="AccessDenied.aspx.cs" Inherits="Presentation.Modules.AccessDenied" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page relative">
        <div class="page_layout page_margin_top clearfix">
            <div class="page_header clearfix">
                <div class="page_header_left">
                    <h1 class="page_title">
                        Acceso Denegado</h1>
                    <ul class="bread_crumb">
                        <li><a href="/Main.aspx" title="Docencia Médica">Docencia Médica</a></li>
                    </ul>
                </div>
            </div>
            <div class="clearfix">
                <h1 class="not_found page_margin_top_section">
                    !!!</h1>
                <h1 class="page_margin_top_section">
                    !!!</h1>
                <p style="font-size: 14px; padding: 0;" class="page_margin_top">
                    Al parecer lo que busca no está disponible en estos momento. Vuelva a intentarlo luego.
                    <br />
                    Es posible que no tengas los permisos necesarios para ver lo que busca.
                </p>
                <a title="Docencia Médica" href="/Main.aspx" class="more blue medium page_margin_top_section">
                    Volver atrás</a>
            </div>
        </div>
    </div>
</asp:Content>
