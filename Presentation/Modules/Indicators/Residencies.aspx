﻿<%@ Page Title="Residencias" Language="C#" MasterPageFile="~/Modules/Indicators/Indicators.master"
    AutoEventWireup="true" CodeBehind="Residencies.aspx.cs" Inherits="Presentation.Modules.Indicators.Residencies" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="divBetter" style="height:400px;">

    </div>
    <hr />
    <div class="row-fluid">
        <div class="span9">
            <fieldset>
                <legend>Residencias</legend>
                <div class="controls">
                    <asp:GridView ID="grvResidences" runat="server" class="table table-condensed table-striped table-bordered"
                        AutoGenerateColumns="False" DataKeyNames="residenceId" ShowHeader="false" 
                        onselectedindexchanged="grvResidences_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="position" HeaderText="#" SortExpression="position" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="residenceSelect" runat="server" CommandArgument='<%# Eval("residenceId") %>'
                                        CommandName="Select" ToolTip="Seleccionar" Text='<%# Eval("residenceName") %>'>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="average" HeaderText="Promedio" SortExpression="average" />
                            <asp:BoundField DataField="progress" HeaderText="Progreso" SortExpression="progress" />
                            <asp:BoundField DataField="progressPercent" HeaderText="%" SortExpression="progressPercent" />
                        </Columns>
                    </asp:GridView>
                </div>
            </fieldset>
        </div>
        <div class="span3">
            <div class="well">
                <%--<h3>Cantidad de Residencias</h3>--%>
                <div class="media">
                    <a class="pull-left">
                        <%--<img class="media-object" data-src="holder.js/64x64">--%>
                        <img class="media-object" src="../../bootstrap/img/64x64.png" />
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">
                            Residencias
                        </h4>
                        <!-- Nested media object -->
                        <h3>
                            <strong><label id="lblResidencies" runat="server"></label> </strong>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="well">
                <%--<h3>Cantidad de Residencias</h3>--%>
                <div class="media">
                    <a class="pull-left">
                        <%--<img class="media-object" data-src="holder.js/64x64">--%>
                        <img class="media-object" src="../../bootstrap/img/64x64.png" />
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">
                            Residentes
                        </h4>
                        <!-- Nested media object -->
                        <h3>
                            <strong><label id="lblResidents" runat="server"></label> </strong>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="well">
                <%--<h3>Cantidad de Residencias</h3>--%>
                <div class="media">
                    <a class="pull-left">
                        <%--<img class="media-object" data-src="holder.js/64x64">--%>
                        <img class="media-object" src="../../bootstrap/img/64x64.png" />
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">
                            Mejor Prom.
                        </h4>
                        <!-- Nested media object -->
                        <h3>
                            <strong><label id="lblMore" runat="server"></label> </strong>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="well">
                <%--<h3>Cantidad de Residencias</h3>--%>
                <div class="media">
                    <a class="pull-left">
                        <%--<img class="media-object" data-src="holder.js/64x64">--%>
                        <img class="media-object" src="../../bootstrap/img/64x64.png" />
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">
                            Peor Prom.
                        </h4>
                        <!-- Nested media object -->
                        <h3>
                            <strong><label id="lblFewer" runat="server"></label> </strong>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script language="javascript" type="text/javascript">
        function getChart() {
            google.load("visualization", "1", { packages: ["corechart"] });
            google.setOnLoadCallback(drawChart);
            function drawChart() {
                //Residencies
                var dataValues = JSON.parse('<%=GetResidenciesAverages() %>');

                var data = new google.visualization.DataTable();
                data.addColumn('number', 'Residencia');
                data.addColumn('number', 'Promedio');

                for (var i = 0; i < dataValues.length; i++) {
                    data.addRow([dataValues[i].position, dataValues[i].average]);
                }

                var options = {
                    title: 'Promedio de Notas por Residencias'
                };

                var chart = new google.visualization.ColumnChart(document.getElementById('divBetter'));
                chart.draw(data, options);

//                //Residencies %
//                var dataValues = JSON.parse('<%=GetResidenciesAverages() %>');

//                var data = new google.visualization.DataTable();
//                data.addColumn('string', 'Residencia');
//                data.addColumn('number', 'Promedio');

//                for (var i = 0; i < dataValues.length; i++) {
//                    data.addRow([dataValues[i].residenceName, dataValues[i].average]);
//                }

//                var options = {
//                    title: 'Porciento que representan las Residencias'
//                };

//                var chart = new google.visualization.PieChart(document.getElementById('divPie'));
//                chart.draw(data, options);
            }
        }
        </script>
</asp:Content>
