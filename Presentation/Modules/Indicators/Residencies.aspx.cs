﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI;
using General.GeneralCommons;
using Modules.Indicators.Indicators;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Indicators
{
    public partial class Residencies : System.Web.UI.Page
    {
        private IList<IndIndicators.Residences> CurrentResidences { get { return (IList<IndIndicators.Residences>)ViewState["currentResidences"]; } set { ViewState["currentResidences"] = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.IndicatorResidencies))
                    {
                        if (!IsPostBack)
                        {
                            ScriptManager.RegisterStartupScript(Page, GetType(), "JsStatus", "getChart();", true);

                            CurrentResidences = IndIndicators.GetResidenciesAverages();
                            grvResidences.DataSource = CurrentResidences;
                            grvResidences.DataBind();

                            decimal more = -100, fewer = 100;
                            int residencies = 0;

                            foreach (IndIndicators.Residences residences in CurrentResidences)
                            {
                                if (residences.average > more)
                                    more = residences.average;

                                if (residences.average < fewer)
                                    fewer = residences.average;

                                residencies++;
                            }

                            lblResidencies.InnerText = residencies.ToString();
                            lblResidents.InnerText = IndIndicators.NumberOfResidents((int)((SysUserAccess)Session["access"]).CenterId, 0).ToString();
                            lblMore.InnerText = more.ToString();
                            lblFewer.InnerText = fewer.ToString();
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        public string GetResidenciesAverages()
        {
            //if (ddlExecutives.SelectedIndex > 0)
            //{           

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            return serializer.Serialize(CurrentResidences);
            
            //}
            //else
            //    return "";
        }

        protected void grvResidences_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("/Modules/Indicators/Residencie.aspx?id=" + grvResidences.SelectedDataKey[0]);
        }


        //public string GetResidencieGradesAverages()
        //{
        //    //if (ddlExecutives.SelectedIndex > 0)
        //    //{
        //    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //    return serializer.Serialize(IndIndicators.GetResidencieGradesAverages());
        //    //}
        //    //else
        //    //    return "";
        //}
    }
}