﻿<%@ Page Title="Comunicaciones" Language="C#" MasterPageFile="~/Modules/Posts/Posts.master" AutoEventWireup="true"
    CodeBehind="Post.aspx.cs" Inherits="Presentation.Modules.Posts.Form.Post" validateRequest="false" %>

<%@ MasterType VirtualPath="~/Modules/Residences/Residences.master" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%-- <link href="../../../TextEditor/CSS/demo.css" rel="stylesheet" type="text/css" />
    <link href="../../../TextEditor/CSS/jquery-te-1.4.0.css" rel="stylesheet" type="text/css" />--%>
    <fieldset>
        <legend>Comunicaciones</legend>
        <asp:ValidationSummary ID="PostValidationSummary" runat="server" CssClass="alert alert-danger"
            ValidationGroup="PostValidationGroup" />
        <div class="form-inline">
            <asp:Button ID="btnNew" runat="server" Text="Nuevo" class="btn" 
                onclick="btnNew_Click" />
        </div>
        <br />
        <asp:GridView ID="grvPosts" runat="server" class="table table-condensed table-striped table-bordered"
            AutoGenerateColumns="False" DataKeyNames="PostId" 
            onselectedindexchanged="grvPosts_SelectedIndexChanged">
            <Columns>
                <asp:BoundField DataField="title" HeaderText="Título" SortExpression="title" />
                <asp:BoundField DataField="EndDate" HeaderText="Fecha" SortExpression="EndDate" />
                <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
            </Columns>
        </asp:GridView>
    </fieldset>
    <div id="divDetails" runat="server">
        <fieldset>
            <legend>Detalles</legend>
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">
                        Residencias</label>
                    <div class="controls">
                        <asp:DropDownList ID="ddlResidences" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Grados</label>
                    <div class="controls">
                        <asp:DropDownList ID="ddlGrades" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Título</label>
                    <div class="controls">
                        <asp:TextBox ID="txtTitle" runat="server" Width="98%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredtxtTitle" runat="server" ControlToValidate="txtTitle"
                            CssClass="failureNotification" ErrorMessage="Título es requerido." ValidationGroup="PostValidationGroup">*</asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
                    <FTB:FreeTextBox ID="FreeTextBox1" runat="server" Width="900px" 
                Height="500px" ButtonSet="Office2003">
                    </FTB:FreeTextBox>
               <br />
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">
                        Fecha de Fin</label>
                    <div class="controls">
                        <div class="input-append">
                            <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
                            <asp:MaskedEditExtender ID="MaskedEditExtenderEndDate" runat="server" TargetControlID="txtEndDate"
                                MaskType="Date" Mask="99/99/9999">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="CalendarExtenderEndDate" runat="server" TargetControlID="txtEndDate"
                                PopupButtonID="calendarEndDate">
                            </asp:CalendarExtender>
                            <span class="add-on"><i id="calendarEndDate" class="icon-calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" ValidationGroup="PostValidationGroup"
                    OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
            </div>
        </fieldset>
    </div>
    <script type="text/javascript">
        function validate() {
            var doc = document.getElementById('FreeTextBox1');
            if (doc.value.length == 0) {
                alert('Please Enter data in Richtextbox');
                return false;
            }
        }
    </script>
    <%--    <script src="../../../TextEditor/JS/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../../../TextEditor/JS/jquery-te-1.4.0.min.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
    //    $('.textEditor1').jqte();
    $(".textEditor").jqte({ blur: function () {
        document.getElementById('<%=hdText.ClientID %>').value = document.getElementById('<%=txtEditor.ClientID %>').value;
    }
    });
</script>--%>
</asp:Content>
