﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI.WebControls;
using General.EMails;
using General.GeneralCommons;
using Posts.Posts;
using Residences.Grades;
using Residences.Residences;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using SystemSecurity.SysError;
using SystemSecurity.UserAccess;
using Posts.Residences;
using Posts.Grades;

namespace Presentation.Modules.Posts.Form
{
    public partial class Post : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.CreateArticle))
                    {
                        if (!IsPostBack)
                        {
                            FillPosts();
                            Initializate();
                            divDetails.Visible = false;
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }

        private void FillPosts()
        {
            PosPosts sPosts = new PosPosts();
            sPosts.PostTypeId = (int)GeneralCommon.PostTypes.Comunications;
            sPosts.EndDate = DateTime.Now;
            sPosts.CenterId = ((SysUserAccess)Session["access"]).CenterId;
            grvPosts.DataSource = PosPosts.Search(sPosts);
            grvPosts.DataBind();
            divDetails.Visible = false;
        }

        private void Initializate()
        {
            ddlResidences.Items.Clear();
            ddlResidences.DataTextField = "residenceName";
            ddlResidences.DataValueField = "residenceId";
            ddlResidences.DataSource = ResResidences.GetResidences((int)((SysUserAccess)Session["access"]).CenterId, (int)((SysUserAccess)Session["access"]).TeachingYearId);
            ddlResidences.DataBind();
            ddlResidences.Items.Insert(0, new ListItem("-- Todas las Residencias --", "0"));

            ddlGrades.DataTextField = "gradeName";
            ddlGrades.DataValueField = "gradeId";
            ddlGrades.DataSource = ResGrades.GetGrades();
            ddlGrades.DataBind();
            ddlGrades.Items.Insert(0, new ListItem("-- Todos los Grados --", "0"));
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            grvPosts.SelectedIndex = -1;
            divDetails.Visible = true;
            ddlResidences.SelectedIndex = 0;
            ddlGrades.SelectedIndex = 0;
            txtTitle.Text = string.Empty;
            FreeTextBox1.Text = string.Empty;
            txtEndDate.Text = string.Empty;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //temp
            if (grvPosts.SelectedIndex > -1)
                return;

            PosPosts nPost;
            if (grvPosts.SelectedIndex > -1)
                nPost = PosPosts.Get(Convert.ToInt32(grvPosts.SelectedDataKey[0]));
            else
                nPost = new PosPosts();

            nPost.Title = txtTitle.Text;
            nPost.Body = FreeTextBox1.Text;
            nPost.PostTypeId = (int)GeneralCommon.PostTypes.Comunications;
            if (txtEndDate.Text != string.Empty)
                nPost.EndDate = Convert.ToDateTime(txtEndDate.Text);
            nPost.CenterId = (int)((SysUserAccess)Session["access"]).CenterId;
            nPost.CreatedBy = (int)((SysUserAccess)Session["access"]).UserCenterId;
            nPost.CreatedDate = DateTime.Now;
            int _postId = nPost.Insert();

            //those has to be change cause could be more than one residence
            if (grvPosts.SelectedIndex == -1)
            {
                if (Convert.ToInt32(ddlResidences.SelectedValue) > 0)
                {
                    PosPostResidences nPostResidence = new PosPostResidences();
                    nPostResidence.PostId = _postId;
                    nPostResidence.ResidenceId = Convert.ToInt32(ddlResidences.SelectedValue);
                    nPostResidence.Insert();
                }
            
                if (Convert.ToInt32(ddlGrades.SelectedValue) > 0)
                {
                    PosPostGrades nPostGrade = new PosPostGrades();
                    nPostGrade.PostId = _postId;
                    nPostGrade.GradeId = Convert.ToInt32(ddlGrades.SelectedValue);
                    nPostGrade.Insert();
                }
            }

            GenEMails.Comunication((int)((SysUserAccess)Session["access"]).CenterId, Convert.ToInt32(ddlResidences.SelectedValue), Convert.ToInt32(ddlGrades.SelectedValue), txtTitle.Text, FreeTextBox1.Text, (int)((SysUserAccess)Session["access"]).TeachingYearId);

            Master.Master.Message("¡Comunicación enviada con éxito!", "", 3);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divDetails.Visible = false;
        }

        protected void grvPosts_SelectedIndexChanged(object sender, EventArgs e)
        {
            divDetails.Visible = true;
            PosPosts cPost = PosPosts.Get(Convert.ToInt32(grvPosts.SelectedDataKey[0]));
            //ddlResidences.SelectedValue = cPost.ResidenceId.ToString();
            //ddlGrades.SelectedValue = cPost.GradeId.ToString();
            txtTitle.Text = cPost.Title;
            FreeTextBox1.Text = cPost.Body;
            txtEndDate.Text = cPost.EndDate.ToString();
        }
    }
}