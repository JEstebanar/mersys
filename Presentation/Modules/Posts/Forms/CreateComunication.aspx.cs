﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SystemSecurity.Screens;
using SystemSecurity.SecurityAccess;
using General.GeneralCommons;
using SystemSecurity.SysError;
using System.IO;
using System.Reflection;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Posts.Forms
{
    public partial class CreateComunication : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["access"] != null)
                {
                    if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.CreateComunication))
                    {
                        if (!IsPostBack)
                        {
                            wucCreatePost1.PostType = GeneralCommon.PostTypes.Comunications;
                        }
                    }
                    else
                        Response.Redirect("/Modules/AccessDenied.aspx");
                }
                else
                    Response.Redirect("/Account/LogIn.aspx");
            }
            catch (Exception ex)
            {
                SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
            }
        }
    }
}