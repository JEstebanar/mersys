﻿<%@ Page Title="Nuevo Artículo" Language="C#" MasterPageFile="~/Modules/Posts/Posts.master" AutoEventWireup="true"
    CodeBehind="CreateArticle.aspx.cs" Inherits="Presentation.Modules.Posts.Forms.CreateArticle" ValidateRequest="false"%>

<%@ MasterType VirtualPath="~/Modules/Posts/Posts.master" %>
<%@ Register Src="~/Modules/Posts/UserControls/wucCreatePost.ascx" TagName="wucCreatePost"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:wucCreatePost ID="wucCreatePost1" runat="server" />
</asp:Content>
