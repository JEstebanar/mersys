﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI.WebControls;
using Posts.Posts;

namespace Presentation.Modules.Posts
{
    public partial class Post : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString.Get("id") != null)
                {
                    int id = Convert.ToInt32(Request.QueryString.Get("id"));
                    DisplayBlog(id);
                }
            }
        }

        private void DisplayBlog(int id)
        {
            PosPosts cPost = PosPosts.Get(id);

            lblTitle.Text = cPost.Title;
            divContent.InnerHtml = cPost.Body;

            //BlogDAL objBlogDAL = new BlogDAL();
            //Blog objBlog = objBlogDAL.GetBlogById(id);
            //divTitle.InnerHtml = objBlog.Title;
            //spVisitor.InnerHtml = "Total Visits " + objBlog.Visit;
            //divContent.InnerHtml = objBlog.Content;
        }
    }
}