﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/Posts/Posts.master"
    AutoEventWireup="true" CodeBehind="xPost.aspx.cs" Inherits="Presentation.Modules.Posts.Post"
    ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row-fluid">
        <div class="span8">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
            <p id="divContent" runat="server">
            </p>
        </div>
        <div class="span4">
        </div>
    </div>
</asp:Content>
