﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="wucCreatePost.ascx.cs"
    Inherits="Presentation.Modules.Posts.UserControls.wucCreatePost" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!-- Le styles -->
<%--<link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../../../bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="../../../bootstrap/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="../../../medicenter/style/reset.css" />
<link rel="stylesheet" type="text/css" href="../../../medicenter/style/superfish.css" />
<link rel="stylesheet" type="text/css" href="../../../medicenter/style/style.css" />
<link rel="stylesheet" type="text/css" href="../../../medicenter/style/responsive.css" />
<link rel="shortcut icon" href="../../../medicenter/images/favicon.ico" />
<link rel="stylesheet" href="../../../Styles/Site.css" type="text/css" />--%>
<fieldset>
    <legend><asp:Label ID="lblTitle" runat="server"></asp:Label></legend>
    <asp:ValidationSummary ID="PostValidationSummary" runat="server" CssClass="alert alert-danger"
        ValidationGroup="PostValidationGroup" />
    <asp:UpdatePanel runat="server" ID="upMessage" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divMessage" runat="server" class="alert alert-block" visible="false">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="form-inline">
        <asp:Button ID="btnNew" runat="server" Text="Nuevo" class="btn" OnClick="btnNew_Click" />
    </div>
    <br />
    <asp:GridView ID="grvPosts" runat="server" class="table table-condensed table-striped table-bordered"
        AutoGenerateColumns="False" DataKeyNames="PostId" OnSelectedIndexChanged="grvPosts_SelectedIndexChanged"
        AllowPaging="True" OnPageIndexChanging="grvPosts_PageIndexChanging">
        <Columns>
            <asp:BoundField DataField="title" HeaderText="Título" SortExpression="title" />
            <asp:BoundField DataField="CreatedDate" HeaderText="Fecha" SortExpression="CreatedDate" />
            <%--<asp:BoundField DataField="EndDate" HeaderText="Fecha Fin" SortExpression="EndDate" />--%>
            <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
        </Columns>
    </asp:GridView>
</fieldset>
<div id="divDetails" runat="server">
    <fieldset>
        <legend>Detalles</legend>
        <div class="form-horizontal">
            <div id="divResidencies" runat="server" class="control-group">
                <label class="control-label">
                    Residencias</label>
                <div class="controls">
                    <asp:DropDownList ID="ddlResidences" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div id="divGrades" runat="server" class="control-group">
                <label class="control-label">
                    Grados</label>
                <div class="controls">
                    <asp:DropDownList ID="ddlGrades" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <%--           <div class="control-group">
                <label class="control-label">
                    Título</label>
                <div class="controls">
                    <asp:TextBox ID="txtTitle" runat="server" Width="98%"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredtxtTitle" runat="server" ControlToValidate="txtTitle"
                        CssClass="failureNotification" ErrorMessage="Título es requerido." ValidationGroup="PostValidationGroup">*</asp:RequiredFieldValidator>
                </div> 
            </div>--%>
        <asp:TextBox ID="txtTitle" runat="server" Width="98%" placeholder="Título"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredtxtTitle" runat="server" ControlToValidate="txtTitle"
            CssClass="failureNotification" ErrorMessage="Título es requerido." ValidationGroup="PostValidationGroup">*</asp:RequiredFieldValidator>
        <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
        <script type="text/javascript">
//<![CDATA[
            bkLib.onDomLoaded(function () {
                var myNicEditor =
            new nicEditor({ buttonList: ['bold', 'italic', 'underline'
                        , 'strikeThrough']
            });
                myNicEditor.setPanel('myNicPanel');
                myNicEditor.addInstance('<%=myInstance.ClientID%>');
            });
  //]]>
        </script>
        <div id="myNicPanel" style="width: 100%;">
        </div>
        <div style="width: 100%;">
            <textarea id="myInstance" runat="server" style="font-size: 16px; background-color: #ccc;
                min-height: 200px; padding: 3px; width: 100%;">
            </textarea>
        </div>
        <br />
        <asp:TextBox ID="txtTags" runat="server" Style="width: 98%;" placeholder="(Opcional) Etiquetas separadas por (,)"></asp:TextBox>
        <%--<asp:TextBox ID="txtContent" runat="server" Width="98%" Rows="10" TextMode="MultiLine"></asp:TextBox>--%>
        <%--<asp:RequiredFieldValidator ID="RequiredtxtContent" runat="server" ControlToValidate="myInstance"
                        CssClass="failureNotification" ErrorMessage="Contenido es requerido." ValidationGroup="PostValidationGroup">*</asp:RequiredFieldValidator>--%>
        <br />
        <div class="form-horizontal">
            <div class="control-group">
                <label class="control-label">
                    Foto de Portada</label>
                <div class="controls">
                    <asp:FileUpload ID="fuPostImage" runat="server" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">
                    Autor</label>
                <div class="controls">
                    <asp:TextBox ID="txtAuthor" runat="server" placeholder="Opcional"></asp:TextBox>
                </div>
            </div>
            <div id="divEndDate" runat="server" class="control-group">
                <label class="control-label">
                    Fecha</label>
                <div class="controls">
                    <div class="input-append">
                        <asp:TextBox ID="txtEndDate" runat="server" TextMode="DateTime"></asp:TextBox>
                        <asp:MaskedEditExtender ID="MaskedEditExtenderEndDate" runat="server" TargetControlID="txtEndDate"
                            MaskType="Date" Mask="99/99/9999" UserDateFormat="DayMonthYear">
                        </asp:MaskedEditExtender>
                        <asp:CalendarExtender ID="CalendarExtenderEndDate" runat="server" TargetControlID="txtEndDate"
                            PopupButtonID="calendarEndDate">
                        </asp:CalendarExtender>
                        <span class="add-on"><i id="calendarEndDate" class="icon-calendar"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <asp:Button ID="btnSave" runat="server" Text="Guardar" class="btn btn-primary" ValidationGroup="PostValidationGroup"
                OnClick="btnSave_Click" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancelar" class="btn" OnClick="btnCancel_Click" />
        </div>
    </fieldset>
</div>
