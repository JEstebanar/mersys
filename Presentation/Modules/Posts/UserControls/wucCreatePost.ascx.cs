﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using General.EMails;
using General.GeneralCommons;
using General.Utilities;
using Posts.Grades;
using Posts.Posts;
using Posts.Residences;
using Residences.Grades;
using Residences.Residences;
using SystemSecurity.UserAccess;

namespace Presentation.Modules.Posts.UserControls
{
    public partial class wucCreatePost : System.Web.UI.UserControl
    {
        public GeneralCommon.PostTypes PostType
        {
            get { return (GeneralCommon.PostTypes)ViewState["postType"]; }
            set { ViewState["postType"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillPostsByPostType();

                switch (PostType)
                {
                    case GeneralCommon.PostTypes.Comunications:
                        lblTitle.Text = "Comunicaciones";
                        divResidencies.Visible = true;
                        divGrades.Visible = true;
                        divEndDate.Visible = true;
                        InitializateComunications();
                        break;
                    case GeneralCommon.PostTypes.Articles:
                        lblTitle.Text = "Artículos";
                        divResidencies.Visible = false;
                        divGrades.Visible = false;
                        divEndDate.Visible = false;
                        InitializateComunications();
                        break;
                    default:
                        divResidencies.Visible = false;
                        divGrades.Visible = false;
                        divEndDate.Visible = false;
                        break;
                }

                divDetails.Visible = false;
            }
            Message("", "", 0);
        }

        private void FillPostsByPostType()
        {
            PosPosts sPosts = new PosPosts();
            sPosts.PostTypeId = (int)PostType;
            //sPosts.EndDate = DateTime.Now;
            sPosts.CenterId = ((SysUserAccess)Session["access"]).CenterId;

            grvPosts.DataSource = PosPosts.Search(sPosts);
            grvPosts.DataBind();

            divDetails.Visible = false;
        }

        private void InitializateComunications()
        {
            ddlResidences.Items.Clear();
            ddlResidences.DataTextField = "residenceName";
            ddlResidences.DataValueField = "residenceId";
            ddlResidences.DataSource = ResResidences.GetResidences((int)((SysUserAccess)Session["access"]).CenterId, (int)((SysUserAccess)Session["access"]).TeachingYearId);
            ddlResidences.DataBind();
            ddlResidences.Items.Insert(0, new ListItem("-- Todas las Residencias --", "0"));

            ddlGrades.DataTextField = "gradeName";
            ddlGrades.DataValueField = "gradeId";
            ddlGrades.DataSource = ResGrades.GetGrades();
            ddlGrades.DataBind();
            ddlGrades.Items.Insert(0, new ListItem("-- Todos los Grados --", "0"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">Message you want to display</param>
        /// <param name="header">Message header</param>
        /// <param name="type">1 - Warning Message, || 2 - Error Message, || 3 - Message of Success, || 4 - Message Information</param>
        public void Message(System.String message, System.String header, int? type)
        {
            HtmlGenericControl h4 = new HtmlGenericControl("h4");

            if (type == null || type > 4 || type <= 0)
            {
                divMessage.Controls.Remove(h4);

                divMessage.Attributes.Add("class", "");
                divMessage.Visible = false;
                return;
            }
            else
            {
                divMessage.Visible = true;

                switch (type)
                {
                    case 1:         //warning
                        divMessage.Attributes.Add("class", "alert alert-block");
                        h4.InnerText = "Advertencia";
                        break;
                    case 2:         //error
                        divMessage.Attributes.Add("class", "alert alert-error");
                        h4.InnerText = "Algo esta mal";
                        break;
                    case 3:         //success
                        divMessage.Attributes.Add("class", "alert alert-success");
                        h4.InnerText = "¡Bien Hecho!";
                        break;
                    case 4:         //info
                        divMessage.Attributes.Add("class", "alert alert-info");
                        h4.InnerText = "¡Algo esta mal!";
                        break;
                    default:
                        divMessage.Attributes.Add("class", "");
                        h4.InnerText = "";
                        break;
                }

                divMessage.Controls.Add(h4);

                divMessage.InnerText = message;
            }
            upMessage.Update();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            grvPosts.SelectedIndex = -1;
            divDetails.Visible = true;
            if (PostType == GeneralCommon.PostTypes.Comunications)
            {
                ddlResidences.SelectedIndex = 0;
                ddlGrades.SelectedIndex = 0;
            }
            txtTitle.Text = string.Empty;
            //FreeTextBox1.Text = string.Empty;
            myInstance.InnerText = string.Empty;
            txtAuthor.Text = string.Empty;
            txtTags.Text = string.Empty;
            txtEndDate.Text = string.Empty;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (PostType == GeneralCommon.PostTypes.Articles)
            {
                if (myInstance.Value == string.Empty || myInstance.Value.Length < 50)
                {
                    Message("El contenido del artículo es requerido.", "", 2);
                    return;
                }
            }

            if (PostType == GeneralCommon.PostTypes.Comunications)
            {
                if (txtEndDate.Text == string.Empty)
                {
                    Message("Fecha es requerida.", "", 2);
                    return;
                }
                if (txtEndDate.Text != string.Empty)
                {
                    if (DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) <= DateTime.Now)
                    {
                        Message("fecha debe de ser mayor al día de hoy.", "", 2);
                        return;
                    }
                }
            }

            ////temp
            //if (grvPosts.SelectedIndex > -1)
            //    return;

            PosPosts nPost;
            if (grvPosts.SelectedIndex > -1)
                nPost = PosPosts.Get(Convert.ToInt32(grvPosts.SelectedDataKey[0]));
            else
                nPost = new PosPosts();

            nPost.Title = txtTitle.Text;
            nPost.Body = myInstance.InnerText; // FreeTextBox1.Text;            
            if (fuPostImage.HasFile)
                nPost.PostImage = GenUtilities.SaveFileInPath(ref fuPostImage, GeneralCommon.TypeFilePaths.Posts, txtTitle.Text);
            nPost.Author = txtAuthor.Text;
            nPost.Tags = txtTags.Text;
            if (txtEndDate.Text != string.Empty)
                nPost.EndDate = Convert.ToDateTime(txtEndDate.Text);

            int _postId;
            if (grvPosts.SelectedIndex > -1)
            {
                _postId = Convert.ToInt32(grvPosts.SelectedDataKey[0]);
                nPost.Update();
            }
            else
            {
                nPost.PostTypeId = (int)PostType;
                nPost.Visits = 0;
                nPost.CenterId = (int)((SysUserAccess)Session["access"]).CenterId;
                nPost.CreatedBy = (int)((SysUserAccess)Session["access"]).UserCenterId;
                nPost.CreatedDate = DateTime.Now;
                _postId = nPost.Insert();
            }

            if (PostType == GeneralCommon.PostTypes.Comunications)
            {
                //has to be change cause could be more than one residence
                if (grvPosts.SelectedIndex == -1)
                {
                    if (Convert.ToInt32(ddlResidences.SelectedValue) > 0)
                    {
                        PosPostResidences nPostResidence = new PosPostResidences();
                        nPostResidence.PostId = _postId;
                        nPostResidence.ResidenceId = Convert.ToInt32(ddlResidences.SelectedValue);
                        nPostResidence.Insert();
                    }

                    if (Convert.ToInt32(ddlGrades.SelectedValue) > 0)
                    {
                        PosPostGrades nPostGrade = new PosPostGrades();
                        nPostGrade.PostId = _postId;
                        nPostGrade.GradeId = Convert.ToInt32(ddlGrades.SelectedValue);
                        nPostGrade.Insert();
                    }
                }
                GenEMails.Comunication((int)((SysUserAccess)Session["access"]).CenterId, Convert.ToInt32(ddlResidences.SelectedValue), Convert.ToInt32(ddlGrades.SelectedValue), txtTitle.Text, myInstance.InnerText, (int)((SysUserAccess)Session["access"]).TeachingYearId);
            }

            Message("¡Comunicación enviada con éxito!", "", 3);
            FillPostsByPostType();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divDetails.Visible = false;
        }

        protected void grvPosts_SelectedIndexChanged(object sender, EventArgs e)
        {
            divDetails.Visible = true;
            PosPosts cPost = PosPosts.Get(Convert.ToInt32(grvPosts.SelectedDataKey[0]));
            //if (PostType == GeneralCommon.PostTypes.Comunications)
            //{
            //    ddlResidences.SelectedValue = cPost.ResidenceId.ToString();
            //    ddlGrades.SelectedValue = cPost.GradeId.ToString();
            //}
            txtTitle.Text = cPost.Title;
            myInstance.InnerText = cPost.Body;
            txtTags.Text = cPost.Tags;
            txtAuthor.Text = cPost.Author;
            txtEndDate.Text = cPost.EndDate.ToString();
        }

        protected void grvPosts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvPosts.PageIndex = e.NewPageIndex;
            grvPosts.SelectedIndex = -1;
            FillPostsByPostType();
        }
    }
}