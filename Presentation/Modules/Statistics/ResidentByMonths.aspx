﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/Statistics/Statistics.master" AutoEventWireup="true" CodeBehind="ResidentByMonths.aspx.cs" Inherits="Presentation.Modules.Statistics.ResidentByMonths" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div class="page-header">
        <h1>
            Promedio de Notas por Residencia y Año</h1>
    </div>
    <div class="form-horizontal">
    </div>
        <div id="chartResident" style="height: 500px;">
    </div>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script language="javascript" type="text/javascript">
        function getBarChart() {
            google.load("visualization", "1", { packages: ["corechart"] });
            google.setOnLoadCallback(drawChart);
            function drawChart() {
                //Residencies
                var dataValues = JSON.parse('<%=GetResidentMonth() %>');

                var data = new google.visualization.DataTable();
                //data.addColumn('string', 'Residencia');
                data.addColumn('number', 'Julio');
                data.addColumn('number', 'Agosto');
                data.addColumn('number', 'Septiembre');
                data.addColumn('number', 'Octubre');

                for (var i = 0; i < dataValues.length; i++) {
                    data.addRow([dataValues[i].july, dataValues[i].august, dataValues[i].september, dataValues[i].october]);
                }

                var options = {
                    title: 'Promedio de Notas por Residencias'
                };

                var chart = new google.visualization.LineChart(document.getElementById('chartResident'));
                chart.draw(data, options);                
            }
        }
        </script>
</asp:Content>
