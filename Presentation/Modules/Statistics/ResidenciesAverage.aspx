﻿<%@ Page Title="Promedio de Notas por Residencia y Año" Language="C#" MasterPageFile="~/Modules/Statistics/Statistics.master"
    AutoEventWireup="true" CodeBehind="ResidenciesAverage.aspx.cs" Inherits="Presentation.Modules.Statistics.ResidenciesAverage" %>

<%@ MasterType VirtualPath="~/Modules/Statistics/Statistics.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div class="page-header">
        <h1>
            Promedio de Notas por Residencia y Año</h1>
    </div>
    <div class="form-horizontal">
    </div>
    <div id="chartResidencies">
    </div>
        <div id="chartResidencieGrades" style="height: 1800px;">
    </div>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script language="javascript" type="text/javascript">
        function getBarChart() {
            google.load("visualization", "1", { packages: ["corechart"] });
            google.setOnLoadCallback(drawChart);
            function drawChart() {
                //Residencies
                var dataValues = JSON.parse('<%=GetResidenciesAverages() %>');

                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Residencia');
                data.addColumn('number', 'Promedio', { role: 'Promedio' });

                for (var i = 0; i < dataValues.length; i++) {
                    data.addRow([dataValues[i].residenceName, dataValues[i].average]);
                }

                var options = {
                    title: 'Promedio de Notas por Residencias'
                };

                var chart = new google.visualization.ColumnChart(document.getElementById('chartResidencies'));
                chart.draw(data, options);

                //Grades
                var dataValues = JSON.parse('<%=GetResidencieGradesAverages() %>');

                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Residencia');
                data.addColumn('number', 'R1');
                data.addColumn('number', 'R2');
                data.addColumn('number', 'R3');
                data.addColumn('number', 'R4');
                data.addColumn('number', 'R5');
                data.addColumn('number', 'R6');
                data.addColumn('number', 'R7');

                for (var i = 0; i < dataValues.length; i++) {
                    data.addRow([dataValues[i].residenceName, dataValues[i].r1, dataValues[i].r2, dataValues[i].r3, dataValues[i].r4, dataValues[i].r5, dataValues[i].r6, dataValues[i].r7]);
                }

                var options = {
                    title: 'Promedios de Notas en Residencias por Año'
                };

                var chart = new google.visualization.BarChart(document.getElementById('chartResidencieGrades'));
                chart.draw(data, options);
            }
        }
        </script>
</asp:Content>
