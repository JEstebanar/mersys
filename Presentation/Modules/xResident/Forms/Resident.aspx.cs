﻿
namespace Presentation.Modules.Resident.Forms
{
    public partial class Resident : System.Web.UI.Page
    {
        //private IList<ResResidents.Especialties> AsignedEspecialties { get { return (IList<ResResidents.Especialties>)ViewState["asignedEspecialties"]; } set { ViewState["asignedEspecialties"] = value; } }
        ////private IList<ResResidents.Especialties> AsignedSubEspecialties { get { return (IList<ResResidents.Especialties>)ViewState["asignedSubEspecialties"]; } set { ViewState["asignedSubEspecialties"] = value; } }
        //private IList<ResRotations.Rotations> AsignedRotations { get { return (IList<ResRotations.Rotations>)ViewState["asignedRotations"]; } set { ViewState["asignedRotations"] = value; } }
        //private IList<ResRotations.Rotations> DeletedRotations { get { return (IList<ResRotations.Rotations>)ViewState["deletedRotations"]; } set { ViewState["deletedRotations"] = value; } }

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (Session["access"] != null)
        //        {
        //            if (SysSecurityAccess.UserAccess((List<SysScreens.Screens>)Session["menu"], GeneralCommon.SystemScreens.Resident))
        //            {
        //                if (!IsPostBack)
        //                {
        //                    ////residents by entity type
        //                    FillResidents();

        //                    Initialize();
        //                }
        //            }
        //            else
        //                Response.Redirect("/Modules/AccessDenied.aspx");
        //        }
        //        else
        //            Response.Redirect("/Account/LogIn.aspx");
        //    }
        //    catch (Exception ex)
        //    {
        //        SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
        //    }
        //}

        //#region Methods
        //private void Initialize()
        //{
        //    ddlUniversities.DataTextField = "universityName";
        //    ddlUniversities.DataValueField = "universityId";
        //    ddlUniversities.DataSource = ResUniversities.GetUniversities();
        //    ddlUniversities.DataBind();
        //    ddlUniversities.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

        //    ddlGrade.DataTextField = "gradeName";
        //    ddlGrade.DataValueField = "gradeId";
        //    ddlGrade.DataSource = ResGrades.GetGrades();
        //    ddlGrade.DataBind();
        //    ddlGrade.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

        //    ////centers
        //    ddlRotationCenters.DataTextField = "centerName";
        //    ddlRotationCenters.DataValueField = "centerId";
        //    ddlRotationCenters.DataSource = SysCenters.GetCenters();
        //    ddlRotationCenters.DataBind();
        //    ddlRotationCenters.Items.Insert(0, new ListItem("-- Extrangero --", "0"));

        //    ////department
        //    ddlDepartments.DataTextField = "departmentName";
        //    ddlDepartments.DataValueField = "departmentId";
        //    ddlDepartments.DataSource = ResDepartments.GetDepartments();
        //    ddlDepartments.DataBind();
        //    ddlDepartments.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

        //    ////rotation tipe
        //    ddlRotationType.DataTextField = "typeName";
        //    ddlRotationType.DataValueField = "typeId";
        //    ddlRotationType.DataSource = ResRotationTypes.GetRotationTypes();
        //    ddlRotationType.DataBind();
        //    ddlRotationType.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

        //    ////emergency info
        //    ddlRelationship.DataTextField = "typeName";
        //    ddlRelationship.DataValueField = "typeId";
        //    ddlRelationship.DataSource = GenPersonRelationshipType.GetRelationshipTypes();
        //    ddlRelationship.DataBind();
        //    ddlRelationship.Items.Insert(0, new ListItem("-- Seleccione --", "0"));
        //}

        //private void FillResidents()
        //{
        //    ////Persons by entity type
        //    grvResidents.DataSource = GenPersons.GetPersonsByEntityType((int)GeneralCommon.EntityTypes.Resident, (int)((SysUserAccess)Session["access"]).CenterId, true);
        //    grvResidents.DataBind();
        //}

        //private void MakeClean()
        //{
        //    txtExequatur.Text = string.Empty;
        //    txtEnrollment.Text = string.Empty;
        //    ddlUniversities.SelectedIndex = 0;
        //    AsignedEspecialties = null;
        //    grvResidences.DataSource = null;
        //    grvResidences.DataBind();
        //    //ddlResidences.SelectedValue = "0";
        //    ddlGrade.SelectedIndex = 0;
        //    ddlGrade.Enabled = true;
        //    txtStartDateResidence.Text = string.Empty;
        //    AsignedEspecialties = null;
        //    ddlRotationCenters.SelectedIndex = 0;
        //    ddlDepartments.SelectedIndex = 0;
        //    txtStartDate.Text = string.Empty;
        //    txtEndDate.Text = string.Empty;
        //    ddlRotationType.SelectedIndex = 0;
        //    txtRotationNote.Text = string.Empty;
        //    AsignedRotations = null;
        //    grvRotations.DataSource = null;
        //    grvRotations.DataBind();
        //    txtName.Text = string.Empty;
        //    txtMiddleName.Text = string.Empty;
        //    txtFirstLastName.Text = string.Empty;
        //    txtSecondLastName.Text = string.Empty;
        //    ddlRelationship.SelectedIndex = 0;
        //    txtAllergies.Text = string.Empty;
        //    txtDrugs.Text = string.Empty;
        //}

        //private void HideOptionsInEspecialities()
        //{
        //    foreach (GridViewRow row in grvResidences.Rows)
        //    {
        //        ImageButton obj = row.FindControl("residenceSelect") as ImageButton;
        //        obj.Visible = false;
        //        if (Convert.ToInt32(grvResidences.DataKeys[row.RowIndex].Values["gradeId"]) == (int)((ResResidentInfo)Session["resident"]).GradeId)
        //        {
        //            if ((int)((ResResidentInfo)Session["resident"]).ResidentStatusId == (int)GeneralCommon.ResidentStatuses.Active)
        //                obj.Visible = true;
        //        }
        //    }
        //    ddlResidences.Enabled = false;
        //    ddlGrade.Enabled = false;
        //    ddlResidences.SelectedIndex = 0;
        //    ddlGrade.SelectedValue = ((ResResidentInfo)Session["resident"]).GradeId.ToString();
        //}

        //private void HideOptionsInRotations()
        //{
        //    foreach (GridViewRow row in grvRotations.Rows)
        //    {
        //        ImageButton obj = row.FindControl("rotationSelect") as ImageButton;
        //        obj.Visible = true;
        //        if (Convert.ToDateTime(grvRotations.DataKeys[row.RowIndex].Values["endDate"]) <= DateTime.Now)
        //            obj.Visible = false;
        //        else
        //            btnAddRotation.Visible = false;                    
        //    }
        //    ddlRotationType.SelectedIndex = 0;
        //    divCenter.Visible = false;
        //    ddlRotationCenters.SelectedIndex = 0;
        //    ddlDepartments.SelectedIndex = 0;
        //    txtStartDate.Text = string.Empty;
        //    txtEndDate.Text = string.Empty;
        //    txtRotationNote.Text = string.Empty;
        //}

        //private bool Validations()
        //{
        //    bool StopValidation = false;
        //    ////especialties
        //    if (AsignedEspecialties == null)
        //    {
        //        Master.Master.Message("Al menos una especialidad es requerida.", "", 0);
        //        StopValidation = true;
        //    }

        //    ////rotations
        //    if (AsignedRotations == null)
        //    {
        //        Master.Master.Message("Al menos una rotación es requerida.", "", 2);

        //        StopValidation = true;
        //    }

        //    return StopValidation;
        //}
        //#endregion

        //#region Buttons
        //protected void btnAddResidence_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Master.Master.Message("", "", 0);
        //        if (Convert.ToInt32(ddlResidences.SelectedValue) < 1 || Convert.ToInt32(ddlGrade.SelectedValue) < 1 || txtStartDateResidence.Text == string.Empty)
        //        {
        //            Master.Master.Message("Complete los campos de la especialidad y inténtelo de nuevo.", "", 2);
        //            return;
        //        }

        //        if (Convert.ToDateTime(txtStartDateResidence.Text) <= (DateTime)((SysUserAccess)Session["access"]).StartDate || Convert.ToDateTime(txtStartDateResidence.Text) >= (DateTime)((SysUserAccess)Session["access"]).EndDate)
        //        { 
        //            Master.Master.Message("La fecha de inicio no se encuentra en el año docente actual.", "", 2);
        //            return;
        //        }                

        //        //just for first time
        //        if ((int)((ResResidentInfo)Session["resident"]).ResidentStatusId == null && AsignedEspecialties.Count == 1)
        //        {
        //            foreach (ResResidents.Especialties especialtyInList in AsignedEspecialties)
        //            {
        //                if (especialtyInList.startDate == Convert.ToDateTime(txtStartDateResidence.Text))
        //                {
        //                    Master.Master.Message("A este Médico Residente ya se le fue asignado una especialidad", "", 2);
        //                    return;
        //                }
        //            }
        //        }

        //        List<ResResidents.Especialties> tempEspecialties = new List<ResResidents.Especialties>();
        //        if (AsignedEspecialties != null)
        //            tempEspecialties = AsignedEspecialties.ToList();

        //        ResResidents.Especialties newEspecialty = new ResResidents.Especialties();
        //        newEspecialty.residenceId = Convert.ToInt32(ddlResidences.SelectedValue);
        //        newEspecialty.residenceName = ddlResidences.SelectedItem.ToString();
        //        newEspecialty.gradeId = Convert.ToInt32(ddlGrade.SelectedValue);
        //        newEspecialty.gradeName = ddlGrade.SelectedItem.ToString();
        //        newEspecialty.startDate = Convert.ToDateTime(txtStartDateResidence.Text);
        //        newEspecialty.endDate = null;

        //        ////allow single grades and update startdate
        //        foreach (ResResidents.Especialties especialtyInList in tempEspecialties.ToList())
        //        {
        //            if (especialtyInList.gradeId == newEspecialty.gradeId && especialtyInList.residenceId == newEspecialty.residenceId)
        //            {
        //                if (especialtyInList.startDate == newEspecialty.startDate)
        //                {
        //                    Master.Master.Message("Ya existe una especialidad para este año.", "", 2);
        //                    HideOptionsInEspecialities();
        //                    return;
        //                }
        //                else
        //                {
        //                    //if is true -> editing
        //                    if (ddlResidences.Enabled == true)
        //                    {
        //                        foreach (ResResidents.Especialties inList in AsignedEspecialties.Where(a => a.gradeId == newEspecialty.gradeId))
        //                            inList.startDate = newEspecialty.startDate;

        //                        grvResidences.DataSource = AsignedEspecialties;
        //                        grvResidences.DataBind();
        //                        HideOptionsInEspecialities();
        //                        return;
        //                    }
        //                }
        //            }
        //            //has a assigned residence in this year
        //            if ((int)((ResResidentInfo)Session["resident"]).ResidentStatusId == (int)GeneralCommon.ResidentStatuses.Active)
        //            {
        //                Master.Master.Message("Este Médico Residente se encuentra activo para el año " + especialtyInList.gradeName + " y no puede ser Activado en otro.", "", 2);
        //                HideOptionsInEspecialities();
        //                return;
        //            }
        //        }                   
            
        //        tempEspecialties.Add(newEspecialty);
        //        AsignedEspecialties = tempEspecialties.OrderByDescending(p => p.gradeId).ToList();

        //        grvResidences.DataSource = AsignedEspecialties;
        //        grvResidences.DataBind();
        //        HideOptionsInEspecialities();
        //    }
        //    catch (Exception ex)
        //    {
        //        SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
        //    }
        //}

        //protected void btnAddRotation_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Master.Master.Message("", "", 0);
        //        if (Convert.ToInt32(ddlDepartments.SelectedValue) < 1 || txtStartDate.Text == string.Empty || txtEndDate.Text == string.Empty || Convert.ToInt32(ddlRotationType.SelectedValue) < 1)
        //        {
        //            Master.Master.Message("Complete los campos de rotación y inténtelo de nuevo.", "", 2);
        //            return;
        //        }

        //        DateTime _startDate = Convert.ToDateTime(txtStartDate.Text);
        //        DateTime _endDate = Convert.ToDateTime(txtEndDate.Text);
        //        if (_startDate > _endDate)
        //        {
        //            Master.Master.Message("La fecha de inicio de la rotación debe de ser menor que la fecha de fin.", "", 2);
        //            return;
        //        }

        //        TimeSpan differentIdDates = _endDate - _startDate;
        //        ////1 month -> 30 days || 6 month -> 30*6=180+3 (+3 cause month with 31 days
        //        if (differentIdDates.Days < 30 || differentIdDates.Days > 183)
        //        {
        //            Master.Master.Message("El Tiempo de duración de las rotaciones debe de ser de mínimo un (1) mes y máximo seix (6) mese.", "", 2);
        //            return;
        //        }

        //        ////has to be >= now
        //        if (_startDate.Date < DateTime.Now.Date)
        //        {
        //            Master.Master.Message("La fecha de inicio no es válida.", "", 2);
        //            return;
        //        }

        //        if (Convert.ToInt32(ddlRotationType.SelectedValue) == (int)GeneralCommon.RotationTypes.External && txtRotationNote.Text == string.Empty)
        //        {
        //            Master.Master.Message("La rotación es Externa, favor agregar alguna nota.", "", 2);
        //            return;
        //        }

        //        ////dont allow more than 1 rotation in date
        //        if (btnAddRotation.Visible == true)
        //        {
        //            foreach (ResRotations.Rotations rotationInList in AsignedRotations.Where(r => r.endDate.Date > DateTime.Now.Date))
        //            {
        //                rotationInList.departmentId = Convert.ToInt32(ddlDepartments.SelectedValue);
        //                rotationInList.departmentName = ddlDepartments.SelectedItem.ToString();
        //                rotationInList.startDate = Convert.ToDateTime(txtStartDate.Text);
        //                rotationInList.endDate = Convert.ToDateTime(txtEndDate.Text);
        //                rotationInList.rotationTypeId = Convert.ToInt32(ddlRotationType.SelectedValue);
        //                rotationInList.rotationType = ddlRotationType.SelectedItem.ToString();
        //                rotationInList.rotationNote = txtRotationNote.Text;
        //                rotationInList.centerId = Convert.ToInt32(ddlRotationCenters.SelectedValue);
        //                grvRotations.DataSource = AsignedRotations.OrderByDescending(p => p.startDate).ToList();
        //                grvRotations.DataBind();
        //                HideOptionsInRotations();
        //                return;
        //            }
        //        }
        //        else
        //        {
        //            foreach (ResRotations.Rotations rotationInList in AsignedRotations)
        //            {
        //                if (rotationInList.endDate.Date > DateTime.Now.Date)
        //                {
        //                    Master.Master.Message("Tiene una rotación sin finalizar. No puede agregar otra rotación.", "", 2);
        //                    return;
        //                }
        //            }
        //        }


        //        List<ResRotations.Rotations> tempRotations = new List<ResRotations.Rotations>();
        //        if (AsignedRotations != null)
        //            tempRotations = AsignedRotations.ToList();

        //        ResRotations.Rotations newRotation = new ResRotations.Rotations();
        //        if (grvRotations.SelectedIndex > -1)
        //            newRotation.rotationId = (int)grvRotations.SelectedDataKey[0];
        //        else
        //            newRotation.rotationId = 0;

        //        newRotation.departmentId = Convert.ToInt32(ddlDepartments.SelectedValue);
        //        newRotation.departmentName = ddlDepartments.SelectedItem.ToString();
        //        newRotation.startDate = Convert.ToDateTime(txtStartDate.Text);
        //        newRotation.endDate = Convert.ToDateTime(txtEndDate.Text);
        //        newRotation.rotationTypeId = Convert.ToInt32(ddlRotationType.SelectedValue);
        //        newRotation.rotationType = ddlRotationType.SelectedItem.ToString();
        //        newRotation.rotationNote = txtRotationNote.Text;
        //            newRotation.centerId = Convert.ToInt32(ddlRotationCenters.SelectedValue);
                
        //        tempRotations.Add(newRotation);
        //        AsignedRotations = tempRotations.OrderByDescending(p => p.startDate).ToList();

        //        grvRotations.DataSource = AsignedRotations;
        //        grvRotations.DataBind();
        //        HideOptionsInRotations();
        //    }
        //    catch (Exception ex)
        //    {
        //        SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
        //    }
        //}

        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (Validations())
        //            return;

        //        int GeneralEntityId = 0;
        //        ResResidents ActualResident;
        //        //ResResidentDocuments ResidentDocuments;
        //        //ResRotations ResidentRotations;  
        //        //GenGenerals GeneralEntity;
        //        //GenEntities Entity;

        //        GenPersonEmergencyContacts Contact;
        //        GenPersons EmergencyContact;
        //        if ((int)grvResidents.SelectedDataKey[0] > 0)
        //        {
        //            ActualResident = ResResidents.Get((int)grvResidents.SelectedDataKey[0]);
        //            //ResidentDocuments = ResResidentDocuments.Get((int)grvResidents.SelectedDataKey[0]);
        //            //ResidentRotations = ResRotations.Get((int)grvResidents.SelectedDataKey[0]);
        //            //GeneralEntity = GenGenerals.Get((int)grvResidents.SelectedDataKey[2]);
        //            //Entity = GenEntities.Get((int)grvResidents.SelectedDataKey[2], (int)GeneralCommon.EntityTypes.Resident);
        //            //EmergencyContact =GenPersons.Get((int)grvResidents.SelectedDataKey[3]);
        //            Contact = GenPersonEmergencyContacts.Get((int)grvResidents.SelectedDataKey[3]);
        //            EmergencyContact = GenPersons.Get((int)Contact.ContactId);
        //        }
        //        else
        //        {
        //            ActualResident = new ResResidents();
        //            //ResidentDocuments = new ResResidentDocuments();
        //            //ResidentRotations = new ResRotations();
        //            //GeneralEntity = new GenGenerals();
        //            //Entity = new GenEntities();

        //            Contact = new GenPersonEmergencyContacts();
        //            EmergencyContact = new GenPersons();
        //        }

        //        ////resident
        //        //ResResidents ActualResident = new ResResidents();
        //        ActualResident.GeneralId = (int)grvResidents.SelectedDataKey[2];
        //        foreach (ResResidents.Especialties especialtiesInList in AsignedEspecialties.ToList())
        //        {
        //            ////take the top 1
        //            ActualResident.ResidenceId = especialtiesInList.residenceId;
        //            ActualResident.GradeId = especialtiesInList.gradeId;
        //            ActualResident.StartDate = especialtiesInList.startDate;
        //            break;
        //        }
        //        ActualResident.UniversityId = Convert.ToInt32(ddlUniversities.SelectedValue);
        //        ActualResident.CentreId = (int)((SysUserAccess)Session["access"]).UserCenterId;

        //        ////resident documents
        //        ResResidentDocuments ResidentDocuemnts = new ResResidentDocuments();

        //        ////resident rotations
        //        ResRotations ResidentRotations = new ResRotations();

        //        ////emergency contact info
        //        //GenPersons EmergencyContact = new GenPersons();
        //        //EmergencyContact.GeneralId = (int)grvResidents.SelectedDataKey[2];
        //        EmergencyContact.Name = txtName.Text;
        //        EmergencyContact.MiddleName = txtMiddleName.Text;
        //        EmergencyContact.FirstLastName = txtFirstLastName.Text;
        //        EmergencyContact.SecondLastName = txtSecondLastName.Text;

        //        ////emergency contact
        //        //GenPersonEmergencyContacts Contact = new GenPersonEmergencyContacts();                               
        //        Contact.RelationshipId = Convert.ToInt32(ddlRelationship.SelectedValue);
        //        Contact.Allergies = txtAllergies.Text;
        //        Contact.Drugs = txtDrugs.Text;

        //        int _residentId = 0;
        //        if (((ResResidentInfo)Session["resident"]).ResidentStatusId == (int)GeneralCommon.ResidentStatuses.Active)
        //        {
        //            ActualResident.ResidentId = (int)grvResidents.SelectedDataKey[0];
        //            ActualResident.ModifiedBy = ((SysUserAccess)Session["access"]).UserCenterId;
        //            ActualResident.ModifiedDate = DateTime.Now;
        //            ActualResident.Update();
        //        }
        //        else
        //        {
        //            ActualResident.TeachingYearId = (int)((SysUserAccess)Session["access"]).TeachingYearId;
        //            ActualResident.ResidentStatusId = (int)GeneralCommon.ResidentStatuses.Active;
        //            ActualResident.CreatedBY = ((SysUserAccess)Session["access"]).UserCenterId;
        //            ActualResident.CreatedDate = DateTime.Now;
        //            _residentId=ActualResident.Insert();
        //        }

        //        if ((int)grvResidents.SelectedDataKey[0] != 0)
        //        {
        //            //EmergencyContact.PersonId = (int)grvResidents.SelectedDataKey[3];
        //            EmergencyContact.ModifiedBy = ((SysUserAccess)Session["access"]).UserCenterId;
        //            EmergencyContact.ModifiedDate = DateTime.Now;
        //            EmergencyContact.Update();

        //            //Contact.ContactId = (int)grvResidents.SelectedDataKey[3];
        //            Contact.ModifiedBy = ((SysUserAccess)Session["access"]).UserCenterId;
        //            Contact.ModifiedDate = DateTime.Now;
        //            Contact.Update();

        //            ResidentDocuemnts.ResidentId = (int)grvResidents.SelectedDataKey[0];
        //        }
        //        else
        //        {
        //            ////general for emergency contact
        //            GenGenerals GeneralEntity = new GenGenerals();
        //            GeneralEntityId = GeneralEntity.Insert();

        //            ResidentRotations.ResidentId = _residentId;

        //            ResidentDocuemnts.ResidentId = ResidentRotations.ResidentId;

        //            //// emergency contact entity
        //            GenEntities EmergencyEntity = new GenEntities();
        //            EmergencyEntity.GeneralId = GeneralEntityId;
        //            EmergencyEntity.EntityTypeId = (int)GeneralCommon.EntityTypes.EmergencyContact;
        //            EmergencyEntity.CenterId = ((SysUserAccess)Session["access"]).CenterId;
        //            EmergencyEntity.EntityStatus = true;
        //            EmergencyEntity.CreatedBy = ((SysUserAccess)Session["access"]).UserCenterId;
        //            EmergencyEntity.CreatedDate = DateTime.Now;
        //            EmergencyEntity.Insert();

        //            EmergencyContact.GeneralId = GeneralEntityId;

        //            Contact.ContactId = EmergencyContact.Insert();
        //            Contact.PersonId = (int)grvResidents.SelectedDataKey[1];
        //            Contact.CreatedBY = ((SysUserAccess)Session["access"]).UserCenterId;
        //            Contact.CreatedDate = DateTime.Now;
        //            Contact.Insert();
        //        }

        //        ////residnet documents                
        //        ResidentDocuemnts.CreatedBY = ((SysUserAccess)Session["access"]).UserCenterId;
        //        ResidentDocuemnts.CreatedDate = DateTime.Now;
        //        ResidentDocuemnts.ModifiedBy = ((SysUserAccess)Session["access"]).UserCenterId;
        //        ResidentDocuemnts.ModifiedDate = DateTime.Now;
        //        if ((int)grvResidents.SelectedDataKey[0] > 0)
        //        {
        //            ResidentDocuemnts.DocumentNumber = txtEnrollment.Text;
        //            ResidentDocuemnts.DocumentTypeId = (int)GeneralCommon.ResidentDocTypes.Enrollment;
        //            ResidentDocuemnts.Update();

        //            ResidentDocuemnts.DocumentNumber = txtExequatur.Text;
        //            ResidentDocuemnts.DocumentTypeId = (int)GeneralCommon.ResidentDocTypes.Exequatur;
        //            ResidentDocuemnts.Update();
        //        }
        //        else
        //        {
        //            ResidentDocuemnts.DocumentNumber = txtEnrollment.Text;
        //            ResidentDocuemnts.DocumentTypeId = (int)GeneralCommon.ResidentDocTypes.Enrollment;
        //            ResidentDocuemnts.Insert();

        //            ResidentDocuemnts.DocumentNumber = txtExequatur.Text;
        //            ResidentDocuemnts.DocumentTypeId = (int)GeneralCommon.ResidentDocTypes.Exequatur;
        //            ResidentDocuemnts.Insert();
        //        }

        //        ////rotation
        //        ResidentRotations.ResidentId = (int)grvResidents.SelectedDataKey[0];
        //        //just for the firs rotation in top
        //        foreach (ResRotations.Rotations rotationsInList in AsignedRotations.OrderByDescending(p => p.startDate).ToList())
        //        {
        //            ResidentRotations.RotationId = rotationsInList.rotationId;
        //            ResidentRotations.DepartmentId = rotationsInList.departmentId;
        //            ResidentRotations.StartDate = rotationsInList.startDate;
        //            ResidentRotations.EndDate = rotationsInList.endDate;
        //            ResidentRotations.RotationTypeId = rotationsInList.rotationTypeId;
        //            ResidentRotations.Note = rotationsInList.rotationNote;
        //            if (rotationsInList.centerId != 0)
        //                ResidentRotations.CenterId = rotationsInList.centerId;
        //            else
        //                ResidentRotations.CenterId = null;
        //            break;
        //        }

        //        ResidentRotations.CreatedBy = ((SysUserAccess)Session["access"]).UserCenterId;
        //        ResidentRotations.CreatedDate = DateTime.Now;
        //        ResidentRotations.ModifiedBy = ((SysUserAccess)Session["access"]).UserCenterId;
        //        ResidentRotations.ModifiedDate = DateTime.Now;
        //        if (ResidentRotations.RotationId > 0)
        //            ResidentRotations.Update();
        //        else
        //            ResidentRotations.Insert();

        //        if (DeletedRotations != null)
        //        {
        //            ////just the top one
        //            foreach (ResRotations.Rotations rotationsInList in DeletedRotations.ToList())
        //            {
        //                ResidentRotations.RotationId = rotationsInList.rotationId;
        //                ResidentRotations.Delete();
        //                break;
        //            }
        //        }

        //        MakeClean();
        //        FillResidents();
        //        Master.Master.Message("Los datos para este Médico Residente se han guardado exitosamente.", "", 3);
        //    }
        //    catch (Exception ex)
        //    {
        //        SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
        //    }
        //}

        //protected void btnCancel_Click(object sender, EventArgs e)
        //{
        //    MakeClean();
        //    btnSave.Enabled = false;
        //    btnCancel.Enabled = false;
        //}
        //#endregion

        //#region GridViews
        //protected void grvResidents_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        MakeClean();

        //        ResResidentInfo ResidentInfo;
        //        if ((int)grvResidents.SelectedDataKey[0] != 0)
        //            ResidentInfo = ResResidentInfo.Get((int)grvResidents.SelectedDataKey[0]);
        //        else
        //            ResidentInfo = new ResResidentInfo();

        //        if (ResidentInfo.UniversityId != null)
        //            ddlUniversities.SelectedValue = ResidentInfo.UniversityId.ToString();

        //        Session["resident"] = ResidentInfo;

        //        ////especialty  
        //        ddlResidences.Items.Clear();
        //        ddlResidences.DataTextField = "residenceName";
        //        ddlResidences.DataValueField = "residenceId";
        //        ddlResidences.DataSource = ResResidences.GetResidencesByGrade(ResidentInfo.ResidenceId, ResidentInfo.GradeId, (int)((SysUserAccess)Session["access"]).CenterId);
        //        ddlResidences.DataBind();
        //        ddlResidences.Items.Insert(0, new ListItem("-- Seleccione --", "0"));

        //        ////if (ResidentInfo.GradeId != null)
        //        ////{
        //        //ddlGrade.SelectedValue = ResidentInfo.GradeId.ToString();
        //        //ddlGrade.Enabled = false;
        //        ////if (ResidentInfo.ResidentStatusId == (int)GeneralCommon.ResidentStatuses.Initiated)
        //        ////    btnAddResidence.Enabled = true;
        //        ////}
        //        ////else
        //        ////{
        //        ////    btnAddResidence.Enabled = true;
        //        ////}

        //        //if (ResidentInfo.GradeId != null)
        //        //{
        //        //    //switch (ResidentInfo.ResidentStatusId)
        //        //    //{
        //        //    //    case (int)GeneralCommon.ResidentStatuses.Active:
        //        //    //        ddlGrade.Enabled = false;
        //        //    //        break;
        //        //    //    case (int)GeneralCommon.ResidentStatuses.Inactive:
        //        //    //        ddlGrade.Enabled = true;
        //        //    //        break;
        //        //    //    case (int)GeneralCommon.ResidentStatuses.Retired:
        //        //    //        ddlGrade.Enabled = true;
        //        //    //        break;
        //        //    //    case (int)GeneralCommon.ResidentStatuses.Espelled:
        //        //    //        ddlGrade.Enabled = true;
        //        //    //        break;
        //        //    //    case (int)GeneralCommon.ResidentStatuses.Rotating:
        //        //    //        ddlGrade.Enabled = false;
        //        //    //        break;
        //        //    //    case (int)GeneralCommon.ResidentStatuses.Initiated:
        //        //    //        ddlGrade.Enabled = false;
        //        //    //        break;
        //        //    //    case (int)GeneralCommon.ResidentStatuses.Moved:
        //        //    //        ddlGrade.Enabled = true;
        //        //    //        break;
        //        //    //}
        //        //}            

        //        if ((int)grvResidents.SelectedDataKey[0] != 0)
        //        {
        //            txtExequatur.Text = ResidentInfo.Exequatur;
        //            txtEnrollment.Text = ResidentInfo.Enrollment;

        //            AsignedEspecialties = ResResidents.GetByGeneralId((int)grvResidents.SelectedDataKey[2]);
        //            grvResidences.DataSource = AsignedEspecialties;
        //            grvResidences.DataBind();

        //            if (ResidentInfo.ResidentStatusId == (int)GeneralCommon.ResidentStatuses.Promoted)
        //            {
        //                ddlGrade.SelectedValue = (ResidentInfo.GradeId + 1).ToString();
        //                ddlGrade.Enabled = true;
        //            }
        //            else
        //            {
        //                ddlGrade.SelectedValue = ResidentInfo.GradeId.ToString();
        //                ddlGrade.Enabled = false;
        //            }
                    
        //            HideOptionsInEspecialities();

        //            AsignedRotations = ResRotations.GetByResidentId((int)grvResidents.SelectedDataKey[0]);
        //            grvRotations.DataSource = AsignedRotations.OrderByDescending(p => p.startDate).ToList();
        //            grvRotations.DataBind();

        //            HideOptionsInRotations();

        //            txtName.Text = ResidentInfo.Name;
        //            txtMiddleName.Text = ResidentInfo.MiddleName;
        //            txtFirstLastName.Text = ResidentInfo.FirstLastName;
        //            txtSecondLastName.Text = ResidentInfo.SecondLastName;
        //            ddlRelationship.SelectedValue = ResidentInfo.RelationshipId.ToString();
        //            txtAllergies.Text = ResidentInfo.Allergies;
        //            txtDrugs.Text = ResidentInfo.Drugs;
        //        }

        //        btnSave.Enabled = true;
        //        btnCancel.Enabled = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
        //    }
        //}
        
        //protected void grvResidences_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName == "Editing")
        //        {
        //            IList<ResResidents.Especialties> asignedEspecialtyList = AsignedEspecialties;

        //            foreach (ResResidents.Especialties especialtyInList in asignedEspecialtyList.ToList())
        //            {
        //                if (especialtyInList.gradeId == Convert.ToInt32(e.CommandArgument))
        //                {
        //                    ddlResidences.SelectedValue = especialtyInList.residenceId.ToString();
        //                    ddlGrade.SelectedValue = especialtyInList.gradeId.ToString();
        //                    txtStartDateResidence.Text = especialtyInList.startDate.ToString();
        //                    ddlResidences.Enabled = true;
        //                    if ((int)((ResResidentInfo)Session["resident"]).GradeId == (int)GeneralCommon.ResidenceGrades.RI)
        //                        ddlGrade.Enabled = true;                            
        //                    return;
        //                }
        //            }
        //        }

        //        //if (e.CommandName == "Deleting")
        //        //{
        //        //    IList<ResResidents.Especialties> asignedEspecialtyList = AsignedEspecialties;

        //        //    foreach (ResResidents.Especialties especialtyInList in asignedEspecialtyList.ToList())
        //        //    {
        //        //        if (especialtyInList.gradeId == Convert.ToInt32(e.CommandArgument))
        //        //        {
        //        //            asignedEspecialtyList.Remove(especialtyInList);
        //        //            divResidentError.Visible = false;
        //        //            grvResidences.DataSource = asignedEspecialtyList;
        //        //            grvResidences.DataBind();
        //        //            return;
        //        //        }
        //        //    }
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
        //    }
        //}
        
        //protected void grvRotations_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName == "Editing")
        //        {
        //            IList<ResRotations.Rotations> asignedRotationList = AsignedRotations;

        //            foreach (ResRotations.Rotations rotationInList in asignedRotationList.ToList())
        //            {
        //                if (rotationInList.rotationId == Convert.ToInt32(e.CommandArgument))
        //                {
        //                    ddlDepartments.SelectedValue = rotationInList.departmentId.ToString();
        //                    txtStartDate.Text = rotationInList.startDate.ToString();
        //                    txtEndDate.Text = rotationInList.endDate.ToString();
        //                    ddlRotationType.SelectedValue = rotationInList.rotationTypeId.ToString();
        //                    txtRotationNote.Text = rotationInList.rotationNote;
        //                    btnAddRotation.Visible = true;
        //                    return;
        //                }
        //            }
        //        }

        //        //if (e.CommandName == "Deleting")
        //        //{
        //        //    IList<ResRotations.Rotations> asignedRotationList = AsignedRotations;
        //        //    IList<ResRotations.Rotations> deletedRotationList = DeletedRotations;

        //        //    foreach (ResRotations.Rotations rotationInList in asignedRotationList.ToList())
        //        //    {
        //        //        if (rotationInList.rotationId == Convert.ToInt32(e.CommandArgument))
        //        //        {
        //        //            asignedRotationList.Remove(rotationInList);
        //        //            deletedRotationList.Add(rotationInList);
        //        //            grvRotations.DataSource = asignedRotationList;
        //        //            grvRotations.DataBind();
        //        //            return;
        //        //        }
        //        //    }
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        SysErrors.SetSystemError(Path.GetFileName(Request.PhysicalPath), MethodInfo.GetCurrentMethod().Name, (Session["access"] == null ? null : (int?)((SysUserAccess)Session["access"]).UserCenterId), ref ex);
        //    }
        //}
        //#endregion

        //protected void ddlRotationType_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (Convert.ToInt32(ddlRotationType.SelectedValue) == (int)GeneralCommon.RotationTypes.Internal)
        //        divCenter.Visible = false;
        //    else
        //        divCenter.Visible = true;
        //}
    }
}