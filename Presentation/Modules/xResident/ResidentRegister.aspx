﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/Residents/Residents.master"
    AutoEventWireup="true" CodeBehind="ResidentRegister.aspx.cs" Inherits="Presentation.Modules.Residents.Forms.Residents" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#Personals" data-toggle="tab">Datos Personales</a></li>
            <li><a href="#Localization" data-toggle="tab">Localización</a></li>
            <li><a href="#Identification" data-toggle="tab">Identificación</a></li>
            <li><a href="#Resident" data-toggle="tab">Residente</a></li>
            <li><a href="#Rotation" data-toggle="tab">Rotación</a></li>
            <li><a href="#EmergencyDate" data-toggle="tab">Datos de Emergencia</a></li>
        </ul>
        <div class="tab-content">
            <div id="Personals" class="tab-pane active">
                <asp:ValidationSummary ID="ResidentValidationSummary" runat="server" CssClass="alert alert-danger"
                    ValidationGroup="ResidentValidationGroup" />
                <fieldset>
                    <legend>Datos Personales</legend>
                    <div class="control-group">
                        <label>
                            Nombre</label>
                        <div class="controls">
                            <asp:TextBox ID="txtName" runat="server" CssClass="textEntry" placeholder=""></asp:TextBox>
                            <asp:RequiredFieldValidator ID="NameRequired" runat="server" ControlToValidate="txtName"
                                CssClass="failureNotification" ErrorMessage="Nombre es requerido." ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="control-group">
                        <label>
                            Segundo Nombre</label>
                        <div class="controls">
                            <asp:TextBox ID="txtMiddleName" runat="server" CssClass="textEntry" placeholder=""></asp:TextBox>
                            <asp:RequiredFieldValidator ID="MiddleNameRequired" runat="server" ControlToValidate="txtMiddleName"
                                CssClass="failureNotification" ErrorMessage="Segundo Nombre es requerido." ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="control-group">
                        <label>
                            Primer Apellido</label>
                        <div class="controls">
                            <asp:TextBox ID="txtFirstLastName" runat="server" CssClass="textEntry" placeholder=""></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label>
                            Segundo Apellido</label>
                        <div class="controls">
                            <asp:TextBox ID="txtSecondLastName" runat="server" CssClass="textEntry" placeholder=""></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label>
                            Fecha de Nacimiento</label>
                        <div class="controls">
                            <asp:TextBox ID="txtBirthday" runat="server" CssClass="textEntry" placeholder=""></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label>
                            Sexo</label>
                        <div class="controls">
                            <asp:DropDownList ID="ddlGenders" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="control-group">
                        <label>
                            Estado Civil</label>
                        <div class="controls">
                            <asp:DropDownList ID="ddlMaritalStatuses" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div id="Localization" class="tab-pane">
                <asp:UpdatePanel ID="upanelLocalization" runat="server">
                    <ContentTemplate>
                        <fieldset>
                            <legend>Teléfonos</legend>
                            <div>
                                <table style="width: 100%">
                                    <tr>
                                        <td>
                                            <div class="control-group">
                                                <label>
                                                    Teléfono</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtPhone" runat="server" CssClass="textEntry" placeholder=""></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="control-group">
                                                <label>
                                                    Tipo</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddlPhoneTipes" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAddPhone" runat="server" Text="Agregar" class="btn btn-large btn-link"
                                                type="submit" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <asp:GridView ID="grvPhones" runat="server" Width="100%" class="table-striped">
                                    </asp:GridView>
                                </div>
                            </div>
                            
                            </fieldset>
                            <fieldset><legend>Direccón</legend>

                            <div>
                                <label>
                                    Dirección 1</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddress1" runat="server" CssClass="textEntry" placeholder=""></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label>
                                    Dirección 2</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="textEntry" placeholser=""></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label>
                                    Ciudad</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddCities" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label>
                                    Provincia</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlStates" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label>
                                    Nacionalidad</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlNationality" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </fieldset>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="Identification" class="tab-pane">
            <asp:UpdatePanel ID="upanelIdentification" runat="server">
            <ContentTemplate>
                <fieldset>
                    <legend>Identificación Personal</legend>
                    <div>
                        <table style="width: 100%">
                            <tr>
                                <td>
                                    <div class="control-group">
                                        <label>
                                            Identificación</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtIdentification" runat="server" CssClass="textEntry" placeholder=""></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="control-group">
                                        <label>
                                            Tipo</label>
                                        <div class="controls">
                                            <asp:DropDownList ID="ddlIndentificationTypes" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <asp:Button ID="btnAddIdentification" runat="server" Text="Agregar" class="btn btn-large btn-link"
                                        type="submit" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <asp:GridView ID="grvIdentifications" runat="server" Width="100%" class="table-striped">
                            </asp:GridView>
                        </div>
                    </div>
                </fieldset>
                </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="Resident" class="tab-pane">
                <asp:UpdatePanel ID="upanelResident" runat="server">
                    <ContentTemplate>
                        <fieldset>
                            <legend>Especialidad</legend>
                            <div class="control-group">
                                <label>
                                    Universidad</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlUniversities" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div>
                                <table style="width: 100%">
                                    <tr>
                                        <td>
                                            <div class="control-group">
                                                <label>
                                                    Residencia</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddlResidences" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="control-group">
                                                <label>
                                                    Año</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddlGrade" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="control-group">
                                                <label>
                                                    Fecha de Inicio</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtStartDateResidence" runat="server" CssClass="textEntry" placeholder=""></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAddResidence" runat="server" Text="Agregar" class="btn btn-large btn-link"
                                                type="submit" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="control-group">
                            <asp:GridView ID="grvResidences" runat="server" Width="100%" class="table-striped">
                                </asp:GridView>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Sub Especialidad</legend>
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <div class="control-group">
                                            <label>
                                                Residencia</label>
                                            <div class="controls">
                                                <asp:DropDownList ID="ddlSubResidences" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="control-group">
                                            <label>
                                                Año</label>
                                            <div class="controls">
                                                <asp:DropDownList ID="ddlSubGrade" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="control-group">
                                            <label>
                                                Fecha de Inicio</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txtSubStartDate" runat="server" CssClass="textEntry" placeholder=""></asp:TextBox>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSubAddresidence" runat="server" Text="Agregar" class="btn btn-large btn-link"
                                            type="submit" />
                                    </td>
                                </tr>
                            </table>
                            <div class="control-group">
                                <asp:GridView ID="grvSubResidences" runat="server" Width="100%" class="table-striped">
                                </asp:GridView>
                            </div>
                        </fieldset>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="Rotation" class="tab-pane">
                <fieldset>
                    <legend>Rotaciones</legend>
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <div class="control-group">
                                    <label>
                                        Departamento</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlDepartments" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="control-group">
                                    <label>
                                        Fecha de Inicio</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtStartDate" runat="server" CssClass="textEntry" placeholder=""></asp:TextBox>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="control-group">
                                    <label>
                                        Fecha de Fin</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtEndDate" runat="server" CssClass="textEntry" placeholder=""></asp:TextBox>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="control-group">
                        <asp:GridView ID="grvRotations" runat="server" Width="100%" class="table-striped">
                        </asp:GridView>
                    </div>
                </fieldset>
            </div>
            <div id="EmergencyDate" class="tab-pane">
                <fieldset>
                    <legend>Datos de Emergencia</legend>
                    <div class="control-group">
                        <label>
                            Primer Nombre</label>
                        <div class="controls">
                            <asp:TextBox ID="TextBox1" runat="server" CssClass="textEntry" placeholder="Primer Nombre"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                                CssClass="failureNotification" ErrorMessage="Nombre es requerido." ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="control-group">
                        <label>
                            Segundo Nombre</label>
                        <div class="controls">
                            <asp:TextBox ID="TextBox2" runat="server" CssClass="textEntry" placeholder="Segundo Nombre"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtMiddleName"
                                CssClass="failureNotification" ErrorMessage="Segundo Nombre es requerido." ValidationGroup="ResidentValidationGroup">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="control-group">
                        <label>
                            Primer Apellido</label>
                        <div class="controls">
                            <asp:TextBox ID="TextBox3" runat="server" CssClass="textEntry" placeholder="Primer Apellido"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label>
                            Segundo Apellido</label>
                        <div class="controls">
                            <asp:TextBox ID="TextBox4" runat="server" CssClass="textEntry" placeholder="Segundo Apellido"></asp:TextBox>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Informacón Médica</legend>
                    <div class="control-group">
                        <label>
                            Alergías</label>
                        <div class="controls">
                            <asp:TextBox ID="txtAllergies" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label>
                            Medicamentos</label>
                        <div class="controls">
                            <asp:TextBox ID="txtDrugs" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
<%--    <script type="text/javascript">
        function comprueba() {
            return; // alert("Acción");
        }
    </script>--%>
</asp:Content>
