/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 11/18/2013 8:05:26 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class SysUserAccess
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[sys_UserAccess]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _userId;
	private System.Int32? _userCenterId;
	private System.Int32? _centerId;
	private System.Int32? _teachingYearId;
	private System.DateTime? _startDate;
	private System.DateTime? _endDate;
	
	#endregion
	
	
	#region Properties
	public System.Int32? UserId
	{
		get
		{
			return _userId;
		}
		set
		{
			_userId = value;
		}
	}
	
	public System.Int32? UserCenterId
	{
		get
		{
			return _userCenterId;
		}
		set
		{
			_userCenterId = value;
		}
	}
	
	public System.Int32? CenterId
	{
		get
		{
			return _centerId;
		}
		set
		{
			_centerId = value;
		}
	}
	
	public System.Int32? TeachingYearId
	{
		get
		{
			return _teachingYearId;
		}
		set
		{
			_teachingYearId = value;
		}
	}
	
	public System.DateTime? StartDate
	{
		get
		{
			return _startDate;
		}
		set
		{
			_startDate = value;
		}
	}
	
	public System.DateTime? EndDate
	{
		get
		{
			return _endDate;
		}
		set
		{
			_endDate = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("UserId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("UserCenterId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("TeachingYearId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("StartDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("EndDate", typeof(System.DateTime) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (UserId == null)
		dr["UserId"] = DBNull.Value;
		else
		dr["UserId"] = UserId;
		
		if (UserCenterId == null)
		dr["UserCenterId"] = DBNull.Value;
		else
		dr["UserCenterId"] = UserCenterId;
		
		if (CenterId == null)
		dr["CenterId"] = DBNull.Value;
		else
		dr["CenterId"] = CenterId;
		
		if (TeachingYearId == null)
		dr["TeachingYearId"] = DBNull.Value;
		else
		dr["TeachingYearId"] = TeachingYearId;
		
		if (StartDate == null)
		dr["StartDate"] = DBNull.Value;
		else
		dr["StartDate"] = StartDate;
		
		if (EndDate == null)
		dr["EndDate"] = DBNull.Value;
		else
		dr["EndDate"] = EndDate;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		UserId = dr["UserId"] != DBNull.Value ? Convert.ToInt32(dr["UserId"]) : UserId = null;
		UserCenterId = dr["UserCenterId"] != DBNull.Value ? Convert.ToInt32(dr["UserCenterId"]) : UserCenterId = null;
		CenterId = dr["CenterId"] != DBNull.Value ? Convert.ToInt32(dr["CenterId"]) : CenterId = null;
		TeachingYearId = dr["TeachingYearId"] != DBNull.Value ? Convert.ToInt32(dr["TeachingYearId"]) : TeachingYearId = null;
		StartDate = dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : StartDate = null;
		EndDate = dr["EndDate"] != DBNull.Value ? Convert.ToDateTime(dr["EndDate"]) : EndDate = null;
	}
	
	public static SysUserAccess[] MapFrom(DataSet ds)
	{
		List<SysUserAccess> objects;
		
		
		// Initialise Collection.
		objects = new List<SysUserAccess>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[sys_UserAccess] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[sys_UserAccess] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			SysUserAccess instance = new SysUserAccess();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	
	#region INSERT
	public void Insert(System.Int32? userId, System.Int32? userCenterId, System.Int32? centerId, System.Int32? teachingYearId, System.DateTime? startDate, System.DateTime? endDate, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysUserAccess_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, userId, userCenterId, centerId, teachingYearId, startDate, endDate);
		
		if (transaction == null)
		db.ExecuteScalar(dbCommand);
		else
		db.ExecuteScalar(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? userId, System.Int32? userCenterId, System.Int32? centerId, System.Int32? teachingYearId, System.DateTime? startDate, System.DateTime? endDate)
	{
		Insert(userId, userCenterId, centerId, teachingYearId, startDate, endDate, null);
	}
	/// <summary>
	/// Insert current SysUserAccess to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(UserId, UserCenterId, CenterId, TeachingYearId, StartDate, EndDate, transaction);
	}
	
	/// <summary>
	/// Insert current SysUserAccess to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysUserAccess[] Search(System.Int32? userId, System.Int32? userCenterId, System.Int32? centerId, System.Int32? teachingYearId, System.DateTime? startDate, System.DateTime? endDate)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysUserAccess_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, userId, userCenterId, centerId, teachingYearId, startDate, endDate);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return SysUserAccess.MapFrom(ds);
	}
	
	
	public static SysUserAccess[] Search(SysUserAccess searchObject)
	{
		return Search ( searchObject.UserId, searchObject.UserCenterId, searchObject.CenterId, searchObject.TeachingYearId, searchObject.StartDate, searchObject.EndDate);
	}
	
	/// <summary>
	/// Returns all SysUserAccess objects.
	/// </summary>
	/// <returns>List of all SysUserAccess objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static SysUserAccess[] Search()
	{
		return Search ( null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

