/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 03/07/2014 7:44:18 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class SysScreens
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[sys_Screens]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _screenId;
	private System.Int32? _moduleId;
	private System.Int32? _screenParentId;
	private System.String _screenName;
	private System.String _screenUrl;
	private System.Int32? _orderInModule;
	private System.Boolean? _screenStatus;
	
	#endregion
	
	
	#region Properties
	public System.Int32? ScreenId
	{
		get
		{
			return _screenId;
		}
		set
		{
			_screenId = value;
		}
	}
	
	public System.Int32? ModuleId
	{
		get
		{
			return _moduleId;
		}
		set
		{
			_moduleId = value;
		}
	}
	
	public System.Int32? ScreenParentId
	{
		get
		{
			return _screenParentId;
		}
		set
		{
			_screenParentId = value;
		}
	}
	
	public System.String ScreenName
	{
		get
		{
			return _screenName;
		}
		set
		{
			_screenName = value;
		}
	}
	
	public System.String ScreenUrl
	{
		get
		{
			return _screenUrl;
		}
		set
		{
			_screenUrl = value;
		}
	}
	
	public System.Int32? OrderInModule
	{
		get
		{
			return _orderInModule;
		}
		set
		{
			_orderInModule = value;
		}
	}
	
	public System.Boolean? ScreenStatus
	{
		get
		{
			return _screenStatus;
		}
		set
		{
			_screenStatus = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("ScreenId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModuleId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ScreenParentId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ScreenName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("ScreenUrl", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("OrderInModule", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ScreenStatus", typeof(System.Boolean) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (ScreenId == null)
		dr["ScreenId"] = DBNull.Value;
		else
		dr["ScreenId"] = ScreenId;
		
		if (ModuleId == null)
		dr["ModuleId"] = DBNull.Value;
		else
		dr["ModuleId"] = ModuleId;
		
		if (ScreenParentId == null)
		dr["ScreenParentId"] = DBNull.Value;
		else
		dr["ScreenParentId"] = ScreenParentId;
		
		if (ScreenName == null)
		dr["ScreenName"] = DBNull.Value;
		else
		dr["ScreenName"] = ScreenName;
		
		if (ScreenUrl == null)
		dr["ScreenUrl"] = DBNull.Value;
		else
		dr["ScreenUrl"] = ScreenUrl;
		
		if (OrderInModule == null)
		dr["OrderInModule"] = DBNull.Value;
		else
		dr["OrderInModule"] = OrderInModule;
		
		if (ScreenStatus == null)
		dr["ScreenStatus"] = DBNull.Value;
		else
		dr["ScreenStatus"] = ScreenStatus;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		ScreenId = dr["ScreenId"] != DBNull.Value ? Convert.ToInt32(dr["ScreenId"]) : ScreenId = null;
		ModuleId = dr["ModuleId"] != DBNull.Value ? Convert.ToInt32(dr["ModuleId"]) : ModuleId = null;
		ScreenParentId = dr["ScreenParentId"] != DBNull.Value ? Convert.ToInt32(dr["ScreenParentId"]) : ScreenParentId = null;
		ScreenName = dr["ScreenName"] != DBNull.Value ? Convert.ToString(dr["ScreenName"]) : ScreenName = null;
		ScreenUrl = dr["ScreenUrl"] != DBNull.Value ? Convert.ToString(dr["ScreenUrl"]) : ScreenUrl = null;
		OrderInModule = dr["OrderInModule"] != DBNull.Value ? Convert.ToInt32(dr["OrderInModule"]) : OrderInModule = null;
		ScreenStatus = dr["ScreenStatus"] != DBNull.Value ? Convert.ToBoolean(dr["ScreenStatus"]) : ScreenStatus = null;
	}
	
	public static SysScreens[] MapFrom(DataSet ds)
	{
		List<SysScreens> objects;
		
		
		// Initialise Collection.
		objects = new List<SysScreens>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[sys_Screens] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[sys_Screens] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			SysScreens instance = new SysScreens();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysScreens Get(System.Int32 screenId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		SysScreens instance;
		
		
		instance = new SysScreens();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysScreens_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, screenId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysScreens ID:" + screenId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.Int32? moduleId, System.Int32? screenParentId, System.String screenName, System.String screenUrl, System.Int32? orderInModule, System.Boolean? screenStatus, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysScreens_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, moduleId, screenParentId, screenName, screenUrl, orderInModule, screenStatus);
		
		if (transaction == null)
		this.ScreenId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.ScreenId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? moduleId, System.Int32? screenParentId, System.String screenName, System.String screenUrl, System.Int32? orderInModule, System.Boolean? screenStatus)
	{
		Insert(moduleId, screenParentId, screenName, screenUrl, orderInModule, screenStatus, null);
	}
	/// <summary>
	/// Insert current SysScreens to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(ModuleId, ScreenParentId, ScreenName, ScreenUrl, OrderInModule, ScreenStatus, transaction);
	}
	
	/// <summary>
	/// Insert current SysScreens to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? screenId, System.Int32? moduleId, System.Int32? screenParentId, System.String screenName, System.String screenUrl, System.Int32? orderInModule, System.Boolean? screenStatus, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysScreens_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@screenId"].Value = screenId;
		dbCommand.Parameters["@moduleId"].Value = moduleId;
		dbCommand.Parameters["@screenParentId"].Value = screenParentId;
		dbCommand.Parameters["@screenName"].Value = screenName;
		dbCommand.Parameters["@screenUrl"].Value = screenUrl;
		dbCommand.Parameters["@orderInModule"].Value = orderInModule;
		dbCommand.Parameters["@screenStatus"].Value = screenStatus;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? screenId, System.Int32? moduleId, System.Int32? screenParentId, System.String screenName, System.String screenUrl, System.Int32? orderInModule, System.Boolean? screenStatus)
	{
		Update(screenId, moduleId, screenParentId, screenName, screenUrl, orderInModule, screenStatus, null);
	}
	
	public static void Update(SysScreens sysScreens)
	{
		sysScreens.Update();
	}
	
	public static void Update(SysScreens sysScreens, DbTransaction transaction)
	{
		sysScreens.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysScreens_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@screenId"].SourceColumn = "ScreenId";
		dbCommand.Parameters["@moduleId"].SourceColumn = "ModuleId";
		dbCommand.Parameters["@screenParentId"].SourceColumn = "ScreenParentId";
		dbCommand.Parameters["@screenName"].SourceColumn = "ScreenName";
		dbCommand.Parameters["@screenUrl"].SourceColumn = "ScreenUrl";
		dbCommand.Parameters["@orderInModule"].SourceColumn = "OrderInModule";
		dbCommand.Parameters["@screenStatus"].SourceColumn = "ScreenStatus";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? screenId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysScreens_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, screenId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? screenId)
	{
		Delete(
		screenId);
	}
	
	/// <summary>
	/// Delete current SysScreens from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysScreens_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, ScreenId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.ScreenId = null;
	}
	
	/// <summary>
	/// Delete current SysScreens from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysScreens[] Search(System.Int32? screenId, System.Int32? moduleId, System.Int32? screenParentId, System.String screenName, System.String screenUrl, System.Int32? orderInModule, System.Boolean? screenStatus)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysScreens_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, screenId, moduleId, screenParentId, screenName, screenUrl, orderInModule, screenStatus);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return SysScreens.MapFrom(ds);
	}
	
	
	public static SysScreens[] Search(SysScreens searchObject)
	{
		return Search ( searchObject.ScreenId, searchObject.ModuleId, searchObject.ScreenParentId, searchObject.ScreenName, searchObject.ScreenUrl, searchObject.OrderInModule, searchObject.ScreenStatus);
	}
	
	/// <summary>
	/// Returns all SysScreens objects.
	/// </summary>
	/// <returns>List of all SysScreens objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static SysScreens[] Search()
	{
		return Search ( null, null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

