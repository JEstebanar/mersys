/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 11/24/2013 3:04:36 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class EvaEvaluationDetails
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[eva_EvaluationDetails]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _detailId;
	private System.Int32? _evaluationId;
	private System.String _detailName;
	private System.Int32? _detailMaxPoints;
	private System.Boolean? _detailStatus;
	private System.Int32? _centerId;
	private System.Int32? _createdBy;
	private System.DateTime? _createdDate;
	private System.Int32? _modifiedBy;
	private System.DateTime? _modifiedDate;
	
	#endregion
	
	
	#region Properties
	public System.Int32? DetailId
	{
		get
		{
			return _detailId;
		}
		set
		{
			_detailId = value;
		}
	}
	
	public System.Int32? EvaluationId
	{
		get
		{
			return _evaluationId;
		}
		set
		{
			_evaluationId = value;
		}
	}
	
	public System.String DetailName
	{
		get
		{
			return _detailName;
		}
		set
		{
			_detailName = value;
		}
	}
	
	public System.Int32? DetailMaxPoints
	{
		get
		{
			return _detailMaxPoints;
		}
		set
		{
			_detailMaxPoints = value;
		}
	}
	
	public System.Boolean? DetailStatus
	{
		get
		{
			return _detailStatus;
		}
		set
		{
			_detailStatus = value;
		}
	}
	
	public System.Int32? CenterId
	{
		get
		{
			return _centerId;
		}
		set
		{
			_centerId = value;
		}
	}
	
	public System.Int32? CreatedBy
	{
		get
		{
			return _createdBy;
		}
		set
		{
			_createdBy = value;
		}
	}
	
	public System.DateTime? CreatedDate
	{
		get
		{
			return _createdDate;
		}
		set
		{
			_createdDate = value;
		}
	}
	
	public System.Int32? ModifiedBy
	{
		get
		{
			return _modifiedBy;
		}
		set
		{
			_modifiedBy = value;
		}
	}
	
	public System.DateTime? ModifiedDate
	{
		get
		{
			return _modifiedDate;
		}
		set
		{
			_modifiedDate = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("DetailId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("EvaluationId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("DetailName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("DetailMaxPoints", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("DetailStatus", typeof(System.Boolean) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (DetailId == null)
		dr["DetailId"] = DBNull.Value;
		else
		dr["DetailId"] = DetailId;
		
		if (EvaluationId == null)
		dr["EvaluationId"] = DBNull.Value;
		else
		dr["EvaluationId"] = EvaluationId;
		
		if (DetailName == null)
		dr["DetailName"] = DBNull.Value;
		else
		dr["DetailName"] = DetailName;
		
		if (DetailMaxPoints == null)
		dr["DetailMaxPoints"] = DBNull.Value;
		else
		dr["DetailMaxPoints"] = DetailMaxPoints;
		
		if (DetailStatus == null)
		dr["DetailStatus"] = DBNull.Value;
		else
		dr["DetailStatus"] = DetailStatus;
		
		if (CenterId == null)
		dr["CenterId"] = DBNull.Value;
		else
		dr["CenterId"] = CenterId;
		
		if (CreatedBy == null)
		dr["CreatedBy"] = DBNull.Value;
		else
		dr["CreatedBy"] = CreatedBy;
		
		if (CreatedDate == null)
		dr["CreatedDate"] = DBNull.Value;
		else
		dr["CreatedDate"] = CreatedDate;
		
		if (ModifiedBy == null)
		dr["ModifiedBy"] = DBNull.Value;
		else
		dr["ModifiedBy"] = ModifiedBy;
		
		if (ModifiedDate == null)
		dr["ModifiedDate"] = DBNull.Value;
		else
		dr["ModifiedDate"] = ModifiedDate;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		DetailId = dr["DetailId"] != DBNull.Value ? Convert.ToInt32(dr["DetailId"]) : DetailId = null;
		EvaluationId = dr["EvaluationId"] != DBNull.Value ? Convert.ToInt32(dr["EvaluationId"]) : EvaluationId = null;
		DetailName = dr["DetailName"] != DBNull.Value ? Convert.ToString(dr["DetailName"]) : DetailName = null;
		DetailMaxPoints = dr["DetailMaxPoints"] != DBNull.Value ? Convert.ToInt32(dr["DetailMaxPoints"]) : DetailMaxPoints = null;
		DetailStatus = dr["DetailStatus"] != DBNull.Value ? Convert.ToBoolean(dr["DetailStatus"]) : DetailStatus = null;
		CenterId = dr["CenterId"] != DBNull.Value ? Convert.ToInt32(dr["CenterId"]) : CenterId = null;
		CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : CreatedBy = null;
		CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
		ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
		ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
	}
	
	public static EvaEvaluationDetails[] MapFrom(DataSet ds)
	{
		List<EvaEvaluationDetails> objects;
		
		
		// Initialise Collection.
		objects = new List<EvaEvaluationDetails>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[eva_EvaluationDetails] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[eva_EvaluationDetails] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			EvaEvaluationDetails instance = new EvaEvaluationDetails();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static EvaEvaluationDetails Get(System.Int32 detailId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		EvaEvaluationDetails instance;
		
		
		instance = new EvaEvaluationDetails();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluationDetails_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, detailId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get EvaEvaluationDetails ID:" + detailId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.Int32? evaluationId, System.String detailName, System.Int32? detailMaxPoints, System.Boolean? detailStatus, System.Int32? centerId, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluationDetails_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, evaluationId, detailName, detailMaxPoints, detailStatus, centerId, createdBy, createdDate, modifiedBy, modifiedDate);
		
		if (transaction == null)
		this.DetailId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.DetailId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? evaluationId, System.String detailName, System.Int32? detailMaxPoints, System.Boolean? detailStatus, System.Int32? centerId, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Insert(evaluationId, detailName, detailMaxPoints, detailStatus, centerId, createdBy, createdDate, modifiedBy, modifiedDate, null);
	}
	/// <summary>
	/// Insert current EvaEvaluationDetails to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(EvaluationId, DetailName, DetailMaxPoints, DetailStatus, CenterId, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, transaction);
	}
	
	/// <summary>
	/// Insert current EvaEvaluationDetails to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? detailId, System.Int32? evaluationId, System.String detailName, System.Int32? detailMaxPoints, System.Boolean? detailStatus, System.Int32? centerId, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluationDetails_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@detailId"].Value = detailId;
		dbCommand.Parameters["@evaluationId"].Value = evaluationId;
		dbCommand.Parameters["@detailName"].Value = detailName;
		dbCommand.Parameters["@detailMaxPoints"].Value = detailMaxPoints;
		dbCommand.Parameters["@detailStatus"].Value = detailStatus;
		dbCommand.Parameters["@centerId"].Value = centerId;
		dbCommand.Parameters["@createdBy"].Value = createdBy;
		dbCommand.Parameters["@createdDate"].Value = createdDate;
		dbCommand.Parameters["@modifiedBy"].Value = modifiedBy;
		dbCommand.Parameters["@modifiedDate"].Value = modifiedDate;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? detailId, System.Int32? evaluationId, System.String detailName, System.Int32? detailMaxPoints, System.Boolean? detailStatus, System.Int32? centerId, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Update(detailId, evaluationId, detailName, detailMaxPoints, detailStatus, centerId, createdBy, createdDate, modifiedBy, modifiedDate, null);
	}
	
	public static void Update(EvaEvaluationDetails evaEvaluationDetails)
	{
		evaEvaluationDetails.Update();
	}
	
	public static void Update(EvaEvaluationDetails evaEvaluationDetails, DbTransaction transaction)
	{
		evaEvaluationDetails.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluationDetails_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@detailId"].SourceColumn = "DetailId";
		dbCommand.Parameters["@evaluationId"].SourceColumn = "EvaluationId";
		dbCommand.Parameters["@detailName"].SourceColumn = "DetailName";
		dbCommand.Parameters["@detailMaxPoints"].SourceColumn = "DetailMaxPoints";
		dbCommand.Parameters["@detailStatus"].SourceColumn = "DetailStatus";
		dbCommand.Parameters["@centerId"].SourceColumn = "CenterId";
		dbCommand.Parameters["@createdBy"].SourceColumn = "CreatedBy";
		dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
		dbCommand.Parameters["@modifiedBy"].SourceColumn = "ModifiedBy";
		dbCommand.Parameters["@modifiedDate"].SourceColumn = "ModifiedDate";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? detailId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluationDetails_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, detailId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? detailId)
	{
		Delete(
		detailId);
	}
	
	/// <summary>
	/// Delete current EvaEvaluationDetails from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluationDetails_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, DetailId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.DetailId = null;
	}
	
	/// <summary>
	/// Delete current EvaEvaluationDetails from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static EvaEvaluationDetails[] Search(System.Int32? detailId, System.Int32? evaluationId, System.String detailName, System.Int32? detailMaxPoints, System.Boolean? detailStatus, System.Int32? centerId, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluationDetails_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, detailId, evaluationId, detailName, detailMaxPoints, detailStatus, centerId, createdBy, createdDate, modifiedBy, modifiedDate);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return EvaEvaluationDetails.MapFrom(ds);
	}
	
	
	public static EvaEvaluationDetails[] Search(EvaEvaluationDetails searchObject)
	{
		return Search ( searchObject.DetailId, searchObject.EvaluationId, searchObject.DetailName, searchObject.DetailMaxPoints, searchObject.DetailStatus, searchObject.CenterId, searchObject.CreatedBy, searchObject.CreatedDate, searchObject.ModifiedBy, searchObject.ModifiedDate);
	}
	
	/// <summary>
	/// Returns all EvaEvaluationDetails objects.
	/// </summary>
	/// <returns>List of all EvaEvaluationDetails objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static EvaEvaluationDetails[] Search()
	{
		return Search ( null, null, null, null, null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

