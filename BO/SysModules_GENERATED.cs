/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 03/07/2014 7:44:18 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class SysModules
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[sys_Modules]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _moduleId;
	private System.String _moduleName;
	private System.String _moduleDescription;
	private System.Int32? _moduleOrder;
	private System.Boolean? _moduleStatus;
	
	#endregion
	
	
	#region Properties
	public System.Int32? ModuleId
	{
		get
		{
			return _moduleId;
		}
		set
		{
			_moduleId = value;
		}
	}
	
	public System.String ModuleName
	{
		get
		{
			return _moduleName;
		}
		set
		{
			_moduleName = value;
		}
	}
	
	public System.String ModuleDescription
	{
		get
		{
			return _moduleDescription;
		}
		set
		{
			_moduleDescription = value;
		}
	}
	
	public System.Int32? ModuleOrder
	{
		get
		{
			return _moduleOrder;
		}
		set
		{
			_moduleOrder = value;
		}
	}
	
	public System.Boolean? ModuleStatus
	{
		get
		{
			return _moduleStatus;
		}
		set
		{
			_moduleStatus = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("ModuleId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModuleName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("ModuleDescription", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("ModuleOrder", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModuleStatus", typeof(System.Boolean) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (ModuleId == null)
		dr["ModuleId"] = DBNull.Value;
		else
		dr["ModuleId"] = ModuleId;
		
		if (ModuleName == null)
		dr["ModuleName"] = DBNull.Value;
		else
		dr["ModuleName"] = ModuleName;
		
		if (ModuleDescription == null)
		dr["ModuleDescription"] = DBNull.Value;
		else
		dr["ModuleDescription"] = ModuleDescription;
		
		if (ModuleOrder == null)
		dr["ModuleOrder"] = DBNull.Value;
		else
		dr["ModuleOrder"] = ModuleOrder;
		
		if (ModuleStatus == null)
		dr["ModuleStatus"] = DBNull.Value;
		else
		dr["ModuleStatus"] = ModuleStatus;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		ModuleId = dr["ModuleId"] != DBNull.Value ? Convert.ToInt32(dr["ModuleId"]) : ModuleId = null;
		ModuleName = dr["ModuleName"] != DBNull.Value ? Convert.ToString(dr["ModuleName"]) : ModuleName = null;
		ModuleDescription = dr["ModuleDescription"] != DBNull.Value ? Convert.ToString(dr["ModuleDescription"]) : ModuleDescription = null;
		ModuleOrder = dr["ModuleOrder"] != DBNull.Value ? Convert.ToInt32(dr["ModuleOrder"]) : ModuleOrder = null;
		ModuleStatus = dr["ModuleStatus"] != DBNull.Value ? Convert.ToBoolean(dr["ModuleStatus"]) : ModuleStatus = null;
	}
	
	public static SysModules[] MapFrom(DataSet ds)
	{
		List<SysModules> objects;
		
		
		// Initialise Collection.
		objects = new List<SysModules>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[sys_Modules] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[sys_Modules] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			SysModules instance = new SysModules();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysModules Get(System.Int32 moduleId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		SysModules instance;
		
		
		instance = new SysModules();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysModules_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, moduleId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysModules ID:" + moduleId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.String moduleName, System.String moduleDescription, System.Int32? moduleOrder, System.Boolean? moduleStatus, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysModules_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, moduleName, moduleDescription, moduleOrder, moduleStatus);
		
		if (transaction == null)
		this.ModuleId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.ModuleId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.String moduleName, System.String moduleDescription, System.Int32? moduleOrder, System.Boolean? moduleStatus)
	{
		Insert(moduleName, moduleDescription, moduleOrder, moduleStatus, null);
	}
	/// <summary>
	/// Insert current SysModules to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(ModuleName, ModuleDescription, ModuleOrder, ModuleStatus, transaction);
	}
	
	/// <summary>
	/// Insert current SysModules to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? moduleId, System.String moduleName, System.String moduleDescription, System.Int32? moduleOrder, System.Boolean? moduleStatus, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysModules_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@moduleId"].Value = moduleId;
		dbCommand.Parameters["@moduleName"].Value = moduleName;
		dbCommand.Parameters["@moduleDescription"].Value = moduleDescription;
		dbCommand.Parameters["@moduleOrder"].Value = moduleOrder;
		dbCommand.Parameters["@moduleStatus"].Value = moduleStatus;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? moduleId, System.String moduleName, System.String moduleDescription, System.Int32? moduleOrder, System.Boolean? moduleStatus)
	{
		Update(moduleId, moduleName, moduleDescription, moduleOrder, moduleStatus, null);
	}
	
	public static void Update(SysModules sysModules)
	{
		sysModules.Update();
	}
	
	public static void Update(SysModules sysModules, DbTransaction transaction)
	{
		sysModules.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysModules_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@moduleId"].SourceColumn = "ModuleId";
		dbCommand.Parameters["@moduleName"].SourceColumn = "ModuleName";
		dbCommand.Parameters["@moduleDescription"].SourceColumn = "ModuleDescription";
		dbCommand.Parameters["@moduleOrder"].SourceColumn = "ModuleOrder";
		dbCommand.Parameters["@moduleStatus"].SourceColumn = "ModuleStatus";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? moduleId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysModules_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, moduleId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? moduleId)
	{
		Delete(
		moduleId);
	}
	
	/// <summary>
	/// Delete current SysModules from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysModules_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, ModuleId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.ModuleId = null;
	}
	
	/// <summary>
	/// Delete current SysModules from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysModules[] Search(System.Int32? moduleId, System.String moduleName, System.String moduleDescription, System.Int32? moduleOrder, System.Boolean? moduleStatus)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysModules_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, moduleId, moduleName, moduleDescription, moduleOrder, moduleStatus);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return SysModules.MapFrom(ds);
	}
	
	
	public static SysModules[] Search(SysModules searchObject)
	{
		return Search ( searchObject.ModuleId, searchObject.ModuleName, searchObject.ModuleDescription, searchObject.ModuleOrder, searchObject.ModuleStatus);
	}
	
	/// <summary>
	/// Returns all SysModules objects.
	/// </summary>
	/// <returns>List of all SysModules objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static SysModules[] Search()
	{
		return Search ( null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

