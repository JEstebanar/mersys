/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 11/21/2013 10:25:05 AM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class EvaScoreActions
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[eva_ScoreActions]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _actionId;
	private System.Int32? _scoreId;
	private System.Int32? _monthNumber;
	private System.Decimal? _score;
	private System.Int32? _actionTypeId;
	private System.Int32? _createdBy;
	private System.DateTime? _createdDate;
	
	#endregion
	
	
	#region Properties
	public System.Int32? ActionId
	{
		get
		{
			return _actionId;
		}
		set
		{
			_actionId = value;
		}
	}
	
	public System.Int32? ScoreId
	{
		get
		{
			return _scoreId;
		}
		set
		{
			_scoreId = value;
		}
	}
	
	public System.Int32? MonthNumber
	{
		get
		{
			return _monthNumber;
		}
		set
		{
			_monthNumber = value;
		}
	}
	
	public System.Decimal? Score
	{
		get
		{
			return _score;
		}
		set
		{
			_score = value;
		}
	}
	
	public System.Int32? ActionTypeId
	{
		get
		{
			return _actionTypeId;
		}
		set
		{
			_actionTypeId = value;
		}
	}
	
	public System.Int32? CreatedBy
	{
		get
		{
			return _createdBy;
		}
		set
		{
			_createdBy = value;
		}
	}
	
	public System.DateTime? CreatedDate
	{
		get
		{
			return _createdDate;
		}
		set
		{
			_createdDate = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("ActionId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ScoreId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("MonthNumber", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("Score", typeof(System.Decimal) );
		ds.Tables[TABLE_NAME].Columns.Add("ActionTypeId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (ActionId == null)
		dr["ActionId"] = DBNull.Value;
		else
		dr["ActionId"] = ActionId;
		
		if (ScoreId == null)
		dr["ScoreId"] = DBNull.Value;
		else
		dr["ScoreId"] = ScoreId;
		
		if (MonthNumber == null)
		dr["MonthNumber"] = DBNull.Value;
		else
		dr["MonthNumber"] = MonthNumber;
		
		if (Score == null)
		dr["Score"] = DBNull.Value;
		else
		dr["Score"] = Score;
		
		if (ActionTypeId == null)
		dr["ActionTypeId"] = DBNull.Value;
		else
		dr["ActionTypeId"] = ActionTypeId;
		
		if (CreatedBy == null)
		dr["CreatedBy"] = DBNull.Value;
		else
		dr["CreatedBy"] = CreatedBy;
		
		if (CreatedDate == null)
		dr["CreatedDate"] = DBNull.Value;
		else
		dr["CreatedDate"] = CreatedDate;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		ActionId = dr["ActionId"] != DBNull.Value ? Convert.ToInt32(dr["ActionId"]) : ActionId = null;
		ScoreId = dr["ScoreId"] != DBNull.Value ? Convert.ToInt32(dr["ScoreId"]) : ScoreId = null;
		MonthNumber = dr["MonthNumber"] != DBNull.Value ? Convert.ToInt32(dr["MonthNumber"]) : MonthNumber = null;
		Score = dr["Score"] != DBNull.Value ? Convert.ToDecimal(dr["Score"]) : Score = null;
		ActionTypeId = dr["ActionTypeId"] != DBNull.Value ? Convert.ToInt32(dr["ActionTypeId"]) : ActionTypeId = null;
		CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : CreatedBy = null;
		CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
	}
	
	public static EvaScoreActions[] MapFrom(DataSet ds)
	{
		List<EvaScoreActions> objects;
		
		
		// Initialise Collection.
		objects = new List<EvaScoreActions>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[eva_ScoreActions] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[eva_ScoreActions] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			EvaScoreActions instance = new EvaScoreActions();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static EvaScoreActions Get(System.Int32 actionId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		EvaScoreActions instance;
		
		
		instance = new EvaScoreActions();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaScoreActions_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, actionId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get EvaScoreActions ID:" + actionId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.Int32? scoreId, System.Int32? monthNumber, System.Decimal? score, System.Int32? actionTypeId, System.Int32? createdBy, System.DateTime? createdDate, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaScoreActions_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, scoreId, monthNumber, score, actionTypeId, createdBy, createdDate);
		
		if (transaction == null)
		this.ActionId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.ActionId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? scoreId, System.Int32? monthNumber, System.Decimal? score, System.Int32? actionTypeId, System.Int32? createdBy, System.DateTime? createdDate)
	{
		Insert(scoreId, monthNumber, score, actionTypeId, createdBy, createdDate, null);
	}
	/// <summary>
	/// Insert current EvaScoreActions to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(ScoreId, MonthNumber, Score, ActionTypeId, CreatedBy, CreatedDate, transaction);
	}
	
	/// <summary>
	/// Insert current EvaScoreActions to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? actionId, System.Int32? scoreId, System.Int32? monthNumber, System.Decimal? score, System.Int32? actionTypeId, System.Int32? createdBy, System.DateTime? createdDate, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaScoreActions_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@actionId"].Value = actionId;
		dbCommand.Parameters["@scoreId"].Value = scoreId;
		dbCommand.Parameters["@monthNumber"].Value = monthNumber;
		dbCommand.Parameters["@score"].Value = score;
		dbCommand.Parameters["@actionTypeId"].Value = actionTypeId;
		dbCommand.Parameters["@createdBy"].Value = createdBy;
		dbCommand.Parameters["@createdDate"].Value = createdDate;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? actionId, System.Int32? scoreId, System.Int32? monthNumber, System.Decimal? score, System.Int32? actionTypeId, System.Int32? createdBy, System.DateTime? createdDate)
	{
		Update(actionId, scoreId, monthNumber, score, actionTypeId, createdBy, createdDate, null);
	}
	
	public static void Update(EvaScoreActions evaScoreActions)
	{
		evaScoreActions.Update();
	}
	
	public static void Update(EvaScoreActions evaScoreActions, DbTransaction transaction)
	{
		evaScoreActions.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaScoreActions_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@actionId"].SourceColumn = "ActionId";
		dbCommand.Parameters["@scoreId"].SourceColumn = "ScoreId";
		dbCommand.Parameters["@monthNumber"].SourceColumn = "MonthNumber";
		dbCommand.Parameters["@score"].SourceColumn = "Score";
		dbCommand.Parameters["@actionTypeId"].SourceColumn = "ActionTypeId";
		dbCommand.Parameters["@createdBy"].SourceColumn = "CreatedBy";
		dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? actionId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaScoreActions_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, actionId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? actionId)
	{
		Delete(
		actionId);
	}
	
	/// <summary>
	/// Delete current EvaScoreActions from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaScoreActions_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, ActionId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.ActionId = null;
	}
	
	/// <summary>
	/// Delete current EvaScoreActions from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static EvaScoreActions[] Search(System.Int32? actionId, System.Int32? scoreId, System.Int32? monthNumber, System.Decimal? score, System.Int32? actionTypeId, System.Int32? createdBy, System.DateTime? createdDate)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaScoreActions_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, actionId, scoreId, monthNumber, score, actionTypeId, createdBy, createdDate);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return EvaScoreActions.MapFrom(ds);
	}
	
	
	public static EvaScoreActions[] Search(EvaScoreActions searchObject)
	{
		return Search ( searchObject.ActionId, searchObject.ScoreId, searchObject.MonthNumber, searchObject.Score, searchObject.ActionTypeId, searchObject.CreatedBy, searchObject.CreatedDate);
	}
	
	/// <summary>
	/// Returns all EvaScoreActions objects.
	/// </summary>
	/// <returns>List of all EvaScoreActions objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static EvaScoreActions[] Search()
	{
		return Search ( null, null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

