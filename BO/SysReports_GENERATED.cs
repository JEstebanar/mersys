/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 03/30/2014 5:52:10 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class SysReports
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[sys_Reports]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _reportId;
	private System.String _reportName;
	private System.Int32? _reportOrder;
	private System.Boolean? _reportStatus;
	private System.Int32? _reportTypeId;
	
	#endregion
	
	
	#region Properties
	public System.Int32? ReportId
	{
		get
		{
			return _reportId;
		}
		set
		{
			_reportId = value;
		}
	}
	
	public System.String ReportName
	{
		get
		{
			return _reportName;
		}
		set
		{
			_reportName = value;
		}
	}
	
	public System.Int32? ReportOrder
	{
		get
		{
			return _reportOrder;
		}
		set
		{
			_reportOrder = value;
		}
	}
	
	public System.Boolean? ReportStatus
	{
		get
		{
			return _reportStatus;
		}
		set
		{
			_reportStatus = value;
		}
	}
	
	public System.Int32? ReportTypeId
	{
		get
		{
			return _reportTypeId;
		}
		set
		{
			_reportTypeId = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("ReportId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ReportName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("ReportOrder", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ReportStatus", typeof(System.Boolean) );
		ds.Tables[TABLE_NAME].Columns.Add("ReportTypeId", typeof(System.Int32) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (ReportId == null)
		dr["ReportId"] = DBNull.Value;
		else
		dr["ReportId"] = ReportId;
		
		if (ReportName == null)
		dr["ReportName"] = DBNull.Value;
		else
		dr["ReportName"] = ReportName;
		
		if (ReportOrder == null)
		dr["ReportOrder"] = DBNull.Value;
		else
		dr["ReportOrder"] = ReportOrder;
		
		if (ReportStatus == null)
		dr["ReportStatus"] = DBNull.Value;
		else
		dr["ReportStatus"] = ReportStatus;
		
		if (ReportTypeId == null)
		dr["ReportTypeId"] = DBNull.Value;
		else
		dr["ReportTypeId"] = ReportTypeId;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		ReportId = dr["ReportId"] != DBNull.Value ? Convert.ToInt32(dr["ReportId"]) : ReportId = null;
		ReportName = dr["ReportName"] != DBNull.Value ? Convert.ToString(dr["ReportName"]) : ReportName = null;
		ReportOrder = dr["ReportOrder"] != DBNull.Value ? Convert.ToInt32(dr["ReportOrder"]) : ReportOrder = null;
		ReportStatus = dr["ReportStatus"] != DBNull.Value ? Convert.ToBoolean(dr["ReportStatus"]) : ReportStatus = null;
		ReportTypeId = dr["ReportTypeId"] != DBNull.Value ? Convert.ToInt32(dr["ReportTypeId"]) : ReportTypeId = null;
	}
	
	public static SysReports[] MapFrom(DataSet ds)
	{
		List<SysReports> objects;
		
		
		// Initialise Collection.
		objects = new List<SysReports>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[sys_Reports] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[sys_Reports] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			SysReports instance = new SysReports();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysReports Get(System.Int32 reportId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		SysReports instance;
		
		
		instance = new SysReports();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysReports_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, reportId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysReports ID:" + reportId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.String reportName, System.Int32? reportOrder, System.Boolean? reportStatus, System.Int32? reportTypeId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysReports_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, reportName, reportOrder, reportStatus, reportTypeId);
		
		if (transaction == null)
		this.ReportId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.ReportId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.String reportName, System.Int32? reportOrder, System.Boolean? reportStatus, System.Int32? reportTypeId)
	{
		Insert(reportName, reportOrder, reportStatus, reportTypeId, null);
	}
	/// <summary>
	/// Insert current SysReports to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(ReportName, ReportOrder, ReportStatus, ReportTypeId, transaction);
	}
	
	/// <summary>
	/// Insert current SysReports to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? reportId, System.String reportName, System.Int32? reportOrder, System.Boolean? reportStatus, System.Int32? reportTypeId, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysReports_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@reportId"].Value = reportId;
		dbCommand.Parameters["@reportName"].Value = reportName;
		dbCommand.Parameters["@reportOrder"].Value = reportOrder;
		dbCommand.Parameters["@reportStatus"].Value = reportStatus;
		dbCommand.Parameters["@reportTypeId"].Value = reportTypeId;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? reportId, System.String reportName, System.Int32? reportOrder, System.Boolean? reportStatus, System.Int32? reportTypeId)
	{
		Update(reportId, reportName, reportOrder, reportStatus, reportTypeId, null);
	}
	
	public static void Update(SysReports sysReports)
	{
		sysReports.Update();
	}
	
	public static void Update(SysReports sysReports, DbTransaction transaction)
	{
		sysReports.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysReports_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@reportId"].SourceColumn = "ReportId";
		dbCommand.Parameters["@reportName"].SourceColumn = "ReportName";
		dbCommand.Parameters["@reportOrder"].SourceColumn = "ReportOrder";
		dbCommand.Parameters["@reportStatus"].SourceColumn = "ReportStatus";
		dbCommand.Parameters["@reportTypeId"].SourceColumn = "ReportTypeId";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? reportId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysReports_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, reportId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? reportId)
	{
		Delete(
		reportId);
	}
	
	/// <summary>
	/// Delete current SysReports from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysReports_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, ReportId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.ReportId = null;
	}
	
	/// <summary>
	/// Delete current SysReports from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysReports[] Search(System.Int32? reportId, System.String reportName, System.Int32? reportOrder, System.Boolean? reportStatus, System.Int32? reportTypeId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysReports_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, reportId, reportName, reportOrder, reportStatus, reportTypeId);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return SysReports.MapFrom(ds);
	}
	
	
	public static SysReports[] Search(SysReports searchObject)
	{
		return Search ( searchObject.ReportId, searchObject.ReportName, searchObject.ReportOrder, searchObject.ReportStatus, searchObject.ReportTypeId);
	}
	
	/// <summary>
	/// Returns all SysReports objects.
	/// </summary>
	/// <returns>List of all SysReports objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static SysReports[] Search()
	{
		return Search ( null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

