/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 10/01/2013 10:48:34 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class ResDepartments
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[res_Departments]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _departmentId;
	private System.String _departmentName;
	private System.Boolean? _departmentStatus;
	private System.Int32? _createdBy;
	private System.DateTime? _createdDate;
	private System.Int32? _modifiedBy;
	private System.DateTime? _modifiedDate;
	
	#endregion
	
	
	#region Properties
	public System.Int32? DepartmentId
	{
		get
		{
			return _departmentId;
		}
		set
		{
			_departmentId = value;
		}
	}
	
	public System.String DepartmentName
	{
		get
		{
			return _departmentName;
		}
		set
		{
			_departmentName = value;
		}
	}
	
	public System.Boolean? DepartmentStatus
	{
		get
		{
			return _departmentStatus;
		}
		set
		{
			_departmentStatus = value;
		}
	}
	
	public System.Int32? CreatedBy
	{
		get
		{
			return _createdBy;
		}
		set
		{
			_createdBy = value;
		}
	}
	
	public System.DateTime? CreatedDate
	{
		get
		{
			return _createdDate;
		}
		set
		{
			_createdDate = value;
		}
	}
	
	public System.Int32? ModifiedBy
	{
		get
		{
			return _modifiedBy;
		}
		set
		{
			_modifiedBy = value;
		}
	}
	
	public System.DateTime? ModifiedDate
	{
		get
		{
			return _modifiedDate;
		}
		set
		{
			_modifiedDate = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("DepartmentId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("DepartmentName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("DepartmentStatus", typeof(System.Boolean) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (DepartmentId == null)
		dr["DepartmentId"] = DBNull.Value;
		else
		dr["DepartmentId"] = DepartmentId;
		
		if (DepartmentName == null)
		dr["DepartmentName"] = DBNull.Value;
		else
		dr["DepartmentName"] = DepartmentName;
		
		if (DepartmentStatus == null)
		dr["DepartmentStatus"] = DBNull.Value;
		else
		dr["DepartmentStatus"] = DepartmentStatus;
		
		if (CreatedBy == null)
		dr["CreatedBy"] = DBNull.Value;
		else
		dr["CreatedBy"] = CreatedBy;
		
		if (CreatedDate == null)
		dr["CreatedDate"] = DBNull.Value;
		else
		dr["CreatedDate"] = CreatedDate;
		
		if (ModifiedBy == null)
		dr["ModifiedBy"] = DBNull.Value;
		else
		dr["ModifiedBy"] = ModifiedBy;
		
		if (ModifiedDate == null)
		dr["ModifiedDate"] = DBNull.Value;
		else
		dr["ModifiedDate"] = ModifiedDate;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		DepartmentId = dr["DepartmentId"] != DBNull.Value ? Convert.ToInt32(dr["DepartmentId"]) : DepartmentId = null;
		DepartmentName = dr["DepartmentName"] != DBNull.Value ? Convert.ToString(dr["DepartmentName"]) : DepartmentName = null;
		DepartmentStatus = dr["DepartmentStatus"] != DBNull.Value ? Convert.ToBoolean(dr["DepartmentStatus"]) : DepartmentStatus = null;
		CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : CreatedBy = null;
		CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
		ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
		ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
	}
	
	public static ResDepartments[] MapFrom(DataSet ds)
	{
		List<ResDepartments> objects;
		
		
		// Initialise Collection.
		objects = new List<ResDepartments>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[res_Departments] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[res_Departments] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			ResDepartments instance = new ResDepartments();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static ResDepartments Get(System.Int32 departmentId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		ResDepartments instance;
		
		
		instance = new ResDepartments();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResDepartments_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, departmentId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get ResDepartments ID:" + departmentId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.String departmentName, System.Boolean? departmentStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResDepartments_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, departmentName, departmentStatus, createdBy, createdDate, modifiedBy, modifiedDate);
		
		if (transaction == null)
		this.DepartmentId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.DepartmentId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.String departmentName, System.Boolean? departmentStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Insert(departmentName, departmentStatus, createdBy, createdDate, modifiedBy, modifiedDate, null);
	}
	/// <summary>
	/// Insert current ResDepartments to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(DepartmentName, DepartmentStatus, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, transaction);
	}
	
	/// <summary>
	/// Insert current ResDepartments to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? departmentId, System.String departmentName, System.Boolean? departmentStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResDepartments_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@departmentId"].Value = departmentId;
		dbCommand.Parameters["@departmentName"].Value = departmentName;
		dbCommand.Parameters["@departmentStatus"].Value = departmentStatus;
		dbCommand.Parameters["@createdBy"].Value = createdBy;
		dbCommand.Parameters["@createdDate"].Value = createdDate;
		dbCommand.Parameters["@modifiedBy"].Value = modifiedBy;
		dbCommand.Parameters["@modifiedDate"].Value = modifiedDate;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? departmentId, System.String departmentName, System.Boolean? departmentStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Update(departmentId, departmentName, departmentStatus, createdBy, createdDate, modifiedBy, modifiedDate, null);
	}
	
	public static void Update(ResDepartments resDepartments)
	{
		resDepartments.Update();
	}
	
	public static void Update(ResDepartments resDepartments, DbTransaction transaction)
	{
		resDepartments.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResDepartments_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@departmentId"].SourceColumn = "DepartmentId";
		dbCommand.Parameters["@departmentName"].SourceColumn = "DepartmentName";
		dbCommand.Parameters["@departmentStatus"].SourceColumn = "DepartmentStatus";
		dbCommand.Parameters["@createdBy"].SourceColumn = "CreatedBy";
		dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
		dbCommand.Parameters["@modifiedBy"].SourceColumn = "ModifiedBy";
		dbCommand.Parameters["@modifiedDate"].SourceColumn = "ModifiedDate";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? departmentId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResDepartments_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, departmentId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? departmentId)
	{
		Delete(
		departmentId);
	}
	
	/// <summary>
	/// Delete current ResDepartments from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResDepartments_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, DepartmentId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.DepartmentId = null;
	}
	
	/// <summary>
	/// Delete current ResDepartments from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static ResDepartments[] Search(System.Int32? departmentId, System.String departmentName, System.Boolean? departmentStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResDepartments_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, departmentId, departmentName, departmentStatus, createdBy, createdDate, modifiedBy, modifiedDate);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return ResDepartments.MapFrom(ds);
	}
	
	
	public static ResDepartments[] Search(ResDepartments searchObject)
	{
		return Search ( searchObject.DepartmentId, searchObject.DepartmentName, searchObject.DepartmentStatus, searchObject.CreatedBy, searchObject.CreatedDate, searchObject.ModifiedBy, searchObject.ModifiedDate);
	}
	
	/// <summary>
	/// Returns all ResDepartments objects.
	/// </summary>
	/// <returns>List of all ResDepartments objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static ResDepartments[] Search()
	{
		return Search ( null, null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

