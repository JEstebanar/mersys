/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 03/07/2014 7:44:18 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class SysCenterModules
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[sys_CenterModules]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _centerModuleId;
	private System.Int32? _centerId;
	private System.Int32? _moduleId;
	private System.Boolean? _centerModuleStatus;
	
	#endregion
	
	
	#region Properties
	public System.Int32? CenterModuleId
	{
		get
		{
			return _centerModuleId;
		}
		set
		{
			_centerModuleId = value;
		}
	}
	
	public System.Int32? CenterId
	{
		get
		{
			return _centerId;
		}
		set
		{
			_centerId = value;
		}
	}
	
	public System.Int32? ModuleId
	{
		get
		{
			return _moduleId;
		}
		set
		{
			_moduleId = value;
		}
	}
	
	public System.Boolean? CenterModuleStatus
	{
		get
		{
			return _centerModuleStatus;
		}
		set
		{
			_centerModuleStatus = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("CenterModuleId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModuleId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterModuleStatus", typeof(System.Boolean) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (CenterModuleId == null)
		dr["CenterModuleId"] = DBNull.Value;
		else
		dr["CenterModuleId"] = CenterModuleId;
		
		if (CenterId == null)
		dr["CenterId"] = DBNull.Value;
		else
		dr["CenterId"] = CenterId;
		
		if (ModuleId == null)
		dr["ModuleId"] = DBNull.Value;
		else
		dr["ModuleId"] = ModuleId;
		
		if (CenterModuleStatus == null)
		dr["CenterModuleStatus"] = DBNull.Value;
		else
		dr["CenterModuleStatus"] = CenterModuleStatus;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		CenterModuleId = dr["CenterModuleId"] != DBNull.Value ? Convert.ToInt32(dr["CenterModuleId"]) : CenterModuleId = null;
		CenterId = dr["CenterId"] != DBNull.Value ? Convert.ToInt32(dr["CenterId"]) : CenterId = null;
		ModuleId = dr["ModuleId"] != DBNull.Value ? Convert.ToInt32(dr["ModuleId"]) : ModuleId = null;
		CenterModuleStatus = dr["CenterModuleStatus"] != DBNull.Value ? Convert.ToBoolean(dr["CenterModuleStatus"]) : CenterModuleStatus = null;
	}
	
	public static SysCenterModules[] MapFrom(DataSet ds)
	{
		List<SysCenterModules> objects;
		
		
		// Initialise Collection.
		objects = new List<SysCenterModules>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[sys_CenterModules] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[sys_CenterModules] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			SysCenterModules instance = new SysCenterModules();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysCenterModules Get(System.Int32 centerModuleId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		SysCenterModules instance;
		
		
		instance = new SysCenterModules();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenterModules_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, centerModuleId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysCenterModules ID:" + centerModuleId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.Int32? centerId, System.Int32? moduleId, System.Boolean? centerModuleStatus, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenterModules_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, centerId, moduleId, centerModuleStatus);
		
		if (transaction == null)
		this.CenterModuleId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.CenterModuleId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? centerId, System.Int32? moduleId, System.Boolean? centerModuleStatus)
	{
		Insert(centerId, moduleId, centerModuleStatus, null);
	}
	/// <summary>
	/// Insert current SysCenterModules to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(CenterId, ModuleId, CenterModuleStatus, transaction);
	}
	
	/// <summary>
	/// Insert current SysCenterModules to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? centerModuleId, System.Int32? centerId, System.Int32? moduleId, System.Boolean? centerModuleStatus, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenterModules_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@centerModuleId"].Value = centerModuleId;
		dbCommand.Parameters["@centerId"].Value = centerId;
		dbCommand.Parameters["@moduleId"].Value = moduleId;
		dbCommand.Parameters["@centerModuleStatus"].Value = centerModuleStatus;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? centerModuleId, System.Int32? centerId, System.Int32? moduleId, System.Boolean? centerModuleStatus)
	{
		Update(centerModuleId, centerId, moduleId, centerModuleStatus, null);
	}
	
	public static void Update(SysCenterModules sysCenterModules)
	{
		sysCenterModules.Update();
	}
	
	public static void Update(SysCenterModules sysCenterModules, DbTransaction transaction)
	{
		sysCenterModules.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenterModules_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@centerModuleId"].SourceColumn = "CenterModuleId";
		dbCommand.Parameters["@centerId"].SourceColumn = "CenterId";
		dbCommand.Parameters["@moduleId"].SourceColumn = "ModuleId";
		dbCommand.Parameters["@centerModuleStatus"].SourceColumn = "CenterModuleStatus";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? centerModuleId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenterModules_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, centerModuleId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? centerModuleId)
	{
		Delete(
		centerModuleId);
	}
	
	/// <summary>
	/// Delete current SysCenterModules from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenterModules_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, CenterModuleId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.CenterModuleId = null;
	}
	
	/// <summary>
	/// Delete current SysCenterModules from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysCenterModules[] Search(System.Int32? centerModuleId, System.Int32? centerId, System.Int32? moduleId, System.Boolean? centerModuleStatus)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenterModules_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, centerModuleId, centerId, moduleId, centerModuleStatus);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return SysCenterModules.MapFrom(ds);
	}
	
	
	public static SysCenterModules[] Search(SysCenterModules searchObject)
	{
		return Search ( searchObject.CenterModuleId, searchObject.CenterId, searchObject.ModuleId, searchObject.CenterModuleStatus);
	}
	
	/// <summary>
	/// Returns all SysCenterModules objects.
	/// </summary>
	/// <returns>List of all SysCenterModules objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static SysCenterModules[] Search()
	{
		return Search ( null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

