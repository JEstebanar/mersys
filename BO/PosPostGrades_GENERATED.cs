/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 03/24/2014 12:41:47 AM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class PosPostGrades
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[pos_PostGrades]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _postId;
	private System.Int32? _gradeId;
	
	#endregion
	
	
	#region Properties
	public System.Int32? PostId
	{
		get
		{
			return _postId;
		}
		set
		{
			_postId = value;
		}
	}
	
	public System.Int32? GradeId
	{
		get
		{
			return _gradeId;
		}
		set
		{
			_gradeId = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("PostId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("GradeId", typeof(System.Int32) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (PostId == null)
		dr["PostId"] = DBNull.Value;
		else
		dr["PostId"] = PostId;
		
		if (GradeId == null)
		dr["GradeId"] = DBNull.Value;
		else
		dr["GradeId"] = GradeId;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		PostId = dr["PostId"] != DBNull.Value ? Convert.ToInt32(dr["PostId"]) : PostId = null;
		GradeId = dr["GradeId"] != DBNull.Value ? Convert.ToInt32(dr["GradeId"]) : GradeId = null;
	}
	
	public static PosPostGrades[] MapFrom(DataSet ds)
	{
		List<PosPostGrades> objects;
		
		
		// Initialise Collection.
		objects = new List<PosPostGrades>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[pos_PostGrades] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[pos_PostGrades] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			PosPostGrades instance = new PosPostGrades();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	
	#region INSERT
	public void Insert(System.Int32? postId, System.Int32? gradeId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspPosPostGrades_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, postId, gradeId);
		
		if (transaction == null)
		db.ExecuteScalar(dbCommand);
		else
		db.ExecuteScalar(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? postId, System.Int32? gradeId)
	{
		Insert(postId, gradeId, null);
	}
	/// <summary>
	/// Insert current PosPostGrades to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(PostId, GradeId, transaction);
	}
	
	/// <summary>
	/// Insert current PosPostGrades to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static PosPostGrades[] Search(System.Int32? postId, System.Int32? gradeId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspPosPostGrades_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, postId, gradeId);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return PosPostGrades.MapFrom(ds);
	}
	
	
	public static PosPostGrades[] Search(PosPostGrades searchObject)
	{
		return Search ( searchObject.PostId, searchObject.GradeId);
	}
	
	/// <summary>
	/// Returns all PosPostGrades objects.
	/// </summary>
	/// <returns>List of all PosPostGrades objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static PosPostGrades[] Search()
	{
		return Search ( null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

