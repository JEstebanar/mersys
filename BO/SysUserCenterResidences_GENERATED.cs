/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 10/31/2013 9:08:13 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class SysUserCenterResidences
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[sys_UserCenterResidences]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _userCenterResidenceId;
	private System.Int32? _userRoleId;
	private System.Int32? _centerResidenceId;
	private System.Boolean? _userResidenceStatus;
	
	#endregion
	
	
	#region Properties
	public System.Int32? UserCenterResidenceId
	{
		get
		{
			return _userCenterResidenceId;
		}
		set
		{
			_userCenterResidenceId = value;
		}
	}
	
	public System.Int32? UserRoleId
	{
		get
		{
			return _userRoleId;
		}
		set
		{
			_userRoleId = value;
		}
	}
	
	public System.Int32? CenterResidenceId
	{
		get
		{
			return _centerResidenceId;
		}
		set
		{
			_centerResidenceId = value;
		}
	}
	
	public System.Boolean? UserResidenceStatus
	{
		get
		{
			return _userResidenceStatus;
		}
		set
		{
			_userResidenceStatus = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("UserCenterResidenceId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("UserRoleId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterResidenceId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("UserResidenceStatus", typeof(System.Boolean) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (UserCenterResidenceId == null)
		dr["UserCenterResidenceId"] = DBNull.Value;
		else
		dr["UserCenterResidenceId"] = UserCenterResidenceId;
		
		if (UserRoleId == null)
		dr["UserRoleId"] = DBNull.Value;
		else
		dr["UserRoleId"] = UserRoleId;
		
		if (CenterResidenceId == null)
		dr["CenterResidenceId"] = DBNull.Value;
		else
		dr["CenterResidenceId"] = CenterResidenceId;
		
		if (UserResidenceStatus == null)
		dr["UserResidenceStatus"] = DBNull.Value;
		else
		dr["UserResidenceStatus"] = UserResidenceStatus;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		UserCenterResidenceId = dr["UserCenterResidenceId"] != DBNull.Value ? Convert.ToInt32(dr["UserCenterResidenceId"]) : UserCenterResidenceId = null;
		UserRoleId = dr["UserRoleId"] != DBNull.Value ? Convert.ToInt32(dr["UserRoleId"]) : UserRoleId = null;
		CenterResidenceId = dr["CenterResidenceId"] != DBNull.Value ? Convert.ToInt32(dr["CenterResidenceId"]) : CenterResidenceId = null;
		UserResidenceStatus = dr["UserResidenceStatus"] != DBNull.Value ? Convert.ToBoolean(dr["UserResidenceStatus"]) : UserResidenceStatus = null;
	}
	
	public static SysUserCenterResidences[] MapFrom(DataSet ds)
	{
		List<SysUserCenterResidences> objects;
		
		
		// Initialise Collection.
		objects = new List<SysUserCenterResidences>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[sys_UserCenterResidences] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[sys_UserCenterResidences] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			SysUserCenterResidences instance = new SysUserCenterResidences();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysUserCenterResidences Get(System.Int32 userCenterResidenceId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		SysUserCenterResidences instance;
		
		
		instance = new SysUserCenterResidences();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysUserCenterResidences_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterResidenceId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysUserCenterResidences ID:" + userCenterResidenceId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.Int32? userRoleId, System.Int32? centerResidenceId, System.Boolean? userResidenceStatus, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysUserCenterResidences_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, userRoleId, centerResidenceId, userResidenceStatus);
		
		if (transaction == null)
		this.UserCenterResidenceId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.UserCenterResidenceId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? userRoleId, System.Int32? centerResidenceId, System.Boolean? userResidenceStatus)
	{
		Insert(userRoleId, centerResidenceId, userResidenceStatus, null);
	}
	/// <summary>
	/// Insert current SysUserCenterResidences to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(UserRoleId, CenterResidenceId, UserResidenceStatus, transaction);
	}
	
	/// <summary>
	/// Insert current SysUserCenterResidences to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? userCenterResidenceId, System.Int32? userRoleId, System.Int32? centerResidenceId, System.Boolean? userResidenceStatus, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysUserCenterResidences_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@userCenterResidenceId"].Value = userCenterResidenceId;
		dbCommand.Parameters["@userRoleId"].Value = userRoleId;
		dbCommand.Parameters["@centerResidenceId"].Value = centerResidenceId;
		dbCommand.Parameters["@userResidenceStatus"].Value = userResidenceStatus;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? userCenterResidenceId, System.Int32? userRoleId, System.Int32? centerResidenceId, System.Boolean? userResidenceStatus)
	{
		Update(userCenterResidenceId, userRoleId, centerResidenceId, userResidenceStatus, null);
	}
	
	public static void Update(SysUserCenterResidences sysUserCenterResidences)
	{
		sysUserCenterResidences.Update();
	}
	
	public static void Update(SysUserCenterResidences sysUserCenterResidences, DbTransaction transaction)
	{
		sysUserCenterResidences.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysUserCenterResidences_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@userCenterResidenceId"].SourceColumn = "UserCenterResidenceId";
		dbCommand.Parameters["@userRoleId"].SourceColumn = "UserRoleId";
		dbCommand.Parameters["@centerResidenceId"].SourceColumn = "CenterResidenceId";
		dbCommand.Parameters["@userResidenceStatus"].SourceColumn = "UserResidenceStatus";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? userCenterResidenceId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysUserCenterResidences_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterResidenceId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? userCenterResidenceId)
	{
		Delete(
		userCenterResidenceId);
	}
	
	/// <summary>
	/// Delete current SysUserCenterResidences from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysUserCenterResidences_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, UserCenterResidenceId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.UserCenterResidenceId = null;
	}
	
	/// <summary>
	/// Delete current SysUserCenterResidences from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysUserCenterResidences[] Search(System.Int32? userCenterResidenceId, System.Int32? userRoleId, System.Int32? centerResidenceId, System.Boolean? userResidenceStatus)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysUserCenterResidences_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, userCenterResidenceId, userRoleId, centerResidenceId, userResidenceStatus);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return SysUserCenterResidences.MapFrom(ds);
	}
	
	
	public static SysUserCenterResidences[] Search(SysUserCenterResidences searchObject)
	{
		return Search ( searchObject.UserCenterResidenceId, searchObject.UserRoleId, searchObject.CenterResidenceId, searchObject.UserResidenceStatus);
	}
	
	/// <summary>
	/// Returns all SysUserCenterResidences objects.
	/// </summary>
	/// <returns>List of all SysUserCenterResidences objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static SysUserCenterResidences[] Search()
	{
		return Search ( null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

