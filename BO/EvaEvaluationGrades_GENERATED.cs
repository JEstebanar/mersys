/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 11/24/2013 3:04:36 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class EvaEvaluationGrades
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[eva_EvaluationGrades]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _evaluationId;
	private System.Int32? _gradeId;
	
	#endregion
	
	
	#region Properties
	public System.Int32? EvaluationId
	{
		get
		{
			return _evaluationId;
		}
		set
		{
			_evaluationId = value;
		}
	}
	
	public System.Int32? GradeId
	{
		get
		{
			return _gradeId;
		}
		set
		{
			_gradeId = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("EvaluationId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("GradeId", typeof(System.Int32) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (EvaluationId == null)
		dr["EvaluationId"] = DBNull.Value;
		else
		dr["EvaluationId"] = EvaluationId;
		
		if (GradeId == null)
		dr["GradeId"] = DBNull.Value;
		else
		dr["GradeId"] = GradeId;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		EvaluationId = dr["EvaluationId"] != DBNull.Value ? Convert.ToInt32(dr["EvaluationId"]) : EvaluationId = null;
		GradeId = dr["GradeId"] != DBNull.Value ? Convert.ToInt32(dr["GradeId"]) : GradeId = null;
	}
	
	public static EvaEvaluationGrades[] MapFrom(DataSet ds)
	{
		List<EvaEvaluationGrades> objects;
		
		
		// Initialise Collection.
		objects = new List<EvaEvaluationGrades>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[eva_EvaluationGrades] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[eva_EvaluationGrades] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			EvaEvaluationGrades instance = new EvaEvaluationGrades();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	
	#region INSERT
	public void Insert(System.Int32? evaluationId, System.Int32? gradeId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluationGrades_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, evaluationId, gradeId);
		
		if (transaction == null)
		db.ExecuteScalar(dbCommand);
		else
		db.ExecuteScalar(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? evaluationId, System.Int32? gradeId)
	{
		Insert(evaluationId, gradeId, null);
	}
	/// <summary>
	/// Insert current EvaEvaluationGrades to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(EvaluationId, GradeId, transaction);
	}
	
	/// <summary>
	/// Insert current EvaEvaluationGrades to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static EvaEvaluationGrades[] Search(System.Int32? evaluationId, System.Int32? gradeId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluationGrades_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, evaluationId, gradeId);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return EvaEvaluationGrades.MapFrom(ds);
	}
	
	
	public static EvaEvaluationGrades[] Search(EvaEvaluationGrades searchObject)
	{
		return Search ( searchObject.EvaluationId, searchObject.GradeId);
	}
	
	/// <summary>
	/// Returns all EvaEvaluationGrades objects.
	/// </summary>
	/// <returns>List of all EvaEvaluationGrades objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static EvaEvaluationGrades[] Search()
	{
		return Search ( null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

