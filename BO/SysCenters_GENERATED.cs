/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 12/03/2013 10:51:49 AM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class SysCenters
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[sys_Centers]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _centerId;
	private System.Int32? _centerParentId;
	private System.Int32? _generalId;
	private System.String _centerName;
	private System.Int32? _centerPrincipalId;
	private System.Int32? _centerSubPrincipalId;
	private System.Int32? _centerAdministratorId;
	private System.Int32? _headTrainingId;
	private System.Boolean? _centerStatus;
	private System.String _centerLogo;
	
	#endregion
	
	
	#region Properties
	public System.Int32? CenterId
	{
		get
		{
			return _centerId;
		}
		set
		{
			_centerId = value;
		}
	}
	
	public System.Int32? CenterParentId
	{
		get
		{
			return _centerParentId;
		}
		set
		{
			_centerParentId = value;
		}
	}
	
	public System.Int32? GeneralId
	{
		get
		{
			return _generalId;
		}
		set
		{
			_generalId = value;
		}
	}
	
	public System.String CenterName
	{
		get
		{
			return _centerName;
		}
		set
		{
			_centerName = value;
		}
	}
	
	public System.Int32? CenterPrincipalId
	{
		get
		{
			return _centerPrincipalId;
		}
		set
		{
			_centerPrincipalId = value;
		}
	}
	
	public System.Int32? CenterSubPrincipalId
	{
		get
		{
			return _centerSubPrincipalId;
		}
		set
		{
			_centerSubPrincipalId = value;
		}
	}
	
	public System.Int32? CenterAdministratorId
	{
		get
		{
			return _centerAdministratorId;
		}
		set
		{
			_centerAdministratorId = value;
		}
	}
	
	public System.Int32? HeadTrainingId
	{
		get
		{
			return _headTrainingId;
		}
		set
		{
			_headTrainingId = value;
		}
	}
	
	public System.Boolean? CenterStatus
	{
		get
		{
			return _centerStatus;
		}
		set
		{
			_centerStatus = value;
		}
	}
	
	public System.String CenterLogo
	{
		get
		{
			return _centerLogo;
		}
		set
		{
			_centerLogo = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("CenterId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterParentId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("GeneralId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterPrincipalId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterSubPrincipalId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterAdministratorId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("HeadTrainingId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterStatus", typeof(System.Boolean) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterLogo", typeof(System.String) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (CenterId == null)
		dr["CenterId"] = DBNull.Value;
		else
		dr["CenterId"] = CenterId;
		
		if (CenterParentId == null)
		dr["CenterParentId"] = DBNull.Value;
		else
		dr["CenterParentId"] = CenterParentId;
		
		if (GeneralId == null)
		dr["GeneralId"] = DBNull.Value;
		else
		dr["GeneralId"] = GeneralId;
		
		if (CenterName == null)
		dr["CenterName"] = DBNull.Value;
		else
		dr["CenterName"] = CenterName;
		
		if (CenterPrincipalId == null)
		dr["CenterPrincipalId"] = DBNull.Value;
		else
		dr["CenterPrincipalId"] = CenterPrincipalId;
		
		if (CenterSubPrincipalId == null)
		dr["CenterSubPrincipalId"] = DBNull.Value;
		else
		dr["CenterSubPrincipalId"] = CenterSubPrincipalId;
		
		if (CenterAdministratorId == null)
		dr["CenterAdministratorId"] = DBNull.Value;
		else
		dr["CenterAdministratorId"] = CenterAdministratorId;
		
		if (HeadTrainingId == null)
		dr["HeadTrainingId"] = DBNull.Value;
		else
		dr["HeadTrainingId"] = HeadTrainingId;
		
		if (CenterStatus == null)
		dr["CenterStatus"] = DBNull.Value;
		else
		dr["CenterStatus"] = CenterStatus;
		
		if (CenterLogo == null)
		dr["CenterLogo"] = DBNull.Value;
		else
		dr["CenterLogo"] = CenterLogo;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		CenterId = dr["CenterId"] != DBNull.Value ? Convert.ToInt32(dr["CenterId"]) : CenterId = null;
		CenterParentId = dr["CenterParentId"] != DBNull.Value ? Convert.ToInt32(dr["CenterParentId"]) : CenterParentId = null;
		GeneralId = dr["GeneralId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralId"]) : GeneralId = null;
		CenterName = dr["CenterName"] != DBNull.Value ? Convert.ToString(dr["CenterName"]) : CenterName = null;
		CenterPrincipalId = dr["CenterPrincipalId"] != DBNull.Value ? Convert.ToInt32(dr["CenterPrincipalId"]) : CenterPrincipalId = null;
		CenterSubPrincipalId = dr["CenterSubPrincipalId"] != DBNull.Value ? Convert.ToInt32(dr["CenterSubPrincipalId"]) : CenterSubPrincipalId = null;
		CenterAdministratorId = dr["CenterAdministratorId"] != DBNull.Value ? Convert.ToInt32(dr["CenterAdministratorId"]) : CenterAdministratorId = null;
		HeadTrainingId = dr["HeadTrainingId"] != DBNull.Value ? Convert.ToInt32(dr["HeadTrainingId"]) : HeadTrainingId = null;
		CenterStatus = dr["CenterStatus"] != DBNull.Value ? Convert.ToBoolean(dr["CenterStatus"]) : CenterStatus = null;
		CenterLogo = dr["CenterLogo"] != DBNull.Value ? Convert.ToString(dr["CenterLogo"]) : CenterLogo = null;
	}
	
	public static SysCenters[] MapFrom(DataSet ds)
	{
		List<SysCenters> objects;
		
		
		// Initialise Collection.
		objects = new List<SysCenters>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[sys_Centers] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[sys_Centers] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			SysCenters instance = new SysCenters();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysCenters Get(System.Int32 centerId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		SysCenters instance;
		
		
		instance = new SysCenters();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenters_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, centerId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysCenters ID:" + centerId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.Int32? centerParentId, System.Int32? generalId, System.String centerName, System.Int32? centerPrincipalId, System.Int32? centerSubPrincipalId, System.Int32? centerAdministratorId, System.Int32? headTrainingId, System.Boolean? centerStatus, System.String centerLogo, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenters_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, centerParentId, generalId, centerName, centerPrincipalId, centerSubPrincipalId, centerAdministratorId, headTrainingId, centerStatus, centerLogo);
		
		if (transaction == null)
		this.CenterId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.CenterId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? centerParentId, System.Int32? generalId, System.String centerName, System.Int32? centerPrincipalId, System.Int32? centerSubPrincipalId, System.Int32? centerAdministratorId, System.Int32? headTrainingId, System.Boolean? centerStatus, System.String centerLogo)
	{
		Insert(centerParentId, generalId, centerName, centerPrincipalId, centerSubPrincipalId, centerAdministratorId, headTrainingId, centerStatus, centerLogo, null);
	}
	/// <summary>
	/// Insert current SysCenters to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(CenterParentId, GeneralId, CenterName, CenterPrincipalId, CenterSubPrincipalId, CenterAdministratorId, HeadTrainingId, CenterStatus, CenterLogo, transaction);
	}
	
	/// <summary>
	/// Insert current SysCenters to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? centerId, System.Int32? centerParentId, System.Int32? generalId, System.String centerName, System.Int32? centerPrincipalId, System.Int32? centerSubPrincipalId, System.Int32? centerAdministratorId, System.Int32? headTrainingId, System.Boolean? centerStatus, System.String centerLogo, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenters_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@centerId"].Value = centerId;
		dbCommand.Parameters["@centerParentId"].Value = centerParentId;
		dbCommand.Parameters["@generalId"].Value = generalId;
		dbCommand.Parameters["@centerName"].Value = centerName;
		dbCommand.Parameters["@centerPrincipalId"].Value = centerPrincipalId;
		dbCommand.Parameters["@centerSubPrincipalId"].Value = centerSubPrincipalId;
		dbCommand.Parameters["@centerAdministratorId"].Value = centerAdministratorId;
		dbCommand.Parameters["@headTrainingId"].Value = headTrainingId;
		dbCommand.Parameters["@centerStatus"].Value = centerStatus;
		dbCommand.Parameters["@centerLogo"].Value = centerLogo;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? centerId, System.Int32? centerParentId, System.Int32? generalId, System.String centerName, System.Int32? centerPrincipalId, System.Int32? centerSubPrincipalId, System.Int32? centerAdministratorId, System.Int32? headTrainingId, System.Boolean? centerStatus, System.String centerLogo)
	{
		Update(centerId, centerParentId, generalId, centerName, centerPrincipalId, centerSubPrincipalId, centerAdministratorId, headTrainingId, centerStatus, centerLogo, null);
	}
	
	public static void Update(SysCenters sysCenters)
	{
		sysCenters.Update();
	}
	
	public static void Update(SysCenters sysCenters, DbTransaction transaction)
	{
		sysCenters.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenters_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@centerId"].SourceColumn = "CenterId";
		dbCommand.Parameters["@centerParentId"].SourceColumn = "CenterParentId";
		dbCommand.Parameters["@generalId"].SourceColumn = "GeneralId";
		dbCommand.Parameters["@centerName"].SourceColumn = "CenterName";
		dbCommand.Parameters["@centerPrincipalId"].SourceColumn = "CenterPrincipalId";
		dbCommand.Parameters["@centerSubPrincipalId"].SourceColumn = "CenterSubPrincipalId";
		dbCommand.Parameters["@centerAdministratorId"].SourceColumn = "CenterAdministratorId";
		dbCommand.Parameters["@headTrainingId"].SourceColumn = "HeadTrainingId";
		dbCommand.Parameters["@centerStatus"].SourceColumn = "CenterStatus";
		dbCommand.Parameters["@centerLogo"].SourceColumn = "CenterLogo";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? centerId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenters_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, centerId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? centerId)
	{
		Delete(
		centerId);
	}
	
	/// <summary>
	/// Delete current SysCenters from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenters_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, CenterId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.CenterId = null;
	}
	
	/// <summary>
	/// Delete current SysCenters from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysCenters[] Search(System.Int32? centerId, System.Int32? centerParentId, System.Int32? generalId, System.String centerName, System.Int32? centerPrincipalId, System.Int32? centerSubPrincipalId, System.Int32? centerAdministratorId, System.Int32? headTrainingId, System.Boolean? centerStatus, System.String centerLogo)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenters_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, centerId, centerParentId, generalId, centerName, centerPrincipalId, centerSubPrincipalId, centerAdministratorId, headTrainingId, centerStatus, centerLogo);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return SysCenters.MapFrom(ds);
	}
	
	
	public static SysCenters[] Search(SysCenters searchObject)
	{
		return Search ( searchObject.CenterId, searchObject.CenterParentId, searchObject.GeneralId, searchObject.CenterName, searchObject.CenterPrincipalId, searchObject.CenterSubPrincipalId, searchObject.CenterAdministratorId, searchObject.HeadTrainingId, searchObject.CenterStatus, searchObject.CenterLogo);
	}
	
	/// <summary>
	/// Returns all SysCenters objects.
	/// </summary>
	/// <returns>List of all SysCenters objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static SysCenters[] Search()
	{
		return Search ( null, null, null, null, null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

