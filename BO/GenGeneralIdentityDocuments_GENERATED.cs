/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 09/17/2013 9:14:21 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class GenGeneralIdentityDocuments
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[gen_GeneralIdentityDocuments]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _identityId;
	private System.Int32? _generalId;
	private System.String _identityNumber;
	private System.Int32? _identityTypeId;
	private System.DateTime? _expirationDate;
	private System.String _identityPath;
	private System.Int32? _createdBY;
	private System.DateTime? _createdDate;
	private System.Int32? _modifiedBy;
	private System.DateTime? _modifiedDate;
	
	#endregion
	
	
	#region Properties
	public System.Int32? IdentityId
	{
		get
		{
			return _identityId;
		}
		set
		{
			_identityId = value;
		}
	}
	
	public System.Int32? GeneralId
	{
		get
		{
			return _generalId;
		}
		set
		{
			_generalId = value;
		}
	}
	
	public System.String IdentityNumber
	{
		get
		{
			return _identityNumber;
		}
		set
		{
			_identityNumber = value;
		}
	}
	
	public System.Int32? IdentityTypeId
	{
		get
		{
			return _identityTypeId;
		}
		set
		{
			_identityTypeId = value;
		}
	}
	
	public System.DateTime? ExpirationDate
	{
		get
		{
			return _expirationDate;
		}
		set
		{
			_expirationDate = value;
		}
	}
	
	public System.String IdentityPath
	{
		get
		{
			return _identityPath;
		}
		set
		{
			_identityPath = value;
		}
	}
	
	public System.Int32? CreatedBY
	{
		get
		{
			return _createdBY;
		}
		set
		{
			_createdBY = value;
		}
	}
	
	public System.DateTime? CreatedDate
	{
		get
		{
			return _createdDate;
		}
		set
		{
			_createdDate = value;
		}
	}
	
	public System.Int32? ModifiedBy
	{
		get
		{
			return _modifiedBy;
		}
		set
		{
			_modifiedBy = value;
		}
	}
	
	public System.DateTime? ModifiedDate
	{
		get
		{
			return _modifiedDate;
		}
		set
		{
			_modifiedDate = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("IdentityId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("GeneralId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("IdentityNumber", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("IdentityTypeId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ExpirationDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("IdentityPath", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedBY", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (IdentityId == null)
		dr["IdentityId"] = DBNull.Value;
		else
		dr["IdentityId"] = IdentityId;
		
		if (GeneralId == null)
		dr["GeneralId"] = DBNull.Value;
		else
		dr["GeneralId"] = GeneralId;
		
		if (IdentityNumber == null)
		dr["IdentityNumber"] = DBNull.Value;
		else
		dr["IdentityNumber"] = IdentityNumber;
		
		if (IdentityTypeId == null)
		dr["IdentityTypeId"] = DBNull.Value;
		else
		dr["IdentityTypeId"] = IdentityTypeId;
		
		if (ExpirationDate == null)
		dr["ExpirationDate"] = DBNull.Value;
		else
		dr["ExpirationDate"] = ExpirationDate;
		
		if (IdentityPath == null)
		dr["IdentityPath"] = DBNull.Value;
		else
		dr["IdentityPath"] = IdentityPath;
		
		if (CreatedBY == null)
		dr["CreatedBY"] = DBNull.Value;
		else
		dr["CreatedBY"] = CreatedBY;
		
		if (CreatedDate == null)
		dr["CreatedDate"] = DBNull.Value;
		else
		dr["CreatedDate"] = CreatedDate;
		
		if (ModifiedBy == null)
		dr["ModifiedBy"] = DBNull.Value;
		else
		dr["ModifiedBy"] = ModifiedBy;
		
		if (ModifiedDate == null)
		dr["ModifiedDate"] = DBNull.Value;
		else
		dr["ModifiedDate"] = ModifiedDate;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		IdentityId = dr["IdentityId"] != DBNull.Value ? Convert.ToInt32(dr["IdentityId"]) : IdentityId = null;
		GeneralId = dr["GeneralId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralId"]) : GeneralId = null;
		IdentityNumber = dr["IdentityNumber"] != DBNull.Value ? Convert.ToString(dr["IdentityNumber"]) : IdentityNumber = null;
		IdentityTypeId = dr["IdentityTypeId"] != DBNull.Value ? Convert.ToInt32(dr["IdentityTypeId"]) : IdentityTypeId = null;
		ExpirationDate = dr["ExpirationDate"] != DBNull.Value ? Convert.ToDateTime(dr["ExpirationDate"]) : ExpirationDate = null;
		IdentityPath = dr["IdentityPath"] != DBNull.Value ? Convert.ToString(dr["IdentityPath"]) : IdentityPath = null;
		CreatedBY = dr["CreatedBY"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBY"]) : CreatedBY = null;
		CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
		ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
		ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
	}
	
	public static GenGeneralIdentityDocuments[] MapFrom(DataSet ds)
	{
		List<GenGeneralIdentityDocuments> objects;
		
		
		// Initialise Collection.
		objects = new List<GenGeneralIdentityDocuments>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[gen_GeneralIdentityDocuments] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[gen_GeneralIdentityDocuments] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			GenGeneralIdentityDocuments instance = new GenGeneralIdentityDocuments();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static GenGeneralIdentityDocuments Get(System.Int32 identityId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		GenGeneralIdentityDocuments instance;
		
		
		instance = new GenGeneralIdentityDocuments();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGeneralIdentityDocuments_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, identityId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenGeneralIdentityDocuments ID:" + identityId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.Int32? generalId, System.String identityNumber, System.Int32? identityTypeId, System.DateTime? expirationDate, System.String identityPath, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGeneralIdentityDocuments_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, generalId, identityNumber, identityTypeId, expirationDate, identityPath, createdBY, createdDate, modifiedBy, modifiedDate);
		
		if (transaction == null)
		this.IdentityId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.IdentityId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? generalId, System.String identityNumber, System.Int32? identityTypeId, System.DateTime? expirationDate, System.String identityPath, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Insert(generalId, identityNumber, identityTypeId, expirationDate, identityPath, createdBY, createdDate, modifiedBy, modifiedDate, null);
	}
	/// <summary>
	/// Insert current GenGeneralIdentityDocuments to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(GeneralId, IdentityNumber, IdentityTypeId, ExpirationDate, IdentityPath, CreatedBY, CreatedDate, ModifiedBy, ModifiedDate, transaction);
	}
	
	/// <summary>
	/// Insert current GenGeneralIdentityDocuments to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? identityId, System.Int32? generalId, System.String identityNumber, System.Int32? identityTypeId, System.DateTime? expirationDate, System.String identityPath, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGeneralIdentityDocuments_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@identityId"].Value = identityId;
		dbCommand.Parameters["@generalId"].Value = generalId;
		dbCommand.Parameters["@identityNumber"].Value = identityNumber;
		dbCommand.Parameters["@identityTypeId"].Value = identityTypeId;
		dbCommand.Parameters["@expirationDate"].Value = expirationDate;
		dbCommand.Parameters["@identityPath"].Value = identityPath;
		dbCommand.Parameters["@createdBY"].Value = createdBY;
		dbCommand.Parameters["@createdDate"].Value = createdDate;
		dbCommand.Parameters["@modifiedBy"].Value = modifiedBy;
		dbCommand.Parameters["@modifiedDate"].Value = modifiedDate;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? identityId, System.Int32? generalId, System.String identityNumber, System.Int32? identityTypeId, System.DateTime? expirationDate, System.String identityPath, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Update(identityId, generalId, identityNumber, identityTypeId, expirationDate, identityPath, createdBY, createdDate, modifiedBy, modifiedDate, null);
	}
	
	public static void Update(GenGeneralIdentityDocuments genGeneralIdentityDocuments)
	{
		genGeneralIdentityDocuments.Update();
	}
	
	public static void Update(GenGeneralIdentityDocuments genGeneralIdentityDocuments, DbTransaction transaction)
	{
		genGeneralIdentityDocuments.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGeneralIdentityDocuments_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@identityId"].SourceColumn = "IdentityId";
		dbCommand.Parameters["@generalId"].SourceColumn = "GeneralId";
		dbCommand.Parameters["@identityNumber"].SourceColumn = "IdentityNumber";
		dbCommand.Parameters["@identityTypeId"].SourceColumn = "IdentityTypeId";
		dbCommand.Parameters["@expirationDate"].SourceColumn = "ExpirationDate";
		dbCommand.Parameters["@identityPath"].SourceColumn = "IdentityPath";
		dbCommand.Parameters["@createdBY"].SourceColumn = "CreatedBY";
		dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
		dbCommand.Parameters["@modifiedBy"].SourceColumn = "ModifiedBy";
		dbCommand.Parameters["@modifiedDate"].SourceColumn = "ModifiedDate";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? identityId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGeneralIdentityDocuments_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, identityId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? identityId)
	{
		Delete(
		identityId);
	}
	
	/// <summary>
	/// Delete current GenGeneralIdentityDocuments from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGeneralIdentityDocuments_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, IdentityId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.IdentityId = null;
	}
	
	/// <summary>
	/// Delete current GenGeneralIdentityDocuments from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static GenGeneralIdentityDocuments[] Search(System.Int32? identityId, System.Int32? generalId, System.String identityNumber, System.Int32? identityTypeId, System.DateTime? expirationDate, System.String identityPath, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGeneralIdentityDocuments_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, identityId, generalId, identityNumber, identityTypeId, expirationDate, identityPath, createdBY, createdDate, modifiedBy, modifiedDate);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return GenGeneralIdentityDocuments.MapFrom(ds);
	}
	
	
	public static GenGeneralIdentityDocuments[] Search(GenGeneralIdentityDocuments searchObject)
	{
		return Search ( searchObject.IdentityId, searchObject.GeneralId, searchObject.IdentityNumber, searchObject.IdentityTypeId, searchObject.ExpirationDate, searchObject.IdentityPath, searchObject.CreatedBY, searchObject.CreatedDate, searchObject.ModifiedBy, searchObject.ModifiedDate);
	}
	
	/// <summary>
	/// Returns all GenGeneralIdentityDocuments objects.
	/// </summary>
	/// <returns>List of all GenGeneralIdentityDocuments objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static GenGeneralIdentityDocuments[] Search()
	{
		return Search ( null, null, null, null, null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

