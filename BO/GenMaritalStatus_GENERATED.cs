/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 09/07/2013 11:59:58 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class GenMaritalStatus
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[gen_MaritalStatus]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _maritalStatusId;
	private System.String _maritalStatusName;
	private System.Boolean? _maritalStatusStatus;
	
	#endregion
	
	
	#region Properties
	public System.Int32? MaritalStatusId
	{
		get
		{
			return _maritalStatusId;
		}
		set
		{
			_maritalStatusId = value;
		}
	}
	
	public System.String MaritalStatusName
	{
		get
		{
			return _maritalStatusName;
		}
		set
		{
			_maritalStatusName = value;
		}
	}
	
	public System.Boolean? MaritalStatusStatus
	{
		get
		{
			return _maritalStatusStatus;
		}
		set
		{
			_maritalStatusStatus = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("MaritalStatusId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("MaritalStatusName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("MaritalStatusStatus", typeof(System.Boolean) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (MaritalStatusId == null)
		dr["MaritalStatusId"] = DBNull.Value;
		else
		dr["MaritalStatusId"] = MaritalStatusId;
		
		if (MaritalStatusName == null)
		dr["MaritalStatusName"] = DBNull.Value;
		else
		dr["MaritalStatusName"] = MaritalStatusName;
		
		if (MaritalStatusStatus == null)
		dr["MaritalStatusStatus"] = DBNull.Value;
		else
		dr["MaritalStatusStatus"] = MaritalStatusStatus;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		MaritalStatusId = dr["MaritalStatusId"] != DBNull.Value ? Convert.ToInt32(dr["MaritalStatusId"]) : MaritalStatusId = null;
		MaritalStatusName = dr["MaritalStatusName"] != DBNull.Value ? Convert.ToString(dr["MaritalStatusName"]) : MaritalStatusName = null;
		MaritalStatusStatus = dr["MaritalStatusStatus"] != DBNull.Value ? Convert.ToBoolean(dr["MaritalStatusStatus"]) : MaritalStatusStatus = null;
	}
	
	public static GenMaritalStatus[] MapFrom(DataSet ds)
	{
		List<GenMaritalStatus> objects;
		
		
		// Initialise Collection.
		objects = new List<GenMaritalStatus>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[gen_MaritalStatus] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[gen_MaritalStatus] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			GenMaritalStatus instance = new GenMaritalStatus();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static GenMaritalStatus Get(System.Int32 maritalStatusId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		GenMaritalStatus instance;
		
		
		instance = new GenMaritalStatus();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenMaritalStatus_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, maritalStatusId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenMaritalStatus ID:" + maritalStatusId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.String maritalStatusName, System.Boolean? maritalStatusStatus, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenMaritalStatus_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, maritalStatusName, maritalStatusStatus);
		
		if (transaction == null)
		this.MaritalStatusId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.MaritalStatusId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.String maritalStatusName, System.Boolean? maritalStatusStatus)
	{
		Insert(maritalStatusName, maritalStatusStatus, null);
	}
	/// <summary>
	/// Insert current GenMaritalStatus to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(MaritalStatusName, MaritalStatusStatus, transaction);
	}
	
	/// <summary>
	/// Insert current GenMaritalStatus to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? maritalStatusId, System.String maritalStatusName, System.Boolean? maritalStatusStatus, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenMaritalStatus_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@maritalStatusId"].Value = maritalStatusId;
		dbCommand.Parameters["@maritalStatusName"].Value = maritalStatusName;
		dbCommand.Parameters["@maritalStatusStatus"].Value = maritalStatusStatus;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? maritalStatusId, System.String maritalStatusName, System.Boolean? maritalStatusStatus)
	{
		Update(maritalStatusId, maritalStatusName, maritalStatusStatus, null);
	}
	
	public static void Update(GenMaritalStatus genMaritalStatus)
	{
		genMaritalStatus.Update();
	}
	
	public static void Update(GenMaritalStatus genMaritalStatus, DbTransaction transaction)
	{
		genMaritalStatus.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenMaritalStatus_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@maritalStatusId"].SourceColumn = "MaritalStatusId";
		dbCommand.Parameters["@maritalStatusName"].SourceColumn = "MaritalStatusName";
		dbCommand.Parameters["@maritalStatusStatus"].SourceColumn = "MaritalStatusStatus";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? maritalStatusId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenMaritalStatus_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, maritalStatusId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? maritalStatusId)
	{
		Delete(
		maritalStatusId);
	}
	
	/// <summary>
	/// Delete current GenMaritalStatus from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenMaritalStatus_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, MaritalStatusId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.MaritalStatusId = null;
	}
	
	/// <summary>
	/// Delete current GenMaritalStatus from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static GenMaritalStatus[] Search(System.Int32? maritalStatusId, System.String maritalStatusName, System.Boolean? maritalStatusStatus)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenMaritalStatus_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, maritalStatusId, maritalStatusName, maritalStatusStatus);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return GenMaritalStatus.MapFrom(ds);
	}
	
	
	public static GenMaritalStatus[] Search(GenMaritalStatus searchObject)
	{
		return Search ( searchObject.MaritalStatusId, searchObject.MaritalStatusName, searchObject.MaritalStatusStatus);
	}
	
	/// <summary>
	/// Returns all GenMaritalStatus objects.
	/// </summary>
	/// <returns>List of all GenMaritalStatus objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static GenMaritalStatus[] Search()
	{
		return Search ( null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

