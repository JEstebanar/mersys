/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 04/19/2014 12:46:20 AM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class SysCenterLetters
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[sys_CenterLetters]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _centerRequestTypeId;
	private System.Int32? _centerId;
	private System.Int32? _letterId;
	private System.Decimal? _letterPrice;
	private System.Int32? _versionNumber;
	private System.Boolean? _centerLetterStatus;
	
	#endregion
	
	
	#region Properties
	public System.Int32? CenterRequestTypeId
	{
		get
		{
			return _centerRequestTypeId;
		}
		set
		{
			_centerRequestTypeId = value;
		}
	}
	
	public System.Int32? CenterId
	{
		get
		{
			return _centerId;
		}
		set
		{
			_centerId = value;
		}
	}
	
	public System.Int32? LetterId
	{
		get
		{
			return _letterId;
		}
		set
		{
			_letterId = value;
		}
	}
	
	public System.Decimal? LetterPrice
	{
		get
		{
			return _letterPrice;
		}
		set
		{
			_letterPrice = value;
		}
	}
	
	public System.Int32? VersionNumber
	{
		get
		{
			return _versionNumber;
		}
		set
		{
			_versionNumber = value;
		}
	}
	
	public System.Boolean? CenterLetterStatus
	{
		get
		{
			return _centerLetterStatus;
		}
		set
		{
			_centerLetterStatus = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("CenterRequestTypeId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("LetterId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("LetterPrice", typeof(System.Decimal) );
		ds.Tables[TABLE_NAME].Columns.Add("VersionNumber", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterLetterStatus", typeof(System.Boolean) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (CenterRequestTypeId == null)
		dr["CenterRequestTypeId"] = DBNull.Value;
		else
		dr["CenterRequestTypeId"] = CenterRequestTypeId;
		
		if (CenterId == null)
		dr["CenterId"] = DBNull.Value;
		else
		dr["CenterId"] = CenterId;
		
		if (LetterId == null)
		dr["LetterId"] = DBNull.Value;
		else
		dr["LetterId"] = LetterId;
		
		if (LetterPrice == null)
		dr["LetterPrice"] = DBNull.Value;
		else
		dr["LetterPrice"] = LetterPrice;
		
		if (VersionNumber == null)
		dr["VersionNumber"] = DBNull.Value;
		else
		dr["VersionNumber"] = VersionNumber;
		
		if (CenterLetterStatus == null)
		dr["CenterLetterStatus"] = DBNull.Value;
		else
		dr["CenterLetterStatus"] = CenterLetterStatus;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		CenterRequestTypeId = dr["CenterRequestTypeId"] != DBNull.Value ? Convert.ToInt32(dr["CenterRequestTypeId"]) : CenterRequestTypeId = null;
		CenterId = dr["CenterId"] != DBNull.Value ? Convert.ToInt32(dr["CenterId"]) : CenterId = null;
		LetterId = dr["LetterId"] != DBNull.Value ? Convert.ToInt32(dr["LetterId"]) : LetterId = null;
		LetterPrice = dr["LetterPrice"] != DBNull.Value ? Convert.ToDecimal(dr["LetterPrice"]) : LetterPrice = null;
		VersionNumber = dr["VersionNumber"] != DBNull.Value ? Convert.ToInt32(dr["VersionNumber"]) : VersionNumber = null;
		CenterLetterStatus = dr["CenterLetterStatus"] != DBNull.Value ? Convert.ToBoolean(dr["CenterLetterStatus"]) : CenterLetterStatus = null;
	}
	
	public static SysCenterLetters[] MapFrom(DataSet ds)
	{
		List<SysCenterLetters> objects;
		
		
		// Initialise Collection.
		objects = new List<SysCenterLetters>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[sys_CenterLetters] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[sys_CenterLetters] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			SysCenterLetters instance = new SysCenterLetters();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysCenterLetters Get(System.Int32 centerRequestTypeId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		SysCenterLetters instance;
		
		
		instance = new SysCenterLetters();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenterLetters_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, centerRequestTypeId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysCenterLetters ID:" + centerRequestTypeId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.Int32? centerId, System.Int32? letterId, System.Decimal? letterPrice, System.Int32? versionNumber, System.Boolean? centerLetterStatus, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenterLetters_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, centerId, letterId, letterPrice, versionNumber, centerLetterStatus);
		
		if (transaction == null)
		this.CenterRequestTypeId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.CenterRequestTypeId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? centerId, System.Int32? letterId, System.Decimal? letterPrice, System.Int32? versionNumber, System.Boolean? centerLetterStatus)
	{
		Insert(centerId, letterId, letterPrice, versionNumber, centerLetterStatus, null);
	}
	/// <summary>
	/// Insert current SysCenterLetters to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(CenterId, LetterId, LetterPrice, VersionNumber, CenterLetterStatus, transaction);
	}
	
	/// <summary>
	/// Insert current SysCenterLetters to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? centerRequestTypeId, System.Int32? centerId, System.Int32? letterId, System.Decimal? letterPrice, System.Int32? versionNumber, System.Boolean? centerLetterStatus, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenterLetters_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@centerRequestTypeId"].Value = centerRequestTypeId;
		dbCommand.Parameters["@centerId"].Value = centerId;
		dbCommand.Parameters["@letterId"].Value = letterId;
		dbCommand.Parameters["@letterPrice"].Value = letterPrice;
		dbCommand.Parameters["@versionNumber"].Value = versionNumber;
		dbCommand.Parameters["@centerLetterStatus"].Value = centerLetterStatus;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? centerRequestTypeId, System.Int32? centerId, System.Int32? letterId, System.Decimal? letterPrice, System.Int32? versionNumber, System.Boolean? centerLetterStatus)
	{
		Update(centerRequestTypeId, centerId, letterId, letterPrice, versionNumber, centerLetterStatus, null);
	}
	
	public static void Update(SysCenterLetters sysCenterLetters)
	{
		sysCenterLetters.Update();
	}
	
	public static void Update(SysCenterLetters sysCenterLetters, DbTransaction transaction)
	{
		sysCenterLetters.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenterLetters_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@centerRequestTypeId"].SourceColumn = "CenterRequestTypeId";
		dbCommand.Parameters["@centerId"].SourceColumn = "CenterId";
		dbCommand.Parameters["@letterId"].SourceColumn = "LetterId";
		dbCommand.Parameters["@letterPrice"].SourceColumn = "LetterPrice";
		dbCommand.Parameters["@versionNumber"].SourceColumn = "VersionNumber";
		dbCommand.Parameters["@centerLetterStatus"].SourceColumn = "CenterLetterStatus";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? centerRequestTypeId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenterLetters_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, centerRequestTypeId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? centerRequestTypeId)
	{
		Delete(
		centerRequestTypeId);
	}
	
	/// <summary>
	/// Delete current SysCenterLetters from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenterLetters_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, CenterRequestTypeId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.CenterRequestTypeId = null;
	}
	
	/// <summary>
	/// Delete current SysCenterLetters from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysCenterLetters[] Search(System.Int32? centerRequestTypeId, System.Int32? centerId, System.Int32? letterId, System.Decimal? letterPrice, System.Int32? versionNumber, System.Boolean? centerLetterStatus)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysCenterLetters_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, centerRequestTypeId, centerId, letterId, letterPrice, versionNumber, centerLetterStatus);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return SysCenterLetters.MapFrom(ds);
	}
	
	
	public static SysCenterLetters[] Search(SysCenterLetters searchObject)
	{
		return Search ( searchObject.CenterRequestTypeId, searchObject.CenterId, searchObject.LetterId, searchObject.LetterPrice, searchObject.VersionNumber, searchObject.CenterLetterStatus);
	}
	
	/// <summary>
	/// Returns all SysCenterLetters objects.
	/// </summary>
	/// <returns>List of all SysCenterLetters objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static SysCenterLetters[] Search()
	{
		return Search ( null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

