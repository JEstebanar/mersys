/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 03/30/2014 5:52:10 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class SysReportTypes
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[sys_ReportTypes]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _typeId;
	private System.String _typeName;
	
	#endregion
	
	
	#region Properties
	public System.Int32? TypeId
	{
		get
		{
			return _typeId;
		}
		set
		{
			_typeId = value;
		}
	}
	
	public System.String TypeName
	{
		get
		{
			return _typeName;
		}
		set
		{
			_typeName = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("TypeId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("TypeName", typeof(System.String) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (TypeId == null)
		dr["TypeId"] = DBNull.Value;
		else
		dr["TypeId"] = TypeId;
		
		if (TypeName == null)
		dr["TypeName"] = DBNull.Value;
		else
		dr["TypeName"] = TypeName;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		TypeId = dr["TypeId"] != DBNull.Value ? Convert.ToInt32(dr["TypeId"]) : TypeId = null;
		TypeName = dr["TypeName"] != DBNull.Value ? Convert.ToString(dr["TypeName"]) : TypeName = null;
	}
	
	public static SysReportTypes[] MapFrom(DataSet ds)
	{
		List<SysReportTypes> objects;
		
		
		// Initialise Collection.
		objects = new List<SysReportTypes>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[sys_ReportTypes] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[sys_ReportTypes] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			SysReportTypes instance = new SysReportTypes();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysReportTypes Get(System.Int32 typeId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		SysReportTypes instance;
		
		
		instance = new SysReportTypes();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysReportTypes_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, typeId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get SysReportTypes ID:" + typeId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.String typeName, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysReportTypes_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, typeName);
		
		if (transaction == null)
		this.TypeId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.TypeId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.String typeName)
	{
		Insert(typeName, null);
	}
	/// <summary>
	/// Insert current SysReportTypes to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(TypeName, transaction);
	}
	
	/// <summary>
	/// Insert current SysReportTypes to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? typeId, System.String typeName, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysReportTypes_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@typeId"].Value = typeId;
		dbCommand.Parameters["@typeName"].Value = typeName;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? typeId, System.String typeName)
	{
		Update(typeId, typeName, null);
	}
	
	public static void Update(SysReportTypes sysReportTypes)
	{
		sysReportTypes.Update();
	}
	
	public static void Update(SysReportTypes sysReportTypes, DbTransaction transaction)
	{
		sysReportTypes.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysReportTypes_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@typeId"].SourceColumn = "TypeId";
		dbCommand.Parameters["@typeName"].SourceColumn = "TypeName";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? typeId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysReportTypes_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, typeId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? typeId)
	{
		Delete(
		typeId);
	}
	
	/// <summary>
	/// Delete current SysReportTypes from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysReportTypes_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, TypeId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.TypeId = null;
	}
	
	/// <summary>
	/// Delete current SysReportTypes from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static SysReportTypes[] Search(System.Int32? typeId, System.String typeName)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspSysReportTypes_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, typeId, typeName);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return SysReportTypes.MapFrom(ds);
	}
	
	
	public static SysReportTypes[] Search(SysReportTypes searchObject)
	{
		return Search ( searchObject.TypeId, searchObject.TypeName);
	}
	
	/// <summary>
	/// Returns all SysReportTypes objects.
	/// </summary>
	/// <returns>List of all SysReportTypes objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static SysReportTypes[] Search()
	{
		return Search ( null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

