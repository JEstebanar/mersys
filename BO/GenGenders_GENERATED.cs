/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 09/07/2013 11:59:58 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class GenGenders
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[gen_Genders]";

    private static readonly string DataConection = "ConeccionData";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _genderId;
	private System.String _genderName;
	private System.Boolean? _genderStatus;
	
	#endregion
	
	
	#region Properties
	public System.Int32? GenderId
	{
		get
		{
			return _genderId;
		}
		set
		{
			_genderId = value;
		}
	}
	
	public System.String GenderName
	{
		get
		{
			return _genderName;
		}
		set
		{
			_genderName = value;
		}
	}
	
	public System.Boolean? GenderStatus
	{
		get
		{
			return _genderStatus;
		}
		set
		{
			_genderStatus = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("GenderId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("GenderName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("GenderStatus", typeof(System.Boolean) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (GenderId == null)
		dr["GenderId"] = DBNull.Value;
		else
		dr["GenderId"] = GenderId;
		
		if (GenderName == null)
		dr["GenderName"] = DBNull.Value;
		else
		dr["GenderName"] = GenderName;
		
		if (GenderStatus == null)
		dr["GenderStatus"] = DBNull.Value;
		else
		dr["GenderStatus"] = GenderStatus;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		GenderId = dr["GenderId"] != DBNull.Value ? Convert.ToInt32(dr["GenderId"]) : GenderId = null;
		GenderName = dr["GenderName"] != DBNull.Value ? Convert.ToString(dr["GenderName"]) : GenderName = null;
		GenderStatus = dr["GenderStatus"] != DBNull.Value ? Convert.ToBoolean(dr["GenderStatus"]) : GenderStatus = null;
	}
	
	public static GenGenders[] MapFrom(DataSet ds)
	{
		List<GenGenders> objects;
		
		
		// Initialise Collection.
		objects = new List<GenGenders>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[gen_Genders] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[gen_Genders] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			GenGenders instance = new GenGenders();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static GenGenders Get(System.Int32 genderId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		GenGenders instance;
		
		
		instance = new GenGenders();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGenders_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, genderId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenGenders ID:" + genderId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.String genderName, System.Boolean? genderStatus, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGenders_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, genderName, genderStatus);
		
		if (transaction == null)
		this.GenderId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.GenderId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.String genderName, System.Boolean? genderStatus)
	{
		Insert(genderName, genderStatus, null);
	}
	/// <summary>
	/// Insert current GenGenders to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(GenderName, GenderStatus, transaction);
	}
	
	/// <summary>
	/// Insert current GenGenders to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? genderId, System.String genderName, System.Boolean? genderStatus, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGenders_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@genderId"].Value = genderId;
		dbCommand.Parameters["@genderName"].Value = genderName;
		dbCommand.Parameters["@genderStatus"].Value = genderStatus;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? genderId, System.String genderName, System.Boolean? genderStatus)
	{
		Update(genderId, genderName, genderStatus, null);
	}
	
	public static void Update(GenGenders genGenders)
	{
		genGenders.Update();
	}
	
	public static void Update(GenGenders genGenders, DbTransaction transaction)
	{
		genGenders.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGenders_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@genderId"].SourceColumn = "GenderId";
		dbCommand.Parameters["@genderName"].SourceColumn = "GenderName";
		dbCommand.Parameters["@genderStatus"].SourceColumn = "GenderStatus";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? genderId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGenders_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, genderId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? genderId)
	{
		Delete(
		genderId);
	}
	
	/// <summary>
	/// Delete current GenGenders from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGenders_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, GenderId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.GenderId = null;
	}
	
	/// <summary>
	/// Delete current GenGenders from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static GenGenders[] Search(System.Int32? genderId, System.String genderName, System.Boolean? genderStatus)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGenders_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, genderId, genderName, genderStatus);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return GenGenders.MapFrom(ds);
	}
	
	
	public static GenGenders[] Search(GenGenders searchObject)
	{
		return Search ( searchObject.GenderId, searchObject.GenderName, searchObject.GenderStatus);
	}
	
	/// <summary>
	/// Returns all GenGenders objects.
	/// </summary>
	/// <returns>List of all GenGenders objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static GenGenders[] Search()
	{
		return Search ( null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

