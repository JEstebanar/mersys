/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 12/03/2013 10:51:49 AM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class GenGeneralAddresses
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[gen_GeneralAddresses]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _addressId;
	private System.Int32? _generalId;
	private System.String _address1;
	private System.String _address2;
	private System.Int32? _cityId;
	private System.Boolean? _addressStatus;
	private System.Int32? _createdBy;
	private System.DateTime? _createdDate;
	private System.Int32? _modifiedBy;
	private System.DateTime? _modifiedDate;
	
	#endregion
	
	
	#region Properties
	public System.Int32? AddressId
	{
		get
		{
			return _addressId;
		}
		set
		{
			_addressId = value;
		}
	}
	
	public System.Int32? GeneralId
	{
		get
		{
			return _generalId;
		}
		set
		{
			_generalId = value;
		}
	}
	
	public System.String Address1
	{
		get
		{
			return _address1;
		}
		set
		{
			_address1 = value;
		}
	}
	
	public System.String Address2
	{
		get
		{
			return _address2;
		}
		set
		{
			_address2 = value;
		}
	}
	
	public System.Int32? CityId
	{
		get
		{
			return _cityId;
		}
		set
		{
			_cityId = value;
		}
	}
	
	public System.Boolean? AddressStatus
	{
		get
		{
			return _addressStatus;
		}
		set
		{
			_addressStatus = value;
		}
	}
	
	public System.Int32? CreatedBy
	{
		get
		{
			return _createdBy;
		}
		set
		{
			_createdBy = value;
		}
	}
	
	public System.DateTime? CreatedDate
	{
		get
		{
			return _createdDate;
		}
		set
		{
			_createdDate = value;
		}
	}
	
	public System.Int32? ModifiedBy
	{
		get
		{
			return _modifiedBy;
		}
		set
		{
			_modifiedBy = value;
		}
	}
	
	public System.DateTime? ModifiedDate
	{
		get
		{
			return _modifiedDate;
		}
		set
		{
			_modifiedDate = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("AddressId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("GeneralId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("Address1", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("Address2", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("CityId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("AddressStatus", typeof(System.Boolean) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (AddressId == null)
		dr["AddressId"] = DBNull.Value;
		else
		dr["AddressId"] = AddressId;
		
		if (GeneralId == null)
		dr["GeneralId"] = DBNull.Value;
		else
		dr["GeneralId"] = GeneralId;
		
		if (Address1 == null)
		dr["Address1"] = DBNull.Value;
		else
		dr["Address1"] = Address1;
		
		if (Address2 == null)
		dr["Address2"] = DBNull.Value;
		else
		dr["Address2"] = Address2;
		
		if (CityId == null)
		dr["CityId"] = DBNull.Value;
		else
		dr["CityId"] = CityId;
		
		if (AddressStatus == null)
		dr["AddressStatus"] = DBNull.Value;
		else
		dr["AddressStatus"] = AddressStatus;
		
		if (CreatedBy == null)
		dr["CreatedBy"] = DBNull.Value;
		else
		dr["CreatedBy"] = CreatedBy;
		
		if (CreatedDate == null)
		dr["CreatedDate"] = DBNull.Value;
		else
		dr["CreatedDate"] = CreatedDate;
		
		if (ModifiedBy == null)
		dr["ModifiedBy"] = DBNull.Value;
		else
		dr["ModifiedBy"] = ModifiedBy;
		
		if (ModifiedDate == null)
		dr["ModifiedDate"] = DBNull.Value;
		else
		dr["ModifiedDate"] = ModifiedDate;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		AddressId = dr["AddressId"] != DBNull.Value ? Convert.ToInt32(dr["AddressId"]) : AddressId = null;
		GeneralId = dr["GeneralId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralId"]) : GeneralId = null;
		Address1 = dr["Address1"] != DBNull.Value ? Convert.ToString(dr["Address1"]) : Address1 = null;
		Address2 = dr["Address2"] != DBNull.Value ? Convert.ToString(dr["Address2"]) : Address2 = null;
		CityId = dr["CityId"] != DBNull.Value ? Convert.ToInt32(dr["CityId"]) : CityId = null;
		AddressStatus = dr["AddressStatus"] != DBNull.Value ? Convert.ToBoolean(dr["AddressStatus"]) : AddressStatus = null;
		CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : CreatedBy = null;
		CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
		ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
		ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
	}
	
	public static GenGeneralAddresses[] MapFrom(DataSet ds)
	{
		List<GenGeneralAddresses> objects;
		
		
		// Initialise Collection.
		objects = new List<GenGeneralAddresses>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[gen_GeneralAddresses] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[gen_GeneralAddresses] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			GenGeneralAddresses instance = new GenGeneralAddresses();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static GenGeneralAddresses Get(System.Int32 addressId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		GenGeneralAddresses instance;
		
		
		instance = new GenGeneralAddresses();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGeneralAddresses_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, addressId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenGeneralAddresses ID:" + addressId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.Int32? generalId, System.String address1, System.String address2, System.Int32? cityId, System.Boolean? addressStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGeneralAddresses_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, generalId, address1, address2, cityId, addressStatus, createdBy, createdDate, modifiedBy, modifiedDate);
		
		if (transaction == null)
		this.AddressId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.AddressId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? generalId, System.String address1, System.String address2, System.Int32? cityId, System.Boolean? addressStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Insert(generalId, address1, address2, cityId, addressStatus, createdBy, createdDate, modifiedBy, modifiedDate, null);
	}
	/// <summary>
	/// Insert current GenGeneralAddresses to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(GeneralId, Address1, Address2, CityId, AddressStatus, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, transaction);
	}
	
	/// <summary>
	/// Insert current GenGeneralAddresses to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? addressId, System.Int32? generalId, System.String address1, System.String address2, System.Int32? cityId, System.Boolean? addressStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGeneralAddresses_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@addressId"].Value = addressId;
		dbCommand.Parameters["@generalId"].Value = generalId;
		dbCommand.Parameters["@address1"].Value = address1;
		dbCommand.Parameters["@address2"].Value = address2;
		dbCommand.Parameters["@cityId"].Value = cityId;
		dbCommand.Parameters["@addressStatus"].Value = addressStatus;
		dbCommand.Parameters["@createdBy"].Value = createdBy;
		dbCommand.Parameters["@createdDate"].Value = createdDate;
		dbCommand.Parameters["@modifiedBy"].Value = modifiedBy;
		dbCommand.Parameters["@modifiedDate"].Value = modifiedDate;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? addressId, System.Int32? generalId, System.String address1, System.String address2, System.Int32? cityId, System.Boolean? addressStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Update(addressId, generalId, address1, address2, cityId, addressStatus, createdBy, createdDate, modifiedBy, modifiedDate, null);
	}
	
	public static void Update(GenGeneralAddresses genGeneralAddresses)
	{
		genGeneralAddresses.Update();
	}
	
	public static void Update(GenGeneralAddresses genGeneralAddresses, DbTransaction transaction)
	{
		genGeneralAddresses.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGeneralAddresses_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@addressId"].SourceColumn = "AddressId";
		dbCommand.Parameters["@generalId"].SourceColumn = "GeneralId";
		dbCommand.Parameters["@address1"].SourceColumn = "Address1";
		dbCommand.Parameters["@address2"].SourceColumn = "Address2";
		dbCommand.Parameters["@cityId"].SourceColumn = "CityId";
		dbCommand.Parameters["@addressStatus"].SourceColumn = "AddressStatus";
		dbCommand.Parameters["@createdBy"].SourceColumn = "CreatedBy";
		dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
		dbCommand.Parameters["@modifiedBy"].SourceColumn = "ModifiedBy";
		dbCommand.Parameters["@modifiedDate"].SourceColumn = "ModifiedDate";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? addressId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGeneralAddresses_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, addressId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? addressId)
	{
		Delete(
		addressId);
	}
	
	/// <summary>
	/// Delete current GenGeneralAddresses from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGeneralAddresses_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, AddressId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.AddressId = null;
	}
	
	/// <summary>
	/// Delete current GenGeneralAddresses from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static GenGeneralAddresses[] Search(System.Int32? addressId, System.Int32? generalId, System.String address1, System.String address2, System.Int32? cityId, System.Boolean? addressStatus, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenGeneralAddresses_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, addressId, generalId, address1, address2, cityId, addressStatus, createdBy, createdDate, modifiedBy, modifiedDate);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return GenGeneralAddresses.MapFrom(ds);
	}
	
	
	public static GenGeneralAddresses[] Search(GenGeneralAddresses searchObject)
	{
		return Search ( searchObject.AddressId, searchObject.GeneralId, searchObject.Address1, searchObject.Address2, searchObject.CityId, searchObject.AddressStatus, searchObject.CreatedBy, searchObject.CreatedDate, searchObject.ModifiedBy, searchObject.ModifiedDate);
	}
	
	/// <summary>
	/// Returns all GenGeneralAddresses objects.
	/// </summary>
	/// <returns>List of all GenGeneralAddresses objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static GenGeneralAddresses[] Search()
	{
		return Search ( null, null, null, null, null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

