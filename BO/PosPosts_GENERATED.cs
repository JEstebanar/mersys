/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 05/12/2014 11:35:22 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class PosPosts
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[pos_Posts]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _postId;
	private System.String _title;
	private System.String _body;
	private System.Int32? _postTypeId;
	private System.String _postImage;
	private System.String _author;
	private System.String _tags;
	private System.DateTime? _endDate;
	private System.Int32? _visits;
	private System.Int32? _centerId;
	private System.Int32? _createdBy;
	private System.DateTime? _createdDate;
	private System.Int32? _modifiedBy;
	private System.DateTime? _modifiedDate;
	
	#endregion
	
	
	#region Properties
	public System.Int32? PostId
	{
		get
		{
			return _postId;
		}
		set
		{
			_postId = value;
		}
	}
	
	public System.String Title
	{
		get
		{
			return _title;
		}
		set
		{
			_title = value;
		}
	}
	
	public System.String Body
	{
		get
		{
			return _body;
		}
		set
		{
			_body = value;
		}
	}
	
	public System.Int32? PostTypeId
	{
		get
		{
			return _postTypeId;
		}
		set
		{
			_postTypeId = value;
		}
	}
	
	public System.String PostImage
	{
		get
		{
			return _postImage;
		}
		set
		{
			_postImage = value;
		}
	}
	
	public System.String Author
	{
		get
		{
			return _author;
		}
		set
		{
			_author = value;
		}
	}
	
	public System.String Tags
	{
		get
		{
			return _tags;
		}
		set
		{
			_tags = value;
		}
	}
	
	public System.DateTime? EndDate
	{
		get
		{
			return _endDate;
		}
		set
		{
			_endDate = value;
		}
	}
	
	public System.Int32? Visits
	{
		get
		{
			return _visits;
		}
		set
		{
			_visits = value;
		}
	}
	
	public System.Int32? CenterId
	{
		get
		{
			return _centerId;
		}
		set
		{
			_centerId = value;
		}
	}
	
	public System.Int32? CreatedBy
	{
		get
		{
			return _createdBy;
		}
		set
		{
			_createdBy = value;
		}
	}
	
	public System.DateTime? CreatedDate
	{
		get
		{
			return _createdDate;
		}
		set
		{
			_createdDate = value;
		}
	}
	
	public System.Int32? ModifiedBy
	{
		get
		{
			return _modifiedBy;
		}
		set
		{
			_modifiedBy = value;
		}
	}
	
	public System.DateTime? ModifiedDate
	{
		get
		{
			return _modifiedDate;
		}
		set
		{
			_modifiedDate = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("PostId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("Title", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("Body", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("PostTypeId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("PostImage", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("Author", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("Tags", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("EndDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("Visits", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (PostId == null)
		dr["PostId"] = DBNull.Value;
		else
		dr["PostId"] = PostId;
		
		if (Title == null)
		dr["Title"] = DBNull.Value;
		else
		dr["Title"] = Title;
		
		if (Body == null)
		dr["Body"] = DBNull.Value;
		else
		dr["Body"] = Body;
		
		if (PostTypeId == null)
		dr["PostTypeId"] = DBNull.Value;
		else
		dr["PostTypeId"] = PostTypeId;
		
		if (PostImage == null)
		dr["PostImage"] = DBNull.Value;
		else
		dr["PostImage"] = PostImage;
		
		if (Author == null)
		dr["Author"] = DBNull.Value;
		else
		dr["Author"] = Author;
		
		if (Tags == null)
		dr["Tags"] = DBNull.Value;
		else
		dr["Tags"] = Tags;
		
		if (EndDate == null)
		dr["EndDate"] = DBNull.Value;
		else
		dr["EndDate"] = EndDate;
		
		if (Visits == null)
		dr["Visits"] = DBNull.Value;
		else
		dr["Visits"] = Visits;
		
		if (CenterId == null)
		dr["CenterId"] = DBNull.Value;
		else
		dr["CenterId"] = CenterId;
		
		if (CreatedBy == null)
		dr["CreatedBy"] = DBNull.Value;
		else
		dr["CreatedBy"] = CreatedBy;
		
		if (CreatedDate == null)
		dr["CreatedDate"] = DBNull.Value;
		else
		dr["CreatedDate"] = CreatedDate;
		
		if (ModifiedBy == null)
		dr["ModifiedBy"] = DBNull.Value;
		else
		dr["ModifiedBy"] = ModifiedBy;
		
		if (ModifiedDate == null)
		dr["ModifiedDate"] = DBNull.Value;
		else
		dr["ModifiedDate"] = ModifiedDate;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		PostId = dr["PostId"] != DBNull.Value ? Convert.ToInt32(dr["PostId"]) : PostId = null;
		Title = dr["Title"] != DBNull.Value ? Convert.ToString(dr["Title"]) : Title = null;
		Body = dr["Body"] != DBNull.Value ? Convert.ToString(dr["Body"]) : Body = null;
		PostTypeId = dr["PostTypeId"] != DBNull.Value ? Convert.ToInt32(dr["PostTypeId"]) : PostTypeId = null;
		PostImage = dr["PostImage"] != DBNull.Value ? Convert.ToString(dr["PostImage"]) : PostImage = null;
		Author = dr["Author"] != DBNull.Value ? Convert.ToString(dr["Author"]) : Author = null;
		Tags = dr["Tags"] != DBNull.Value ? Convert.ToString(dr["Tags"]) : Tags = null;
		EndDate = dr["EndDate"] != DBNull.Value ? Convert.ToDateTime(dr["EndDate"]) : EndDate = null;
		Visits = dr["Visits"] != DBNull.Value ? Convert.ToInt32(dr["Visits"]) : Visits = null;
		CenterId = dr["CenterId"] != DBNull.Value ? Convert.ToInt32(dr["CenterId"]) : CenterId = null;
		CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : CreatedBy = null;
		CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
		ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
		ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
	}
	
	public static PosPosts[] MapFrom(DataSet ds)
	{
		List<PosPosts> objects;
		
		
		// Initialise Collection.
		objects = new List<PosPosts>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[pos_Posts] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[pos_Posts] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			PosPosts instance = new PosPosts();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static PosPosts Get(System.Int32 postId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		PosPosts instance;
		
		
		instance = new PosPosts();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspPosPosts_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, postId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get PosPosts ID:" + postId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.String title, System.String body, System.Int32? postTypeId, System.String postImage, System.String author, System.String tags, System.DateTime? endDate, System.Int32? visits, System.Int32? centerId, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspPosPosts_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, title, body, postTypeId, postImage, author, tags, endDate, visits, centerId, createdBy, createdDate, modifiedBy, modifiedDate);
		
		if (transaction == null)
		this.PostId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.PostId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.String title, System.String body, System.Int32? postTypeId, System.String postImage, System.String author, System.String tags, System.DateTime? endDate, System.Int32? visits, System.Int32? centerId, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Insert(title, body, postTypeId, postImage, author, tags, endDate, visits, centerId, createdBy, createdDate, modifiedBy, modifiedDate, null);
	}
	/// <summary>
	/// Insert current PosPosts to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(Title, Body, PostTypeId, PostImage, Author, Tags, EndDate, Visits, CenterId, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, transaction);
	}
	
	/// <summary>
	/// Insert current PosPosts to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? postId, System.String title, System.String body, System.Int32? postTypeId, System.String postImage, System.String author, System.String tags, System.DateTime? endDate, System.Int32? visits, System.Int32? centerId, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspPosPosts_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@postId"].Value = postId;
		dbCommand.Parameters["@title"].Value = title;
		dbCommand.Parameters["@body"].Value = body;
		dbCommand.Parameters["@postTypeId"].Value = postTypeId;
		dbCommand.Parameters["@postImage"].Value = postImage;
		dbCommand.Parameters["@author"].Value = author;
		dbCommand.Parameters["@tags"].Value = tags;
		dbCommand.Parameters["@endDate"].Value = endDate;
		dbCommand.Parameters["@visits"].Value = visits;
		dbCommand.Parameters["@centerId"].Value = centerId;
		dbCommand.Parameters["@createdBy"].Value = createdBy;
		dbCommand.Parameters["@createdDate"].Value = createdDate;
		dbCommand.Parameters["@modifiedBy"].Value = modifiedBy;
		dbCommand.Parameters["@modifiedDate"].Value = modifiedDate;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? postId, System.String title, System.String body, System.Int32? postTypeId, System.String postImage, System.String author, System.String tags, System.DateTime? endDate, System.Int32? visits, System.Int32? centerId, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Update(postId, title, body, postTypeId, postImage, author, tags, endDate, visits, centerId, createdBy, createdDate, modifiedBy, modifiedDate, null);
	}
	
	public static void Update(PosPosts posPosts)
	{
		posPosts.Update();
	}
	
	public static void Update(PosPosts posPosts, DbTransaction transaction)
	{
		posPosts.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspPosPosts_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@postId"].SourceColumn = "PostId";
		dbCommand.Parameters["@title"].SourceColumn = "Title";
		dbCommand.Parameters["@body"].SourceColumn = "Body";
		dbCommand.Parameters["@postTypeId"].SourceColumn = "PostTypeId";
		dbCommand.Parameters["@postImage"].SourceColumn = "PostImage";
		dbCommand.Parameters["@author"].SourceColumn = "Author";
		dbCommand.Parameters["@tags"].SourceColumn = "Tags";
		dbCommand.Parameters["@endDate"].SourceColumn = "EndDate";
		dbCommand.Parameters["@visits"].SourceColumn = "Visits";
		dbCommand.Parameters["@centerId"].SourceColumn = "CenterId";
		dbCommand.Parameters["@createdBy"].SourceColumn = "CreatedBy";
		dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
		dbCommand.Parameters["@modifiedBy"].SourceColumn = "ModifiedBy";
		dbCommand.Parameters["@modifiedDate"].SourceColumn = "ModifiedDate";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? postId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspPosPosts_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, postId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? postId)
	{
		Delete(
		postId);
	}
	
	/// <summary>
	/// Delete current PosPosts from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspPosPosts_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, PostId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.PostId = null;
	}
	
	/// <summary>
	/// Delete current PosPosts from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static PosPosts[] Search(System.Int32? postId, System.String title, System.String body, System.Int32? postTypeId, System.String postImage, System.String author, System.String tags, System.DateTime? endDate, System.Int32? visits, System.Int32? centerId, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspPosPosts_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, postId, title, body, postTypeId, postImage, author, tags, endDate, visits, centerId, createdBy, createdDate, modifiedBy, modifiedDate);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return PosPosts.MapFrom(ds);
	}
	
	
	public static PosPosts[] Search(PosPosts searchObject)
	{
		return Search ( searchObject.PostId, searchObject.Title, searchObject.Body, searchObject.PostTypeId, searchObject.PostImage, searchObject.Author, searchObject.Tags, searchObject.EndDate, searchObject.Visits, searchObject.CenterId, searchObject.CreatedBy, searchObject.CreatedDate, searchObject.ModifiedBy, searchObject.ModifiedDate);
	}
	
	/// <summary>
	/// Returns all PosPosts objects.
	/// </summary>
	/// <returns>List of all PosPosts objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static PosPosts[] Search()
	{
		return Search ( null, null, null, null, null, null, null, null, null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

