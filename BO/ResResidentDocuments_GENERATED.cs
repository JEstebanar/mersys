/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 11/27/2013 10:59:45 AM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class ResResidentDocuments
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[res_ResidentDocuments]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _residentDocumentId;
	private System.Int32? _generalId;
	private System.String _documentNumber;
	private System.Int32? _documentTypeId;
	private System.DateTime? _expirationDate;
	private System.String _documentPath;
	private System.Int32? _createdBY;
	private System.DateTime? _createdDate;
	private System.Int32? _modifiedBy;
	private System.DateTime? _modifiedDate;
	
	#endregion
	
	
	#region Properties
	public System.Int32? ResidentDocumentId
	{
		get
		{
			return _residentDocumentId;
		}
		set
		{
			_residentDocumentId = value;
		}
	}
	
	public System.Int32? GeneralId
	{
		get
		{
			return _generalId;
		}
		set
		{
			_generalId = value;
		}
	}
	
	public System.String DocumentNumber
	{
		get
		{
			return _documentNumber;
		}
		set
		{
			_documentNumber = value;
		}
	}
	
	public System.Int32? DocumentTypeId
	{
		get
		{
			return _documentTypeId;
		}
		set
		{
			_documentTypeId = value;
		}
	}
	
	public System.DateTime? ExpirationDate
	{
		get
		{
			return _expirationDate;
		}
		set
		{
			_expirationDate = value;
		}
	}
	
	public System.String DocumentPath
	{
		get
		{
			return _documentPath;
		}
		set
		{
			_documentPath = value;
		}
	}
	
	public System.Int32? CreatedBY
	{
		get
		{
			return _createdBY;
		}
		set
		{
			_createdBY = value;
		}
	}
	
	public System.DateTime? CreatedDate
	{
		get
		{
			return _createdDate;
		}
		set
		{
			_createdDate = value;
		}
	}
	
	public System.Int32? ModifiedBy
	{
		get
		{
			return _modifiedBy;
		}
		set
		{
			_modifiedBy = value;
		}
	}
	
	public System.DateTime? ModifiedDate
	{
		get
		{
			return _modifiedDate;
		}
		set
		{
			_modifiedDate = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("ResidentDocumentId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("GeneralId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("DocumentNumber", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("DocumentTypeId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ExpirationDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("DocumentPath", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedBY", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (ResidentDocumentId == null)
		dr["ResidentDocumentId"] = DBNull.Value;
		else
		dr["ResidentDocumentId"] = ResidentDocumentId;
		
		if (GeneralId == null)
		dr["GeneralId"] = DBNull.Value;
		else
		dr["GeneralId"] = GeneralId;
		
		if (DocumentNumber == null)
		dr["DocumentNumber"] = DBNull.Value;
		else
		dr["DocumentNumber"] = DocumentNumber;
		
		if (DocumentTypeId == null)
		dr["DocumentTypeId"] = DBNull.Value;
		else
		dr["DocumentTypeId"] = DocumentTypeId;
		
		if (ExpirationDate == null)
		dr["ExpirationDate"] = DBNull.Value;
		else
		dr["ExpirationDate"] = ExpirationDate;
		
		if (DocumentPath == null)
		dr["DocumentPath"] = DBNull.Value;
		else
		dr["DocumentPath"] = DocumentPath;
		
		if (CreatedBY == null)
		dr["CreatedBY"] = DBNull.Value;
		else
		dr["CreatedBY"] = CreatedBY;
		
		if (CreatedDate == null)
		dr["CreatedDate"] = DBNull.Value;
		else
		dr["CreatedDate"] = CreatedDate;
		
		if (ModifiedBy == null)
		dr["ModifiedBy"] = DBNull.Value;
		else
		dr["ModifiedBy"] = ModifiedBy;
		
		if (ModifiedDate == null)
		dr["ModifiedDate"] = DBNull.Value;
		else
		dr["ModifiedDate"] = ModifiedDate;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		ResidentDocumentId = dr["ResidentDocumentId"] != DBNull.Value ? Convert.ToInt32(dr["ResidentDocumentId"]) : ResidentDocumentId = null;
		GeneralId = dr["GeneralId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralId"]) : GeneralId = null;
		DocumentNumber = dr["DocumentNumber"] != DBNull.Value ? Convert.ToString(dr["DocumentNumber"]) : DocumentNumber = null;
		DocumentTypeId = dr["DocumentTypeId"] != DBNull.Value ? Convert.ToInt32(dr["DocumentTypeId"]) : DocumentTypeId = null;
		ExpirationDate = dr["ExpirationDate"] != DBNull.Value ? Convert.ToDateTime(dr["ExpirationDate"]) : ExpirationDate = null;
		DocumentPath = dr["DocumentPath"] != DBNull.Value ? Convert.ToString(dr["DocumentPath"]) : DocumentPath = null;
		CreatedBY = dr["CreatedBY"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBY"]) : CreatedBY = null;
		CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
		ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
		ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
	}
	
	public static ResResidentDocuments[] MapFrom(DataSet ds)
	{
		List<ResResidentDocuments> objects;
		
		
		// Initialise Collection.
		objects = new List<ResResidentDocuments>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[res_ResidentDocuments] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[res_ResidentDocuments] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			ResResidentDocuments instance = new ResResidentDocuments();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static ResResidentDocuments Get(System.Int32 residentDocumentId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		ResResidentDocuments instance;
		
		
		instance = new ResResidentDocuments();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidentDocuments_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, residentDocumentId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get ResResidentDocuments ID:" + residentDocumentId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.Int32? generalId, System.String documentNumber, System.Int32? documentTypeId, System.DateTime? expirationDate, System.String documentPath, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidentDocuments_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, generalId, documentNumber, documentTypeId, expirationDate, documentPath, createdBY, createdDate, modifiedBy, modifiedDate);
		
		if (transaction == null)
		this.ResidentDocumentId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.ResidentDocumentId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? generalId, System.String documentNumber, System.Int32? documentTypeId, System.DateTime? expirationDate, System.String documentPath, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Insert(generalId, documentNumber, documentTypeId, expirationDate, documentPath, createdBY, createdDate, modifiedBy, modifiedDate, null);
	}
	/// <summary>
	/// Insert current ResResidentDocuments to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(GeneralId, DocumentNumber, DocumentTypeId, ExpirationDate, DocumentPath, CreatedBY, CreatedDate, ModifiedBy, ModifiedDate, transaction);
	}
	
	/// <summary>
	/// Insert current ResResidentDocuments to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? residentDocumentId, System.Int32? generalId, System.String documentNumber, System.Int32? documentTypeId, System.DateTime? expirationDate, System.String documentPath, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidentDocuments_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@residentDocumentId"].Value = residentDocumentId;
		dbCommand.Parameters["@generalId"].Value = generalId;
		dbCommand.Parameters["@documentNumber"].Value = documentNumber;
		dbCommand.Parameters["@documentTypeId"].Value = documentTypeId;
		dbCommand.Parameters["@expirationDate"].Value = expirationDate;
		dbCommand.Parameters["@documentPath"].Value = documentPath;
		dbCommand.Parameters["@createdBY"].Value = createdBY;
		dbCommand.Parameters["@createdDate"].Value = createdDate;
		dbCommand.Parameters["@modifiedBy"].Value = modifiedBy;
		dbCommand.Parameters["@modifiedDate"].Value = modifiedDate;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? residentDocumentId, System.Int32? generalId, System.String documentNumber, System.Int32? documentTypeId, System.DateTime? expirationDate, System.String documentPath, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Update(residentDocumentId, generalId, documentNumber, documentTypeId, expirationDate, documentPath, createdBY, createdDate, modifiedBy, modifiedDate, null);
	}
	
	public static void Update(ResResidentDocuments resResidentDocuments)
	{
		resResidentDocuments.Update();
	}
	
	public static void Update(ResResidentDocuments resResidentDocuments, DbTransaction transaction)
	{
		resResidentDocuments.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidentDocuments_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@residentDocumentId"].SourceColumn = "ResidentDocumentId";
		dbCommand.Parameters["@generalId"].SourceColumn = "GeneralId";
		dbCommand.Parameters["@documentNumber"].SourceColumn = "DocumentNumber";
		dbCommand.Parameters["@documentTypeId"].SourceColumn = "DocumentTypeId";
		dbCommand.Parameters["@expirationDate"].SourceColumn = "ExpirationDate";
		dbCommand.Parameters["@documentPath"].SourceColumn = "DocumentPath";
		dbCommand.Parameters["@createdBY"].SourceColumn = "CreatedBY";
		dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
		dbCommand.Parameters["@modifiedBy"].SourceColumn = "ModifiedBy";
		dbCommand.Parameters["@modifiedDate"].SourceColumn = "ModifiedDate";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? residentDocumentId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidentDocuments_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, residentDocumentId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? residentDocumentId)
	{
		Delete(
		residentDocumentId);
	}
	
	/// <summary>
	/// Delete current ResResidentDocuments from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidentDocuments_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, ResidentDocumentId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.ResidentDocumentId = null;
	}
	
	/// <summary>
	/// Delete current ResResidentDocuments from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static ResResidentDocuments[] Search(System.Int32? residentDocumentId, System.Int32? generalId, System.String documentNumber, System.Int32? documentTypeId, System.DateTime? expirationDate, System.String documentPath, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidentDocuments_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, residentDocumentId, generalId, documentNumber, documentTypeId, expirationDate, documentPath, createdBY, createdDate, modifiedBy, modifiedDate);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return ResResidentDocuments.MapFrom(ds);
	}
	
	
	public static ResResidentDocuments[] Search(ResResidentDocuments searchObject)
	{
		return Search ( searchObject.ResidentDocumentId, searchObject.GeneralId, searchObject.DocumentNumber, searchObject.DocumentTypeId, searchObject.ExpirationDate, searchObject.DocumentPath, searchObject.CreatedBY, searchObject.CreatedDate, searchObject.ModifiedBy, searchObject.ModifiedDate);
	}
	
	/// <summary>
	/// Returns all ResResidentDocuments objects.
	/// </summary>
	/// <returns>List of all ResResidentDocuments objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static ResResidentDocuments[] Search()
	{
		return Search ( null, null, null, null, null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

