/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 09/08/2013 11:06:07 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class ResGrades
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[res_Grades]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _gradeId;
	private System.String _gradeName;
	private System.Boolean? _gradeStatus;
	
	#endregion
	
	
	#region Properties
	public System.Int32? GradeId
	{
		get
		{
			return _gradeId;
		}
		set
		{
			_gradeId = value;
		}
	}
	
	public System.String GradeName
	{
		get
		{
			return _gradeName;
		}
		set
		{
			_gradeName = value;
		}
	}
	
	public System.Boolean? GradeStatus
	{
		get
		{
			return _gradeStatus;
		}
		set
		{
			_gradeStatus = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("GradeId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("GradeName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("GradeStatus", typeof(System.Boolean) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (GradeId == null)
		dr["GradeId"] = DBNull.Value;
		else
		dr["GradeId"] = GradeId;
		
		if (GradeName == null)
		dr["GradeName"] = DBNull.Value;
		else
		dr["GradeName"] = GradeName;
		
		if (GradeStatus == null)
		dr["GradeStatus"] = DBNull.Value;
		else
		dr["GradeStatus"] = GradeStatus;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		GradeId = dr["GradeId"] != DBNull.Value ? Convert.ToInt32(dr["GradeId"]) : GradeId = null;
		GradeName = dr["GradeName"] != DBNull.Value ? Convert.ToString(dr["GradeName"]) : GradeName = null;
		GradeStatus = dr["GradeStatus"] != DBNull.Value ? Convert.ToBoolean(dr["GradeStatus"]) : GradeStatus = null;
	}
	
	public static ResGrades[] MapFrom(DataSet ds)
	{
		List<ResGrades> objects;
		
		
		// Initialise Collection.
		objects = new List<ResGrades>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[res_Grades] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[res_Grades] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			ResGrades instance = new ResGrades();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static ResGrades Get(System.Int32 gradeId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		ResGrades instance;
		
		
		instance = new ResGrades();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResGrades_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, gradeId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get ResGrades ID:" + gradeId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.String gradeName, System.Boolean? gradeStatus, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResGrades_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, gradeName, gradeStatus);
		
		if (transaction == null)
		this.GradeId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.GradeId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.String gradeName, System.Boolean? gradeStatus)
	{
		Insert(gradeName, gradeStatus, null);
	}
	/// <summary>
	/// Insert current ResGrades to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(GradeName, GradeStatus, transaction);
	}
	
	/// <summary>
	/// Insert current ResGrades to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? gradeId, System.String gradeName, System.Boolean? gradeStatus, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResGrades_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@gradeId"].Value = gradeId;
		dbCommand.Parameters["@gradeName"].Value = gradeName;
		dbCommand.Parameters["@gradeStatus"].Value = gradeStatus;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? gradeId, System.String gradeName, System.Boolean? gradeStatus)
	{
		Update(gradeId, gradeName, gradeStatus, null);
	}
	
	public static void Update(ResGrades resGrades)
	{
		resGrades.Update();
	}
	
	public static void Update(ResGrades resGrades, DbTransaction transaction)
	{
		resGrades.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResGrades_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@gradeId"].SourceColumn = "GradeId";
		dbCommand.Parameters["@gradeName"].SourceColumn = "GradeName";
		dbCommand.Parameters["@gradeStatus"].SourceColumn = "GradeStatus";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? gradeId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResGrades_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, gradeId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? gradeId)
	{
		Delete(
		gradeId);
	}
	
	/// <summary>
	/// Delete current ResGrades from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResGrades_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, GradeId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.GradeId = null;
	}
	
	/// <summary>
	/// Delete current ResGrades from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static ResGrades[] Search(System.Int32? gradeId, System.String gradeName, System.Boolean? gradeStatus)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResGrades_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, gradeId, gradeName, gradeStatus);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return ResGrades.MapFrom(ds);
	}
	
	
	public static ResGrades[] Search(ResGrades searchObject)
	{
		return Search ( searchObject.GradeId, searchObject.GradeName, searchObject.GradeStatus);
	}
	
	/// <summary>
	/// Returns all ResGrades objects.
	/// </summary>
	/// <returns>List of all ResGrades objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static ResGrades[] Search()
	{
		return Search ( null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

