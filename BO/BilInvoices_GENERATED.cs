/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 12/12/2013 10:03:07 AM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class BilInvoices
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[bil_Invoices]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _invoiceId;
	private System.Int32? _generalId;
	private System.Int32? _invoiceRequestTypeId;
	private System.DateTime? _createdDate;
	private System.Boolean? _invoiceStatus;
	private System.Int32? _centerId;
	
	#endregion
	
	
	#region Properties
	public System.Int32? InvoiceId
	{
		get
		{
			return _invoiceId;
		}
		set
		{
			_invoiceId = value;
		}
	}
	
	public System.Int32? GeneralId
	{
		get
		{
			return _generalId;
		}
		set
		{
			_generalId = value;
		}
	}
	
	public System.Int32? InvoiceRequestTypeId
	{
		get
		{
			return _invoiceRequestTypeId;
		}
		set
		{
			_invoiceRequestTypeId = value;
		}
	}
	
	public System.DateTime? CreatedDate
	{
		get
		{
			return _createdDate;
		}
		set
		{
			_createdDate = value;
		}
	}
	
	public System.Boolean? InvoiceStatus
	{
		get
		{
			return _invoiceStatus;
		}
		set
		{
			_invoiceStatus = value;
		}
	}
	
	public System.Int32? CenterId
	{
		get
		{
			return _centerId;
		}
		set
		{
			_centerId = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("InvoiceId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("GeneralId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("InvoiceRequestTypeId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("InvoiceStatus", typeof(System.Boolean) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterId", typeof(System.Int32) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (InvoiceId == null)
		dr["InvoiceId"] = DBNull.Value;
		else
		dr["InvoiceId"] = InvoiceId;
		
		if (GeneralId == null)
		dr["GeneralId"] = DBNull.Value;
		else
		dr["GeneralId"] = GeneralId;
		
		if (InvoiceRequestTypeId == null)
		dr["InvoiceRequestTypeId"] = DBNull.Value;
		else
		dr["InvoiceRequestTypeId"] = InvoiceRequestTypeId;
		
		if (CreatedDate == null)
		dr["CreatedDate"] = DBNull.Value;
		else
		dr["CreatedDate"] = CreatedDate;
		
		if (InvoiceStatus == null)
		dr["InvoiceStatus"] = DBNull.Value;
		else
		dr["InvoiceStatus"] = InvoiceStatus;
		
		if (CenterId == null)
		dr["CenterId"] = DBNull.Value;
		else
		dr["CenterId"] = CenterId;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		InvoiceId = dr["InvoiceId"] != DBNull.Value ? Convert.ToInt32(dr["InvoiceId"]) : InvoiceId = null;
		GeneralId = dr["GeneralId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralId"]) : GeneralId = null;
		InvoiceRequestTypeId = dr["InvoiceRequestTypeId"] != DBNull.Value ? Convert.ToInt32(dr["InvoiceRequestTypeId"]) : InvoiceRequestTypeId = null;
		CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
		InvoiceStatus = dr["InvoiceStatus"] != DBNull.Value ? Convert.ToBoolean(dr["InvoiceStatus"]) : InvoiceStatus = null;
		CenterId = dr["CenterId"] != DBNull.Value ? Convert.ToInt32(dr["CenterId"]) : CenterId = null;
	}
	
	public static BilInvoices[] MapFrom(DataSet ds)
	{
		List<BilInvoices> objects;
		
		
		// Initialise Collection.
		objects = new List<BilInvoices>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[bil_Invoices] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[bil_Invoices] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			BilInvoices instance = new BilInvoices();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static BilInvoices Get(System.Int32 invoiceId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		BilInvoices instance;
		
		
		instance = new BilInvoices();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspBilInvoices_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, invoiceId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get BilInvoices ID:" + invoiceId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.Int32? generalId, System.Int32? invoiceRequestTypeId, System.DateTime? createdDate, System.Boolean? invoiceStatus, System.Int32? centerId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspBilInvoices_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, generalId, invoiceRequestTypeId, createdDate, invoiceStatus, centerId);
		
		if (transaction == null)
		this.InvoiceId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.InvoiceId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? generalId, System.Int32? invoiceRequestTypeId, System.DateTime? createdDate, System.Boolean? invoiceStatus, System.Int32? centerId)
	{
		Insert(generalId, invoiceRequestTypeId, createdDate, invoiceStatus, centerId, null);
	}
	/// <summary>
	/// Insert current BilInvoices to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(GeneralId, InvoiceRequestTypeId, CreatedDate, InvoiceStatus, CenterId, transaction);
	}
	
	/// <summary>
	/// Insert current BilInvoices to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? invoiceId, System.Int32? generalId, System.Int32? invoiceRequestTypeId, System.DateTime? createdDate, System.Boolean? invoiceStatus, System.Int32? centerId, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspBilInvoices_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@invoiceId"].Value = invoiceId;
		dbCommand.Parameters["@generalId"].Value = generalId;
		dbCommand.Parameters["@invoiceRequestTypeId"].Value = invoiceRequestTypeId;
		dbCommand.Parameters["@createdDate"].Value = createdDate;
		dbCommand.Parameters["@invoiceStatus"].Value = invoiceStatus;
		dbCommand.Parameters["@centerId"].Value = centerId;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? invoiceId, System.Int32? generalId, System.Int32? invoiceRequestTypeId, System.DateTime? createdDate, System.Boolean? invoiceStatus, System.Int32? centerId)
	{
		Update(invoiceId, generalId, invoiceRequestTypeId, createdDate, invoiceStatus, centerId, null);
	}
	
	public static void Update(BilInvoices bilInvoices)
	{
		bilInvoices.Update();
	}
	
	public static void Update(BilInvoices bilInvoices, DbTransaction transaction)
	{
		bilInvoices.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspBilInvoices_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@invoiceId"].SourceColumn = "InvoiceId";
		dbCommand.Parameters["@generalId"].SourceColumn = "GeneralId";
		dbCommand.Parameters["@invoiceRequestTypeId"].SourceColumn = "InvoiceRequestTypeId";
		dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
		dbCommand.Parameters["@invoiceStatus"].SourceColumn = "InvoiceStatus";
		dbCommand.Parameters["@centerId"].SourceColumn = "CenterId";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? invoiceId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspBilInvoices_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, invoiceId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? invoiceId)
	{
		Delete(
		invoiceId);
	}
	
	/// <summary>
	/// Delete current BilInvoices from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspBilInvoices_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, InvoiceId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.InvoiceId = null;
	}
	
	/// <summary>
	/// Delete current BilInvoices from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static BilInvoices[] Search(System.Int32? invoiceId, System.Int32? generalId, System.Int32? invoiceRequestTypeId, System.DateTime? createdDate, System.Boolean? invoiceStatus, System.Int32? centerId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspBilInvoices_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, invoiceId, generalId, invoiceRequestTypeId, createdDate, invoiceStatus, centerId);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return BilInvoices.MapFrom(ds);
	}
	
	
	public static BilInvoices[] Search(BilInvoices searchObject)
	{
		return Search ( searchObject.InvoiceId, searchObject.GeneralId, searchObject.InvoiceRequestTypeId, searchObject.CreatedDate, searchObject.InvoiceStatus, searchObject.CenterId);
	}
	
	/// <summary>
	/// Returns all BilInvoices objects.
	/// </summary>
	/// <returns>List of all BilInvoices objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static BilInvoices[] Search()
	{
		return Search ( null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

