/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 04/18/2014 10:23:42 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class LetLetters
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[let_Letters]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _letterId;
	private System.String _letterName;
	private System.Boolean? _letterStatus;
	
	#endregion
	
	
	#region Properties
	public System.Int32? LetterId
	{
		get
		{
			return _letterId;
		}
		set
		{
			_letterId = value;
		}
	}
	
	public System.String LetterName
	{
		get
		{
			return _letterName;
		}
		set
		{
			_letterName = value;
		}
	}
	
	public System.Boolean? LetterStatus
	{
		get
		{
			return _letterStatus;
		}
		set
		{
			_letterStatus = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("LetterId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("LetterName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("LetterStatus", typeof(System.Boolean) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (LetterId == null)
		dr["LetterId"] = DBNull.Value;
		else
		dr["LetterId"] = LetterId;
		
		if (LetterName == null)
		dr["LetterName"] = DBNull.Value;
		else
		dr["LetterName"] = LetterName;
		
		if (LetterStatus == null)
		dr["LetterStatus"] = DBNull.Value;
		else
		dr["LetterStatus"] = LetterStatus;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		LetterId = dr["LetterId"] != DBNull.Value ? Convert.ToInt32(dr["LetterId"]) : LetterId = null;
		LetterName = dr["LetterName"] != DBNull.Value ? Convert.ToString(dr["LetterName"]) : LetterName = null;
		LetterStatus = dr["LetterStatus"] != DBNull.Value ? Convert.ToBoolean(dr["LetterStatus"]) : LetterStatus = null;
	}
	
	public static LetLetters[] MapFrom(DataSet ds)
	{
		List<LetLetters> objects;
		
		
		// Initialise Collection.
		objects = new List<LetLetters>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[let_Letters] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[let_Letters] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			LetLetters instance = new LetLetters();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static LetLetters Get(System.Int32 letterId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		LetLetters instance;
		
		
		instance = new LetLetters();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspLetLetters_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, letterId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get LetLetters ID:" + letterId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.String letterName, System.Boolean? letterStatus, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspLetLetters_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, letterName, letterStatus);
		
		if (transaction == null)
		this.LetterId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.LetterId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.String letterName, System.Boolean? letterStatus)
	{
		Insert(letterName, letterStatus, null);
	}
	/// <summary>
	/// Insert current LetLetters to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(LetterName, LetterStatus, transaction);
	}
	
	/// <summary>
	/// Insert current LetLetters to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? letterId, System.String letterName, System.Boolean? letterStatus, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspLetLetters_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@letterId"].Value = letterId;
		dbCommand.Parameters["@letterName"].Value = letterName;
		dbCommand.Parameters["@letterStatus"].Value = letterStatus;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? letterId, System.String letterName, System.Boolean? letterStatus)
	{
		Update(letterId, letterName, letterStatus, null);
	}
	
	public static void Update(LetLetters letLetters)
	{
		letLetters.Update();
	}
	
	public static void Update(LetLetters letLetters, DbTransaction transaction)
	{
		letLetters.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspLetLetters_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@letterId"].SourceColumn = "LetterId";
		dbCommand.Parameters["@letterName"].SourceColumn = "LetterName";
		dbCommand.Parameters["@letterStatus"].SourceColumn = "LetterStatus";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? letterId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspLetLetters_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, letterId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? letterId)
	{
		Delete(
		letterId);
	}
	
	/// <summary>
	/// Delete current LetLetters from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspLetLetters_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, LetterId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.LetterId = null;
	}
	
	/// <summary>
	/// Delete current LetLetters from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static LetLetters[] Search(System.Int32? letterId, System.String letterName, System.Boolean? letterStatus)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspLetLetters_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, letterId, letterName, letterStatus);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return LetLetters.MapFrom(ds);
	}
	
	
	public static LetLetters[] Search(LetLetters searchObject)
	{
		return Search ( searchObject.LetterId, searchObject.LetterName, searchObject.LetterStatus);
	}
	
	/// <summary>
	/// Returns all LetLetters objects.
	/// </summary>
	/// <returns>List of all LetLetters objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static LetLetters[] Search()
	{
		return Search ( null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

