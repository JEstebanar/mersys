/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 11/24/2013 3:04:36 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class EvaEvaluationResidences
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[eva_EvaluationResidences]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _evaluationId;
	private System.Int32? _residenceId;
	
	#endregion
	
	
	#region Properties
	public System.Int32? EvaluationId
	{
		get
		{
			return _evaluationId;
		}
		set
		{
			_evaluationId = value;
		}
	}
	
	public System.Int32? ResidenceId
	{
		get
		{
			return _residenceId;
		}
		set
		{
			_residenceId = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("EvaluationId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ResidenceId", typeof(System.Int32) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (EvaluationId == null)
		dr["EvaluationId"] = DBNull.Value;
		else
		dr["EvaluationId"] = EvaluationId;
		
		if (ResidenceId == null)
		dr["ResidenceId"] = DBNull.Value;
		else
		dr["ResidenceId"] = ResidenceId;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		EvaluationId = dr["EvaluationId"] != DBNull.Value ? Convert.ToInt32(dr["EvaluationId"]) : EvaluationId = null;
		ResidenceId = dr["ResidenceId"] != DBNull.Value ? Convert.ToInt32(dr["ResidenceId"]) : ResidenceId = null;
	}
	
	public static EvaEvaluationResidences[] MapFrom(DataSet ds)
	{
		List<EvaEvaluationResidences> objects;
		
		
		// Initialise Collection.
		objects = new List<EvaEvaluationResidences>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[eva_EvaluationResidences] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[eva_EvaluationResidences] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			EvaEvaluationResidences instance = new EvaEvaluationResidences();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	
	#region INSERT
	public void Insert(System.Int32? evaluationId, System.Int32? residenceId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluationResidences_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, evaluationId, residenceId);
		
		if (transaction == null)
		db.ExecuteScalar(dbCommand);
		else
		db.ExecuteScalar(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? evaluationId, System.Int32? residenceId)
	{
		Insert(evaluationId, residenceId, null);
	}
	/// <summary>
	/// Insert current EvaEvaluationResidences to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(EvaluationId, ResidenceId, transaction);
	}
	
	/// <summary>
	/// Insert current EvaEvaluationResidences to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static EvaEvaluationResidences[] Search(System.Int32? evaluationId, System.Int32? residenceId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluationResidences_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, evaluationId, residenceId);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return EvaEvaluationResidences.MapFrom(ds);
	}
	
	
	public static EvaEvaluationResidences[] Search(EvaEvaluationResidences searchObject)
	{
		return Search ( searchObject.EvaluationId, searchObject.ResidenceId);
	}
	
	/// <summary>
	/// Returns all EvaEvaluationResidences objects.
	/// </summary>
	/// <returns>List of all EvaEvaluationResidences objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static EvaEvaluationResidences[] Search()
	{
		return Search ( null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

