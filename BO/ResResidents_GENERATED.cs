/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 10/02/2013 9:54:51 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class ResResidents
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[res_Residents]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _residentId;
	private System.Int32? _generalId;
	private System.Int32? _residenceId;
	private System.Boolean? _subResidence;
	private System.Int32? _gradeId;
	private System.Int32? _universityId;
	private System.Int32? _teachingYearId;
	private System.DateTime? _startDate;
	private System.DateTime? _endDate;
	private System.Int32? _residentStatusId;
	private System.Int32? _centreId;
	private System.Int32? _createdBY;
	private System.DateTime? _createdDate;
	private System.Int32? _modifiedBy;
	private System.DateTime? _modifiedDate;
	
	#endregion
	
	
	#region Properties
	public System.Int32? ResidentId
	{
		get
		{
			return _residentId;
		}
		set
		{
			_residentId = value;
		}
	}
	
	public System.Int32? GeneralId
	{
		get
		{
			return _generalId;
		}
		set
		{
			_generalId = value;
		}
	}
	
	public System.Int32? ResidenceId
	{
		get
		{
			return _residenceId;
		}
		set
		{
			_residenceId = value;
		}
	}
	
	public System.Boolean? SubResidence
	{
		get
		{
			return _subResidence;
		}
		set
		{
			_subResidence = value;
		}
	}
	
	public System.Int32? GradeId
	{
		get
		{
			return _gradeId;
		}
		set
		{
			_gradeId = value;
		}
	}
	
	public System.Int32? UniversityId
	{
		get
		{
			return _universityId;
		}
		set
		{
			_universityId = value;
		}
	}
	
	public System.Int32? TeachingYearId
	{
		get
		{
			return _teachingYearId;
		}
		set
		{
			_teachingYearId = value;
		}
	}
	
	public System.DateTime? StartDate
	{
		get
		{
			return _startDate;
		}
		set
		{
			_startDate = value;
		}
	}
	
	public System.DateTime? EndDate
	{
		get
		{
			return _endDate;
		}
		set
		{
			_endDate = value;
		}
	}
	
	public System.Int32? ResidentStatusId
	{
		get
		{
			return _residentStatusId;
		}
		set
		{
			_residentStatusId = value;
		}
	}
	
	public System.Int32? CentreId
	{
		get
		{
			return _centreId;
		}
		set
		{
			_centreId = value;
		}
	}
	
	public System.Int32? CreatedBY
	{
		get
		{
			return _createdBY;
		}
		set
		{
			_createdBY = value;
		}
	}
	
	public System.DateTime? CreatedDate
	{
		get
		{
			return _createdDate;
		}
		set
		{
			_createdDate = value;
		}
	}
	
	public System.Int32? ModifiedBy
	{
		get
		{
			return _modifiedBy;
		}
		set
		{
			_modifiedBy = value;
		}
	}
	
	public System.DateTime? ModifiedDate
	{
		get
		{
			return _modifiedDate;
		}
		set
		{
			_modifiedDate = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("ResidentId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("GeneralId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ResidenceId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("SubResidence", typeof(System.Boolean) );
		ds.Tables[TABLE_NAME].Columns.Add("GradeId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("UniversityId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("TeachingYearId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("StartDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("EndDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("ResidentStatusId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CentreId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedBY", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (ResidentId == null)
		dr["ResidentId"] = DBNull.Value;
		else
		dr["ResidentId"] = ResidentId;
		
		if (GeneralId == null)
		dr["GeneralId"] = DBNull.Value;
		else
		dr["GeneralId"] = GeneralId;
		
		if (ResidenceId == null)
		dr["ResidenceId"] = DBNull.Value;
		else
		dr["ResidenceId"] = ResidenceId;
		
		if (SubResidence == null)
		dr["SubResidence"] = DBNull.Value;
		else
		dr["SubResidence"] = SubResidence;
		
		if (GradeId == null)
		dr["GradeId"] = DBNull.Value;
		else
		dr["GradeId"] = GradeId;
		
		if (UniversityId == null)
		dr["UniversityId"] = DBNull.Value;
		else
		dr["UniversityId"] = UniversityId;
		
		if (TeachingYearId == null)
		dr["TeachingYearId"] = DBNull.Value;
		else
		dr["TeachingYearId"] = TeachingYearId;
		
		if (StartDate == null)
		dr["StartDate"] = DBNull.Value;
		else
		dr["StartDate"] = StartDate;
		
		if (EndDate == null)
		dr["EndDate"] = DBNull.Value;
		else
		dr["EndDate"] = EndDate;
		
		if (ResidentStatusId == null)
		dr["ResidentStatusId"] = DBNull.Value;
		else
		dr["ResidentStatusId"] = ResidentStatusId;
		
		if (CentreId == null)
		dr["CentreId"] = DBNull.Value;
		else
		dr["CentreId"] = CentreId;
		
		if (CreatedBY == null)
		dr["CreatedBY"] = DBNull.Value;
		else
		dr["CreatedBY"] = CreatedBY;
		
		if (CreatedDate == null)
		dr["CreatedDate"] = DBNull.Value;
		else
		dr["CreatedDate"] = CreatedDate;
		
		if (ModifiedBy == null)
		dr["ModifiedBy"] = DBNull.Value;
		else
		dr["ModifiedBy"] = ModifiedBy;
		
		if (ModifiedDate == null)
		dr["ModifiedDate"] = DBNull.Value;
		else
		dr["ModifiedDate"] = ModifiedDate;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		ResidentId = dr["ResidentId"] != DBNull.Value ? Convert.ToInt32(dr["ResidentId"]) : ResidentId = null;
		GeneralId = dr["GeneralId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralId"]) : GeneralId = null;
		ResidenceId = dr["ResidenceId"] != DBNull.Value ? Convert.ToInt32(dr["ResidenceId"]) : ResidenceId = null;
		SubResidence = dr["SubResidence"] != DBNull.Value ? Convert.ToBoolean(dr["SubResidence"]) : SubResidence = null;
		GradeId = dr["GradeId"] != DBNull.Value ? Convert.ToInt32(dr["GradeId"]) : GradeId = null;
		UniversityId = dr["UniversityId"] != DBNull.Value ? Convert.ToInt32(dr["UniversityId"]) : UniversityId = null;
		TeachingYearId = dr["TeachingYearId"] != DBNull.Value ? Convert.ToInt32(dr["TeachingYearId"]) : TeachingYearId = null;
		StartDate = dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : StartDate = null;
		EndDate = dr["EndDate"] != DBNull.Value ? Convert.ToDateTime(dr["EndDate"]) : EndDate = null;
		ResidentStatusId = dr["ResidentStatusId"] != DBNull.Value ? Convert.ToInt32(dr["ResidentStatusId"]) : ResidentStatusId = null;
		CentreId = dr["CentreId"] != DBNull.Value ? Convert.ToInt32(dr["CentreId"]) : CentreId = null;
		CreatedBY = dr["CreatedBY"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBY"]) : CreatedBY = null;
		CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
		ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
		ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
	}
	
	public static ResResidents[] MapFrom(DataSet ds)
	{
		List<ResResidents> objects;
		
		
		// Initialise Collection.
		objects = new List<ResResidents>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[res_Residents] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[res_Residents] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			ResResidents instance = new ResResidents();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static ResResidents Get(System.Int32 residentId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		ResResidents instance;
		
		
		instance = new ResResidents();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidents_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, residentId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get ResResidents ID:" + residentId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.Int32? generalId, System.Int32? residenceId, System.Boolean? subResidence, System.Int32? gradeId, System.Int32? universityId, System.Int32? teachingYearId, System.DateTime? startDate, System.DateTime? endDate, System.Int32? residentStatusId, System.Int32? centreId, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidents_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, generalId, residenceId, subResidence, gradeId, universityId, teachingYearId, startDate, endDate, residentStatusId, centreId, createdBY, createdDate, modifiedBy, modifiedDate);
		
		if (transaction == null)
		this.ResidentId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.ResidentId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? generalId, System.Int32? residenceId, System.Boolean? subResidence, System.Int32? gradeId, System.Int32? universityId, System.Int32? teachingYearId, System.DateTime? startDate, System.DateTime? endDate, System.Int32? residentStatusId, System.Int32? centreId, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Insert(generalId, residenceId, subResidence, gradeId, universityId, teachingYearId, startDate, endDate, residentStatusId, centreId, createdBY, createdDate, modifiedBy, modifiedDate, null);
	}
	/// <summary>
	/// Insert current ResResidents to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(GeneralId, ResidenceId, SubResidence, GradeId, UniversityId, TeachingYearId, StartDate, EndDate, ResidentStatusId, CentreId, CreatedBY, CreatedDate, ModifiedBy, ModifiedDate, transaction);
	}
	
	/// <summary>
	/// Insert current ResResidents to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? residentId, System.Int32? generalId, System.Int32? residenceId, System.Boolean? subResidence, System.Int32? gradeId, System.Int32? universityId, System.Int32? teachingYearId, System.DateTime? startDate, System.DateTime? endDate, System.Int32? residentStatusId, System.Int32? centreId, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidents_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@residentId"].Value = residentId;
		dbCommand.Parameters["@generalId"].Value = generalId;
		dbCommand.Parameters["@residenceId"].Value = residenceId;
		dbCommand.Parameters["@subResidence"].Value = subResidence;
		dbCommand.Parameters["@gradeId"].Value = gradeId;
		dbCommand.Parameters["@universityId"].Value = universityId;
		dbCommand.Parameters["@teachingYearId"].Value = teachingYearId;
		dbCommand.Parameters["@startDate"].Value = startDate;
		dbCommand.Parameters["@endDate"].Value = endDate;
		dbCommand.Parameters["@residentStatusId"].Value = residentStatusId;
		dbCommand.Parameters["@centreId"].Value = centreId;
		dbCommand.Parameters["@createdBY"].Value = createdBY;
		dbCommand.Parameters["@createdDate"].Value = createdDate;
		dbCommand.Parameters["@modifiedBy"].Value = modifiedBy;
		dbCommand.Parameters["@modifiedDate"].Value = modifiedDate;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? residentId, System.Int32? generalId, System.Int32? residenceId, System.Boolean? subResidence, System.Int32? gradeId, System.Int32? universityId, System.Int32? teachingYearId, System.DateTime? startDate, System.DateTime? endDate, System.Int32? residentStatusId, System.Int32? centreId, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Update(residentId, generalId, residenceId, subResidence, gradeId, universityId, teachingYearId, startDate, endDate, residentStatusId, centreId, createdBY, createdDate, modifiedBy, modifiedDate, null);
	}
	
	public static void Update(ResResidents resResidents)
	{
		resResidents.Update();
	}
	
	public static void Update(ResResidents resResidents, DbTransaction transaction)
	{
		resResidents.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidents_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@residentId"].SourceColumn = "ResidentId";
		dbCommand.Parameters["@generalId"].SourceColumn = "GeneralId";
		dbCommand.Parameters["@residenceId"].SourceColumn = "ResidenceId";
		dbCommand.Parameters["@subResidence"].SourceColumn = "SubResidence";
		dbCommand.Parameters["@gradeId"].SourceColumn = "GradeId";
		dbCommand.Parameters["@universityId"].SourceColumn = "UniversityId";
		dbCommand.Parameters["@teachingYearId"].SourceColumn = "TeachingYearId";
		dbCommand.Parameters["@startDate"].SourceColumn = "StartDate";
		dbCommand.Parameters["@endDate"].SourceColumn = "EndDate";
		dbCommand.Parameters["@residentStatusId"].SourceColumn = "ResidentStatusId";
		dbCommand.Parameters["@centreId"].SourceColumn = "CentreId";
		dbCommand.Parameters["@createdBY"].SourceColumn = "CreatedBY";
		dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
		dbCommand.Parameters["@modifiedBy"].SourceColumn = "ModifiedBy";
		dbCommand.Parameters["@modifiedDate"].SourceColumn = "ModifiedDate";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? residentId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidents_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, residentId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? residentId)
	{
		Delete(
		residentId);
	}
	
	/// <summary>
	/// Delete current ResResidents from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidents_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, ResidentId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.ResidentId = null;
	}
	
	/// <summary>
	/// Delete current ResResidents from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static ResResidents[] Search(System.Int32? residentId, System.Int32? generalId, System.Int32? residenceId, System.Boolean? subResidence, System.Int32? gradeId, System.Int32? universityId, System.Int32? teachingYearId, System.DateTime? startDate, System.DateTime? endDate, System.Int32? residentStatusId, System.Int32? centreId, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidents_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, residentId, generalId, residenceId, subResidence, gradeId, universityId, teachingYearId, startDate, endDate, residentStatusId, centreId, createdBY, createdDate, modifiedBy, modifiedDate);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return ResResidents.MapFrom(ds);
	}
	
	
	public static ResResidents[] Search(ResResidents searchObject)
	{
		return Search ( searchObject.ResidentId, searchObject.GeneralId, searchObject.ResidenceId, searchObject.SubResidence, searchObject.GradeId, searchObject.UniversityId, searchObject.TeachingYearId, searchObject.StartDate, searchObject.EndDate, searchObject.ResidentStatusId, searchObject.CentreId, searchObject.CreatedBY, searchObject.CreatedDate, searchObject.ModifiedBy, searchObject.ModifiedDate);
	}
	
	/// <summary>
	/// Returns all ResResidents objects.
	/// </summary>
	/// <returns>List of all ResResidents objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static ResResidents[] Search()
	{
		return Search ( null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

