/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 09/16/2013 9:09:51 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class GenPersons
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[gen_Persons]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _personId;
	private System.Int32? _generalId;
	private System.String _name;
	private System.String _middleName;
	private System.String _firstLastName;
	private System.String _secondLastName;
	private System.String _fullName;
	private System.Int32? _genderId;
	private System.Int32? _maritalStatusId;
	private System.DateTime? _birthday;
	private System.Int32? _modifyBy;
	private System.DateTime? _modifyDate;
	
	#endregion
	
	
	#region Properties
	public System.Int32? PersonId
	{
		get
		{
			return _personId;
		}
		set
		{
			_personId = value;
		}
	}
	
	public System.Int32? GeneralId
	{
		get
		{
			return _generalId;
		}
		set
		{
			_generalId = value;
		}
	}
	
	public System.String Name
	{
		get
		{
			return _name;
		}
		set
		{
			_name = value;
		}
	}
	
	public System.String MiddleName
	{
		get
		{
			return _middleName;
		}
		set
		{
			_middleName = value;
		}
	}
	
	public System.String FirstLastName
	{
		get
		{
			return _firstLastName;
		}
		set
		{
			_firstLastName = value;
		}
	}
	
	public System.String SecondLastName
	{
		get
		{
			return _secondLastName;
		}
		set
		{
			_secondLastName = value;
		}
	}
	
	public System.String FullName
	{
		get
		{
			return _fullName;
		}
		set
		{
			_fullName = value;
		}
	}
	
	public System.Int32? GenderId
	{
		get
		{
			return _genderId;
		}
		set
		{
			_genderId = value;
		}
	}
	
	public System.Int32? MaritalStatusId
	{
		get
		{
			return _maritalStatusId;
		}
		set
		{
			_maritalStatusId = value;
		}
	}
	
	public System.DateTime? Birthday
	{
		get
		{
			return _birthday;
		}
		set
		{
			_birthday = value;
		}
	}
	
	public System.Int32? ModifyBy
	{
		get
		{
			return _modifyBy;
		}
		set
		{
			_modifyBy = value;
		}
	}
	
	public System.DateTime? ModifyDate
	{
		get
		{
			return _modifyDate;
		}
		set
		{
			_modifyDate = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("PersonId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("GeneralId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("Name", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("MiddleName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("FirstLastName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("SecondLastName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("FullName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("GenderId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("MaritalStatusId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("Birthday", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifyBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifyDate", typeof(System.DateTime) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (PersonId == null)
		dr["PersonId"] = DBNull.Value;
		else
		dr["PersonId"] = PersonId;
		
		if (GeneralId == null)
		dr["GeneralId"] = DBNull.Value;
		else
		dr["GeneralId"] = GeneralId;
		
		if (Name == null)
		dr["Name"] = DBNull.Value;
		else
		dr["Name"] = Name;
		
		if (MiddleName == null)
		dr["MiddleName"] = DBNull.Value;
		else
		dr["MiddleName"] = MiddleName;
		
		if (FirstLastName == null)
		dr["FirstLastName"] = DBNull.Value;
		else
		dr["FirstLastName"] = FirstLastName;
		
		if (SecondLastName == null)
		dr["SecondLastName"] = DBNull.Value;
		else
		dr["SecondLastName"] = SecondLastName;
		
		if (FullName == null)
		dr["FullName"] = DBNull.Value;
		else
		dr["FullName"] = FullName;
		
		if (GenderId == null)
		dr["GenderId"] = DBNull.Value;
		else
		dr["GenderId"] = GenderId;
		
		if (MaritalStatusId == null)
		dr["MaritalStatusId"] = DBNull.Value;
		else
		dr["MaritalStatusId"] = MaritalStatusId;
		
		if (Birthday == null)
		dr["Birthday"] = DBNull.Value;
		else
		dr["Birthday"] = Birthday;
		
		if (ModifyBy == null)
		dr["ModifyBy"] = DBNull.Value;
		else
		dr["ModifyBy"] = ModifyBy;
		
		if (ModifyDate == null)
		dr["ModifyDate"] = DBNull.Value;
		else
		dr["ModifyDate"] = ModifyDate;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		PersonId = dr["PersonId"] != DBNull.Value ? Convert.ToInt32(dr["PersonId"]) : PersonId = null;
		GeneralId = dr["GeneralId"] != DBNull.Value ? Convert.ToInt32(dr["GeneralId"]) : GeneralId = null;
		Name = dr["Name"] != DBNull.Value ? Convert.ToString(dr["Name"]) : Name = null;
		MiddleName = dr["MiddleName"] != DBNull.Value ? Convert.ToString(dr["MiddleName"]) : MiddleName = null;
		FirstLastName = dr["FirstLastName"] != DBNull.Value ? Convert.ToString(dr["FirstLastName"]) : FirstLastName = null;
		SecondLastName = dr["SecondLastName"] != DBNull.Value ? Convert.ToString(dr["SecondLastName"]) : SecondLastName = null;
		FullName = dr["FullName"] != DBNull.Value ? Convert.ToString(dr["FullName"]) : FullName = null;
		GenderId = dr["GenderId"] != DBNull.Value ? Convert.ToInt32(dr["GenderId"]) : GenderId = null;
		MaritalStatusId = dr["MaritalStatusId"] != DBNull.Value ? Convert.ToInt32(dr["MaritalStatusId"]) : MaritalStatusId = null;
		Birthday = dr["Birthday"] != DBNull.Value ? Convert.ToDateTime(dr["Birthday"]) : Birthday = null;
		ModifyBy = dr["ModifyBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifyBy"]) : ModifyBy = null;
		ModifyDate = dr["ModifyDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifyDate"]) : ModifyDate = null;
	}
	
	public static GenPersons[] MapFrom(DataSet ds)
	{
		List<GenPersons> objects;
		
		
		// Initialise Collection.
		objects = new List<GenPersons>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[gen_Persons] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[gen_Persons] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			GenPersons instance = new GenPersons();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static GenPersons Get(System.Int32 personId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		GenPersons instance;
		
		
		instance = new GenPersons();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenPersons_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, personId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get GenPersons ID:" + personId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.Int32? generalId, System.String name, System.String middleName, System.String firstLastName, System.String secondLastName, System.Int32? genderId, System.Int32? maritalStatusId, System.DateTime? birthday, System.Int32? modifyBy, System.DateTime? modifyDate, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenPersons_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, generalId, name, middleName, firstLastName, secondLastName, genderId, maritalStatusId, birthday, modifyBy, modifyDate);
		
		if (transaction == null)
		this.PersonId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.PersonId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? generalId, System.String name, System.String middleName, System.String firstLastName, System.String secondLastName, System.Int32? genderId, System.Int32? maritalStatusId, System.DateTime? birthday, System.Int32? modifyBy, System.DateTime? modifyDate)
	{
		Insert(generalId, name, middleName, firstLastName, secondLastName, genderId, maritalStatusId, birthday, modifyBy, modifyDate, null);
	}
	/// <summary>
	/// Insert current GenPersons to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(GeneralId, Name, MiddleName, FirstLastName, SecondLastName, GenderId, MaritalStatusId, Birthday, ModifyBy, ModifyDate, transaction);
	}
	
	/// <summary>
	/// Insert current GenPersons to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? personId, System.Int32? generalId, System.String name, System.String middleName, System.String firstLastName, System.String secondLastName, System.String fullName, System.Int32? genderId, System.Int32? maritalStatusId, System.DateTime? birthday, System.Int32? modifyBy, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenPersons_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@personId"].Value = personId;
		dbCommand.Parameters["@generalId"].Value = generalId;
		dbCommand.Parameters["@name"].Value = name;
		dbCommand.Parameters["@middleName"].Value = middleName;
		dbCommand.Parameters["@firstLastName"].Value = firstLastName;
		dbCommand.Parameters["@secondLastName"].Value = secondLastName;
		dbCommand.Parameters["@genderId"].Value = fullName;
		dbCommand.Parameters["@maritalStatusId"].Value = genderId;
		dbCommand.Parameters["@birthday"].Value = maritalStatusId;
		dbCommand.Parameters["@modifyBy"].Value = birthday;
		dbCommand.Parameters["@modifyDate"].Value = modifyBy;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? personId, System.Int32? generalId, System.String name, System.String middleName, System.String firstLastName, System.String secondLastName, System.String fullName, System.Int32? genderId, System.Int32? maritalStatusId, System.DateTime? birthday, System.Int32? modifyBy)
	{
		Update(personId, generalId, name, middleName, firstLastName, secondLastName, fullName, genderId, maritalStatusId, birthday, modifyBy, null);
	}
	
	public static void Update(GenPersons genPersons)
	{
		genPersons.Update();
	}
	
	public static void Update(GenPersons genPersons, DbTransaction transaction)
	{
		genPersons.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenPersons_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@personId"].SourceColumn = "PersonId";
		dbCommand.Parameters["@generalId"].SourceColumn = "GeneralId";
		dbCommand.Parameters["@name"].SourceColumn = "Name";
		dbCommand.Parameters["@middleName"].SourceColumn = "MiddleName";
		dbCommand.Parameters["@firstLastName"].SourceColumn = "FirstLastName";
		dbCommand.Parameters["@secondLastName"].SourceColumn = "SecondLastName";
		dbCommand.Parameters["@genderId"].SourceColumn = "GenderId";
		dbCommand.Parameters["@maritalStatusId"].SourceColumn = "MaritalStatusId";
		dbCommand.Parameters["@birthday"].SourceColumn = "Birthday";
		dbCommand.Parameters["@modifyBy"].SourceColumn = "ModifyBy";
		dbCommand.Parameters["@modifyDate"].SourceColumn = "ModifyDate";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? personId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenPersons_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, personId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? personId)
	{
		Delete(
		personId);
	}
	
	/// <summary>
	/// Delete current GenPersons from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenPersons_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, PersonId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.PersonId = null;
	}
	
	/// <summary>
	/// Delete current GenPersons from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static GenPersons[] Search(System.Int32? personId, System.Int32? generalId, System.String name, System.String middleName, System.String firstLastName, System.String secondLastName, System.String fullName, System.Int32? genderId, System.Int32? maritalStatusId, System.DateTime? birthday, System.Int32? modifyBy, System.DateTime? modifyDate)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspGenPersons_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, personId, generalId, name, middleName, firstLastName, secondLastName, fullName, genderId, maritalStatusId, birthday, modifyBy, modifyDate);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return GenPersons.MapFrom(ds);
	}
	
	
	public static GenPersons[] Search(GenPersons searchObject)
	{
		return Search ( searchObject.PersonId, searchObject.GeneralId, searchObject.Name, searchObject.MiddleName, searchObject.FirstLastName, searchObject.SecondLastName, searchObject.FullName, searchObject.GenderId, searchObject.MaritalStatusId, searchObject.Birthday, searchObject.ModifyBy, searchObject.ModifyDate);
	}
	
	/// <summary>
	/// Returns all GenPersons objects.
	/// </summary>
	/// <returns>List of all GenPersons objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static GenPersons[] Search()
	{
		return Search ( null, null, null, null, null, null, null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

