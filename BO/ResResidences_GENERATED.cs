/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 11/18/2013 8:05:26 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class ResResidences
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[res_Residences]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _residenceId;
	private System.Int32? _residenceParentId;
	private System.Int32? _preYearId;
	private System.Int32? _preResidenceId;
	private System.String _residenceName;
	private System.Int32? _residenceMaxGradeId;
	private System.Int32? _residenceTypeId;
	private System.Boolean? _residenceStatus;
	private System.Int32? _departmentId;
	private System.Int32? _teachingYearId;
	private System.Int32? _createdBY;
	private System.DateTime? _createdDate;
	private System.Int32? _modifiedBy;
	private System.DateTime? _modifiedDate;
	
	#endregion
	
	
	#region Properties
	public System.Int32? ResidenceId
	{
		get
		{
			return _residenceId;
		}
		set
		{
			_residenceId = value;
		}
	}
	
	public System.Int32? ResidenceParentId
	{
		get
		{
			return _residenceParentId;
		}
		set
		{
			_residenceParentId = value;
		}
	}
	
	public System.Int32? PreYearId
	{
		get
		{
			return _preYearId;
		}
		set
		{
			_preYearId = value;
		}
	}
	
	public System.Int32? PreResidenceId
	{
		get
		{
			return _preResidenceId;
		}
		set
		{
			_preResidenceId = value;
		}
	}
	
	public System.String ResidenceName
	{
		get
		{
			return _residenceName;
		}
		set
		{
			_residenceName = value;
		}
	}
	
	public System.Int32? ResidenceMaxGradeId
	{
		get
		{
			return _residenceMaxGradeId;
		}
		set
		{
			_residenceMaxGradeId = value;
		}
	}
	
	public System.Int32? ResidenceTypeId
	{
		get
		{
			return _residenceTypeId;
		}
		set
		{
			_residenceTypeId = value;
		}
	}
	
	public System.Boolean? ResidenceStatus
	{
		get
		{
			return _residenceStatus;
		}
		set
		{
			_residenceStatus = value;
		}
	}
	
	public System.Int32? DepartmentId
	{
		get
		{
			return _departmentId;
		}
		set
		{
			_departmentId = value;
		}
	}
	
	public System.Int32? TeachingYearId
	{
		get
		{
			return _teachingYearId;
		}
		set
		{
			_teachingYearId = value;
		}
	}
	
	public System.Int32? CreatedBY
	{
		get
		{
			return _createdBY;
		}
		set
		{
			_createdBY = value;
		}
	}
	
	public System.DateTime? CreatedDate
	{
		get
		{
			return _createdDate;
		}
		set
		{
			_createdDate = value;
		}
	}
	
	public System.Int32? ModifiedBy
	{
		get
		{
			return _modifiedBy;
		}
		set
		{
			_modifiedBy = value;
		}
	}
	
	public System.DateTime? ModifiedDate
	{
		get
		{
			return _modifiedDate;
		}
		set
		{
			_modifiedDate = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("ResidenceId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ResidenceParentId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("PreYearId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("PreResidenceId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ResidenceName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("ResidenceMaxGradeId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ResidenceTypeId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ResidenceStatus", typeof(System.Boolean) );
		ds.Tables[TABLE_NAME].Columns.Add("DepartmentId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("TeachingYearId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedBY", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (ResidenceId == null)
		dr["ResidenceId"] = DBNull.Value;
		else
		dr["ResidenceId"] = ResidenceId;
		
		if (ResidenceParentId == null)
		dr["ResidenceParentId"] = DBNull.Value;
		else
		dr["ResidenceParentId"] = ResidenceParentId;
		
		if (PreYearId == null)
		dr["PreYearId"] = DBNull.Value;
		else
		dr["PreYearId"] = PreYearId;
		
		if (PreResidenceId == null)
		dr["PreResidenceId"] = DBNull.Value;
		else
		dr["PreResidenceId"] = PreResidenceId;
		
		if (ResidenceName == null)
		dr["ResidenceName"] = DBNull.Value;
		else
		dr["ResidenceName"] = ResidenceName;
		
		if (ResidenceMaxGradeId == null)
		dr["ResidenceMaxGradeId"] = DBNull.Value;
		else
		dr["ResidenceMaxGradeId"] = ResidenceMaxGradeId;
		
		if (ResidenceTypeId == null)
		dr["ResidenceTypeId"] = DBNull.Value;
		else
		dr["ResidenceTypeId"] = ResidenceTypeId;
		
		if (ResidenceStatus == null)
		dr["ResidenceStatus"] = DBNull.Value;
		else
		dr["ResidenceStatus"] = ResidenceStatus;
		
		if (DepartmentId == null)
		dr["DepartmentId"] = DBNull.Value;
		else
		dr["DepartmentId"] = DepartmentId;
		
		if (TeachingYearId == null)
		dr["TeachingYearId"] = DBNull.Value;
		else
		dr["TeachingYearId"] = TeachingYearId;
		
		if (CreatedBY == null)
		dr["CreatedBY"] = DBNull.Value;
		else
		dr["CreatedBY"] = CreatedBY;
		
		if (CreatedDate == null)
		dr["CreatedDate"] = DBNull.Value;
		else
		dr["CreatedDate"] = CreatedDate;
		
		if (ModifiedBy == null)
		dr["ModifiedBy"] = DBNull.Value;
		else
		dr["ModifiedBy"] = ModifiedBy;
		
		if (ModifiedDate == null)
		dr["ModifiedDate"] = DBNull.Value;
		else
		dr["ModifiedDate"] = ModifiedDate;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		ResidenceId = dr["ResidenceId"] != DBNull.Value ? Convert.ToInt32(dr["ResidenceId"]) : ResidenceId = null;
		ResidenceParentId = dr["ResidenceParentId"] != DBNull.Value ? Convert.ToInt32(dr["ResidenceParentId"]) : ResidenceParentId = null;
		PreYearId = dr["PreYearId"] != DBNull.Value ? Convert.ToInt32(dr["PreYearId"]) : PreYearId = null;
		PreResidenceId = dr["PreResidenceId"] != DBNull.Value ? Convert.ToInt32(dr["PreResidenceId"]) : PreResidenceId = null;
		ResidenceName = dr["ResidenceName"] != DBNull.Value ? Convert.ToString(dr["ResidenceName"]) : ResidenceName = null;
		ResidenceMaxGradeId = dr["ResidenceMaxGradeId"] != DBNull.Value ? Convert.ToInt32(dr["ResidenceMaxGradeId"]) : ResidenceMaxGradeId = null;
		ResidenceTypeId = dr["ResidenceTypeId"] != DBNull.Value ? Convert.ToInt32(dr["ResidenceTypeId"]) : ResidenceTypeId = null;
		ResidenceStatus = dr["ResidenceStatus"] != DBNull.Value ? Convert.ToBoolean(dr["ResidenceStatus"]) : ResidenceStatus = null;
		DepartmentId = dr["DepartmentId"] != DBNull.Value ? Convert.ToInt32(dr["DepartmentId"]) : DepartmentId = null;
		TeachingYearId = dr["TeachingYearId"] != DBNull.Value ? Convert.ToInt32(dr["TeachingYearId"]) : TeachingYearId = null;
		CreatedBY = dr["CreatedBY"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBY"]) : CreatedBY = null;
		CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
		ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
		ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
	}
	
	public static ResResidences[] MapFrom(DataSet ds)
	{
		List<ResResidences> objects;
		
		
		// Initialise Collection.
		objects = new List<ResResidences>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[res_Residences] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[res_Residences] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			ResResidences instance = new ResResidences();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static ResResidences Get(System.Int32 residenceId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		ResResidences instance;
		
		
		instance = new ResResidences();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidences_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, residenceId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get ResResidences ID:" + residenceId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.Int32? residenceParentId, System.Int32? preYearId, System.Int32? preResidenceId, System.String residenceName, System.Int32? residenceMaxGradeId, System.Int32? residenceTypeId, System.Boolean? residenceStatus, System.Int32? departmentId, System.Int32? teachingYearId, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidences_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, residenceParentId, preYearId, preResidenceId, residenceName, residenceMaxGradeId, residenceTypeId, residenceStatus, departmentId, teachingYearId, createdBY, createdDate, modifiedBy, modifiedDate);
		
		if (transaction == null)
		this.ResidenceId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.ResidenceId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.Int32? residenceParentId, System.Int32? preYearId, System.Int32? preResidenceId, System.String residenceName, System.Int32? residenceMaxGradeId, System.Int32? residenceTypeId, System.Boolean? residenceStatus, System.Int32? departmentId, System.Int32? teachingYearId, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Insert(residenceParentId, preYearId, preResidenceId, residenceName, residenceMaxGradeId, residenceTypeId, residenceStatus, departmentId, teachingYearId, createdBY, createdDate, modifiedBy, modifiedDate, null);
	}
	/// <summary>
	/// Insert current ResResidences to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(ResidenceParentId, PreYearId, PreResidenceId, ResidenceName, ResidenceMaxGradeId, ResidenceTypeId, ResidenceStatus, DepartmentId, TeachingYearId, CreatedBY, CreatedDate, ModifiedBy, ModifiedDate, transaction);
	}
	
	/// <summary>
	/// Insert current ResResidences to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? residenceId, System.Int32? residenceParentId, System.Int32? preYearId, System.Int32? preResidenceId, System.String residenceName, System.Int32? residenceMaxGradeId, System.Int32? residenceTypeId, System.Boolean? residenceStatus, System.Int32? departmentId, System.Int32? teachingYearId, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidences_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@residenceId"].Value = residenceId;
		dbCommand.Parameters["@residenceParentId"].Value = residenceParentId;
		dbCommand.Parameters["@preYearId"].Value = preYearId;
		dbCommand.Parameters["@preResidenceId"].Value = preResidenceId;
		dbCommand.Parameters["@residenceName"].Value = residenceName;
		dbCommand.Parameters["@residenceMaxGradeId"].Value = residenceMaxGradeId;
		dbCommand.Parameters["@residenceTypeId"].Value = residenceTypeId;
		dbCommand.Parameters["@residenceStatus"].Value = residenceStatus;
		dbCommand.Parameters["@departmentId"].Value = departmentId;
		dbCommand.Parameters["@teachingYearId"].Value = teachingYearId;
		dbCommand.Parameters["@createdBY"].Value = createdBY;
		dbCommand.Parameters["@createdDate"].Value = createdDate;
		dbCommand.Parameters["@modifiedBy"].Value = modifiedBy;
		dbCommand.Parameters["@modifiedDate"].Value = modifiedDate;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? residenceId, System.Int32? residenceParentId, System.Int32? preYearId, System.Int32? preResidenceId, System.String residenceName, System.Int32? residenceMaxGradeId, System.Int32? residenceTypeId, System.Boolean? residenceStatus, System.Int32? departmentId, System.Int32? teachingYearId, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Update(residenceId, residenceParentId, preYearId, preResidenceId, residenceName, residenceMaxGradeId, residenceTypeId, residenceStatus, departmentId, teachingYearId, createdBY, createdDate, modifiedBy, modifiedDate, null);
	}
	
	public static void Update(ResResidences resResidences)
	{
		resResidences.Update();
	}
	
	public static void Update(ResResidences resResidences, DbTransaction transaction)
	{
		resResidences.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidences_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@residenceId"].SourceColumn = "ResidenceId";
		dbCommand.Parameters["@residenceParentId"].SourceColumn = "ResidenceParentId";
		dbCommand.Parameters["@preYearId"].SourceColumn = "PreYearId";
		dbCommand.Parameters["@preResidenceId"].SourceColumn = "PreResidenceId";
		dbCommand.Parameters["@residenceName"].SourceColumn = "ResidenceName";
		dbCommand.Parameters["@residenceMaxGradeId"].SourceColumn = "ResidenceMaxGradeId";
		dbCommand.Parameters["@residenceTypeId"].SourceColumn = "ResidenceTypeId";
		dbCommand.Parameters["@residenceStatus"].SourceColumn = "ResidenceStatus";
		dbCommand.Parameters["@departmentId"].SourceColumn = "DepartmentId";
		dbCommand.Parameters["@teachingYearId"].SourceColumn = "TeachingYearId";
		dbCommand.Parameters["@createdBY"].SourceColumn = "CreatedBY";
		dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
		dbCommand.Parameters["@modifiedBy"].SourceColumn = "ModifiedBy";
		dbCommand.Parameters["@modifiedDate"].SourceColumn = "ModifiedDate";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? residenceId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidences_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, residenceId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? residenceId)
	{
		Delete(
		residenceId);
	}
	
	/// <summary>
	/// Delete current ResResidences from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidences_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, ResidenceId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.ResidenceId = null;
	}
	
	/// <summary>
	/// Delete current ResResidences from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static ResResidences[] Search(System.Int32? residenceId, System.Int32? residenceParentId, System.Int32? preYearId, System.Int32? preResidenceId, System.String residenceName, System.Int32? residenceMaxGradeId, System.Int32? residenceTypeId, System.Boolean? residenceStatus, System.Int32? departmentId, System.Int32? teachingYearId, System.Int32? createdBY, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspResResidences_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, residenceId, residenceParentId, preYearId, preResidenceId, residenceName, residenceMaxGradeId, residenceTypeId, residenceStatus, departmentId, teachingYearId, createdBY, createdDate, modifiedBy, modifiedDate);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return ResResidences.MapFrom(ds);
	}
	
	
	public static ResResidences[] Search(ResResidences searchObject)
	{
		return Search ( searchObject.ResidenceId, searchObject.ResidenceParentId, searchObject.PreYearId, searchObject.PreResidenceId, searchObject.ResidenceName, searchObject.ResidenceMaxGradeId, searchObject.ResidenceTypeId, searchObject.ResidenceStatus, searchObject.DepartmentId, searchObject.TeachingYearId, searchObject.CreatedBY, searchObject.CreatedDate, searchObject.ModifiedBy, searchObject.ModifiedDate);
	}
	
	/// <summary>
	/// Returns all ResResidences objects.
	/// </summary>
	/// <returns>List of all ResResidences objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static ResResidences[] Search()
	{
		return Search ( null, null, null, null, null, null, null, null, null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

