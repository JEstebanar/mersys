/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 11/27/2013 10:59:45 AM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[DataObject]
[Serializable]
public partial class EvaEvaluations
{
	
	
	#region Constants
	private static readonly string TABLE_NAME = "[dbo].[eva_Evaluations]";
	
	#endregion
	
	
	#region Fields
	private System.Int32? _evaluationId;
	private System.String _evaluationName;
	private System.Decimal? _evaluationPercent;
	private System.Boolean? _evaluationStatus;
	private System.Int32? _centerId;
	private System.Int32? _teachingYearId;
	private System.Int32? _createdBy;
	private System.DateTime? _createdDate;
	private System.Int32? _modifiedBy;
	private System.DateTime? _modifiedDate;
	
	#endregion
	
	
	#region Properties
	public System.Int32? EvaluationId
	{
		get
		{
			return _evaluationId;
		}
		set
		{
			_evaluationId = value;
		}
	}
	
	public System.String EvaluationName
	{
		get
		{
			return _evaluationName;
		}
		set
		{
			_evaluationName = value;
		}
	}
	
	public System.Decimal? EvaluationPercent
	{
		get
		{
			return _evaluationPercent;
		}
		set
		{
			_evaluationPercent = value;
		}
	}
	
	public System.Boolean? EvaluationStatus
	{
		get
		{
			return _evaluationStatus;
		}
		set
		{
			_evaluationStatus = value;
		}
	}
	
	public System.Int32? CenterId
	{
		get
		{
			return _centerId;
		}
		set
		{
			_centerId = value;
		}
	}
	
	public System.Int32? TeachingYearId
	{
		get
		{
			return _teachingYearId;
		}
		set
		{
			_teachingYearId = value;
		}
	}
	
	public System.Int32? CreatedBy
	{
		get
		{
			return _createdBy;
		}
		set
		{
			_createdBy = value;
		}
	}
	
	public System.DateTime? CreatedDate
	{
		get
		{
			return _createdDate;
		}
		set
		{
			_createdDate = value;
		}
	}
	
	public System.Int32? ModifiedBy
	{
		get
		{
			return _modifiedBy;
		}
		set
		{
			_modifiedBy = value;
		}
	}
	
	public System.DateTime? ModifiedDate
	{
		get
		{
			return _modifiedDate;
		}
		set
		{
			_modifiedDate = value;
		}
	}
	
	#endregion
	
	
	#region Methods
	
	
	#region Mapping Methods
	
	protected void MapTo(DataSet ds)
	{
		DataRow dr;
		
		
		if (ds == null)
		ds = new DataSet();
		
		if (ds.Tables["TABLE_NAME"] == null)
		ds.Tables.Add(TABLE_NAME);
		
		ds.Tables[TABLE_NAME].Columns.Add("EvaluationId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("EvaluationName", typeof(System.String) );
		ds.Tables[TABLE_NAME].Columns.Add("EvaluationPercent", typeof(System.Decimal) );
		ds.Tables[TABLE_NAME].Columns.Add("EvaluationStatus", typeof(System.Boolean) );
		ds.Tables[TABLE_NAME].Columns.Add("CenterId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("TeachingYearId", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("CreatedDate", typeof(System.DateTime) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedBy", typeof(System.Int32) );
		ds.Tables[TABLE_NAME].Columns.Add("ModifiedDate", typeof(System.DateTime) );
		
		dr = ds.Tables[TABLE_NAME].NewRow();
		
		if (EvaluationId == null)
		dr["EvaluationId"] = DBNull.Value;
		else
		dr["EvaluationId"] = EvaluationId;
		
		if (EvaluationName == null)
		dr["EvaluationName"] = DBNull.Value;
		else
		dr["EvaluationName"] = EvaluationName;
		
		if (EvaluationPercent == null)
		dr["EvaluationPercent"] = DBNull.Value;
		else
		dr["EvaluationPercent"] = EvaluationPercent;
		
		if (EvaluationStatus == null)
		dr["EvaluationStatus"] = DBNull.Value;
		else
		dr["EvaluationStatus"] = EvaluationStatus;
		
		if (CenterId == null)
		dr["CenterId"] = DBNull.Value;
		else
		dr["CenterId"] = CenterId;
		
		if (TeachingYearId == null)
		dr["TeachingYearId"] = DBNull.Value;
		else
		dr["TeachingYearId"] = TeachingYearId;
		
		if (CreatedBy == null)
		dr["CreatedBy"] = DBNull.Value;
		else
		dr["CreatedBy"] = CreatedBy;
		
		if (CreatedDate == null)
		dr["CreatedDate"] = DBNull.Value;
		else
		dr["CreatedDate"] = CreatedDate;
		
		if (ModifiedBy == null)
		dr["ModifiedBy"] = DBNull.Value;
		else
		dr["ModifiedBy"] = ModifiedBy;
		
		if (ModifiedDate == null)
		dr["ModifiedDate"] = DBNull.Value;
		else
		dr["ModifiedDate"] = ModifiedDate;
		
		
		ds.Tables[TABLE_NAME].Rows.Add(dr);
		
	}
	
	protected void MapFrom(DataRow dr)
	{
		EvaluationId = dr["EvaluationId"] != DBNull.Value ? Convert.ToInt32(dr["EvaluationId"]) : EvaluationId = null;
		EvaluationName = dr["EvaluationName"] != DBNull.Value ? Convert.ToString(dr["EvaluationName"]) : EvaluationName = null;
		EvaluationPercent = dr["EvaluationPercent"] != DBNull.Value ? Convert.ToDecimal(dr["EvaluationPercent"]) : EvaluationPercent = null;
		EvaluationStatus = dr["EvaluationStatus"] != DBNull.Value ? Convert.ToBoolean(dr["EvaluationStatus"]) : EvaluationStatus = null;
		CenterId = dr["CenterId"] != DBNull.Value ? Convert.ToInt32(dr["CenterId"]) : CenterId = null;
		TeachingYearId = dr["TeachingYearId"] != DBNull.Value ? Convert.ToInt32(dr["TeachingYearId"]) : TeachingYearId = null;
		CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : CreatedBy = null;
		CreatedDate = dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : CreatedDate = null;
		ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(dr["ModifiedBy"]) : ModifiedBy = null;
		ModifiedDate = dr["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ModifiedDate"]) : ModifiedDate = null;
	}
	
	public static EvaEvaluations[] MapFrom(DataSet ds)
	{
		List<EvaEvaluations> objects;
		
		
		// Initialise Collection.
		objects = new List<EvaEvaluations>();
		
		// Validation.
		if (ds == null)
		throw new ApplicationException("Cannot map to dataset null.");
		else if (ds.Tables[TABLE_NAME].Rows.Count == 0)
		return objects.ToArray();
		
		if (ds.Tables[TABLE_NAME] == null)
		throw new ApplicationException("Cannot find table [dbo].[eva_Evaluations] in DataSet.");
		
		if (ds.Tables[TABLE_NAME].Rows.Count < 1)
		throw new ApplicationException("Table [dbo].[eva_Evaluations] is empty.");
		
		// Map DataSet to Instance.
		foreach (DataRow dr in ds.Tables[TABLE_NAME].Rows)
		{
			EvaEvaluations instance = new EvaEvaluations();
			instance.MapFrom(dr);
			objects.Add(instance);
		}
		
		// Return collection.
		return objects.ToArray();
	}
	
	
	#endregion
	
	
	#region CRUD Methods
	
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static EvaEvaluations Get(System.Int32 evaluationId)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		EvaEvaluations instance;
		
		
		instance = new EvaEvaluations();
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluations_SELECT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, evaluationId);
		
		// Get results.
		ds = db.ExecuteDataSet(dbCommand);
		// Verification.
		if (ds == null || ds.Tables[0].Rows.Count == 0) throw new ApplicationException("Could not get EvaEvaluations ID:" + evaluationId.ToString()+ " from Database.");
		// Return results.
		ds.Tables[0].TableName = TABLE_NAME;
		
		instance.MapFrom( ds.Tables[0].Rows[0] );
		return instance;
	}
	
	#region INSERT
	public void Insert(System.String evaluationName, System.Decimal? evaluationPercent, System.Boolean? evaluationStatus, System.Int32? centerId, System.Int32? teachingYearId, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluations_INSERT";
		dbCommand = db.GetStoredProcCommand(sqlCommand, evaluationName, evaluationPercent, evaluationStatus, centerId, teachingYearId, createdBy, createdDate, modifiedBy, modifiedDate);
		
		if (transaction == null)
		this.EvaluationId = Convert.ToInt32(db.ExecuteScalar(dbCommand));
		else
		this.EvaluationId = Convert.ToInt32(db.ExecuteScalar(dbCommand, transaction));
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
	public void Insert(System.String evaluationName, System.Decimal? evaluationPercent, System.Boolean? evaluationStatus, System.Int32? centerId, System.Int32? teachingYearId, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Insert(evaluationName, evaluationPercent, evaluationStatus, centerId, teachingYearId, createdBy, createdDate, modifiedBy, modifiedDate, null);
	}
	/// <summary>
	/// Insert current EvaEvaluations to database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Insert(DbTransaction transaction)
	{
		Insert(EvaluationName, EvaluationPercent, EvaluationStatus, CenterId, TeachingYearId, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, transaction);
	}
	
	/// <summary>
	/// Insert current EvaEvaluations to database.
	/// </summary>
	public void Insert()
	{
		this.Insert((DbTransaction)null);
	}
	#endregion
	
	
	#region UPDATE
	public static void Update(System.Int32? evaluationId, System.String evaluationName, System.Decimal? evaluationPercent, System.Boolean? evaluationStatus, System.Int32? centerId, System.Int32? teachingYearId, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate, DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluations_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@evaluationId"].Value = evaluationId;
		dbCommand.Parameters["@evaluationName"].Value = evaluationName;
		dbCommand.Parameters["@evaluationPercent"].Value = evaluationPercent;
		dbCommand.Parameters["@evaluationStatus"].Value = evaluationStatus;
		dbCommand.Parameters["@centerId"].Value = centerId;
		dbCommand.Parameters["@teachingYearId"].Value = teachingYearId;
		dbCommand.Parameters["@createdBy"].Value = createdBy;
		dbCommand.Parameters["@createdDate"].Value = createdDate;
		dbCommand.Parameters["@modifiedBy"].Value = modifiedBy;
		dbCommand.Parameters["@modifiedDate"].Value = modifiedDate;
		
		if (transaction == null)
		db.ExecuteNonQuery(dbCommand);
		else
		db.ExecuteNonQuery(dbCommand, transaction);
		return;
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
	public static void Update(System.Int32? evaluationId, System.String evaluationName, System.Decimal? evaluationPercent, System.Boolean? evaluationStatus, System.Int32? centerId, System.Int32? teachingYearId, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		Update(evaluationId, evaluationName, evaluationPercent, evaluationStatus, centerId, teachingYearId, createdBy, createdDate, modifiedBy, modifiedDate, null);
	}
	
	public static void Update(EvaEvaluations evaEvaluations)
	{
		evaEvaluations.Update();
	}
	
	public static void Update(EvaEvaluations evaEvaluations, DbTransaction transaction)
	{
		evaEvaluations.Update(transaction);
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Update(DbTransaction transaction)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluations_UPDATE";
		dbCommand = db.GetStoredProcCommand(sqlCommand);
		db.DiscoverParameters(dbCommand);
		dbCommand.Parameters["@evaluationId"].SourceColumn = "EvaluationId";
		dbCommand.Parameters["@evaluationName"].SourceColumn = "EvaluationName";
		dbCommand.Parameters["@evaluationPercent"].SourceColumn = "EvaluationPercent";
		dbCommand.Parameters["@evaluationStatus"].SourceColumn = "EvaluationStatus";
		dbCommand.Parameters["@centerId"].SourceColumn = "CenterId";
		dbCommand.Parameters["@teachingYearId"].SourceColumn = "TeachingYearId";
		dbCommand.Parameters["@createdBy"].SourceColumn = "CreatedBy";
		dbCommand.Parameters["@createdDate"].SourceColumn = "CreatedDate";
		dbCommand.Parameters["@modifiedBy"].SourceColumn = "ModifiedBy";
		dbCommand.Parameters["@modifiedDate"].SourceColumn = "ModifiedDate";
		
		ds = new DataSet();
		this.MapTo( ds );
		ds.AcceptChanges();
		ds.Tables[0].Rows[0].SetModified();
		if (transaction == null)
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, UpdateBehavior.Standard);
		else
		db.UpdateDataSet(ds, TABLE_NAME, null, dbCommand, null, transaction);
		return;
	}
	
	/// <summary>
	/// Updates changes to the database.
	/// </summary>
	public void Update()
	{
		this.Update((DbTransaction)null);
	}
	#endregion
	
	
	#region DELETE
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
	public static void Delete(System.Int32? evaluationId, DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluations_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, evaluationId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
	}
	
	[DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
	public static void Delete(System.Int32? evaluationId)
	{
		Delete(
		evaluationId);
	}
	
	/// <summary>
	/// Delete current EvaEvaluations from database.
	/// </summary>
	/// <param name="transaction">optional SQL Transaction</param>
	public void Delete(DbTransaction transaction)
	{
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluations_DELETE";
		dbCommand = db.GetStoredProcCommand(sqlCommand, EvaluationId);
		
		// Execute.
		if (transaction != null)
		{
			db.ExecuteNonQuery(dbCommand, transaction);
		}
		else
		{
			db.ExecuteNonQuery(dbCommand);
		}
		this.EvaluationId = null;
	}
	
	/// <summary>
	/// Delete current EvaEvaluations from database.
	/// </summary>
	public void Delete()
	{
		this.Delete((DbTransaction)null);
	}
	
	#endregion
	
	
	#region SEARCH
	[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
	public static EvaEvaluations[] Search(System.Int32? evaluationId, System.String evaluationName, System.Decimal? evaluationPercent, System.Boolean? evaluationStatus, System.Int32? centerId, System.Int32? teachingYearId, System.Int32? createdBy, System.DateTime? createdDate, System.Int32? modifiedBy, System.DateTime? modifiedDate)
	{
		DataSet ds;
		Database db;
		string sqlCommand;
		DbCommand dbCommand;
		
		
		db = DatabaseFactory.CreateDatabase();
		sqlCommand = "[dbo].gspEvaEvaluations_SEARCH";
		dbCommand = db.GetStoredProcCommand(sqlCommand, evaluationId, evaluationName, evaluationPercent, evaluationStatus, centerId, teachingYearId, createdBy, createdDate, modifiedBy, modifiedDate);
		
		ds = db.ExecuteDataSet(dbCommand);
		ds.Tables[0].TableName = TABLE_NAME;
		return EvaEvaluations.MapFrom(ds);
	}
	
	
	public static EvaEvaluations[] Search(EvaEvaluations searchObject)
	{
		return Search ( searchObject.EvaluationId, searchObject.EvaluationName, searchObject.EvaluationPercent, searchObject.EvaluationStatus, searchObject.CenterId, searchObject.TeachingYearId, searchObject.CreatedBy, searchObject.CreatedDate, searchObject.ModifiedBy, searchObject.ModifiedDate);
	}
	
	/// <summary>
	/// Returns all EvaEvaluations objects.
	/// </summary>
	/// <returns>List of all EvaEvaluations objects. </returns>
	[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
	public static EvaEvaluations[] Search()
	{
		return Search ( null, null, null, null, null, null, null, null, null, null);
	}
	
	#endregion
	
	
	#endregion
	
	
	#endregion
	
	
}

