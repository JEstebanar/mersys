/****************************************************************************/
/* Code Author (written by Xin Zhao)                                        */
/*                                                                          */
/* This file was automatically generated using Code Author.                 */
/* Any manual changes to this file will be overwritten by a automated tool. */
/*                                                                          */
/* Date Generated: 06/07/2014 12:45:50 PM                                    */
/*                                                                          */
/* www.CodeAuthor.org                                                       */
/****************************************************************************/
/*************************************************/
/* [dbo].gspSysCenterResidences_INSERT */
/*************************************************/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].gspSysCenterResidences_INSERT') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].gspSysCenterResidences_INSERT
GO

CREATE PROCEDURE [dbo].gspSysCenterResidences_INSERT
(
@centerId AS int = null,
@residenceId AS int = null,
@centerResidenceStatus AS bit = null,
@teachingYearId AS int = null
)
AS

INSERT INTO
  [dbo].[sys_CenterResidences]
(
  [CenterId],
  [ResidenceId],
  [CenterResidenceStatus],
  [TeachingYearId]
)
VALUES
(
  @centerId,
  @residenceId,
  @centerResidenceStatus,
  @teachingYearId
)

SELECT SCOPE_IDENTITY()

GO

/*************************************************/
/* [dbo].gspSysCenterResidences_SELECT */
/*************************************************/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].gspSysCenterResidences_SELECT') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].gspSysCenterResidences_SELECT
GO

CREATE PROCEDURE [dbo].gspSysCenterResidences_SELECT
(
@centerResidenceId AS int
)
AS

SELECT
  *
FROM
  [dbo].[sys_CenterResidences]
WHERE
  CenterResidenceId = @centerResidenceId

GO

/*************************************************/
/* [dbo].gspSysCenterResidences_UPDATE */
/*************************************************/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].gspSysCenterResidences_UPDATE') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].gspSysCenterResidences_UPDATE
GO

CREATE PROCEDURE [dbo].gspSysCenterResidences_UPDATE
(
@centerResidenceId int = null,
@centerId int = null,
@residenceId int = null,
@centerResidenceStatus bit = null,
@teachingYearId int = null
)
AS

UPDATE
  [dbo].[sys_CenterResidences]
SET
  [CenterId] = @centerId,
  [ResidenceId] = @residenceId,
  [CenterResidenceStatus] = @centerResidenceStatus,
  [TeachingYearId] = @teachingYearId
WHERE
  [CenterResidenceId] = @centerResidenceId

GO

/*************************************************/
/* [dbo].gspSysCenterResidences_DELETE */
/*************************************************/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].gspSysCenterResidences_DELETE') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].gspSysCenterResidences_DELETE
GO

CREATE PROCEDURE [dbo].gspSysCenterResidences_DELETE
(
@centerResidenceId int
)
AS

DELETE
  [dbo].[sys_CenterResidences]
WHERE
  [CenterResidenceId] = @centerResidenceId

GO

/*************************************************/
/* [dbo].gspSysCenterResidences_SEARCH */
/*************************************************/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].gspSysCenterResidences_SEARCH') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].gspSysCenterResidences_SEARCH
GO

CREATE PROCEDURE [dbo].gspSysCenterResidences_SEARCH
(
@centerResidenceId int = null,
@centerId int = null,
@residenceId int = null,
@centerResidenceStatus bit = null,
@teachingYearId int = null
)
AS

SELECT
  *
FROM
  [dbo].[sys_CenterResidences]
WHERE
  (@centerResidenceId IS NULL OR [CenterResidenceId] = @centerResidenceId)
AND
  (@centerId IS NULL OR [CenterId] = @centerId)
AND
  (@residenceId IS NULL OR [ResidenceId] = @residenceId)
AND
  (@centerResidenceStatus IS NULL OR [CenterResidenceStatus] = @centerResidenceStatus)
AND
  (@teachingYearId IS NULL OR [TeachingYearId] = @teachingYearId)

GO

