ALTER PROCEDURE [dbo].[PosPosts_SEARCH]
(
@postId int = null,
@title varchar(100) = null,
@body text = null,
@postTypeId int = null,
@endDate datetime = null,
@centerId int = null,
@createdBy int = null,
@createdDate datetime = null,
@modifiedBy int = null,
@modifiedDate datetime = null
)
AS

SELECT
  *
FROM
  [dbo].[pos_Posts]
WHERE
  (@postId IS NULL OR [PostId] = @postId)
AND
  (@title IS NULL OR @title = '' OR [Title] LIKE @title + '%')
AND
  (@postTypeId IS NULL OR [PostTypeId] = @postTypeId)
AND
  (@endDate IS NULL OR [EndDate] >= @endDate)
AND
  (@centerId IS NULL OR [CenterId] = @centerId)
AND
  (@createdBy IS NULL OR [CreatedBy] = @createdBy)
AND
  (@createdDate IS NULL OR [CreatedDate] = @createdDate)
AND
  (@modifiedBy IS NULL OR [ModifiedBy] = @modifiedBy)
AND
  (@modifiedDate IS NULL OR [ModifiedDate] = @modifiedDate)
order by EndDate
go
exec PosPosts_SEARCH 